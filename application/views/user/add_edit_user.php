<?php
$view_mode = $mode;
?>
<div id="user-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Add User</h2>
			<ul class="nav navbar-right panel_toolbox">
				<!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                      <li class="dropdown"><a href="#" class="dropdown-toggle"
                            data-toggle="dropdown" role="button" aria-expanded="false"><i
                                    class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                            </ul></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br />
            <?php
												$form_attr = array (
														'name' => 'edit_user_form',
														'id' => 'edit_user_form',
														'method' => 'POST',
														'data-parsley-validate' => '',
														'class' => 'form-horizontal form-label-left' 
												);
												echo form_open ( base_url ( 'user/saveUser' ), $form_attr );
												
												// user id by default is -1
												echo form_input ( array (
														'type' => 'hidden',
														'id' => 'user_id',
														'name' => 'id',
														'value' => ($user_model->get ( 'id' )) ? $user_model->get ( 'id' ) : - 1 
												) );
												?>

            <?php
												if ($user_model->get ( 'userIdentity' )) {
													echo '<div class="form-group">';
													echo '<div class="col-md-6 col-sm-6 col-xs-12">';
													$profile_image = ($user_model->get ( "profileImage" )) ? usr_content_url ( $user_model->get ( "id" ) . '/' . $user_model->get ( "profileImage" ) ) : image_url ( 'app/user.png' );
													echo '<img src="' . $profile_image . '" alt="Profile Image" width="80" height="80" style="margin-left:50%">';
													echo '</div></div>';
													echo '<div class="form-group">';
													echo form_label ( 'User Identity:', 'userIdentity', array (
															'class' => 'control-label col-md-3 col-sm-3 col-xs-12' 
													) );
													
													echo '<div class="col-md-6 col-sm-6 col-xs-12">';
													echo text ( $user_model->get ( 'userIdentity' ) );
													
													echo '</div></div>';
												}
												?>

            <div class="form-group">
            <?php
												// validation for user first name
												echo form_label ( 'First Name:', 'firstName', array (
														'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
												) );
												?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
																// validation for passenger first name
																if ($view_mode == EDIT_MODE) {
																	echo form_input ( array (
																			'id' => 'firstName',
																			'name' => 'firstName',
																			'class' => 'form-control col-md-7 col-xs-12',
																			'required' => 'required',
																			'pattern' => '[A-Za-z\s]{3,15}',
																			'value' => ($user_model->get ( 'firstName' )) ? $user_model->get ( 'firstName' ) : '' 
																	) );
																} else {
																	echo text ( $user_model->get ( 'firstName' ) );
																}
																?>
                </div>

			</div>
			<div class="form-group">
<?php
echo form_label ( 'Last Name:', 'lastName', array (
		'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
) );
?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
																// validation for user last name
																if ($view_mode == EDIT_MODE) {
																	echo form_input ( array (
																			'id' => 'lastName',
																			'name' => 'lastName',
																			'class' => 'form-control col-md-7 col-xs-12',
																			
																			// 'required' => 'required',
																			// 'pattern' => '[A-Za-z\s]{1,15}',
																			'value' => ($user_model->get ( 'lastName' )) ? $user_model->get ( 'lastName' ) : '' 
																	) );
																} else {
																	echo text ( $user_model->get ( 'lastName' ) );
																}
																?>
                </div>

			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'Date of Birth:', 'dob', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
<?php
// validation for user Date of Birth
if ($view_mode == EDIT_MODE) {
	echo form_input ( array (
			'id' => 'dob',
			'name' => 'dob',
			'class' => 'form-control col-md-7 col-xs-12',
			'autocomplete' => 'off',
			//'required' => 'required',
			'value' => ($user_personal_details_model->get ( 'dob' )) ? $user_personal_details_model->get ( 'dob' ) : '' 
	) );
} else {
	echo text ( $user_personal_details_model->get ( 'dob' ) );
}
?>
                </div>
			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'Personal Email:', 'email', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
																				// validation for user Date of Birth
																				if ($view_mode == EDIT_MODE) {
																					echo form_input ( array (
																							'id' => 'email',
																							'name' => 'email',
																							'class' => 'form-control col-md-7 col-xs-12',
																							//'required' => 'required',
																							//'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
																							'value' => ($user_personal_details_model->get ( 'email' )) ? $user_personal_details_model->get ( 'email' ) : '' 
																					) );
																				} else {
																					echo text ( $user_personal_details_model->get ( 'email' ) );
																				}
																				?>
                </div>
			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'Mobile:', 'mobile', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
																				// validation for user Date of Birth
																				if ($view_mode == EDIT_MODE) {
																					echo form_input ( array (
																							'id' => 'mobile',
																							'name' => 'mobile',
																							'class' => 'form-control col-md-7 col-xs-12',
																							'required' => 'required',
																							'pattern' => '[789]\d{9}',
																							'value' => ($user_personal_details_model->get ( 'mobile' )) ? $user_personal_details_model->get ( 'mobile' ) : '' 
																					) );
																				} else {
																					echo text ( $user_personal_details_model->get ( 'mobile' ) );
																				}
																				?>
                </div>
			</div>

			<!--  <div class="form-group">
                    <?php
																				/*
																				 * echo form_label('Date of Releave:', 'dor', array(
																				 * 'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
																				 * ));
																				 */
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
																				// validation for user Date of Birth
																				/*
																				 * if ($view_mode == EDIT_MODE) {
																				 * echo form_input(array(
																				 * 'id' => 'dor',
																				 * 'name' => 'dor',
																				 * 'class' => 'form-control col-md-7 col-xs-12',
																				 * 'value' => ($user_model->get('dob')) ? $user_model->get('dor') : ''
																				 * ));
																				 * } else {
																				 * echo text($user_model->get('dor'));
																				 * }
																				 */
																				?>
                </div>
            </div> -->

			<div class="form-group">
                <?php
																echo form_label ( 'Alternate Mobile:', 'alternateMobile', array (
																		'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
																) );
																?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
																				// validation for user Date of Birth
																				if ($view_mode == EDIT_MODE) {
																					echo form_input ( array (
																							'id' => 'alternateMobile',
																							'name' => 'alternateMobile',
																							'class' => 'form-control col-md-7 col-xs-12',
																							
																							// 'required' => 'required',
																							// 'pattern' => '[789]\d{9}',
																							'value' => ($user_personal_details_model->get ( 'alternateMobile' )) ? $user_personal_details_model->get ( 'alternateMobile' ) : '' 
																					) );
																				} else {
																					echo text ( $user_personal_details_model->get ( 'alternateMobile' ) );
																				}
																				?>
                </div>
			</div>
			<div class="form-group">
                <?php
																echo form_label ( 'Qualification:', 'qualification', array (
																		'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
																) );
																?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
																				// validation for user Date of Birth
																				if ($view_mode == EDIT_MODE) {
																					echo form_input ( array (
																							'id' => 'qualification',
																							'name' => 'qualification',
																							'class' => 'form-control col-md-7 col-xs-12',
																							
																							// 'required' => 'required',
																							'value' => ($user_personal_details_model->get ( 'qualification' )) ? $user_personal_details_model->get ( 'qualification' ) : '' 
																					) );
																				} else {
																					echo text ( $user_personal_details_model->get ( 'qualification' ) );
																				}
																				?>
                </div>
			</div>
			<div class="form-group">
                <?php
																echo form_label ( 'Experience:', 'experience', array (
																		'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
																) );
																?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
																// validation for user Date of Birth
																if ($view_mode == EDIT_MODE) {
																	echo form_input ( array (
																			'id' => 'experience',
																			'name' => 'experience',
																			'class' => 'form-control col-md-7 col-xs-12',
																			//'required' => 'required',
																			'value' => ($user_personal_details_model->get ( 'experience' )) ? $user_personal_details_model->get ( 'experience' ) : '' 
																	) );
																} else {
																	echo text ( $user_personal_details_model->get ( 'experience' ) );
																}
																?>
                </div>
			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'Permanent Address:', 'permanentAddress', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
																// validation for passenger first name
																if ($view_mode == EDIT_MODE) {
																	echo form_textarea ( array (
																			'id' => 'permanentAddress',
																			'name' => 'permanentAddress',
																			'class' => 'form-control',
																			'rows' => '3',
																			//'required' => 'required',
																			'value' => ($user_personal_details_model->get ( 'permanentAddress' )) ? $user_personal_details_model->get ( 'permanentAddress' ) : '' 
																	) );
																} else {
																	echo text ( $user_personal_details_model->get ( 'permanentAddress' ) );
																}
																?>
                </div>
			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'Communication Address:', 'communicationAddress', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
																// validation for passenger first name
																if ($view_mode == EDIT_MODE) {
																	echo form_textarea ( array (
																			'id' => 'communicationAddress',
																			'name' => 'communicationAddress',
																			'class' => 'form-control',
																			'rows' => '3',
																			//'required' => 'required',
																			'value' => ($user_personal_details_model->get ( 'communicationAddress' )) ? $user_personal_details_model->get ( 'communicationAddress' ) : '' 
																	) );
																} else {
																	echo text ( $user_personal_details_model->get ( 'communicationAddress' ) );
																}
																?>
                </div>
			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'Internal Email Id:', 'internalEmail', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
																// validation for user internal email
																if ($view_mode == EDIT_MODE) {
																	echo form_input ( array (
																			'id' => 'internalEmail',
																			'name' => 'internalEmail',
																			'class' => 'form-control col-md-7 col-xs-12',
																			'required' => 'required',
																			'value' => ($user_model->get ( 'internalEmail' )) ? $user_model->get ( 'internalEmail' ) : '' 
																	) );
																} else {
																	echo text ( $user_model->get ( 'internalEmail' ) );
																}
																?>
                </div>
			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'Designation:', 'designation', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
																// validation for user internal email
																if ($view_mode == EDIT_MODE) {
																	echo form_input ( array (
																			'id' => 'designation',
																			'name' => 'designation',
																			'class' => 'form-control col-md-7 col-xs-12',
																			//'required' => 'required',
																			'value' => ($user_model->get ( 'designation' )) ? $user_model->get ( 'designation' ) : '' 
																	) );
																} else {
																	echo text ( $user_model->get ( 'designation' ) );
																}
																?>
                </div>
			</div>

			<div class="form-group">
                    <?php
																				echo form_label ( 'Date of Joining:', 'doj', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
																				// validation for user Date of Birth
																				if ($view_mode == EDIT_MODE) {
																					echo form_input ( array (
																							'id' => 'doj',
																							'name' => 'doj',
																							'class' => 'form-control col-md-7 col-xs-12',
																							'autocomplete' => 'off',
																							//'required' => 'required',
																							'value' => ($user_model->get ( 'doj' )) ? $user_model->get ( 'doj' ) : '' 
																					) );
																				} else {
																					echo text ( $user_model->get ( 'doj' ) );
																				}
																				?>
                </div>
			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'Entity Name:', 'companyId', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
<?php
// validation for passenger first name
if ($view_mode == EDIT_MODE) {
	echo form_dropdown ( 'companyId', $company_list, $user_model->get ( 'companyId' ) ? $user_model->get ( 'companyId' ) : DEFAULT_COMPANY_ID, array (
			'id' => 'companyId',
			'class' => 'form-control',
			'required' => 'required' 
	) );
} else {
	echo text ( $user_model->get ( 'companyId' ) );
}
?>
                </div>
			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'City Name:', 'cityId', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
																				// validation for passenger first name
																				if ($view_mode == EDIT_MODE) {
																					echo form_dropdown ( 'cityId', $city_list, $user_model->get ( 'cityId' ), array (
																							'id' => 'cityId',
																							'class' => 'form-control',
																							'required' => 'required' 
																					) );
																				} else {
																					echo text ( $city_list [$user_model->get ( 'cityId' )] );
																				}
																				?>
                </div>
			</div>
			<div class="form-group">
                    <?php
																				echo form_label ( 'Role Type:', 'roleType', array (
																						'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
																				) );
																				?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
																				// validation for passenger first name
																				if ($view_mode == EDIT_MODE) {
																					echo form_dropdown ( 'roleType', $role_type_list, $user_model->get ( 'roleType' ), array (
																							'id' => 'roleType',
																							'class' => 'form-control',
																							'required' => 'required' 
																					) );
																				} else {
																					echo text ( $role_type_list [$user_model->get ( 'roleType' )] );
																				}
																				?>
                </div>
			</div>

			<div class="ln_solid"></div>
			<div
				<?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<button type="button" id="load_user_btn" class="btn btn-primary">Cancel</button>
					<button type="button" id="save_user_btn" class="btn btn-success"
						data-loading-text="<i  class='fa fa-spinner fa-spin '>
						</i> Processing" onclick="saveUser();">Save
					</button>
				</div>
			</div>
            <?php  if($view_mode == VIEW_MODE) : ?>
            <div class="form-group">
				<center>
					<button type="button" id="load_user_btn" class="btn btn-primary">Back</button>
				</center>
			</div>  
            <?php endif ; ?>
                    <?php
																				echo form_close ();
																				?>
        </div>
	</div>
</div>

