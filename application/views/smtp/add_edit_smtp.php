<?php
$view_mode = $mode;
?>
<div id="smtp-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Add SMTP Settings</h2>
			<ul class="nav navbar-right panel_toolbox">
				<!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br />
			<?php
			$form_attr = array (
					'name' => 'edit_smtp_form',
					'id' => 'edit_smtp_form',
					'method' => 'POST',
					'data-parsley-validate' => '',
					'class' => 'form-horizontal form-label-left' 
			);
			echo form_open ( base_url ( 'smtp/saveSmtp' ), $form_attr );
			
			// passenger id by default is -1
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'smtp_id',
					'name' => 'id',
					'value' => ($smtp_model->get ( 'id' )) ? $smtp_model->get ( 'id' ) :-1 
			) );
			
			?>
			<?php
			/*
			 * if ($view_mode == VIEW_MODE || $passenger_model->get ( 'passengerCode' )) {
			 * echo '<div class="form-group">';
			 *
			 * echo form_label ( 'Passenger Referral Code:', 'passengerCode', array (
			 * 'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
			 * ) );
			 *
			 * echo '<div class="col-md-6 col-sm-6 col-xs-12">';
			 * echo text ( $passenger_model->get ( 'passengerCode' ) );
			 *
			 * echo '</div></div>';
			 * }
			 * if ($view_mode == VIEW_MODE || $passenger_model->get ( 'lastLoggedIn' )) {
			 * echo '<div class="form-group">';
			 *
			 * echo form_label ( 'Last Logged In:', 'lastLoggedIn', array (
			 * 'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
			 * ) );
			 *
			 * echo '<div class="col-md-6 col-sm-6 col-xs-12">';
			 * echo text ( $passenger_model->get ( 'lastLoggedIn' ) );
			 *
			 * echo '</div></div>';
			 * }
			 */
			?>
			
			

			<div class="form-group">
					<?php
					echo form_label ( 'SMTP Host:', 'smtpHost', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'smtpHost',
									'name' => 'smtpHost',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z0-9.-#?\s]',
									'value' => ($smtp_model->get ( 'smtpHost' )) ? $smtp_model->get ( 'smtpHost' ) : '' 
							) );
						} else {
							echo text ( $smtp_model->get ( 'smtpHost' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'SMTP Username:', 'smtpUsername', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'smtpUsername',
									'name' => 'smtpUsername',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z0-9\s]',
									'value' => ($smtp_model->get ( 'smtpUsername' )) ? $smtp_model->get ( 'smtpUsername' ) : '' 
							) );
						} else {
							echo text ( $smtp_model->get ( 'smtpUsername' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'SMTP Transport Layer Security:', 'smtpTransportLayerSecurity', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'smtpTransportLayerSecurity',
									'name' => 'smtpTransportLayerSecurity',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z0-9\s]',
									'value' => ($smtp_model->get ( 'smtpTransportLayerSecurity' )) ? $smtp_model->get ( 'smtpTransportLayerSecurity' ) : '' 
							) );
						} else {
							echo text ( $smtp_model->get ( 'smtpTransportLayerSecurity' ) );
						}
						?>
					</div>
			</div>
			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<button type="submit" id="cancel_smtp_btn" class="btn btn-primary">Cancel</button>
					<button type="submit" id="save_smtp_btn" class="btn btn-success">Save</button>
				</div>
			</div>

			<?php
			echo form_close ();
			?>
		</div>
	</div>
</div>