<?php
$view_mode = $mode;
?>
<div id="driver-details-information"
	class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>
				Site Setting<small>Genreal</small>
			</h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br />
      <?php
						$form_attr = array (
								'name' => 'edit_site_form',
								'id' => 'edit_site_form',
								'class' => 'form-horizontal form-label-left',
								'data-parsley-validate' => '',
								'method' => 'POST' 
						);
						echo form_open_multipart ( base_url ( 'site/saveSite' ), $form_attr );
						
						// driver id by default is -1
						echo form_input ( array (
								'type' => 'hidden',
								'id' => 'site_id',
								'name' => 'id',
								'value' => ($site_model->get ( 'id' )) ? $site_model->get ( 'id' ) :-1 
						) );
						?>

      
         <div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              
               <?php
															// validation for site Title
															echo form_label ( 'Title:', 'title', array (
																	'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
															) );
															
															if ($view_mode == EDIT_MODE) {
																echo form_input ( array (
																		'id' => 'title',
																		'name' => 'title',
																		'class' => 'form-control',
																		'required' => 'required',
																		'pattern'=>'[a-zA-Z\s]',
																		'value' => ($site_model->get ( 'title' )) ? $site_model->get ( 'title' ) : '' 
																) );
															} else {
																echo text ( $site_model->get ( 'title' ) );
															}
															?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
               <?php
															// validation for Site tag line
															echo form_label ( 'Tag Line:', 'tagLine', array (
																	'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
															) );
															if ($view_mode == EDIT_MODE) {
																echo form_input ( array (
																		'id' => 'tagLine',
																		'name' => 'tagLine',
																		'class' => 'form-control',
																		'required' => 'required',
																		'pattern'=>'[a-zA-Z\s]',
																		'value' => ($site_model->get ( 'tagLine' )) ? $site_model->get ( 'tagLine' ) : '' 
																) );
															} else {
																echo text ( $site_model->get ( 'tagLine' ) );
															}
															?>
            </div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation site 	description
												echo form_label ( 'Description:', 'description', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_textarea ( array (
															'id' => 'description',
															'name' => 'description',
															'class' => 'form-control',
															'required' => 'required',
															'rows' => '3',
															'value' => ($site_model->get ( 'description' )) ? $site_model->get ( 'description' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'description' ) );
												}
												
												?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            
            	 <?php
												// validation site 	metaKeyword
												echo form_label ( 'Meta Keywords:', 'metaKeyword', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_textarea ( array (
															'id' => 'metaKeyword',
															'name' => 'metaKeyword',
															'class' => 'form-control',
															'required' => 'required',
															'rows' => '3',
															'value' => ($site_model->get ( 'metaKeyword' )) ? $site_model->get ( 'metaKeyword' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'metaKeyword' ) );
												}
												
												?>
            </div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site email
												echo form_label ( 'Email:', 'email', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'email',
															'name' => 'email',
															'class' => 'form-control',
															'required' => 'required',
															'pattern'=>'[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}',
															'value' => ($site_model->get ( 'email' )) ? $site_model->get ( 'email' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'email' ) );
												}
												?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site Contact No
												echo form_label ( 'Contact No:', 'contactNo', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'contactNo',
															'name' => 'contactNo',
															'class' => 'form-control',
															'required' => 'required',
															'placeholder'=>'Start Landline with STD code/Mobile with zero(0)',
															'pattern'=>'[0]\d{9,10}',
															'value' => ($site_model->get ( 'contactNo' )) ? $site_model->get ( 'contactNo' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'contactNo' ) );
												}
												?>
            </div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site Emergency Email
												echo form_label ( 'Emergency Email:', 'emergencyEmail', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'emergencyEmail',
															'name' => 'emergencyEmail',
															'class' => 'form-control',
															'required' => 'required',
															'pattern'=>'[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}',
															'value' => ($site_model->get ( 'emergencyEmail' )) ? $site_model->get ( 'emergencyEmail' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'emergencyEmail' ) );
												}
												?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site Emergency Contact No
												echo form_label ( 'Emergency Contact No:', 'emergencyContactNo', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'emergencyContactNo',
															'name' => 'emergencyContactNo',
															'class' => 'form-control',
															'required' => 'required',
															'placeholder'=>'Start Landline with STD code/Mobile with zero(0)',
															'pattern'=>'[0]\d{9,10}',
															'value' => ($site_model->get ( 'emergencyContactNo' )) ? $site_model->get ( 'emergencyContactNo' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'emergencyContactNo' ) );
												}
												?>
            </div>
			</div>
			<div class="form-group">
				<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site Send SMS flag
												
												echo form_label ( 'Send SMS:', 'sendSms', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													
														echo '<p>';
														
															// validation for pm to show only delete buttons & admin for their draft release only delete buttons
															
															echo 'Yes:';
															
															echo form_radio ( array (
																	'id' => 'sendSmsY',
																	'name' => 'sendSms',
																	'class' => 'flat',
																	'value' => 'Y',
																	'checked' => ($site_model->get ( 'sendSms' )=='Y') ? 'true' : 'false' 
															) );
															
															echo 'No:';
																
															echo form_radio ( array (
																	'id' => 'sendSmsN',
																	'name' => 'sendSms',
																	'class' => 'flat',
																	'value' => 'N',
																	'checked' => ($site_model->get ( 'sendSms' )=='N') ? 'true' : 'false'
															) );
														
														echo '</p>';
													
												} else {
													echo text ( $site_model->get ( 'sendSms' ) );
												}
												
												?>
               
            </div>
				<div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for driver DOB
												echo form_label ( 'Toll Free No:', 'tollFreeNo', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'tollFreeNo',
															'name' => 'tollFreeNo',
															'class' => 'form-control',
															'placeholder'=>'Start Landline with STD code/Mobile with zero(0)',
															'pattern'=>'[0]\d{9,10}',
															'value' => ($site_model->get ( 'tollFreeNo' )) ? $site_model->get ( 'tollFreeNo' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'tollFreeNo' ) );
												}
												
												?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
				 <?php
												// validation for driver notification Timeout
												echo form_label ( 'Notification Timeout:', 'notificationTimeout', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'notificationTimeout',
															'name' => 'notificationTimeout',
															'class' => 'form-control',
															'required' => 'required',
															'pattern'=>'\d{2}',
															'value' => ($site_model->get ( 'notificationTimeout' )) ? $site_model->get ( 'notificationTimeout' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'notificationTimeout' ) );
												}
												
												?>
					
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              
               <?php
															// validation for site adminCommission
															echo form_label ( 'Admin Commission:', 'adminCommission', array (
																	'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
															) );
															
															if ($view_mode == EDIT_MODE) {
																echo form_input ( array (
																		'id' => 'adminCommission',
																		'name' => 'adminCommission',
																		'class' => 'form-control',
																		'required' => 'required',
																		'pattern'=>'\d{1,4}\.?\d{2}',
																		'value' => ($site_model->get ( 'adminCommission' )) ? $site_model->get ( 'adminCommission' ) : '' 
																) );
															} else {
																echo text ( $site_model->get ( 'adminCommission' ) );
															}
															?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
               <?php
															// validation for Site referralDiscountAmount
															echo form_label ( 'Referral Discount Amount:', 'referralDiscountAmount', array (
																	'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
															) );
															if ($view_mode == EDIT_MODE) {
																echo form_input ( array (
																		'id' => 'referralDiscountAmount',
																		'name' => 'referralDiscountAmount',
																		'class' => 'form-control',
																		'required' => 'required',
																		'pattern'=>'\d{1,4}\.?\d{2}',
																		'value' => ($site_model->get ( 'referralDiscountAmount' )) ? $site_model->get ( 'referralDiscountAmount' ) : '' 
																) );
															} else {
																echo text ( $site_model->get ( 'referralDiscountAmount' ) );
															}
															?>
            </div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              
               <?php
															// validation for site androidAppUrl
															echo form_label ( 'Android APP URL:', 'androidAppUrl', array (
																	'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
															) );
															
															if ($view_mode == EDIT_MODE) {
																echo form_input ( array (
																		'id' => 'androidAppUrl',
																		'name' => 'androidAppUrl',
																		'class' => 'form-control',
																		'required' => 'required',
																		
																		'value' => ($site_model->get ( 'androidAppUrl' )) ? $site_model->get ( 'androidAppUrl' ) : '' 
																) );
															} else {
																echo text ( $site_model->get ( 'androidAppUrl' ) );
															}
															?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
               <?php
															// validation for Site iphoneAppUrl
															echo form_label ( 'Iphone APP URL:', 'iphoneAppUrl', array (
																	'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
															) );
															if ($view_mode == EDIT_MODE) {
																echo form_input ( array (
																		'id' => 'iphoneAppUrl',
																		'name' => 'iphoneAppUrl',
																		'class' => 'form-control',
																		'required' => 'required',
																		'value' => ($site_model->get ( 'iphoneAppUrl' )) ? $site_model->get ( 'iphoneAppUrl' ) : '' 
																) );
															} else {
																echo text ( $site_model->get ( 'iphoneAppUrl' ) );
															}
															?>
            </div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
              
               <?php
															// validation for site tax
															echo form_label ( 'Tax:', 'tax', array (
																	'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
															) );
															
															if ($view_mode == EDIT_MODE) {
																echo form_input ( array (
																		'id' => 'tax',
																		'name' => 'tax',
																		'class' => 'form-control',
																		'required' => 'required',
																		'pattern'=>'\d{1,4}\.?\d{2}',
																		'value' => ($site_model->get ( 'tax' )) ? $site_model->get ( 'tax' ) : '' 
																) );
															} else {
																echo text ( $site_model->get ( 'tax' ) );
															}
															?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
               <?php
															// validation for Site copyrights
															echo form_label ( 'Copy Rights:', 'copyrights', array (
																	'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
															) );
															if ($view_mode == EDIT_MODE) {
																echo form_input ( array (
																		'id' => 'copyrights',
																		'name' => 'copyrights',
																		'class' => 'form-control',
																		'required' => 'required',
																		'pattern'=>'[a-zA-Z0-9@.]',
																		'value' => ($site_model->get ( 'copyrights' )) ? $site_model->get ( 'copyrights' ) : '' 
																) );
															} else {
																echo text ( $site_model->get ( 'copyrights' ) );
															}
															?>
            </div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation site 	passenger TellToFriendMsg
												echo form_label ( 'Passenger Tell To Friend Msg:', 'passengerTellToFriendMsg', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_textarea ( array (
															'id' => 'passengerTellToFriendMsg',
															'name' => 'passengerTellToFriendMsg',
															'class' => 'form-control',
															'required' => 'required',
															'rows' => '3',
															'value' => ($site_model->get ( 'passengerTellToFriendMsg' )) ? $site_model->get ( 'passengerTellToFriendMsg' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'passengerTellToFriendMsg' ) );
												}
												
												?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            
            	 <?php
												// validation site 	driverTellToFriendMsg
												echo form_label ( 'Driver Tell To Friend Msg:', 'driverTellToFriendMsg', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_textarea ( array (
															'id' => 'driverTellToFriendMsg',
															'name' => 'driverTellToFriendMsg',
															'class' => 'form-control',
															'required' => 'required',
															'rows' => '3',
															'value' => ($site_model->get ( 'driverTellToFriendMsg' )) ? $site_model->get ( 'driverTellToFriendMsg' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'driverTellToFriendMsg' ) );
												}
												
												?>
            </div>
			</div>
			
			<div class="form-group">

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site logo 
												echo form_label ( 'Logo:', 'logo', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_upload ( array (
															'id' => 'logo',
															'name' => 'logo',
															'class' => 'form-control',
															'required' => 'required',
															'value' => ($site_model->get ( 'logo' )) ? $site_model->get ( 'logo' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'logo' ) );
												}
												
												?>
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site emailLogo
												echo form_label ( 'Email Logo:', 'emailLogo', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_upload ( array (
															'id' => 'emailLogo',
															'name' => 'emailLogo',
															'class' => 'form-control',
															'required' => 'required',
															'value' => ($site_model->get ( 'emailLogo' )) ? $site_model->get ( 'emailLogo' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'emailLogo' ) );
												}
												?>
               
            </div>
			</div>
			<div class="form-group">

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site favicon
												echo form_label ( 'Favicon:', 'favicon', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_upload ( array (
															'id' => 'favicon',
															'name' => 'favicon',
															'class' => 'form-control',
															'required' => 'required',
															'value' => ($site_model->get ( 'favicon' )) ? $site_model->get ( 'favicon' ) : '' 
													) );
												} else {
													echo text ( $site_model->get ( 'favicon' ) );
												}
												
												?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
           
               <?php
												// validation for site Country Name
												echo form_label ( 'Country Name:', 'countryId', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_dropdown ( 'countryId', $country_list, $site_model->get ( 'countryId' ), array (
															'id' => 'countryId',
															'class' => 'form-control',
															'required' => 'required' 
													) );
												} else {
													echo text ($country_list[$site_model->get ( 'countryId' )] );
												}
												
												?>
            </div>
			</div>
			
			<div class="form-group">

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site State Name
												echo form_label ( 'State Name:', 'stateId', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_dropdown ( 'stateId', $state_list, $site_model->get ( 'stateId' ), array (
															'id' => 'stateId',
															'class' => 'form-control',
															'required' => 'required' 
													) );
												} else {
													echo text ( $state_list[$site_model->get ( 'stateId' )]);
												}
												
												?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for driver City Name
												echo form_label ( 'City Name:', 'cityId', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_dropdown ( 'cityId', $city_list, $site_model->get ( 'cityId' ), array (
															'id' => 'companyId',
															'class' => 'form-control',
															'required' => 'required' 
													) );
												} else {
													echo text ( $city_list[$site_model->get ( 'cityId' )]);
												}
												
												?>
               
            </div>
			</div>
			
			
			
			<div class="x_title">
			<h2>
				Site Setting<small>Social Media</small>
			</h2>
				
				<div class="clearfix"></div>
			</div>
			<?php foreach ($site_social_media_model as $list):;?>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php 
												// validation for site social media userkey
												echo form_label ( 'User Key:', 'userkey', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'userkey',
															'name' => 'userkey',
															'class' => 'form-control',
															'required' => 'required',
															'pattern'=>'[a-zA-Z0-9\s]',
															'value' => ($list->userkey) ? $list->userkey : '' 
													) );
												} else {
													echo text ( $list->userkey);
												}
												
												?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
             <?php
												// validation for site social media secretkey
												echo form_label ( 'Secret key:', 'userkey', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'secretkey',
															'name' => 'secretkey',
															'class' => 'form-control',
															'required' => 'required',
															'pattern'=>'[a-zA-Z0-9.-!@#$*_]',
															'value' => ($list->secretkey) ? $list->secretkey : '' 
													) );
												} else {
													echo text ( $list->secretkey);
												}
												
												?>
               
            </div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
            <?php
												// validation for site social media shareUrl
												echo form_label ( 'Share Url:', 'shareUrl', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'shareUrl',
															'name' => 'shareUrl',
															'class' => 'form-control',
															'required' => 'required',
															'value' => ($list->shareUrl) ? $list->shareUrl: '' 
													) );
												} else {
													echo text ( $list->shareUrl);
												}
												
												?>
               
            </div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
             <?php
												// validation for site social media name
												echo form_label ( 'Share Name:', 'name', array (
														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
												) );
												if ($view_mode == EDIT_MODE) {
													echo form_input ( array (
															'id' => 'name',
															'name' => 'name',
															'class' => 'form-control',
															'required' => 'required',
															'pattern'=>'[a-zA-Z\s]',
															'value' => ($list->name) ? $list->name: '' 
													) );
												} else {
													echo text ( $list->name);
												}
												
												?>
               
            </div>
			</div>
			<?php endforeach;?>
			<div class="ln_solid"></div>
			<div class="form-group">
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
					<button type="submit" id="cancel_site_btn" class="btn btn-primary">Cancel</button>
					<button type="submit" id="save_site_btn" class="btn btn-success">Submit</button>
				</div>
			</div>
			<?php 
			echo form_close ();
			?>
		</div>
	</div>
</div>

