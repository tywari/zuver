<div id="smtp-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>
				Site List
			</h2>
			<!--<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				 <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li> 
				
			</ul>
			<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" id="search_passenger_content" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="search_passenger_btn" type="button">Go!</button>
                    </span>
                    
                  </div>
                  <div><span>Search by Referral Code, Passenger Full Name, Email and Mobile</span></div>
                </div>
             </div>
             <div class="title_right pull-right">
                
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<a href="<?php //echo base_url('smtp/add')?>"><button id="add_smtp_btn"
						class="btn btn-primary">Add SMTP</button></a>
					
				</div>
			</div>-->
			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			<table id="datatable-checkbox"
				class="table table-striped table-bordered datatable-button-init-collection">
				<thead>
					<tr>
						<th><!-- <input type="checkbox" id="check-all" class="flat"> --></th>
						<!-- <th>Sl no</th> -->
						<th>Action</th>
						<th>Site Title</th>
						<th>Site Description</th>
						<th>Site Email</th>
						<th>Site Contact</th>
						<th>Site Admin Commission</th>
						<th>Site Refferal Discount</th>
						
					</tr>
				</thead>


				<tbody>
				<?php 
				//sl no intialization
				//$i=1;
				//$smtp_model_list=array(0=>array('smtpHost'=>'AWS','smtpUsername'=>'Zuver','smtpTransportLayerSecurity'=>'21','status'=>1));
				foreach ($site_model_list as $list)
				{
					
				echo '<tr>';
					
				echo '<td></td>';//<input type="checkbox" class="flat" name="table_records">
				//echo '<th>'.$i.'</th>';
				echo '<td>'
						. '<a href="javascript:void(0);" id="viewSite-'.$list->id.'" onclick="viewSite(this.id);" class="userlink" >'
								. '<span class="glyphicon glyphicon-eye-open"></span></a> '
										. '/ <a href="javascript:void(0);" id="editSite-'.$list->id.'" onclick="editSite(this.id);" class="userlink" >'
												. '<span class="glyphicon glyphicon-edit"></span></a>'
														. '</td>';
				echo '<td>'.$list->title.'</td>';
				echo '<td>'.$list->description.'</td>';
				echo '<td>'.$list->email.'</td>';
				echo '<td>'.$list->contactNo.'</td>';
				echo '<td>'.$list->adminCommission.'%</td>';
				echo '<td>'.$list->referralDiscountAmount.'</td>';
				
				//Status toogle
				//$active=($list->status==Status_Type_Enum::ACTIVE)?'btn-primary active':'btn-default" onclick="changeSmtpStatus(this.id);';
				//$deactive=($list->status==Status_Type_Enum::INACTIVE)?'btn-primary active':'btn-default" onclick="changeSmtpStatus(this.id);';
				/* echo '<td> <ul class="list-inline"> <li>';
				echo '<div class="btn-group btn-toggle">';
				echo '<button id="active-'.$list->id.'" class="btn btn-xs '.$active.'">Active</button>';
				echo '<button id="deactive-'.$list->id.'" class="btn btn-xs '.$deactive.'">Deactive</button>'; 
				
				echo '</div></li></ul></td>';*/
				//echo '<td><div class="checkbox">';
				//echo '<label> <input type="checkbox" '.($list['status'])?"checked=checked":"checked".' data-toggle="toggle" class="flat">'.($list['status'])?'Enabled':'Disabled'.'</label>';
				//echo '</div></td>';
				
				echo '</tr>';
				//$i++;
				}
	            ?>
				</tbody>
			</table>
		</div>
	</div>
</div>