<?php
$view_mode = $mode;
?>
<div id="menu-details-information"
	class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Add Web Link</h2>
			<ul class="nav navbar-right panel_toolbox">
				<!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br />
			<?php
			$form_attr = array (
					'name' => 'edit_menu_form',
					'id' => 'edit_menu_form',
					'method' => 'POST',
					'data-parsley-validate' => '',
					'class' => 'form-horizontal form-label-left' 
			);
			echo form_open ( base_url ( 'menu/saveMenu' ), $form_attr );
			
			// passenger id by default is -1
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'menu_id',
					'name' => 'id',
					'value' => ($menu_model->get ( 'id' )) ? $menu_model->get ( 'id' ) :-1 
			) );
			
			?>
			<div class="form-group">
					<?php
					echo form_label ( 'Menu Name:', 'name', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'name',
									'name' => 'name',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z\s]',
									'value' => ($menu_model->get ( 'name' )) ? $menu_model->get ( 'name' ) : '' 
							) );
						} else {
							echo text ( $menu_model->get ( 'name' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Menu Title:', 'title', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'title',
									'name' => 'title',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z\s]',
									'value' => ($menu_model->get ( 'title' )) ? $menu_model->get ( 'title' ) : '' 
							) );
						} else {
							echo text ( $menu_model->get ( 'title' ) );
						}
						?>
					</div>

			</div>
				<div class="form-group">
					<?php
					echo form_label ( 'Menu Description:', 'description', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_textarea( array (
									'id' => 'description',
									'name' => 'description',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'rows' => '3',
									'pattern'=>'[a-zA-Z0-9.#,-\s]',
									'value' => ($menu_model->get ( 'description' )) ? $menu_model->get ( 'description' ) : '' 
							) );
						} else {
							echo text ( $menu_model->get ( 'description' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Menu Meta Keyword:', 'metaKeyword', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_textarea( array (
									'id' => 'metaKeyword',
									'name' => 'metaKeyword',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z0-9.#,-\s]',
									'rows' => '3',
									'value' => ($menu_model->get ( 'metaKeyword' )) ? $menu_model->get ( 'metaKeyword' ) : '' 
							) );
						} else {
							echo text ( $menu_model->get ( 'metaKeyword' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Menu Link Name:', 'linkName', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'linkName',
									'name' => 'linkName',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z\s]',
									'value' => ($menu_model->get ( 'linkName' )) ? $menu_model->get ( 'linkName' ) : '' 
							) );
						} else {
							echo text ( $menu_model->get ( 'linkName' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Menu Content:', 'content', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_textarea( array (
									'id' => 'content',
									'name' => 'content',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'rows' => '3',
									'value' => ($menu_model->get ( 'content' )) ? $menu_model->get ( 'content' ) : '' 
							) );
						} else {
							//echo text ( $menu_model->get ( 'content' ) );
							echo form_textarea( array (
									'id' => 'content',
									'name' => 'content',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'readonly'=>'readonly',
									'rows' => '5',
									'value' => ($menu_model->get ( 'content' )) ? $menu_model->get ( 'content' ) : ''
							) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Sequence Order:', 'name', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'sequenceOrder',
									'name' => 'sequenceOrder',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'/d{1,3}',
									'value' => ($menu_model->get ( 'sequenceOrder' )) ? $menu_model->get ( 'sequenceOrder' ) : '' 
							) );
						} else {
							echo text ( $menu_model->get ( 'sequenceOrder' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Parent Menu Name:', 'menuId', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_dropdown ( 'menuId', $menu_list, $menu_model->get ( 'menuId' ),array('id' =>'menuId', 'class' =>'form-control'));
						} else {
							echo text ( $menu_model->get ( 'menuId' )?$menu_list[$menu_model->get ( 'menuId' )]:'');
						}
						
						?>
					</div>
			</div>

			<div class="ln_solid"></div>
			<div <?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<button type="button" id="cancel_menu_btn"
						class="btn btn-primary">Cancel</button>
					
					<button type="button" id="save_menu_btn"
						class="btn btn-success" onclick="endTrip();">Save</button>
					 
				</div>
			</div>
			<?php  if($view_mode == VIEW_MODE) : ?>
                        <div  class="form-group">
                            <center><button type="button" id="cancel_menu_btn" class="btn btn-primary">Back</button></center>
                        </div>  
                      <?php endif ; ?>  
			<?php
			echo form_close ();
			?>
		</div>
	</div>
</div>