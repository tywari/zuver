<?php
$view_mode = $mode;
?>
<div id="state-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Add State</h2>
			<ul class="nav navbar-right panel_toolbox">
				<!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br />
			<?php
			$form_attr = array (
					'name' => 'edit_state_form',
					'id' => 'edit_state_form',
					'method' => 'POST',
					'data-parsley-validate' => '',
					'class' => 'form-horizontal form-label-left' 
			);
			echo form_open ( base_url ( '' ), $form_attr );
			
			// passenger id by default is -1
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'state_id',
					'name' => 'id',
					'value' => ($state_model->get ( 'id' )) ? $state_model->get ( 'id' ) :-1 
			) );
			
			?>
			<?php
			/*
			 * if ($view_mode == VIEW_MODE || $passenger_model->get ( 'passengerCode' )) {
			 * echo '<div class="form-group">';
			 *
			 * echo form_label ( 'Passenger Referral Code:', 'passengerCode', array (
			 * 'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
			 * ) );
			 *
			 * echo '<div class="col-md-6 col-sm-6 col-xs-12">';
			 * echo text ( $passenger_model->get ( 'passengerCode' ) );
			 *
			 * echo '</div></div>';
			 * }
			 * if ($view_mode == VIEW_MODE || $passenger_model->get ( 'lastLoggedIn' )) {
			 * echo '<div class="form-group">';
			 *
			 * echo form_label ( 'Last Logged In:', 'lastLoggedIn', array (
			 * 'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
			 * ) );
			 *
			 * echo '<div class="col-md-6 col-sm-6 col-xs-12">';
			 * echo text ( $passenger_model->get ( 'lastLoggedIn' ) );
			 *
			 * echo '</div></div>';
			 * }
			 */
			?>
			<div class="form-group">
					<?php
					echo form_label ( 'Country Name:', 'countryId', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_dropdown ( 'countryId', $country_list, $state_model->get ( 'countryId' ), array (
									'id' => 'countryId',
									'class' => 'form-control',
									'required' => 'required' 
							) );
						} else {
							echo text ( $country_list[$state_model->get ( 'countryId' )] );
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'State Name:', 'name', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'name',
									'name' => 'name',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[A-Za-z\s]{1,20}',
									'value' => ($state_model->get ( 'name' )) ? $state_model->get ( 'name' ) : '' 
							) );
						} else {
							echo text ( $state_model->get ( 'name' ) );
						}
						?>
					</div>

			</div>
						<div class="ln_solid"></div>
			<div <?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<button type="button" id="load_state_btn" class="btn btn-primary">Cancel</button>
					<button type="button" id="save_state_btn" class="btn btn-success" onclick="saveState();">Save</button>
				</div>
			</div>
                        <?php  if($view_mode == VIEW_MODE) : ?>
                        <div  class="form-group">
                            <center><button type="button" id="load_state_btn" class="btn btn-primary" >Back</button></center>
                        </div>  
                      <?php endif ; ?>                         
			<?php
			echo form_close ();
			?>
		</div>
	</div>
</div>