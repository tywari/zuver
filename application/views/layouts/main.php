<!DOCTYPE html>
<html lang="en">
<head>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $template['title']; ?></title>

    <!-- Bootstrap -->
    <link
            href="<?php echo js_url('vendors/bootstrap/dist/css/bootstrap.min.css'); ?>"
            rel="stylesheet">
    <!-- Font Awesome -->
    <link
            href="<?php echo js_url('vendors/font-awesome/css/font-awesome.min.css'); ?>"
            rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo js_url('vendors/nprogress/nprogress.css'); ?>"
          rel="stylesheet">
    <!-- iCheck -->
    <link
            href="<?php echo js_url('vendors/iCheck/skins/flat/green.css'); ?>"
            rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link
            href="<?php echo js_url('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css'); ?>"
            rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo js_url('vendors/jqvmap/dist/jqvmap.min.css'); ?>"
          rel="stylesheet"/>
    <!-- Datatables -->
    <link
            href="<?php echo js_url('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>"
            rel="stylesheet"/>
    <link
            href="<?php echo js_url('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>"
            rel="stylesheet"/>
    <link
            href="<?php echo js_url('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>"
            rel="stylesheet"/>
    <link
            href="<?php echo js_url('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>"
            rel="stylesheet"/>
    <link
            href="<?php echo js_url('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>"
            rel="stylesheet"/>
    <link
            href="<?php echo js_url('vendors/datepicker/css/jquery.datetimepicker.css'); ?>"
            rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo css_url('app/custom.css'); ?>" rel="stylesheet">

    <link href="<?php echo css_url('app/icon-style.css'); ?>" rel="stylesheet">
    <link href="<?php echo css_url('app/components.css'); ?>" rel="stylesheet">
    <link href="<?php echo css_url('app/jquery.timepicker.css'); ?>" rel="stylesheet">
    <!-- jQuery -->

    <script>var BASE_URL = '<?php echo base_url() ?>';</script>
    <script src="<?php echo js_url('vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Bootstrap -->
    <script
            src="<?php echo js_url('vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- FastClick -->
    <script
            src="<?php echo js_url('vendors/fastclick/lib/fastclick.js'); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo js_url('vendors/nprogress/nprogress.js'); ?>"></script>

    <!-- bootstrap-progressbar -->
    <script
            src="<?php echo js_url('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>"></script>
    <!-- iCheck -->
    <script src="<?php echo js_url('vendors/iCheck/icheck.min.js'); ?>"></script>
    <!-- Skycons -->
    <script src="<?php echo js_url('vendors/skycons/skycons.js'); ?>"></script>

    <!-- DateJS -->
    <script src="<?php echo js_url('vendors/DateJS/build/date.js'); ?>"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo js_url('moment/moment.min.js'); ?>"></script>
    <script src="<?php echo js_url('datepicker/daterangepicker.js'); ?>"></script>
    <script src="<?php echo js_url('datepicker/jquery.datetimepicker.full.js'); ?>"></script>


    <!--- Trip Js -->
    <script src="<?php echo js_url('app/trip.js'); ?>" type="text/javascript"></script>

    <!-- Flot -->
    <!--<script src="<?php //echo js_url('vendors/Flot/jquery.flot.js');  ?>"></script>
        <script src="<?php //echo js_url('vendors/Flot/jquery.flot.pie.js');  ?>"></script>
        <script
                src="<?php //echo js_url('vendors/Flot/jquery.flot.time.js');  ?>"></script>
        <script
                src="<?php //echo js_url('vendors/Flot/jquery.flot.stack.js');  ?>"></script>
        <script
                src="<?php //echo js_url('vendors/Flot/jquery.flot.resize.js');  ?>"></script>-->
    <!-- Flot plugins -->
    <!--<script
                src="<?php //echo js_url('vendors/flot.orderbars/js/jquery.flot.orderBars.js');  ?>"></script>
        <script
                src="<?php //echo js_url('vendors/flot-spline/js/jquery.flot.spline.min.js');  ?>"></script>
        <script
                src="<?php //echo js_url('vendors/flot.curvedlines/curvedLines.js');  ?>"></script>-->
    <!-- JQVMap -->
    <!--<script
                src="<?php //echo js_url('vendors/jqvmap/dist/jquery.vmap.js');  ?>"></script>
        <script
                src="<?php //echo js_url('vendors/jqvmap/dist/maps/jquery.vmap.world.js');  ?>"></script>
        <script
                src="<?php //echo js_url('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js');  ?>"></script>-->
    <!-- Chart.js -->
    <!--<script
                src="<?php //echo js_url('vendors/Chart.js/dist/Chart.min.js');  ?>"></script>-->
    <!-- gauge.js -->
    <!--<script
                src="<?php //echo js_url('vendors/gauge.js/dist/gauge.min.js');  ?>"></script>-->

    <!--  Loading all the javascript files  -->
    <?php echo $template['metadata']; ?>
    <!--  End of Loading all the javascript files  -->
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <!-- side bar menu -->
        <?php if (isset($template['partials']['side_bar_menu'])): ?>
            <div class="col-md-3 left_col">

                <?php echo $template['partials']['side_bar_menu']; ?>
            </div>
        <?php endif; ?>


        <!-- side bar menu -->


        <!-- top navigation -->
        <?php if (isset($template['partials']['header'])): ?>
            <div class="top_nav">
                <?php echo $template['partials']['header']; ?>
            </div>
        <?php endif; ?>
        <!-- /top navigation -->

        <!-- **************************** page content *********************** -->
        <div class="right_col" role="main">
            <div id="controller-container" class="">
                <div class="page-title">
                    <div class="title_left">
                        <!--  Form Elements-->
                        <h3></h3>
                    </div>

                    <!-- <div class="title_right">
                            <div
                                    class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                            <input type="text" class="form-control"
                                                    placeholder="Search for..."> <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button">Go!</button>
                                            </span>
                                    </div>
                            </div>
                    </div> -->
                </div>
                <div class="clearfix"></div>
                <div class="pull-right btn btn-default hidden" id="manual-refresh" style="padding: 10px;"
                     onclick="location.reload();"><i class="fa fa-refresh" aria-hidden="true"></i>
                </div>
                <div class="pull-right btn btn-default hidden fa fa-refresh" id="timer"
                     style="cursor: unset;padding: 10px;">&nbsp;Auto:<i id="timer_display" class="timerDisplay"></i>
                </div>
                <?php if ($this->uri->segment(1) == 'trip' && ($this->uri->segment(2) == 'getBtoCTripList' || $this->uri->segment(2) == 'getBtoBTripList' || $this->uri->segment(2) == 'getDetailsById')): ?>

                    <script>


                        /*
                         Auto Refresh Page with Time script
                         By JavaScript Kit (javascriptkit.com)
                         Over 200+ free scripts here!
                         */
                            <?php if($this->uri->segment(2) == 'getDetailsById'){ ?>var limit = "20:00"<?php }else{ ?> var limit = "02:00" <?php } ?>
                        //enter refresh time in "minutes:seconds" Minutes should range from 0 to inifinity. Seconds should range from 0 to 59

                        $('#manual-refresh').removeClass('hidden');
                        $('#timer').removeClass('hidden');
                        var parselimit = limit.split(":")
                        parselimit = parselimit[0] * 60 + parselimit[1] * 1

                        function beginrefresh() {
                            if (parselimit == 1)
                                window.location.reload()
                            else {
                                parselimit -= 1
                                curmin = Math.floor(parselimit / 60)
                                cursec = parselimit % 60
                                if (curmin <= 9) {
                                    curmin = '0' + curmin;
                                }
                                if (cursec <= 9) {
                                    cursec = '0' + cursec;
                                }
                                if (curmin != 0)
                                    curtime = curmin + ":" + cursec;
                                else
                                    curtime = "00:" + cursec;
                                $('#timer_display').html(curtime);
                                setTimeout("beginrefresh()", 1000)
                            }
                        }

                        if (window.addEventListener)
                            window.addEventListener("load", beginrefresh, false)
                        else if (window.attachEvent)
                            window.attachEvent("load", beginrefresh)

                    </script>
                <?php endif; ?>
                <section>
                    <div class="x_content bs-example-popovers">
                        <div id="controller-messages" style="display: none;">
                            <div class="alert alert-success alert-dismissible fade in"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <span id="app-message"></span>
                            </div>
                        </div>
                        <!-- <div class="alert alert-info alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                            </button>
                            <strong>Info</strong>Msg comes here.
                            </div>
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                            </button>
                            <strong>Warning</strong>Msg comes here.
                            </div>
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span>
                            </button>
                            <strong>Danger</strong>Msg comes here.
                        </div> -->

                    </div>
                </section>
                <div class="row">
                    <!-- Controller View Content -->
                    <div id="controller-content">
                        <?php echo $template['body']; ?>
                    </div>

                </div>


                <!-- /page content -->
                <!-- compose -->
                <div class="compose col-md-6 col-xs-12">
                    <div class="compose-header">
                        <h2>
                            <span id='booking_type_title'>New Bookings</span>
                            <button type="button" class="close compose-close">
                                <span>×</span>
                            </button>
                        </h2>
                    </div>
                    <?php
                    $trip_type_list = $this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName' => strtoupper('TRIP_TYPE'),
                        'isVisible' => 'Y'), 'sequenceOrder');
                    $trip_model = $this->Trip_Details_Model->getBlankModel();
                    $payment_mode_list = $this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName' => strtoupper('PAYMENT_MODE'),
                        'isVisible' => 'Y'), 'sequenceOrder');
                    $car_type_list = $this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName' => strtoupper('CAR_TYPE'),
                        'isVisible' => 'Y'), 'sequenceOrder');
                    $bill_type_list = $this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName' => strtoupper('BILL_TYPE'),
                        'isVisible' => 'Y'), 'sequenceOrder');
                    $transmission_type_list = $this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName' => strtoupper('TRANSMISSION_TYPE'),
                        'isVisible' => 'Y'), 'sequenceOrder');
                    $transaction_details_model = $this->Trip_Transaction_Details_Model->getBlankModel();
                    $company_list = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions(array('status' => 'Y'), 'id');
                    $passenger_model = $this->Passenger_Model->getBlankModel();
                    ?>
                    <div class="compose-body">
                        <?php
                        $form_attr = array(
                            'name' => 'edit_trip_form',
                            'id' => 'edit_trip_form',
                            'class' => 'form-horizontal form-label-left',
                            'data-parsley-validate' => '',
                            'method' => 'POST'
                        );
                        echo form_open_multipart(base_url(''), $form_attr);
                        echo form_input(array(
                            'type' => 'hidden',
                            'id' => 'trip_id',
                            'name' => 'id',
                            'value' => ($trip_model->get('id')) ? $trip_model->get('id') : -1
                        ));
                        ?>
                        <div id="alerts"></div>

                        <div class="x_content">
                            <br/>


                            <div class="form-group">
                                <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                    <?php $class_name = ($this->session->userdata('role_type') == 'CO') ? '' : 'class="required"'; ?>
                                    <label for="fullname" <?php echo $class_name; ?>>Entity Name</label>


                                    <?php
                                    if ($this->session->userdata('role_type') != 'CO') {

                                        echo form_dropdown('companyId', $company_list, '', array(
                                            'id' => 'companyId-booking',
                                            'class' => 'form-control',
                                            'required' => 'required',
                                        ));
                                    } else if ($this->session->userdata('role_type') == 'CO') {

                                        echo form_input(array(
                                            'type' => 'hidden',
                                            'id' => 'companyId',
                                            'name' => 'companyId',
                                            'value' => ($this->session->userdata('user_company')) ? $this->session->userdata('user_company') : ''
                                        ));

                                        echo text($company_list[$this->session->userdata('user_company')]);

                                        /* echo form_dropdown('companyId', $company_list,$this->session->userdata('user_company'), array(
                                                'id' => 'companyId',
                                                'class' => 'form-control',
                                                'required' => 'required',
                                        ));
                                         */
                                    }
                                    ?>

                                </div>
                                <div id='createdBy' class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback hidden">
                                    <?php $class_name = ($this->session->userdata('role_type') == 'CO') ? 'class="required"' : ''; ?>
                                    <label for="fullname" <?php echo $class_name; ?>>Entity User Name</label>


                                    <?php
                                    echo form_dropdown('createdBy', array(), '', array(
                                        'id' => 'createdBy-booking',
                                        'class' => 'form-control',

                                    ));

                                    ?>

                                </div>
                                <div id="client_name_section"
                                     class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback hidden">
                                    <label for="clientname" class="">Client Name </label>
                                    <?php
                                    echo form_input(array(
                                        'type' => 'text',
                                        'id' => 'clientName',
                                        'class' => 'form-control',
                                        'name' => 'clientName',
                                        'value' => ''
                                    ));

                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php if ($this->session->userdata('role_type') != 'CO') : ?>
                                    <div id="cust_mobile_section"
                                         class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="required">Customer Mobile </label>
                                        <?php

                                        echo form_input(array(
                                            'type' => 'text',
                                            'id' => 'cmobile-booking',
                                            'class' => 'form-control',
                                            'name' => 'cmobile',
                                            'required' => 'required',
                                            'value' => ''
                                        ));
                                        ?>
                                        <div id='mobile-suggesstion-box'></div>
                                    </div>

                                    <div id="cust_email_section"
                                         class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="required">Customer Email</label>
                                        <input type="text" id="cEmail-booking" class="form-control" name="cEmail"
                                               required/>
                                        <div id='email-suggesstion-box'></div>
                                    </div>

                                    <div id="cust_name_section"
                                         class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="required">Customer Name </label>
                                        <?php
                                        echo form_input(array(
                                            'type' => 'text',
                                            'id' => 'cname-booking',
                                            'class' => 'form-control',
                                            'name' => 'cname',
                                            'required' => 'required',
                                            'value' => ''
                                        ));


                                        ?>
                                    </div>
                                <?php endif; ?>


                                <div id="vechile_no_section"
                                     class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback hidden">
                                    <label for="vechilenumber" class="">Vehicle Number </label>
                                    <?php
                                    echo form_input(array(
                                        'type' => 'hidden',
                                        'id' => 'passengerId',
                                        'class' => 'form-control',
                                        'name' => 'passengerId',
                                        'value' => ''
                                    ));

                                    echo form_input(array(
                                        'type' => 'text',
                                        'id' => 'vehicleNo',
                                        'class' => 'form-control',
                                        'name' => 'vehicleNo',
                                        'value' => ''
                                    ));

                                    ?>
                                </div>
                                <div id="vechile_make_section"
                                     class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback hidden">
                                    <label for="vechilemake" class="">Vehicle Make </label>
                                    <?php
                                    echo form_input(array(
                                        'type' => 'text',
                                        'id' => 'vehicleMake',
                                        'class' => 'form-control',
                                        'name' => 'vehicleMake',
                                        'value' => ''
                                    ));

                                    ?>
                                </div>
                                <div id="vechile_model_section"
                                     class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback hidden">
                                    <label for="vechilemodel" class="">Vehicle Model </label>
                                    <?php
                                    echo form_input(array(
                                        'type' => 'text',
                                        'id' => 'vehicleModel',
                                        'class' => 'form-control',
                                        'name' => 'vehicleModel',
                                        'value' => ''
                                    ));

                                    ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="x_title">
                                    <h4><strong> Booking Details</strong>
                                        <small>According to Cities</small>
                                    </h4>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="required">Pickup Location </label>
                                        <?php
                                        echo form_input(array(
                                            'type' => 'text',
                                            'id' => 'searchTextField',
                                            'class' => 'form-control',
                                            'name' => 'pickupLocation',
                                            'required' => 'required',
                                            'placeholder' => 'Enter pickup  location',
                                            'autocomplete' => 'on',
                                            'runat' => 'server',
                                            'value' => ''
                                        ));
                                        echo form_input(array(
                                            'type' => 'hidden',
                                            'id' => 'city_id',
                                            'class' => 'form-control',
                                            'name' => 'bookingCityId',
                                            'required' => 'required',
                                            'value' => ''
                                        ));
                                        echo form_input(array(
                                            'type' => 'hidden',
                                            'id' => 'cityLat',
                                            'class' => 'form-control',
                                            'name' => 'pickupLatitude',
                                            'required' => 'required',
                                            'value' => ''
                                        ));
                                        echo form_input(array(
                                            'type' => 'hidden',
                                            'id' => 'cityLng',
                                            'class' => 'form-control',
                                            'name' => 'pickupLongitude',
                                            'required' => 'required',
                                            'value' => ''
                                        ));

                                        ?>


                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="required">Pickup Date & Time </label>
                                        <?php
                                        echo form_input(array(
                                            'type' => 'text',
                                            'id' => 'pickupDatetime-booking',
                                            'class' => 'form-control',
                                            'name' => 'pickupDatetime',
                                            'required' => 'required',
                                            'value' => ''
                                        ));
                                        ?>
                                    </div>
                                    <div id="drop-location-booking"
                                         class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback hidden">
                                        <label for="fullname" class="required">Drop Location </label>
                                        <?php
                                        echo form_input(array(
                                            'type' => 'text',
                                            'id' => 'drop_location',
                                            'class' => 'form-control',
                                            'name' => 'dropLocation',
                                            'placeholder' => 'Enter drop location',
                                            'autocomplete' => 'on',
                                            'runat' => 'server',
                                            'value' => ''
                                        ));

                                        echo form_input(array(
                                            'type' => 'hidden',
                                            'id' => 'dropLatitude-booking',
                                            'class' => 'form-control',
                                            'name' => 'dropLatitude',
                                            'value' => ''
                                        ));
                                        echo form_input(array(
                                            'type' => 'hidden',
                                            'id' => 'dropLongitude-booking',
                                            'class' => 'form-control',
                                            'name' => 'dropLongitude',
                                            'value' => ''
                                        ));

                                        ?>


                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="">Landmark </label>
                                        <?php
                                        echo form_input(array(
                                            'type' => 'text',
                                            'id' => 'landmark',
                                            'class' => 'form-control',
                                            'name' => 'landmark',
                                            'value' => ''
                                        ));
                                        ?>
                                    </div>

                                    <div id="drop-date-time"
                                         class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback hidden">
                                        <label for="fullname" class="">Drop Date & Time </label>
                                        <?php
                                        echo form_input(array(
                                            'type' => 'text',
                                            'id' => 'dropDatetime',
                                            'class' => 'form-control',
                                            'name' => 'dropDatetime',
                                            'value' => ''
                                        ));
                                        ?>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="required">Trip Type </label>
                                        <?php
                                        echo form_dropdown('tripType', $trip_type_list, $trip_model->get('tripType'), array(
                                            'id' => 'tripType-booking',
                                            'class' => 'form-control',
                                            'required' => 'required',
                                        ));
                                        ?>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="required">Payment Mode  </label>
                                        <?php
                                        echo form_dropdown('paymentMode', $payment_mode_list, $trip_model->get('paymentMode'), array(
                                            'id' => 'paymentMode',
                                            'class' => 'form-control'
                                        ));
                                        ?>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="required">Transmission Type </label>
                                        <?php
                                        echo form_dropdown('transmissionType', $transmission_type_list, $trip_model->get('transmissionType'), array(
                                            'id' => 'transmissionType',
                                            'class' => 'form-control',
                                            'required' => 'required',
                                        ));
                                        ?>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                                        <label for="fullname" class="required">Car Type </label>
                                        <?php
                                        echo form_dropdown('carType', $car_type_list, $trip_model->get('carType'), array(
                                            'id' => 'carType',
                                            'class' => 'form-control',
                                            'required' => 'required'
                                        ));
                                        ?>
                                        <?php
                                        echo form_input(array(
                                            'type' => 'hidden',
                                            'id' => 'booking_type_val',
                                            'class' => 'form-control',
                                            'name' => 'booking_type_val',
                                            'value' => ''
                                        ));
                                        ?>
                                    </div>
                                    <div id='bill_type_section'
                                         class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback hidden">
                                        <label for="billtype" class="">Bill Type </label>
                                        <?php
                                        echo form_dropdown('billType', $bill_type_list, $trip_model->get('billType'), array(
                                            'id' => 'billType',
                                            'class' => 'form-control'
                                        ));
                                        ?>

                                    </div>
                                    <!-- Commented By Anuj   -->
<!--                                    <div id='bill_type_section'-->
<!--                                         class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">-->
<!--                                        <label for="promoCode" class="">Promocode</label>-->
<!--                                        --><?php
//
//                                        echo form_input(array(
//                                            'type' => 'text',
//                                            'id' => 'promoCode',
//                                            'class' => 'form-control',
//                                            'name' => 'promoCode'
//                                        ));
//
//                                        ?>
<!---->
<!--                                    </div>-->
                                    <!-- Commented By Anuj   -->

                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5" id="trip_booking_btn_section">
                                <span type="button" id="cancel_trip_btn" class="btn btn-primary">Cancel</span>
                                <span class="btn btn-success" id="save_b2b_booking">Submit</span>
                                <span class="btn btn-success" id="save_b2c_booking">Submit</span>
                            </div>
                            <span id="trip_process_img" style='display:none;margin-left:50%'>
                                            <img src="../public/images/loader.gif" alt='loading...'> Processing...
                                    </span>
                        </div>
                        <?php
                        echo form_close();
                        ?>
                    </div>
                </div>


            </div>

        </div>

    </div>
    <!-- footer content -->
    <?php if (isset($template['partials']['footer'])): ?>
        <footer><?php echo $template['partials']['footer']; ?></footer>
    <?php endif; ?>
    <!-- /footer content -->
</div>


<!-- Custom Theme Scripts -->
<script src="<?php echo js_url('vendors/custom.min.js'); ?>"></script>

<script>
    $(document).ready(function () {
        var data1 = [
            [gd(2012, 1, 1), 17],
            [gd(2012, 1, 2), 74],
            [gd(2012, 1, 3), 6],
            [gd(2012, 1, 4), 39],
            [gd(2012, 1, 5), 20],
            [gd(2012, 1, 6), 85],
            [gd(2012, 1, 7), 7]
        ];

        var data2 = [
            [gd(2012, 1, 1), 82],
            [gd(2012, 1, 2), 23],
            [gd(2012, 1, 3), 66],
            [gd(2012, 1, 4), 9],
            [gd(2012, 1, 5), 119],
            [gd(2012, 1, 6), 6],
            [gd(2012, 1, 7), 9]
        ];
        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
            data1, data2
        ], {
            series: {
                lines: {
                    show: false,
                    fill: true
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.4
                },
                points: {
                    radius: 0,
                    show: true
                },
                shadowSize: 2
            },
            grid: {
                verticalLines: true,
                hoverable: true,
                clickable: true,
                tickColor: "#d5d5d5",
                borderWidth: 1,
                color: '#fff'
            },
            colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
            xaxis: {
                tickColor: "rgba(51, 51, 51, 0.06)",
                mode: "time",
                tickSize: [1, "day"],
                //tickLength: 10,
                axisLabel: "Date",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 10
            },
            yaxis: {
                ticks: 8,
                tickColor: "rgba(51, 51, 51, 0.06)",
            },
            tooltip: false
        });

        function gd(year, month, day) {
            return new Date(year, month - 1, day).getTime();
        }
    });
</script>
<!-- /Flot -->

<!-- JQVMap -->
<script>
    /*$(document).ready(function () {
     $('#world-map-gdp').vectorMap({
     map: 'world_en',
     backgroundColor: null,
     color: '#ffffff',
     hoverOpacity: 0.7,
     selectedColor: '#666666',
     enableZoom: true,
     showTooltip: true,
     values: sample_data,
     scaleColors: ['#E6F2F0', '#149B7E'],
     normalizeFunction: 'polynomial'
     });
     });*/
</script>
<!-- /JQVMap -->

<!-- Skycons -->
<script>
    /*** B TO B Booking ***/
    $("#b_b_booking").unbind('click');
    $('#b_b_booking').click(function () {
        partnerTripTypeControll();
        $('#save_b2b_booking').show();
        $('#save_b2c_booking').hide();

        $('#booking_type_title').html('B 2 B Booking');
        $('#bill_type_section').removeClass('hidden');
        $('#client_name_section').removeClass('hidden');
        $('#vechile_no_section').removeClass('hidden');
        $('#vechile_make_section').removeClass('hidden');
        $('#vechile_model_section').removeClass('hidden');
        // $('#b_b_company_section ').removeClass('hidden');
        $('#cust_mobile_section').addClass('hidden');
        $('#cust_email_section').addClass('hidden');
        $('#cust_name_section').addClass('hidden');
        // $('#b_c_company_section ').addClass('hidden');

        $("#companyId-booking option[value='1']").hide();
        $('#companyId-booking').prop('selected', false);
        $('#companyId-booking').prop('disabled', false);

        $("#paymentMode option[value='W']").hide();
        $("#paymentMode option[value='P']").hide();
        $("#paymentMode option[value='C']").hide();

        $('#billType').prop('required', false);
        $('#cmobile-booking').prop('required', false);
        $('#cEmail-booking').prop('required', false);
        $('#cname-booking').prop('required', false);
        $('.compose').fadeIn('fast');
    });
    /*** B TO C Booking ***/
    $("#b_c_booking").unbind('click');
    $('#b_c_booking').click(function () {

        $('#save_b2b_booking').hide();
        $('#save_b2c_booking').show();
        $('#booking_type_title').html('B 2 C Booking');
        $('#bill_type_section').addClass('hidden');
        $('#client_name_section').addClass('hidden');
        $('#vechile_no_section').addClass('hidden');
        $('#vechile_make_section').addClass('hidden');
        $('#vechile_model_section').addClass('hidden');
        // $('#b_b_company_section ').addClass('hidden');
        $('#cust_mobile_section').removeClass('hidden');
        $('#cust_email_section').removeClass('hidden');
        $('#cust_name_section').removeClass('hidden');
        // $('#b_c_company_section ').removeClass('hidden');

        $("#companyId-booking option[value='1']").show();
        $('#companyId-booking').val('1');
        $('#companyId-booking').prop('disabled', 'disabled');

        $("#paymentMode option[value='W']").show();
        $("#paymentMode option[value='P']").show();
        $("#paymentMode option[value='C']").show();

        $('#billType').prop('required', false);
        $('#cmobile-booking').prop('required', true);
        $('#cEmail-booking').prop('required', true);
        $('#cname-booking').prop('required', true);
        $('.compose').fadeIn('fast');
    });
    $(".compose-close").unbind('click');
    $('.compose-close').click(function () {
        $('#booking_type_val').val('');
        $('.compose').fadeOut('fast');
    });
    $("#cancel_trip_btn").unbind('click');
    $('#cancel_trip_btn').on('click', function () {
        $('.compose').fadeOut('fast');
    });


    $(document).ready(function () {
        var icons = new Skycons({
                "color": "#73879C"
            }),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

        for (i = list.length; i--;)
            icons.set(list[i], list[i]);

        icons.play();
    });
</script>
<!-- /Skycons -->

<!-- Doughnut Chart -->
<script>
    $(document).ready(function () {
        var options = {
            legend: false,
            responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
            type: 'doughnut',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    "Symbian",
                    "Blackberry",
                    "Other",
                    "Android",
                    "IOS"
                ],
                datasets: [{
                    data: [15, 20, 30, 10, 30],
                    backgroundColor: [
                        "#BDC3C7",
                        "#9B59B6",
                        "#E74C3C",
                        "#26B99A",
                        "#3498DB"
                    ],
                    hoverBackgroundColor: [
                        "#CFD4D8",
                        "#B370CF",
                        "#E95E4F",
                        "#36CAAB",
                        "#49A9EA"
                    ]
                }]
            },
            options: options
        });
    });
</script>
<!-- /Doughnut Chart -->

<!-- bootstrap-daterangepicker -->
<script>
    $(document).ready(function () {

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>
<!-- /bootstrap-daterangepicker -->

<!-- gauge.js -->
<script>
    var opts = {
        lines: 12,
        angle: 0,
        lineWidth: 0.4,
        pointer: {
            length: 0.75,
            strokeWidth: 0.042,
            color: '#1D212A'
        },
        limitMax: 'false',
        colorStart: '#1ABC9C',
        colorStop: '#1ABC9C',
        strokeColor: '#F0F3F3',
        generateGradient: true
    };
    var target = document.getElementById('foo'),
        gauge = new Gauge(target).setOptions(opts);

    gauge.maxValue = 6000;
    gauge.animationSpeed = 32;
    gauge.set(3200);
    gauge.setTextField(document.getElementById("gauge-text"));
</script>

<!-- compose -->

<!-- /compose -->
<!-- /gauge.js -->

<style>
    .compose {
        z-index: 20;
    }
</style>

<!---- Google Api For Auto Complete Address ----->

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC-6McsYQgunIuJ9xai8Zm95ekfJxfRHRw&amp;libraries=places"
        type="text/javascript"></script>
<script type="text/javascript">
    function initialize() {
        var input = document.getElementById('searchTextField');

        var bounds = new google.maps.LatLngBounds();
        var options = {
            bounds: bounds,
            types: ['geocode'],
            componentRestrictions: {country: 'in'}
        };

        var autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('cityLat').value = place.geometry.location.lat();
            document.getElementById('cityLng').value = place.geometry.location.lng();
            var map;
            var coord1 = new google.maps.LatLng(document.getElementById('cityLat').value, document.getElementById('cityLng').value);
            var bounds = new google.maps.LatLngBounds();
            bounds.extend(coord1);
            var marker1 = new google.maps.Marker({
                map: map,
                position: coord1
            });
            var city = 0;
            var checkBangaloreCity = checkInPolygon(marker1, bangalorePolygan);
            var checkMumbaiCity = checkInPolygon(marker1, mumbaiPolygan);
            var checkPuneCity = checkInPolygon(marker1, punePolygan);
            if (checkBangaloreCity == true) {
                var city = 2;
            }
            if (checkMumbaiCity == true) {
                var city = 1;
            }
            if (checkPuneCity == true) {
                var city = 3;
            }
            document.getElementById('city_id').value = city;
            if (city == 0) {
                infoMessage('Invalid PickupLocation');
                $('#searchTextField').val('');
            }
        });
        // drop location
        var southWest = new google.maps.LatLng(12.942117, 77.575363);
        var bounds = new google.maps.LatLngBounds(southWest);
        var options = {
            bounds: bounds,
            types: ['geocode'],
            componentRestrictions: {country: 'in'}
        };
        var input = document.getElementById('drop_location');
        var dp_autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(dp_autocomplete, 'place_changed', function () {
            var place = dp_autocomplete.getPlace();
            //console.log(Object.values(place));
            //document.getElementById('city2').value = place.state;
            var map;
            document.getElementById('dropLatitude').value = place.geometry.location.lat();
            document.getElementById('dropLongitude').value = place.geometry.location.lng();
            var coord1 = new google.maps.LatLng(document.getElementById('dropLatitude').value, document.getElementById('dropLongitude').value);
            var bounds = new google.maps.LatLngBounds();
            bounds.extend(coord1);
            var marker1 = new google.maps.Marker({
                map: map,
                position: coord1
            });
            var city = 0;
            var checkBangaloreCity = checkInPolygon(marker1, bangalorePolygan);
            var checkMumbaiCity = checkInPolygon(marker1, mumbaiPolygan);
            var checkPuneCity = checkInPolygon(marker1, punePolygan);
            if (checkBangaloreCity == true) {
                var city = 2;
            }
            if (checkMumbaiCity == true) {
                var city = 1;
            }
            if (checkPuneCity == true) {
                var city = 3;
            }
            var pikup_city = document.getElementById('city_id').value;
            var drop_city = city;
            if (document.getElementById('tripType').value == 'O') {
                if (drop_city != pikup_city) {
                    infoMessage('Invalid PickupLocation');
                    $('#drop_location').val('');
                    $('#dropLatitude').val('');
                    $('#dropLongitude').val('');
                }
            }

        });


        /**** show auto complete in trip details  ***/
        var input = document.getElementById('pickupLocation');
        var pk_tripautocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(pk_tripautocomplete, 'place_changed', function () {
            var place = pk_tripautocomplete.getPlace();
            //console.log(Object.values(place));
            //document.getElementById('city2').value = place.state;
            document.getElementById('pickupLatitude').value = place.geometry.location.lat();
            document.getElementById('pickupLongitude').value = place.geometry.location.lng();

        });
        var input = document.getElementById('dropLocation');
        var dl_tripautocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(dl_tripautocomplete, 'place_changed', function () {
            var place = dl_tripautocomplete.getPlace();
            //console.log(Object.values(place));
            //document.getElementById('city2').value = place.state;
            document.getElementById('dropLatitude').value = place.geometry.location.lat();
            document.getElementById('dropLongitude').value = place.geometry.location.lng();

        });

        var input = document.getElementById('subTrip_pickupLocation');
        var subtrip_pk_tripautocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(subtrip_pk_tripautocomplete, 'place_changed', function () {
            var place = subtrip_pk_tripautocomplete.getPlace();
            document.getElementById('subTrip_pickupLatitude').value = place.geometry.location.lat();
            document.getElementById('subTrip_pickupLongitude').value = place.geometry.location.lng();

        });
        var input = document.getElementById('subTrip_dropLocation');
        var subtrip_dl_tripautocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(subtrip_dl_tripautocomplete, 'place_changed', function () {
            var place = subtrip_dl_tripautocomplete.getPlace();
            document.getElementById('subTrip_dropLatitude').value = place.geometry.location.lat();
            document.getElementById('subTrip_dropLongitude').value = place.geometry.location.lng();

        });

    }
    // Bangalore City Polygan Lat and Long
    var bangalorePolygan = new google.maps.Polygon({
        paths: [
            new google.maps.LatLng(13.125518, 77.414759),
            new google.maps.LatLng(13.182242, 77.467529),
            new google.maps.LatLng(13.194253, 77.534691),
            new google.maps.LatLng(13.182318, 77.593214),
            new google.maps.LatLng(13.192753, 77.711112),
            new google.maps.LatLng(13.135212, 77.801395),
            new google.maps.LatLng(13.087795, 77.808228),
            new google.maps.LatLng(12.966298, 77.815915),
            new google.maps.LatLng(12.853069, 77.788582),
            new google.maps.LatLng(12.788106, 77.766374),
            new google.maps.LatLng(12.798934, 77.683521),
            new google.maps.LatLng(12.773112, 77, 589564),
            new google.maps.LatLng(12.868891, 77.431546),
            new google.maps.LatLng(13.086963, 77.412755),
            new google.maps.LatLng(12.144462, 77.423859),
            new google.maps.LatLng(13.125518, 77.414759),
        ]
    });
    // Mumbai City Polygan Lat and Long
    var mumbaiPolygan = new google.maps.Polygon({
        paths: [
            new google.maps.LatLng(18.894421, 72.809406),
            new google.maps.LatLng(18.952392, 72.798934),
            new google.maps.LatLng(19.166563, 72.782455),
            new google.maps.LatLng(19.223628, 72.776962),
            new google.maps.LatLng(19.311781, 72.792068),
            new google.maps.LatLng(19.309189, 72.823654),
            new google.maps.LatLng(19.333812, 72.837387),
            new google.maps.LatLng(19.335108, 72.808547),
            new google.maps.LatLng(19.372683, 72.761856),
            new google.maps.LatLng(119.474998, 72.770095),
            new google.maps.LatLng(19.465935, 72.842880),
            new google.maps.LatLng(19.441332, 72.863479),
            new google.maps.LatLng(19.372683, 72.914291),
            new google.maps.LatLng(19.301413, 72.967849),
            new google.maps.LatLng(19.205473, 73.035140),
            new google.maps.LatLng(19.246968, 73.084579),
            new google.maps.LatLng(19.232230, 73.112550),
            new google.maps.LatLng(19.265460, 73.105682),
            new google.maps.LatLng(19.280046, 73.131436),
            new google.maps.LatLng(19.250872, 73.173502),
            new google.maps.LatLng(19.228177, 73.232737),
            new google.maps.LatLng(19.189264, 73.249907),
            new google.maps.LatLng(19.189264, 73.249907),
            new google.maps.LatLng(19.144665, 73.244756),
            new google.maps.LatLng(19.168182, 73.096239),
            new google.maps.LatLng(19.144665, 73.057607),
            new google.maps.LatLng(19.129255, 73.060182),
            new google.maps.LatLng(19.113170, 73.105989),
            new google.maps.LatLng(18.978117, 73.122746),
            new google.maps.LatLng(19.016477, 73.061010),
            new google.maps.LatLng(19.003969, 73.005448),
            new google.maps.LatLng(19.066497, 72.980754),
            new google.maps.LatLng(19.001467, 72.916373),
            new google.maps.LatLng(18.894421, 72.809406),

        ]
    });

    // Pune City Polygan Lat and Long  
    var punePolygan = new google.maps.Polygon({
        paths: [
            new google.maps.LatLng(18.417593, 73.897289),
            new google.maps.LatLng(18.452118, 73.790859),
            new google.maps.LatLng(18.541329, 73.740047),
            new google.maps.LatLng(18.618130, 73.697475),
            new google.maps.LatLng(18.694897, 73.724254),
            new google.maps.LatLng(18.705303, 73.758587),
            new google.maps.LatLng(18.703352, 73.853322),
            new google.maps.LatLng(18.681888, 73.975567),
            new google.maps.LatLng(18.648060, 74.022945),
            new google.maps.LatLng(18.510729, 74.018139),
            new google.maps.LatLng(18.428668, 73.989986),
            new google.maps.LatLng(18.417593, 73.897289),
        ]
    });
    function checkInPolygon(marker, polygon) {
        var infowindow = new google.maps.InfoWindow();
        var html = "";
        if (google.maps.geometry.poly.containsLocation(marker.getPosition(), polygon)) {
            return true;
        } else {
            //html = "outside polygon";
            return false;
        }
        infowindow.setContent(html);
        infowindow.open(map, marker);
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>

</body>
</html>
