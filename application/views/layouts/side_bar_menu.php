<!-- side bar menu -->
<div class="left_col scroll-view">
	<div class="navbar nav_title" style="border: 0;">
		<a href="<?php echo base_url('dashboard')?>" class="site_title"> <img
			src="<?php echo image_url('app/logo.png')?>" alt=""><span> <img
				src="<?php echo image_url('app/ZuverLogo.png')?>" alt=""></span></a>
	</div>

	<div class="clearfix"></div>

	<!-- menu profile quick info -->
	<div class="profile hidden">
		<div class="profile_pic">
			<img src="<?php echo image_url('app/Profileimg.jpg')?>" alt="..."
				class="img-circle profile_img">
		</div>
		<div class="profile_info">
			<span>Welcome,</span>
			<h2>John Doe</h2>
		</div>
	</div>
	<!-- /menu profile quick info -->

	<br />

	<!-- sidebar menu -->
	<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
		<div class="menu_section">
			<h3 class="hidden">General</h3>
			<ul class="nav side-menu">
				<li
					<?php echo ($active_menu=='dashboard')?'class="current-page"':''; ?>><a
					href="<?php echo base_url('dashboard')?>"><i class="fa fa-home"></i>
						Dashboard <span class="fa fa-chevron-right"></span></a></li>
                         <?php if($this->User_Access_Model->showTrips()) :?>
                                    <li <?php echo ($active_menu=='trip')?'class="current-page"':''; ?>>
                                        <a href="javascript:void(0);">
                                            <i class="fa fa-road"></i>Trips
                                            <span class="fa fa-chevron-down"></span>
                                        </a>
                                        <ul class="nav child_menu">
						<li><a href="<?php echo base_url('trip/getBtoBTripList')?>">B2B Booking</a></li>
						<?php if($this->session->userdata('role_type') !=Role_Type_Enum::COMPANY): ; ?>
						<li><a href="<?php echo base_url('trip/getBtoCTripList')?>">B2C Booking</a></li>
						<?php endif; ?>
					</ul>
                                    </li>
                        <?php endif; ?>
                                <?php if($this->User_Access_Model->showPassenger()) :?>
				<li
					<?php echo ($active_menu=='passenger')?'class="current-page"':''; ?>>
					<a href="<?php echo base_url('passenger/getPassengerList'); ?>"><i
						class="fa fa-users"></i>Customers<span class="fa fa-chevron-right"></span></a>
					<!-- <ul class="nav child_menu">
						<li><a href="<?php //echo base_url('passenger/add'); ?>">New Customer</a></li>
						<li><a href="<?php //echo base_url('passenger/getPassengerList'); ?>">View Customer List</a></li>
                                </ul> -->
				</li>
                                <?php endif; ?>

                                <?php if($this->User_Access_Model->showDriver()) :?>
				<li
					<?php echo ($active_menu=='driver')?'class="current-page"':''; ?>><a
					href="<?php echo base_url('driver/getDriverList'); ?>"><i
						class="ico"></i>Drivers<span class="fa fa-chevron-right"></span></a>
					<!-- <ul class="nav child_menu">
						<li><a href="<?php echo base_url('driver/add'); ?>">New Driver</a></li>
						<li><a href="<?php echo base_url('driver/getDriverList'); ?>">View Driver List</a></li>
				</ul> --></li>
                                <?php endif; ?>
                                <?php if($this->User_Access_Model->showPromoCode()) :?>
				<li
					<?php echo ($active_menu=='promocode')?'class="current-page"':''; ?>>
					<a href="<?php echo base_url('promocode/getPromocodeList'); ?>"><i
						class="fa fa-percent"></i>Promo code<span
						class="fa fa-chevron-right"></span></a> <!-- <ul class="nav child_menu">
						<li><a href="<?php echo base_url('promocode/add'); ?>">New Offers</a></li>
						<li><a href="<?php echo base_url('promocode/getPromocodeList'); ?>">View Offers List</a></li>
				</ul> -->
				</li>
                                <?php endif; ?>
                                <?php if($this->User_Access_Model->showRateCard()) :?>
				<li
					<?php echo ($active_menu=='tariff')?'class="current-page"':''; ?>>
										<a href="javascript:void(0);">
                                            <i class="fa fa-road"></i>Rate Card
                                            <span class="fa fa-chevron-down"></span>
                                        </a>
                                        <ul class="nav child_menu">
						<li><a href="<?php echo base_url('tariff/getTariffList'); ?>">Trip Rate card</a></li>
						<li><a href="<?php echo base_url('tariff/getDriverTariffList'); ?>">Driver Rate card</a></li>
					</ul>
				</li>
                                <?php endif; ?>

                                 <?php if($this->User_Access_Model->showBillSummary()) :?>
                                    <li <?php echo ($active_menu=='transaction')?'class="current-page"':''; ?>>
                                        <a href="javascript:void(0);">
                                            <i class="fa fa-road"></i>Completed Trips
                                            <span class="fa fa-chevron-down"></span>
                                        </a>
                                        <ul class="nav child_menu">
						<li><a href="<?php echo base_url('transaction/getBtoBTransactionList')?>">B2B Completed Trips</a></li>
						<?php if($this->session->userdata('role_type') !=Role_Type_Enum::COMPANY): ; ?>
						<li><a href="<?php echo base_url('transaction/getBtoCTransactionList')?>">B2C Completed Trips</a></li>
						<?php endif; ?>
					</ul>
                                    </li>
                        <?php endif; ?>


                                <?php if($this->User_Access_Model->showEmployees()) :?>
				<li <?php echo ($active_menu=='user')?'class="current-page"':''; ?>><a
					href="<?php echo base_url('user/getUserList'); ?>"><i
						class="fa fa-users"></i>Users<span class="fa fa-chevron-right"></span></a>
					<!-- <ul class="nav child_menu">
						<li><a href="<?php echo base_url('user/add'); ?>">New User</a></li>
						<li><a href="<?php echo base_url('user/getUserList'); ?>">View User List</a></li>
                                </ul> --></li>
                                <?php endif; ?>

                                <?php if($this->User_Access_Model->showPartners()) :?>
				<li
					<?php echo ($active_menu=='corporatepartners')?'class="current-page"':''; ?>><a
					href="<?php echo base_url('corporatepartners/getCorporatePartnersList'); ?>"><i
						class="fa fa-building"></i>Partners<span class="fa fa-chevron-right"></span></a>
					<!-- <ul class="nav child_menu">
						<li><a href="<?php echo base_url('corporatepartners/add'); ?>">New Corporate/Partner</a></li>
						<li><a href="<?php echo base_url('corporatepartners/getCorporatePartnersList'); ?>">View Corporate/Partner List</a></li>
				</ul> --></li>
                   <?php endif; ?>
                 <?php if($this->User_Access_Model->showEmergency()) :?>
                                <li
					<?php echo ($active_menu=='emergency')?'class="current-page"':''; ?>>
					<a href="<?php echo base_url('emergency/getEmergencyList'); ?>"> <i
						class="fa fa-phone"></i>SOS<span class="fa fa-chevron-right"></span></a>
				</li>
                  <?php endif; ?>
                             <?php if($this->User_Access_Model->showReports()) :?>
				<li
					<?php echo ($active_menu=='report')?'class="current-page"':''; ?>>
					<a> <i
						class="fa fa-bar-chart-o"></i>Reports<span
						class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						<li><a href="<?php echo base_url('report/getCompleteTripReportLayout'); ?>">Complete Trip</a></li>
						<li><a href="<?php echo base_url('report/getCancelledTripReportLayout'); ?>">Cancelled Trip</a></li>
						<li><a href="<?php echo base_url('report/getTripStatusReportLayout'); ?>">Trip Status</a></li>
						<li><a href="<?php echo base_url('report/getShiftDriverReportLayout'); ?>">Shift Drivers</a></li>
					</ul>
				</li>
                              <?php endif; //$this->User_Access_Model->showSettings()?>
                   <?php if($this->User_Access_Model->showWalletTransaction()) :?>
                                    <li
					<?php echo ($active_menu=='wallet')? 'class="current-page"':''; ?>>
					<a href="<?php echo base_url('wallet/add'); ?>">
						<i class="fa fa-inr"></i>Wallet Transaction<span
						class="fa fa-chevron-right"></span>
				</a>
				</li>
                                <?php endif; ?>
                                <?php //if($this->User_Access_Model->showBankDetails()) :?>
                                    <!-- <li
					<?php //echo ($active_menu=='bank')? 'class="current-page"':''; ?>>
					<a href="<?php //echo base_url('bank/getBankList'); ?>">
						<i class="fa fa-money"></i>Bank Account List<span
						class="fa fa-chevron-down"></span>
				</a>
				</li> -->
                                <?php //endif; ?>


                                <?php if($this->User_Access_Model->showSettings() || $this->User_Access_Model->showBankDetails()) :?>
                                    <li
					<?php

				echo ($active_menu == 'country' || $active_menu == 'bank' || $active_menu == 'state' || $active_menu == 'city' || $active_menu == 'site' || $active_menu == 'smtp' || $active_menu == 'menu') ? 'class="current-page"' : '';
				?>>
				<a> <i class="fa fa-cogs"></i>Settings
						<span class="fa fa-chevron-down"></span>
				</a>
					<ul class="nav child_menu">
						<?php if($this->User_Access_Model->showSettings()) :?>
						<li><a href="<?php echo base_url('country/getCountryList'); ?>">Country</a></li>
						<li><a href="<?php echo base_url('state/getStateList'); ?>">States</a></li>
						<li><a href="<?php echo base_url('city/getCityList'); ?>">City</a></li>
						<li><a href="<?php echo base_url('site/getDetailsById/1'); ?>">Site</a></li>
						<li><a href="<?php echo base_url('smtp/getDetailsById/1'); ?>">SMTP</a></li>
						<li><a href="<?php echo base_url('menu/getMenuList'); ?>">Web links</a></li>
						<?php endif; ?>
						<?php if($this->User_Access_Model->showBankDetails()) :?>
						<li><a href="<?php echo base_url('bank/getBankList'); ?>">Bank Account List</a></li>
						<?php endif; ?>
					</ul></li>
                                <?php endif; ?>
                                <?php if($this->User_Access_Model->showDriverMap()) : ?>
                                <li
                                <?php echo ($active_menu=='driver_map')?'class="current-page"':''; ?>>
                                <a href="<?php echo base_url('driver/driverCurrentMap'); ?>"> <i
                                        class="fa fa-map-marker"></i>Driver Map<span
                                        class="fa fa-chevron-right"></span></a>
				</li>
                            <?php endif ;?>
							<?php
								if($this->User_Access_Model->showDriverMapForPartner())
								{
									echo '
									<li>
									<a href="'.base_url('trip/loadB2BDriverMap').'"> <i class="fa fa-map-marker"></i>Driver Map<span class="fa fa-chevron-right"></span></a>
									</li>
									';
								}
							?>
			</ul>
		</div>
	</div>
	<!-- /sidebar menu -->


	<!-- /menu footer buttons -->
	<div class="sidebar-footer hidden-small hidden">
		<a data-toggle="tooltip" data-placement="top" title="Settings"> <span
			class="glyphicon glyphicon-cog" aria-hidden="true"></span>
		</a> <a data-toggle="tooltip" data-placement="top" title="FullScreen">
			<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
		</a> <a data-toggle="tooltip" data-placement="top" title="Lock"> <span
			class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
		</a> <a href="<?php echo base_url();?>Logout/signOut"
			data-toggle="tooltip" data-placement="top" title="Logout"> <span
			class="glyphicon glyphicon-off" aria-hidden="true"></span>
		</a>
	</div>
	<!-- /menu footer buttons -->
</div>
