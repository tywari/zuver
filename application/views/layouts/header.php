<div class="nav_menu">
    <nav>
        <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>
        <?php if ($this->User_Access_Model->showTrips()) : ; ?>
            <div class="nav navbar-nav">
                <button id="b_b_booking" class="btn btn-sm btn-success btn-block" type="button"
                        style="margin-top: 15px;">
                    B <i>2</i> B Booking
                </button>
            </div>
            <?php if ($this->session->userdata('role_type') != Role_Type_Enum::COMPANY) : ; ?>
                <div class="nav navbar-nav" style="margin-left: 3px">
                    <button id="b_c_booking" class="btn btn-sm btn-success btn-block" type="button"
                            style="margin-top: 15px;">
                        B <i>2</i> C Booking
                    </button>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <ul class="nav navbar-nav navbar-right">
            <li class=""><a href="javascript:;"
                            class="user-profile dropdown-toggle" data-toggle="dropdown"
                            aria-expanded="false">
                    <?php
                    /*$image = $this->config->item('user_content_path').$user_id.'/'.$profileImage;
                    if(file_exists($image))
                    { */ ?>
                    <!--<img src="<?php //echo $image ;?>" alt="user">-->
                    <?php // } else { ?>
                    <img src="<?php echo ($this->session->userdata('user_profile_image')) ? user_content_url($this->session->userdata('user_id') . '/' . $this->session->userdata('user_profile_image')) : image_url('app/user.png') ?>"
                         alt="user">
                    <?php //} ?>
                    <?php
                    $username = ($this->session->userdata('user_name') != '') ? $this->session->userdata('user_name') : "user";
                    echo $username;
                    ?>
                    <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <!--					<li><a href="javascript:;"> Profile</a></li>
                                        <li><a href="javascript:;"> <span class="badge bg-red pull-right">50%</span>
                                                <span>Settings</span>
                                        </a></li>
                                        <li><a href="javascript:;">Help</a></li>-->
                    <li>
                        <a href="<?php echo base_url(); ?>Logout/signOut">
                            <i class="fa fa-sign-out pull-right"></i>
                            Log Out
                        </a>
                    </li>
                </ul>
            </li>

            <li role="presentation" class="dropdown hidden"><a href="javascript:;"
                                                               class="dropdown-toggle info-number"
                                                               data-toggle="dropdown"
                                                               aria-expanded="false"> <i class="fa fa-envelope-o"></i>
                    <span
                            class="badge bg-green">6</span>
                </a>
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list"
                    role="menu">
                    <li><a> <span class="image"><img src="<?php echo image_url('app/Profileimg.jpg') ?>"
                                                     alt="Profile Image"/></span> <span> <span>John Smith</span> <span
                                        class="time">3 mins ago</span>
						</span> <span class="message"> Film festivals used to be do-or-die
								moments for movie makers. They were where... </span>
                        </a></li>
                    <li><a> <span class="image"><img src="<?php echo image_url('app/Profileimg.jpg') ?>"
                                                     alt="Profile Image"/></span> <span> <span>John Smith</span> <span
                                        class="time">3 mins ago</span>
						</span> <span class="message"> Film festivals used to be do-or-die
								moments for movie makers. They were where... </span>
                        </a></li>
                    <li><a> <span class="image"><img src="<?php echo image_url('app/Profileimg.jpg') ?>"
                                                     alt="Profile Image"/></span> <span> <span>John Smith</span> <span
                                        class="time">3 mins ago</span>
						</span> <span class="message"> Film festivals used to be do-or-die
								moments for movie makers. They were where... </span>
                        </a></li>
                    <li><a> <span class="image"><img src="<?php echo image_url('app/Profileimg.jpg') ?>"
                                                     alt="Profile Image"/></span> <span> <span>John Smith</span> <span
                                        class="time">3 mins ago</span>
						</span> <span class="message"> Film festivals used to be do-or-die
								moments for movie makers. They were where... </span>
                        </a></li>
                    <li>
                        <div class="text-center">
                            <a> <strong>See All Alerts</strong> <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown" role="presentation">
                <?php
                $city_list = $this->City_Model->getSelectDropdownOptions(array('status' => 'Y'), 'name');
                $city_model = $this->City_Model->getBlankModel();
                $city_id = $this->uri->segment('3');
                /*  if(!empty($this->uri->segment('3'))){
                     $filter_cityid =    $this->uri->segment('3');
                    }else{ */
                $filter_cityid = $this->session->userdata('user_city');
                //  }
                echo form_dropdown('filter_cityId', $city_list, $filter_cityid, array(
                    'id' => 'filter_cityId',
                    'class' => 'form-control',
                    'required' => 'required',
                    'style' => 'padding-top: 6px; margin-top: 12px; border-top-width: 1px; width: 200px;'
                ));
                ?>
            </li>
        </ul>
    </nav>
</div>
</div>