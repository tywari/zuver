<div id="city-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>
				City's List
			</h2>
			<!--<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				 <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li> 
				
			</ul>
			<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" id="search_passenger_content" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="search_passenger_btn" type="button">Go!</button>
                    </span>
                    
                  </div>
                  <div><span>Search by Referral Code, Passenger Full Name, Email and Mobile</span></div>
                </div>
             </div>-->
             <div class="title_right pull-right">
                
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<a href="<?php echo base_url('city/add')?>"><button id="add_city_btn"
						class="btn btn-primary">Add City</button></a>
					
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			<table id="datatable-checkbox"
				class="table table-striped table-bordered datatable-button-init-collection">
				<thead>
					<tr>
						<th><!-- <input type="checkbox" id="check-all" class="flat"> --></th>
						<!-- <th>Sl no</th> -->
						<th>Action</th>
						<th>City Name</th>
						<!-- <th>Latitude</th>
						<th>Longitude</th>
						<th>Radius</th> -->
						
						<th>Status</th>
						
					</tr>
				</thead>


				<tbody>
				<?php 
				//sl no intialization
				//$i=1;
				//$city_model_list=array(0=>array('id'=>1,'name'=>'Bangalore','latitude'=>17.01206631,'longitude'=>27.01206631,'radius'=>25,'status'=>1),1=>array('id'=>2,'name'=>'Mumbai','latitude'=>17.01206631,'longitude'=>27.01206631,'radius'=>30,'status'=>1));
				foreach ($city_model_list as $list)
				{
					
				echo '<tr>';
					
				echo '<td></td>';//<input type="checkbox" class="flat" name="table_records">
				//echo '<th>'.$i.'</th>';
				echo '<td>'
						. '<a href="javascript:void(0);" id="viewCity-'.$list->id.'" onclick="viewCity(this.id);" class="userlink">'
								. '<span class="glyphicon glyphicon-eye-open"></span></a> '
										. '/ <a href="javascript:void(0);" id="editCity-'.$list->id.'" onclick="editCity(this.id);" class="userlink">'
												. '<span class="glyphicon glyphicon-edit"></span></a></td>';
				echo '<td>'.$list->name.'</td>';
				//echo '<td>'.$list->latitude.'</td>';
				//echo '<td>'.$list->longitude.'</td>';
				//echo '<td>'.$list->radius.'</td>';
				
				//Status toogle
				$active=($list->status==Status_Type_Enum::ACTIVE)?'btn-primary active-status':'btn-default" onclick="changeCityStatus(this.id);';
				$deactive=($list->status==Status_Type_Enum::INACTIVE)?'btn-primary active-status':'btn-default" onclick="changeCityStatus(this.id);';
				echo '<td> <ul class="list-inline"> <li>';
				echo '<div class="btn-group btn-toggle">';
				echo '<button id="active-'.$list->id.'" class="btn btn-xs '.$active.'">Active</button>';
				echo '<button id="deactive-'.$list->id.'" class="btn btn-xs '.$deactive.'">Deactive</button>';
				
				echo '</div></li></ul></td>';
				//echo '<td><div class="checkbox">';
				//echo '<label> <input type="checkbox" '.($list['status'])?"checked=checked":"checked".' data-toggle="toggle" class="flat">'.($list['status'])?'Enabled':'Disabled'.'</label>';
				//echo '</div></td>';
				
				echo '</tr>';
				//$i++;
				}
	            ?>
				</tbody>
			</table>
		</div>
	</div>
</div>