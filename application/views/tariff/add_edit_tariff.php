<?php
$view_mode = $mode;
?>
<div class="">

	<div class="clearfix"></div>

	<div class="row">
		<!-- form input mask -->
		<div id="tariff-details-information"
			class="col-md-12 col-sm-12 col-xs-12">
            <?php
												$form_attr = array (
														'name' => 'edit_tariff_form',
														'id' => 'edit_tariff_form',
														'method' => 'POST',
														'data-parsley-validate' => '',
														'class' => 'form-horizontal form-label-left' 
												);
												echo form_open ( base_url ( 'tariff/saveTariff' ), $form_attr );
												?>     
            <div class="x_panel">
				<div class="x_title">
					<h2>
						<i class="fa fa-tag"></i> Rate Card <small>According to Trip Type,
							City, Company/Partners</small>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<br />
                    <?php
																				
																				// user id by default is -1
																				echo form_input ( array (
																						'type' => 'hidden',
																						'id' => 'user_id',
																						'name' => 'id',
																						'value' => ($tariff_model->get ( 'id' )) ? $tariff_model->get ( 'id' ) : - 1 
																				) );
																				// user id by default is -1
																				echo form_input ( array (
																						'type' => 'hidden',
																						'id' => 'isMonthly',
																						'name' => 'isMonthly',
																						'value' => ($tariff_model->get ( 'isMonthly' )) ? $tariff_model->get ( 'isMonthly' ) : Status_Type_Enum::INACTIVE
																				) );
																				?>
                    <div class="form-group">
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												// validation for user name
																												echo form_label ( 'Rate Name:', 'name', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for passenger first name
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'name',
																															'name' => 'name',
																															'class' => 'form-control',
																															'required' => 'required',
																															//'pattern' => '[A-Za-z\s]{3,30}',
																															'value' => ($tariff_model->get ( 'name' )) ? $tariff_model->get ( 'name' ) : '' 
																													) );
																												} else {
																													echo text ( $tariff_model->get ( 'name' ) );
																												}
																												?>
                        </div>

						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Trip Type:', 'tripType', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'tripType', $trip_type_list, $tariff_model->get ( 'tripType' ), array (
																															'id' => 'tripType',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( $trip_type_list [$tariff_model->get ( 'tripType' )] );
																												}
																												?>
                        </div>
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'City Name:', 'cityId', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'cityId', $city_list, $tariff_model->get ( 'cityId' ), array (
																															'id' => 'cityId',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( $city_list [$tariff_model->get ( 'cityId' )] );
																												}
																												?>
                        </div>
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Entity Name:', 'companyId', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'companyId', $company_list, $tariff_model->get ( 'companyId' )?$tariff_model->get ( 'companyId' ):DEFAULT_COMPANY_ID, array (
																															'id' => 'companyId',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( $company_list [$tariff_model->get ( 'companyId' )] );
																												}
																												?>
                        </div>
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Bill Type:', 'billType', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												
																												echo form_dropdown ( 'billType', $bill_type_list, Bill_Type_Enum::HOURLY, array (
																														'id' => 'billType',
																														'class' => 'form-control',
																														'required' => 'required' 
																												) );
																												
																												?>
                        </div>
					</div>

				</div>
				<div class="x_title">
					<h4>
						<strong>Tariff</strong>
					</h4>
					<div class="clearfix"></div>
				</div>
				<div id="no_rate_card" class="panel rate-card">
					<span><h2>Select Bill Type</h2></span>
				</div>
				<div id="normal_rate_card" class="panel rate-card">
					<div class="form-group">
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                        <?php
																								echo form_label ( 'Convenience Day Fare:', 'dayConvenienceCharge', array (
																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																								) );
																								?>
                        <?php
																								// validation for fixedAmount
																								if ($view_mode == EDIT_MODE) {
																									echo form_input ( array (
																											'id' => 'dayConvenienceCharge',
																											'name' => 'dayConvenienceCharge',
																											'class' => 'form-control',
																											'required' => 'required',
																											'pattern' => '[0-9]+([\.,][0-9]+)?',
																											'value' => ($tariff_model->get ( 'dayConvenienceCharge' )) ? $tariff_model->get ( 'dayConvenienceCharge' ) : '00.00' 
																									) );
																								} else {
																									echo text ( $tariff_model->get ( 'dayConvenienceCharge' ) );
																								}
																								?>
                    </div>
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                        <?php
																								echo form_label ( 'Convenience Night Fare:', 'nightConvenienceCharge', array (
																										'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																								) );
																								?>
                        <?php
																								// validation for fixedAmount
																								if ($view_mode == EDIT_MODE) {
																									echo form_input ( array (
																											'id' => 'nightConvenienceCharge',
																											'name' => 'nightConvenienceCharge',
																											'class' => 'form-control',
																											'required' => 'required',
																											'pattern' => '[0-9]+([\.,][0-9]+)?',
																											'value' => ($tariff_model->get ( 'nightConvenienceCharge' )) ? $tariff_model->get ( 'nightConvenienceCharge' ) : '00.00' 
																									) );
																								} else {
																									echo text ( $tariff_model->get ( 'nightConvenienceCharge' ) );
																								}
																								?>
                    </div>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in"
						role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Hours</th>
										<th>1</th>
										<th>2</th>
										<th>3</th>
										<th>4</th>
										<th>5</th>
										<th>6</th>
										<th>7</th>
										<th>8</th>
										<th>9</th>
										<th>10</th>
										<th>11</th>
										<th>12</th>
										<th>>12</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">Rs</th>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'firstHourCharge',
																																															'name' => 'firstHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'firstHourCharge' )) ? $tariff_model->get ( 'firstHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'firstHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'secondHourCharge',
																																															'name' => 'secondHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'secondHourCharge' )) ? $tariff_model->get ( 'secondHourCharge' ) : '00.00' 
																																													) );
																																												} else {

																																													echo text ( $tariff_model->get ( 'secondHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'thirdHourCharge',
																																															'name' => 'thirdHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'thirdHourCharge' )) ? $tariff_model->get ( 'thirdHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'thirdHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'fourthHourCharge',
																																															'name' => 'fourthHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'fourthHourCharge' )) ? $tariff_model->get ( 'fourthHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'fourthHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'fifthHourCharge',
																																															'name' => 'fifthHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'fifthHourCharge' )) ? $tariff_model->get ( 'fifthHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'fifthHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'sixthHourCharge',
																																															'name' => 'sixthHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',

																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'sixthHourCharge' )) ? $tariff_model->get ( 'sixthHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'sixthHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'seventhHourCharge',
																																															'name' => 'seventhHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'seventhHourCharge' )) ? $tariff_model->get ( 'seventhHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'seventhHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'eighthHourCharge',
																																															'name' => 'eighthHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'eighthHourCharge' )) ? $tariff_model->get ( 'eighthHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'eighthHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'ninthHourCharge',
																																															'name' => 'ninthHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'ninthHourCharge' )) ? $tariff_model->get ( 'ninthHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'ninthHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'tenthHourCharge',
																																															'name' => 'tenthHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'tenthHourCharge' )) ? $tariff_model->get ( 'tenthHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'tenthHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'eleventhHourCharge',
																																															'name' => 'eleventhHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'eleventhHourCharge' )) ? $tariff_model->get ( 'eleventhHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'eleventhHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'twelveHourCharge',
																																															'name' => 'twelveHourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'twelveHourCharge' )) ? $tariff_model->get ( 'twelveHourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'twelveHourCharge' ) );
																																												}
																																												?>
                                        </td>
										<td>   
                                            <?php
																																												if ($view_mode == EDIT_MODE) {
																																													echo form_input ( array (
																																															'id' => 'above12HourCharge',
																																															'name' => 'above12HourCharge',
																																															'class' => 'form-control',
																																															'required' => 'required',
																																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																																															'value' => ($tariff_model->get ( 'above12HourCharge' )) ? $tariff_model->get ( 'above12HourCharge' ) : '00.00' 
																																													) );
																																												} else {
																																													echo text ( $tariff_model->get ( 'above12HourCharge' ) );
																																												}
																																												?>
                                        </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="fixed_rate_card" class="panel rate-card hidden">
					<div class="form-group">
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Fixed Fare:', 'fixedAmount', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for fixedAmount
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'fixedAmount',
																															'name' => 'fixedAmount',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																															'value' => ($tariff_model->get ( 'fixedAmount' )) ? $tariff_model->get ( 'fixedAmount' ) : '00.00' 
																													) );
																												} else {
																													echo text ( $tariff_model->get ( 'fixedAmount' ) );
																												}
																												?>
                    </div>

					</div>

				</div>
				<div id="distance_rate_card" class="panel rate-card hidden">
					<div class="form-group">
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Minimum Travel Distance:', 'minDistance', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for fixedAmount
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'minDistance',
																															'name' => 'minDistance',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																															'value' => ($tariff_model->get ( 'minDistance' )) ? $tariff_model->get ( 'minDistance' ) : '00.00' 
																													) );
																												} else {
																													echo text ( $tariff_model->get ( 'minDistance' ) );
																												}
																												?>
                    </div>
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Distance Convenience Charge:', 'distanceConvenienceCharge', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for fixedAmount
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'distanceConvenienceCharge',
																															'name' => 'distanceConvenienceCharge',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																															'value' => ($tariff_model->get ( 'distanceConvenienceCharge' )) ? $tariff_model->get ( 'distanceConvenienceCharge' ) : '00.00' 
																													) );
																												} else {
																													echo text ( $tariff_model->get ( 'distanceConvenienceCharge' ) );
																												}
																												?>
                    </div>
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Cost per Kilometer:', 'distanceCostPerKm', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for fixedAmount
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'distanceCostPerKm',
																															'name' => 'distanceCostPerKm',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																															'value' => ($tariff_model->get ( 'distanceCostPerKm' )) ? $tariff_model->get ( 'distanceCostPerKm' ) : '00.00' 
																													) );
																												} else {
																													echo text ( $tariff_model->get ( 'distanceCostPerKm' ) );
																												}
																												?>
                    </div>
					</div>

				</div>
				<div id="time_rate_card" class="panel rate-card hidden">
					<div class="form-group">
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Minimum Travel Time:', 'minTime', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for fixedAmount
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'minTime',
																															'name' => 'minTime',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																															'value' => ($tariff_model->get ( 'minTime' )) ? $tariff_model->get ( 'minTime' ) : '00.00' 
																													) );
																												} else {
																													echo text ( $tariff_model->get ( 'minTime' ) );
																												}
																												?>
                    </div>
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Time Convenience Charge:', 'timeConvenienceCharge', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for fixedAmount
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'timeConvenienceCharge',
																															'name' => 'timeConvenienceCharge',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																															'value' => ($tariff_model->get ( 'timeConvenienceCharge' )) ? $tariff_model->get ( 'timeConvenienceCharge' ) : '00.00' 
																													) );
																												} else {
																													echo text ( $tariff_model->get ( 'timeConvenienceCharge' ) );
																												}
																												?>
                    </div>
						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Cost per Hour:', 'timeCostPerHour', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for fixedAmount
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'timeCostPerHour',
																															'name' => 'timeCostPerHour',
																															'class' => 'form-control',
																															'required' => 'required',
																															'pattern' => '[0-9]+([\.,][0-9]+)?',
																															'value' => ($tariff_model->get ( 'timeCostPerHour' )) ? $tariff_model->get ( 'timeCostPerHour' ) : '00.00' 
																													) );
																												} else {
																													echo text ( $tariff_model->get ( 'timeCostPerHour' ) );
																												}
																												?>
                    </div>
					</div>

				</div>

			</div>

			<div class="ln_solid"></div>
			<div
				<?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
					<button type="button" id="load_tariff_btn" class="btn btn-primary">Cancel</button>
					<button type="button" class="btn btn-success"
						onclick="javascript:saveTariff();">Save</button>
				</div>
			</div>
            <?php if ($view_mode == VIEW_MODE) : ?>
                <div class="form-group">
				<center>
					<button type="button" id="load_tariff_btn" class="btn btn-primary">Back</button>
				</center>
			</div>  
            <?php endif; ?>
           
        </div>
	</div>
</div>
<?php
echo form_close ();
?>