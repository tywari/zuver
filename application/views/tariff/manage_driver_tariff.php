<div id="driver-tariff-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>
				Driver Tariff's List
			</h2>
			<!--<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				 <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li> 
				
			</ul>
			<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" id="search_passenger_content" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="search_passenger_btn" type="button">Go!</button>
                    </span>
                    
                  </div>
                  <div><span>Search by Referral Code, Passenger Full Name, Email and Mobile</span></div>
                </div>
             </div>-->
                <?php if($this->User_Access_Model->getAccessLevelForRateCard()== 2) : ?>
                        <div class="title_right pull-right">
                                   <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                     
                                   </div>
                        </div>
                <?php endif ;?>        
			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			<table id="datatable-checkbox"
				class="table table-striped table-bordered datatable-button-init-collection">
				<thead>
					<tr>
						<th><!-- <input type="checkbox" id="check-all" class="flat"> --></th>
						<!-- <th>Sl no</th> -->
						<th>Action</th>
						<th>Driver Type</th>
						<th>Basic Rates</th>
					</tr>
				</thead>


				<tbody>
				<?php 
				//print_r($driver_tariff_model_list);
				//sl no intialization
				//$i=1;
				//$tariff_model_list=array(0=>array('name'=>'OneWay','dayConvenienceCharge'=>'100.00','nightConvenienceCharge'=>'150.00','fixedAmount'=>'185.00','cityName'=>'Bangalore','status'=>'Y'),1=>array('name'=>'Return','dayConvenienceCharge'=>'200.00','nightConvenienceCharge'=>'250.00','fixedAmount'=>'285.00','cityName'=>'Mumbai','status'=>'Y'));
				foreach ($driver_tariff_model_list as $list)
				{
					
				echo '<tr>';
					
				echo '<td></td>';//<input type="checkbox" class="flat" name="table_records">
				//echo '<th>'.$i.'</th>';
				if($this->User_Access_Model->getAccessLevelForRateCard()== 2) {
					echo '<td>'
							. '<a  href="javascript:void(0);" id="viewTariff-'.$list->driverType.'" onclick="viewDriverTariff(this.id);" >'
									. '<span class="glyphicon glyphicon-eye-open"></span></a> '
											. '/ <a href="javascript:void(0);" id="editDriverTariff-'.$list->driverType.'" onclick="editDriverTariff(this.id);">'
													. '<span class="glyphicon glyphicon-edit"></span></a>'
															. '</td>';
				}else{
					echo '<td>'
							. '<a  href="javascript:void(0);" id="viewDriverTariff-'.$list->driverType.'" onclick="viewDriverTariff(this.id);" >'
									. '<span class="glyphicon glyphicon-eye-open"></span></a> '
											. '</td>';
				}
				echo '<td>'.$list->driverType.'</td>';
				echo '<td>Rs. '.$list->baseRate.' / '.$list->rateFor.' '.($list->extraRateForMin?'For first '.$list->extraRateForMin.' '.$list->extraRateFor:'').'</td>';
				echo '</tr>';
                                
				//$i++;
				}
	            ?>
				</tbody>
			</table>
		</div>
	</div>
</div>