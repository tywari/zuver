<?php
$view_mode = $mode;
date_default_timezone_set("Asia/Kolkata");
?>
<div class="">
	<div class="clearfix"></div>
		<!-- form input mask -->
		<div id="driver-tariff-details-information" class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
	            <div class="x_title">
		            <h2>
		              <?=text ($driver_type_list [$driver_tariff_model[0]->get( 'driverType' )] )?>
		            </h2>
		            <ul class="nav nav-tabs" >
		                <li class="active">
		                	<a href="#tab1default" data-toggle="tab">Trip Allowance</a>
		                </li>
		                <li>
							<a href="#tab2default" data-toggle="tab">Login Incentive</a>
						</li>
		            </ul>
		            <div class="clearfix"></div>
	        	</div>

	        	<div class="x_content tab-content">
                    <div class="tab-pane fade active in" id="tab1default">
	                    <?php
							$form_attr = array (
									'name' => 'edit_driver_tariff_form',
									'id' => 'edit_driver_tariff_form',
									'method' => 'POST',
									'data-parsley-validate' => '',
									'class' => 'form-horizontal form-label-left'
							);
							echo form_open ( base_url ( 'tariff/savedrivertariff' ), $form_attr );
							echo form_input ( array (
										'type' => 'hidden',
										'id' => 'driverType',
										'name' => 'driverType',
										'value' => $driver_tariff_model[0]->get( 'driverType' )
								) );
						?>
			            <table style="border: solid 1px #000; width: 100%">
			            	<tr style="border: solid 1px #000;">
			            		<th>Trip Type</th>
			            		<th>Earning Type</th>
			            		<th>Base Rate</th>
			            		<th>Limit </th>
			            		<th>Extra</th>
			            	</tr>
							<?php //echo count($driver_tariff_model);
							//print_r($driver_tariff_model);
							//print_r($trip_type_list);
							foreach ($driver_tariff_model as $driver_tariff_earningtype) {
								?>
								<tr>
			            		<td><?=text ($trip_type_list [$driver_tariff_earningtype->get( 'tripType' )] ) ?></td>
			            		<?php echo form_input ( array (
														'type' => 'hidden',
														'id' => 'tripType_'.$driver_tariff_earningtype->id,
														'name' => 'driver_rate_card['.$driver_tariff_earningtype->id.'][tripType]',
														'value' =>$driver_tariff_earningtype->get( 'tripType' )
												) );
			            		?>
			            		<td><?=text ($earning_type_list [$driver_tariff_earningtype->get( 'earningType' )] ) ?></td>
			            		<td><?php if($driver_tariff_earningtype->baseRate):
			            		    echo '&#8377 ';
										if ($view_mode == EDIT_MODE) {
												echo form_input ( array (
														'id' => 'baseRate_'.$driver_tariff_earningtype->id,
														'name' => 'driver_rate_card['.$driver_tariff_earningtype->id.'][baseRate]',
														'class' => 'form-control',
														'required' => 'required',
														'style' =>'width:50px; display:inline-block',
														'pattern' => '[0-9]+([\.,][0-9]+)?',
														'value' => $driver_tariff_earningtype->baseRate
												) );
											} else {
												echo $driver_tariff_earningtype->baseRate;
											} ?> / <?=$driver_tariff_earningtype->rateFor ?>
										<?php endif; ?></td>
			            		<td><?php if($driver_tariff_earningtype->rateForLimit):
			            					if ($view_mode == EDIT_MODE) {
												echo form_input ( array (
														'id' => 'rateForLimit_'.$driver_tariff_earningtype->id,
														'name' => 'driver_rate_card['.$driver_tariff_earningtype->id.'][rateForLimit]',
														'class' => 'form-control',
														'style' =>'width:50px; display:inline-block',
														'pattern' => '[0-9]+([\.,][0-9]+)?',
														'value' => $driver_tariff_earningtype->rateForLimit
												) );
											} else {
												echo $driver_tariff_earningtype->rateForLimit;
											} ?><?=$driver_tariff_earningtype->rateFor?> <?=($driver_tariff_earningtype->limitFor=='M'?'/ Month':($driver_tariff_earningtype->limitFor=='D'?'/ Day':''))?><?php endif; ?>
											</td>
			            		<td><?php if($driver_tariff_earningtype->extraRate):
			            		    echo '&#8377 '; if ($view_mode == EDIT_MODE) {
												echo form_input ( array (
														'id' => 'extraRate_'.$driver_tariff_earningtype->id,
														'name' => 'driver_rate_card['.$driver_tariff_earningtype->id.'][extraRate]',
														'class' => 'form-control',
														'required' => 'required',
														'style' =>'width:50px; display:inline-block',
														'pattern' => '[0-9]+([\.,][0-9]+)?',
														'value' => $driver_tariff_earningtype->extraRate
												) );
											} else {
												echo $driver_tariff_earningtype->extraRate;
											} ?> / <?=($driver_tariff_earningtype->earningType=='MA'?$driver_tariff_earningtype->rateFor:$driver_tariff_earningtype->extraRateFor) ?> <?php endif; ?>
											<?php if($driver_tariff_earningtype->extraRateForMin):
												echo 'After';
												if ($view_mode == EDIT_MODE) {
												echo form_input ( array (
														'id' => 'extraRateForMin_'.$driver_tariff_earningtype->id,
														'name' => 'driver_rate_card['.$driver_tariff_earningtype->id.'][extraRateForMin]',
														'class' => 'form-control',
														'required' => 'required',
														'style' =>'width:50px; display:inline-block',
														'pattern' => '[0-9]+([\.,][0-9]+)?',
														'value' => $driver_tariff_earningtype->extraRateForMin
												) );
											} else {
												echo $driver_tariff_earningtype->extraRateForMin;
											} ?> <?=$driver_tariff_earningtype->extraRateFor ?><?php endif; ?></td>
			            	</tr>
						<?php
							}

						?>
						</table>
						<div class="ln_solid"></div>
						<div
							<?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
								<button type="button" id="load_driver_tariff_btn" class="btn btn-primary">Cancel</button>
								<button type="button" class="btn btn-success"
									onclick="javascript:saveDriverTariff();">Save</button>
							</div>
						</div>
			            <?php if ($view_mode == VIEW_MODE) : ?>
			                <div class="form-group">
							<center>
								<button type="button" id="load_driver_tariff_btn" class="btn btn-primary">Back</button>
							</center>
						</div>
			            <?php endif;
							echo form_close ();
						?>
					</div>
                    <div class="tab-pane fade" id="tab2default">
	                    <?php
							$form_attr = array (
									'name' => 'edit_driver_login_incentive_form',
									'id' => 'edit_driver_login_incentive_form',
									'method' => 'POST',
									'data-parsley-validate' => '',
									'class' => 'form-horizontal form-label-left'
							);
							echo form_open ( base_url ( 'tariff/savedrivertariff' ), $form_attr );
							echo form_input ( array (
										'type' => 'hidden',
										'id' => 'driverType',
										'name' => 'driverType',
										'value' => $driver_tariff_model[0]->get( 'driverType' )
								) );
						?>
			            <table style="border: solid 1px #000; width: 100%">
			            	<tr style="border: solid 1px #000;">
			            		<th style="width: ">Day</th>
			            		<th>Shift</th>
			            		<th>Start Time</th>
			            		<th>End Time</th>
			            		<th>Incentive</th>
			            	</tr>
							<?php //echo count($driver_tariff_model);
							//print_r($driver_tariff_model);
							//print_r($trip_type_list);
							foreach ($driver_login_incentive_model as $driver_login_incentives) {
								?>
								<tr>
			            		<td><?php echo date("l", mktime(0,0,0,8,$driver_login_incentives->day,2011)) ?></td>
			            		<td><?php echo $driver_login_incentives->shift; ?></td>
			            		<td><?php if ($view_mode == EDIT_MODE) {
												echo form_input ( array (
														'id' => 'start_time_'.$driver_login_incentives->id,
														'name' => 'driver_incentive['.$driver_login_incentives->id.'][start_time]',
														'class' => 'form-control ui-timepicker-input',
														'required' => 'required',
														'style' =>'width:100px; display:inline-block',
														'pattern' => '^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$',
														'value' => $driver_login_incentives->start_time
												) );
												 } else {
												echo $driver_login_incentives->start_time;
											} ?>
								</td>
								<td><?php if ($view_mode == EDIT_MODE) {
												echo form_input ( array (
														'id' => 'end_time_'.$driver_login_incentives->id,
														'name' => 'driver_incentive['.$driver_login_incentives->id.'][end_time]',
														'class' => 'form-control ui-timepicker-input',
														'required' => 'required',
														'style' =>'width:100px; display:inline-block',
														'pattern' => '^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$',
														'value' => $driver_login_incentives->end_time
												) );
											} else {
												echo $driver_login_incentives->end_time;
											} ?>
								</td>
			            		<td><?php
			            		    echo '&#8377 ';
			            		    if ($view_mode == EDIT_MODE) {
												echo form_input ( array (
														'id' => 'rate_'.$driver_login_incentives->id,
														'name' => 'driver_incentive['.$driver_login_incentives->id.'][rate]',
														'class' => 'form-control',
														'required' => 'required',
														'style' =>'width:100px; display:inline-block',
														'pattern' => '[0-9]+([\.,][0-9]+)?',
														'value' => $driver_login_incentives->rate
												) );
											} else {
												echo $driver_login_incentives->rate;
											} ?>
								</td>
			            	</tr>
						<?php
							}

						?>
						</table>
						<div class="ln_solid"></div>
						<div
							<?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
								<button type="button" id="load_driver_tariff_btn" class="btn btn-primary">Cancel</button>
								<button type="button" class="btn btn-success"
									onclick="javascript:saveDriverLoginIncentive();">Save</button>
							</div>
						</div>
			            <?php if ($view_mode == VIEW_MODE) : ?>
			                <div class="form-group">
							<center>
								<button type="button" id="load_driver_tariff_btn" class="btn btn-primary">Back</button>
							</center>
						</div>
			            <?php endif;
							echo form_close ();
						?>
                    </div>
                </div>
            </div>
        </div>
</div>
