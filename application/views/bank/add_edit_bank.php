<?php
$view_mode = $mode;
?>
<div id="bank-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Edit Bank Account Details</h2>
			<ul class="nav navbar-right panel_toolbox">
				<!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br />
			<?php
			$form_attr = array (
					'name' => 'edit_bank_form',
					'id' => 'edit_bank_form',
					
					'data-parsley-validate' => '',
					'class' => 'form-horizontal form-label-left' 
			);
			echo form_open_multipart ( base_url(''), $form_attr );
			
			// passenger id by default is -1
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'bank_id',
					'name' => 'id',
					'value' => ($bank_model->get ( 'id' )) ? $bank_model->get ( 'id' ) : - 1 
			) );
			
			?>
			<?php
			/*
			 * if ($view_mode == VIEW_MODE || $passenger_model->get ( 'passengerCode' )) {
			 * echo '<div class="form-group">';
			 *
			 * echo form_label ( 'Passenger Referral Code:', 'passengerCode', array (
			 * 'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
			 * ) );
			 *
			 * echo '<div class="col-md-6 col-sm-6 col-xs-12">';
			 * echo text ( $passenger_model->get ( 'passengerCode' ) );
			 *
			 * echo '</div></div>';
			 * }
			 * if ($view_mode == VIEW_MODE || $passenger_model->get ( 'lastLoggedIn' )) {
			 * echo '<div class="form-group">';
			 *
			 * echo form_label ( 'Last Logged In:', 'lastLoggedIn', array (
			 * 'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
			 * ) );
			 *
			 * echo '<div class="col-md-6 col-sm-6 col-xs-12">';
			 * echo text ( $passenger_model->get ( 'lastLoggedIn' ) );
			 *
			 * echo '</div></div>';
			 * }
			 */
			?>
			<div class="form-group">
					<?php
					echo form_label ( 'Account Holder Name:', 'accountHolderName', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'accountHolderName',
									'name' => 'accountHolderName',
									'class' => 'form-control',
									//'required' => 'required',
									//'pattern' => '[A-Za-z\s]{3,15}',
									'value' => ($bank_model->get ( 'accountHolderName' )) ? $bank_model->get ( 'accountHolderName' ) : '' 
							) );
						} else {
							echo text ( $bank_model->get ( 'accountHolderName' ) );
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Account Number:', 'accountNumber', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'accountNumber',
									'name' => 'accountNumber',
									'class' => 'form-control',
									//'required' => 'required',
									//'pattern' => '\d{7,15}\.\d+',
									'value' => ($bank_model->get ( 'accountNumber' )) ? $bank_model->get ( 'accountNumber' ) : '' 
							) );
						} else {
							echo text ( $bank_model->get ( 'accountNumber' ) );
						}
						
						?>
					</div>
			</div>

			<div class="form-group">
					<?php
					echo form_label ( 'IFSC Code:', 'ifscCode', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'ifscCode',
									'name' => 'ifscCode',
									'class' => 'form-control col-md-7 col-xs-12',
									//'required' => 'required',
									//'pattern' => '[A-Z]{4}[0-9]{7}',
									'value' => ($bank_model->get ( 'ifscCode' )) ? $bank_model->get ( 'ifscCode' ) : '' 
							) );
						} else {
							echo text ( $bank_model->get ( 'ifscCode' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Bank Name:', 'bankName', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'bankName',
									'name' => 'bankName',
									'class' => 'form-control col-md-7 col-xs-12',
									//'required' => 'required',
									//'pattern' => '[a-zA-Z\s]{3,30}',
									'value' => ($bank_model->get ( 'bankName' )) ? $bank_model->get ( 'bankName' ) : '' 
							) );
						} else {
							echo text ( $bank_model->get ( 'bankName' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Bank Branch:', 'bankBranch', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'bankBranch',
									'name' => 'bankBranch',
									'class' => 'form-control col-md-7 col-xs-12',
									//'required' => 'required',
									//'pattern' => '[a-zA-Z\s]{3,15}',
									'value' => ($bank_model->get ( 'bankBranch' )) ? $bank_model->get ( 'bankBranch' ) : '' 
							) );
						} else {
							echo text ( $bank_model->get ( 'bankBranch' ) );
						}
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Bank Address:', 'bankAddress', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_textarea ( array (
									'id' => 'bankAddress',
									'name' => 'bankAddress',
									'class' => 'form-control',
									'rows' => '3',
									//'required' => 'required',
									'value' => ($bank_model->get ( 'bankAddress' )) ? $bank_model->get ( 'bankAddress' ) : '' 
							) );
						} else {
							echo text ( $bank_model->get ( 'bankAddress' ) );
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Bank Code:', 'bankCode', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'bankCode',
									'name' => 'bankCode',
									'class' => 'form-control',
									//'required' => 'required',
									//'pattern' => '\d{9}\.\d+',
									'value' => ($bank_model->get ( 'bankCode' )) ? $bank_model->get ( 'bankCode' ) : '' 
							) );
						} else {
							echo text ( $bank_model->get ( 'bankCode' ) );
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Bank Routing Code:', 'bankRoutingCode', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'bankRoutingCode',
									'name' => 'bankRoutingCode',
									'class' => 'form-control',
									//'required' => 'required',
									//'pattern' => '\d{9}\.\d+',
									'value' => ($bank_model->get ( 'bankRoutingCode' )) ? $bank_model->get ( 'bankRoutingCode' ) : '' 
							) );
						} else {
							echo text ( $bank_model->get ( 'bankRoutingCode' ) );
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Upload Cheque Image:', 'chequeImage', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 ' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_upload ( array (
									'id' => 'chequeImage',
									'name' => 'chequeImage[]',
									'class' => 'form-control',
									//'required' => 'required',
									'value' => ($bank_model->get ( 'chequeImage' )) ? $bank_model->get ( 'chequeImage' ) : '' 
							) );
						} else {
							echo '<img alt="cheque_image" width="240" height="60" src="'.bank_content_url($bank_model->get ( 'id' ).'/'.$bank_model->get ( 'chequeImage' )).'">';
							//echo text ( $bank_model->get ( 'chequeImage' ) );
						}
						
						?>
					</div>
					
			</div>
			<div class="ln_solid"></div>
			<div
				<?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<button type="button" id="load_bank_btn" class="btn btn-primary">Cancel</button>
					<button type="button" id="save_bank_btn" class="btn btn-success"
						onclick="saveBank();">Save</button>
				</div>
			</div>
                        <?php  if($view_mode == VIEW_MODE) : ?>
                        <div class="form-group">
				<center>
					<button type="button" id="load_bank_btn" class="btn btn-primary">Back</button>
				</center>
			</div>  
                      <?php endif ; ?>  
			<?php
			echo form_close ();
			?>
		</div>
	</div>
</div>