 <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

<script src="//datatables.net/download/build/nightly/jquery.dataTables.js"></script>
<div id="passenger-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>
				Customer's List
			</h2>
			<!--<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				 <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li> 
				
			</ul>
			<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" id="search_passenger_content" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="search_passenger_btn" type="button">Go!</button>
                    </span>
                    
                  </div>
                  <div><span>Search by Referral Code, Passenger Full Name, Email and Mobile</span></div>
                </div>
             </div>-->
             <div class="title_right pull-right">
                
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 ">
					<a href="<?php echo base_url('passenger/add')?>"><button id="add_passenger_btn"
						class="btn btn-primary">Add Customer</button></a>
					
                </div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			<table id="datatable-checkbox"
				class="table table-striped table-bordered datatable-button-init-collection">
				<thead>
					<tr>
						<th>Action</th>
						<th>Passenger Name</th>
						<th>Email</th>
						<th>Mobile</th>
						<th>Wallet Balance </th>
						<th>Trips Completed</th>
						<th>Rating </th>
						<th>Last Logged In</th>
						<th>SignUp By</th>
						<th>Registered By</th>
						<th>Status</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">

if ( $.fn.dataTable.isDataTable( '#datatable-checkbox' ) ) {
    table = $('#datatable-checkbox').DataTable();
}
else {
    table = $('#datatable-checkbox').DataTable({
        "orderCellsTop": true, 
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "ajax":{
		    "url":  BASE_URL +"/passenger/getPassengersAjax",
		    "type": "POST"
		  },
        "columns": [
        	{ "data": "actions"},
            { "data": "name" },
            { "data": "email" },
            { "data": "mobile" },
            { "data": "walletAmount" },
            { "data": "totalTripComplete" },
            { "data": "rating" },
            { "data": "lastLoggedIn" },
            { "data": "signupFrom" },
            { "data": "registerType" },
            { "data": "status"}
        ],
        rowCallback: function(row, data, index) {
        	//console.log(data);  
    	}
  } );
}

</script>