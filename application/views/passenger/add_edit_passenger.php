<?php
$view_mode = $mode;
?>
<div id="passenger-details-information"
	class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Add Customer</h2>
			<ul class="nav navbar-right panel_toolbox">
				<!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br />
			<?php
			$form_attr = array (
					'name' => 'edit_passenger_form',
					'id' => 'edit_passenger_form',
					'method' => 'POST',
					'data-parsley-validate' => '',
					'class' => 'form-horizontal form-label-left' 
			);
			echo form_open ( base_url ( '' ), $form_attr );
			
			// passenger id by default is -1
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'passenger_id',
					'name' => 'id',
					'value' => ($passenger_model->get ( 'id' )) ? $passenger_model->get ( 'id' ) :-1 
			) );
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'companyId',
					'name' => 'companyId',
					'value' => DEFAULT_COMPANY_ID
			) );
			?>
				
			<?php
			
			if ($passenger_model->get ( 'passengerCode' ) || $passenger_model->get ( 'profileImage' ) || $passenger_model->get ( 'lastLoggedIn' ) || (count($passenger_emergency_model)> 0)) {
				//start of emergency details
				if(count($passenger_emergency_model)>0)
				{
					echo '<div class="form-group"><div class="x_title"><h4><strong>Emergency Contact Details</strong></h4><div class="clearfix"></div></div>';
					echo '<div class="panel"><div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body">';
					echo '<table class="table table-bordered">';
					echo '<thead><tr><th>Contact Person</th><th>Contact Number</th><th>Email-Id</th></tr></thead>';
					echo '<tbody>';
					foreach ($passenger_emergency_model as $list)
					{
						echo '<tr>';
						echo '<td>'.$list->name.'</td>';
						echo '<td>'.$list->mobile.'</td>';
						echo '<td>'.$list->email.'</td>';
						echo '</tr>';
					}
					echo '</tbody>';
					echo '</table>';
					echo '</div></div></div></div>';
				}
				//end of emergency details
				echo '<div class="form-group">';
				echo '<div class="col-md-6 col-sm-6 col-xs-12">';
				//echo text ( $passenger_model->get ( 'profileImage' ) );
				$profile_image=($passenger_model->get ("profileImage"))?passenger_content_url($passenger_model->get ("id").'/'.$passenger_model->get ("profileImage")):image_url('app/user.png');
				echo '<img src="'.$profile_image.'" alt="Profile Image" width="80" height="80" style="margin-left:50%">';
				echo '</div></div>';
			
			
				echo '<div class="form-group">';
				
				echo form_label ( 'Passenger Referral Code:', 'passengerCode', array (
						'class' => 'control-label col-md-3 col-sm-3 col-xs-12' 
				) );
				
				echo '<div class="col-md-6 col-sm-6 col-xs-12">';
				echo text ( $passenger_model->get ( 'passengerCode' ) );
				
				echo '</div></div>';
			
				echo '<div class="form-group">';
				
				echo form_label ( 'Last Logged In:', 'lastLoggedIn', array (
						'class' => 'control-label col-md-3 col-sm-3 col-xs-12' 
				) );
				
				echo '<div class="col-md-6 col-sm-6 col-xs-12">';
				echo text ( $passenger_model->get ( 'lastLoggedIn' ) );
				
				echo '</div></div>';
			}
			?>
			
			<div class="form-group">
					<?php
					echo form_label ( 'First Name:', 'firstName', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'firstName',
									'name' => 'firstName',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z\s]{3,30}',
									'value' => ($passenger_model->get ( 'firstName' )) ? $passenger_model->get ( 'firstName' ) : '' 
							) );
						} else {
							echo text ( $passenger_model->get ( 'firstName' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Last Name:', 'lastName', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'lastName',
									'name' => 'lastName',
									'class' => 'form-control col-md-7 col-xs-12',
									//'required' => 'required',
									//'pattern'=>'[a-zA-Z\s]{1,15}',
									'value' => ($passenger_model->get ( 'lastName' )) ? $passenger_model->get ( 'lastName' ) : '' 
							) );
						} else {
							echo text ( $passenger_model->get ( 'lastName' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Email:', 'email', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'email',
									'name' => 'email',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
									'value' => ($passenger_model->get ( 'email' )) ? $passenger_model->get ( 'email' ) : '' 
							) );
						} else {
							echo text ( $passenger_model->get ( 'email' ) );
						}
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Mobile:', 'mobile', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'mobile',
									'name' => 'mobile',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[789]\d{9}',
									'value' => ($passenger_model->get ( 'mobile' )) ? $passenger_model->get ( 'mobile' ) : '' 
							) );
						} else {
							echo text ( $passenger_model->get ( 'mobile' ) );
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'City Name:', 'cityId', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_dropdown ( 'cityId', $city_list, $passenger_model->get ( 'cityId' ), array (
									'id' => 'cityId',
									'class' => 'form-control',
									'required' => 'required' 
							) );
						} else {
							echo text ( $city_list[$passenger_model->get ( 'cityId' )]);
						}
						
						?>
					</div>
			</div>
			<div class="ln_solid"></div>
			<div
				<?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<button type="button" id="load_passenger_btn"
						class="btn btn-primary">Cancel</button>
					<button type="button" id="save_psassenger_btn"
						class="btn btn-success" onclick="savePassenger();"
						data-loading-text="<i  class='fa fa-spinner fa-spin '>
						</i> Processing">Save
					</button>
				</div>
			</div>
                         <?php  if($view_mode == VIEW_MODE) : ?>
             <div class="form-group">
				<center>
					<button type="button" id="load_passenger_btn"
						class="btn btn-primary">Back</button>
				</center>
			</div>  
                        <?php endif ; ?>
			<?php
			echo form_close ();
			?>
			
			
		</div>
	</div>
</div>