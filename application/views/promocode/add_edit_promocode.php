<?php
$view_mode = $mode;
?>
<div id="promocode-details-information"
     class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Add Promocode</h2>
            <ul class="nav navbar-right panel_toolbox">
                    <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                      <li class="dropdown"><a href="#" class="dropdown-toggle"
                            data-toggle="dropdown" role="button" aria-expanded="false"><i
                                    class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                            </ul></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <?php
            $form_attr = array(
                'name' => 'edit_promocode_form',
                'id' => 'edit_promocode_form',
                'class' => 'form-horizontal form-label-left',
                'method' => 'POST'
            );
            echo form_open(base_url('promocode/savePromocode'), $form_attr);

            // passenger id by default is -1
            echo form_input(array(
                'type' => 'hidden',
                'id' => 'promocode_id',
                'name' => 'id',
                'value' => ($promocode_model->get('id')) ? $promocode_model->get('id') : -1
            ));
            ?>
            <div class="form-group">
                <?php
                echo form_label('Passengers Type:', 'passengerType', array(
                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                ));
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
                if ($view_mode == EDIT_MODE) {

                    echo '<p>';
                    //debug_exit($driver_gender);
                    // validation for pm to show only delete buttons & admin for their draft release only delete buttons



                    echo form_radio(array(
                        'id' => 'passengerType',
                        'name' => 'passengerType',
                        'class' => 'flat',
                        'value' => 0,
                    	'checked' => ($promocode_model->get('passengerType')==0)?'checked':''
                    ));
                    echo 'Selected Passenger';

                    echo "</br>";
                    echo form_radio(array(
                        'id' => 'passengerType',
                        'name' => 'passengerType',
                        'class' => 'flat',
                        'value' => 1,
                        'checked' => ($promocode_model->get('passengerType')==1)?'checked':''
                    ));
                    echo 'All Passenger';
                    echo '</p>';
                } else {
                    //echo text($promocode_model->get('passengerType'));
                	echo '<p>';
                	//debug_exit($driver_gender);
                	// validation for pm to show only delete buttons & admin for their draft release only delete buttons
                	
                	
                	
                	echo form_radio(array(
                			'id' => 'passengerType',
                			'name' => 'passengerType',
                			'class' => 'flat',
                			'value' => 0,
                			'checked' => ($promocode_model->get('passengerType')==0)?'checked':'',
                			'disabled'=>'true'
                	));
                	echo 'Selected Passenger';
                	
                	echo "</br>";
                	echo form_radio(array(
                			'id' => 'passengerType',
                			'name' => 'passengerType',
                			'class' => 'flat',
                			'value' => 1,
                			'checked' => ($promocode_model->get('passengerType')==1)?'checked':'',
                			'disabled'=>'true'
                	));
                	echo 'All Passenger';
                	echo '</p>';
                }
                ?>
                </div>

            </div>

            <div class="form-group" id="passenger-details">
                    <?php
                    echo form_label('Passengers:', 'passengerName', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                    ));
                    ?>
                
                
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
                // validation for passenger first name
                if ($view_mode == EDIT_MODE) {
                    echo form_input(array(
                        'id' => 'passengerName',
                        'class' => 'form-control col-md-7 col-xs-12',
                        'required' => 'required',
                        'placeholder'=>'Mobile',
                        'value' => ($passenger_model->get ( 'firstName' )) ? $passenger_model->get ( 'firstName' ) .' '. $passenger_model->get ( 'lastName' ) : ''
                    ));
                    echo "<div id='suggesstion-box'></div>";
                       echo form_input(array(
                            'type' => 'hidden',
                            'id' => 'passengerId',
                            'name' => 'passengerId',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'required' => 'required',
                            'placeholder'=>'Mobile',
                            'value' => ($promocode_model->get('passengerId')) ? $promocode_model->get('passengerId') : '0'
                        ));
                } else {
                    //echo text($promocode_model->get('passengerId'));
                    echo text($passenger_model->get ( 'firstName' ) .' '. $passenger_model->get ( 'lastName' ));
                }
                ?>
                </div>

            </div>
            <div class="form-group">
                    <?php
                    echo form_label('City Name:', '	cityId[]', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                    ));
                    ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <?php
                // validation for passenger first name
                if ($view_mode == EDIT_MODE) {
                    echo form_multiselect('cityId[]', $city_list, $promocode_model->get('cityId'), array(
                        'id' => 'cityId',
                        'class' => 'form-control col-md-7 col-xs-12',
                        'required' => 'required'
                    ));
                } else {
                    echo text($promocode_model->get('cityId'));
                }
                ?>
                </div>

            </div>
            <div class="form-group">
                    <?php
                    echo form_label('Promocode:', 'promoCode', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                    ));
                    ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
<?php
// validation for passenger first name
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'promoCode',
        'name' => 'promoCode',
        'class' => 'form-control col-md-7 col-xs-12',
        'required' => 'required',
        'pattern' => '[a-zA-Z0-9-_+.#\s]{1,15}',
        'value' => ($promocode_model->get('promoCode')) ? $promocode_model->get('promoCode') : ''
    ));
} else {
    echo text($promocode_model->get('promoCode'));
}
?>
                </div>

            </div>
            <div class="form-group">
                    <?php
                    echo form_label('Discount Type:', '	promoDiscountType', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                    ));
                    ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
<?php
// validation for passenger first name
if ($view_mode == EDIT_MODE) {
    echo form_dropdown('promoDiscountType', $promo_discount_type_list, $promocode_model->get('promoDiscountType'), array(
        'id' => 'promoDiscountType',
        'class' => 'form-control col-md-7 col-xs-12',
        'required' => 'required'
    ));
} else {
    echo text($promo_discount_type_list[$promocode_model->get('promoDiscountType')]);
}
?>
                </div>

            </div>
            <div class="form-group">
                    <?php
                    echo form_label('Discount Amount:', 'promoDiscountAmount', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                    ));
                    ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'promoDiscountAmount',
                            'name' => 'promoDiscountAmount',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'required' => 'required',
                            'pattern' => '\d{1,4}\.?\d{1,2}',
                        	'placeholder'=>'Eg:05 / 05.00',
                            'value' => ($promocode_model->get('promoDiscountAmount')) ? $promocode_model->get('promoDiscountAmount') : ''
                        ));
                    } else {
                        echo text($promocode_model->get('promoDiscountAmount'));
                    }
                    ?>
                </div>

            </div>
            <div class="form-group">
                    <?php
                    echo form_label('Start Date:', 'promoStartDate', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                    ));
                    ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'promoStartDate',
                            'name' => 'promoStartDate',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'autocomplete' => 'off',
                            'required' => 'required',
                            'value' => ($promocode_model->get('promoStartDate')) ? $promocode_model->get('promoStartDate') : ''
                        ));
                    } else {
                        echo text($promocode_model->get('promoStartDate'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                    <?php
                    echo form_label('End Date:', 'promoEndDate', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                    ));
                    ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'promoEndDate',
                            'name' => 'promoEndDate',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'autocomplete' => 'off',
                            'required' => 'required',
                            'value' => ($promocode_model->get('promoEndDate')) ? $promocode_model->get('promoEndDate') : ''
                        ));
                    } else {
                        echo text($promocode_model->get('promoEndDate'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                    <?php
                    echo form_label('Total Limit Count:', 'promoCodeLimit', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                    ));
                    ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'promoCodeLimit',
                            'name' => 'promoCodeLimit',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'required' => 'required',
                            'pattern' => '\d{1,3}',
                            'value' => ($promocode_model->get('promoCodeLimit')) ? $promocode_model->get('promoCodeLimit') : ''
                        ));
                    } else {
                        echo text($promocode_model->get('promoCodeLimit'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                    <?php
                    echo form_label('User Limit Count:', 'promoCodeUserLimit', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                    ));
                    ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'promoCodeUserLimit',
                            'name' => 'promoCodeUserLimit',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'required' => 'required',
                            'pattern' => '\d{1,3}',
                            'value' => ($promocode_model->get('promoCodeUserLimit')) ? $promocode_model->get('promoCodeUserLimit') : ''
                        ));
                    } else {
                        echo text($promocode_model->get('promoCodeUserLimit'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                    <?php
                    echo form_label('Is Free Ride Promocode:', 'isFreeRide', array(
                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                    ));
                    ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_checkbox(array(
                            'id' => 'isFreeRide',
                            'name' => 'isFreeRide',
                        	'checked' =>($promocode_model->get('isFreeRide')== Status_Type_Enum::ACTIVE) ? TRUE : FALSE,
                            'value' => ($promocode_model->get('isFreeRide')== Status_Type_Enum::ACTIVE) ? 'Y' : 'N'
                        ));
                    } else {
                        echo text(($promocode_model->get('isFreeRide')== Status_Type_Enum::ACTIVE)?'Yes':'No');
                    }
                    ?>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div <?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="button" id="load_promocode_btn"
                            class="btn btn-primary">Cancel</button>
                    <button type="button" id="save_promocode_btn"
                            class="btn btn-success" onclick="savePromocode();">Save</button>
                </div>
            </div>
            <?php  if($view_mode == VIEW_MODE) : ?>
                            <div  class="form-group">
                                <center><button type="button" id="load_promocode_btn" class="btn btn-primary" >Back</button></center>
                            </div>  
            <?php endif ; ?>
                    <?php
                    echo form_close();
                    ?>
        </div>
    </div>
</div>


