<div id="wallet-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
                    <?php  
                       $passnger_fname = (isset($wallet_model_list[0]->passengerFirstName)) ? $wallet_model_list[0]->passengerFirstName : 'Passenger' ;  
                       $passnger_Lname = (isset($wallet_model_list[0]->passengerLastName)) ? $wallet_model_list[0]->passengerLastName : '' ;  
                       
                      ;?>
                    
			<h2> <?php echo $passnger_fname.' '.$passnger_Lname  ;?> Transaction Details </h2>
			<!--<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				 <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li> 
				
			</ul>
			<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" id="search_promocode_content" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="search_promocode_btn" type="button">Go!</button>
                    </span>
                    
                  </div>
                  <div><span>Search by Promo Code, Passenger Name, </span></div>
                </div>
             </div>-->
            <?php //if($this->User_Access_Model->getAccessLevelForPromoCode()== 2) : ?>
             <div class="title_right pull-right hidden">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a href="<?php echo base_url('wallet/add')?>"><button id="add_wallet_btn"
                            class="btn btn-primary">Add Wallet</button>
                    </a>
                </div>
             </div>
            <?php //endif ;?>            
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
                    
			<table id="datatable-checkbox"
				class="table table-striped table-bordered datatable-button-init-collection">
				<thead>
					<tr>
						<th><!-- <input type="checkbox" id="check-all" class="flat"> --></th>
						<!-- <th>Sl no</th> -->
						<th>Previous Wallet Amount</th>
						<th>Transaction Amount</th>
						<th>Current Wallet Amount</th>
						<th>Trip Id</th>
						<th>Transaction Type</th>
						<th>Payment Mode</th>
						<th>Manipulation Type</th>
						<th>User Name</th>
						<th>Comments</th>
						<th>Date Time</th>
					</tr>
				</thead>


				<tbody>
				<?php 
				//sl no intialization
				//$i=1;
				//$promocode_model_list=array(0=>array('promoCode'=>'Zuver','promoDiscountAmount'=>'100 Rs','promoDiscountType'=>'Fixed','promoStartDate'=>'19-09-2016 10:10:10','promoEndDate'=>'21-09-2016 10:10:00','promoCodeLimit'=>1,'cityName'=>'Pune','passengerName'=>'All','status'=>1),1=>array('promoCode'=>'ZuverKiran','promoDiscountAmount'=>'10 %','promoDiscountType'=>'Percentage','promoStartDate'=>'19-09-2016 10:10:10','promoEndDate'=>'21-09-2016 10:10:00','promoCodeLimit'=>1,'cityName'=>'Bangalore','passengerName'=>'Kiran Kumar','status'=>0));
				foreach ($wallet_model_list as $list)
				{
				    $trip_id       = (($list->tripId > 0)) ? $list->tripId : "N/A";
				    $payment_mode  = (isset($list->paymentMode)) ? $list->paymentMode : "N/A";
                                    $user_name     = (($list->transactionType ==Transaction_Type_Enum::MN)) ?
                                                      $list->userFirstName.' '.$list->userLastName : "N/A" ;
                                    $comments      =  (($list->transactionType ==Transaction_Type_Enum::MN)) ?
                                                      $list->comments : "N/A" ;
                                        echo '<tr>';

                                        echo '<td></td>';//<input type="checkbox" class="flat" name="table_records">
                                        //echo '<th>'.$i.'</th>';
                                        echo '<td>'.$list->previousAmount.'</td>';
                                        echo '<td>'.$list->transactionAmount.'</td>';
                                        echo '<td>'.$list->currentAmount.'</td>';
                                        echo '<td>'.$trip_id.'</td>';
                                        echo '<td>'.$list->transactionType.'</td>';
                                        echo '<td>'.$payment_mode.'</td>';
                                        echo '<td>'.$list->manipulationType.'</td>';
                                        echo '<td>'.$user_name.'</td>';
                                        echo '<td>'.$comments.'</td>';
                                        echo '<td>'.$list->modifyDatetime.'</td>';


                                        echo '</tr>';
				//$i++;
				}
	            ?>
				</tbody>
			</table>
                    <div class="clearfix"></div>
                    <div class='row row-centered' style="text-align:center;padding:10px;"> 
                          <button type="button" id="load_passenger_btn" class="btn btn-primary">Back</button>  
                    </div>
		</div>
	</div>
</div>

