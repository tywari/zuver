<?php
$view_mode = $mode;
?>
<div id="wallet-details-information"
     class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Manage Wallet</h2>
            <ul class="nav navbar-right panel_toolbox">
                    <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li class="dropdown"><a href="#" class="dropdown-toggle"
                data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a></li>
                        <li><a href="#">Settings 2</a></li>
                </ul></li>
        <li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <?php
            $form_attr = array(
                'name' => 'edit_wallet_form',
                'id' => 'edit_wallet_form',
                'class' => 'form-horizontal form-label-left',
                'method' => 'POST'
            );
            echo form_open(base_url('wallet/saveWallet'), $form_attr);

            // passenger id by default is -1
            echo form_input(array(
                'type' => 'hidden',
                'id' => 'wallet_id',
                'name' => 'id',
                'value' => ($wallet_details_model->get('id')) ? $wallet_details_model->get('id') : - 1
            ));
            ?>


            <div class="form-group">
                <?php
                echo form_label('Passengers:', 'passengerId', array(
                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                ));
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'passengerName',
                            'name' => 'passengerName',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'required' => 'required',
                            'placeholder'=>'Mobile',
                            'value' =>  ''
                        ));
                       echo "<div id='suggesstion-box'></div>";
                       echo form_input(array(
                            'type' => 'hidden',
                            'id' => 'passengerId',
                            'name' => 'passengerId',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'required' => 'required',
                            'placeholder'=>'Mobile',
                            'value' => ($wallet_details_model->get('passengerId')) ? $wallet_details_model->get('passengerId') : ''
                        ));
                    } else {
                        echo text($wallet_details_model->get('passengerId'));
                    }
                    ?>
                </div>

            </div>
            <div class="form-group">
                <?php
                echo form_label('Previous Wallet Balance:', 'previousAmount', array(
                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                ));
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
// validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'previousAmount',
                            'name' => 'previousAmount',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'required' => 'required',
                            'pattern' => '[a-zA-Z0-9-_+.#\s]{1,15}',
                            'value' => ($wallet_details_model->get('previousAmount')) ? $wallet_details_model->get('previousAmount') : ''
                        ));
                    } else {
                        echo text($wallet_details_model->get('previousAmount'));
                    }
                    ?>
                </div>

            </div>
            <div class="form-group">
                <?php
                echo form_label('Manupilation Type:', '	manipulationType', array(
                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                ));
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('manipulationType', $manipulation_type_list, $wallet_details_model->get('manipulationType'), array(
                            'id' => 'manipulationType',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'required' => 'required'
                        ));
                    } else {
                        echo text($wallet_details_model->get('manipulationType'));
                    }
                    ?>
                </div>

            </div>


            <div class="form-group">
                <?php
                echo form_label('Transaction Amount:', 'transactionAmount', array(
                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                ));
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'transactionAmount',
                            'name' => 'transactionAmount',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'required' => 'required',
                            'pattern' => '\d{1,4}\.?\d{2}',
                            'value' => ($wallet_details_model->get('transactionAmount')) ? $wallet_details_model->get('transactionAmount') : ''
                        ));
                    } else {
                        echo text($wallet_details_model->get('transactionAmount'));
                    }
                    ?>
                </div>

            </div>
            <div class="form-group">
                <?php
                echo form_label('Updated Wallet Balance:', 'currentAmount', array(
                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                ));
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'currentAmount',
                            'name' => 'currentAmount',
                            'class' => 'form-control col-md-7 col-xs-12',
                            'autocomplete' => 'off',
                            'required' => 'required',
                            'readonly' => 'readonly',
                            'value' => ($wallet_details_model->get('currentAmount')) ? $wallet_details_model->get('currentAmount') : ''
                        ));
                    } else {
                        echo text($wallet_details_model->get('currentAmount'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?php
                echo form_label('Comments:', 'comments', array(
                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                ));
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_textarea(array(
                            'id' => 'comments',
                            'name' => 'comments',
                            'class' => 'form-control',
                        	'rows' => '3',
                            'required' => 'required',
                            'value' => ($wallet_details_model->get('comments')) ? $wallet_details_model->get('comments') : ''
                        ));
                    } else {
                        echo form_textarea(array(
                            'id' => 'comments',
                            'name' => 'comments',
                            'class' => 'form-control',
                        	'rows' => '3',
                            'required' => 'required',
                        	'readonly'=>'readonly',
                            'value' => ($wallet_details_model->get('comments')) ? $wallet_details_model->get('comments') : ''
                        ));
                    }
                    ?>
                </div>
            </div>

            <div class="ln_solid"></div>
            <div
                <?php echo ($view_mode == VIEW_MODE) ? 'class="form-group hidden" ' : 'class="form-group"'; ?>>
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="button" id="load_wallet_btn" class="btn btn-primary">Cancel</button>
                    <button type="button" id="save_wallet_btn" class="btn btn-success"
                            onclick="saveWallet();">Save</button>
                </div>
            </div>
            <?php if ($view_mode == VIEW_MODE) : ?>
                <div class="form-group">
                    <center>
                        <button type="button" id="load_wallet_btn" class="btn btn-primary">Back</button>
                    </center>
                </div>  
            <?php endif; ?>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</div>


