<div class="">
    <div class="tile_count"> 

        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-users"></i>Cash Collections</span>
            <div class="count"><?php echo $total_cash_collection;?></div>
           <!--  <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span> -->
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-ticket"></i>Wallet Collections</span>
            <div class="count"> <?php echo $total_wallet_collection;?></div>
           <!--  <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-rupee"></i>PAYTM Collections</span>
            <div class="count"><?php echo $total_paytm_collection;?></div>
            <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-rupee"></i>Invoice Collections</span>
            <div class="count"><?php echo $total_invoice_collection;?></div>
            <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-rupee"></i> Total Collections</span>
            <div class="count"><?php echo $total_collection;?></div>
            <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
        </div>
    </div>
    <div class="">
    <!-- Just added to temporaary for display clean & empty by kiran-->
<div style="height:500px"></div>
<!-- Just added to temporaary for display clean & empty by kiran-->
        <!-- bar chart -->
        <!--<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Weekly revenue<small>Sessions</small></h2>                   
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div id="graph_bar" style="width:100%; height:280px;"></div>
                </div>
            </div>
        </div>-->
        <!-- /bar charts -->

        <!-- bar charts group -->
        <!--<div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Revenue Against Booking<small>Sessions</small></h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content1">
                    <div id="graph_bar_group" style="width:100%; height:280px;"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>-->
        <!-- /bar charts group -->


    </div>
    <div class="">
        <!-- <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-bell"></i> Top 10 <small>Transaction Summary</small></h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content bs-example-popovers">

                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>ZU0023</strong> Trip Confirmed Coimbatore to Bangalore
                    </div>
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>ZU0025</strong> New Booking Jpnagar to malleswaram
                    </div>
                    <div class="alert alert-warning alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>ZU0020</strong> Triped completed
                    </div>
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>ZU0021</strong> Trip cancelled
                    </div>

                </div>
            </div> 
        </div>
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Active Partners <small>Sessions</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <ul class="list-unstyled msg_list">
                        <li>
                            <a>
                                <span class="image">
                                    <img src="images/Profileimg.jpg" alt="img" />
                                </span>
                                <span>
                                    <span>John Smith</span>
                                    <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that
                                </span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span class="image">
                                    <img src="images/Profileimg.jpg" alt="img" />
                                </span>
                                <span>
                                    <span>John Smith</span>
                                    <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that
                                </span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span class="image">
                                    <img src="images/Profileimg.jpg" alt="img" />
                                </span>
                                <span>
                                    <span>John Smith</span>
                                    <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that
                                </span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span class="image">
                                    <img src="images/Profileimg.jpg" alt="img" />
                                </span>
                                <span>
                                    <span>John Smith</span>
                                    <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>-->
    </div>


</div>

<script>
      
    </script>