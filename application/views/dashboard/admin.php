<div class="">

	<div class="top_tiles">
		<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon">
					<i class="fa  fa-volume-control-phone"></i>
				</div>
				<div class="count"><?php echo $active_employees; ?></div>
				<h3>Employee</h3>
				<p>
					<span class="count_bottom"><i class="red"><i
							class="fa fa-sort-desc"></i><?php echo $deactive_employees; ?></i>
						Deactive Zuver Employees</span>
				</p>
			</div>
		</div>
		<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon">
					<i class="fa fa-users""></i>
				</div>
				<div class="count"><?php echo $active_passengers; ?></div>
				<h3>Customers</h3>
				<p>
					<span class="count_bottom"><i class="red"><i
							class="fa fa-sort-desc"></i><?php echo $deactive_passengers; ?></i>
						Deactive Customers</span>
				</p>
			</div>
		</div>
		<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon">
					<i class="fa fa-building-o"></i>
				</div>
				<div class="count"><?php echo $active_partners; ?></div>
				<h3>Partners</h3>
				<p>
					<span class="count_bottom"><i class="red"><i
							class="fa fa-sort-desc"></i><?php echo $deactive_partners; ?></i>
						Deactive Partners</span>
				</p>
			</div>
		</div>
		<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="tile-stats">
				<div class="icon">
					<i class="fa fa-globe"></i>
				</div>
				<div class="count"><?php echo $active_city; ?></div>
				<h3>Cities</h3>
				<p>
					<span class="count_bottom"><i class="red"><i
							class="fa fa-sort-desc"></i><?php echo $deactive_city; ?></i>
						Deactive Cities</span>
				</p>
			</div>
		</div>
	</div>
	<div class="dynamicTile">
		<div class="">
			<!-- start of weather widget -->
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Trip Details<small>
								<!-- trip charter -->
							</small>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tile-stats ">
						<div class="row ">
							<div class="col-sm-12 green">
								<div class="">
									<div class="icon">
										<i class="fa fa-road"></i>
									</div>
									<div class="count"><?php echo $total_trip; ?></div>
									<h3>Total Trips</h3>
									<p></p>
								</div>
							</div>
						</div>
						<div class="clearfix "></div>
						<div class="row weather-days ">
							<div class="col-sm-4 ">
								<div class="daily-weather">
									<h2 class="day">UnAssigned</h2>
									<h3 class="text-center"><?php echo $total_unassigned_trip; ?></h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="daily-weather">
									<h2 class="day">Upcoming</h2>
									<h3 class="text-center"><?php echo $total_upcoming_trip; ?></h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="daily-weather">
									<h2 class="day">Ongoing</h2>
									<h3 class="text-center"><?php echo $total_ongoing_trip; ?></h3>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="daily-weather">
									<h2 class="day red">Cancelled</h2>
									<h3 class="text-center red">
										<span class="red"><?php echo $total_cancelled_trip; ?></span>
									</h3>
								</div>
							</div>
							<div class="col-sm-4 ">
								<div class="daily-weather">
									<h2 class="day green">Completed</h2>
									<h3 class="text-center ">
										<span class="green"><?php echo $total_completed_trip; ?></span>
									</h3>
								</div>
							</div>
							<!--<div class="col-sm-4">
								<div class="daily-weather">
									<h2 class="day">Avg Trip</h2>
									<h3 class="text-center"><?php echo $total_trip; ?></h3>
								</div>
							</div>-->
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

			</div>
			<!-- end of weather widget -->
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Collection Details<small>
								<!-- cash basis -->
							</small>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tile-stats">
						<div class="row">
							<div class="col-sm-12">
								<div class="">
									<div class="icon">
										<i class="fa fa-rupee"></i>
									</div>
									<div class="count green"><?php echo $total_collection; ?></div>
									<h3>Total Amount</h3>
									<p></p>
								</div>
							</div>
						</div>
						<div class="clearfix "></div>
						<div class="row weather-days ">
							<div class="col-sm-3 ">
								<div class="daily-weather">
									<h2 class="day">Cash</h2>
									<h3 class="text-center"><?php echo $total_cash_collection; ?></h3>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="daily-weather">
									<h2 class="day">Wallet</h2>
									<h3 class="text-center"><?php echo $total_wallet_collection; ?></h3>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="daily-weather">
									<h2 class="day">PayTM</h2>
									<h3 class="text-center"><?php echo $total_paytm_collection; ?></h3>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="daily-weather">
									<h2 class="day">Invoice</h2>
									<h3 class="text-center"><?php echo $total_invoice_collection; ?></h3>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="daily-weather">
									<h2 class="day">Tax</h2>
									<h3 class="text-center"><?php echo $total_tax_collection; ?></h3>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="daily-weather">
									<h2 class="day">Discount</h2>
									<h3 class="text-center"><?php echo $total_discount_offered; ?></h3>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="daily-weather">
									<h2 class="day">Commission</h2>
									<h3 class="text-center"><?php echo $total_admin_collection; ?></h3>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="daily-weather">
									<h2 class="day">Driver Earning</h2>
									<h3 class="text-center"><?php echo $total_diver_earning; ?></h3>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

			</div>
			<!-- end of weather widget -->
		</div>
	</div>
	<div class="tile_count">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2> 
						Performance Metrics<!-- <small>given quantity based on observed data</small> -->
					</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="row tile_count">
						<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
							<span class="count_top"><i class="fa fa-user"></i> Average Driver
								Trip</span>
							<div class="count green"><?php echo $average_driver_trip; ?></div>
							<!-- <span class="count_bottom"><i class="green">4% </i> From last Week</span> -->
						</div>
						<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
							<span class="count_top"><i class="fa fa-rupee"></i> Average
								Driver Revenue</span>
							<div class="count green"><?php echo $average_driver_revenue; ?></div>
							<!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span> -->
						</div>
						<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
							<span class="count_top"><i class="fa fa-rupee"></i> Average Driver
								Earning</span>
							<div class="count green"><?php echo $average_driver_earning; ?></div>
							<!--  <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
						</div>
						<div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
							<span class="count_top"><i class="fa fa-rupee"></i> Average
								Passenger Revenue</span>
							<div class="count green"><?php echo $average_passenger_revenue; ?></div>
							<!-- <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span> -->
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="">
			<div class="col-md-12 col-sm-6 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>
							Quick Creator <small>Click quick links</small>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="x_content">
							<a href="<?php echo base_url('user/add')?>"><button type="button"
									id="add-user" class="btn btn-default">New Zuver Employee</button></a>

							<a href="<?php echo base_url('corporatepartners/add')?>"><button
									type="button" id="add-partner" class="btn btn-default">Create
									Partner</button></a> <a
								href="<?php echo base_url('driver/add')?>"><button type="button"
									id="add-driver" class="btn btn-default">Create Driver</button></a>

							<a href="<?php echo base_url('tariff/add')?>"><button
									type="button" id="add-tariff" class="btn btn-default">New
									Pricing</button></a> <a
								href="<?php echo base_url('promocode/add')?>"><button
									type="button" id="add-promocode" class="btn btn-default">Offers</button></a>

							<!-- <button type="button" id="add-trip" class="btn btn-default">Invoice / Billings</button> -->

						</div>
					</div>
				</div>
			</div>
		</div>


	</div>
	<!-- /page content -->

	<div class="">
		
		<!--  <div class="col-md-9 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div id="chartContainer" style="height: 300px; width: 100%;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div>
            <div class="x_title">
                <h2>Zuver Supports</h2>                         
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 profile_details">
                <div class="well profile_view">
                    <div class="col-sm-12">
                        <h4 class="brief"><i>Digital Strategist</i></h4>
                        <div class="left col-xs-9">
                            <h2>Nicole Pearson</h2>
                            <p><strong>About: </strong>Tech Support</p>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-building"></i> Address: </li>
                                <li><i class="fa fa-phone"></i> Phone #: </li>
                            </ul>
                        </div>
                        <div class="right col-xs-3 text-center">
                            <img src="images/user.png" alt="" class="img-circle img-responsive">
                        </div>
                    </div>
                    <div class="col-xs-12 bottom text-center">

                        <div class="col-xs-12 col-sm-12 emphasis">
                            <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                                </i> <i class="fa fa-comments-o"></i> </button>
                            <button type="button" class="btn btn-primary btn-xs">
                                <i class="fa fa-user"> </i> View Profile
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="x_panel tile fixed_height_320 overflow_hidden">
            <div class="x_title">
                <h2>Device Usage</h2>                  
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="" style="width:100%">
                    <tr>
                        <th style="width:37%;">
                            <p></p>
                        </th>
                        <th>
                            <div class="col-lg-5 col-md-7 col-sm-7 col-xs-7">
                                <p class="">Device</p>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <p class="">Progress</p>
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <canvas id="canvas1" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                        </td>
                        <td>
                            <table class="tile_info">

                                <tr>
                                    <td>
                                        <p><i class="fa fa-square aero"></i>App Booking</p>
                                    </td>
                                    <td>50%</td>
                                </tr>
                                <tr>
                                    <td>
                                        <p><i class="fa fa-square purple"></i>Online Booking </p>
                                    </td>
                                    <td>20%</td>
                                </tr>

                                <tr>
                                    <td>
                                        <p><i class="fa fa-square red"></i>Call Booking </p>
                                    </td>
                                    <td>30%</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>-->
		<!--<div class="col-md-4 col-sm-6 col-xs-12">
<div class="x_panel fixed_height_320">
<div class="x_title">
<h2>Driver Usage <small>Sessions</small></h2>                    
<div class="clearfix"></div>
</div>
<div class="x_content">
<div class="dashboard-widget-content">
 <ul class="quick-list">                       
   <li><i class="fa fa-thumbs-up"></i><a href="#">Favorites</a></li>
   <li><i class="fa fa-calendar-o"></i><a href="#">Activities</a></li>                       
 </ul>

 <div class="sidebar-widget">
   <h4>Usage Meter</h4>
   <canvas width="150" height="80" id="foo" class="" style="width: 160px; height: 100px;"></canvas>
   <div class="goal-wrapper">
     <span id="gauge-text" class="gauge-value pull-left">0</span>
     <span class="gauge-value pull-left">%</span>
     <span id="goal-text" class="goal-value pull-right">100%</span>
   </div>
 </div>
</div>
</div>
</div>
</div>-->
		<!--  <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="x_panel fixed_height_320">
            <div class="x_title">
                <h2>Invoice Chart <small>Sessions</small></h2>                    
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <canvas id="pieChart"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="x_panel fixed_height_320">
            <div class="x_title ">
                <h2>Booking Range <small>Peak booking hour</small></h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">
                    <div class="col-md-10" style="overflow:hidden;">
                        <span class="sparkline_one" style="height: 160px; padding: 10px 25px;">
                            <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                        </span>
                        <h6 style="margin:18px">Average Peak Booking Hour</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
	</div>