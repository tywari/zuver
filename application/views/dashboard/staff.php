<div class="">
    <div class="top_tiles">
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                <div class="count"><?php echo $new_bookings ;?> </div>
                <h3>New Bookings</h3>
                
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-thumbs-o-up""></i></div>
                <div class="count"><?php echo $trip_confirmed ;?></div>
                <h3>Trip Confirmed</h3>
               
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-taxi"></i></div>
                <div class="count"><?php echo $on_board ;?></div>
                <h3>Onboard Trip</h3>
                
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-thumbs-o-down"></i></div>
                <div class="count"><?php echo $trip_canceled ;?></div>
                <h3>Trip Cancelled</h3>
              
            </div>
        </div>
    </div>

    <div class="">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div id="trip-details-information" class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
        <div class="x_title">
            <h2>
                Trip Details
            </h2>
            <br><br>
                <ul class="nav nav-tabs" >
                    <li class="active" id='upcoming_tab'>
                        <a href="javascript:void(0);" onclick="setTripStatusFilter('upcoming');">Up coming</a>
                    </li>
                    <li id='unassigned_tab'>
                        <a href="javascript:void(0);" onclick="setTripStatusFilter('unassigned');">Un Assigned</a>
                    </li>
                    <li id='inpogress_tab'>
                        <a href="javascript:void(0);"onclick="setTripStatusFilter('inprogress');" >In Progress</a>
                    </li>
                </ul>
            <!--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown"><a href="#" class="dropdown-toggle"
                            data-toggle="dropdown" role="button" aria-expanded="false"><i
                                    class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                            </ul></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li> 
                    
            </ul>
            <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group">
        <input type="text" id="search_driver_content" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-default" id="search_driver_btn" type="button">Go!</button>
        </span>
        
      </div>
      <div><span>Search by Referral Code, Driver Full Name, Email and Mobile</span></div>
    </div>
 </div>-->
            <div class="title_right pull-right">

                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 hidden">
                    <a href="<?php echo base_url('trip/add') ?>"><button id="add_driver_btn"
                                                                        class="btn btn-primary">Add Trip Details</button></a>

                </div>

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div id="loading" style="padding-left:40%;display:none;"><img src="../public/images/loader.gif"></div>
            <table id="datatable-checkbox"
                   class="table table-striped table-bordered bulk_action" >
                <thead>
                    <tr>
                        <th><!-- <input type="checkbox" id="check-all" class="flat"> --></th>
                        <th>Sl no</th>
                        <th>Passenger</th>
                        <th>Driver</th>
                        <th>bookingKey</th>
                        <th>Company</th>
                        <th>City</th>
                        <th>pickupLocation</th>
                        <th>dropLocation</th>
                        <th>Trip Status</th>
                        <th>Action</th>
                    </tr>
                </thead>


                <tbody>
                    <?php
                    //sl no intialization
                    $i = 1;
                    /* $driver_model_list=array(
                      0=>array('firstName'=>'Kiran','lastName'=>'Kumar','driverCode'=>'Kiran001','email'=>'global@gmail.com','mobile'=>'9901206631','status'=>1,'skills'=>'Automatic','driverLanguagesKnown'=>'English, Kannada','driverExperience'=>'3 Years','isVerified'=>1,'companyId'=>'ZOOMCAR','doj'=>'20-09-2016','totalTripComplete'=>12,'rating'=>4.5),
                      1=>array('firstName'=>'Kiran','lastName'=>'Kumar','driverCode'=>'Kiran001','email'=>'global@gmail.com','mobile'=>'9901206631','status'=>1,'skills'=>'Manual','driverLanguagesKnown'=>'English, Kannada','driverExperience'=>'5 Years','isVerified'=>0,'companyId'=>'Zuver','doj'=>'20-09-2016','totalTripComplete'=>12,'rating'=>3.8)
                      ); */
                    foreach ($trip_details_model_list as $list) {

                        echo '<tr>';

                        echo '<td></td>';//<input type="checkbox" class="flat" name="table_records">
                        echo '<th>' . $i . '</th>';
                        echo '<td>' . $list->passengerFirstName .' '.$list->passengerLastName. '</td>';
                        echo '<td> ' . $list->driverFirstName .' '.$list->driverLastName . '</td>';
                        echo '<td>' . $list->bookingKey . '</td>';
                        echo '<td>' . $list->company . '</td>';
                        echo '<td>' . $list->city . '</td>';
                        echo '<td>' . $list->pickupLocation . '</td>';
                        echo '<td>' . $list->dropLocation . '</td>';
                        //echo '<td> <ul class="list-inline"> <li>';
                        //echo '<div class="btn-group btn-toggle">';
                        //echo '<button class="btn btn-xs btn-primary active">Verified</button>';
                        //echo '<button class="btn btn-xs btn-default">Not Verified</button>';
                        //echo '</div></li></ul></td>';
                        //echo '<td>'.($list['isVerified'])?'Verified':'Not Verified'.'</td>';
                        echo '<td>' . $list->tripStatus . '</td>';
                     if($this->session->userdata('role_type') !='CO') : ;       
                        echo '<td>
                            <a href="javascript:void(0);" id="editTrip-' . $list->id . '" onclick="editTrip(this.id);" class="userlink">'
                        . '<span class="glyphicon glyphicon-edit"></span></a>'
                        . '</td>';
                     endif;   
                        echo '</tr>';
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
            <input type='hidden' name='trip_status_filter' id='trip_status_filter' value='upcoming'/>
        </div>
    </div>
            </div>         
                
        </div>



        <div class="col-md-4 hidden">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-bell"></i> Notifications <small>Transaction Summary</small></h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content bs-example-popovers">

                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>ZU0023</strong> Trip Confirmed Coimbatore to Bangalore
                    </div>
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>ZU0025</strong> New Booking Jpnagar to malleswaram
                    </div>
                    <div class="alert alert-warning alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>ZU0020</strong> Triped completed
                    </div>
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>ZU0021</strong> Trip cancelled
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12 " style="height:200px;">
            
        </div>
    </div>


</div>