
<div class="">
    <div class="tile_count">
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-road"></i> No of Bookings</span>
            <div class="count"><?php echo $no_of_bookings; ?></div>
            <span class="count_bottom"><i class="green"><?php echo $today_bookings; ?></i>
                Booking Today</span>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-building-o"></i> Trips
                Completed</span>
            <div class="count"><?php echo $trips_completed; ?></div>
            <span class="count_bottom"> <i class="red"><i class="fa fa-sort-desc"></i> <?php echo $trips_completed_today; ?></i>
                Trips Completed Today
            </span>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa fa-rupee"></i> Trips Invoice
                Amount</span>
            <div class="count green">00</div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>10,000</i>
                Pending Payment</span>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa fa-rupee"></i> Last Payment
                Made</span>
            <div class="count">00</div>
            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12</i>
                Oct 2016</span>
        </div>
    </div>
        
        <div class="">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title hidden">
                        <h2>
                            <i class="fa fa-align-left"></i> Quick Bookings <small>Sessions</small><small>
                                <a class="btn btn-info" id="compose" href="javascript:void(0);">Book
                                    a Trip</a>
                            </small>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <!-- start accordion -->
                        <div class="accordion" id="accordion1" role="tablist"
                             aria-multiselectable="true">
                            <div class="panel ">
                               <!--  <a class="panel-heading" role="tab" id="headingOne1"
                                   data-toggle="collapse" data-parent="#accordion1"
                                   href="#collapseOne1" aria-expanded="true"
                                   aria-controls="collapseOne">
                                    <h4 class="panel-title">Bookings History</h4>
                                </a> -->
                                <div id="trip-details-information"
                                     class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Trip Details</h2>
                                            <br>
                                            <br>
                                            <ul class="nav nav-tabs">
                                                <li class="active" id='upcoming_tab'><a
                                                        href="javascript:void(0);"
                                                        onclick="setTripStatusFilter('upcoming');">Up coming</a></li>
                                                <li id='unassigned_tab'><a href="javascript:void(0);"
                                                                           onclick="setTripStatusFilter('unassigned');">Un Assigned</a>
                                                </li>
                                                <li id='inpogress_tab'><a href="javascript:void(0);"
                                                                          onclick="setTripStatusFilter('inprogress');">In Progress</a>
                                                </li>
                                            </ul>
                                            <!--<ul class="nav navbar-right panel_toolbox">
<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
<li class="dropdown"><a href="#" class="dropdown-toggle"
data-toggle="dropdown" role="button" aria-expanded="false"><i
class="fa fa-wrench"></i></a>
<ul class="dropdown-menu" role="menu">
<li><a href="#">Settings 1</a></li>
<li><a href="#">Settings 2</a></li>
</ul></li>
<li><a class="close-link"><i class="fa fa-close"></i></a></li> 

</ul>
<div class="title_right">
<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
<div class="input-group">
<input type="text" id="search_driver_content" class="form-control" placeholder="Search for...">
<span class="input-group-btn">
<button class="btn btn-default" id="search_driver_btn" type="button">Go!</button>
</span>

</div>
<div><span>Search by Referral Code, Driver Full Name, Email and Mobile</span></div>
</div>
</div>-->
                                            <div class="title_right pull-right">

                                                <div
                                                    class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 hidden">
                                                    <a href="<?php echo base_url('trip/add') ?>"><button
                                                            id="add_driver_btn" class="btn btn-primary">Add Trip
                                                            Details</button></a>

                                                </div>

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div id="loading" style="padding-left: 40%; display: none;">
                                                <img src="../public/images/loader.gif">
                                            </div>
                                            <table id="datatable-checkbox"
                                                   class="table table-striped table-bordered bulk_action">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                                <!-- <input type="checkbox" id="check-all" class="flat"> -->
                                                        </th>
                                                        <th>Sl no</th>
                                                        <th>Passenger</th>
                                                        <th>Driver</th>
                                                        <th>bookingKey</th>
                                                        <th>Company</th>
                                                        <th>City</th>
                                                        <th>pickupLocation</th>
                                                        <th>dropLocation</th>
                                                        <th>Trip Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>


                                                <tbody>
                                                    <?php
                                                    // sl no intialization
                                                    $i = 1;
                                                    /*
                                                     * $driver_model_list=array(
                                                     * 0=>array('firstName'=>'Kiran','lastName'=>'Kumar','driverCode'=>'Kiran001','email'=>'global@gmail.com','mobile'=>'9901206631','status'=>1,'skills'=>'Automatic','driverLanguagesKnown'=>'English, Kannada','driverExperience'=>'3 Years','isVerified'=>1,'companyId'=>'ZOOMCAR','doj'=>'20-09-2016','totalTripComplete'=>12,'rating'=>4.5),
                                                     * 1=>array('firstName'=>'Kiran','lastName'=>'Kumar','driverCode'=>'Kiran001','email'=>'global@gmail.com','mobile'=>'9901206631','status'=>1,'skills'=>'Manual','driverLanguagesKnown'=>'English, Kannada','driverExperience'=>'5 Years','isVerified'=>0,'companyId'=>'Zuver','doj'=>'20-09-2016','totalTripComplete'=>12,'rating'=>3.8)
                                                     * );
                                                     */
                                                    foreach ($trip_details_model_list as $list) {

                                                        echo '<tr>';

                                                        echo '<td></td>'; // <input type="checkbox" class="flat" name="table_records">
                                                        echo '<th>' . $i . '</th>';
                                                        echo '<td>' . $list->passengerFirstName . ' ' . $list->passengerLastName . '</td>';
                                                        echo '<td> ' . $list->driverFirstName . ' ' . $list->driverLastName . '</td>';
                                                        echo '<td>' . $list->bookingKey . '</td>';
                                                        echo '<td>' . $list->company . '</td>';
                                                        echo '<td>' . $list->city . '</td>';
                                                        echo '<td>' . $list->pickupLocation . '</td>';
                                                        echo '<td>' . $list->dropLocation . '</td>';
                                                        // echo '<td> <ul class="list-inline"> <li>';
                                                        // echo '<div class="btn-group btn-toggle">';
                                                        // echo '<button class="btn btn-xs btn-primary active">Verified</button>';
                                                        // echo '<button class="btn btn-xs btn-default">Not Verified</button>';
                                                        // echo '</div></li></ul></td>';
                                                        // echo '<td>'.($list['isVerified'])?'Verified':'Not Verified'.'</td>';
                                                        echo '<td>' . $list->tripStatus . '</td>';
                                                       echo '<td>
                            <a href="javascript:void(0);" id="editTrip-' . $list->id . '" onclick="editTrip(this.id);" class="userlink">' . '<span class="glyphicon glyphicon-edit"></span></a>' . '</td>';

                                                                                                                echo '</tr>';
                                                       $i ++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                            <input type='hidden' name='trip_status_filter'
                                                   id='trip_status_filter' value='unassigned' />
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- end of accordion -->


                    </div>
                </div>
            </div>
        </div>

<div class="">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        <i class="fa fa-money" aria-hidden="true"></i> Zuver Banking Details <small></small>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-8 product_price">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Holder Name:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <label class="control-label"><?php echo $bank_details_model->get('accountHolderName'); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Number:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <label class="control-label"><?php echo $bank_details_model->get('accountNumber'); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">IFSC Code:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <label class="control-label"><?php echo $bank_details_model->get('ifscCode'); ?></label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Name:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <label class="control-label"><?php echo $bank_details_model->get('bankName'); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Branch:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <label class="control-label"><?php echo $bank_details_model->get('bankBranch'); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Address:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <span><label class="control-label"><?php echo $bank_details_model->get('bankAddress'); ?></label></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Code:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <label class="control-label"><?php echo $bank_details_model->get('bankCode'); ?></label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Routing Code:</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <label class="control-label"><?php echo $bank_details_model->get('bankRoutingCode'); ?></label>

                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <br>
                </div>
                <div class="col-md-4 product_price">
                    <span id="cName"><h4>
                            <i class="fa fa-shield" aria-hidden="true"></i> Cancelled Cheque
                        </h4></span>
                    <div class="form-group">
                        <img src="<?php echo bank_content_url($bank_details_model->get('id') . '/' . $bank_details_model->get('chequeImage')); ?>">

                    </div>
                    <!-- /.col -->
                </div>
            </div>


        </div>
    </div>
    
    
</div>


UPDATE `tripdetails` SET `tripStatus`='TCT',`notificationStatus`='TCT'  WHERE id in (13677,13613,13603,13474,13471,13238,13052,12855,12767,12760,12650,11499,11444,11303,11298,10814,9224,8673,8544,
8350,7736,7183,6601,2327,629,9651,8946,9165,9164,7805,8500,8276,7653,6121)

