<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Zuver Login | </title>

    <!-- Bootstrap -->
    <link href="<?php echo js_url('vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo js_url('vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo js_url('vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo js_url('vendors/animate.css/animate.min.css');?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo css_url('app/custom.min.css'); ?>" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
           <?php
                            $form_attr = array (
                                            'name' => 'user_login_from',
                                            'id' => 'user_login_from',
                                            'method' => 'POST',
                                            'data-parsley-validate' => '',
                                            'class' => 'form-horizontal form-label-left' 
                            );
                            echo form_open ( base_url ( 'login/userLogin' ), $form_attr );
                        
                        ?>
              <h1>Login panel </h1>
              <span class="red"><h4><?php echo $this->session->flashdata('login_err'); ?></h4></span>
              <div>
              <?php
						
						
							echo form_input ( array (
									'id' => 'username',
									'name' => 'username',
									'class' => 'form-control',
		                            'placeholder'=>'Username/Email',
									'required' => 'required',
									'value' => '' 
							) );
						
						?>
                
              </div>
              <div>
              <?php
						
						
							echo form_password ( array (
									'id' => 'password',
									'name' => 'password',
									'class' => 'form-control',
		                            'placeholder'=>'Password',
									'required' => 'required',
									'value' => '',
									'onpaste' =>'return false;'
							) );
						
						?>
                
              </div>
              <div>
                <!--<a class="btn btn-default submit" href="index.html">Log in</a>-->   
                                <button type="submit" id="user_login" class="btn btn-default submit" >Log in</button>           
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">
                  <a href="#signup" class="to_register"> Reset your password</a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="<?php echo image_url('app/loginZuver.png'); ?>" /></h1>
                  <!-- <h5>Your Driver On Demand</h5> -->
                  <p>&#169;2016 All Rights Reserved.</p>
                </div>
              </div>
           <?php
			echo form_close ();
			?>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <?php
                            $form_attr = array (
                                            'name' => 'user_reset_from',
                                            'id' => 'user_reset_from',
                                            'method' => 'POST',
                                            'data-parsley-validate' => '',
                                            'class' => 'form-horizontal form-label-left' 
                            );
                            echo form_open ( base_url ( 'login/resetPassword' ), $form_attr );
                        
                        ?>
              <h1>Reset Password</h1>
              <span class="red"><h4><?php echo $this->session->flashdata('login_err'); ?></h4></span>
              <div>
              <?php
						
						
							echo form_input ( array (
									'id' => 'username',
									'name' => 'username',
									'class' => 'form-control',
		                            'placeholder'=>'Email',
									'required' => 'required',
									'value' => '' 
							) );
						
						?>
                
              </div>
              <div class="hidden">
               <!-- <input type="email" class="form-control" placeholder="Email" required="" /> --> 
              </div>             
              <div>
               <!--  <a class="btn btn-default submit" href="index.html">Submit</a> -->
                <button type="submit" id="reset_password" class="btn btn-default submit" >Submit</button> 
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="<?php echo image_url('app/loginZuver.png'); ?>" /></h1>
                  <!-- <h5>Your Driver On Demand</h5> -->
                  <p>&#169;2016 All Rights Reserved.</p>
                </div>
              </div>
             <?php
			echo form_close ();
			?>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
