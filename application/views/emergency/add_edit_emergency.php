<?php
$view_mode = $mode;
?>
<div id="emergency-details-information"
	class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Edit Emergency</h2>
			<ul class="nav navbar-right panel_toolbox">
				<!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br />
			<?php
			$form_attr = array (
					'name' => 'edit_emergency_form',
					'id' => 'edit_emergency_form',
					'method' => 'POST',
					'data-parsley-validate' => '',
					'class' => 'form-horizontal form-label-left' 
			);
			echo form_open ( base_url ( '' ), $form_attr );
			
			// passenger id by default is -1
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'emergency_id',
					'name' => 'id',
					'value' => ($emergency_model->get ( 'id' )) ? $emergency_model->get ( 'id' ):-1 
			) );
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'passengerId',
					'name' => 'passengerId',
					'value' => ($emergency_model->get ( 'passengerId' )) ? $emergency_model->get ( 'passengerId' ):-1
			) );
			if(count($passenger_emergency_contacts)>0)
			{
				echo '<div class="form-group"><div class="x_title"><h4><strong>Emergency Contact Details</strong></h4><div class="clearfix"></div></div>';
				echo '<div class="panel"><div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body">';
				echo '<table class="table table-bordered">';
				echo '<thead><tr><th>Contact Person</th><th>Contact Number</th><th>Email-Id</th></tr></thead>';
				echo '<tbody>';
				foreach ($passenger_emergency_contacts as $list)
				{
					echo '<tr>';
					echo '<td>'.$list->name.'</td>';
					echo '<td>'.$list->mobile.'</td>';
					echo '<td>'.$list->email.'</td>';
					echo '</tr>';
				}
				echo '</tbody>';
				echo '</table>';
				echo '</div></div></div></div>';
			}
			?>
			<div class="form-group">
					<?php
					echo form_label ( 'Passenger Name:', 'passengerFullName', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						/* if ($view_mode == EDIT_MODE) {
							echo form_input( array (
									'id' => 'passengerId',
									'name' => 'passengerId',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'row' => '3',
									'value' => ($emergency_model->get ( 'passengerId' )) ? $emergency_model->get ( 'passengerId' ) : '' 
							) );
						} else {
							echo text ( $emergency_model->get ( 'passengerId' ) );
						} */
						
						echo text ($passenger_model->get ( 'firstName' ).' '.$passenger_model->get ( 'lastName' ));
						?>
					</div>

			</div>
				<div class="form-group">
					<?php
					echo form_label ( 'Location:', 'location', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						/* if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'location',
									'name' => 'location',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'maxlength' => 10,
									'value' => ($emergency_model->get ( 'location' )) ? $emergency_model->get ( 'location' ) : '' 
							) );
						} else {
							echo text ( $emergency_model->get ( 'location' ) );
						} */
						
						echo form_textarea( array (
								'id' => 'location',
								'name' => 'location',
								'class' => 'form-control',
								'required' => 'required',
								'maxlength' => 10,
								'rows'=>4,
								'readonly'=>'readonly',
								'value' => ($emergency_model->get ( 'location' )) ? $emergency_model->get ( 'location' ) : ''
						) );
						?>
					</div>

			</div>
			
			
			<div class="form-group">
					<?php
					echo form_label ( 'Emergency Staus:', 'emergencyStaus', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_dropdown ( 'emergencyStatus', $emergency_status_list, $emergency_model->get ( 'emergencyStatus' ),'id ="emergencyStaus" class ="form-control" required');
						} else {
							echo text ( $emergency_status_list[$emergency_model->get ( 'emergencyStatus' )]);
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'Comments:', 'comments', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						/* if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'location',
									'name' => 'location',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'maxlength' => 10,
									'value' => ($emergency_model->get ( 'location' )) ? $emergency_model->get ( 'location' ) : '' 
							) );
						} else {
							echo text ( $emergency_model->get ( 'location' ) );
						} */
						
						echo form_textarea( array (
								'id' => 'comments',
								'name' => 'comments',
								'class' => 'form-control',
								//'required' => 'required',
								'maxlength' => 10,
								'rows'=>4,
								
								'value' => ($emergency_model->get ( 'comments' )) ? $emergency_model->get ( 'comments' ) : ''
						) );
						?>
					</div>

			</div>
			<div class="ln_solid"></div>
			<div <?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<button type="button" id="load_emergency_btn"
						class="btn btn-primary">Cancel</button>
					<button type="button" id="save_emergency_btn"
						class="btn btn-success" onclick="saveEmergency();">Save</button>
				</div>
			</div>
                     <?php  if($view_mode == VIEW_MODE) : ?>
                        <div  class="form-group">
                            <center><button type="button" id="load_emergency_btn" class="btn btn-primary" >Back</button></center>
                        </div>  
                    <?php endif ; ?>       
			<?php
			echo form_close ();
			?>
		</div>
	</div>
</div>