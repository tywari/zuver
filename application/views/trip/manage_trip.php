<div id="trip-details-information" class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>
                B <i>2</i> B Booking Trip Details
            </h2>
            <br><br>
            <ul class="nav nav-tabs">
                <li id='freshTrip_tab' class="active">
                    <a onclick="setTripStatusFilter('freshTrip');">Fresh Trip</a>
                </li>
                <li id='unassigned_tab'>
                    <a onclick="setTripStatusFilter('unassigned');">Un Assigned</a>
                </li>
                <li id='assigned_tab'>
                    <a onclick="setTripStatusFilter('assigned');">Assigned Trip</a>
                </li>
                <li id='inpogress_tab'>
                    <a onclick="setTripStatusFilter('inprogress');">In Progress</a>
                </li>
                <li id='completed_tab'>
                    <a onclick="setTripStatusFilter('completed');">Completed Trip</a>
                </li>
                <li id='cancelled_tab'>
                    <a onclick="setTripStatusFilter('cancelled');">Cancelled Trip</a>
                </li>
            </ul>

            <div class="title_right pull-right">

                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 hidden">
                    <a href="<?php echo base_url('trip/add') ?>">
                        <button id="add_driver_btn"
                                class="btn btn-primary">Add Trip Details
                        </button>
                    </a>

                </div>

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div id="loading" style="padding-left:40%;display:none;"><img src="../public/images/loader.gif"></div>
            <table id="datatable-checkbox" class="table table-striped table-bordered datatable-button-init-collection">
                <thead>
                <tr>
                    <th><!-- <input type="checkbox" id="check-all" class="flat"> --></th>
                    <!-- <th>Sl no</th> -->
                    <?php if ($this->User_Access_Model->getAccessLevelForTripEdit() == User_Access_Level_Enum::EDIT) : ?>
                        <th>Action</th>
                    <?php endif; ?>
                    <th>Booking Key</th>
                    <th>User Name (Mobile)</th>
                    <th>Client Name</th>
                    <th>Driver Name (Mobile)</th>
                    <th>Pickup Location</th>
                    <th>Pickup Datetime</th>
                    <th>Drop Location</th>
                    <th>Created Datetime</th>
                    <th>Trip Type</th>
                    <th>Payment Mode</th>
                    <th>Transmission type</th>
                    <th>Trip Status</th>
                    <th>City</th>
                    <th>Entity</th>
                    <th>Bill Type</th>
                    <th>Vechile Number</th>
                    <th>Vechile Make</th>
                    <th>Vechile Model</th>

                </tr>
                </thead>


                <tbody>
                <?php

                foreach ($trip_details_model_list as $list) {

                    echo '<tr>';

                    if ($list->pickupDatetime >= date("Y-m-d", getCurrentUnixDate())):
                        echo '<td class="' . $list->tripStatusEnum . '"></td>';//<input type="checkbox" class="flat" name="table_records">
                    else:
                        echo '<td class="EXP"></td>';
                    endif;

                    if ($this->User_Access_Model->getAccessLevelForTripEdit() == 2) :
                        echo '<td>
                            <a id="editTrip-' . $list->id . '" onclick="editTrip(this.id);" class="userlink">'
                            . '<span class="glyphicon glyphicon-edit"></span></a>'
                            . '</td>';
                    endif;
                    echo '<td>' . $list->id . '</td>';
                    $user_mobile = ($list->userMobile) ? '(' . $list->userMobile . ')' : '';
                    echo '<td>' . $list->userFirstName . ' ' . $list->userLastName . ' ' . $user_mobile . '</td>';
                    $driver_mobile = ($list->driverMobile) ? '(' . $list->driverMobile . ')' : '';
                    echo '<td>' . $list->clientName . '</td>';
                    echo '<td> ' . $list->driverFirstName . ' ' . $list->driverLastName . ' ' . $driver_mobile . '</td>';
                    echo '<td>' . $list->pickupLocation . '</td>';
                    echo '<td>' . $list->pickupDatetime . '</td>';
                    echo '<td>' . $list->dropLocation . '</td>';
                    echo '<td>' . $list->createdDate . '</td>';
                    echo '<td>' . $list->tripType . '</td>';
                    echo '<td>' . $list->paymentMode . '</td>';
                    echo '<td>' . $list->transmissionType . '</td>';
                    echo '<td>' . $list->tripStatus . '</td>';
                    echo '<td>' . $list->city . '</td>';
                    echo '<td>' . $list->company . '</td>';
                    echo '<td>' . $list->billType . '</td>';
                    echo '<td>' . $list->vehicleNo . '</td>';
                    echo '<td>' . $list->vehicleMake . '</td>';
                    echo '<td>' . $list->vehicleModel . '</td>';

                    echo '</tr>';

                   }
                ?>
                </tbody>
            </table>
            <input type='hidden' name='trip_status_filter' id='trip_status_filter' value='freshTrip'/>
        </div>
    </div>
</div>

