<?php
$view_mode = $mode;
?>

<style>
    .assigndriver{width:1000px}
</style>
<div id="trip-details-information" class="col-md-12 col-sm-12 col-xs-12">

    <div id="style-4" class="col-md-12 TripPlannerScroll scrollbar">
        <div class="x_panel">
            <div class="x_title">
                <div class="col-sm-4 invoice-col">
                <?php if ($view_mode == EDIT_MODE) { ?>
                    <h2>Trip Planner <small >Booked at :
                            <span id="bookingDetail"> 
                            <?php
                                $booked_date = ($trip_model->get('createdDate')) ? $trip_model->get('createdDate') : '';
                                echo $booked_date;
                            ?>
                           <!--<i class="fa fa-clock-o "></i> 12:00</span>-->
                        </small>
                    </h2>     
                    <?php
                }
                ?>
                </div>
                 <div class="col-sm-2 invoice-col">
                    <a href="javascript:void(0);" class="btn btn-default" id="driver_rejected" ><i class="fa fa-send" ></i> Driver Rejected</a> 
                 </div>
                 <div class="col-sm-2 invoice-col">
                    <a href="javascript:void(0);" class="btn btn-default" id="passenger_rejected" ><i class="fa fa-send" ></i> Cancelled By Passenger</a> 
                 </div>
                <span style='float:right;padding-right:50px;'>
                     <span class="badge">
                    <?php 
                    echo $trip_status[$trip_model->get('tripStatus')] ;?>
                     </span> 
                </span>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


                <br />
                <?php
                $form_attr = array(
                    'name' => 'edit_trip_form',
                    'id' => 'edit_trip_form',
                    'class' => 'form-horizontal form-label-left',
                    'data-parsley-validate' => '',
                    'method' => 'POST'
                );
                echo form_open_multipart(base_url(''), $form_attr);

                // driver id by default is -1
                echo form_input(array(
                    'type' => 'hidden',
                    'id' => 'trip_id',
                    'name' => 'id',
                    'value' => ($trip_model->get('id')) ? $trip_model->get('id') : -1
                ));
                ?> 
                <?php
                if ($view_mode == EDIT_MODE) {
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'bookingtrip_id',
                        'name' => 'bookingtrip_id',
                        'value' => $trip_model->get('id')
                    ));
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'driver_id',
                        'name' => 'driver_id',
                        'value' => ($trip_model->get('driverId')) ? $trip_model->get('driverId') : 0
                    ));
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'latitude',
                        'name' => 'latitude',
                        'value' => $trip_model->get('pickupLatitude')
                    ));
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'longitude',
                        'name' => 'longitude',
                        'value' => $trip_model->get('pickupLongitude')
                    ));
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'tripType',
                        'name' => 'tripType',
                        'value' => $trip_model->get('tripType')
                    ));
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'companyId',
                        'name' => 'companyId',
                        'value' => $trip_model->get('companyId')
                    ));
                    ?>
                    <section class="content invoice">
                        <!-- title row -->
                        <div class="row">                       
                            <div class="col-md-6 product_price">
                                <span id="cName"><h4><i class="fa fa-user"></i> Customer Details</h4></span>
                                <?php if(!empty($passenger_model)) : ?>
                                <small class="badge">C-Id : <span id="cId"><?php echo $passenger_model->get('id'); ?></span></small>
                                <small class="badge">Name : <span id="cId"><?php echo $passenger_model->get('firstName'); ?></span></small>
                                <small class="badge">Mobile : <span id="cMobile"><?php echo $passenger_model->get('mobile'); ?></span></small>
                                <small class="badge"><i class="fa fa-headphones"></i></small>
                             <?php endif ;?>   

                                <br>
                            </div>
                            <div class="col-md-6 product_price">
                                <span id="cName"><h4><i class="fa fa-cab"></i> Driver Details</h4></span>
                                <?php if(!empty($driver_model)) : ?>
                                 <small class="badge">C-Id : <span id="cId"><?php echo $driver_model->get('id'); ?></span></small>
                                <small class="badge">Name : <span id="cId"><?php echo $driver_model->get('firstName'); ?></span></small>
                                <small class="badge">Mobile : <span id="cMobile"><?php echo $driver_model->get('mobile'); ?></span></small>
                                <?php endif ;?>
<!--                                <a href="#"  class="pull-right" type="button"">
                                    <span class="badge alert-success">
                                        <i class="fa fa-send"></i> Assign A driver</span>
                                </a>					  -->
                                <br>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col" style="line-height:22px">
                                <table><tr><td>
                                            <b>Booking Id :</b> </td>
                                    <td><span id="bookingId"><?php echo $trip_model->get('bookingKey'); ?></span>
                                    </td>
                                    </tr>
                                <tr><td>
                                            <b>Pickup Date Time :</b> </td>
                                        <td> <span class="bookingDate">
                                    <?php 
                if ($trip_model->get('tripStatus') != Trip_Status_Enum::DRIVER_ARRIVED ) {
                                        echo form_input(array(
                                            'id' => 'pickupDatetime',
                                            'name' => 'pickupDatetime',
                                            'class' => 'form-control',
                                            'autocomplete' => 'off',
                                            'required' => 'required',
                                            'style' =>'padding-top: 6px; margin-top: 12px; border-top-width: 1px; width:160px;',
                                            'value' => ($trip_model->get('pickupDatetime')) ? $trip_model->get('pickupDatetime') : ''
                                        ));
                                    } else {
                                        echo text($trip_model->get('pickupDatetime'));
                                        echo form_input(array(
                                                'type' => 'hidden',
                                                'id' => 'pickupDatetime',
                                                'name' => 'pickupDatetime',
                                                'value' => ($trip_model->get('pickupDatetime')) ? $trip_model->get('pickupDatetime') : ''
                                            ));
                                    }
                                    ?>
                                </span>
                                        </td>
                                    </tr>
                                
                                    <tr><td>    
                                            <b>Payment Mode :</b> </td>
                                        <td> <span class="paymentType">
                                    <?php
                                        echo form_dropdown('paymentMode', $payment_mode, $trip_model->get('paymentMode'), array(
                                            'id' => 'paymentMode',
                                            'class' => 'form-control',
                                            'required' => 'required'
                                        ));
                                   
                                    ?>
                                            </span></td></tr>
                                    <tr><td>  
                                            <b>Trip Type :</b> </td>
                                <td><span class="tripType">
                                    <?php 
                                     if ($trip_model->get('tripStatus') != Trip_Status_Enum::IN_PROGRESS )  {
                                        echo form_dropdown('tripType', $trip_type, $trip_model->get('tripType'), array(
                                                                'id' => 'tripType',
                                                                'class' => 'form-control',
                                                                'required' => 'required',
                                                            ));
                                    } else {
                                        echo $trip_type[$trip_model->get('tripType')];
                                        echo form_input(array(
                                                'type' => 'hidden',
                                                'id' => 'tripType',
                                                'name' => 'tripType',
                                                'value' => ($trip_model->get('tripType')) ? $trip_model->get('tripType') : ''
                                            ));
                                    }
                                     
                                    ?>
                                </span>
                                </td></tr>
                                    <tr><td>    
                                            <b>Transmission Type :</b></td>
                                        <td> <span class="transmissionType">
                                    <?php 
                                      if ($trip_model->get('tripStatus') != Trip_Status_Enum::IN_PROGRESS )  {
                                        echo form_dropdown('transmissionType', $transmission_type, $trip_model->get('transmissionType'), array(
                                                                'id' => 'transmissionType',
                                                                'class' => 'form-control',
                                                                'required' => 'required',
                                                            ));
                                    } else {
                                     echo $transmission_type[$trip_model->get('transmissionType')]; 
                                     echo form_input(array(
                                                'type' => 'hidden',
                                                'id' => 'transmissionType',
                                                'name' => 'transmissionType',
                                                'value' => ($trip_model->get('transmissionType')) ? $trip_model->get('transmissionType') : ''
                                            ));
                                    }
                                    ?>
                                            </span></td></tr>
                                    <tr><td>
                                <a href="javascript:void(0);" class="btn btn-primary" id="update_trip_details" >
                                    <i class="fa fa-check"></i> update
                                </a>
                                        </td>
                                    </tr>
                                    </table>
                            </div>

                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <b>Pick Point</b>
                                <address>
                                    <span class="pickupLocation">
                                        <?php
                    if ($trip_model->get('tripStatus') != Trip_Status_Enum::DRIVER_ARRIVED ) {
                                            echo form_input(array(
                                                    'id' => 'pickupLocation',
                                                    'name' => 'pickupLocation',
                                                    'class' => 'form-control',
                                                    'required' => 'required',
                                                    'value' => ($trip_model->get('pickupLocation')) ? $trip_model->get('pickupLocation') : ''
                                                ));
                                            echo form_input(array(
                                                    'type' => 'hidden',
                                                    'id' => 'pickupLatitude',
                                                    'name' => 'pickupLatitude',
                                                    'class' => 'form-control',
                                                    'value' => ($trip_model->get('pickupLatitude')) ? $trip_model->get('pickupLatitude') : ''
                                                ));
                                            echo form_input(array(
                                                    'type' => 'hidden',
                                                    'id' => 'pickupLongitude',
                                                    'name' => 'pickupLongitude',
                                                    'class' => 'form-control',
                                                    'value' => ($trip_model->get('pickupLongitude')) ? $trip_model->get('pickupLongitude') : ''
                                                ));
                                            } else {
                                                echo $trip_model->get('pickupLocation');
                                                 echo form_input(array(
                                                'type' => 'hidden',
                                                'id' => 'pickupLocation',
                                                'name' => 'pickupLocation',
                                                'value' => ($trip_model->get('pickupLocation')) ? $trip_model->get('pickupLocation') : ''
                                            ));
                                         }
                                        ?>
                                    </span> 

                                </address>
                            </div>
                            <div class="col-sm-4 invoice-col">
                                <b>Destination</b>
                                <address>
                                    <span id="dAdd"><?php echo $trip_model->get('dropLocation'); ?></span> 

                                </address>
                            </div>


                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row">
                            <div class="col-md-6 product_price">
                                Function bar
                            </div>
                            <div class="col-md-6  product_price">
                                <div class="bs-example" data-example-id="simple-jumbotron">
                                   
            <iframe id='iframeId' src="<?php echo base_url('trip/loadGmap/'.$trip_model->get('id')) ;?>" width="520px" height="230px" frameBorder="0">Browser not compatible.</iframe>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <!-- this row will not appear when printing -->

                    </section>

                <?php } ?>
                <?php
                echo form_close();
                ?>
            </div>
        </div>
    </div>
    <?php if ($view_mode == EDIT_MODE) { ?>
        <div class="col-md-12">
            <div class="x_panel">                 
                <div class="x_content" style="position">
                    <div class="col-xs-12">
                        <?php
                        // Assign Driver
                        if ($trip_model->get('tripStatus') == Trip_Status_Enum::BOOKED) {
                            ?>
                            <a href="javascript:void(0);" class="btn btn-default" id="assigndriver"><i class="fa fa-send" ></i> Assign Driver</a> 
                        <?php } else { ?>
                            <a href="javascript:void(0);" class="btn btn-default disabled" ><i class="fa fa-send" ></i> Assign Driver</a> 
                        <?php } ?>

                        <?php
                        // Driver Arrived
                        if ($trip_model->get('tripStatus') == Trip_Status_Enum::TRIP_CONFIRMED ) {
                            ?>   
                            <a href="javascript:void(0);" class="btn btn-default"  id="driver_arrived_btn" onclick="driverArrived();">
                                <i class="fa fa-send"  ></i> Driver Arrived</a>
                        <?php } else { ?>
                            <a href="javascript:void(0);" class="btn btn-default disabled" ><i class="fa fa-send" ></i> Driver Arrived</a>  
                        <?php } ?>   

                        <?php
                        // End Trip
                        if ($trip_model->get('tripStatus') == Trip_Status_Enum::IN_PROGRESS) {
                            ?>
                            <a href="javascript:void(0);" class="btn btn-danger pull-right" id="endtrip_popup" style="margin-right: 5px;" >
                                <i class="fa fa-close"></i> End Trip
                            </a>
                        <?php }else{ ?> 
                            <a href="javascript:void(0);" class="btn btn-danger pull-right disabled" style="margin-right: 5px;" >
                                <i class="fa fa-close"></i> End Trip
                            </a>
                        <?php } ?>    
                            
                        <?php
                        // Start Trip
                        if ($trip_model->get('tripStatus') == Trip_Status_Enum::DRIVER_ARRIVED) {
                            ?>  
                            <a href="javascript:void(0);" class="btn btn-primary pull-right " id="start_trip_btn" 
                               style="margin-right: 5px;" onclick="startTrip();">
                                <i class="fa fa-check"></i> Start Trip
                            </a>
                        <?php }else {  ?>
                            <a href="javascript:void(0);" class="btn btn-primary pull-right disabled" id="start_trip_btn" 
                               style="margin-right: 5px;">
                                <i class="fa fa-check"></i> Start Trip
                            </a>
                        <?php } ?>   
                        <button class="btn btn-success pull-right hidden"><i class="fa fa-credit-card"></i> Submit Payment</button>

                    </div>
                </div>
            </div>
        </div>

<?php } ?>
</div>
<!---- Assign Driver Popup Starts ---->
<div class="assigndriver col-md-6 col-xs-12">
    <div class="assigndriver-header">
        <h2>New Driver Booking
            <button type="button" class="close assigndriver-close">
                <span>×</span>
            </button></h2>		
    </div>
    <div class="assigndriver-body">
        <div id="alerts"></div>

        <div class="x_content">
            <br />
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                <!-- info row -->
                <div class="row invoice-info">
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col" style="line-height:22px">
                        <b>Booking Id :</b> <span id="bookingId"><?php echo $trip_model->get('bookingKey'); ?></span>
                        <br>                         
                        <b>Pickup Date :</b> <span id="bookingDate"><?php echo $trip_model->get('pickupDatetime'); ?></span>
                        <br>
                        <b>Pickup Time :</b> <span id="bookingTime"><?php echo $trip_model->get('pickupDatetime'); ?></span>
                        <br>                         
                    </div>

                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <b>Pick Point</b>
                        <address>
                            <span id="pAdd"><?php echo $trip_model->get('pickupLocation'); ?></span> 

                        </address>
                    </div>
                    <div class="col-sm-4 invoice-col">
                        <b>Destination</b>
                        <address>
                            <span id="dAdd"><?php echo $trip_model->get('dropLocation'); ?></span> 

                        </address>
                    </div>


                </div>
                <!-- /.row -->
                <div class="panel-body" id="driver_list">

                </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group hidden">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                <button type="submit" class="btn btn-primary">Cancel</button>
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>					 
        </form>
    </div>
</div>   

<!---- Assign Driver Popup Ends ---->

<!--- End Trip Popup Starts--->
<div class="endtrip_popup col-md-6 col-xs-12">
    <div class="endtrip_popup-header">
        <h2>End Trip
            <button type="button" class="close endtrip_popup-close">
                <span>×</span>
            </button></h2>		
    </div>
    <div class="endtrip_popup-body">
        <div id="alerts"></div>

            <div class="form-group">
                <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                    <label for="fullname">Parking Charge</label>
                    <input type="text" id="parkingCharge" class="form-control" name="parkingCharge"  /> 
                </div>   
                <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                    <label for="fullname">Toll Charge</label>
                    <input type="text" id="tollCharge" class="form-control" name="tollCharge"  /> 
                </div>                    

                <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                    <label for="fullname">Rating</label>
                    <input type="text" id="driverRating" class="form-control" name="driverRating"  />
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <label for="fullname">Rating Comments</label>
                    <textarea rows="5" cols="10" id="driverComments" name="driverComments"></textarea>
                </div>					  
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <label for="fullname">Is Zoom Car Trip</label>
                    <input type="checkbox" class="js-switch" name="is_zoom_car" id="is_zoom_car" value="0"/> 
                </div> 


            </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                <button type="button" class="btn btn-primary">Cancel</button>
                <a href="javascript:void(0);"  class="btn btn-success" onclick="endTrip();">Submit</a>
            </div>
        </div>
    </div>
   
</div>

<!--- End Trip Popup Ends--->

<script>
    $("#assigndriver").unbind('click');
    $('#assigndriver').click(function () {
        $('.assigndriver').show();
        getDriversListToAssign();
    });
    $(".assigndriver-close").unbind('click');
    $('.assigndriver-close').click(function () {
        $('.assigndriver').fadeOut('slow');
        return false;
    });

    $('#endtrip_popup, .endtrip_popup-close').click(function () {
        $('.endtrip_popup').slideToggle();
        getDriversListToAssign();
    });
  
</script>


    