<div id="trip-details-information" class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>
                B <i>2</i> C Booking Trip Details
            </h2>
            <br><br>
            <ul class="nav nav-tabs">
                <li id='freshTrip_tab' class="active">
                    <a onclick="setTripStatusFilter('freshTrip');">Fresh Trip</a>
                </li>
                <li id='unassigned_tab'>
                    <a onclick="setTripStatusFilter('unassigned');">Un Assigned</a>
                </li>
                <li id='assigned_tab'>
                    <a onclick="setTripStatusFilter('assigned');">Assigned Trip</a>
                </li>
                <li id='inpogress_tab'>
                    <a onclick="setTripStatusFilter('inprogress');">In Progress</a>
                </li>
                <li id='completed_tab'>
                    <a onclick="setTripStatusFilter('completed');">Completed Trip</a>
                </li>
                <li id='cancelled_tab'>
                    <a onclick="setTripStatusFilter('cancelled');">Cancelled Trip</a>
                </li>
            </ul>
            <!--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown"><a href="#" class="dropdown-toggle"
                            data-toggle="dropdown" role="button" aria-expanded="false"><i
                                    class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                            </ul></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li> 
                    
            </ul>
            <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
      <div class="input-group">
        <input type="text" id="search_driver_content" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-default" id="search_driver_btn" type="button">Go!</button>
        </span>
        
      </div>
      <div><span>Search by Referral Code, Driver Full Name, Email and Mobile</span></div>
    </div>
 </div>-->
            <div class="title_right pull-right">

                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 hidden">
                    <a href="<?php echo base_url('trip/add') ?>">
                        <button id="add_driver_btn"
                                class="btn btn-primary">Add Trip Details
                        </button>
                    </a>

                </div>

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div id="loading" style="padding-left:40%;display:none;"><img src="../public/images/loader.gif"></div>
            <table id="datatable-checkbox"
                   class="table table-striped table-bordered datatable-button-init-collection">
                <thead>
                <tr>
                    <th><!-- <input type="checkbox" id="check-all" class="flat"> --></th>
                    <!-- <th>Sl no</th> -->
                    <?php if ($this->User_Access_Model->getAccessLevelForTripEdit() == User_Access_Level_Enum::EDIT) : ?>
                        <th>Action</th>
                    <?php endif; ?>
                    <th>Booking Key</th>
                    <th>Passenger Name (Mobile)</th>
                    <th>Driver Name (Mobile)</th>
                    <th>Pickup Location</th>
                    <th>Pickup Datetime</th>
                    <th>Drop Location</th>
                    <th>Created Datetime</th>
                    <th>Trip Type</th>
                    <th>Payment Mode</th>
                    <th>Transmission type</th>
                    <th>Trip Status</th>
                    <th>City</th>
                    <!--<th>Entity</th>-->

                </tr>
                </thead>


                <tbody>
                <?php
                //sl no intialization
                // $i = 1;
                /* $driver_model_list=array(
                  0=>array('firstName'=>'Kiran','lastName'=>'Kumar','driverCode'=>'Kiran001','email'=>'global@gmail.com','mobile'=>'9901206631','status'=>1,'skills'=>'Automatic','driverLanguagesKnown'=>'English, Kannada','driverExperience'=>'3 Years','isVerified'=>1,'companyId'=>'ZOOMCAR','doj'=>'20-09-2016','totalTripComplete'=>12,'rating'=>4.5),
                  1=>array('firstName'=>'Kiran','lastName'=>'Kumar','driverCode'=>'Kiran001','email'=>'global@gmail.com','mobile'=>'9901206631','status'=>1,'skills'=>'Manual','driverLanguagesKnown'=>'English, Kannada','driverExperience'=>'5 Years','isVerified'=>0,'companyId'=>'Zuver','doj'=>'20-09-2016','totalTripComplete'=>12,'rating'=>3.8)
                  ); */
                foreach ($trip_details_model_list as $list) {

                    echo '<tr>';
                    if ($list->pickupDatetime >= date("Y-m-d", getCurrentUnixDate())):
                        echo '<td class="' . $list->tripStatusEnum . '"></td>';//<input type="checkbox" class="flat" name="table_records">
                    else:
                        echo '<td class="EXP"></td>';
                    endif;
                    // echo '<th>' . $i . '</th>';
                    if ($this->User_Access_Model->getAccessLevelForTripEdit() == 2) :
                        echo '<td>
                            <a id="editTrip-' . $list->id . '" onclick="editTrip(this.id);" class="userlink">'
                            . '<span class="glyphicon glyphicon-edit"></span></a>'
                            . '</td>';
                    endif;
                    echo '<td>' . $list->id . '</td>';
                    $passenger_mobile = ($list->passengerMobile) ? '(' . $list->passengerMobile . ')' : '';
                    echo '<td>' . $list->passengerFirstName . ' ' . $list->passengerLastName . ' ' . $passenger_mobile . '</td>';
                    $driver_mobile = ($list->driverMobile) ? '(' . $list->driverMobile . ')' : '';
                    echo '<td> ' . $list->driverFirstName . ' ' . $list->driverLastName . ' ' . $driver_mobile . '</td>';
                    echo '<td>' . $list->pickupLocation . '</td>';
                    echo '<td>' . $list->pickupDatetime . '</td>';
                    echo '<td>' . $list->dropLocation . '</td>';
                    echo '<td>' . $list->createdDate . '</td>';
                    echo '<td>' . $list->tripType . '</td>';
                    echo '<td>' . $list->paymentMode . '</td>';
                    echo '<td>' . $list->transmissionType . '</td>';
                    echo '<td>' . $list->tripStatus . '</td>';
                    echo '<td>' . $list->city . '</td>';
                    // echo '<td>' . $list->company . '</td>';

                    //echo '<td> <ul class="list-inline"> <li>';
                    //echo '<div class="btn-group btn-toggle">';
                    //echo '<button class="btn btn-xs btn-primary active">Verified</button>';
                    //echo '<button class="btn btn-xs btn-default">Not Verified</button>';
                    //echo '</div></li></ul></td>';
                    //echo '<td>'.($list['isVerified'])?'Verified':'Not Verified'.'</td>';


                    echo '</tr>';
                    // $i++;
                }
                ?>
                </tbody>
            </table>
            <input type='hidden' name='trip_status_filter' id='trip_status_filter' value='freshTrip'/>
        </div>
    </div>
</div>

