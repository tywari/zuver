<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Trip Tracking</title>
    <style>
      #map_wrapper {
    height: 700px;
}

#map {
    width: 100%;
    height: 100%;
}

#travel_selector {
    margin-top: 10px;
    margin-left: 10px;
}
    </style>
  </head>
  <body>
     
      
      
    <div id="map_wrapper">
    <div id="map" class="mapping"></div>
</div>

<!--<div id="travel_selector">
    <p><strong>Mode of Travel: </strong>
    <select id="travelType" onchange="calcRoute();">
        <option value="WALKING">Walking</option>
        <option value="BICYCLING">Bicycling</option>
        <option value="DRIVING">Driving</option>
        <option value="TRANSIT">Transit</option>
    </select></p>
</div>-->
    <script>
   
function initialize() {
    // Change a few 'var variableName' to 'window.' This lets us set global variables from within our function
    window.directionsService = new google.maps.DirectionsService();
    window.directionsDisplay = new google.maps.DirectionsRenderer();
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
        zoom: 11,
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers (Start & end destination)
    window.markers = [
        ['', <?php echo $pickupLatitude;?>, <?php echo $pickupLongitude;?>],
        ['', <?php echo $driverLatitude;?>, <?php echo $driverLongitude;?>]
    ];
    
    // Render our directions on the map
    directionsDisplay.setMap(map);

    // Set the current route - default: walking
    calcRoute();
    
}

// Calculate our route between the markers & set/change the mode of travel
function calcRoute() {
    //var selectedMode = document.getElementById('travelType').value;
    var request = {
        // London Eye
        origin: new google.maps.LatLng(markers[0][1], markers[0][2]),
        // Palace of Westminster
        destination: new google.maps.LatLng(markers[1][1], markers[1][2]),
        // Set our mode of travel - default: walking
        travelMode: 'DRIVING'
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
}
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcN2hpqU2MY00KFk3jVtrsLJvrucZBr6I&callback=initialize">
    </script>
  </body>
</html>