<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Mobile</th> 
            <th>Languages</th>						  
            <th>Experience</th>                          
            <th>Skill</th> 
            <th>Log Status</th>                         
            <th>Shift</th>
            <th>Available</th>
            <th>Rating</th>
            <th>Distance</th>
            <th>Assign</th>
        </tr>
    </thead>
    <tbody>   
        <?php
        foreach ($driver_list as $list) {
            ?>
            <tr>
                <td><?php echo $list->id; ?></td>
                <td><?php echo $list->driver_name; ?></td>
                <td><?php echo $list->mobile; ?></td>
                <td>
                <?php 
                $languages=explode(' ', $list->driverLanguagesKnown);
                foreach ($languages as $language){
                echo '<span class="badge">'.$language.'</span>'; 
        		}
       			 ?>
                </td>
                <td><?php echo $list->driverExperience; ?></td> 
                <td><?php echo $list->skill; ?></td>
                <td><?php echo ($list->loginStatus == 'Y')?'Active':'Inactive'; ?></td> 
                <td><?php echo $list->shiftStatus; ?></td> 
                <td><?php echo $list->availabilityStatus; ?></td>
                <td><?php echo round($list->driverRating,1); ?></td>
                <td><?php echo round($list->distance,3); ?></td>
                <td> <ul class="list-inline"> 
                        <li>
                            <div class="btn-group btn-toggle"> 
                            
                                <a  <?php echo ($list->availabilityStatus == Driver_Available_Status_Enum::BUSY)?'class="btn btn-xs btn-primary busy"':'class="btn btn-xs btn-primary free"'; ?> id="driver-<?php echo $list->id; ?>"
                                   <?php echo ($list->availabilityStatus == Driver_Available_Status_Enum::BUSY)?'':'onclick="AssignDrivers(this.id)"'; ?>>
                                    <?php echo ($list->availabilityStatus == Driver_Available_Status_Enum::BUSY)?'Busy':'Assign'; ?></a>
                                   
                        <!--   <a href="javascript:void(0);" class="btn btn-xs btn-default">Cancel</a> -->
                            </div>
                        </li></ul></td>

            </tr> 
        <?php } ?>

    </tbody>
</table>