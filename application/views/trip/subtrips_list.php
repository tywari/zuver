<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<table class="table table-striped table-bordered">
<?php if($sub_trip_list){ ?>
    <thead>
        <tr>
            <th width="10%">Id</th>
            <th width="7%">Booking Key</th>
            <th width="25%">Pickup Location</th>
            <!--th>Pickup Latitude</th> 
            <th>Pickup Longitude</th-->						  
            <th width="8%">Pickup Datetime</th>                          
            <th width="25%">Drop Location</th>
            <!--th>Drop Latitude</th> 
            <th>Drop Longitude</th-->                         
            <th width="8%">Drop Datetime</th>
            <th>Odometer</th>
        </tr>
    </thead>
    <tbody>   
        <?php
        $i=1;
        foreach ($sub_trip_list as $list) {
            ?>
            <tr>
                <td width="10%"><?php echo $list->subTripId; ?></td>
                <td width="7%"><?php echo $list->bookingKey; ?></td>
                <td width="25%"><b>Location : </b><?php echo $list->pickupLocation; ?><br ><b>Latitude : </b><?php echo $list->pickupLatitude; ?>   <br ><b>Longitude : </b><?php echo $list->pickupLongitude; ?></td>
                <!--td><?php echo $list->pickupLatitude; ?></td>
                <td><?php echo $list->pickupLongitude; ?></td-->
                <td width="10%"><?php echo $list->pickupDatetime; ?></td>
                <td width="25%"><b>Location : </b><?php echo $list->dropLocation; ?><br ><b>Latitude : </b><?php echo $list->dropLatitude; ?><br ><b>Longitude : </b><?php echo $list->dropLongitude; ?></td>
                <!--td><?php echo $list->dropLatitude; ?></td>
                <td><?php echo $list->dropLongitude; ?></td-->
                <td width="10%"><?php echo $list->dropDatetime; ?></td>
                <td><b>Start : </b><?php echo $list->meterStart; ?><br ><b>End : </b><?php echo $list->meterEnd; ?></td>
            </tr> 
        <?php $i++; } ?>

    </tbody>
    <?php } else { ?>
    <tr><td><p>No Sub-Trips available for this trip.</p></td></tr>
    <?php } ?>
</table>