<?php
$view_mode = $mode;
?>

<style>
    .assigndriver {
        width: 1000px
    }
</style>
<div id="trip-details-information" class="col-md-12 col-sm-12 col-xs-12">

    <div id="style-4" class="col-md-12">
        <div class="x_panel">
            <div class="x_title">

                <div class="col-sm-4 invoice-col">
                    <?php if ($view_mode == EDIT_MODE) { ?>
                        <h2>
                            <?php echo $trip_model->get('bookingKey'); ?>
                            <small>Booked at : <span id="bookingDetail">
                            <?php
                            $booked_date = ($trip_model->get('createdDate')) ? $trip_model->get('createdDate') : '';
                            echo $booked_date;
                            ?>
                                    <!--<i class="fa fa-clock-o "></i> 12:00--></span></small>
                        </h2>
                        <?php
                    }
                    ?>
                </div>
                <?php
                if ($trip_model->get('tripStatus') == Trip_Status_Enum::READY_TO_START || $trip_model->get('tripStatus') == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_model->get('tripStatus') == Trip_Status_Enum::TRIP_CONFIRMED) :?>
                    <div class="col-sm-2 invoice-col">
                        <a class="btn btn-default"
                           id="driver_rejected"><i class="fa fa-send"></i> Driver Rejected</a>
                    </div>
                <?php endif; ?>
                <?php
                if ($trip_model->get('tripStatus') == Trip_Status_Enum::BOOKED || $trip_model->get('tripStatus') == Trip_Status_Enum::READY_TO_START || $trip_model->get('tripStatus') == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_model->get('tripStatus') == Trip_Status_Enum::TRIP_CONFIRMED || $trip_model->get('tripStatus') == Trip_Status_Enum::CANCELLED_BY_DRIVER) :?>
                    <div class="col-sm-2 invoice-col">
                        <a class="btn btn-default"
                           id="passenger_rejected"><i class="fa fa-send"></i> Cancelled By
                            Passenger</a>
                    </div>
                <?php endif;
                if ($trip_model->get('companyId') == ZOOMCAR_COMPANYID && $trip_model->get('tripStatus') != Trip_Status_Enum::TRIP_COMPLETED && $trip_model->get('tripStatus') != Trip_Status_Enum::CANCELLED_BY_PASSENGER ) :?>
                    <div class="col-sm-2 invoice-col">
                        <a class="btn btn-default"
                           id="cancel_zoomcar_trip"><i class="fa fa-send"></i> Cancel ZoomCar Trip
                        </a>
                    </div>
                <?php endif;
                $class_name = ($trip_model->get('tripStatus')) ? 'class="badge ' . $trip_model->get('tripStatus') . '"' : 'class="badge"'; ?>
                <span style='float: right;'><div class="" role="alert"><a <?php echo $class_name; ?>
                                style='font-size: 20px;'> <i class="<?php echo $trip_model->get('tripStatus'); ?>"></i>

                            <?php echo $trip_status[$trip_model->get('tripStatus')];
                            echo form_input(array(
                                'type' => 'hidden',
                                'id' => 'trip-status',
                                'name' => 'trip-status',
                                'value' => $trip_model->get('tripStatus')
                            ));
                            ?>
                    </a></div>
                </span>
                <div class="clearfix"></div>
            </div>
            <!----  Vechile Details  Part For B2B ---->
            <?php if (empty($passenger_model)) : ?>
                <div class='row'>
                    <small class="badge">Vechile Number : <span
                                id="cId"><?php echo $trip_model->get('vehicleNo'); ?></span></small>
                    <small class="badge">Vechile Make : <span
                                id="cId"><?php echo $trip_model->get('vehicleMake'); ?></span></small>
                    <small class="badge">Vechile Model : <span
                                id="cId"><?php echo $trip_model->get('vehicleModel'); ?></span></small>
                </div>
            <?php endif; ?>

            <div class="x_content">


                <br/>
                <?php
                $form_attr = array(
                    'name' => 'edit_trip_form',
                    'id' => 'edit_trip_form',
                    'class' => 'form-horizontal form-label-left',
                    'data-parsley-validate' => '',
                    'method' => 'POST'
                );
                echo form_open_multipart(base_url(''), $form_attr);

                // driver id by default is -1
                echo form_input(array(
                    'type' => 'hidden',
                    'id' => 'trip_id',
                    'name' => 'id',
                    'value' => ($trip_model->get('id')) ? $trip_model->get('id') : -1
                ));
                ?>
                <?php
                if ($view_mode == EDIT_MODE) {
                echo form_input(array(
                    'type' => 'hidden',
                    'id' => 'bookingtrip_id',
                    'name' => 'bookingtrip_id',
                    'value' => $trip_model->get('id')
                ));
                echo form_input(array(
                    'type' => 'hidden',
                    'id' => 'driver_id',
                    'name' => 'driver_id',
                    'value' => ($trip_model->get('driverId')) ? $trip_model->get('driverId') : 0
                ));
                echo form_input(array(
                    'type' => 'hidden',
                    'id' => 'latitude',
                    'name' => 'latitude',
                    'value' => $trip_model->get('pickupLatitude')
                ));
                echo form_input(array(
                    'type' => 'hidden',
                    'id' => 'longitude',
                    'name' => 'longitude',
                    'value' => $trip_model->get('pickupLongitude')
                ));

                echo form_input(array(
                    'type' => 'hidden',
                    'id' => 'companyId',
                    'name' => 'companyId',
                    'value' => $trip_model->get('companyId')
                ));
                ?>
                <section class="content invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-md-6 product_price">
                                <span id="cName"><h4>
                                        <i class="fa fa-user"></i> Customer Details
                                    </h4></span>
                            <?php if (!empty($passenger_model)) : ?>
                                <small class="badge">C-Id : <span
                                            id="cId"><?php echo $passenger_model->get('id'); ?></span></small>
                                <small class="badge">Name : <span
                                            id="cId"><?php echo $passenger_model->get('firstName'); ?></span></small>
                                <small class="badge">Mobile : <span
                                            id="cMobile"><?php echo $passenger_model->get('mobile'); ?></span></small>
                                <small class="badge">Email : <span
                                            id="cEmail"><?php echo $passenger_model->get('email'); ?></span></small>
                                <!-- <small class="badge"><i class="fa fa-headphones"></i></small> -->
                            <?php endif; ?>
                            <?php if (empty($passenger_model)) : ?>
                                <small class="badge">Client Name : <span
                                            id="c-name"><?php echo $trip_model->get('clientName'); ?></span></small><br>
                                <small class="badge">Partner Name : <span
                                            id="p-name"><?php echo $partner_model->get('name'); ?></span></small>
                                <small class="badge">Partner Mobile : <span
                                            id="cMobile"><?php echo $partner_model->get('contactMobile'); ?></span>
                                </small>
                                <br>
                                <small class="badge">User Id : <span id="u-id">
                                        <?php echo $user_personal_detail->get('userId') ?></span></small>
                                <small class="badge">User Name : <span id="u-name">
                                        <?php echo $user_model->get('firstName') . ' ' . $user_model->get('lastName'); ?></span>
                                </small>
                                <small class="badge">User Mobile : <span
                                            id="u-mobile"><?php echo $user_personal_detail->get('mobile'); ?></span>
                                </small>
                                <small class="badge">User Email : <span
                                            id="u-email"><?php echo $user_personal_detail->get('email'); ?></span>
                                </small>
                            <?php endif; ?>

                            <br>
                        </div>
                        <div class="col-md-6 product_price">
                                <span id="cName"><h4>
                                        <i class="fa fa-cab"></i> Driver Details
                                    </h4></span>
                            <?php if (!empty($driver_model)) : ?>
                                <small class="badge">C-Id : <span
                                            id="cId"><?php echo $driver_model->get('id'); ?></span></small>
                                <small class="badge">Name : <span
                                            id="cId"><?php echo $driver_model->get('firstName'); ?></span></small>
                                <small class="badge">Mobile : <span
                                            id="cMobile"><?php echo $driver_model->get('mobile'); ?></span></small>
                            <?php endif; ?>
                            <!--                                <a href="#"  class="pull-right" type="button"">
                                                                <span class="badge alert-success">
                                                                    <i class="fa fa-send"></i> Assign A driver</span>
                                                            </a>					  -->
                            <br>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col" style="line-height: 22px">

                        </div>

                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">

                        </div>
                        <div class="col-sm-4 invoice-col">

                        </div>


                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-md-6 product_price">
                            <!-- <div class="form-group">
                                <?php
                            // validation for driver driver type
                            /* echo form_label ( 'Booking Id :', 'bookingKey', array (
                              'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                              ) );
                              echo '<div class="col-md-9 col-sm-9 col-xs-12">';

                              echo text($trip_model->get('bookingKey'));

                              echo '</div>'; */
                            ?>
                                </div> -->
                            <div class="form-group">
                                <?php
                                // validation for driver driver type
                                echo form_label('Requested PickUp:', 'pickupDatetime', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';
                                if ($trip_model->get('tripStatus') != Trip_Status_Enum::DRIVER_ARRIVED) {
                                    echo form_input(array(
                                        'id' => 'pickupDatetime',
                                        'name' => 'pickupDatetime',
                                        'class' => 'form-control',
                                        'autocomplete' => 'off',
                                        'required' => 'required',
                                        'value' => ($trip_model->get('pickupDatetime')) ? $trip_model->get('pickupDatetime') : ''
                                    ));
                                } else {
                                    echo text($trip_model->get('pickupDatetime'));
                                    echo form_input(array(
                                        'type' => 'hidden',
                                        'id' => 'pickupDatetime',
                                        'name' => 'pickupDatetime',
                                        'value' => ($trip_model->get('pickupDatetime')) ? $trip_model->get('pickupDatetime') : ''
                                    ));
                                }
                                echo '</div>';
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                //assign driver
                                if ($trip_model->get('tripStatus') == Trip_Status_Enum::DRIVER_ACCEPTED || $trip_model->get('tripStatus') == Trip_Status_Enum::TRIP_CONFIRMED || $trip_model->get('tripStatus') == Trip_Status_Enum::DRIVER_ARRIVED || $trip_model->get('tripStatus') == Trip_Status_Enum::IN_PROGRESS) {
                                    // validation for driver driver type
                                    echo form_label('Driver Assigned:', 'Driver Assigned', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                                    ));
                                    echo '<div class="col-md-9 col-sm-9 col-xs-12">';
                                    echo form_input(array(
                                        'id' => 'driverAssignedDatetime',
                                        'name' => 'driverAssignedDatetime',
                                        'class' => 'form-control',
                                        'value' => ($trip_model->get('driverAssignedDatetime') != '0000-00-00 00:00:00') ? $trip_model->get('driverAssignedDatetime') : ''
                                    ));
                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                //driver arrived
                                if ($trip_model->get('tripStatus') == Trip_Status_Enum::DRIVER_ARRIVED || $trip_model->get('tripStatus') == Trip_Status_Enum::IN_PROGRESS) {
                                    // validation for driver driver type
                                    echo form_label('Driver Arrived:', 'Driver Arrived', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                                    ));
                                    echo '<div class="col-md-9 col-sm-9 col-xs-12">';

                                    echo form_input(array(
                                        'id' => 'driverArrivedDatetime',
                                        'name' => 'driverArrivedDatetime',
                                        'class' => 'form-control',
                                        'value' => ($trip_model->get('driverArrivedDatetime') != '0000-00-00 00:00:00') ? $trip_model->get('driverArrivedDatetime') : ''
                                    ));
                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                // Start Trip
                                if ($trip_model->get('tripStatus') == Trip_Status_Enum::IN_PROGRESS) {
                                    // validation for driver driver type
                                    echo form_label('Trip Start:', 'Trip Start', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                                    ));
                                    echo '<div class="col-md-9 col-sm-9 col-xs-12">';
                                    echo form_input(array(
                                        'id' => 'actualPickupDatetime',
                                        'name' => 'actualPickupDatetime',
                                        'class' => 'form-control',
                                        'value' => ($trip_model->get('actualPickupDatetime') != '0000-00-00 00:00:00') ? $trip_model->get('actualPickupDatetime') : ''
                                    ));
                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                // End Trip
                                if ($trip_model->get('tripStatus') == Trip_Status_Enum::IN_PROGRESS) {
                                    // validation for driver driver type
                                    echo form_label('Trip End:', 'Trip End', array(
                                        'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                                    ));
                                    echo '<div class="col-md-9 col-sm-9 col-xs-12">';
                                    echo form_input(array(
                                        'id' => 'dropDatetime',
                                        'name' => 'dropDatetime',
                                        'class' => 'form-control',
                                        'value' => ($trip_model->get('dropDatetime') != '0000-00-00 00:00:00') ? $trip_model->get('dropDatetime') : ''
                                    ));
                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                // validation for driver driver type
                                echo form_label('PickUp Location:', 'PickUp Location:', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';
                                if ($trip_model->get('tripStatus') != Trip_Status_Enum::DRIVER_ARRIVED) {
                                    echo form_input(array(
                                        'id' => 'pickupLocation',
                                        'name' => 'pickupLocation',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'value' => ($trip_model->get('pickupLocation')) ? $trip_model->get('pickupLocation') : ''
                                    ));
                                    echo form_input(array(
                                        'type' => 'hidden',
                                        'id' => 'pickupLatitude',
                                        'name' => 'pickupLatitude',
                                        'class' => 'form-control',
                                        'value' => ($trip_model->get('pickupLatitude')) ? $trip_model->get('pickupLatitude') : ''
                                    ));
                                    echo form_input(array(
                                        'type' => 'hidden',
                                        'id' => 'pickupLongitude',
                                        'name' => 'pickupLongitude',
                                        'class' => 'form-control',
                                        'value' => ($trip_model->get('pickupLongitude')) ? $trip_model->get('pickupLongitude') : ''
                                    ));
                                } else {
                                    echo $trip_model->get('pickupLocation');
                                    echo form_input(array(
                                        'type' => 'hidden',
                                        'id' => 'pickupLocation',
                                        'name' => 'pickupLocation',
                                        'value' => ($trip_model->get('pickupLocation')) ? $trip_model->get('pickupLocation') : ''
                                    ));
                                }
                                echo '</div>';
                                ?>
                            </div>
                            <!-- Show Landmark for pickup location code done by Anuj  -->

                            <div class="form-group">
                                <?php
                                // validation for driver driver type
                                echo form_label('Landmark for pickup:', 'landmark', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';
                                if ($trip_model->get('landmark') != "") {
                                    echo form_input(array(
                                        'id' => 'landmark',
                                        'name' => 'landmark',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'value' => ($trip_model->get('landmark')) ? $trip_model->get('landmark') : ''
                                    ));
                                } else {
                                    echo form_input(array(
                                        'id' => 'landmark',
                                        'name' => 'landmark',
                                        'class' => 'form-control',
                                        'value' => 'No Landmark Available',
                                        'disabled' => 'disabled'
                                    ));

                                }
                                echo '</div>';
                                ?>
                            </div>


                            <!-- Show Landmark for pickup location code done by Anuj  -->

                            <div class="form-group">
                                <?php
                                // validation for driver driver type
                                echo form_label('Drop Location:', 'dropLocation', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';
                                if ($trip_model->get('tripStatus') != Trip_Status_Enum::DRIVER_ARRIVED) {
                                    echo form_input(array(
                                        'id' => 'dropLocation',
                                        'name' => 'dropLocation',
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'value' => ($trip_model->get('dropLocation')) ? $trip_model->get('dropLocation') : ''
                                    ));
                                    echo form_input(array(
                                        'type' => 'hidden',
                                        'id' => 'dropLatitude',
                                        'name' => 'dropLatitude',
                                        'class' => 'form-control',
                                        'value' => ($trip_model->get('dropLatitude')) ? $trip_model->get('dropLatitude') : ''
                                    ));
                                    echo form_input(array(
                                        'type' => 'hidden',
                                        'id' => 'dropLongitude',
                                        'name' => 'dropLongitude',
                                        'class' => 'form-control',
                                        'value' => ($trip_model->get('dropLongitude')) ? $trip_model->get('dropLongitude') : ''
                                    ));
                                } else {
                                    echo $trip_model->get('dropLocation');
                                    echo form_input(array(
                                        'type' => 'hidden',
                                        'id' => 'dropLocation',
                                        'name' => 'dropLocation',
                                        'value' => ($trip_model->get('dropLocation')) ? $trip_model->get('dropLocation') : ''
                                    ));
                                }
                                echo '</div>';
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                // validation for driver driver type
                                echo form_label('Payment Mode :', 'paymentMode', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';

                                echo form_dropdown('paymentMode', $payment_mode_list, $trip_model->get('paymentMode'), array(
                                    'id' => 'paymentMode',
                                    'class' => 'form-control',
                                    'required' => 'required'
                                ));
                                echo '</div>';
                                ?>

                            </div>
                            <div class="form-group">
                                <?php
                                // validation for driver driver type
                                echo form_label('Trip Type:', 'tripType', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';


                                echo form_dropdown('tripType', $trip_type_list, $trip_model->get('tripType'), array(
                                    'id' => 'tripType',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                ));


                                echo '</div>';
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                // validation for driver driver type
                                echo form_label('Transmission Type:', 'transmissionType', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';

                                echo form_dropdown('transmissionType', $transmission_type_list, $trip_model->get('transmissionType'), array(
                                    'id' => 'transmissionType',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                ));

                                echo '</div>';
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                // validation for driver driver type
                                echo form_label('Car Type:', 'carType', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';

                                echo form_dropdown('carType', $car_type_list, $trip_model->get('carType'), array(
                                    'id' => 'carType',
                                    'class' => 'form-control',
                                    'required' => 'required',
                                ));

                                echo '</div>';
                                ?>
                            </div>
                            <div class="form-group hidden" id='b_b_bill_type_sec'>
                                <?php
                                echo form_label('Bill Type:', 'billType', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';

                                echo form_dropdown('billType', $bill_type_list, $trip_model->get('billType'), array(
                                    'id' => 'billType',
                                    'name' => 'billType',
                                    'class' => 'form-control'
                                ));

                                echo '</div>';
                                ?>
                            </div>
                            <div class="form-group">
                                <?php
                                // validation for driver driver type
                                echo form_label('Promocode:', 'promoCode', array(
                                    'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12'
                                ));
                                echo '<div class="col-md-9 col-sm-9 col-xs-12">';

                                echo form_input(array(
                                    'type' => 'text',
                                    'id' => 'promoCode',
                                    'class' => 'form-control',
                                    'name' => 'promoCode',
                                    'value' => ($trip_model->get('promoCode')) ? $trip_model->get('promoCode') : ''
                                ));

                                echo '</div>';
                                ?>

                            </div>
                            <div class="form-group">
                                <?php
                                if ($trip_model->get('tripStatus') != Trip_Status_Enum::TRIP_COMPLETED) {
                                    echo '<div class="col-md-9 col-sm-9 col-xs-12">';
                                    //echo '<button type="button" id="load_driver_btn" class="btn btn-primary" >Cancel</button>';
                                    echo '<button type="button" id="update_trip_details" class="btn btn-primary" >Update</button>';
                                    echo '</div>';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-6  product_price">
                            <div class="bs-example" data-example-id="simple-jumbotron">

                                <iframe id='iframeId'
                                        src="<?php echo base_url('trip/loadGmap/' . $trip_model->get('id')); ?>"
                                        width="100%" height="450px" frameBorder="0">Browser not
                                    compatible.
                                </iframe>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->

            </div>
            </section>
            <!-- /.row -->
            <!-- this row will not appear when printing -->


        <?php } ?>
            <?php
            echo form_close();
            ?>
        </div>
    </div>

    <!---- Assign Driver Popup Starts ---->
    <div class="assigndriver col-md-6 col-xs-12">
        <div class="assigndriver-header">
            <h2>
                Assign Driver
                <button type="button" class="close assigndriver-close">
                    <span>&times;</span>
                </button>
            </h2>
        </div>
        <div class="assigndriver-body">
            <div id="alerts"></div>

            <div class="x_content">
                <br/>
                <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                    <!-- info row -->
                    <div class="row invoice-info">
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col" style="line-height: 22px">
                            <b>Booking Id :</b> <span id="bookingId"><?php echo $trip_model->get('id'); ?></span>
                            <br> <b>Pickup Time :</b> <span
                                    id="bookingTime"><?php echo $trip_model->get('pickupDatetime'); ?></span>
                            <br>
                        </div>

                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <b>Pick Point</b>
                            <address>
                                <span id="pAdd"><?php echo $trip_model->get('pickupLocation'); ?></span>

                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                            <b>Destination</b>
                            <address>
                                <span id="dAdd"><?php echo $trip_model->get('dropLocation'); ?></span>

                            </address>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="panel-body" id="driver_list"></div>

            </div>

            <div class="ln_solid"></div>
            <div class="form-group hidden">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                    <button type="submit" class="btn btn-primary">Cancel</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>

    <!---- Assign Driver Popup Ends ---->

    <!--- End Trip Popup Starts--->
    <div class="endtrip_popup col-md-6 col-xs-12">
        <div class="endtrip_popup-header">
            <h2>
                End Trip
                <button type="button" class="close endtrip_popup-close">
                    <span>&times;</span>
                </button>
            </h2>
        </div>
        <div class="endtrip_popup-body">
            <div id="alerts"></div>

            <div class="form-group">
                <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                    <label for="fullname">Parking Charge</label> <input type="text"
                                                                        id="parkingCharge" class="form-control"
                                                                        name="parkingCharge"/>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                    <label for="fullname">Toll Charge</label> <input type="text"
                                                                     id="tollCharge" class="form-control"
                                                                     name="tollCharge"/>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                    <label for="fullname">Rating</label>
                    <!-- <input type="text" id="driverRating" class="rating rating-loading" name="driverRating" value="0" data-size="xs" title="" /> -->
                    <input type="text" id="driverRating" class="form-control" placeholder="Enter driver rating 0-5"
                           name="driverRating"/>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <label for="fullname">Rating Comments</label>
                    <textarea rows="5" cols="10" id="driverComments"
                              name="driverComments"></textarea>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback hidden">
                    <label for="fullname">Is Zoom Car Trip</label> <input
                            type="checkbox" class="js-switch" name="is_zoom_car"
                            id="is_zoom_car" value="0"/>
                </div>


            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                    <button type="button" class="btn btn-primary" id="cancel_endtrip">Cancel</button>
                    <a href="javascript:void(0);" class="btn btn-success"
                       onclick="endTrip();">Submit</a>
                </div>
            </div>
        </div>

    </div>

    <!--- End Trip Popup Ends -->

    <!-- Add Sub Trip Popup Starts -->
    <div class="subtrip_popup col-md-6 col-xs-12">
        <div class="subtrip_popup-header">
            <h2>
                Add Sub Trip
                <button type="button" class="close subtrip_popup-close">
                    <span>&times;</span>
                </button>
            </h2>
        </div>
        <div class="subtrip_popup-body" style="margin-bottom: 155px;">
            <div id="alerts"></div>

            <div class="form-group">
                <?php
                $form_attr = array(
                    'name' => 'add_subtrip_form',
                    'id' => 'add_subtrip_form',
                    'class' => 'form-horizontal form-label-left',
                    'data-parsley-validate' => '',
                    'method' => 'POST'
                );
                echo form_open('', $form_attr);
                // driver id by default is -1
                echo form_input(array(
                    'type' => 'hidden',
                    'id' => 'masterTripId',
                    'name' => 'masterTripId',
                    'value' => ($trip_model->get('id')) ? $trip_model->get('id') : -1
                ));
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('PickUp Location:', 'PickUp Location:', array('class' => 'control-label required'));
                    echo form_input(array(
                        'id' => 'subTrip_pickupLocation',
                        'name' => 'subTrip_pickupLocation',
                        'class' => 'form-control',
                        'required' => 'required',
                        'value' => ($trip_model->get('pickupLocation')) ? $trip_model->get('pickupLocation') : ''
                    ));
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'subTrip_pickupLatitude',
                        'name' => 'subTrip_pickupLatitude',
                        'class' => 'form-control',
                        'value' => ($trip_model->get('pickupLatitude')) ? $trip_model->get('pickupLatitude') : ''
                    ));
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'subTrip_pickupLongitude',
                        'name' => 'subTrip_pickupLongitude',
                        'class' => 'form-control',
                        'value' => ($trip_model->get('pickupLongitude')) ? $trip_model->get('pickupLongitude') : ''
                    ));
                    ?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('PickUp DateTime:', 'pickupDatetime', array('class' => 'control-label required'
                    ));
                    echo form_input(array(
                        'id' => 'subTrip_pickupDatetime',
                        'name' => 'subTrip_pickupDatetime',
                        'class' => 'form-control',
                        'required' => 'required',
                        'value' => ($trip_model->get('pickupDatetime')) ? $trip_model->get('pickupDatetime') : ''
                    ));
                    ?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('Drop Location:', 'dropLocation', array(
                        'class' => 'control-label'));
                    echo form_input(array(
                        'id' => 'subTrip_dropLocation',
                        'name' => 'subTrip_dropLocation',
                        'class' => 'form-control',
                        'value' => ($trip_model->get('dropLocation')) ? $trip_model->get('dropLocation') : ''
                    ));
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'subTrip_dropLatitude',
                        'name' => 'subTrip_dropLatitude',
                        'class' => 'form-control',
                        'value' => ($trip_model->get('dropLatitude')) ? $trip_model->get('dropLatitude') : ''
                    ));
                    echo form_input(array(
                        'type' => 'hidden',
                        'id' => 'subTrip_dropLongitude',
                        'name' => 'subTrip_dropLongitude',
                        'class' => 'form-control',
                        'value' => ($trip_model->get('dropLongitude')) ? $trip_model->get('dropLongitude') : ''
                    ));
                    ?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('Drop DateTime:', 'Drop DateTime', array('class' => 'control-label required'));
                    echo form_input(array(
                        'id' => 'subTrip_dropDatetime',
                        'name' => 'subTrip_dropDatetime',
                        'class' => 'form-control',
                        'required' => 'required',
                        'value' => ($trip_model->get('dropDatetime') != '0000-00-00 00:00:00') ? $trip_model->get('dropDatetime') : ''
                    ));
                    ?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('Odometer Start:', 'Odometer Start', array('class' => 'control-label '));
                    echo form_input(array(
                        'id' => 'subTrip_meterStart',
                        'name' => 'subTrip_meterStart',
                        'class' => 'form-control',
                        'value' => '0'
                    ));
                    ?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('Odometer End:', 'Odometer End', array('class' => 'control-label '));
                    echo form_input(array(
                        'id' => 'subTrip_meterEnd',
                        'name' => 'subTrip_meterEnd',
                        'class' => 'form-control',
                        'value' => '0'
                    ));
                    ?>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                    <button type="button" class="btn btn-primary" id="cancel_subtrip">Cancel</button>
                    <a href="javascript:void(0);" class="btn btn-success"
                       onclick="AddSubTrip();">Submit</a>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- Add Sub Trip Popup Ends -->
    <!-- Sub trip list pop-up -->
    <div id="subtripsListModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">List of Sub Trips</h4>
                </div>
                <div class="modal-body" id="subtrip_list">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Done</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Sub trip list pop-up -->
</div>
<?php if ($view_mode == EDIT_MODE) { ?>
    <div class="col-md-12">
        <div class="bt-fix">
            <div class="x_panel">
                <div class="x_content" style="">
                    <?php if ($trip_model->get('companyId') && $trip_model->get('companyId') != ZOOMCAR_COMPANYID && $trip_model->get('companyId') != DEFAULT_COMPANY_ID): ?>
                        <div class="col-xs-3">
                        </div>
                        <div class="col-xs-9">
                            <?php
                            if ($trip_model->get('tripStatus') == Trip_Status_Enum::IN_PROGRESS) : ?>
                                <a class="btn btn-default" id="subtrip_popup"><i class="fa fa-cab"></i> Add Sub-Trip</a>
                            <?php else : ?>
                                <a class="btn btn-default disabled"><i class="fa fa-cab"></i> Add Sub-Trip</a>
                            <?php endif; ?>
                            <a class="btn btn-default" id="list_subtrip_btn" onclick="getSubTrips();"><i
                                        class="fa fa-cab"></i> View Sub-Trips</a>
                        </div>
                    <?php endif; ?>
                    <div class="col-xs-12">
                        <?php
                        // Assign Driver
                        if (($trip_model->get('tripStatus') == Trip_Status_Enum::BOOKED || $trip_model->get('tripStatus') == Trip_Status_Enum::CANCELLED_BY_DRIVER)) {
                            ?>
                            <a
                                    class="btn btn-default" id="assigndriver"><i class="fa fa-send"></i>
                                Assign Driver</a>
                        <?php } else { ?>
                            <a
                                    class="btn btn-default disabled"><i class="fa fa-send"></i> Assign
                                Driver</a>
                        <?php } ?>

                        <?php
                        // Driver Arrived
                        if ($trip_model->get('tripStatus') == Trip_Status_Enum::TRIP_CONFIRMED || $trip_model->get('tripStatus') == Trip_Status_Enum::DRIVER_ACCEPTED) {
                            ?>
                            <a
                                    class="btn btn-default" id="driver_arrived_btn"
                                    onclick="driverArrived();"> <i class="fa fa-send"></i> Driver
                                Arrived
                            </a>
                        <?php } else { ?>
                            <a
                                    class="btn btn-default disabled"><i class="fa fa-send"></i> Driver
                                Arrived</a>
                        <?php } ?>
                        <!-- <button class="btn btn-danger" id="load_trip_btn">Cancel</button> -->
                        <?php
                        // Start Trip
                        if ($trip_model->get('tripStatus') == Trip_Status_Enum::DRIVER_ARRIVED) {
                            ?>
                            <a
                                    class="btn btn-default " id="start_trip_btn"
                                    style="margin-right: 5px;" onclick="startTrip();"> <i
                                        class="fa fa-check"></i> Start Trip
                            </a>
                        <?php } else { ?>
                            <a
                                    class="btn btn-default disabled" id="start_trip_btn"
                                    style="margin-right: 5px;"> <i class="fa fa-check"></i> Start Trip
                            </a>
                        <?php } ?>
                        <?php
                        // End Trip
                        if ($trip_model->get('tripStatus') == Trip_Status_Enum::IN_PROGRESS) {
                            ?>
                            <a
                                    class="btn btn-default " id="endtrip_popup"
                                    style="margin-right: 5px;"> <i class="fa fa-close"></i> End Trip
                            </a>
                        <?php } else { ?>
                            <a
                                    class="btn btn-default disabled"
                                    style="margin-right: 5px;"> <i class="fa fa-close"></i> End Trip
                            </a>
                        <?php } ?>


                        <?php
                        // Start Trip
                        if ($trip_model->get('tripStatus') == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT) {
                            ?>
                            <a
                                    class="btn btn-default " id="payment_confirm_btn" onclick="paymentConfirm();"
                                    style="margin-right: 5px;">
                                <i class="fa fa-credit-card"></i> Submit Payment
                            </a>
                        <?php } else { ?>
                            <a
                                    class="btn btn-default disabled" id="payment_confirm_btn"
                                    style="margin-right: 5px;">
                                <i class="fa fa-credit-card"></i> Submit Payment
                            </a>
                        <?php } ?>
                        <?php
                        // Bill Summary
                        if ($transcation_model) {
                            if ($transcation_model->get('id') > 0) {

                                if ($trip_model->get('tripStatus') == Trip_Status_Enum::TRIP_COMPLETED) {
                                    ?>
                                    <a href="<?php echo base_url('transaction/getDetailsById/' . $transcation_model->get('id')) . '/view' ?>"
                                       class="btn btn-default "
                                       style="margin-right: 5px;"> <i class="fa fa-inr"></i> Bill Summary
                                    </a>
                                <?php } else {
                                    ?>
                                    <a
                                            class="btn btn-default disabled"
                                            style="margin-right: 5px;"> <i class="fa fa-inr"></i> Bill Summary
                                    </a>
                                    <?php

                                }
                            } else {
                                ?>
                                <a
                                        class="btn btn-default disabled"
                                        style="margin-right: 5px;"> <i class="fa fa-inr"></i> Bill Summary
                                </a>
                                <?php

                            }
                        } else {
                            ?>
                            <a
                                    class="btn btn-default disabled"
                                    style="margin-right: 5px;"> <i class="fa fa-inr"></i> Bill Summary
                            </a>
                            <?php

                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>


<script>
    //$("#assigndriver").unbind('click');
    $('#assigndriver').click(function () {
        $('.assigndriver').slideToggle();
        getDriversListToAssign();
    });

    $('.assigndriver-close').click(function () {
        $('.assigndriver').fadeOut('slow');
        return false;
    });

    $('#endtrip_popup, .endtrip_popup-close').unbind('click').bind('click', function (e) {
        $('.endtrip_popup').slideToggle('toggle');
        getDriversListToAssign();
    });

    $('#subtrip_popup, .subtrip_popup-close').unbind('click').bind('click', function (e) {
        $('.subtrip_popup').slideToggle('toggle');
        $('#subTrip_pickupDatetime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            startDate: '',
            format: 'Y-m-d H:i',
            minDate: 0,
            minTime: 0,
            step: 15,
            onShow: function (ct) {
                this.setOptions({
                    minDate: jQuery('#pickupDatetime').val() ? jQuery('#pickupDatetime').val() : false,
                    minTime: jQuery('#pickupDatetime').val() ? jQuery('#pickupDatetime').val() : false,

                })
            }
        });

        $('#subTrip_dropDatetime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'en',
            startDate: '',
            format: 'Y-m-d H:i',
            minDate: 0,
            minTime: 0,
            step: 15,
            onShow: function (ct) {
                this.setOptions({
                    minDate: jQuery('#pickupDatetime').val() ? jQuery('#pickupDatetime').val() : false,
                    minTime: jQuery('#pickupDatetime').val() ? jQuery('#pickupDatetime').val() : false,

                })
            }
        });
    });

    $('#cancel_subtrip, .subtrip_popup-close').unbind('click').bind('click', function (e) {
        $('.subtrip_popup').slideToggle('toggle');
    });

    $('#cancel_endtrip, .endtrip_popup-close').unbind('click').bind('click', function (e) {
        $('.endtrip_popup').slideToggle('toggle');
    });

</script>