<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple markers</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        width: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
     <script src="<?php echo js_url('vendors/jquery/dist/jquery.min.js'); ?>"></script>
  </head>
  <body>
  <input type='hidden' id='city_id' name='city_id'  value='<?php echo $cityId;?> ' >
  <input type='hidden' id='pickup_lat' name='pickup_lat'  value='<?php echo $pickupLatitude;?> ' >
  <input type='hidden' id='pickup_long' name='pickup_long'  value='<?php echo $pickupLongitude;?> ' >

  <input type='hidden' id='drop_lat' name='drop_lat'  value='<?php echo $dropLatitude;?> ' >
  <input type='hidden' id='drop_long' name='drop_long'  value='<?php echo $dropLongitude;?> ' >

  <input type='hidden' id='pickupLocation' name='pickupLocation'  value='<?php echo $pickupLocation;?>' >
  <input type='hidden' id='dropLocation' name='dropLocation'  value='<?php echo $dropLocation;?> ' >

  	<!--<span style='margin-left:200px;'>Free &nbsp;<img src="/public/images/app/driver_free.png" alt="Free"/> <span id='free_count_sec'> </span></span>-->
    <span>
        Awaiting Trip
        <img src="<?php echo $this->config->item('base_url'); ?>/public/images/app/driver_on.png" alt="Free" style="width: 20px;" /> 
        <span id='free_count_sec'> </span>
      </span>
      <span style='margin-left:20px;'>
        Off Duty
        <img src="<?php echo $this->config->item('base_url'); ?>/public/images/app/driver_off.png" alt="Free" style="width: 20px;"/> 
        <span id='yellow_count_sec'> </span>
      </span> 
      <span style='margin-left:20px;'>
        Logged Out
        <img src="<?php echo $this->config->item('base_url'); ?>/public/images/app/driver_logged_in.png" alt="Not available" style="width: 20px;"/>
        <span id='na_count_sec'> </span>
      </span>
      <span style='margin-left:20px;'>
        On Trip
        <span id='busy_count_sec'> </span>
      </span> 
<!--      <span style='margin-left:50px;'>Not Available &nbsp;<img src="/public/images/app/driver_notavailable.png" alt="Not available"/><span id='na_count_sec'> </span></span> -
      <span style='margin-left:50px;'>Busy &nbsp;<img src="/public/images/app/driver_busy.png" alt="Busy"/><span id='busy_count_sec'> </span></span> -->
      
    <div id="map"></div>
    <script>
      BASE_URL = "<?php echo $this->config->item('base_url'); ?>";
      function initMap() {
    	  var city_id = $('#city_id').val();
    	  var pickup_lat =parseFloat( $('#pickup_lat').val());
    	  var pickup_long =parseFloat( $('#pickup_long').val());
        
        var drop_lat =parseFloat( $('#drop_lat').val());
        var drop_long =parseFloat( $('#drop_long').val());

        var myLatLng = {lat: pickup_lat, lng: pickup_long};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: myLatLng
        });


        // Pickup location
        var image = {
           url: BASE_URL+'/public/images/app/pickup.png',
           // This marker is 20 pixels wide by 32 pixels high.
           size: new google.maps.Size(32, 32),
           // The origin for this image is (0, 0).
           origin: new google.maps.Point(0, 0),
           // The anchor for this image is the base of the flagpole at (0, 32).
           anchor: new google.maps.Point(0, 32)
        };

        var shape = {
                  coords: [1, 1, 1, 20, 18, 20, 18, 1],
                  type: 'poly'
                };
           
        var lt  = pickup_lat;
        var lnt = pickup_long;

        var marker = new google.maps.Marker({
                    position: {lat: lt, lng: lnt},
                    map: map,
                    icon: image,
                    shape: shape,
                    title: "Pickup Location: <?php echo $pickupLocation;?>",
                    zIndex: 10
                  });


        // Drop Location
        var image = {
           url: BASE_URL+'/public/images/app/drop.png',
           // This marker is 20 pixels wide by 32 pixels high.
           size: new google.maps.Size(32, 32),
           // The origin for this image is (0, 0).
           origin: new google.maps.Point(0, 0),
           // The anchor for this image is the base of the flagpole at (0, 32).
           anchor: new google.maps.Point(0, 32)
        };

        var shape = {
                  coords: [1, 1, 1, 20, 18, 20, 18, 1],
                  type: 'poly'
                };
           
        var lt  = drop_lat;
        var lnt = drop_long;

        var marker = new google.maps.Marker({
                    position: {lat: lt, lng: lnt},
                    map: map,
                    icon: image,
                    shape: shape,
                    title: 'Drop Location: <?php echo $dropLocation;?>',
                    zIndex: 10
                  });
       /*  var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        }); */

        //get Driver Latitude and Longitude
        var surl = BASE_URL + '/driver/getCurrentDriversForMapView_Trip/';
        $.ajax({
            method: "POST",
            url: surl,
            data:{
                  'city_id': city_id,
                  'tripPLat' : '<?php echo $pickupLatitude; ?>',
                  'tripPLon' : '<?php echo $pickupLongitude;?>'
                },
            async: false
        }).done(function (response) {
            
           var data = $.parseJSON(response);
           /*var free_count           = data.free_count;
           var notavailable_count   = data.notavailable_count;
           var busy_count           = data.busy_count*/
           var on_trip = 0;
           var green_count = 0;
           var yellow_count = 0;
           var red_count = 0;

         /*$('#free_count_sec').html(' : '+free_count);
         //$('#na_count_sec').html(' : '+notavailable_count);
         $('#busy_count_sec').html(' : '+busy_count);
         //$('#busy_count_sec').html(' : '+busy_count);*/
           if(data.length!=0){
                $.each(data, function(i, item) {
                  if (
                      (item.loginStatus == 'Y') &&
                      (item.shiftStatus == 'IN')
                      ) 
                  {
                        if (item.availabilityStatus=='B') {
                          //Means this driver is On a Trip right now, one level above Green so no display
                          on_trip++;
                          return true;
                        } 
                        else if (item.availabilityStatus=='F') 
                        {
                            //Means this driver is Not On Trip but is ready to take trips, Awaiting Trip, Green Level
                            green_count++;
                            var image = {
                               url: BASE_URL+'/public/images/app/driver_on.png',
                               // This marker is 20 pixels wide by 32 pixels high.
                               size: new google.maps.Size(32, 32),
                               // The origin for this image is (0, 0).
                               origin: new google.maps.Point(0, 0),
                               // The anchor for this image is the base of the flagpole at (0, 32).
                               anchor: new google.maps.Point(0, 32)
                            };
                        }
                  }
                  else if (
                      (item.loginStatus == 'Y') &&
                      (item.shiftStatus == 'OUT')
                      ) 
                  {
                        //This means that Driver as of now is Off Duty, Yellow Level, not interested in taking trips
                        yellow_count++;
                        var image = {
                           url: BASE_URL+'/public/images/app/driver_off.png',
                           // This marker is 20 pixels wide by 32 pixels high.
                           size: new google.maps.Size(32, 32),
                           // The origin for this image is (0, 0).
                           origin: new google.maps.Point(0, 0),
                           // The anchor for this image is the base of the flagpole at (0, 32).
                           anchor: new google.maps.Point(0, 32)
                        };
                  }
                  else {
                      red_count++;
                      return true;
                  }

                	//if(item.loginStatus=='Y'&& item.availabilityStatus=='F' && item.shiftStatus=='IN' ||item.shiftStatus=='OUT' ){
                    /* Commented by Aditya on 25th Apr 2017
                    if(item.availabilityStatus=='F'){
                           var image = {
                           url: BASE_URL+'/public/images/app/driver_free.png',
                           // This marker is 20 pixels wide by 32 pixels high.
                           size: new google.maps.Size(20, 32),
                           // The origin for this image is (0, 0).
                           origin: new google.maps.Point(0, 0),
                           // The anchor for this image is the base of the flagpole at (0, 32).
                           anchor: new google.maps.Point(0, 32)
                         };
                  }
                  */
                  //if(item.loginStatus=='N'&& item.availabilityStatus=='F' && item.shiftStatus=='OUT'){
                  /* if(item.loginStatus=='N'&& item.availabilityStatus=='F' && item.shiftStatus=='OUT'){
                            var image = {
                           url: BASE_URL+'/public/images/app/driver_notavailable.png',
                           // This marker is 20 pixels wide by 32 pixels high.
                           size: new google.maps.Size(20, 32),
                           // The origin for this image is (0, 0).
                           origin: new google.maps.Point(0, 0),
                           // The anchor for this image is the base of the flagpole at (0, 32).
                           anchor: new google.maps.Point(0, 32)
                         };
                  } */
                //  if(item.loginStatus=='Y' && item.availabilityStatus=='B' && item.shiftStatus=='IN' || item.shiftStatus=='OUT'){
                  /* Commented By Aditya on 25th Apr 2017
                  if(item.availabilityStatus=='B'){
                            var image = {
                           url: BASE_URL+'/public/images/app/driver_busy.png',
                           // This marker is 20 pixels wide by 32 pixels high.
                           size: new google.maps.Size(20, 32),
                           // The origin for this image is (0, 0).
                           origin: new google.maps.Point(0, 0),
                           // The anchor for this image is the base of the flagpole at (0, 32).
                           anchor: new google.maps.Point(0, 32)
                         };
                  }
                  */
                // Shapes define the clickable region of the icon. The type defines an HTML
                // <area> element 'poly' which traces out a polygon as a series of X,Y points.
                // The final coordinate closes the poly by connecting to the first coordinate.
                var shape = {
                  coords: [1, 1, 1, 20, 18, 20, 18, 1],
                  type: 'poly'
                };
                   
                        var lt  = parseFloat(item.currentLatitude);
                        var lnt = parseFloat(item.currentLongitude);
                        var log_status=(item.loginStatus == 'Y')?'Active':'Inactive';
                        var marker = new google.maps.Marker({
                                    position: {lat: lt, lng: lnt},
                                    map: map,
                                    icon: image,
                                    shape: shape,
                                    title: 'Name:'+item.driver_name+'  Mobile:'+item.mobile+'  Log:'+log_status+'  Shift:'+item.shiftStatus+' Distance: '+parseFloat(item.distance).toFixed(2),
                                    zIndex: 2
                                  });
                        marker.addListener('click', function() {
                          var cmf = confirm('Are you sure you wish to assign this trip to\n\nDriver Name: '+item.driver_name+'\nDistance to Pickup: '+parseFloat(item.distance).toFixed(2)+'\n\nClick on OK button to Assign Driver To This Trip\nClick on Cancel button to cancel this assignment\n\n');
                          if (cmf) {
                            // Perform assign trip to driver action
                            // AssignDrivers(this.id)item.id
                            parent.AssignDrivers_GoogleMap(item.id);
                          }
                        });
                    });
                }
                
          $('#free_count_sec').html(' : '+green_count);
          //$('#na_count_sec').html(' : '+notavailable_count);
          $('#busy_count_sec').html(' : '+on_trip);
          //$('#busy_count_sec').html(' : '+busy_count);
          $('#yellow_count_sec').html(' : '+yellow_count);
          $('#na_count_sec').html(' : '+red_count);
        
             
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcN2hpqU2MY00KFk3jVtrsLJvrucZBr6I&callback=initMap">
    </script>

    
    
  </body>
</html>