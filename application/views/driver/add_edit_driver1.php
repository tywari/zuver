<?php
$view_mode = $mode;
?>
<div id="driver-details-information"
     class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>
                Driver Register<small>Personal Details</small>
            </h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <?php
            $form_attr = array(
                'name' => 'edit_driver_form',
                'id' => 'edit_driver_form',
                'class' => 'form-horizontal form-label-left',
                'data-parsley-validate' => '',
                'method' => 'POST'
            );
            echo form_open_multipart(base_url(''), $form_attr);

            // driver id by default is -1
            echo form_input(array(
                'type' => 'hidden',
                'id' => 'driver_id',
                'name' => 'id',
                'value' => ($driver_model->get('id')) ? $driver_model->get('id') : -1
            ));
            ?>
            <?php
            if ($driver_model->get('driverCode')) {
                echo '<div class="form-group">';

                echo '<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">';
                echo form_label('Driver Referral Code:', 'driverCode', array(
                    'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
                ));
                echo text($driver_model->get('driverCode'));

                echo '</div></div>';
            }
            if ($driver_model->get('lastLoggedIn')) {
                echo '<div class="form-group">';

                echo '<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">';
                echo form_label('Last Logged In:', 'lastLoggedIn', array(
                    'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
                ));
                echo text($driver_model->get('lastLoggedIn'));

                echo '</div></div>';
            }
            ?>

            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">

                    <?php
                    // validation for driver first name
                    echo form_label('First Name:', 'firstName', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));

                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'firstName',
                            'name' => 'firstName',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[A-Za-z\s]{3,15}',
                            'value' => ($driver_model->get('firstName')) ? $driver_model->get('firstName') : ''
                        ));
                    } else {
                        echo text($driver_model->get('firstName'));
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver last name
                    echo form_label('Last Name:', 'lastName', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'lastName',
                            'name' => 'lastName',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[A-Za-z\s]{1,15}',
                            'value' => ($driver_model->get('lastName')) ? $driver_model->get('lastName') : ''
                        ));
                    } else {
                        echo text($driver_model->get('lastName'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Gender

                    echo form_label('Gender:', 'gender', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        if (is_array($driver_gender) && count($driver_gender)) {
                            echo '<p>';
                            //debug_exit($driver_gender);
                            foreach ($driver_gender as $value) {
                                // validation for pm to show only delete buttons & admin for their draft release only delete buttons

                                echo $value->description . ":";

                                echo form_radio(array(
                                    'id' => 'gender' . $value->enumName,
                                    'name' => 'gender',
                                    'class' => 'flat',
                                    'value' => $value->enumName,
                                    'required' => 'required',
                                    'checked' => $driver_personal_details_model->get('gender') ? 'true' : 'false'
                                ));
                            }
                            echo '</p>';
                        }
                    } else {
                        echo text($driver_personal_details_model->get('gender'));
                    }
                    ?>

                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver DOB
echo form_label('Date of Birth:', 'dob', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'dob',
        'name' => 'dob',
        'class' => 'form-control',
        'autocomplete' => 'off',
        'required' => 'required',
        'value' => ($driver_personal_details_model->get('dob')) ? $driver_personal_details_model->get('dob') : ''
    ));
} else {
    echo text($driver_personal_details_model->get('dob'));
}
?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver profile Image
echo form_label('Profile Image:', 'profileImage', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
));
if ($view_mode == EDIT_MODE) {
    echo form_upload(array(
        'id' => 'profileImage',
        'name' => 'profileImage[]',
        'class' => 'form-control',
       // 'required' => 'required',
        'value' => ($driver_model->get('profileImage')) ? $driver_model->get('profileImage') : ''
    ));
} else {
    echo text($driver_model->get('profileImage'));
}
?>

                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver email
echo form_label('Email:', 'email', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'email',
        'name' => 'email',
        'class' => 'form-control',
        'required' => 'required',
        'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$',
        'value' => ($driver_model->get('email')) ? $driver_model->get('email') : ''
    ));
} else {
    echo text($driver_model->get('email'));
}
?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver mobile
                    echo form_label('Mobile:', 'mobile', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'mobile',
                            'name' => 'mobile',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[789]\d{9}',
                            'data-inputmask' => '"mask" : "(999) 999-9999"',
                            'value' => ($driver_model->get('mobile')) ? $driver_model->get('mobile') : ''
                        ));
                    } else {
                        echo text($driver_model->get('mobile'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver address
echo form_label('Address:', 'address', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
));
if ($view_mode == EDIT_MODE) {
    echo form_textarea(array(
        'id' => 'address',
        'name' => 'address',
        'class' => 'form-control',
       // 'required' => 'required',
        'pattern' => '[a-zA-Z0-9.#,-\s]',
        'rows' => '3',
        'value' => ($driver_personal_details_model->get('address')) ? $driver_personal_details_model->get('address') : ''
    ));
} else {
    echo text($driver_personal_details_model->get('address'));
}
?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">

                    <?php
                    // validation for driver driverLanguagesKnown
                    echo form_label('Languages Known:', 'driverLanguagesKnown', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'driverLanguagesKnown',
                            'name' => 'driverLanguagesKnown',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[a-zA-Z,\s]{1,50}',
                            'value' => ($driver_model->get('driverLanguagesKnown')) ? $driver_model->get('driverLanguagesKnown') : ''
                        ));
                    } else {
                        echo text($driver_model->get('driverLanguagesKnown'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver Address Proof No
echo form_label('Address Proof No:', 'addressProofNo', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'addressProofNo',
        'name' => 'addressProofNo',
        'class' => 'form-control',
        'required' => 'required',
        'pattern' => '[a-zA-Z0-9-\s]{1,250}',
        'value' => ($driver_personal_details_model->get('addressProofNo')) ? $driver_personal_details_model->get('addressProofNo') : ''
    ));
} else {
    echo text($driver_personal_details_model->get('addressProofNo'));
}
?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Address Proof Image
                    echo form_label('Address Proof Image:', 'addressProofImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'addressProofImage',
                            'name' => 'addressProofImage[]',
                            'multiple'=>true,
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => ($driver_personal_details_model->get('addressProofImage')) ? $driver_personal_details_model->get('addressProofImage') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('addressProofImage'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver Identity Proof No
echo form_label('Identity Proof No:', 'identityProofNo', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'identityProofNo',
        'name' => 'identityProofNo',
        'class' => 'form-control',
       // 'required' => 'required',
        'pattern' => '[a-zA-Z0-9-\s]{1,50}',
        'value' => ($driver_personal_details_model->get('identityProofNo')) ? $driver_personal_details_model->get('identityProofNo') : ''
    ));
} else {
    echo text($driver_personal_details_model->get('identityProofNo'));
}
?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Identity Proof Image
                    echo form_label('Identity Proof Image:', 'identityProofImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'identityProofImage',
                            'name' => 'identityProofImage[]',
                            'multiple'=>true,
                            'class' => 'form-control',
                           // 'required' => 'required',
                            'value' => ($driver_personal_details_model->get('identityProofImage')) ? $driver_personal_details_model->get('identityProofImage') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('identityProofImage'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver PAN Card No
                    echo form_label('PAN Card No:', 'panCardNo', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'panCardNo',
                            'name' => 'panCardNo',
                            'class' => 'form-control',
                            //'required' => 'required',
                            'pattern' => '[A-Z0-9]{1,20}',
                            'value' => ($driver_personal_details_model->get('panCardNo')) ? $driver_personal_details_model->get('panCardNo') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('panCardNo'));
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver PAN Card Image
                    echo form_label('PAN Card Image:', 'panCardImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'panCardImage',
                            'name' => 'panCardImage[]',
                            'multiple'=>true,
                            'class' => 'form-control',
                            //'required' => 'required',
                            'value' => ($driver_personal_details_model->get('panCardImage')) ? $driver_personal_details_model->get('panCardImage') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('panCardImage'));
                    }
                    ?>

                </div>
            </div>

            <div class="x_title">
                <h2>
                    Driver Register<small>Career Details</small>
                </h2>

                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver Qualification
echo form_label('Qualification:', 'driverQualification', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : ''
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'driverQualification',
        'name' => 'driverQualification',
        'class' => 'form-control',
        //'required' => 'required',
        'value' => ($driver_personal_details_model->get('driverQualification')) ? $driver_personal_details_model->get('driverQualification') : ''
    ));
} else {
    echo text($driver_personal_details_model->get('driverQualification'));
}
?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver driverExperience
                    echo form_label('Experience:', 'driverExperience', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'driverExperience',
                            'name' => 'driverExperience',
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => ($driver_model->get('driverExperience')) ? $driver_model->get('driverExperience') : ''
                        ));
                    } else {
                        echo text($driver_model->get('driverExperience'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Driving License No
                    echo form_label('Driving License No:', 'drivingLicenseNo', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'drivingLicenseNo',
                            'name' => 'drivingLicenseNo',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[a-zA-Z0-9-\s]{1,15}',
                            'value' => ($driver_personal_details_model->get('drivingLicenseNo')) ? $driver_personal_details_model->get('drivingLicenseNo') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('drivingLicenseNo'));
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Driving License ExpireDate
                    echo form_label('Driving License ExpireDate:', 'drivingLicenseExpireDate', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'drivingLicenseExpireDate',
                            'name' => 'drivingLicenseExpireDate',
                            'class' => 'form-control',
                            'autocomplete' => 'off',
                            'required' => 'required',
                            'value' => ($driver_personal_details_model->get('drivingLicenseExpireDate')) ? $driver_personal_details_model->get('drivingLicenseExpireDate') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('drivingLicenseExpireDate'));
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Driving License Image
                    echo form_label('Driving License Image:', 'drivingLicenseImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'drivingLicenseImage',
                            'name' => 'drivingLicenseImage[]',
                            'multiple'=>true,
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => ($driver_personal_details_model->get('drivingLicenseImage')) ? $driver_personal_details_model->get('drivingLicenseImage') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('drivingLicenseImage'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver skills
                    echo form_label('Transmission Type (Skills):', 'skills', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('skills', $transmission_type_list, $driver_model->get('skills'), array(
                            'id' => 'skills',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                        /*echo form_input(array(
                            'id' => 'skills',
                            'name' => 'skills',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[a-zA-Z\s]{1,200}',
                            'value' => ($driver_model->get('skills')) ? $driver_model->get('skills') : ''
                        ));*/
                    } else {
                        echo text($transmission_type_list[$driver_model->get('skills')]);
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver favouriteCar
                    echo form_label('Favourite Car:', 'favouriteCar', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'favouriteCar',
                            'name' => 'favouriteCar',
                            'class' => 'form-control',
                            //'required' => 'required',
                           // 'pattern' => '[a-zA-Z0-9\s]{1,100}',
                            'value' => ($driver_model->get('favouriteCar')) ? $driver_model->get('favouriteCar') : ''
                        ));
                    } else {
                        echo text($driver_model->get('favouriteCar'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Company Name
                    echo form_label('Bank Name:', 'bankName', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
					if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'bankName',
                            'name' => 'bankName',
                            'class' => 'form-control',
                            'required' => 'required',
                        	'pattern' => '[a-zA-Z0-9\s]{3,30}',
                            'value' => ($driver_personal_details_model->get('bankName')) ? $driver_personal_details_model->get('bankName') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('bankName'));
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver City
                    echo form_label('Bank Branch:', 'bankBranch', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
					if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'bankBranch',
                            'name' => 'bankBranch',
                            'class' => 'form-control',
                            'required' => 'required',
                        	'pattern' => '[a-zA-Z0-9\s]{3,15}',
                            'value' => ($driver_personal_details_model->get('bankBranch')) ? $driver_personal_details_model->get('bankBranch') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('bankBranch'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Company Name
                    echo form_label('Account no:', 'bankAccountNo', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
					if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'bankAccountNo',
                            'name' => 'bankAccountNo',
                            'class' => 'form-control',
                            'autocomplete' => 'off',
                            'required' => 'required',
                        	'pattern' => '\d{7,15}\.\d+',
                            'value' => ($driver_personal_details_model->get('bankAccountNo')) ? $driver_personal_details_model->get('bankAccountNo') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('bankAccountNo'));
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver City
                    echo form_label('IFSC Code:', 'bankIfscCode', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
					if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'bankIfscCode',
                            'name' => 'bankIfscCode',
                            'class' => 'form-control',
                            'autocomplete' => 'off',
                            'required' => 'required',
                        	'pattern' => '[A-Z]{4}[0-9]{7}',
                            'value' => ($driver_personal_details_model->get('bankIfscCode')) ? $driver_personal_details_model->get('bankIfscCode') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('bankIfscCode'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Company Name
                    echo form_label('Entity Name:', 'companyId', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('companyId', $company_list, ($driver_model->get('companyId'))?$driver_model->get('companyId'):DEFAULT_COMPANY_ID, array(
                            'id' => 'companyId',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                    } else {
                        echo text($company_list[$driver_model->get('companyId')]);
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver City
                    echo form_label('City Name:', 'cityId', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('cityId', $city_list, $driver_model->get('cityId'), array(
                            'id' => 'cityId',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                    } else {
                        echo text($city_list[$driver_model->get('cityId')]);
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver driver type
                    echo form_label('Driver Type:', 'driverType', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('driverType', $driver_type_list, $driver_model->get('driverType'), array(
                            'id' => 'driverType',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                    } else {
                        echo text($driver_type_list[$driver_model->get('driverType')]);
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback hidden" id="fixedTripEarning">
                <?php
                    // validation for driver driver type
                    echo form_label('Fixed Trip Earning:', 'fixedTripEarning', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
					if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'fixedTripEarning',
                            'name' => 'fixedTripEarning',
                            'class' => 'form-control',
                           // 'required' => 'required',
                          //  'pattern' => '[0-9]+([\.,][0-9]+)?',
                            'value' => ($driver_model->get('fixedTripEarning')) ? $driver_model->get('fixedTripEarning') : ''
                        ));
                    }  else {
                        echo text($driver_model->get('fixedTripEarning'));
                    }
                    ?>
                
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback hidden" id="commissionTripEarning">
                <?php
                    // validation for driver driver type
                    echo form_label('Commission Trip Earning:', 'commissionTripEarning', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
					if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'commissionTripEarning',
                            'name' => 'commissionTripEarning',
                            'class' => 'form-control',
                           // 'required' => 'required',
                           // 'pattern' => '[0-9]+([\.,][0-9]+)?',
                            'value' => ($driver_model->get('commissionTripEarning')) ? $driver_model->get('commissionTripEarning') : ''
                        ));
                    }  else {
                        echo text($driver_model->get('commissionTripEarning'));
                    }
                    ?>
                
                </div>
            </div>
            <div class="ln_solid"></div>
            <div <?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                    <button type="button" id="load_driver_btn"
                            class="btn btn-primary" >Cancel</button>
                    <button type="button" id="save_driver_btn" class="btn btn-success" onclick="saveDriver();" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing">
                        Submit
                    </button>
                     <?php  if($this->User_Access_Model->deleteDriver() && $driver_model->get('id') > 0) : ?>
                     <button type="button" id="delete_driver_btn" class="btn btn-primary" onclick="deleteDriver();">Delete</button>
                     <?php endif ; ?>
                </div>
            </div>
            <?php  if($view_mode == VIEW_MODE) : ?>
            <div  class="form-group">
                <center><button type="button" id="load_driver_btn" class="btn btn-primary" >Back</button></center>
            </div>  
            <?php endif ; ?>
            
            
            
                    <?php
                    echo form_close();
                    ?>
        </div>
    </div>
</div>

