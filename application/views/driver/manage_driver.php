<?php

	// created by Aditya on 23rd May 2017
	/*** Aditya Code Section Start Here ***/
	$verifiedDriverList_HTML = '';
	$nonVerifiedDriverList_HTML = '';

	foreach ($driver_model_list as $list)
	{
		$tmpList_HTML = '';
		$tmpList_HTML .= '<tr>';

		// $tmpList_HTML .= '<td class="'.$list->loginStatus.''.$list->shiftStatus.''.$list->availabilityStatus.'"></td>';
		
		if ($list->status==Status_Type_Enum::ACTIVE) {
			$active = '<a id="active-'.$list->id.'" href="javascript:void(0);" onclick="changeDriverStatus(this.id);"><i class="fa fa-check-circle" aria-hidden="true"></i></a>';
		}elseif ($list->status==Status_Type_Enum::INACTIVE) {
			$active = '<a id="deactive-'.$list->id.'" href="javascript:void(0);" onclick="changeDriverStatus(this.id);"><i class="fa fa-ban" aria-hidden="true"></i></a>';
		}
		$history = '<a href="downloadSessionHistory/'.$list->id.'" title="Download Session History" class="userlink">' . '<i class="fa fa-download" aria-hidden="true"></i></span></a>';
		if ($list->isVerified == Status_Type_Enum::ACTIVE) {
		$verify = '<button class="btn btn-xs btn-warning" onclick="changeDriverVerificationStatus2('.$list->id.', 0);" title="Switch to Non-Verified"><i class="fa fa-check-circle" aria-hidden="true"></i></button>';
		} else {
			$verify = '<button class="btn btn-xs btn-primary" onclick="changeDriverVerificationStatus2('.$list->id.', 1);" title="Switch to Verified"><i class="fa fa-times-circle" aria-hidden="true"></i></button>';
		}
		$tmpList_HTML .= '<td>'. '<a href="javascript:void(0);" id="editDriver-'.$list->id.'" onclick="editDriver(this.id);" class="userlink">'
										. '<i class="fa fa-pencil" aria-hidden="true"></i></a>'.'/ <a href="downloadEarningHistory/'.$list->id.'" title="Download Earning History" class="userlink">'
										. '<span class="glyphicon glyphicon-download"></span></a> /'
												.$verify.'/'.$history.'/'.$active.'</td>';
		
		/***Aditya Code Start***/	
		$dutyString = 'Off Duty<br/><button type="button" class="btn btn-success" onclick="changeDriverDutyStatus2(this, '.$list->id.', 1)">Turn On Duty</button>';
		if ($list->shiftStatus == 'IN')
		{
			$dutyString = 'On Duty<br/><button type="button" class="btn btn-danger" onclick="changeDriverDutyStatus2(this,'.$list->id.', 0)">Turn Off Duty</button>';
		}
		
		// $tmpList_HTML .= '<td>'.$dutyString.'</td>';
		/***Aditya Code End***/
		
		if($list->licenseType == 'TR') {
			$licence = "TRANSPORT";
		} elseif ($list->licenseType == 'NT') {
			$licence = 'NON TRANSPORT';
		}else {
           $licence = "TRANSPORT";
		}
        if ($list->driverSkill =='NL') {
            $list->driverSkill = 'HA, SE, SU';
        }

		if ($list->image == '') {
			$profile = '';
		}else if ($list->image != '') {

            if (strpos($list->image, 'driver_docs') !== false) {
                $profile = '<img src="https://s3-ap-southeast-1.amazonaws.com/zuverlive/'.$list->image.'" alt="Profile Image" width="80" height="80">';
            } else {
                $profile = '<img width="70" src="'.driver_content_url($list->id.'/'.$list->image).'" height="80"/>';
            }
        }

		$tmpList_HTML .= '<td><a href="javascript:void(0);" id="viewDriver-'.$list->id.'" onclick="viewDriver(this.id);">'.$list->id.'</a></td>';
		$tmpList_HTML .= '<td>'.$list->joiningDate.'</td>';
		$tmpList_HTML .= '<td>'.$list->JoinedDate.'</td>';
		$tmpList_HTML .= '<td>'.$profile.'</td>';
		$tmpList_HTML .= '<td>'.$list->firstName.' '.$list->lastName.'</td>';
		//echo '<td>'.$list->email.'</td>';
		$tmpList_HTML .= '<td>'.$list->mobile.'</td>';
		$tmpList_HTML .= '<td>'.$list->skills.'</td>';
		$tmpList_HTML .= '<td>'.$list->driverSkill.'</td>';
		$tmpList_HTML .= '<td>'.$licence.'</td>';
		$tmpList_HTML .= '<td>'.$dutyString.'</td>';

		//echo '<td>'.$list->driverLanguagesKnown.'</td>';
		//echo '<td>'.$list->driverExperience.'</td>';
		//echo '<td>'.$list->skills.'</td>';
		// $tmpList_HTML .= '<td style="width:10%">'.$list->doj.'</td>';
		// $tmpList_HTML .= '<td style="width:7%">'.$list->totalTripComplete.'</td>';
		// $tmpList_HTML .= '<td style="width:7%">'.round($list->rating,1).'</td>';
		// $tmpList_HTML .= '<td style="width:7%">'.round($list->driverEarning).'</td>';
		// $tmpList_HTML .= '<td style="width:7%">'.$list->companyName.'</td>';
		// $verification_status=($list->isVerified==Status_Type_Enum::ACTIVE)?"Verified":"Not Verified";
		//$tmpList_HTML .= '<td style="width:7%">'.$verification_status.'</td>';
		// $tmpList_HTML .= '<td> <ul class="list-inline"> <li>';
		// $tmpList_HTML .= '<div class="btn-group btn-toggle">';
		
		//$tmpList_HTML .= '<button class="btn btn-xs btn-primary active">Verified</button>';
		//$tmpList_HTML .= '<button class="btn btn-xs btn-default">Not Verified</button>';
		// $tmpList_HTML .= '</div></li></ul></td>';
		//echo '<td>'.($list['isVerified'])?'Verified':'Not Verified'.'</td>';
		//Status toogle
		// $active=($list->status==Status_Type_Enum::ACTIVE)?'btn-primary active-status':'btn-default" onclick="changeDriverStatus(this.id);';
		// $deactive=($list->status==Status_Type_Enum::INACTIVE)?'btn-primary active-status':'btn-default" onclick="changeDriverStatus(this.id);';
		// $tmpList_HTML .= '<td style="width:15%"> <ul class="list-inline"> <li>';
		// $tmpList_HTML .= '<div class="btn-group btn-toggle">';
		// $tmpList_HTML .= '<button id="active-'.$list->id.'" class="btn btn-xs '.$active.'">Unblock</button>';
		// $tmpList_HTML .= '<button id="deactive-'.$list->id.'" class="btn btn-xs '.$deactive.'">Block</button>';
		// $tmpList_HTML .= '</div></li></ul></td>';
		
		$tmpList_HTML .= '</tr>';
		if ( $list->isVerified == Status_Type_Enum::ACTIVE ) {
			$verifiedDriverList_HTML .= $tmpList_HTML;
		} else {
			$nonVerifiedDriverList_HTML .= $tmpList_HTML;
		}
	}
	/*** Aditya Code Section End Here ***/
?>
<div id="driver-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>
				Driver's List
			</h2>
			<!--<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				 <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li>

			</ul>
			<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" id="search_driver_content" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="search_driver_btn" type="button">Go!</button>
                    </span>

                  </div>
                  <div><span>Search by Referral Code, Driver Full Name, Email and Mobile</span></div>
                </div>
             </div>-->
             <div class="title_right pull-right">

                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<a href="<?php echo base_url('driver/add')?>"><button id="add_driver_btn"
						class="btn btn-primary">Add Driver</button></a>
				</div>
             </div>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			<!-- Testing Design Code Start  -->
			<div class="x_content" style="display: block;">


			  <div class="" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				  <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Non-Verified Drivers</a>
				  </li>
				  <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Verified Drivers</a>
				  </li>
				</ul>
				<div id="myTabContent" class="tab-content">
				  <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
					  <table id="datatable-checkbox"
						class="table table-striped table-bordered datatable-button-init-collection">
						<thead>
							<tr>
								<!-- <th><input type="checkbox" id="check-all" class="flat"></th> -->
								<!-- <th>Sl no</th> -->
								<th>Action</th>
								
								<th>Id</th>
								<th>Joined On</th>
								<th>Signup Date</th>
								<th>Image</th>
								<th>Driver Name</th>
								<!-- <th>Email</th> -->
								<th>Mobile</th>
								<th>Trasmission</th>
								<th>Drives</th>
								<th>Licence</th>
								<!-- <th>Languages Known</th>
								<th>Total Experience</th>
								<th>Skills</th> -->
								<!-- <th>Joining Date</th> -->
								<!-- <th>Trips Completed</th> -->
								<!-- <th>Rating </th> -->
								<!-- <th>Driver Earning </th> -->
								<!-- <th>Entity Name</th> -->
								<!-- <th>Verification</th> -->
								<th>Status</th>
								<!-- <th>Sessions</th> -->
							</tr>
						</thead>


						<tbody>
						<?php
							echo $nonVerifiedDriverList_HTML;
						?>
						</tbody>
					</table>
				  </div>
				  <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
					  <table id="datatable-checkbox"
  						class="table table-striped table-bordered datatable-button-init-collection">
  						<thead>
  							<tr>
								<!-- <th><input type="checkbox" id="check-all" class="flat"></th> -->
								<!-- <th>Sl no</th> -->
								<th>Action</th>
								<th>Id</th>
                                <th>Joined On</th>
								<th>Signup Date</th>
								<th>Image</th>
								<th>Driver Name</th>
								<!-- <th>Email</th> -->
								<th>Mobile</th>
								<th>Trasmission</th>
								<th>Drives</th>
								<th>Licence</th>
								<!-- <th>Joining Date</th> -->
								<!-- <th>Trips Completed</th> -->
								<!-- <th>Rating </th> -->
								<!-- <th>Driver Earning </th> -->
								<!-- <th>Entity Name</th> -->
								<!-- <th>Verification</th> -->
								<th>Status</th>
								<!-- <th>Sessions</th> -->
							</tr>
  						</thead>


  						<tbody>
  						<?php
  						//sl no intialization
  						//$i=1;
  						/* $driver_model_list=array(
  								0=>array('firstName'=>'Kiran','lastName'=>'Kumar','driverCode'=>'Kiran001','email'=>'global@gmail.com','mobile'=>'9901206631','status'=>1,'skills'=>'Automatic','driverLanguagesKnown'=>'English, Kannada','driverExperience'=>'3 Years','isVerified'=>1,'companyId'=>'ZOOMCAR','doj'=>'20-09-2016','totalTripComplete'=>12,'rating'=>4.5),
  								1=>array('firstName'=>'Kiran','lastName'=>'Kumar','driverCode'=>'Kiran001','email'=>'global@gmail.com','mobile'=>'9901206631','status'=>1,'skills'=>'Manual','driverLanguagesKnown'=>'English, Kannada','driverExperience'=>'5 Years','isVerified'=>0,'companyId'=>'Zuver','doj'=>'20-09-2016','totalTripComplete'=>12,'rating'=>3.8)
  						        ); */

						// commented by Aditya on May 23 2017
  				// 		foreach ($driver_model_list as $list)
  				// 		{
						//
  				// 		echo '<tr>';
						//
  				// 		echo '<td class="'.$list->loginStatus.''.$list->shiftStatus.''.$list->availabilityStatus.'"></td>';//<input type="checkbox" class="flat" name="table_records">
  				// 		//echo '<th>'.$i.'</th>';
  				// 		echo '<td style="width:8%">'
  				// 				. '<a href="javascript:void(0);" id="viewDriver-'.$list->id.'" onclick="viewDriver(this.id);" class="userlink">'
  				// 						. '<span class="glyphicon glyphicon-eye-open"></span></a> '
  				// 								. '/ <a href="javascript:void(0);" id="editDriver-'.$list->id.'" onclick="editDriver(this.id);" class="userlink">'
  				// 										. '<span class="glyphicon glyphicon-edit"></span></a>'.'/ <a href="downloadEarningHistory/'.$list->id.'" title="Download Earning History" class="userlink">'
  				// 										. '<span class="glyphicon glyphicon-download"></span></a>'
  				// 												. '</td>';
  				// 		/***Aditya Code Start***/
  				// 		$dutyString = 'Off Duty<br/><button type="button" class="btn btn-success" onclick="changeDriverDutyStatus2(this, '.$list->id.', 1)">Turn On Duty</button';
  				// 		if ($list->shiftStatus == 'IN')
  				// 		{
  				// 			$dutyString = 'On Duty<br/><button type="button" class="btn btn-danger" onclick="changeDriverDutyStatus2(this, '.$list->id.', 0)">Turn Off Duty';
  				// 		}
  				// 		echo '<td>'.$dutyString.'</td>';
  				// 		/***Aditya Code End***/
  				// 		echo '<td style="width:10%">'.$list->driverCode.'</td>';
  				// 		echo '<td style="width:12%">'.$list->firstName.' '.$list->lastName.'</td>';
  				// 		//echo '<td>'.$list->email.'</td>';
  				// 		echo '<td style="width:10%">'.$list->mobile.'</td>';
						//
  				// 		//echo '<td>'.$list->driverLanguagesKnown.'</td>';
  				// 		//echo '<td>'.$list->driverExperience.'</td>';
  				// 		//echo '<td>'.$list->skills.'</td>';
  				// 		echo '<td style="width:10%">'.$list->doj.'</td>';
  				// 		echo '<td style="width:7%">'.$list->totalTripComplete.'</td>';
  				// 		echo '<td style="width:7%">'.round($list->rating,1).'</td>';
  				// 		echo '<td style="width:7%">'.round($list->driverEarning).'</td>';
  				// 		echo '<td style="width:7%">'.$list->companyName.'</td>';
  				// 		$verification_status=($list->isVerified==Status_Type_Enum::ACTIVE)?"Verified":"Not Verified";
  				// 		echo '<td style="width:7%">'.$verification_status.'</td>';
  				// 		/* echo '<td> <ul class="list-inline"> <li>';
  				// 		echo '<div class="btn-group btn-toggle">';
  				// 		echo '<button class="btn btn-xs btn-primary active">Verified</button>';
  				// 		echo '<button class="btn btn-xs btn-default">Not Verified</button>';
  				// 		echo '</div></li></ul></td>'; */
  				// 		//echo '<td>'.($list['isVerified'])?'Verified':'Not Verified'.'</td>';
  				// 		//Status toogle
  				// 		$active=($list->status==Status_Type_Enum::ACTIVE)?'btn-primary active-status':'btn-default" onclick="changeDriverStatus(this.id);';
  				// 		$deactive=($list->status==Status_Type_Enum::INACTIVE)?'btn-primary active-status':'btn-default" onclick="changeDriverStatus(this.id);';
  				// 		echo '<td style="width:15%"> <ul class="list-inline"> <li>';
  				// 		echo '<div class="btn-group btn-toggle">';
  				// 		echo '<button id="active-'.$list->id.'" class="btn btn-xs '.$active.'">Unblock</button>';
  				// 		echo '<button id="deactive-'.$list->id.'" class="btn btn-xs '.$deactive.'">Block</button>';
  				// 		echo '</div></li></ul></td>';
  				// 		echo '<td><a href="downloadSessionHistory/'.$list->id.'" title="Download Session History" class="userlink">'
  				// 										. '<span class="glyphicon glyphicon-download"></span></a></td>';
						//
  				// 		echo '</tr>';
  				// 		//$i++;
  				// 		}
							echo $verifiedDriverList_HTML;
  			            ?>
  						</tbody>
  					</table>
				  </div>
				</div>
			  </div>

			</div>
			<!-- Testing Design Code End -->


		</div>
	</div>
</div>
