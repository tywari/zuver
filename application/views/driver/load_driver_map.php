<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Complex icons</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        width: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>

   <!--<script src="./public/js/vendors/jquery/dist/jquery.min.js"></script>-->
   <script src="<?php echo js_url('vendors/jquery/dist/jquery.min.js'); ?>"></script>
  </head>
  <body>
      <input type='hidden' id='city_id' name='city_id'  value='<?php echo $filter_cityid;?> ' >
      <span style='margin-left:200px;'>
        Awaiting Trip &nbsp;
        <img src="<?php echo $this->config->item('base_url'); ?>/public/images/app/driver_on.png" alt="Free" style="width: 20px;" />
        <span id='free_count_sec'> </span>
      </span>
      <span style='margin-left:50px;'>
        Off Duty &nbsp;
        <img src="<?php echo $this->config->item('base_url'); ?>/public/images/app/driver_off.png" alt="Free" style="width: 20px;"/>
        <span id='yellow_count_sec'> </span>
      </span>
      <span style='margin-left:50px;'>
        Logged Out &nbsp;
        <img src="<?php echo $this->config->item('base_url'); ?>/public/images/app/driver_logged_in.png" alt="Not available" style="width: 20px;"/>
        <span id='na_count_sec'> </span>
      </span>
      <span style='margin-left:50px;'>
        On Trip &nbsp;
        <span id='busy_count_sec'> </span>
      </span>
    <div id="map" style="height:800px;width:100%;margin:0 0;padding:0 0;"> </div>
    <script>
      BASE_URL = "<?php echo base_url() ?>";




      // The following example creates complex markers to indicate beaches near
      // Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
      // to the base of the flagpole.
      function initMap() {
        var city_id = $('#city_id').val();
        var ct_lat; var ct_lon;
        if(city_id==1){
            ct_lat = 19.075983;
            ct_lon = 72.877655;
        }else if(city_id==2){
            ct_lat = 12.942117;
            ct_lon = 77.575363;
        }else if(city_id==3){
            ct_lat = 18.520430;
            ct_lon = 73.856743;
        }else if(city_id==4){
            ct_lat = 28.7041;
            ct_lon = 77.1025;
        }else{
           ct_lat = 12.970029;
           ct_lon = 77.572417;
        }

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: {lat: ct_lat, lng: ct_lon}
        });

        //get Driver Latitude and Longitude

        var surl = BASE_URL + 'driver/getCurrentDriversForMapView';
        console.log(surl);

        $.ajax({
            method: "POST",
            url: surl,
            data:{'city_id':city_id},
            async: false
        }).done(function (response) {

           var data = $.parseJSON(response);
           /*var free_count           = data.free_count;
           var notavailable_count   = data.notavailable_count;
           var busy_count           = data.busy_count*/
           var on_trip = 0;
           var green_count = 0;
           var yellow_count = 0;
           var red_count = 0;

           if(data.length!=0){
              $.each(data, function(i, item) {


                  if (
                      (item.loginStatus == 'Y') &&
                      (item.shiftStatus == 'IN')
                      )
                  {
                        if (item.availabilityStatus=='B') {
                          //Means this driver is On a Trip right now, one level above Green so no display
                          on_trip++;
                          return true;
                        }
                        else if (item.availabilityStatus=='F')
                        {
                            //Means this driver is Not On Trip but is ready to take trips, Awaiting Trip, Green Level
                            green_count++;
                            var image = {
                               url: BASE_URL+'/public/images/app/driver_on.png',
                               // This marker is 20 pixels wide by 32 pixels high.
                               size: new google.maps.Size(32, 32),
                               // The origin for this image is (0, 0).
                               origin: new google.maps.Point(0, 0),
                               // The anchor for this image is the base of the flagpole at (0, 32).
                               anchor: new google.maps.Point(0, 32)
                            };
                        }
                  }
                  else if (
                      (item.loginStatus == 'Y') &&
                      (item.shiftStatus == 'OUT')
                      )
                  {
                        //This means that Driver as of now is Off Duty, Yellow Level, not interested in taking trips
                        yellow_count++;
                        var image = {
                           url: BASE_URL+'/public/images/app/driver_off.png',
                           // This marker is 20 pixels wide by 32 pixels high.
                           size: new google.maps.Size(32, 32),
                           // The origin for this image is (0, 0).
                           origin: new google.maps.Point(0, 0),
                           // The anchor for this image is the base of the flagpole at (0, 32).
                           anchor: new google.maps.Point(0, 32)
                        };
                  }
                  else {
                      red_count++;
                      return true;
                  }

                // Shapes define the clickable region of the icon. The type defines an HTML
                // <area> element 'poly' which traces out a polygon as a series of X,Y points.
                // The final coordinate closes the poly by connecting to the first coordinate.
                var shape = {
                  coords: [1, 1, 1, 20, 18, 20, 18, 1],
                  type: 'poly'
                };
                var lt;
                var lnt;
                        if ((item.latitude != null) && (item.longitude != null)) {
                          lt  = parseFloat(item.latitude);
                          lnt = parseFloat(item.longitude);
                        } else {
                          lt  = parseFloat(item.currentLatitude);
                          lnt = parseFloat(item.currentLongitude);
                        }

                        var log_status=(item.loginStatus == 'Y')?'Active':'Inactive';
                        var marker = new google.maps.Marker({
                                    position: {lat: lt, lng: lnt},
                                    map: map,
                                    icon: image,
                                    shape: shape,
                                    title: 'Name:'+item.driver_name+'  Mobile:'+item.mobile+'  Log:'+log_status+'  Shift:'+item.shiftStatus,
                                    zIndex: 2
                                  });
                    });
                }



          $('#free_count_sec').html(' : '+green_count);
          //$('#na_count_sec').html(' : '+notavailable_count);
          $('#busy_count_sec').html(' : '+on_trip);
          //$('#busy_count_sec').html(' : '+busy_count);
          $('#yellow_count_sec').html(' : '+yellow_count);
          $('#na_count_sec').html(' : '+red_count);

        });
        //setMarkers(map);

  }// end of init Map




      // Data for the markers consisting of a name, a LatLng and a zIndex for the
      // order in which these markers should display on top of each other.
//       setTimeout(function(){
//        var tval = $('#driver_detils').val();
//        var beaches = [tval];
//        alert(beaches);
//      }, 1000);


     //alert(beaches);
     //setTimeout(function(){
      function setMarkers(map) {
        // Adds markers to the map.

        // Marker sizes are expressed as a Size of X,Y where the origin of the image
        // (0,0) is located in the top left of the image.

        // Origins, anchor positions and coordinates of the marker increase in the X
        // direction to the right and in the Y direction down.



      }

       // }, 2000);
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcN2hpqU2MY00KFk3jVtrsLJvrucZBr6I&callback=initMap">
    </script>

  </body>
</html>
