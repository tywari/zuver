<?php
$view_mode = $mode;
?>
<?php if ($this->session->flashdata('success-msg')) { ?>
    <p class="alert alert-success" role="alert"><?php echo $this->session->flashdata('success-msg'); ?></p>
<?php } ?>
<div id="driver-details-information"
     class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>
                Driver <?php if ($view_mode == EDIT_MODE) {
                    echo 'Register';
                } else {
                    echo 'Details';
                } ?>
                <small>Career Details</small>
            </h2>
            <?php if ($view_mode !== EDIT_MODE) { ?>
                <small style="float: right;"><!-- Button HTML (to Trigger Modal) -->
                    <a href="#myModal" role="button" data-toggle="modal">Change Password</a></small><?php } ?>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br/>
            <?php
            $form_attr = array(
                'name' => 'edit_driver_form',
                'id' => 'edit_driver_form',
                'class' => 'form-horizontal form-label-left',
                'data-parsley-validate' => '',
                'method' => 'POST',
                ''
            );
            echo form_open_multipart(base_url(''), $form_attr);

            // driver id by default is -1
            echo form_input(array(
                'type' => 'hidden',
                'id' => 'driver_id',
                'name' => 'id',
                'value' => ($driver_model->get('id')) ? $driver_model->get('id') : -1
            ));
            echo form_input(array(
                'type' => 'hidden',
                'id' => 'companyId',
                'name' => 'companyId',
                'value' => DEFAULT_COMPANY_ID
            ));
            echo form_input(array(
                'type' => 'hidden',
                'id' => 'driverSkill',
                'name' => 'driverSkill',
                'value' => ''
            ));
            echo form_input(array(
                'type' => 'hidden',
                'id' => 'working_days',
                'name' => 'working_days',
                'value' => ''
            ));
            ?>

            <?php
            if ($driver_model->get('driverCode') && $driver_model->get('lastLoggedIn')) {
                echo '<div class="form-group">';

                echo '<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">';
                $profile_image = ($driver_model->get("profileImage")) ? driver_content_url($driver_model->get("id") . '/' . $driver_model->get("profileImage")) : image_url('app/user.png');
                //https://s3-ap-southeast-1.amazonaws.com/zuverlive/driver_docs/PRF-742.jpg
                if ($driver_model->get("profileImage") != '') {
                    if (strpos($driver_model->get("profileImage"), 'driver_docs') !== false) {
                        echo '<img src="https://s3-ap-southeast-1.amazonaws.com/zuverlive/' . $driver_model->get("profileImage") . '" alt="Profile Image" width="150" height="150">';
                    } else {
                        echo '<img src="' . $profile_image . '" alt="Profile Image" width="150" height="150">';
                    }
                }

                echo '</div>';
                echo '<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">';
                echo form_label('Driver Code:', 'driverCode', array(
                    'class' => ''
                ));
                echo text($driver_model->get('driverCode'));

                echo '</div>';

                echo '<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">';
                echo form_label('Last Logged In:', 'lastLoggedIn', array(
                    'class' => ''
                ));
                echo text($driver_model->get('lastLoggedIn'));


                echo '</div>';
                echo '<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">';
                echo form_label('Joining Date:', 'joiningDate', array(
                    'class' => ''
                ));
                echo text($driver_model->get('doj'));


                echo '</div>';
                if ($view_mode !== EDIT_MODE) {
                    if ($this->User_Access_Model->deleteDriver() && $driver_model->get('id') > 0) {
                        echo '<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">';
                        echo '<div class="text-center">';
                        $labelString = 'Off Duty';
                        if ($shiftStatus == 'IN') {
                            $labelString = 'On Duty';
                        }

                        /*echo '
      <input class="" type="checkbox" '.$checkedString.' onchange="changeDriverDutyStatus(this)">'.$labelString.'

    <br/><br/><br/>';*/

                        // Shift In Button
                        echo '<div class="" data-toggle="buttons">';
                        $label_class = ($shiftStatus == 'IN') ? 'class="dutyStatus btn btn-success"' : 'class="dutyStatus btn btn-success active"';
                        echo '<button ' . $label_class . '>';
                        echo form_radio(array(
                            'id' => 'dutyStatus-Y',
                            'name' => 'dutyStatus',
                            //'class' => 'loginStatus',
                            'value' => 0,
                            'checked' => ($shiftStatus == 'IN') ? 'checked' : ''
                        ));
                        echo '<i class="fa fa-thumbs-o-up"></i>On Duty</button>';

                        // Shift Out Button
                        $label_class = ($shiftStatus == 'OUT') ? 'class="dutyStatus btn btn-danger"' : 'class="dutyStatus btn btn-danger active"';
                        echo '<button ' . $label_class . '>';
                        echo form_radio(array(
                            'id' => 'dutyStatus-N',
                            'name' => 'dutyStatus',
                            //'class' => 'loginStatus',
                            'value' => 1,
                            ($shiftStatus == 'OUT') ? 'checked' : ''
                        ));
                        echo '<i class="fa fa-thumbs-o-down"></i>Off Duty</button>';
                        echo '</div>';


                        // Login Button
                        echo '<div class="" data-toggle="buttons">';
                        $label_class = ($driver_model->get('loginStatus') == Status_Type_Enum::ACTIVE) ? 'class="loginStatus btn btn-sm btn-success"' : 'class="btn btn-sm btn-success active"';
                        echo '<button ' . $label_class . '>';
                        echo form_radio(array(
                            'id' => 'loginStatus-Y',
                            'name' => 'loginStatus',
                            //'class' => 'loginStatus',
                            'value' => 0,
                            'checked' => ($driver_model->get('loginStatus') == Status_Type_Enum::ACTIVE) ? 'checked' : ''
                        ));
                        echo '<i class="fa fa-thumbs-o-up"></i>Logged In</button>';


                        // Logout Button
                        $label_class = ($driver_model->get('loginStatus') == Status_Type_Enum::INACTIVE) ? 'class="loginStatus btn btn-sm btn-danger"' : 'class="btn btn-sm btn-danger active"';
                        echo '<button ' . $label_class . '>';
                        echo form_radio(array(
                            'id' => 'loginStatus-N',
                            'name' => 'loginStatus',
                            //'class' => 'loginStatus',
                            'value' => 1,
                            'checked' => ($driver_model->get('loginStatus') == Status_Type_Enum::INACTIVE) ? 'checked' : ''
                        ));
                        echo '<i class="fa fa-thumbs-o-down"></i>Logged Out</button>';
                        echo '</div>';


                        echo '<div class="" data-toggle="buttons">';
                        $label_class = ($driver_model->get('isVerified') == Status_Type_Enum::ACTIVE) ? 'class="isVerified btn btn-sm btn-sm btn-success text-success"' : 'class="btn btn-sm btn-sm btn-success text-success active"';
                        echo '<button ' . $label_class . '>';
                        echo form_radio(array(
                            'id' => 'isVerified-Y',
                            'name' => 'isVerified',
                            //'class' => 'isVerified',
                            'value' => 0,
                            'checked' => ($driver_model->get('isVerified') == Status_Type_Enum::INACTIVE) ? 'checked' : ''
                        ));
                        echo '<i class="fa fa-check-circle-o fa-3x animated fadeIn"></i><br />Verified</button>';
                        $label_class = ($driver_model->get('isVerified') == Status_Type_Enum::INACTIVE) ? 'class="isVerified btn btn-sm btn-sm btn-default text-default"' : 'class="btn btn-sm btn-sm btn-default text-default active"';
                        echo '<button ' . $label_class . '>';
                        echo form_radio(array(
                            'id' => 'isVerified-N',
                            'name' => 'isVerified',
                            //'class' => 'isVerified',
                            'value' => 1,
                            'checked' => ($driver_model->get('isVerified') == Status_Type_Enum::INACTIVE) ? 'checked' : ''
                        ));
                        echo '<i class="fa fa-circle-o fa-3x"></i><br/>Not Verified</button>';
                        echo '</div></div></div>';
                    }
                }
                echo '</div>';
            }
            ?>

            <!-- created by Aditya on May 25 2017 -->
            <legend>Driver Tentative Work Preference Details</legend>
            <div class="form-group">
                <!-- Work Days -->
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver City
                    echo form_label('Driver Working Day:', 'working_days', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        // created by Aditya on May 24 2017
                        echo '
                                <select name="driverWorkDays2" id="driverWorkDays2" class="select2_multiple form-control" multiple="multiple" required="required">
                                    <option value="MO">Monday</option>
                                    <option value="TU">Tuesday</option>
                                    <option value="WE">Wednesday</option>
                                    <option value="TH">Thursday</option>
                                    <option value="FR">Friday</option>
                                    <option value="SA">Saturday</option>
                                    <option value="SU">Sunday</option>
                                </select>
                                ';
                        echo '<script type="text/javascript">
                                    $( document ).ready(function() {
                                        var strWorkDays = "' . $driver_model->get('working_days') . '";
                                        $.each(strWorkDays.split(","), function(i,e) {
                                            $("#driverWorkDays2 option[value=\'" + e + "\']").prop("selected", true);
                                        });
                                    });
                                </script>';
                    } else {
                        // original code commented by Aditya on May 24 2017
                        // echo text ( $driver_skill_list [$driver_model->get ( 'driverSkill' )] );
                        $str = $driver_model->get('working_days');
                        $skillsArray = explode(",", $str);
                        $outString = '';
                        foreach ($skillsArray as $key => $value) {
                            if ($value == 'MO') {
                                $outString[] = 'Monday';
                            }
                            if ($value == 'TU') {
                                $outString[] = 'Tuesday';
                            }
                            if ($value == 'WE') {
                                $outString[] = 'Wednesday';
                            }
                            if ($value == 'TH') {
                                $outString[] = 'Thursday';
                            }
                            if ($value == 'FR') {
                                $outString[] = 'Friday';
                            }
                            if ($value == 'SA') {
                                $outString[] = 'Saturday';
                            }
                            if ($value == 'SU') {
                                $outString[] = 'Sunday';
                            }
                            if ($value == '') {
                                $outString[] = 'None';
                            }
                        }
                        echo text(implode(", ", $outString));
                    }
                    ?>

                </div>
                <!-- Work Days end -->

                <!-- Hours Per Day -->
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('Hours Per Day:', 'hours_per_day', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        // echo form_input ( array (
                        // 		'id' => 'hours_per_day',
                        // 		'name' => 'hours_per_day',
                        // 		'class' => 'form-control',
                        // 		'required' => 'required',
                        // 		'value' => ($driver_model->get ( 'hours_per_day' )) ? $driver_model->get ( 'hours_per_day' ) : ''
                        // ) );
                        echo '
                            <select name="hours_per_day" id="hours_per_day" class="form-control" required="required">
                                <option value="1">1 Hrs</option>
                                <option value="2">2 Hrs</option>
                                <option value="3">3 Hrs</option>
                                <option value="4">4 Hrs</option>
                                <option value="5">5 Hrs</option>
                                <option value="6">6 Hrs</option>
                                <option value="7">7 Hrs</option>
                                <option value="8">8 Hrs</option>
                                <option value="9">9 Hrs</option>
                                <option value="10">10 Hrs</option>
                                <option value="11">11 Hrs</option>
                                <option value="12">12 Hrs</option>
                            </select>
                            ';
                        echo '<script type="text/javascript">
                                    $( document ).ready(function() {
                                        $("#hours_per_day").val(' . $driver_model->get('hours_per_day') . ');
                                    });
                                </script>';
                    } else {
                        echo text($driver_model->get('hours_per_day') . ' Hrs');
                    }
                    ?>

                </div>
                <!-- Hours Per Day end -->

                <!-- Duty Start Time Per Day -->
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('Duty Start Time Per Day:', 'duty_start_time', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {

                        $dutyStartTime12 = '';
                        if ($driver_model->get('duty_start_time') != '') {
                            $dutyStartTime24 = $driver_model->get('duty_start_time');
                            $dutyStartTime12 = date("g:i A", strtotime($dutyStartTime24));
                        }


                        echo form_input(array(
                            'id' => 'duty_start_time',
                            'name' => 'duty_start_time',
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => $dutyStartTime12
                        ));
                        // echo '
                        // <select name="duty_start_time" id="duty_start_time" class="form-control" required="required">
                        //     <option value="12:00 AM">12:00 AM</option>
                        //     <option value="12:30 AM">12:30 AM</option>
                        //     <option value="01:00 AM">01:00 AM</option>
                        //     <option value="01:30 AM">01:30 AM</option>
                        //     <option value="02:00 AM">02:00 AM</option>
                        //     <option value="02:30 AM">02:30 AM</option>
                        //     <option value="03:00 AM">03:00 AM</option>
                        //     <option value="03:30 AM">03:30 AM</option>
                        //     <option value="04:00 AM">04:00 AM</option>
                        //     <option value="04:30 AM">04:30 AM</option>
                        //     <option value="05:00 AM">05:00 AM</option>
                        //     <option value="05:30 AM">05:30 AM</option>
                        //     <option value="06:00 AM">06:00 AM</option>
                        //     <option value="06:30 AM">06:30 AM</option>
                        //     <option value="07:00 AM">07:00 AM</option>
                        //     <option value="07:30 AM">07:30 AM</option>
                        //     <option value="08:00 AM">08:00 AM</option>
                        //     <option value="08:30 AM">08:30 AM</option>
                        //     <option value="09:00 AM">09:00 AM</option>
                        //     <option value="09:30 AM">09:30 AM</option>
                        //     <option value="10:00 AM">10:00 AM</option>
                        //     <option value="10:30 AM">10:30 AM</option>
                        //     <option value="11:00 AM">11:00 AM</option>
                        //     <option value="11:30 AM">11:30 AM</option>
                        //     <option value="12:00 PM">12:00 PM</option>
                        //     <option value="12:30 PM">12:30 PM</option>
                        //     <option value="01:00 PM">01:00 PM</option>
                        //     <option value="01:30 PM">01:30 PM</option>
                        //     <option value="02:00 PM">02:00 PM</option>
                        //     <option value="02:30 PM">02:30 PM</option>
                        //     <option value="03:00 PM">03:00 PM</option>
                        //     <option value="03:30 PM">03:30 PM</option>
                        //     <option value="04:00 PM">04:00 PM</option>
                        //     <option value="04:30 PM">04:30 PM</option>
                        //     <option value="05:00 PM">05:00 PM</option>
                        //     <option value="05:30 PM">05:30 PM</option>
                        //     <option value="06:00 PM">06:00 PM</option>
                        //     <option value="06:30 PM">06:30 PM</option>
                        //     <option value="07:00 PM">07:00 PM</option>
                        //     <option value="07:30 PM">07:30 PM</option>
                        //     <option value="08:00 PM">08:00 PM</option>
                        //     <option value="08:30 PM">08:30 PM</option>
                        //     <option value="09:00 PM">09:00 PM</option>
                        //     <option value="09:30 PM">09:30 PM</option>
                        //     <option value="10:00 PM">10:00 PM</option>
                        //     <option value="10:30 PM">10:30 PM</option>
                        //     <option value="11:00 PM">11:00 PM</option>
                        //     <option value="11:30 PM">11:30 PM</option>
                        // </select>
                        // ';
                        // echo '
                        // <script type="text/javascript">
                        // $( document ).ready(function() {
                        //     $.widget( "ui.timespinner", $.ui.spinner, {
                        //         options: {
                        //             // seconds
                        //             step: 60 * 1000,
                        //             // hours
                        //             page: 60
                        //         },
                        //
                        //         _parse: function( value ) {
                        //             if ( typeof value === "string" ) {
                        //                 // already a timestamp
                        //                 if ( Number( value ) == value ) {
                        //                     return Number( value );
                        //                 }
                        //                 return +Globalize.parseDate( value );
                        //             }
                        //             return value;
                        //         },
                        //
                        //         _format: function( value ) {
                        //             return Globalize.format( new Date(value), "t" );
                        //         }
                        //     });
                        //
                        //     $( "#duty_start_time" ).timespinner();
                        // });
                        // </script>
                        // ';

                        echo '<script type="text/javascript">
                                    $( document ).ready(function() {
                                        $("#duty_start_time").val("' . $dutyStartTime12 . '");
                                    });
                                </script>';
                    } else {
                        $dutyStartTime24 = $driver_model->get('duty_start_time');
                        $dutyStartTime12 = date("g:i A", strtotime($dutyStartTime24));
                        echo text($dutyStartTime12);
                    }
                    ?>

                </div>
                <!-- Duty Start Time Per Day end -->

            </div>

            <!-- <div class="form-group">
    			    <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
            // echo form_label ( 'Shift Preference:', 'shift_preference', array (
            // 		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
            // ) );
            // if ($view_mode == EDIT_MODE) {
            //     echo form_dropdown ( 'shift_preference', $shift_preference_list, $driver_model->get ( 'shift_preference' ), array (
            //             'id' => 'shift_preference',
            //             'class' => 'form-control',
            //             'required' => 'required'
            //     ) );
            // } else {
            //     echo text ($driver_model->get ( 'shift_preference' ) );
            // }

            ?>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
            // echo form_label ( 'Employment Type:', 'employment_type', array (
            // 		'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
            // ) );
            // if ($view_mode == EDIT_MODE) {
            //     echo form_dropdown ( 'employment_type', $employment_type_list, $driver_model->get ( 'employment_type' ), array (
            //             'id' => 'employment_type',
            //             'class' => 'form-control',
            //             'required' => 'required'
            //     ) );
            // } else {
            //     echo text ($driver_model->get ( 'employment_type' ) );
            // }

            ?>
                    </div>
                </div> -->
            <!-- Aditya code section End Here -->

            <br/>
            <legend>Driver Personal Details</legend>
            <div class="form-group">
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">

                    <?php
                    // validation for driver first name
                    echo form_label('First Name:', 'firstName', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));

                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'firstName',
                            'name' => 'firstName',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[A-Za-z\s]{3,30}',
                            'value' => ($driver_model->get('firstName')) ? $driver_model->get('firstName') : ''
                        ));
                    } else {
                        echo text($driver_model->get('firstName'));
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver last name
                    echo form_label('Last Name:', 'lastName', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'lastName',
                            'name' => 'lastName',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[A-Za-z\s]{1,30}',
                            'value' => ($driver_model->get('lastName')) ? $driver_model->get('lastName') : ''
                        ));
                    } else {
                        echo text($driver_model->get('lastName'));
                    }
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver email
                    echo form_label('Email:', 'email', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'email',
                            'name' => 'email',
                            'class' => 'form-control',
                            //'required' => 'required',
                            //'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$',
                            'value' => ($driver_model->get('email')) ? $driver_model->get('email') : ''
                        ));
                    } else {
                        echo text($driver_model->get('email'));
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver mobile
                    echo form_label('Mobile:', 'mobile', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'mobile',
                            'name' => 'mobile',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[789]\d{9}',
                            'data-inputmask' => '"mask" : "(999) 999-9999"',
                            'value' => ($driver_model->get('mobile')) ? $driver_model->get('mobile') : ''
                        ));
                    } else {
                        echo text($driver_model->get('mobile'));
                    }
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver profile Image
                    echo form_label('Profile Image:', 'profileImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'profileImage',
                            'name' => 'profileImage[]',
                            'class' => 'form-control',
                            'value' => ($driver_model->get('profileImage')) ? $driver_model->get('profileImage') : ''
                        ));
                    } else {
                        echo '<a target="_blank" href="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_model->get('profileImage') . '">' . text($driver_model->get('profileImage')) . '</a><br />';

                        //if(!empty($driver_model->get ( 'profileImage' )))
                        if (!empty($driver_model->get('profileImage'))) {
                            if (strpos($driver_model->get("profileImage"), 'driver_docs') !== false) {
                                echo '<img src="https://s3-ap-southeast-1.amazonaws.com/zuverlive/' . $driver_model->get("profileImage") . '" alt="Profile Image" width="100" height="100" style="margin-left:50%">';
                            } else {
                                echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_model->get('profileImage') . '" width="100" height="100" />';
                            }
                        }
                    }
                    ?>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">

                    <?php
                    // validation for driver driverLanguagesKnown
                    echo form_label('Languages Known:', 'driverLanguagesKnown', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'driverLanguagesKnown',
                            'name' => 'driverLanguagesKnown',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[a-zA-Z\s]{3,50}',
                            'placeholder' => 'Enter languages seperated by space',
                            'value' => ($driver_model->get('driverLanguagesKnown')) ? $driver_model->get('driverLanguagesKnown') : ''
                        ));
                    } else {
                        echo text($driver_model->get('driverLanguagesKnown'));
                    }
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver driverExperience
                    echo form_label('Experience:', 'driverExperience', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'driverExperience',
                            'name' => 'driverExperience',
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => ($driver_model->get('driverExperience')) ? $driver_model->get('driverExperience') : ''
                        ));
                    } else {
                        echo text($driver_model->get('driverExperience'));
                    }
                    ?>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver favouriteCar
                    echo form_label('Favourite Car:', 'favouriteCar', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'favouriteCar',
                            'name' => 'favouriteCar',
                            'class' => 'form-control',

                            // 'required' => 'required',
                            // 'pattern' => '[a-zA-Z0-9\s]{1,100}',
                            'value' => ($driver_model->get('favouriteCar')) ? $driver_model->get('favouriteCar') : ''
                        ));
                    } else {
                        echo text($driver_model->get('favouriteCar'));
                    }
                    ?>

                </div>


            </div>


            <div class="form-group">
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver profile Image
                    echo form_label('Verified File Upload:', 'verifiedDocument', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'verifiedDocument',
                            'name' => 'verifiedDocument[]',
                            'class' => 'form-control',

                            // 'required' => 'required',
                            'value' => ($driver_model->get('verifiedDocument')) ? $driver_model->get('verifiedDocument') : ''
                        ));
                    } else {
                        echo '<a target="_blank" href="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_model->get('verifiedDocument') . '">' . text($driver_model->get('verifiedDocument')) . '</a><br />';
                        if (!empty($driver_model->get('verifiedDocument'))) echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_model->get('verifiedDocument') . '" width="100" height="100" />';
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver skills
                    echo form_label('Transmission Type (Skills):', 'skills', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('skills', $transmission_type_list, $driver_model->get('skills'), array(
                            'id' => 'skills',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                        /*
                         * echo form_input(array(
                         * 'id' => 'skills',
                         * 'name' => 'skills',
                         * 'class' => 'form-control',
                         * 'required' => 'required',
                         * 'pattern' => '[a-zA-Z\s]{1,200}',
                         * 'value' => ($driver_model->get('skills')) ? $driver_model->get('skills') : ''
                         * ));
                         */
                    } else {
                        echo text($transmission_type_list [$driver_model->get('skills')]);
                    }
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver City
                    echo form_label('Driver Skills:', 'driverSkill', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        // original code commented by Aditya on May 24 2017
                        // echo form_dropdown ( 'driverSkill', $driver_skill_list, $driver_model->get ( 'driverSkill' ), array (
                        // 		'id' => 'driverSkill',
                        // 		'class' => 'form-control',
                        // 		'required' => 'required'
                        // ) );

                        // created by Aditya on May 24 2017
                        echo '
                            <select name="driverSkill2" id="driverSkill2" class="select2_multiple form-control" multiple="multiple" required="required">
                                
                                <option value="HA">Hatchback</option>
                                <option value="SE">Sedan</option>
                                <option value="SU">Suv</option>
                                <option value="FL">Luxury</option>
                                
                            </select>
                            ';
                        echo '<script type="text/javascript">
                                $( document ).ready(function() {
                                    var strSkills = "' . $driver_model->get('driverSkill') . '";
                                    //alert(strSkills);
                                    //var values="HA,SU";
                                    $.each(strSkills.split(";"), function(i,e){
                                        //alert(e);
                                        $("#driverSkill2 option[value=\'" + e + "\']").prop("selected", true);
                                    });
                                });
                            </script>';
                    } else {
                        // original code commented by Aditya on May 24 2017
                        // echo text ( $driver_skill_list [$driver_model->get ( 'driverSkill' )] );
                        $str = $driver_model->get('driverSkill');
                        $skillsArray = explode(";", $str);
                        $outString = '';
                        foreach ($skillsArray as $key => $value) {
                            if ($value == 'FL') {
                                $outString[] = 'Luxury';
                            }
                            if ($value == 'SE') {
                                $outString[] = 'Sedan';
                            }
                            if ($value == 'SU') {
                                $outString[] = 'Suv';
                            }
                            if ($value == 'HA') {
                                $outString[] = 'Hatchback';
                            }
                            if ($value == 'NL') {
                                $outString[] = 'Hatchback, Sedan, Suv';
                            }
                            if ($value == 'NN') {
                                $outString[] = 'None';
                            }
                            if ($value == '') {
                                $outString[] = 'None';
                            }
                        }
                        echo text(implode(", ", $outString));
                    }
                    ?>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver City
                    echo form_label('City Name:', 'cityId', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('cityId', $city_list, $driver_model->get('cityId'), array(
                            'id' => 'cityId',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                    } else {
                        echo text($city_list [$driver_model->get('cityId')]);
                    }
                    ?>

                </div>


            </div>
            <div class="form-group">
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver driver type
                    echo form_label('Driver Type:', 'driverType', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE || $view_mode == VIEW_MODE) {
                        echo form_dropdown('driverType', $driver_type_list, $driver_model->get('driverType'), array(
                            'id' => 'driverType',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                    } else {
                        echo text($driver_type_list [$driver_model->get('driverType')]);
                    }
                    ?>

                </div>
                <div
                        class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback hidden"
                        id="fixedTripEarning">
                    <?php
                    // validation for driver driver type
                    echo form_label('Fixed Trip Earning:', 'fixedTripEarning', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE || $view_mode == VIEW_MODE) {
                        echo form_input(array(
                            'id' => 'fixedTripEarning',
                            'name' => 'fixedTripEarning',
                            'class' => 'form-control',

                            // 'required' => 'required',
                            // 'pattern' => '[0-9]+([\.,][0-9]+)?',
                            'value' => ($driver_model->get('fixedTripEarning')) ? $driver_model->get('fixedTripEarning') : ''
                        ));
                    } else {
                        echo text($driver_model->get('fixedTripEarning'));
                    }
                    ?>

                </div>
                <div
                        class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback hidden"
                        id="commissionTripEarning">
                    <?php
                    // validation for driver driver type
                    echo form_label('Commission Trip Earning:', 'commissionTripEarning', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE || $view_mode == VIEW_MODE) {
                        echo form_input(array(
                            'id' => 'commissionTripEarning',
                            'name' => 'commissionTripEarning',
                            'class' => 'form-control',

                            // 'required' => 'required',
                            // 'pattern' => '[0-9]+([\.,][0-9]+)?',
                            'value' => ($driver_model->get('commissionTripEarning')) ? $driver_model->get('commissionTripEarning') : ''
                        ));
                    } else {
                        echo text($driver_model->get('commissionTripEarning'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group" id="productivityAllowance">
                <div class="col-xs-12 form-group has-feedback">
                    <?php
                    //print_r($driver_productivity_allowance_model);
                    echo form_label('Productivity Allowance:', 'productivity_allowance', array(
                        'class' => 'required'
                    ));
                    ?>
                    <div class="form-group">
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php echo "Rs. " . form_input(array(
                                    'id' => 'productivity_allowance_1',
                                    'name' => 'productivity_allowance[0][extraRate]',
                                    'class' => 'form-control',
                                    'style' => 'width:40%; display:inline',
                                    //'required' => 'required',
                                    'pattern' => '^\d{1,4}$',
                                    'value' => ($driver_productivity_allowance_model) ? (($driver_productivity_allowance_model[0]) ? $driver_productivity_allowance_model[0]->extraRate : '0') : '0'
                                ));
                            ?> / Trip
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php echo "After " . form_input(array(
                                    'id' => 'productivity_allowance_1',
                                    'name' => 'productivity_allowance[0][extraRateForMin]',
                                    'class' => 'form-control',
                                    'style' => 'width:40%; display:inline',
                                    //'required' => 'required',
                                    'pattern' => '^\d{1,4}$',
                                    'value' => ($driver_productivity_allowance_model) ? (($driver_productivity_allowance_model[0]) ? $driver_productivity_allowance_model[0]->extraRateForMin : '5') : '5'
                                ));
                            ?> Trips
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php echo "Rs. " . form_input(array(
                                    'id' => 'productivity_allowance_2',
                                    'name' => 'productivity_allowance[1][extraRate]',
                                    'class' => 'form-control',
                                    'style' => 'width:40%; display:inline',
                                    //'required' => 'required',
                                    'pattern' => '^\d{1,4}$',
                                    'value' => ($driver_productivity_allowance_model) ? (($driver_productivity_allowance_model[1]) ? $driver_productivity_allowance_model[1]->extraRate : '0') : '0'
                                ));
                            ?> / Trip
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php echo "After " . form_input(array(
                                    'id' => 'productivity_allowance_2',
                                    'name' => 'productivity_allowance[1][extraRateForMin]',
                                    'class' => 'form-control',
                                    'style' => 'width:40%; display:inline',
                                    //'required' => 'required',
                                    'pattern' => '^\d{1,4}$',
                                    'value' => ($driver_productivity_allowance_model) ? (($driver_productivity_allowance_model[1]) ? $driver_productivity_allowance_model[1]->extraRateForMin : '7') : '7'
                                ));
                            ?> Trips
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php echo "Rs. " . form_input(array(
                                    'id' => 'productivity_allowance_3',
                                    'name' => 'productivity_allowance[2][extraRate]',
                                    'class' => 'form-control',
                                    'style' => 'width:40%; display:inline',
                                    //'required' => 'required',
                                    'pattern' => '^\d{1,4}$',
                                    'value' => ($driver_productivity_allowance_model) ? (($driver_productivity_allowance_model[2]) ? $driver_productivity_allowance_model[2]->extraRate : '0') : '0'
                                ));
                            ?> / Trip
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php echo "After " . form_input(array(
                                    'id' => 'productivity_allowance_3',
                                    'name' => 'productivity_allowance[2][extraRateForMin]',
                                    'class' => 'form-control',
                                    'style' => 'width:40%; display:inline',
                                    //'required' => 'required',
                                    'pattern' => '^\d{1,4}$',
                                    'value' => ($driver_productivity_allowance_model) ? (($driver_productivity_allowance_model[2]) ? $driver_productivity_allowance_model[2]->extraRateForMin : '9') : '9'
                                ));
                            ?> Trips
                        </div>
                    </div>
                </div>
            </div>
            <div class="x_title">
                <h2>
                    Driver Register
                    <small>Personal Details</small>
                </h2>

                <div class="clearfix"></div>
            </div>
            <div class="form-group">
                <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Gender

                    echo form_label('Gender:', 'gender', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        if (is_array($driver_gender) && count($driver_gender)) {
                            echo '<p>';
                            // debug_exit($driver_gender);
                            foreach ($driver_gender as $value) {
                                // validation for pm to show only delete buttons & admin for their draft release only delete buttons

                                echo $value->description . ":";

                                echo form_radio(array(
                                    'id' => 'gender' . $value->enumName,
                                    'name' => 'gender',
                                    'class' => 'flat',
                                    'value' => $value->enumName,
                                    'required' => 'required',
                                    'checked' => $driver_personal_details_model->get('gender') ? 'true' : 'false'
                                ));
                            }
                            echo '</p>';
                        }
                    } else {
                        echo text($driver_personal_details_model->get('gender'));
                    }
                    ?>

                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver DOB
                    echo form_label('Date of Birth:', 'dob', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'dob',
                            'name' => 'dob',
                            'class' => 'form-control',
                            'autocomplete' => 'off',
                            'required' => 'required',
                            'value' => ($driver_personal_details_model->get('dob')) ? $driver_personal_details_model->get('dob') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('dob'));
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Qualification
                    echo form_label('Qualification:', 'driverQualification', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'driverQualification',
                            'name' => 'driverQualification',
                            'class' => 'form-control',

                            // 'required' => 'required',
                            'value' => ($driver_personal_details_model->get('driverQualification')) ? $driver_personal_details_model->get('driverQualification') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('driverQualification'));
                    }
                    ?>

                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver License type
                    echo form_label('License Type:', 'licenseType', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('licenseType', $license_type_list, $driver_personal_details_model->get('licenseType'), array(
                            'id' => 'licenseType',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                    } else {
                        echo text($license_type_list [$driver_personal_details_model->get('licenseType')]);
                    }
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver address
                    echo form_label('Address:', 'address', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_textarea(array(
                            'id' => 'address',
                            'name' => 'address',
                            'class' => 'form-control',

                            'required' => 'required',
                            //'pattern' => '[a-zA-Z0-9.#,-\s]',
                            'rows' => '3',
                            'value' => ($driver_personal_details_model->get('address')) ? $driver_personal_details_model->get('address') : ''
                        ));
                    } else {
                        echo form_textarea(array(
                            'id' => 'address',
                            'name' => 'address',
                            'class' => 'form-control',
                            'readonly' => 'readonly',
                            // 'required' => 'required',
                            'pattern' => '[a-zA-Z0-9.#,-\s]',
                            'rows' => '3',
                            'value' => ($driver_personal_details_model->get('address')) ? $driver_personal_details_model->get('address') : ''
                        ));
                    }
                    ?>

                </div>

            </div>
            <div class="form-group">
                <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Driving License No
                    echo form_label('DL No:', 'drivingLicenseNo', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'drivingLicenseNo',
                            'name' => 'drivingLicenseNo',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[a-zA-Z0-9-\s]{1,15}',
                            'value' => ($driver_personal_details_model->get('drivingLicenseNo')) ? $driver_personal_details_model->get('drivingLicenseNo') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('drivingLicenseNo'));
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver profile Image
                    echo form_label('Profile Image:', 'profileImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == VIEW_MODE) {
                        echo form_upload(array(
                            'id' => 'profileImage',
                            'name' => 'profileImage[]',
                            'class' => 'form-control',
                            'value' => ($driver_model->get('profileImage')) ? $driver_model->get('profileImage') : ''
                        ));
                    } else {
                        echo '<a target="_blank" href="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_model->get('profileImage') . '">' . text($driver_model->get('profileImage')) . '</a><br />';

                        //if(!empty($driver_model->get ( 'profileImage' )))
                        if (!empty($driver_model->get('profileImage'))) {
                            if (strpos($driver_model->get("profileImage"), 'driver_docs') !== false) {
                                echo '<img src="https://s3-ap-southeast-1.amazonaws.com/zuverlive/' . $driver_model->get("profileImage") . '" alt="Profile Image" width="100" height="100" style="margin-left:50%">';
                            } else {
                                echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_model->get('profileImage') . '" width="100" height="100" />';
                            }
                        }
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Driving License Image
                    echo form_label('DL Image:', 'drivingLicenseImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'drivingLicenseImage',
                            'name' => 'drivingLicenseImage[]',
                            'multiple' => true,
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => ($driver_personal_details_model->get('drivingLicenseImage')) ? $driver_personal_details_model->get('drivingLicenseImage') : ''
                        ));
                    } else {
                        echo '<a target="_blank" href="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('drivingLicenseImage') . '">' . text($driver_personal_details_model->get('drivingLicenseImage')) . '</a><br />';

                        if (!empty($driver_personal_details_model->get('drivingLicenseImage'))) {
                            if (strpos($driver_personal_details_model->get("drivingLicenseImage"), 'driver_docs') !== false) {
                                echo '<img src="https://s3-ap-southeast-1.amazonaws.com/zuverlive/' . $driver_personal_details_model->get("drivingLicenseImage") . '" alt="License Image" width="100" height="100" style="margin-left:0%">';
                            } else {
                                echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('drivingLicenseImage') . '" width="100" height="100" />';
                            }
                        }
                    }
                    ?>

                </div>


                <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver PAN Card No
                    echo form_label('PAN Card No:', 'panCardNo', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'panCardNo',
                            'name' => 'panCardNo',
                            'class' => 'form-control',

                            // 'required' => 'required',
                            'pattern' => '[A-Z0-9]{1,20}',
                            'value' => ($driver_personal_details_model->get('panCardNo')) ? $driver_personal_details_model->get('panCardNo') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('panCardNo'));
                    }
                    ?>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Address Proof Image
                    echo form_label('Address Proof Image:', 'addressProofImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'addressProofImage',
                            'name' => 'addressProofImage[]',
                            'multiple' => true,
                            'class' => 'form-control',
                            //'required' => 'required',
                            'value' => ($driver_personal_details_model->get('addressProofImage')) ? $driver_personal_details_model->get('addressProofImage') : ''
                        ));
                    } else {
                        if (!empty($driver_personal_details_model->get('addressProofImage'))) {
                            if (strpos($driver_personal_details_model->get("addressProofImage"), 'driver_docs') !== false) {
                                echo '<img src="https://s3-ap-southeast-1.amazonaws.com/zuverlive/' . $driver_personal_details_model->get("addressProofImage") . '" alt="License Image" width="100" height="100">';
                            } else {
                                echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('drivingLicenseImage') . '" width="100" height="100" />';
                            }
                        }
                    }
                    ?>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback hide">
                    <?php
                    // validation for driver PAN Card Image
                    echo form_label('PAN Card Image:', 'panCardImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'panCardImage',
                            'name' => 'panCardImage[]',
                            'multiple' => true,
                            'class' => 'form-control',

                            // 'required' => 'required',
                            'value' => ($driver_personal_details_model->get('panCardImage')) ? $driver_personal_details_model->get('panCardImage') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('panCardImage'));
                        if (strpos($driver_personal_details_model->get("panCardImage"), 'driver_docs') !== false) {
                            echo '<img src="https://s3-ap-southeast-1.amazonaws.com/zuverlive/' . $driver_personal_details_model->get("panCardImage") . '" alt="Profile Image" width="100" height="100" style="margin-left:50%">';
                        } else {
                            echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('panCardImage') . '" width="100" height="100" />';
                        }
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">


                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback hide">
                    <?php
                    // validation for driver Address Proof No
                    echo form_label('Address Proof No:', 'addressProofNo', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'addressProofNo',
                            'name' => 'addressProofNo',
                            'class' => 'form-control',
                            //'required' => 'required',
                            //'pattern' => '[a-zA-Z0-9-\s]{1,250}',
                            'value' => ($driver_personal_details_model->get('addressProofNo')) ? $driver_personal_details_model->get('addressProofNo') : ''
                        ));
                    } else {
                        echo '<a target="_blank" href="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('addressProofNo') . '">' . text($driver_personal_details_model->get('addressProofNo')) . '</a><br />';
                        if (!empty($driver_personal_details_model->get('addressProofNo'))) echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('addressProofNo') . '" width="100" height="100" />';
                    }
                    ?>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback hide">
                    <?php
                    // validation for driver Address Proof Image
                    echo form_label('Address Proof Image:', 'addressProofImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'addressProofImage',
                            'name' => 'addressProofImage[]',
                            'multiple' => true,
                            'class' => 'form-control',
                            //'required' => 'required',
                            'value' => ($driver_personal_details_model->get('addressProofImage')) ? $driver_personal_details_model->get('addressProofImage') : ''
                        ));
                    } else {
                        if (!empty($driver_personal_details_model->get('addressProofImage'))) {
                            if (strpos($driver_personal_details_model->get("addressProofImage"), 'driver_docs') !== false) {
                                echo '<img src="https://s3-ap-southeast-1.amazonaws.com/zuverlive/' . $driver_personal_details_model->get("addressProofImage") . '" alt="License Image" width="100" height="100">';
                            } else {
                                echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('drivingLicenseImage') . '" width="100" height="100" />';
                            }
                        }
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback hide">
                    <?php
                    // validation for driver Identity Proof No
                    echo form_label('Identity Proof No:', 'identityProofNo', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'identityProofNo',
                            'name' => 'identityProofNo',
                            'class' => 'form-control',

                            // 'required' => 'required',
                            'pattern' => '[a-zA-Z0-9-\s]{1,50}',
                            'value' => ($driver_personal_details_model->get('identityProofNo')) ? $driver_personal_details_model->get('identityProofNo') : ''
                        ));
                    } else {
                        echo '<a target="_blank" href="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('identityProofNo') . '">' . text($driver_personal_details_model->get('identityProofNo')) . '</a><br />';
                        if (!empty($driver_personal_details_model->get('identityProofNo'))) echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('identityProofNo') . '" width="100" height="100" />';
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback hide">
                    <?php
                    // validation for driver Identity Proof Image
                    echo form_label('Identity Proof Image:', 'identityProofImage', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_upload(array(
                            'id' => 'identityProofImage',
                            'name' => 'identityProofImage[]',
                            'multiple' => true,
                            'class' => 'form-control',

                            // 'required' => 'required',
                            'value' => ($driver_personal_details_model->get('identityProofImage')) ? $driver_personal_details_model->get('identityProofImage') : ''
                        ));
                    } else {
                        echo '<a target="_blank" href="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('identityProofImage') . '">' . text($driver_personal_details_model->get('identityProofImage')) . '</a><br />';
                        if (!empty($driver_personal_details_model->get('identityProofImage'))) echo '<img src="' . base_url() . $this->config->item('driver_content_path') . $driver_model->get('id') . '/' . $driver_personal_details_model->get('identityProofImage') . '" width="100" height="100" />';
                    }
                    ?>

                </div>
            </div>

            <div class="form-group">


            </div>
            <div class="form-group">

                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Company Name
                    echo form_label('Bank Name:', 'bankName', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'bankName',
                            'name' => 'bankName',
                            'class' => 'form-control',
                            //'required' => 'required',
                            //'pattern' => '[a-zA-Z0-9\s]{3,30}',
                            'value' => ($driver_personal_details_model->get('bankName')) ? $driver_personal_details_model->get('bankName') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('bankName'));
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver City
                    echo form_label('Bank Branch:', 'bankBranch', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'bankBranch',
                            'name' => 'bankBranch',
                            'class' => 'form-control',
                            //'required' => 'required',
                            //'pattern' => '[a-zA-Z0-9\s]{3,15}',
                            'value' => ($driver_personal_details_model->get('bankBranch')) ? $driver_personal_details_model->get('bankBranch') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('bankBranch'));
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Company Name
                    echo form_label('Account no:', 'bankAccountNo', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'bankAccountNo',
                            'name' => 'bankAccountNo',
                            'class' => 'form-control',
                            'autocomplete' => 'off',
                            //'required' => 'required',
                            //'pattern' => '\d{7,15}',
                            'value' => ($driver_personal_details_model->get('bankAccountNo')) ? $driver_personal_details_model->get('bankAccountNo') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('bankAccountNo'));
                    }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver City
                    echo form_label('IFSC Code:', 'bankIfscCode', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : ''
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'bankIfscCode',
                            'name' => 'bankIfscCode',
                            'class' => 'form-control',
                            'autocomplete' => 'off',
                            //'required' => 'required',
                            //'pattern' => '[A-Z]{4}[0-9]{7}',
                            'value' => ($driver_personal_details_model->get('bankIfscCode')) ? $driver_personal_details_model->get('bankIfscCode') : ''
                        ));
                    } else {
                        echo text($driver_personal_details_model->get('bankIfscCode'));
                    }
                    ?>

                </div>
            </div>


            <div class="form-group"></div>
            <div class="ln_solid"></div>
            <div
                <?php echo ($view_mode == VIEW_MODE) ? 'class="form-group hidden" ' : 'class="form-group"'; ?>>
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                    <button type="button" id="load_driver_btn" class="btn btn-primary">Cancel</button>
                    <button type="button" id="save_driver_btn" class="btn btn-success"
                            onclick="saveDriver();" data-loading-text="<i
						class='fa fa-spinner fa-spin '>
						</i> Processing"> Submit
                    </button>
                    <?php if ($this->User_Access_Model->deleteDriver() && $driver_model->get('id') > 0) : ?>
                        <button type="button" id="delete_driver_btn"
                                class="btn btn-primary" onclick="deleteDriver();">Delete
                        </button>
                    <?php endif; ?>
                </div>
            </div>
            <?php if ($view_mode == VIEW_MODE) : ?>
                <div class="form-group">
                    <center>
                        <button type="button" id="save_driver_btn" class="btn btn-success"
                                onclick="saveDriver();" data-loading-text="<i
						class='fa fa-spinner fa-spin '>
						</i> Processing"> Save
                        </button>
                        <button type="button" id="load_driver_btn" class="btn btn-primary">Back</button>
                    </center>
                </div>
            <?php endif; ?>



            <?php
            echo form_close();
            ?>
        </div>
    </div>
</div>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <div id="error-msg"></div>
                <?php
                $form_attr = array(
                    'name' => 'change_driver_password_form',
                    'id' => 'change_driver_password_form',
                    'class' => 'form-horizontal form-label-left',
                    'data-parsley-validate' => '',
                    'method' => 'POST'
                );
                echo form_open('', $form_attr);
                // driver id by default is -1
                echo form_input(array(
                    'type' => 'hidden',
                    'id' => 'driver_id',
                    'name' => 'driver_id',
                    'value' => ($driver_model->get('id')) ? $driver_model->get('id') : -1
                ));
                ?>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <?php
                        echo form_label('New Password: ', 'newPassword', array(
                            'class' => 'required'));
                        echo form_input(array(
                            'id' => 'newPassword',
                            'name' => 'newPassword',
                            'type' => 'password',
                            'class' => 'form-control',
                            'autocomplete' => 'off',
                            'required' => 'required',
                            //'pattern' => '\d{7,15}',
                        ));
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <?php
                        echo form_label('Confirm New Password:', 'confirmPassword', array(
                            'class' => 'required'));
                        echo form_input(array(
                            'id' => 'confirmPassword',
                            'name' => 'confirmPassword',
                            'type' => 'password',
                            'class' => 'form-control',
                            'autocomplete' => 'off',
                            'required' => 'required',
                            //'pattern' => '\d{7,15}',
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="changeDriverPasswordStatus">Save</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>
