<?php
$view_mode = $mode;
?>
<div class="">

	<div class="clearfix"></div>

	<div class="row">
		<!-- form input mask -->
		<div id="report-details-information"
			class="col-md-12 col-sm-12 col-xs-12">

			<div class="x_panel">
				<div class="x_title">
					<h2>
						<i class="fa fa-tag"></i> Report for Completed Trip's <small>According
							to Trip Type, City, Company/Partners, Payment Mode</small>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<br />
					<?php
					$form_attr = array (
							'name' => 'edit_report_form',
							'id' => 'edit_report_form',
							'method' => 'POST',
							'data-parsley-validate' => '',
							'class' => 'form-horizontal form-label-left' 
					);
					echo form_open ( base_url ( 'report/saveTariff' ), $form_attr );
					?> 
					<div class="form-group">


						<div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Date Filter Type:', 'dateFilterType', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
                            $date_filter_type_list=array('M'=>'Month','Y'=>'Year','CD'=>'Customize date');
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'dateFilterType', $date_filter_type_list, '', array (
																															'id' => 'dateFilterType',
																															'class' => 'form-control',
																															'required' => 'required' 
																													) );
																												} else {
																													echo text ( $trip_type_list [$tariff_model->get ( 'tripType' )] );
																												}
																												?>
                        </div>
                        <div id="startDate-filter" class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback date-filter-type">
                            <?php
																												// validation for user name
																												echo form_label ( 'Select Month:', 'startDate', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for passenger first name
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'startDate',
																															'name' => 'startDate',
																															'class' => 'form-control',
																															'required' => 'required',
																															'value' => '' 
																													) );
																												} else {
																													echo text ( '' );
																												}
																												?>
                        </div>
                        <div id="endDate-filter" class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback date-filter-type hidden">
                            <?php
																												// validation for user name
																												echo form_label ( 'End Date:', 'endDate', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : 'required' 
																												) );
																												?>
                            <?php
																												// validation for passenger first name
																												if ($view_mode == EDIT_MODE) {
																													echo form_input ( array (
																															'id' => 'endDate',
																															'name' => 'endDate',
																															'class' => 'form-control',
																															'value' => '' 
																													) );
																												} else {
																													echo text ( '' );
																												}
																												?>
                        </div>
						
					</div>
					<div class="form-group">


						<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Trip Type:', 'tripType', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												?>
                            <?php
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'tripType', $trip_type_list, '', array (
																															'id' => 'tripType',
																															'class' => 'form-control'
																															 
																													) );
																												} else {
																													echo text ( $trip_type_list [$tariff_model->get ( 'tripType' )] );
																												}
																												?>
                        </div>
						<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'Payment Mode:', 'paymentMode', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												?>
                            <?php
																												
																												echo form_dropdown ( 'paymentMode', $payment_mode_list, '', array (
																														'id' => 'paymentMode',
																														'class' => 'form-control'
																														 
																												) );
																												
																												?>
                        </div>
                        
						<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
																												echo form_label ( 'City Name:', 'cityId', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												?>
                            <?php
																												if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'cityId', $city_list, '', array (
																															'id' => 'cityId',
																															'class' => 'form-control'
																															 
																													) );
																												} else {
																													echo text ( $city_list [$tariff_model->get ( 'cityId' )] );
																												}
																												?>
                        </div>
						<div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <?php
                            
																												echo form_label ( 'Entity Name:', 'companyId', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												?>
                            <?php
																											if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'companyId', $company_list, '', array (
																															'id' => 'companyId',
																															'class' => 'form-control'
																															
																													) );
																												} else {
																													echo text ( $company_list [$tariff_model->get ( 'companyId' )] );
																												}
																												?>
                        </div>
                        <div id="partner-user" class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback hidden">
                            <?php
                            
																												echo form_label ( 'Entity User:', 'partnerUser', array (
																														'class' => (($view_mode == VIEW_MODE)) ? '' : '' 
																												) );
																												?>
                            <?php
																											if ($view_mode == EDIT_MODE) {
																													echo form_dropdown ( 'partnerUser',array(), '', array (
																															'id' => 'partnerUser',
																															'class' => 'form-control'
																															
																													) );
																												} else {
																													echo text ( $company_list [$tariff_model->get ( 'partnerUser' )] );
																												}
																												?>
                        </div>

					</div>
					<div class="ln_solid"></div>
					<div
						<?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
							<button type="button" class="btn btn-success" id="get-complete-trip-report">Get Complete Trip Report</button>
							<!-- <button type="button" class="btn btn-success" id="get-cancelled-trip-report">Get Cancelled Trip Report</button>
							<button type="button" class="btn btn-success" id="get-trip-status-report">Get Trip Status Report</button> -->
						</div>
					</div>
					<?php
					echo form_close ();
					?>
				</div>

			</div>
			<div id="report_container"></div>
			
		</div>
		
	</div>
</div>
