<div class="x_title">
	<h2>Cancelled Trip Reports</h2>

	<div class="clearfix"></div>
</div>
<div class="x_content">

	<table id="datatable-checkbox"
		class="table table-striped table-bordered datatable-button-init-collection">
		<thead>
			<tr>
				<th>
					<!-- <input type="checkbox" id="check-all" class="flat"> -->
				</th>
				<!-- <th>Sl no</th> -->
				<th>Trip Id</th>
				<th>Passenger Name (Mobile)</th>
				<th>Driver Name (Mobile)</th>
				<th>Trip Datetime</th>
				<th>City Name</th>
				<th>Entity Name</th>
				<th>Trip Type</th>
				<th>Cancelled Reason</th>
				<th>Cancelled Status</th>
				<!-- <th>Cancelled Reason</th> -->
				
			</tr>
		</thead>


		<tbody>
				<?php
				// sl no intialization
				//$i = 1;
				$total_amount_collected = 0;
				// $tariff_model_list=array(0=>array('name'=>'OneWay','dayConvenienceCharge'=>'100.00','nightConvenienceCharge'=>'150.00','fixedAmount'=>'185.00','cityName'=>'Bangalore','status'=>'Y'),1=>array('name'=>'Return','dayConvenienceCharge'=>'200.00','nightConvenienceCharge'=>'250.00','fixedAmount'=>'285.00','cityName'=>'Mumbai','status'=>'Y'));
				foreach ( $cancelled_trip_report_list as $list ) {
					
					echo '<tr>';
					
					echo '<td></td>'; // <input type="checkbox" class="flat" name="table_records">
					//echo '<th>' . $i . '</th>';
					echo '<td>' . $list->tripId . '</td>';
					$passenger_mobile=($list->passengerMobile)?'('.$list->passengerMobile.')':'';
					echo '<td>' . $list->passengerFirstName . ' ' . $list->passengerLastName . ' ' . $passenger_mobile . '</td>';
					$driver_mobile=($list->tripStatusType==Trip_Status_Enum::CANCELLED_BY_DRIVER)?(($list->driverMobile)?'('.$list->driverMobile.')':''):'';
					echo '<td>' . $list->driverFirstName . ' ' . $list->driverLastName . ' ' . $driver_mobile . '</td>';
					echo '<td>' . $list->pickupDatetime . '</td>';
					echo '<td>' . $list->cityName . '</td>';
					echo '<td>' . $list->companyName . '</td>';
					echo '<td>' . $list->tripType . '</td>';
					$reject_reason=($list->tripStatusType==Trip_Status_Enum::CANCELLED_BY_DRIVER)?$list->rejectionReason:$list->passengerRejectComments;
					echo '<td>' . $reject_reason . '</td>';
					echo '<td>' . $list->tripStatus . '</td>';
					
					
					echo '</tr>';
					
					//$i ++;
				}
				?>
		</tbody>
	</table>

</div>