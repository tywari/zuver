<div class="x_title">
	<h2>Completed Trip Reports</h2>

	<div class="clearfix"></div>
</div>
<div class="x_content">

	<table id="datatable-checkbox"
		class="table table-striped table-bordered datatable-button-init-collection">
		<thead>
			<tr>
				<th>
					<!-- <input type="checkbox" id="check-all" class="flat"> -->
				</th>
				<!-- <th>Sl no</th> -->
				<th>Trip Id</th>
				<th>Passenger Name (Mobile)</th>
				<th>User Name (Mobile)</th>
				<th>Driver Name (Mobile)</th>
				<th>Created Datetime</th>
				<th>Request PickUp Datetime</th>
				<th>Driver Assigned Datetime</th>
				<th>Driver Arrived Datetime</th>
				<th>Trip Start Datetime</th>
				<th>Trip End Datetime</th>
				<th>PickUp Location</th>
				<th>Trip Duration</th>
				<th>Trip Type</th>
				<th>Payment Mode</th>
				<th>Transmission Type</th>
				<th>Promocode</th>
				<th>City Name</th>
				<th>Entity Name</th>
				<th>Source</th>
				<th>Convienience Charge</th>
				<th>Traveled Charge</th>
				<th>Parking Charge</th>
				<th>Toll Charge</th>
				<th>Tax Amount</th>
				<th>Dicount Amount</th>
				<th>Amount Collected</th>
				<th>Admin Commission Amount</th>
				<th>Driver Earning</th>
			</tr>
		</thead>


		<tbody>
				<?php
				// sl no intialization
				//$i = 1;
				$total_amount_collected = 0;
				$total_tax_amount = 0;
				$total_discount_amount = 0;
				$total_admin_commission_amount = 0;
				
				// $tariff_model_list=array(0=>array('name'=>'OneWay','dayConvenienceCharge'=>'100.00','nightConvenienceCharge'=>'150.00','fixedAmount'=>'185.00','cityName'=>'Bangalore','status'=>'Y'),1=>array('name'=>'Return','dayConvenienceCharge'=>'200.00','nightConvenienceCharge'=>'250.00','fixedAmount'=>'285.00','cityName'=>'Mumbai','status'=>'Y'));
				foreach ( $complete_trip_report_list as $list ) {
					$total_amount_collected += $list->totalTripCost;
					$total_tax_amount += $list->companyTax;
					$total_discount_amount += $list->promoDiscountAmount+$list->otherDiscountAmount;
					$total_discount_amount += $list->adminAmount;
					echo '<tr>';
					
					echo '<td></td>'; // <input type="checkbox" class="flat" name="table_records">
					//echo '<th>' . $i . '</th>';
					echo '<td>' . $list->tripId . '</td>';
					$passenger_mobile=($list->passengerMobile)?'('.$list->passengerMobile.')':'';
					echo '<td>' . $list->passengerFirstName . ' ' . $list->passengerLastName . ' ' . $passenger_mobile . '</td>';
					$user_mobile=($list->userMobile)?'('.$list->userMobile.')':'';
					echo '<td>' . $list->userFirstName . ' ' . $list->userLastName . ' ' . $user_mobile . '</td>';
					$driver_mobile=($list->driverMobile)?'('.$list->driverMobile.')':'';
					echo '<td>' . $list->driverFirstName . ' ' . $list->driverLastName . ' ' . $driver_mobile . '</td>';
					
					echo '<td>' . $list->tripCreatedDate . '</td>';
					echo '<td>' . $list->pickupDatetime . '</td>';
					echo '<td>' . $list->driverAssignedDatetime . '</td>';
					echo '<td>' . $list->driverArrivedDatetime . '</td>';
					echo '<td>' . $list->actualPickupDatetime . '</td>';
					echo '<td>' . $list->dropDatetime . '</td>';
					echo '<td>' . $list->pickupLocation . '</td>';
					if(strrpos($list->travelHoursMinutes, '.')!==FALSE)
					{
						$travelled_time=explode('.', $list->travelHoursMinutes);
					}
					elseif (strrpos($list->travelHoursMinutes, ':')!==FALSE) {
						$travelled_time=explode(':', $list->travelHoursMinutes);
					}
					$hours=$travelled_time[0];
					$minutes=$travelled_time[1];
					if ($hours<= 9 && strlen($hours)<2)
					{
						$hours='0'.$hours;
					}
					if ($minutes<= 9 && strlen($minutes)<2)
					{
						$minutes='0'.$minutes;
					}
					$trip_duration=$hours.':'.$minutes;
					
					echo '<td>' . $trip_duration . '</td>';
					echo '<td>' . $list->tripType . '</td>';
					echo '<td>' . $list->paymentMode . '</td>';
					echo '<td>' . $list->transmissionType . '</td>';
					echo '<td>' . $list->promoCode . '</td>';
					echo '<td>' . $list->cityName . '</td>';
					echo '<td>' . $list->companyName . '</td>';
					echo '<td>' . $list->bookedFrom . '</td>';
					echo '<td>' . $list->convenienceCharge . '</td>';
					echo '<td>' . $list->travelCharge . '</td>';
					echo '<td>' . $list->parkingCharge . '</td>';
					echo '<td>' . $list->tollCharge . '</td>';
					echo '<td>' . $list->companyTax . '</td>';
					echo '<td>' . ($list->promoDiscountAmount+$list->otherDiscountAmount). '</td>';
					echo '<td>' . $list->totalTripCost . '</td>';
					echo '<td>' . $list->adminAmount . '</td>';
					echo '<td>' . $list->driverEarning . '</td>';
					echo '</tr>';
					
					//$i ++;
				}
				?>
		</tbody>
	</table>

</div>