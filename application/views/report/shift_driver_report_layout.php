<?php
$view_mode = $mode;
?>
<div class="">

	<div class="clearfix"></div>

	<div class="row">
		<!-- form input mask -->
		<div id="report-details-information"
			class="col-md-12 col-sm-12 col-xs-12">

			<div class="x_panel">
				<div class="x_title">
					<h2>
						<i class="fa fa-tag"></i> Report for Drivers Session <small>According
							to Shifts</small>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<br />
					<?php
					$form_attr = array (
							'name' => 'edit_shift_report_form',
							'id' => 'edit_shift_report_form',
							'method' => 'POST',
							'data-parsley-validate' => '',
							'class' => 'form-horizontal form-label-left' 
					);
					echo form_open ('', $form_attr );
					?> 
					<div class="form-group">


						<div id="startDate-filter" class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">Shift Start DateTime
                            <?php
									 echo form_input(array(
                                            'id' => 'shiftStartDatetime',
                                            'name' => 'shiftStartDatetime',
                                            'class' => 'form-control',
                                            'autocomplete' => 'off',
                                            'required' => 'required',
                                        ));
							?>
                        </div>
                        <div id="endDate-filter" class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback date-filter-type">Shift End DateTime
                           <?php
									 echo form_input(array(
                                            'id' => 'shiftEndDatetime',
                                            'name' => 'shiftEndDatetime',
                                            'class' => 'form-control',
                                            'autocomplete' => 'off',
                                            'required' => 'required',
                                        ));
							?>
                        </div>
                       
					</div>
					<div class="ln_solid"></div>
					<div>
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
							<!-- <button type="button" class="btn btn-success" id="get-complete-trip-report">Get Complete Trip Report</button>
							<button type="button" class="btn btn-success" id="get-cancelled-trip-report">Get Cancelled Trip Report</button> -->
							<button type="submit" class="btn btn-success" id="get-shift-driver-report">Download Report</button>
						</div>
					</div>
					<?php
					echo form_close ();
					?>
				</div>

			</div>

		</div>
		
	</div>
</div>
