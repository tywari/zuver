<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php echo SMTP_EMAIL_NAME;?></title>
</head>
<body marginwidth="0" marginheight="0" leftmargin="0" offset="0" topmargin="0" style="margin: 0px; padding: 0px; background-color: #454545;">
	<div style="background-color: #454545;">
		<center>
			<!-- Main wrapper -->
			<table width="600" style="font-size: 12px; line-height: 21px; font-family: 'Lucida Grande',Arial,sans-serif; color: rgb(102, 102, 102); margin-top: 15px; margin-bottom: 25px;" cellpadding="0" cellspacing="0" border="0">
				<tbody>
					<!-- Top rounded corners image -->
					<tr>
						<td colspan="3" height="20px" style="line-height: 0px;">
							<img src="<?php echo image_url('app/email/email_temp_border_top.png');?>" alt="" height="20px" width="600px" style="line-height: 0px"/>
						</td>
					</tr>
					<!-- End Top rounded corners image -->
					
					<tr>
						<!-- Left shadow image -->
						
						<!-- End Left shadow image -->
						<td bgcolor="white" align="center" width="600px" style="background-color: white; line-height:0px">
							<!-- Header image -->
							<div style="height:50px;padding-top:20px;">
							<a href="##SITEURL##"><img height="30" alt="" src="<?php echo image_url('app/email/email_site_logo.png');?>"></img></a>
							</div>
							<!-- Divider -->
							<img height="20" width="520" alt="" src="<?php echo image_url('app/email/email_temp_divider1.jpg');?>"/>
							
							<!-- Full width image / text 1 -->
							<table align="center" cellspacing="0" cellpadding="0" width="520" style="font-size: 13px; line-height: 20px; font-family: 'Lucida Grande',Arial,sans-serif; color: rgb(102, 102, 102);">					
								<tbody>
									<tr>
										<td width="520px" valign="top">
											<!-- Post Heading !-->
											<p style="margin: 0px; padding: 0px; color: #161616; font-size: 15px;">Hi <?php echo $driver_name;?>,</p>
											<!-- End of heading !-->
											<!-- Image for post !-->
											<img src="<?php echo image_url('app/email/email_temp_spacer-header.jpg');?>" width="520px" height="20px" alt="<?php echo SMTP_EMAIL_NAME;?>"/>
										</td>
									</tr>		
									<tr>
										<td width="520px" valign="top" align="left">
											<!-- Post text !-->
											<p style="color:#000;">Your Zuver account has been successfully created!</p>
											<!-- End of post text !-->
										</td>
									</tr>
									<tr>
										<td width="520px" valign="top" align="left">
											<!-- Post text !-->
											<p style="margin: 0px; padding: 0px; color: #161616; font-size: 15px; font-weight: bold">Access Details:</p>
											<img src="<?php echo image_url('app/email/email_temp_divider_posts.jpg');?>" width="520px" height="20px" alt="<?php echo SMTP_EMAIL_NAME;?>"/>
											<p style="color:#000;"><strong>Mobile Number:</strong> <?php echo $mobile;?></p>    
											<p style="color:#000;"><strong>Password:</strong> <?php echo $password;?></p> 
											<p style="color:#000;"><strong>Download the app on</strong> <?php echo DRIVERAPPLINK;?></p>
											<!-- End of post text !-->
										</td>
									</tr>
									
									<tr>
										<td>
											<p style="color:#000;">Regards, </p><p>Team Zuver</p>
										</td>
									</tr>									
								</tbody>	
							</table>
							<!-- End Full width image / text 1 -->
							
						</td>
						<!-- Shadow wrapper right !-->
						
						<!-- End shadow wrapper right !-->
					</tr>
					<!-- Bottom rounded corners image -->
					<tr style="background:#fff;">
										<td width="520px" valign="top" align="left">
											<p style="color:#000;margin-left:0;text-align:center;"><?php echo COPYRIGHTS;?></p>
										</td>
									</tr>
					<tr>
						<td colspan="3" height="20px" style="line-height: 0px;">
							<img src="<?php echo image_url('app/email/email_temp_border_bottom.png');?>" alt="" height="20px" width="600px"/>
						</td>
					</tr>
					<!-- End Bottom rounded corners image -->
				</tbody>
			</table>
			<!-- Main wrapper -->	
		</center>
	</div>
</body>
</html>
