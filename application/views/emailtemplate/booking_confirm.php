<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php echo SMTP_EMAIL_NAME;?></title>
</head>
<body marginwidth="0" marginheight="0" leftmargin="0" offset="0" topmargin="0" style="margin: 0px; padding: 0px; background-color: #454545;">
	<div style="background-color: #454545;">
		<center>
			<!-- Main wrapper -->
			<table width="600" style="font-size: 12px; line-height: 21px; font-family: 'Lucida Grande',Arial,sans-serif; color: rgb(102, 102, 102); margin-top: 15px; margin-bottom: 25px;" cellpadding="0" cellspacing="0" border="0">
				<tbody>
					<!-- Top rounded corners image -->
					<tr>
						<td colspan="3" height="20px" style="line-height: 0px;">
							<img src="<?php echo image_url('app/email/email_temp_border_top.png');?>" alt="" height="20px" width="600px" style="line-height: 0px"/>
						</td>
					</tr>
					<!-- End Top rounded corners image -->
					
					<tr>
						<!-- Left shadow image -->
						
						<!-- End Left shadow image -->
						<td bgcolor="white" align="center" width="600px" style="background-color: white; line-height:0px">
							<!-- Header image -->
							<div style="height:50px;padding-top:20px;">
							<a href="<?php echo base_url(); ?>"><img height="30" alt="" src="<?php echo image_url('app/email/email_site_logo.png');?>"/></a>
							</div>
							<!-- Divider -->
							<img height="20" width="520" alt="" src="<?php echo image_url('app/email/email_temp_divider1.jpg');?>"/>
							
							<!-- Full width image / text 1 -->
							<table align="center" cellspacing="0" cellpadding="0" width="520" style="font-size: 13px; line-height: 20px; font-family: 'Lucida Grande',Arial,sans-serif; color: rgb(102, 102, 102);">					
								<tbody>
									<tr>
										<td width="520px" valign="top">
											<!-- Post Heading !-->
											<h3 style="margin: 0px; padding: 0px; color: #161616; font-size: 15px; font-weight: bold">Hi 
											<?php echo (isset($trip_type) && ($trip_type=='B2B' || $trip_type=='ZOOMCAR'))?'':$passenger_name; ?>,</h3>
											<!-- End of heading !-->
											<!-- Image for post !-->
											<img src="<?php echo image_url('app/email/email_temp_spacer-header.jpg');?>" width="520px" height="20px" alt="<?php echo SMTP_EMAIL_NAME;?>"/>
										</td>
									</tr>		
									<tr>
										<td width="520px" valign="top" align="left">
											<!-- Post text !-->
											<p style="color:#000;">
											<?php if(isset($trip_type) && $trip_type=='B2B'){ ?>You have received a B2B request as follows:<?php }else if(isset($trip_type) && $trip_type=='ZOOMCAR'){ ?>You have received a Zoomcar Booking request as follows:<?php }else{ ?>Your Drive later request is received. Driver will contact you shortly.<?php } ?></p>
											<!-- End of post text !-->
										</td>
									</tr>
									<tr>
										<td width="520px" valign="top" align="left">
											<!-- Post text !-->
											<h3 style="margin: 0px; padding: 0px; color: #161616; font-size: 15px; font-weight: bold">Trip Booking Details</h3>
											<?php if(isset($trip_type) && $trip_type=='B2B'){ ?>
											<p style="color:#000;"><strong>Entity Name:</strong><?php echo $entity_name; ?></p> 
											<p style="color:#000;"><strong>Entity Contact:</strong><?php echo $entity_mobile; ?></p>
											<?php } else { ?>
											<p style="color:#000;"><strong>Passenger Name:</strong><?php echo $passenger_name; ?></p>
											<p style="color:#000;"><strong>Passenger Phone:</strong> <?php echo $passenger_mobile; ?></p>
											<?php } ?> 

											<?php if(isset($trip_id)){ ?><p style="color:#000;"><strong>Trip Id:</strong> <?php echo $trip_id; ?></p><?php } ?>

											<p style="color:#000;"><strong>Booking Key:</strong> <?php echo $booking_key; ?></p>    
											<p style="color:#000;"><strong>Pickup location:</strong> <?php echo $pickup_location; ?></p>
											<p style="color:#000;"><strong>Pickup Date & Time:</strong> <?php echo $pickup_date_time; ?></p>    
																						
											<!-- End of post text !-->
										</td>
									</tr>	
									<tr>
										<td width="520px" valign="top" align="left">
											<!-- Post text !-->
											<!--<h3 style="margin: 0px; padding: 0px; color: #161616; font-size: 15px; font-weight: bold">Referral code : ##PROMOCODE##</h3>!-->
											
											<!-- End of post text !-->
										</td>									
									</tr>	
									<tr>
										<td>
											<p style="color:#000;">Regards, </br>Zuver Team</p>
										</td>
									</tr>
								</tbody>	
							</table>
							<!-- End Full width image / text 1 -->							
							<img src="<?php echo image_url('app/email/email_temp_divider_posts.jpg');?>" alt="" style="line-height: 0px" width="520px" height="51px"/>	
							<!-- End Half half image / text 2 -->																				
						</td>
					</tr>
					<!-- Bottom rounded corners image -->
					<tr style="background:#fff;">
										<td width="520px" valign="top" align="left">
											<p style="color:#000;margin-left:0;text-align:center;"><?php echo COPYRIGHTS;?></p>
										</td>
									</tr>
					<tr>
						<td colspan="3" height="20px" style="line-height: 0px;">
							<img src="<?php echo image_url('app/email/email_temp_border_bottom.png');?>" alt="" height="20px" width="600px"/>
						</td>
					</tr>
					<!-- End Bottom rounded corners image -->
				</tbody>
			</table>
			<!-- Main wrapper -->	
		</center>
	</div>
</body>
</html>
