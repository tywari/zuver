
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" /> 
		<title>Zuver Billing</title>
		<style type="text/css">
			/* RESET STYLES */
			html { background-color:#E1E1E1; margin:0; padding:0; }
			body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
			table{border-collapse:collapse;}
			table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
			img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
			a {text-decoration:none !important;border-bottom: 1px solid;}
			h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}

			/* CLIENT-SPECIFIC STYLES */
			.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
			table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
			#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
			img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */
			body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
			.ExternalClass td[class="ecxflexibleContainerBox"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */

			/* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

			/* ========== Page Styles ========== */
			h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}
			h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}
			h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}
			h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}
			.flexibleImage{height:auto;}
			.linkRemoveBorder{border-bottom:0 !important;}
			table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}

			body, #bodyTable{background-color:#E1E1E1;}
			#emailHeader{background-color:#E1E1E1;}
			#emailBody{background-color:#FFFFFF;}
			#emailFooter{background-color:#E1E1E1;}
			.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}
			.emailButton{background-color:#205478; border-collapse:separate;}
			.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
			.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}
			.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
			.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
			.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
			.imageContentText {margin-top: 10px;line-height:0;}
			.imageContentText a {line-height:0;}
			#invisibleIntroduction {display:none !important;} /* Removing the introduction text from the view */

			/*FRAMEWORK HACKS & OVERRIDES */
			span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} /* Remove all link colors in IOS (below are duplicates based on the color preference) */
			span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}
			span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}
			
			.a[href^="tel"], a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}


			/* MOBILE STYLES */
			@media only screen and (max-width: 480px){
				/*////// CLIENT-SPECIFIC STYLES //////*/
				body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */

				/* FRAMEWORK STYLES */
				/*
				CSS selectors are written in attribute
				selector format to prevent Yahoo Mail
				from rendering media query styles on
				desktop.
				*/
				/*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
				table[id="emailHeader"],
				table[id="emailBody"],
				table[id="emailFooter"],
				table[class="flexibleContainer"],
				td[class="flexibleContainerCell"] {width:100% !important;}
				td[class="flexibleContainerBox"], td[class="flexibleContainerBox"] table {display: block;width: 100%;text-align: left;}
				/*
				The following style rule makes any
				image classed with 'flexibleImage'
				fluid when the query activates.
				Make sure you add an inline max-width
				to those images to prevent them
				from blowing out.
				*/
				td[class="imageContent"] img {height:auto !important; width:100% !important; max-width:100% !important; }
				img[class="flexibleImage"]{height:auto !important; width:100% !important;max-width:100% !important;}
				img[class="flexibleImageSmall"]{height:auto !important; width:auto !important;}


				/*
				Create top space for every second element in a block
				*/
				table[class="flexibleContainerBoxNext"]{padding-top: 10px !important;}

				/*
				Make buttons in the email span the
				full width of their container, allowing
				for left- or right-handed ease of use.
				*/
				table[class="emailButton"]{width:100% !important;}
				td[class="buttonContent"]{padding:0 !important;}
				td[class="buttonContent"] a{padding:15px !important;}

			}

			/*  CONDITIONS FOR ANDROID DEVICES ONLY
			*   http://developer.android.com/guide/webapps/targeting.html
			*   http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
			=====================================================*/

			@media only screen and (-webkit-device-pixel-ratio:.75){
				/* Put CSS for low density (ldpi) Android layouts in here */
			}

			@media only screen and (-webkit-device-pixel-ratio:1){
				/* Put CSS for medium density (mdpi) Android layouts in here */
			}

			@media only screen and (-webkit-device-pixel-ratio:1.5){
				/* Put CSS for high density (hdpi) Android layouts in here */
			}
			/* end Android targeting */

			/* CONDITIONS FOR IOS DEVICES ONLY
			=====================================================*/
			@media only screen and (min-device-width : 320px) and (max-device-width:568px) {

			}
			/* end IOS targeting */
		</style>
		
	</head>
	<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<center style="background-color:#E1E1E1;">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
				<tr>
					<td align="center" valign="top" id="bodyCell">

						<!-- EMAIL HEADER // -->
						<!--
							The table "emailBody" is the email's container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#e899dc" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

							<!-- HEADER ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>		
																	<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td align="right" valign="middle" class="flexibleContainerBox">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="right" class="textContent">
																					<!-- CONTENT // -->
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:11px;color:#828282;text-align:right;line-height:120%;">
																						<?php echo 'Invoice#:'.$invoiceNo;//invoice number ?>
																					</div>
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:11px;color:#828282;text-align:right;line-height:120%;">
																						<?php echo $invoiceDatetime;//invoice date ?>
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // END -->

						</table>
						<!-- // END -->

						<!-- EMAIL BODY // -->
						<!--
							The table "emailBody" is the email's container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

							<!-- MODULE ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<!--
										The centering table keeps the content
										tables centered in the emailBody table,
										in case its width is set to 100%.
									-->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#000;font-size:11px;border-bottom:1px solid #ccc" bgcolor="#ffffff">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<!--
													The flexible container has a set width
													that gets overridden by the media query.
													Most content tables within can then be
													given 100% widths.
												-->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="left" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<!--
															The content table is the first element
																that's entirely separate from the structural
																framework of the email.
															-->
															<table border="0" cellpadding="4" cellspacing="0" width="100%">
																<tr>
																	<td align="left" valign="top" class="textContent">
																		<span><img src="<?php echo image_url('app/email/email_site_logo.png');?>"><span>						
																	</td>
																	<td><span>Hello <?php echo $passengerFullName;//passenger full name ?>. Thanks for choosing Zuver</span></td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<!-- MODULE ROW // -->							
							<tr mc:hideable>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="8" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top" class="flexibleContainerBox">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;">
																			<tr>
																				<td align="center" class="textContent">
																					<h3 style="color:#5F5F5F;line-height:75%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:2px;text-align:center;">Total Fare</h3>
																					<div style="font-size:54px;line-height:58px">&#8377;<span><?php echo round($totalTripCost);//total trip cost ?></span></div>
																				</td>
																			</tr>
																			<tr>
																				<td align="center" class="textContent">
																				<?php 
																					$travelled_time=explode('.', $travelHoursMinutes);
																					$hours=$travelled_time[0];
																					$minutes=$travelled_time[1];
																					if ($hours<= 9)
																					{
																						$hours='0'.$hours;
																					}		
																					if ($minutes<= 9)
																					{
																						$minutes='0'.$minutes;
																					}
																					if ($hours > 0)
																					{
																						$trip_duration=$hours.' Hours '.$minutes.' Minutes';
																					}
																					else 
																					{
																						$trip_duration=$minutes.' Minutes';
																					}
																				?>
																					<div style="font-size:11px;line-height:14px"><span><b>Total Riding Time :  <?php echo $trip_duration;//travel hours minutes ?></b></span></div>
																					<div style="font-size:11px;line-height:10px"><span><b>Total travelled Distance :  <?php echo ($travelDistance?$travelDistance:'NA'); ?></b></span>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr style="padding-top:0;">
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td style="padding-top:0;" align="center" valign="top" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table border="0" cellpadding="0" cellspacing="0" width="100%" >
																<tr>
																	<?php if($paymentMode != Payment_Mode_Enum::INVOICE): ?>
																	<td align="center" valign="middle" >
																	<div><span style="font-size:11px;line-height:12px">ZUVER MONEY DEDUCTED</span></div>
																	<div>&#8377;<?php echo $walletPaymentAmount;//Wallet deducted amount ?></div>										
																	</td>
																	<?php endif; ?>
																	<td align="center" valign="middle" >
																		<div><span style="font-size:11px;line-height:12px"><?php ($paymentMode == Payment_Mode_Enum::INVOICE)?'INVOICE AMOUNT':'CASH PAID'; ?></span></div>
																		<div>&#8377;<?php echo $totalTripCost-$walletPaymentAmount;//cash paid/paytm payment ?></div>								
																	</td>
																</tr>
																
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
													
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->
<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	
																	<td align="right" valign="top" class="flexibleContainerBox" style="background-color:#3b90d0;">
																		<table class="flexibleContainerBoxNext" border="0" cellpadding="2" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<div style="text-align:center;padding:6px 0 5px 0;color:#fff;background-color:#000;margin-bottom:6px;line-height:16px"><b>Tax</b></div>
																					<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#FFFFFF;line-height:135%;"><table cellpadding="4" style="width:100%;border-spacing:0;border-collapse:collapse;line-height:24px">
																								<tbody><tr><td colspan="2" style="padding:0">
																									
																								</td></tr>
																								<!--tr>
																									<td>Service tax(<?php echo $taxPercentage;//tax ?>%):</td>
																									<td>&#8377;  <?php echo $companyTax;//company tax ?></td>
																								</tr-->
																								<tr>
																									<td>Service tax(14%):</td>
																									<td>&#8377;  <?php echo number_format(round(14/100*($travelCharge+$convenienceCharge-$promoDiscountAmount-$otherDiscountAmount), 2),2,'.','');//company tax ?></td>
																								</tr>
																								<tr>
																									<td>SBS(0.5 %):</td>
																									<td>&#8377;  <?php echo number_format(round(0.5/100*($travelCharge+$convenienceCharge-$promoDiscountAmount-$otherDiscountAmount), 2),2,'.','');//company tax ?></td>
																								</tr>
																								<tr>
																									<td>KKC(0.5%):</td>
																									<td>&#8377;  <?php echo number_format(round(0.5/100*($travelCharge+$convenienceCharge-$promoDiscountAmount-$otherDiscountAmount), 2),2,'.','');//company tax ?></td>
																								</tr>
																							</tbody></table></div>
																					
																				</td>
																			</tr>
																			<tr>
																				<td align="left" class="textContent">
																					<div style="text-align:center;padding:6px 0 5px 0;color:#fff;background-color:#000;margin-bottom:6px;line-height:16px"><b>Offers</b></div>
																					
																					<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#FFFFFF;line-height:135%;"><table cellpadding="4" style="width:100%;border-spacing:0;border-collapse:collapse;line-height:24px">
																								<tbody><tr><td colspan="2" style="padding:0">
																									
																								</td></tr>
																								<tr>
																									<td>Promotions :</td>
																									<td>&#8377;  <?php echo $promoDiscountAmount;//promocode discount ?></td>
																								</tr>
																								<?php if($otherDiscountAmount)
																								{
																								echo '<tr><td colspan="2" style="padding:0"></td></tr>';
																								echo '<tr><td>Others :</td><td>&#8377; '.$otherDiscountAmount .'</td></tr>';
																								}
																								?>
																							</tbody></table></div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->

							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																				<table style="width:100%;border-spacing:0;border-collapse:collapse;font-size:12px">
																				<tbody><tr>
																					<td style="vertical-align:top;width:50%">
																						<div style="padding-right:5px">
																							<table style="width:100%;border-spacing:0;border-collapse:collapse;line-height:24px">
																								<tbody><tr><td colspan="2" style="padding:0">
																									<div style="text-align:center;padding:6px 0 5px 0;color:#3b90d0;background-color:#000;margin-bottom:6px;line-height:16px"><b>FARE BREAKUP</b></div>
																								</td></tr>
																								<tr>
																									<td>Fixed Rate: </td>
																									<td style="width:60px">&#8377;  <?php echo $travelCharge;//travel charge ?></td>
																								</tr>
																								<tr>
																									<td>Transit Fare: </td>
																									<td>&#8377;  <?php echo $convenienceCharge;//convieannce charge ?></td>
																								</tr>
																							</tbody></table>
																						</div>
																					</td>
																					<td style="vertical-align:top;width:50%">
																						<div style="padding-left:5px">
																							<table style="width:100%;border-spacing:0;border-collapse:collapse;line-height:24px">
																								<tbody><tr><td colspan="2" style="padding:0">
																									<div style="text-align:center;padding:6px 0 5px 0;color:#3b90d0;background-color:#000;margin-bottom:6px;line-height:16px"><b>OTHER BREAKUP</b></div>
																								</td></tr>
																								<tr>
																									<td>Parking Charge :</td>
																									<td>&#8377;  <?php echo $parkingCharge;//parking charge ?></td>
																								</tr>
																								<tr>
																									<td>Toll Charge :</td>
																									<td>&#8377;  <?php echo $tollCharge;//toll charge ?></td>
																								</tr>
																								<?php 
																								if($otherCharge)
																								{
																								echo '<tr><td>Other Charge :</td><td>&#8377;  '.$otherCharge .'</td></tr>';
																								}
																								?>
																								
																							</tbody></table>
																						</div>
																					</td>
																				</tr>
																			</tbody></table>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							

							


							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="12" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																																			<div>
																				<div style="text-align:center;padding:6px 0 5px 0;margin-bottom:5px;background-color:#ddd;margin-top:10px">
																					<b>BOOKING DETAILS</b>
																				</div>
																				<div>
																					<table style="border-spacing:0;border-collapse:collapse;line-height:24px;font-size:13px">
																						<tbody>
																						<tr>
																							<td style="width:150px"><span>Driver Name</span></td>
																							<td><?php echo $driverFullName;//driver full name ?></td>
																						</tr>
																						<tr>
																							<td style="width:150px"><span>Trip Type</span></td>
																							<td><?php echo $tripType;//trip type name ?></td>
																						</tr>
																						<tr>
																							<td><span>Booking Date</span></td>
																							<td><?php echo $bookedDatetime;//booking date ?></td>
																						</tr>
																						<tr>
																							<td><span>Pickup Date</span></td>
																							<td><?php echo $actualPickupDatetime;//actual pickup date ?></td>
																						</tr>
																						<tr>
																							<td><span>Drop Date</span></td>
																							<td><?php echo $dropDatetime;//actual drop date ?></td>
																						</tr>
																						<tr>
																							<td><span>Booking Email id</span></td>
																							<td><a href="mailto:<?php echo $passengerEmail;//booked by email id ?>" target="_blank"><?php echo $passengerEmail;//booked by email id ?></td></a></td>
																						</tr>
																						
																						<tr>
																						  <td colspan="2"><b>Please Rate our Driver.</b> Feedback helps us to improve our service.</td>
																						</tr>
																					</tbody></table>
																				</div>
																			</div>
																				</td>
																			
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->

						</table>
						<!-- // END -->

						<!-- EMAIL FOOTER // -->
						<!--
							The table "emailBody" is the email's container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="100%" id="emailFooter">

							<!-- FOOTER ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td valign="top" bgcolor="#E1E1E1">

																		<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																			<div>For further queries, please write to support@zuver.in
This is an electronically generated invoice and does not require signature. All terms and conditions are as given on www.zuver.in. </div>
																		</div>
                                                                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																			<div><?php echo COPYRIGHTS;?><a href="https://www.zuver.in/" target="_blank" style="text-decoration:none;color:#828282;"></a> </div>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>

						</table>
						<!-- // END -->

					</td>
				</tr>
			</table>
		</center>
	</body>
</html>