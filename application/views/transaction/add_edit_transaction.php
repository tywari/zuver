<?php
$view_mode = $mode;
?>
<div id="transaction-details-information"
     class="col-md-12 col-sm-12 col-xs-12">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
				<tr>
					<td align="center" valign="top" id="bodyCell">

						<!-- EMAIL HEADER // -->
						<!--
							The table "emailBody" is the email's container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#e899dc" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

							<!-- HEADER ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>		
																	<td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td align="right" valign="middle" class="flexibleContainerBox">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="right" class="textContent">
																					<!-- CONTENT // -->
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:11px;color:#828282;text-align:right;line-height:120%;">
																						<?php echo 'Invoice#:'.$transaction_details_model[0]->invoiceNo;//invoice number ?>
																					</div>
																					<div style="font-family:Helvetica,Arial,sans-serif;font-size:11px;color:#828282;text-align:right;line-height:120%;">
																						<?php echo $transaction_details_model[0]->invoiceDatetime;//invoice date ?>
																					</div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // END -->

						</table>
						<!-- // END -->

						<!-- EMAIL BODY // -->
						<!--
							The table "emailBody" is the email's container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

							<!-- MODULE ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<!--
										The centering table keeps the content
										tables centered in the emailBody table,
										in case its width is set to 100%.
									-->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#000;font-size:11px;border-bottom:1px solid #ccc" bgcolor="#ffffff">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<!--
													The flexible container has a set width
													that gets overridden by the media query.
													Most content tables within can then be
													given 100% widths.
												-->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="left" valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<!--
															The content table is the first element
																that's entirely separate from the structural
																framework of the email.
															-->
															<table border="0" cellpadding="4" cellspacing="0" width="100%">
																<tr>
																	<td align="left" valign="top" class="textContent">
																		<span><img src="<?php echo image_url('app/email/email_site_logo.png');?>"><span>						
																	</td>
																	<td><span>Hello <?php echo $transaction_details_model[0]->passengerFullName;//passenger full name ?>. Thanks for choosing Zuver</span></td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<!-- MODULE ROW // -->							
							<tr mc:hideable>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="8" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top" class="flexibleContainerBox">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 100%;">
																			<tr>
																				<td align="center" class="textContent">
																					<h3 style="color:#5F5F5F;line-height:75%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:2px;text-align:center;">Total Fare</h3>
																					<div style="font-size:54px;line-height:58px">&#8377;<span> <?php echo round($transaction_details_model[0]->totalTripCost);//total trip cost ?></span></div>
																				</td>
																			</tr>
																			<tr>
																				<td align="center" class="textContent">
																				<?php 
																					$travelled_time=explode('.', $transaction_details_model[0]->travelHoursMinutes);
																					$hours=$travelled_time[0];
																					$minutes=$travelled_time[1];
																					if ($hours<= 9)
																					{
																						$hours='0'.$hours;
																					}		
																					if ($minutes<= 9)
																					{
																						$minutes='0'.$minutes;
																					}
																					if ($hours > 0)
																					{
																						$trip_duration=$hours.' Hours '.$minutes.' Minutes';
																					}
																					else 
																					{
																						$trip_duration=$minutes.' Minutes';
																					}
																				?>
																					<div style="font-size:11px;line-height:14px"><span><b>Total Riding Time :  <?php echo $trip_duration;//travel hours minutes ?></b></span></div>
																					<div style="font-size:11px;line-height:10px"><span><b>Total travelled Distance :  <?php echo ($transaction_details_model[0]->travelDistance?$transaction_details_model[0]->travelDistance:'NA'); ?></b></span></div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->


							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr style="padding-top:0;">
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td style="padding-top:0;" align="center" valign="top" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table border="0" cellpadding="0" cellspacing="0" width="100%" >
																<tr>
																	<?php if($transaction_details_model[0]->paymentMode != Payment_Mode_Enum::INVOICE): ?>
																	<td align="center" valign="middle" >
																	<div><span style="font-size:11px;line-height:12px">ZUVER MONEY DEDUCTED</span></div>
																	<div>&#8377;<?php echo $transaction_details_model[0]->walletPaymentAmount;//Wallet deducted amount ?></div>										
																	</td>
																	<?php endif; ?>
																	<td align="center" valign="middle" >
																		<div><span style="font-size:11px;line-height:12px"><?php echo ($transaction_details_model[0]->paymentMode == Payment_Mode_Enum::INVOICE )?'INVOICE AMOUNT':'CASH PAID'; ?></span></div>
																		<div>&#8377;<?php echo $transaction_details_model[0]->totalTripCost-$transaction_details_model[0]->walletPaymentAmount;//cash paid/paytm payment ?></div>								
																	</td>
																</tr>
																
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
													
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->
<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td valign="top" width="500" class="flexibleContainerCell">

															<!-- CONTENT TABLE // -->
															<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	
																	<td align="right" valign="top" class="flexibleContainerBox" style="background-color:#3b90d0;">
																		<table class="flexibleContainerBoxNext" border="0" cellpadding="2" cellspacing="0" width="100%" style="max-width:100%;">
																			<tr>
																				<td align="left" class="textContent">
																					<div style="text-align:center;padding:6px 0 5px 0;color:#fff;background-color:#000;margin-bottom:6px;line-height:16px"><b>Tax</b></div>
																					<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#FFFFFF;line-height:135%;"><table cellpadding="4" style="width:100%;border-spacing:0;border-collapse:collapse;line-height:24px">
																								<tbody><tr><td colspan="2" style="padding:0">
																									
																								</td></tr>
																								<!--tr>
																									<td>Service tax(<?php //echo $transaction_details_model[0]->taxPercentage;//tax ?>15%):</td>
																									<td>&#8377; <?php echo $transaction_details_model[0]->companyTax;//company tax ?></td>
																								</tr-->
																								<tr>
																									<td>Service tax(14%):</td>
																									<td>&#8377;  <?php echo number_format(round(14/100*($transaction_details_model[0]->travelCharge+$transaction_details_model[0]->convenienceCharge-$transaction_details_model[0]->promoDiscountAmount-$transaction_details_model[0]->otherDiscountAmount), 2),2,'.','');//company tax ?></td>
																								</tr>
																								<tr>
																									<td>SBS(0.5 %):</td>
																									<td>&#8377;  <?php echo number_format(round(0.5/100 *($transaction_details_model[0]->travelCharge+$transaction_details_model[0]->convenienceCharge-$transaction_details_model[0]->promoDiscountAmount-$transaction_details_model[0]->otherDiscountAmount), 2),2,'.','');//company tax ?></td>
																								</tr>
																								<tr>
																									<td>KKC(0.5%):</td>
																									<td>&#8377;  <?php echo number_format(round(0.5/100*($transaction_details_model[0]->travelCharge+$transaction_details_model[0]->convenienceCharge-$transaction_details_model[0]->promoDiscountAmount-$transaction_details_model[0]->otherDiscountAmount), 2),2,'.','');//company tax ?></td>
																								</tr>
																							</tbody></table></div>
																					
																				</td>
																			</tr>
																			<tr>
																				<td align="left" class="textContent">
																					<div style="text-align:center;padding:6px 0 5px 0;color:#fff;background-color:#000;margin-bottom:6px;line-height:16px"><b>Offers</b></div>
																					
																					<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#FFFFFF;line-height:135%;"><table cellpadding="4" style="width:100%;border-spacing:0;border-collapse:collapse;line-height:24px">
																								<tbody><tr><td colspan="2" style="padding:0">
																									
																								</td></tr>
																								<tr>
																									<td>Promotions :</td>
																									<td>&#8377; <?php echo $transaction_details_model[0]->promoDiscountAmount;//promocode discount ?></td>
																								</tr>
																								<?php if($transaction_details_model[0]->otherDiscountAmount)
																								{
																								echo '<tr><td colspan="2" style="padding:0"></td></tr>';
																								echo '<tr><td>Others :</td><td>&#8377; '.$transaction_details_model[0]->otherDiscountAmount .'</td></tr>';
																								}
																								?>
																							</tbody></table></div>
																				</td>
																			</tr>
																			<?php if($sub_trips_list): ?>
																			<tr>
																				<td align="left" class="textContent">
																					<div style="text-align:center;padding:6px 0 5px 0;color:#fff;background-color:#000;margin-bottom:6px;line-height:16px"><b>Sub-Trips</b></div>
																					
																					<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#FFFFFF;line-height:135%;"><table cellpadding="4" style="width:100%;border-spacing:0; padding:5px; border-collapse:collapse;line-height:24px">
																								<tbody><tr><td colspan="2" style="padding:0">
																									
																								</td></tr>
																								<tr>
																									<td>&nbsp;</td>
																									<td>Sub-trip Id</td>
																									<td>Start Time :</td>
																									<td>End Time :</td>
																								</tr>
																								<?php foreach ($sub_trips_list as $sub_trip) {
																								?>
																								<tr>
																								 	<td>&nbsp;</td>
																									<td><?php echo $sub_trip->subTripId; ?></td>
																									<td><?php echo $sub_trip->pickupDatetime; ?></td>
																									<td><?php echo $sub_trip->dropDatetime; ?></td>
																								</tr>
																							<?php
																								}
																							?>	
																							</tbody></table></div>
																				</td>
																			</tr>
																			<?php endif; ?>
																		</table>
																	</td>
																</tr>
															</table>
															<!-- // CONTENT TABLE -->

														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->

							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																				<table style="width:100%;border-spacing:0;border-collapse:collapse;font-size:12px">
																				<tbody><tr>
																					<td style="vertical-align:top;width:50%">
																						<div style="padding-right:5px">
																							<table style="width:100%;border-spacing:0;border-collapse:collapse;line-height:24px">
																								<tbody><tr><td colspan="2" style="padding:0">
																									<div style="text-align:center;padding:6px 0 5px 0;color:#3b90d0;background-color:#000;margin-bottom:6px;line-height:16px"><b>FARE BREAKUP</b></div>
																								</td></tr>
																								<tr>
																									<td>Fixed Rate: </td>
																									<td style="width:60px">&#8377; <?php echo $transaction_details_model[0]->travelCharge;//travel charge ?></td>
																								</tr>
																								<tr>
																									<td>Transit Fare: </td>
																									<td>&#8377; <?php echo $transaction_details_model[0]->convenienceCharge;//convieannce charge ?></td>
																								</tr>
																							</tbody></table>
																						</div>
																					</td>
																					<td style="vertical-align:top;width:50%">
																						<div style="padding-left:5px">
																							<table style="width:100%;border-spacing:0;border-collapse:collapse;line-height:24px">
																								<tbody><tr><td colspan="2" style="padding:0">
																									<div style="text-align:center;padding:6px 0 5px 0;color:#3b90d0;background-color:#000;margin-bottom:6px;line-height:16px"><b>OTHER BREAKUP</b></div>
																								</td></tr>
																								<tr>
																									<td>Parking Charge :</td>
																									<td>&#8377; <?php echo $transaction_details_model[0]->parkingCharge;//parking charge ?></td>
																								</tr>
																								<tr>
																									<td>Toll Charge :</td>
																									<td>&#8377; <?php echo $transaction_details_model[0]->tollCharge;//toll charge ?></td>
																								</tr>
																								<?php 
																								if($transaction_details_model[0]->otherCharge)
																								{
																								echo '<tr><td>Other Charge :</td><td>&#8377; '.$transaction_details_model[0]->otherCharge .'</td></tr>';
																								}
																								?>
																								
																							</tbody></table>
																						</div>
																					</td>
																				</tr>
																			</tbody></table>
																				</td>
																			</tr>
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							

							


							<!-- MODULE ROW // -->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="12" cellspacing="0" width="100%">
																<tr>
																	<td align="center" valign="top">

																		<!-- CONTENT TABLE // -->
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td valign="top" class="textContent">
																																			<div>
																				<div style="text-align:center;padding:6px 0 5px 0;margin-bottom:5px;background-color:#ddd;margin-top:10px">
																					<b>BOOKING DETAILS</b>
																				</div>
																				<div>
																					<table style="border-spacing:0;border-collapse:collapse;line-height:24px;font-size:13px">
																						<tbody>
																						<tr>
																							<td style="width:150px"><span>Driver Name</span></td>
																							<td><?php echo $transaction_details_model[0]->driverFullName;//driver full name ?></td>
																						</tr>
																						<tr>
																							<td style="width:150px"><span>Trip Type</span></td>
																							<td><?php echo $transaction_details_model[0]->tripType;//trip type name ?></td>
																						</tr>
																						<tr>
																							<td><span>Booking Date</span></td>
																							<td><?php echo $transaction_details_model[0]->bookedDatetime;//booking date ?></td>
																						</tr>
																						<tr>
																							<td><span>Pickup Date</span></td>
																							<td><?php echo $transaction_details_model[0]->actualPickupDatetime;//actual pickup date ?></td>
																						</tr>
																						<tr>
																							<td><span>Drop Date</span></td>
																							<td><?php echo $transaction_details_model[0]->dropDatetime;//actual drop date ?></td>
																						</tr>
																						<tr>
																							<td><span>Booking Email id</span></td>
																							<td><a href="mailto:<?php echo $transaction_details_model[0]->passengerEmail;//booked by email id ?>" target="_blank"><?php echo $transaction_details_model[0]->passengerEmail;//booked by email id ?></td></a></td>
																						</tr>
																						
																						<tr>
																						  <td colspan="2"><b>Please Rate our Driver.</b> Feedback helps us to improve our service.</td>
																						</tr>
																					</tbody></table>
																				</div>
																			</div>
																				</td>
																			
																		</table>
																		<!-- // CONTENT TABLE -->

																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>
							<!-- // MODULE ROW -->

						</table>
						<!-- // END -->

						<!-- EMAIL FOOTER // -->
						<!--
							The table "emailBody" is the email's container.
							Its width can be set to 100% for a color band
							that spans the width of the page.
						-->
						<table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">

							<!-- FOOTER ROW // -->
							<!--
								To move or duplicate any of the design patterns
								in this email, simply move or copy the entire
								MODULE ROW section for each content block.
							-->
							<tr>
								<td align="center" valign="top">
									<!-- CENTERING TABLE // -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" valign="top">
												<!-- FLEXIBLE CONTAINER // -->
												<table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
													<tr>
														<td align="center" valign="top" width="500" class="flexibleContainerCell">
															<table border="0" cellpadding="30" cellspacing="0" width="100%">
																<tr>
																	<td valign="top" bgcolor="#E1E1E1">

																		<div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																			<div>For further queries, please write to support@zuver.in
This is an electronically generated invoice and does not require signature. All terms and conditions are as given on www.zuver.in. </div>
																		</div>
                                                                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
																			<div><?php echo COPYRIGHTS;?><a href="https://www.zuver.in/" target="_blank" style="text-decoration:none;color:#828282;"></a> </div>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<!-- // FLEXIBLE CONTAINER -->
											</td>
										</tr>
									</table>
									<!-- // CENTERING TABLE -->
								</td>
							</tr>

						</table>
						<!-- // END -->

					</td>
				</tr>
			</table>
			<div class="ln_solid"></div>
            <!-- <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                    <button type="button" id="load_transaction_btn"
                            class="btn btn-primary">Back</button>
                   
                </div>
            </div> -->
			</div>