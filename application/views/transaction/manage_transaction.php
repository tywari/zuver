<div id="transaction-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>
				Transaction's List
			</h2>
			<!--<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				 <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li> 
				
			</ul>
			<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" id="search_driver_content" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="search_driver_btn" type="button">Go!</button>
                    </span>
                    
                  </div>
                  <div><span>Search by Referral Code, Driver Full Name, Email and Mobile</span></div>
                </div>
             </div>
             <div class="title_right pull-right">
                
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<a href="<?php //echo base_url('transaction/add')?>"><button id="add_transaction_btn"
						class="btn btn-primary">Add Driver</button></a>
					
				</div>
                
             </div>-->
			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			<table id="datatable-checkbox"
				class="table table-striped table-bordered datatable-button-init-collection">
				<thead>
					<tr>
						<th><!-- <input type="checkbox" id="check-all" class="flat"> --></th>
						<!-- <th>Sl no</th> -->
						<th>Action</th>
						<th>Invoice Id</th>
						<th>Booking Key</th>
						<th>Passenger Name</th>
						<th>Driver Name</th>
						<th>Trip Date</th>
						<th>Trip Fare</th>
						<th>Tax</th>
						<th>Discount</th>
						<th>Total Trip Cost</th>
						<th>Trip Source</th>
						<th>Payment Mode</th>
						<th>Trip Type</th>
						<th>Transaction Id</th>
						<!-- <th>Payment Status</th> -->
						
					</tr>
				</thead>


				<tbody>
				<?php 
				//sl no intialization
				//$i=1;
				/* $transaction_model_list=array(
						0=>array('passengerFirstName'=>'Kiran','passengerLastName'=>'Kumar','driverFirstName'=>'Kiran','driverLastName'=>'Kumar','driverCode'=>'Kiran001','invoiceNo'=>'INV0001','bookingKey'=>'OCM-P-123-0001','tripFare'=>150,'companyTax'=>22.50,'DiscountAmount'=>100,'totalTripCost'=>72.5,'paymentMode'=>'Cash','companyName'=>'ZOOMCAR','actualPickupDatetime'=>'20-09-2016 10:10:10','tripType'=>'Oneway','transactionId'=>''),
						1=>array('passengerFirstName'=>'Kiran','passengerLastName'=>'Kumar','driverFirstName'=>'Driver Kiran','driverLastName'=>'Driver Kumar','driverCode'=>'Kiran001','invoiceNo'=>'INV0002','bookingKey'=>'RPA-P-123-0002','tripFare'=>100,'companyTax'=>22.50,'DiscountAmount'=>10,'totalTripCost'=>112.5,'paymentMode'=>'PAYTM','companyName'=>'ZOOMCAR2','actualPickupDatetime'=>'02-09-2016 10:10:10','tripType'=>'Return','transactionId'=>'PAYTM12345')
				        ); */
				foreach ($transaction_model_list as $list)
				{
					
				echo '<tr>';
					
				echo '<td></td>';//<input type="checkbox" class="flat" name="table_records">
				//echo '<th>'.$i.'</th>';
				echo '<td>'
						. '<a href="javascript:void(0);" id="viewTransaction-'.$list->id.'" onclick="viewTransaction(this.id);" class="userlink">'
								. '<span class="glyphicon glyphicon-eye-open"></span></a> '
										/* . '/ <a href="javascript:void(0);" id="editTransaction-'.$list->id.'" onclick="editTransaction(this.id);" class="userlink">'
												. '<span class="glyphicon glyphicon-edit"></span></a>' */
														. '</td>';
				echo '<td>'.$list->invoiceNo.'</td>';
				echo '<td>'.$list->bookingKey.'</td>';
				echo '<td>'.$list->passengerFirstName.' '.$list->passengerLastName.'</td>';
				echo '<td>'.$list->driverFirstName.' '.$list->driverLastName.'</td>';
				
				echo '<td>'.$list->actualPickupDatetime.'</td>';
				echo '<td>'.$list->tripFare.'</td>';
				
				echo '<td>'.$list->companyTax.'</td>';
				echo '<td>'.$list->DiscountAmount.'</td>';
				echo '<td>'.$list->totalTripCost.'</td>';
				echo '<td>'.$list->companyName.'</td>';
				echo '<td>'.$list->paymentMode.'</td>';
				echo '<td>'.$list->tripType.'</td>';
				echo '<td>'.$list->transactionId.'</td>';
				
				/*//echo '<td>'.($list->isVerified'])?'Verified':'Not Verified'.'</td>';
				echo '<td> <ul class="list-inline"> <li>';
				echo '<div class="btn-group btn-toggle">';
				echo '<button class="btn btn-xs btn-primary active">Paid</button>';
				echo '<button class="btn btn-xs btn-default">Pending</button>';
				echo '</div></li></ul></td>';*/
				
				
				echo '</tr>';
				//$i++;
				}
	            ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

