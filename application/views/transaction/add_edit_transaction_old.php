<?php
$view_mode = $mode;
?>
<div id="transaction-details-information"
     class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>
                Transcation <small>Transcation Details</small>
            </h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <?php
            $form_attr = array(
                'name' => 'edit_transaction_form',
                'id' => 'edit_transaction_form',
                'class' => 'form-horizontal form-label-left',
                'data-parsley-validate' => '',
                'method' => 'POST'
            );
            echo form_open_multipart(base_url(''), $form_attr);

            // driver id by default is -1
            echo form_input(array(
                'type' => 'hidden',
                'id' => 'transaction_id',
                'name' => 'id',
                'value' => ($transaction_details_model->get('id')) ? $transaction_details_model->get('id') : -1
            ));
            ?>
            <?php
            if ($view_mode == VIEW_MODE || $transaction_details_model->get('passengerCode')) {
                echo '<div class="form-group">';

                echo '<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">';
                echo form_label('Passenger Referral Code:', 'passengerCode', array(
                    'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
                ));
                echo text($transaction_details_model->get('passengerCode'));

                echo '</div></div>';
            }
            if ($view_mode == VIEW_MODE || $transaction_details_model->get('lastLoggedIn')) {
                echo '<div class="form-group">';

                echo '<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">';
                echo form_label('Last Logged In:', 'lastLoggedIn', array(
                    'class' => 'control-label col-md-3 col-sm-3 col-xs-12'
                ));
                echo text($transaction_details_model->get('lastLoggedIn'));

                echo '</div></div>';
            }
            ?>

            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">

                    <?php
                    // validation for driver first name
                    echo form_label('Trip Id:', 'tripId', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));

                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'tripId',
                            'name' => 'tripId',
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => ($transaction_details_model->get('tripId')) ? $transaction_details_model->get('tripId') : ''
                        ));
                    } else {
                        echo text($transaction_details_model->get('tripId'));
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver last name
                    echo form_label('Travel Distance:', 'travelDistance', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'travelDistance',
                            'name' => 'travelDistance',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[0-9]+([\.,][0-9]+)?',
                            'value' => ($transaction_details_model->get('travelDistance')) ? $transaction_details_model->get('travelDistance') : ''
                        ));
                    } else {
                        echo text($transaction_details_model->get('travelDistance'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver DOB
echo form_label('Travel Hours Minutes:', 'travelHoursMinutes', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'travelHoursMinutes',
        'name' => 'travelHoursMinutes',
        'class' => 'form-control',
        'autocomplete' => 'off',
        'required' => 'required',
        'pattern' => '[0-9]+([\.,][0-9]+)?',
        'value' => ($transaction_details_model->get('travelHoursMinutes')) ? $transaction_details_model->get('travelHoursMinutes') : ''
    ));
} else {
    echo text($transaction_details_model->get('travelHoursMinutes'));
}
?>
                    
                    <?php
// validation for driver DOB
echo form_label('Convenience  Charge:', 'convenienceCharge', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'convenienceCharge',
        'name' => 'convenienceCharge',
        'class' => 'form-control',
        'autocomplete' => 'off',
        'required' => 'required',
        'pattern' => '[0-9]+([\.,][0-9]+)?',
        'value' => ($transaction_details_model->get('convenienceCharge')) ? $transaction_details_model->get('convenienceCharge') : ''
    ));
} else {
    echo text($transaction_details_model->get('convenienceCharge'));
}
?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Company Name
                    echo form_label('Travel Charge:', 'travelCharge', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('travelCharge', $company_list, $transaction_details_model->get('travelCharge'), array(
                            'id' => 'travelCharge',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                    } else {
                        echo text($transaction_details_model->get('travelCharge'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('Parking Charge:', 'parkingCharge', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    ?>
                <div >
                    <?php
                    // validation for passenger first name
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'parkingCharge',
                            'name' => 'parkingCharge',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[0-9]+([\.,][0-9]+)?',
                            'value' => ($transaction_details_model->get('parkingCharge')) ? $transaction_details_model->get('parkingCharge') : ''
                        ));
                    } else {
                        echo text($transaction_details_model->get('parkingCharge'));
                    }
                    ?>
                </div>
            </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver tollCharge
                    echo form_label('Toll Charge:', 'tollCharge', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'tollCharge',
                            'name' => 'tollCharge',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[0-9]+([\.,][0-9]+)?',
                            'value' => ($transaction_details_model->get('tollCharge')) ? $transaction_details_model->get('tollCharge') : ''
                        ));
                    } else {
                        echo text($transaction_details_model->get('tollCharge'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver  companyTax
echo form_label('Company Tax:', ' companyTax', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => ' companyTax',
        'name' => ' companyTax',
        'class' => 'form-control',
        'required' => 'required',
        'pattern' => '[0-9]+([\.,][0-9]+)?',
        'value' => ($transaction_details_model->get(' companyTax')) ? $transaction_details_model->get(' companyTax') : ''
    ));
} else {
    echo text($transaction_details_model->get(' companyTax'));
}
?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">

                    <?php
                    // validation for driver  totalTripCost
                    echo form_label('Total Trip Cost:', ' totalTripCost', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => ' totalTripCost',
                            'name' => ' totalTripCost',
                            'class' => 'form-control',
                            'required' => 'required',
                            'pattern' => '[0-9]+([\.,][0-9]+)?',
                            'value' => ($transaction_details_model->get(' totalTripCost')) ? $transaction_details_model->get(' totalTripCost') : ''
                        ));
                    } else {
                        echo text($transaction_details_model->get(' totalTripCost'));
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver Address Proof No
echo form_label('Admin Amount:', '  adminAmount', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => '  totalTripCost',
        'name' => '  totalTripCost',
        'class' => 'form-control',
        'required' => 'required',
        'pattern' => '[0-9]+([\.,][0-9]+)?',
        'value' => ($transaction_details_model->get('  totalTripCost')) ? $transaction_details_model->get('  totalTripCost') : ''
    ));
} else {
    echo text($transaction_details_model->get('  totalTripCost'));
}
?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Address Proof Image
                    echo form_label('Company Amount:', ' companyAmount', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => ' companyAmount',
                            'name' => ' companyAmount',
                            'multiple'=>true,
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => ($transaction_details_model->get(' companyAmount')) ? $transaction_details_model->get(' companyAmount') : ''
                        ));
                    } else {
                        echo text($transaction_details_model->get(' companyAmount'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver Identity Proof No
echo form_label('Promo Discount Amount:', 'promoDiscountAmount', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'promoDiscountAmount',
        'name' => 'promoDiscountAmount',
        'class' => 'form-control',
        'required' => 'required',
        'value' => ($transaction_details_model->get('promoDiscountAmount')) ? $transaction_details_model->get('promoDiscountAmount') : ''
    ));
} else {
    echo text($transaction_details_model->get('promoDiscountAmount'));
}
?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Identity Proof Image
                    echo form_label('Wallet Payment Amount:', 'walletPaymentAmount', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'walletPaymentAmount',
                            'name' => 'walletPaymentAmount',
                            'multiple'=>true,
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => ($transaction_details_model->get('walletPaymentAmount')) ? $transaction_details_model->get('walletPaymentAmount') : ''
                        ));
                    } else {
                        echo text($transaction_details_model->get('walletPaymentAmount'));
                    }
                    ?>

                </div>
            </div>
            <div class="form-group">

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver PAN Card No
                    echo form_label('Other Discount Amount:', 'otherDiscountAmount', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_input(array(
                            'id' => 'otherDiscountAmount',
                            'name' => 'otherDiscountAmount',
                            'class' => 'form-control',
                            'required' => 'required',
                            'value' => ($transaction_details_model->get('otherDiscountAmount')) ? $transaction_details_model->get('otherDiscountAmount') : ''
                        ));
                    } else {
                        echo text($transaction_details_model->get('otherDiscountAmount'));
                    }
                    ?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('Payment Mode:', 'paymentMode', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                        echo form_dropdown('paymentMode', $payment_mode, $transaction_details_model->get('paymentMode'), array(
                            'id' => 'paymentMode',
                            'class' => 'form-control',
                            'required' => 'required'
                        ));
                    } else {
                        echo text($payment_mode[$transaction_details_model->get('paymentMode')]);
                    }
                    ?>

                </div>
            </div>

            
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
<?php
// validation for driver Qualification
echo form_label('Payment Status:', 'paymentStatus', array(
    'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
));
if ($view_mode == EDIT_MODE) {
    echo form_input(array(
        'id' => 'paymentStatus',
        'name' => 'paymentStatus',
        'class' => 'form-control',
        'required' => 'required',
        'value' => ($transaction_details_model->get('paymentStatus')) ? $transaction_details_model->get('paymentStatus') : ''
    ));
} else {
    echo text($transaction_details_model->get('paymentStatus'));
}
?>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    echo form_label('Transcation Id:', 'transactionId', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                    echo form_input(array(
                        'id' => 'transactionId',
                        'name' => 'transactionId',
                        'class' => 'form-control',
                        'required' => 'required',
                        'value' => ($transaction_details_model->get('transactionId')) ? $transaction_details_model->get('transactionId') : ''
                    ));
                } else {
                    echo text($transaction_details_model->get('transactionId'));
                }
                    ?>

                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Driving License No
                    echo form_label('Invoice No:', 'invoiceNo', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                     if ($view_mode == EDIT_MODE) {
                    echo form_input(array(
                        'id' => 'invoiceNo',
                        'name' => 'invoiceNo',
                        'class' => 'form-control',
                        'required' => 'required',
                        'value' => ($transaction_details_model->get('invoiceNo')) ? $transaction_details_model->get('invoiceNo') : ''
                    ));
                } else {
                    echo text($transaction_details_model->get('invoiceNo'));
                }
                    ?>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                    <?php
                    // validation for driver Driving License ExpireDate
                    echo form_label('Other Charges:', 'otherCharge', array(
                        'class' => (($view_mode == VIEW_MODE)) ? '' : 'required'
                    ));
                    if ($view_mode == EDIT_MODE) {
                    echo form_input(array(
                        'id' => 'otherCharge',
                        'name' => 'otherCharge',
                        'class' => 'form-control',
                        'required' => 'required',
                        'pattern' => '[0-9]+([\.,][0-9]+)?',
                        'value' => ($transaction_details_model->get('otherCharge')) ? $transaction_details_model->get('otherCharge') : ''
                    ));
                } else {
                    echo text($transaction_details_model->get('otherCharge'));
                }
                    ?>

                </div>
                
            </div>
            
            
                         
            <div class="ln_solid"></div>
            <div <?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                    <button type="button" id="load_transaction_btn"
                            class="btn btn-primary">Cancel</button>
                    <button type="button" id="save_transaction_btn" class="btn btn-success" onclick="saveTransaction();">Submit</button>
                </div>
            </div>
            <?php  if($view_mode == VIEW_MODE) : ?>
                <div  class="form-group">
                    <center><button type="button" id="load_transaction_btn" class="btn btn-primary" >Back</button></center>
                </div>  
            <?php endif ; ?>
                    <?php
                    echo form_close();
                    ?>
        </div>
    </div>
</div>

