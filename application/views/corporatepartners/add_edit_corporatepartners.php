<?php
$view_mode = $mode;
?>
<div id="corporatepartners-details-information"
	class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Add Corporate/Partners</h2>
			<ul class="nav navbar-right panel_toolbox">
				<!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li>-->
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<br />
			<?php
			
			$form_attr = array (
					'name' => 'edit_corporatepartners_form',
					'id' => 'edit_corporatepartners_form',
					'method' => 'POST',
					'class' => 'form-horizontal form-label-left' 
			);
			echo form_open ( base_url ( '' ), $form_attr );
			// corporatepartners id by default is -1
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'corporatepartners_id',
					'name' => 'id',
					'value' => ($corporatepartners_model->get ( 'id' )) ? $corporatepartners_model->get ( 'id' ) :- 1 
			) );
			
			echo form_input ( array (
					'type' => 'hidden',
					'id' => 'companyType',
					'name' => 'companyType',
					'value' => ($corporatepartners_model->get ( 'companyType' )) ? $corporatepartners_model->get ( 'companyType' ) :Company_Type_Enum::COMPANY
			) );
			
			?>
			
				<div class="form-group">
					<?php
					// validation for corporatepartners name
					echo form_label ( 'Corporate/Partners Name', 'name', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'name',
									'name' => 'name',
									'class' => 'form-control',
									'required' => 'required',
									'placeholder'=>'Eg: Company name',
									//'pattern'=>'[a-zA-Z\s]{2,30}',
									//'title'=>'Company name should only letters  & space',
									'value' => ($corporatepartners_model->get ( 'name' )) ? $corporatepartners_model->get ( 'name' ) : '' 
							) );
						} else {
							echo text ( $corporatepartners_model->get ( 'name' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					// validation for corporatepartners address
					echo form_label ( 'Address', 'address', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for corporatepartners address
						if ($view_mode == EDIT_MODE) {
							echo form_textarea ( array (
									'id' => 'address',
									'name' => 'address',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z0-9.#,-\s]',
									'rows' => '3',
									'value' => ($corporatepartners_model->get ( 'address' )) ? $corporatepartners_model->get ( 'address' ) : '' 
							) );
						} else {
							echo text ( $corporatepartners_model->get ( 'address' ) );
						}
						?>
					</div>

			</div>
			<div class="form-group">
					<?php
					// validation for corporatepartners email
					echo form_label ( 'Email Id', 'email', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for corporatepartners email
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'email',
									'name' => 'email',
									'class' => 'form-control col-md-7 col-xs-12',
									'placeholder'=>'Eg: example@gmail.com',
									'pattern'=>'[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
									'required' => 'required',
									'value' => ($corporatepartners_model->get ( 'email' )) ? $corporatepartners_model->get ( 'email' ) : '' 
							) );
						} else {
							echo text ( $corporatepartners_model->get ( 'email' ) );
						}
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					// validation for corporatepartners email
					echo form_label ( 'Phone', 'phone', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for corporatepartners email
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'phone',
									'name' => 'phone',
									'class' => 'form-control col-md-7 col-xs-12',
									'placeholder'=>'Start Landline with STD code/Mobile with zero(0)',
									'pattern'=>'[0]\d{9,10}',
									'required' => 'required',
									'value' => ($corporatepartners_model->get ( 'phone' )) ? $corporatepartners_model->get ( 'phone' ) : '' 
							) );
						} else {
							echo text ( $corporatepartners_model->get ( 'phone' ) );
						}
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					// validation for corporatepartners description
					echo form_label ( 'Description', 'description', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_textarea ( array (
									'id' => 'description',
									'name' => 'description',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'pattern'=>'[a-zA-Z0-9.#,-\s]',
									'rows' => '3',
									'value' => ($corporatepartners_model->get ( 'description' )) ? $corporatepartners_model->get ( 'description' ) : '' 
							) );
						} else {
							echo text ( $corporatepartners_model->get ( 'description' ) );
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					// validation for corporatepartners website URL
					echo form_label ( 'Website URL', 'url', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for corporatepartners url
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'url',
									'name' => 'url',
									'class' => 'form-control col-md-7 col-xs-12',
									//'required' => 'required',
									'placeholder'=>'Eg: https://example.com / http://example.com',
									//'pattern'=>'https?://[A-Za-z0-9._]+.?[a-z]{1,4}.?[a-z]{1,3}',
									'value' => ($corporatepartners_model->get ( 'url' )) ? $corporatepartners_model->get ( 'url' ) : '' 
							) );
						} else {
							echo text ( $corporatepartners_model->get ( 'url' ) );
						}
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					// validation for corporatepartners contact Name
					echo form_label ( 'Contact Person Name', 'contactName', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for corporatepartners email
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'contactName',
									'name' => 'contactName',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'placeholder'=>'Eg: Contact person name',
									'pattern'=>'[a-zA-Z\s]{2,50}',
									'value' => ($corporatepartners_model->get ( 'contactName' )) ? $corporatepartners_model->get ( 'contactName' ) : '' 
							) );
						} else {
							echo text ( $corporatepartners_model->get ( 'contactName' ) );
						}
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					// validation for corporatepartners contact Email
					echo form_label ( 'Contact Person Email Id', 'contactEmail', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for corporatepartners email
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'contactEmail',
									'name' => 'contactEmail',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'placeholder'=>'Eg: example@gmail.com',
									'pattern'=>'[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$',
									'value' => ($corporatepartners_model->get ( 'contactEmail' )) ? $corporatepartners_model->get ( 'contactEmail' ) : '' 
							) );
						} else {
							echo text ( $corporatepartners_model->get ( 'contactEmail' ) );
						}
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					// validation for corporatepartners contact Mobile
					echo form_label ( 'Contact Person Mobile', 'contactMobile', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for corporatepartners email
						if ($view_mode == EDIT_MODE) {
							echo form_input ( array (
									'id' => 'contactMobile',
									'name' => 'contactMobile',
									'class' => 'form-control col-md-7 col-xs-12',
									'required' => 'required',
									'placeholder'=>'Eg: 9876543210',
									'title'=>'Mobile number starts with 9/8/7 , only 10 digits',
									'pattern'=>'[789]\d{9}',
									'value' => ($corporatepartners_model->get ( 'contactMobile' )) ? $corporatepartners_model->get ( 'contactMobile' ) : '' 
							) );
						} else {
							echo text ( $corporatepartners_model->get ( 'contactMobile' ) );
						}
						?>
					</div>
			</div>
			<!-- <div class="form-group">
					<?php
					// validation for corporatepartners companyType
					/* echo form_label ( 'Company Type', 'companyType', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					 */
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for corporatepartners Company type
						/* if ($view_mode == EDIT_MODE) {
							echo form_dropdown ( 'companyType', $company_type_list, $corporatepartners_model->get ( 'companyType' ), array (
									'id' => 'companyType',
									'class' => 'form-control',
									'required' => 'required' 
							) );
						} else {
							echo text ( $company_type_list[$corporatepartners_model->get ( 'companyType' )]);
						} */
						
						?>
					</div>
			</div> -->

			<div class="form-group">
					<?php
					echo form_label ( 'Country Name:', 'countryId', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_dropdown ( 'countryId', $country_list, $corporatepartners_model->get ( 'countryId' ), array (
									'id' => 'countryId',
									'class' => 'form-control',
									'required' => 'required' 
							) );
						} else {
							echo text ( $country_list[$corporatepartners_model->get ( 'countryId' )]);
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'State Name:', 'stateId', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_dropdown ( 'stateId', $state_list, $corporatepartners_model->get ( 'stateId' ), array (
									'id' => 'stateId',
									'class' => 'form-control',
									'required' => 'required' 
							) );
						} else {
							echo text ( $state_list[$corporatepartners_model->get ( 'stateId' )]);
						}
						
						?>
					</div>
			</div>
			<div class="form-group">
					<?php
					echo form_label ( 'City Name:', 'cityId', array (
							'class' => (($view_mode == VIEW_MODE)) ? 'control-label col-md-3 col-sm-3 col-xs-12' : 'control-label col-md-3 col-sm-3 col-xs-12 required' 
					) );
					
					?>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php
						// validation for passenger first name
						if ($view_mode == EDIT_MODE) {
							echo form_dropdown ( 'cityId', $city_list, $corporatepartners_model->get ( 'cityId' ), array (
									'id' => 'cityId',
									'class' => 'form-control',
									'required' => 'required' 
							) );
						} else {
							echo text ( $city_list[$corporatepartners_model->get ( 'cityId' )]);
						}
						
						?>
					</div>
			</div>


			<div class="ln_solid"></div>
			<div <?php echo ($view_mode == VIEW_MODE)?'class="form-group hidden" ':'class="form-group"'; ?>>
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<button type="button" id="load_partners_btn"
						class="btn btn-primary">Cancel</button>
					<button type="button" id="save_corporatepartners_btn"
					data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing"
                                        class="btn btn-success" onclick="saveCorporatePartners();">Save</button>
				</div>
			</div>
                        <?php  if($view_mode == VIEW_MODE) : ?>
                            <div  class="form-group">
                                <center><button type="button" id="load_partners_btn" class="btn btn-primary" >Back</button></center>
                            </div>  
                        <?php endif ; ?>
			<?php
                            echo form_close ();
			?>
		</div>
	</div>
</div>

