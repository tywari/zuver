<div id="corporate-partner-details-information" class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>
				Corporate/Partner's List
			</h2>
			<!--<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				 <li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><i
						class="fa fa-wrench"></i></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Settings 1</a></li>
						<li><a href="#">Settings 2</a></li>
					</ul></li>
				<li><a class="close-link"><i class="fa fa-close"></i></a></li> 
				
			</ul>
			<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" id="search_corporate_partner_content" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" id="search_corporate_partner_btn" type="button">Go!</button>
                    </span>
                    
                  </div>
                  <div><span>Search by Company Name, Passenger, </span></div>
                </div>
             </div>-->
             <div class="title_right pull-right">
                
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					<a href="<?php echo base_url('corporatepartners/add')?>"><button id="add_corporatepartners_btn"
						class="btn btn-primary">Add Corporate/Partners</button></a>
					
				</div>
                
             </div>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			<table id="datatable-checkbox"
				class="table table-striped table-bordered datatable-button-init-collection">
				<thead>
					<tr>
						<th><!-- <input type="checkbox" id="check-all" class="flat"> --></th>
						<!-- <th>Sl no</th> -->
						<th>Action</th>
						<th>Company Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Website</th>
						<th>Contact Person</th>
						<th>Company Type</th>
						<th>Created Date</th>
						<th>Status</th>
						
					</tr>
				</thead>


				<tbody>
				<?php 
				//sl no intialization
				//$i=1;
				//$corporatepartner_model_list=array(0=>array('name'=>'Zuver','email'=>'globalkiran@gmail.com','createdDate'=>'21-09-2016 10:10:00','url'=>'www.zuer.in','contactName'=>'Zuver','companyType'=>'Company','status'=>1),
				//					   1=>array('name'=>'Zoomcar','email'=>'globalkiran@gmail.com','createdDate'=>'21-09-2016 10:10:00','url'=>'www.zoomcar.in','contactName'=>'ZoomCar','companyType'=>'Partner','status'=>0),
				//		2=>array('name'=>'Global','email'=>'globalkiran@gmail.com','mobile'=>'9901206631','createdDate'=>'21-09-2016 10:10:00','url'=>'www.global.in','contactName'=>'ZoomCar','companyType'=>'Company','status'=>1)
			//	);
				foreach ($corporatepartner_model_list as $list)
				{
					
				echo '<tr>';
					
				echo '<td></td>';//<input type="checkbox" class="flat" name="table_records">
				//echo '<th>'.$i.'</th>';
				echo '<td>'
						. '<a href="javascript:void(0);" id="viewCorpratePartner-'.$list->id.'" onclick="viewCorpratePartner(this.id);" class="userlink">'
								. '<span class="glyphicon glyphicon-eye-open"></span></a>'
										. ' / <a href="javascript:void(0);" id="editCorpratePartner-'.$list->id.'" onclick="editCorpratePartner(this.id);" class="userlink">'
												. '<span class="glyphicon glyphicon-edit"></span></a>'
														. '</td>';
				echo '<td>'.$list->name.'</td>';
				echo '<td>'.$list->email.'</td>';
				echo '<td>'.$list->phone.'</td>';
				echo '<td>'.$list->url.'</td>';
				echo '<td>'.$list->contactName.'</td>';
				echo '<td>'.$company_type_list[$list->companyType].'</td>';
				echo '<td>'.$list->createdDate.'</td>';
				
				//Status toogle
				$active=($list->status==Status_Type_Enum::ACTIVE)?'btn-primary active-status':'btn-default" onclick="changeCorporatePartnersStatus(this.id);';
				$deactive=($list->status==Status_Type_Enum::INACTIVE)?'btn-primary active-status':'btn-default" onclick="changeCorporatePartnersStatus(this.id);';
				echo '<td> <ul class="list-inline"> <li>';
				echo '<div class="btn-group btn-toggle">';
				echo '<button id="active-'.$list->id.'" class="btn btn-xs '.$active.'">Active</button>';
				echo '<button id="deactive-'.$list->id.'" class="btn btn-xs '.$deactive.'">Deactive</button>';
				echo '</div></li></ul></td>';
				
				
				echo '</tr>';
				//$i++;
				}
	            ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

