<?php
require_once(APPPATH . 'config/base_enum.php');

class Company_Type_Enum extends Base_Enum {

	const
	COMPANY =   'C',//35
	PARTNER = 'P',//36
	C= 'Company',
	P= 'Partner';
		
}