<?php
require_once(APPPATH . 'config/base_enum.php');

class Manipulation_Type_Enum extends Base_Enum {

	const
	NONE = 'N',//81
	ADDITION  = 'A',//82
	SUBTRACTION = 'S',//83
	N= 'None',
	A= 'Addition',
	S= 'Subtraction';
}