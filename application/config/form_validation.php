<?php
$config = array(
                 'passenger' => array(
                                    array(
                                            'field' => 'firstName',
                                            'label' => 'Passenger First Name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'lastName',
                                            'label' => 'Passenger Last Name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'mobile',
                                            'label' => 'Passenger Mobile',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Passenger Email',
                                            'rules' => 'required|valid_email'
                                         )
                                    ),
                 'driver' => array(
                                     array(
                                            'field' => 'firstName',
                                            'label' => 'Driver First Name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'lastName',
                                            'label' => 'Driver Last Name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'mobile',
                                            'label' => 'Driver Mobile',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Driver Email',
                                            'rules' => 'required|valid_email'
                                         )
                                    ),
					'driverpersonaldetails' => array(
							array(
									'field' => 'drivingLicenseNo',
									'label' => 'Driver License No',
									'rules' => 'required'
							),
							array(
									'field' => 'drivingLicenseExpireDate	',
									'label' => 'Driver Driving License ExpireDate',
									'rules' => 'required'
							),
							array(
									'field' => 'gender',
									'label' => 'Driver Gender',
									'rules' => 'required'
							),
							array(
									'field' => 'drivingLicenseImage',
									'label' => 'Driver Driving License Image',
									'rules' => 'required'
							),
							array(
									'field' => 'addressProofNo',
									'label' => 'Driver Address Proof No',
									'rules' => 'required'
							),
							array(
									'field' => 'addressProofImage	',
									'label' => 'Driver 	Address Proof Image',
									'rules' => 'required'
							),
							array(
									'field' => 'identityProofNo',
									'label' => 'Driver 	Identity Proof No',
									'rules' => 'required'
							),
							array(
									'field' => 'identityProofImage',
									'label' => 'Driver 	Identity Proof Image',
									'rules' => 'required'
							),
							array(
									'field' => 'panCardNo',
									'label' => 'Driver 	Pan Card No',
									'rules' => 'required'
							),
							
							array(
									'field' => 'panCardImage',
									'label' => 'Driver PanCard Image',
									'rules' => 'required'
							)
					)
               );