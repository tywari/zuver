<?php
require_once(APPPATH . 'config/base_enum.php');

class Payment_Mode_Enum extends Base_Enum {

	const
	CASH = 'C',//22
	WALLET  =  'W',//23
	PAYTM = 'P',//24
	INVOICE = 'I',//84
	C= 'Cash',
	W= 'Wallet + Cash',
	P= 'PAYTM',
	I= 'Invoice';	
}