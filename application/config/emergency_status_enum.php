<?php
require_once(APPPATH . 'config/base_enum.php');

class Emergency_Status_Enum extends Base_Enum {

	const
	OPEN =  'OP',//62
	INACTION = 'IA',//63
	WRONGREQUEST =  'WR',//64
	CLOSED = 'CL',//65
	OP= 'Open',
	IA= 'In-Action',
	WR='Wrong-Request',
	CL='Closed';
		
}