<?php

class Base_Enum
{
   private static $_value = null;

    public function __construct($value){
        $this->_value = $value;
    }
    
    public function toString() {
        return (string) $this->_value;
    }
    
    public function setValue($value) {
        $this->_value = $value;
    }
    public function getValue() {
        return $this->_value;
    }  
    
}