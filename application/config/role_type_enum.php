<?php
require_once(APPPATH . 'config/base_enum.php');

class Role_Type_Enum extends Base_Enum {

	const
	STAFF =  'ST',//1
	SUPERVISOR  =  'SV',//2
	MANAGER = 'MA',//3
	ACCOUNTANT = 'AC',//4
	COMPANY = 'CO',//5
	ADMIN = 'AD',//6
	SUPER_ADMIN = 'SA',//7
	ST= 'Staff',
	SV= 'Supervisor',
	MA= 'Manager',
	AC= 'Accountant',
	CO= 'Company',
	AD= 'Admin',
	SA= 'Super Admin';
	
}