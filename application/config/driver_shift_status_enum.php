<?php
require_once(APPPATH . 'config/base_enum.php');

class Driver_Shift_Status_Enum extends Base_Enum {

	const
	SHIFTIN = 'IN',//59
	SHIFTOUT  = 'OUT',//60
	
	IN= 'Shift In',
	OUT= 'Shift Out';
	
}