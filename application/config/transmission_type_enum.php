<?php
require_once(APPPATH . 'config/base_enum.php');

class Transmission_Type_Enum extends Base_Enum {

	const
	MANUAL =   'M',//25
	AUTOMATIC = 'A',//26
	BOTH ='B',//77
	M= 'Manual',
	A= 'Automatic',
	B='Both';
		
}