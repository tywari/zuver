<?php
require_once(APPPATH . 'config/base_enum.php');

class Measuring_Type_Enum extends Base_Enum {

	const
	KILOMETER = 'Km',//8
	TRIP  = 'Trip',//9
	HOURLY = 'Hr',//10
	DAILY = 'Day',//11
	Km= 'KILOMETER',
	Trip= 'TRIP',
	Day= 'DAILY',
	Hr= 'HOURLY';
	
}