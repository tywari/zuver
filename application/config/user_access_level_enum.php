<?php

require_once(APPPATH.'config/base_enum.php');

class User_Access_Level_Enum extends Base_Enum{

    
    const 
        NO_ACCESS = 0,
        READ_ONLY = 1,
        EDIT = 2;  
    
}