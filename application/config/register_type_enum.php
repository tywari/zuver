<?php
require_once(APPPATH . 'config/base_enum.php');

class Register_Type_Enum extends Base_Enum {

	const
	NONE = 'N',//92
	MANUAL =   'M',//93
	FACEBOOK = 'F',//14
	GOOGLE = 'G',//15
	OTHERS = 'O',//16
	N = 'None',
	M = 'Manual',
	F = 'Facebook',
	G = 'Google',
	O = 'Others';
	
	
}