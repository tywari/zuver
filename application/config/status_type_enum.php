<?php
require_once(APPPATH . 'config/base_enum.php');

class Status_Type_Enum extends Base_Enum {

	const
	ACTIVE =   'Y',
	INACTIVE  =   'N',
	
	Y = 'Active',
	N = 'In-Active';
	
	
}