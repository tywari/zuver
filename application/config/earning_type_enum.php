<?php
require_once(APPPATH . 'config/base_enum.php');

class Earning_Type_Enum extends Base_Enum {

	const
	ConveyanceAllowance  =   'CA',//110
	MobileAllowance = 'MA',//111
	ProductivityAllowanceType1 = 'PAT1',//112
	ProductivityAllowanceType2 = 'PAT2',//113
	ProductivityAllowance='PA',//114
	BASIC =   'B',//115
	NightEarnings='NE', //116
	B='Basic',
	CA='Conveyance Allowance',
	MA='Mobile Allowance',
	PAT1='Productivity Allowance Type 1',
	PAT2='Productivity Allowance Type 2',
	PA='Productivity Allowance',
	NE='Night Earnings';
		
}