<?php
require_once(APPPATH . 'config/base_enum.php');

class Bill_Type_Enum extends Base_Enum {

	const
	HOURLY = 'N',//68
	FIXED  = 'F',//69
	DISTANCE = 'D',//70
	TIME  = 'T',//71
	MONTHLY  = 'M',//76
	WEEKLY = 'W',//78
	N= 'Hourly',
	F= 'Fixed',
	D= 'Distance',
	T= 'Time',
	M= 'Monthly',
	W='Weekly';
}