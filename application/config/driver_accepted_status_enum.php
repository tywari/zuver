<?php
require_once(APPPATH . 'config/base_enum.php');

class Driver_Accepted_Status_Enum extends Base_Enum {

	const
	NONE = 'N', //73
	ACCEPTED =   'A',//27
	REJECTED  =   'R',//28
	TIMEOUT = 'T',//29
	N = 'None',
	A= 'Accepted',
	R= 'Rejected',
	T= 'Time Out';
	
}