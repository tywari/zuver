<?php

require_once(APPPATH . 'config/base_enum.php');

class User_Type_Enum extends Base_Enum {
    //UNKNOWN_USER  - Should not have access to project
    //GENERAL_USER - are not associated to the project but still they can see PLC, TRACKS and Resources in readonly Mode
    //GFP_LEAD has access view and edit mode accept capex and Admin Menu
    //ADMIN has full access, 
    //PROJECT_MANAGER and PM_DESIGNATE has full access to PLC, Tracks, Reports, Dashboard and Restricted access to resource (can edit Release info and Team Members
    //REVIEWER, ORG_READINESS_LEAD, UAT_LEAD, TRAINING_LEAD and OR_DESIGNATE  has same rights that is Edit access to Tracks, Readonly access to rest except Add Release and Admin.
    
    const
        PASSENGER ='P',
        DRIVER = 'D', 
        OTHER = 'O',
        NONE = 'N';

}
