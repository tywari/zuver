<?php
require_once(APPPATH . 'config/base_enum.php');

class Transaction_Type_Enum extends Base_Enum {

	const
	NONE =  'NT',//94
	REFERRAL =  'RF',//50
	PAYUBIZ = 'PB',//52
	TRIP = 'TR',//95
	MANUAL ='MN',//96
	NT = 'None',
	RF = 'Referral',
	PB = 'PAYUBIZ',
	TR = 'Trip',
	MN = 'Manual';
			
}