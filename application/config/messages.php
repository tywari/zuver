<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Save, Update , failure messages
$config['msg']['success'] = ' <strong>Well done!</strong> You have successfully saved. ';
$config['msg']['updated'] = ' <strong>Well done!</strong> You have successfully updated. ';
$config['msg']['failed'] = ' <strong>Sorry!</strong> Failed to save/update. Please try again later.. ';
$config['msg']['ok'] = " Don't do anything. ";

//Status success & failure messages
$config['msg']['status_change_success'] = 'You have successfully changed the status. ';
$config['msg']['status_change_failed'] = '<strong>Sorry!</strong> Failed to change the status. Please try again later.. ';

//Status success & failure messages
$config['msg']['delete_success'] = 'You have successfully deleted. ';
$config['msg']['delete_failed'] = '<strong>Sorry!</strong> Failed to delete. Please try again later.. ';

//rate card exits
$config['msg']['rate_card_exits']='Rate card already exists with combination of trip type, city, company';

//Email & mobile & pomocode Exists check messages
$config['msg']['email_exists']='Email already exists';
$config['msg']['mobile_exists']='Mobile number already exists';

$config['msg']['email_not_exists']='Email is accepted';
$config['msg']['mobile_not_exists']='Mobile number is accepted';

$config['msg']['promocode_exists']='Promocode already exists';
$config['msg']['promocode_not_exists']='Promocode is accepted';

$config['msg']['assign_success']='Driver assigned to trip Sucessfully';
$config['msg']['assign_failed']='Driver assigned to trip Failed';

$config['msg']['payment_confirmation_success']='Payment confirmation Success!! Trip Status changed to complete.';
$config['msg']['payment_confirmation_failed']='Payment confirmation Failed!!';

//API messages
$config['api']['auth_failed']='Authentication Failed';

$config['api']['invalid_user']='Invalid User. Kindly register';
$config['api']['invalid_trip']='Invalid Trip Id';
$config['api']['invalid_subtrip']='Invalid Sub-Trip Id';
$config['api']['invalid_trip_request']='We are not able to get Trip Request Details';
$config['api']['invalid_trip_status'] = "Invalid Trip status to start a sub-trip.";
$config['api']['invalid_email']='Invalid Email Id';
$config['api']['invalid_page']='Invalid Pagename';
$config['api']['invalid_bill_type']='Select Proper billing type';
$config['api']['invalid_drop_time']='Drop time is less than pickup time';
$config['api']['invalid_master_trip_drop_time']='Drop time is less than Sub-Trip drop time';
$config['api']['invalid_rate_card']='Invalid Trip type / City.We are not able to get Rate card details';

$config['api']['data_not_found']='Required data Contents not found';

$config['api']['success']='Success';
$config['api']['failed']='Failed. Please try again later.';

$config['api']['reset_password']='Kindly check your SMS/Mail regarding Reset Password';
$config['api']['confirm_password']='Confirm password must be the same as new password.';
$config['api']['incorrect_old_password']='Old password is Incorrect.';
$config['api']['password_success']='Password has been changed successfully.';
$config['api']['old_new_password_same']='Old Password and New Password are same.';

$config['api']['mandatory_data']='You have missed the required values.';

$config['api']['rating_success']='Thank you for your rating';
$config['api']['rating_failed']='Failed to rating. Please try again!!';

$config['api']['email_exists']='Email already exists';
$config['api']['mobile_exists']='Mobile number already exists';
$config['api']['email_mobile_exists']='Mobile / Email already exists';
$config['api']['account_exists']='Enter valid Email/Mobile & Password';//not exists in Zuver account / User has been blocked by admin !';
$config['api']['valid_referral_code']='Please enter valid referral code else leave it blank.';

$config['api']['signup_success']='Account Details have been saved/updated successfully.';
$config['api']['signup_failed']='Failed to save/update, So please try again.';
$config['api']['signup_success_image_failed']='Account Details saved but image upload failed. Please try again.';

$config['api']['otp']='Kindly check your SMS/Mail regarding OTP';
$config['api']['otp_success']='OTP successfully sent to your Mobile & Email.';
$config['api']['otp_verify_failed']='OTP not verified';
$config['api']['invaild_otp']='Invalid OTP.Please Try Again';

$config['api']['activation_success']='Your account has been activated';

$config['api']['login_success']='You have successfully logged in to Zuver Application ';
$config['api']['login_mandatory']='Email or phone number required';

$config['api']['logout_success']='You have logged out successfully.';
$config['api']['logout_failed']='Logout failed.. Please try again later.';

$config['api']['facebook_login_failed']='Facebook connection failed.. Please try again later.';
$config['api']['facebook_email_not_found']='Email is not found in facebook connection';

$config['api']['promocode_total_limit']='Promotion code usage total limit exceeded';
$config['api']['promocode_user_limit']='Promotion code usage user limit exceeded';
$config['api']['promocode_expiry']='Promotion code was expired';
$config['api']['promocode_start']='You can not use this promotion code now. Wait for promotion available';
$config['api']['invalid_promocode']='Invalid promocode please check or leave it as empty';
$config['api']['promocode_success']='Promocode Applied successfully';

$config['api']['booking_success']='Your booking is confirmed. You will receive an SMS and Email with your driver details';
$config['api']['booking_failed']='Failed Booking Your trip! Try again';
$config['api']['lat_long_mandatory']='Latitude and Longitude cannot be zero';

$config['api']['tariff_failed']='Fare details not found';


$config['api']['fav_add_success']='Favourite place added successfully.';
$config['api']['fav_add_failed']='Favourite place adding failed.. Please try again later';
$config['api']['fav_edit_success']='Favourite place edited successfully.';
$config['api']['fav_edit_failed']='No changes made.. Please try again later';
$config['api']['fav_delete_success']='Your favorite place has been removed successfully.';
$config['api']['fav_delete_failed']='Favourite place Deleted Successfully!!';
$config['api']['fav_exists']='Favourite place already exists.';


$config['api']['zuver_notice']='ZUVER support team,I would like to bring to your notice.';
$config['api']['no_driver']='Driver not available at the moment.. Please contact company';

$config['api']['passenger_cancel_success']='Your booking has been cancelled upon your request';
$config['api']['passenger_cancel_failed']='Trip cancelled failed.. Please try again later.';

$config['api']['trip_confirmed']='Your booking has been confirmed.';
$config['api']['trip_assigned']='You have a trip assigned';
$config['api']['trip_not_started']='Trip not yet to started.';
$config['api']['trip_started']='Trip already started.';
$config['api']['trip_progress']='Your trip is In-progress.';
$config['api']['trip_completed']='Trip fare already updated';
$config['api']['trip_waiting_payment']='Your trip is completed, Waiting for Payment.';
$config['api']['trip_waiting_paytm_payment']='Trip Completed, Please pay Trip fare via PAYTM';
$config['api']['trip_reject_passenger']='Trip has been already cancelled by Customer';
$config['api']['trip_reject_driver']='Trip is already rejected by driver';
$config['api']['trip_driver_arrived']='Your Driver has Arrived.';

$config['api']['sos_add_success']='Emergency contact added Successfully';
$config['api']['sos_add_failed']='Emergency contact added failed.. Please try again later.';
$config['api']['sos_delete_success']='Emergency Contact Deleted Successfully!!';
$config['api']['sos_delete_failed']='Failed to delete contact may be Invalid passenger Id or Emergency Id';
$config['api']['sos_limit_exceeds']='Emergency contact limit exceeded';
$config['api']['sos_notice']='Emergency Contact notified';

$config['api']['paytm_success']='Payment Transaction Successfully Done!!!';
$config['api']['paytm_failed']='Payment Transaction Failed.. Please try again later';

$config['api']['wallet_success']='Amount successfully added to Passenger Zuver money';
$config['api']['wallet_failed']='Amount failed added to Passenger Zuver money';
$config['api']['wallet_empty']='Amount should be greater than zero';

$config['api']['request_set']='Request type is set.';

$config['api']['driver_account_exists']='Enter valid Email/Mobile & Password / Check with Zuver management team!';
$config['api']['driver_login_status']='You are already logged-in in another device kindly contact Zuver Customer Care to allow account access from this device';
$config['api']['driver_login_failed']='You are not logged in. Please log in for further process.';
$config['api']['driver_shift_in']='Your are in On-Duty';
$config['api']['driver_shift_already_in']='Your are already in On-Duty';
$config['api']['driver_shift_out']='Your are in Off-Duty';
$config['api']['driver_shift_already_out']='Your are already in Off-Duty';
$config['api']['driver_confirmed']='You have confirmed the booking.';
$config['api']['driver_rejected']='Request has been rejected';
$config['api']['driver_arrival']='Your arrival has been send to Customer';
$config['api']['driver_start_trip']='Trip has been started';
$config['api']['driver_start_sub_trip']='Sub-Trip has been started';
$config['api']['driver_end_sub_trip']='Sub-Trip has been completed';
$config['api']['exist_running_subtrip']='End running subtrip to start new sub-trip';
$config['api']['driver_start_complete']='Your trip has been completed. Please process payment.';
$config['api']['driver_rejected_reason_success']='Reject reason updated successfully';
$config['api']['driver_rejected_reason_failed']='Reject reason updating failed.. Please try again later';
$config['api']['trip_status_failed']='Failed to update trip status / trip request status for trip_id=';
$config['api']['driver_booking_request']='New booking request sent';

$config['api']['trip_dispatcher_cancel']='Dispatcher has been cancelled the Trip';

$config['api']['driver_location_update']='Driver Location history updated';

$config['api']['trip_finalize_success']='Trip Fare Updated Successfully';
$config['api']['trip_finalize_failed']='Trip Fare Updated failed.. Please try again later';
$config['api']['trip_finalize_waiting_paytm_payment']='Trip Fare Updated Successfully. Please wait till PAYTM payment confirmation message';
$config['api']['invalid_trip_finalize_data']='Trip Id & total trip cost data is missing';

//zoomcar trip control message
$config['msg']['zoom_trip_cancel_success'] = 'You have successfully cancelled the ZoomCar Trip. ';
$config['msg']['zoom_trip_cancel_failed'] = ' You have failed to cancel the ZoomCar Trip. Please try again later.. ';
/* End of file messages.php */
/* Location: ./application/config/messages.php */