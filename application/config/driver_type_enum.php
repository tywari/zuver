<?php
require_once(APPPATH . 'config/base_enum.php');

class Driver_Type_Enum extends Base_Enum {

	const
	FIXEDBASED = 'FD',//66
	COMMISSIONBASED  = 'CM',//67
	FD= 'Fixed Based',
	CM= 'Commission Based',
	TRIPBASED = 'TBD',//103
	HOURLYBASED  = 'HBD',//104
	TBD= 'Trip Based Driver',
	HBD= 'Hourly Based Driver',
	B2C = 'B2C';//105
		
}