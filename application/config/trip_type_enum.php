<?php
require_once(APPPATH . 'config/base_enum.php');

class Trip_Type_Enum extends Base_Enum {

	const
	ONEWAY_TRIP =   'O',//19
	RETURN_TRIP  =   'R',//20
	OUTSTATION_TRIP = 'X',//21
	OUTSTATION_TRIP_ONEWAY = 'XO',//79
	OUTSTATION_TRIP_RETURN = 'XR',//80
	O = 'One Way',
	R = 'Return',
	X = 'Out Station',
	XO = 'Out Station Oneway',
	XR = 'Out Station Return';
	//B2B ='B2B';
		
}