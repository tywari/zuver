<?php
require_once(APPPATH . 'config/base_enum.php');

class Signup_Type_Enum extends Base_Enum {

	const
	NONE = 'N',//72
	WEBSITE =   'W',//12
	MOBILE  =   'M',//13
	BACKEND = 'B',//91
	N = 'None',
	W = 'Website',
	M = 'Mobile',
	B = 'Backend';
	
	
}