<?php
require_once(APPPATH . 'config/base_enum.php');

class Device_Type_Enum extends Base_Enum {

	const
	NO_DEVICE = 'N',//8
	ANDROID  = 'A',//9
	IPHONE = 'I',//10
	WINDOWS = 'W',//11
	N= 'No Device',
	A= 'Android',
	I= 'Iphone',
	W= 'Windows';
	
}