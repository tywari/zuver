<?php
require_once(APPPATH . 'config/base_enum.php');

class Favourite_Type_Enum extends Base_Enum {

	const
	PICKUP = 'P',//17
	DROP  =  'D',//18
	None = 'N',//61
	P= 'PickUp',
	D= 'Drop',
	N= 'None';
		
}