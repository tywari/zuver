<?php
require_once(APPPATH . 'config/base_enum.php');

class License_Type_Enum extends Base_Enum {

	const
//	NONE = 'NN',//100
	NONTRANSPORT  = 'NT',//101
	TRANSPORT = 'TR',//102
//	NN= 'None',
	NT= 'Non Transport',
	TR= 'Transport';
}