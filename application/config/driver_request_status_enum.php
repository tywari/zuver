<?php
require_once(APPPATH . 'config/base_enum.php');

class Driver_Request_Status_Enum extends Base_Enum {

	const
	AVAILABLE_TRIP =   'AT',//30
	SENT_TO_DRIVER  =   'WR',//31
	DRIVER_ACCEPTED = 'DA',//32
	DRIVER_REJECTED = 'DR',//33
	PASSENGER_CANCELLED = 'PC',//34
	COMPLETED_TRIP = 'CT',//74
	AT= 'Available Trip',
	WR= 'Sent To Driver & Waiting For Reply',
	DA= 'Driver Accepted',
	DR= 'Driver Rejected',
	PC= 'Passenger Cancelled',
	CT= 'Completed Trip';
	
}