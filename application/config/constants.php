<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('NULL_SELECT_OPTION_TEXT' , '-- Select --');

define('PATH_JS', 'public/js/');
define('PATH_CSS', 'public/css/');
define('PATH_IMG', 'public/images/');
define('PATH_FONT', 'public/font/');

define('PASSENGER_CONTENT_PATH', 'public/images/app/uploaded/passenger/');
define('DRIVER_CONTENT_PATH', 'public/images/app/uploaded/driver/');
define('USER_CONTENT_PATH', 'public/images/app/uploaded/user/');
define('BANK_CONTENT_PATH', 'public/images/app/uploaded/bank/');

define('VIEW_MODE','view');
define('EDIT_MODE','edit');
define('UNKNOWN_MODE','unknown');

define('TIMEZONE','Asia/Kolkata');
define('NO_DATE', -1);
define('DATE_TABLE_FORMAT', 'm/j/Y');
define('DATE_DROPDOWN_FORMAT', 'n/j/Y');
define('DATE_GRAPH','m/d/Y');
define('DATE_PAGE_FORMAT', 'M j, Y');
define('DATE_MYSQL_FORMAT', 'Y-m-d');
define('DATE_FILE_NAME_FORMAT', 'Y-m-d');
define('DATE_EXCEL_FORMAT', 'j-M-Y');
define('DATE_DISPLAY_FORMAT', 'd-m-Y');

define('NO_DATETIME', -1);
define('DATETIME_TABLE_FORMAT', 'm/j/Y H:i:s');
define('DATETIME_DROPDOWN_FORMAT', 'n/j/Y H:i:s');
define('DATETIME_GRAPH','m/d/Y H:i:s');
define('DATETIME_PAGE_FORMAT', 'M j, Y H:i:s');
define('DATETIME_MYSQL_FORMAT', 'Y-m-d H:i:s');
define('DATETIME_FILE_NAME_FORMAT', 'Y-m-d H:i:s');
define('DATETIME_EXCEL_FORMAT', 'j-M-Y H:i:s');
define('DATETIME_DISPLAY_24_FORMAT', 'd-m-Y H:i:s');
define('DATETIME_DISPLAY_12_FORMAT', 'd-m-Y h:i:s a');


///Temp constant for webservice
define('SKIP_CREDIT_CARD',0);
define('UNIT_NAME','KM');
define("CANCELLATION_FARE",0);
define('SMS_USERNAME',2000149475);
define('SMS_PASSWORD',"WE3euPLAB");
define('SMS',1);
define('SMTP',1);
DEFINE("CUSTOMERCARE_MUMBAI","2243686868");
DEFINE("CUSTOMERCARE_BANGALORE","8067650565");
DEFINE("CUSTOMERCARE_PUNE","2243686868");
DEFINE("CUSTOMERCARE_DEFAULT","2243686868");
define("APPLINK",'Android:https://play.google.com/store/apps/details?id=com.qwikdryver IOS:http://apple.co/1T3N8oD');
define("DRIVERAPPLINK",'Android:https://play.google.com/store/apps/details?id=com.zuver.driver&hl=en');
define("MOBILE_APPLINK",'http://zuver.in');
define("SMTP_EMAIL_ID",'support@zuver.in');
define("SMTP_EMAIL_PASSWORD",'support#@123');
define("SMTP_EMAIL_NAME",'Zuver');
define("COPYRIGHTS",'�2016 Zuver. All rights reserved.');
define("ZUVER_MIN_BALANCE",500);
define("DEFAULT_COMPANY_ID", 1);
define("DEFAULT_COMPANY_NAME", 'Zuver');
define("DEFAULT_CITY_ID", 1);
define("EMERGENCY_COUNT_LIMIT", 3);
define("DAY_START_TIME", '07:00:00');
define("DAY_END_TIME", '20:59:59');
define("NIGHT_START_TIME", '21:00:00');
define("NIGHT_END_TIME", '06:59:59');
define("OUT_STATION_TIME_LIMIT", 12);
define("ANDROID_PASSENGER_APP_VER", '2.2');
define("IPHONE_PASSENGER_APP_VER", '2.2');
define("DRIVER_APP_VER", '2.5.2');

define("ZOOMCAR_COMPANYID", 2);
define("ZOOMCAR_SECRETKEY",'globalkk');
//--------------------------------------------------


DEFINE('APPLICATION_NAME','Zuver');


DEFINE('GOOGLE_MAP_API_KEY',"AIzaSyAYsuBAzSSqzQll1FSzlnvPq3xbjLVKD9s");


DEFINE('GOOGLE_GEO_API_KEY',"AIzaSyBvUrhWyVKjXuwFy_vaVJvP_1rrrRDCmuI");

DEFINE('IPINFOAPI_KEY',"3aec9d045fb56ca9da8994707354ed3a85f6ea8ed850aafb284524eb6a5b3bbe");

DEFINE('ENCRYPT_KEY',"ndotencript_");

//set the headers here
header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.


