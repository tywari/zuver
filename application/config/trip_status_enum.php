<?php
require_once(APPPATH . 'config/base_enum.php');

class Trip_Status_Enum extends Base_Enum {

	const
	BOOKED =   'BKD',//39
	TRIP_CONFIRMED  =   'TCD',//40
	DRIVER_ACCEPTED = 'DAT',//41
	DRIVER_ARRIVED = 'DAD',//42
	READY_TO_START = 'RTS',//43
	IN_PROGRESS = 'IPR',//44
	WAITING_FOR_PAYMENT = 'WFP',//45
	WAITING_FOR_PAYTM_PAYMENT = 'WPP',//46
	TRIP_COMPLETED = 'TCT',//48
	CANCELLED_BY_PASSENGER = 'CBP',//48
	CANCELLED_BY_DRIVER = 'CBD',//49
	NO_NOTIFICATION ='NON',//75
	BKD = 'Booked',
	TCD = 'Trip Confirmed',
	DAT = 'Driver Accepted',
	DAD = 'Driver Arrived',
	RTS = 'Ready To Start',
	IPR = 'In Progress',
	WFP = 'Waiting For Payment',
	WPP = 'Waiting For PAYTM Payment',
	TCT = 'Trip Completed',
	CBP = 'Cancelled By Passenger',
	CBD = 'Cancelled By Driver',
	NON = 'No Notification';
	
}