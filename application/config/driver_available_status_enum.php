<?php
require_once(APPPATH . 'config/base_enum.php');

class Driver_Available_Status_Enum extends Base_Enum {

	const
	NOTAVAILABLE = 'N',//56
	FREE  = 'F',//57
	BUSY = 'B',//58
	N= 'Not Available',
	F= 'Free',
	B= 'Busy';
	
}