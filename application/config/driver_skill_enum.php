<?php
require_once(APPPATH . 'config/base_enum.php');

class Driver_Skill_Enum extends Base_Enum {

	const
	NONE = 'NN',//97
	LUXURY  = 'FL',//98
	NONLUXURY = 'NL',//99
	NN= 'None',
	FL= 'Luxury',
	NL= 'Non Luxury';
	
}