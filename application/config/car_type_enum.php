<?php
require_once(APPPATH . 'config/base_enum.php');

class Car_Type_Enum extends Base_Enum {

	const
	NONE = 'N',//85
	HATCHBACKS  =  'H',//86
	SEDAN = 'C',//87
	MUV = 'M',//88
	SUV = 'S',//89
	LUXURY = 'L',//90
	N= 'None',
	H= 'Hatchback',
	C= 'Sedan',
	M= 'MUV/MPV',
	S= 'SUV',
	L= 'Luxury';	
}