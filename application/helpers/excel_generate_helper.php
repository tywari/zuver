<?php
    date_default_timezone_set("Asia/Kolkata");
    function DownloadTripDetailsAndSummaryExcel($mail_attachment=0,$post_data=array())
	{
		//$post_data=$this->input->post();
		$trip_city='';
		$filter_data=array();
		
		if(isset($post_data['trip_start_date']))
		{
			$filter_data['trip_start_date']=$post_data['start_date'];
		}
		else
		{
			$filter_data['trip_start_date']=date('Y-m-d', strtotime('-1 days'));
		}

		if(isset($post_data['trip_city']))
		{
			$filter_data['trip_city']=$post_data['filter_city'];
		}
		
		//load our new PHPExcel library
		$CI =& get_instance();
     	//$this->load->library('email');
        $CI->load->library('excel');
        //activate worksheet number 1
        $CI->excel->setActiveSheetIndex(0);
        //name the worksheet
        $CI->excel->getActiveSheet()->setTitle('Trips Summary');
 
        $CI->excel->getActiveSheet()->setCellValue('A1', 'Serial Number');
		//change the font size
		$CI->excel->getActiveSheet()->getStyle('A1:L1')->getFont()->setSize(12);
		//make the font become bold
		$CI->excel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);
		
        $trips_list = $CI->Query_Model->getTripsDownload($filter_data);
       // $CI->excel->getActiveSheet()->fromArray($trips_list);
         //set cell A1 content with some text
        $CI->excel->getActiveSheet()->setCellValue('A1', 'Serial Number');
        $CI->excel->getActiveSheet()->setCellValue('B1', 'TRIP ID');
        $CI->excel->getActiveSheet()->setCellValue('C1', 'Date Added');
        $CI->excel->getActiveSheet()->setCellValue('D1', 'City');
        $CI->excel->getActiveSheet()->setCellValue('E1', 'Type');
        $CI->excel->getActiveSheet()->setCellValue('F1', 'Status');
        $CI->excel->getActiveSheet()->setCellValue('G1', 'Driver Name');
        $CI->excel->getActiveSheet()->setCellValue('H1', 'Client');
        $CI->excel->getActiveSheet()->setCellValue('I1', 'Customer Name');
        $CI->excel->getActiveSheet()->setCellValue('J1', 'Trip Start Time');
        $CI->excel->getActiveSheet()->setCellValue('K1', 'Trip End Time');
        $CI->excel->getActiveSheet()->setCellValue('L1', 'Fare Amount');
     											  
        $i = 2;
        $count_trip=1;
        foreach ($trips_list as $row){
            // print_r($row); exit;
            $CI->excel->getActiveSheet()->setCellValue('A'.$i, $count_trip);
            $CI->excel->getActiveSheet()->setCellValue('B'.$i, $row->tripId);
            $CI->excel->getActiveSheet()->setCellValue('C'.$i, $row->createdDate);
            $CI->excel->getActiveSheet()->setCellValue('D'.$i, $row->city);
            $CI->excel->getActiveSheet()->setCellValue('E'.$i, $row->tripType);
            $CI->excel->getActiveSheet()->setCellValue('F'.$i, $row->tripStatus);
            $CI->excel->getActiveSheet()->setCellValue('G'.$i, $row->driverName);
	        $CI->excel->getActiveSheet()->setCellValue('H'.$i, $row->clientName);
	        $CI->excel->getActiveSheet()->setCellValue('I'.$i, $row->passengerName);
	        $CI->excel->getActiveSheet()->setCellValue('J'.$i, $row->tripStartTime);
	        $CI->excel->getActiveSheet()->setCellValue('K'.$i, $row->tripEndTime);
	        $CI->excel->getActiveSheet()->setCellValue('L'.$i, $row->fareAmount);
           $count_trip++;
           $i++;
        }
       
        $filename='trip_summary_for_'.str_ireplace('-', '_', $filter_data['trip_start_date']).'.xls'; //save our workbook as this file name
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
 
        $objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');
 
		 if($mail_attachment)
		 {
		 	$filepath='reports/'.$filename;
		 	$objWriter->save($filepath);
		 	return $filepath;
		 }
	    else
	    {   
			header('Content-Type: application/vnd.ms-excel'); //mime type
  	      	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
       		header('Cache-Control: max-age=0'); //no cache
        	//force user to download the Excel file without writing it to server's HD
	        $objWriter->save('php://output');
	    }
	}

    function DownloadDriverEarningsSummaryExcel($mail_attachment=0,$driver_id=0)
    {
        //$post_data=$this->input->post();
        $trip_city='';
        $driver_id=$driver_id;
        
        //load our new PHPExcel library
        $CI =& get_instance();
        //$this->load->library('email');
        $CI->load->library('excel');
        //activate worksheet number 1
        $CI->excel->setActiveSheetIndex(0);
        //name the worksheet
        $CI->excel->getActiveSheet()->setTitle('Drivers Earning Summary');
 
        $CI->excel->getActiveSheet()->setCellValue('A1', 'Serial Number');
        //change the font size
        $CI->excel->getActiveSheet()->getStyle('A1:H1')->getFont()->setSize(12);
        //make the font become bold
        $CI->excel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);
        
        $driver_earning_list = $CI->Driver_Model->get_driver_earnings_history($driver_id);
        $driver_details=$CI->Driver_Model->getById($driver_id);
        $driver_name=$driver_details->firstName." ".$driver_details->lastName;
      /* print_r($driver_details); 
       print_r($driver_earning_list); die();*/
       // $CI->excel->getActiveSheet()->fromArray($driver_earning_list);
         //set cell A1 content with some text
        $CI->excel->getActiveSheet()->setCellValue('A1', 'Serial Number');
        $CI->excel->getActiveSheet()->setCellValue('B1', 'TRIP ID');
        $CI->excel->getActiveSheet()->setCellValue('C1', 'Trip Type');
        $CI->excel->getActiveSheet()->setCellValue('D1', 'Driver Type');
        $CI->excel->getActiveSheet()->setCellValue('E1', 'Earning Type');
        $CI->excel->getActiveSheet()->setCellValue('F1', 'Earning Amount');
        $CI->excel->getActiveSheet()->setCellValue('G1', 'Earning Date');
        $CI->excel->getActiveSheet()->setCellValue('H1', 'Created Date');
         
        $trip_type_list = $CI->Data_Attributes_Model->getSelectDropdownOptions ( array (
                'tableName' => strtoupper ( 'TRIP_TYPE' ),
                //'isVisible' => Status_Type_Enum::ACTIVE 
        ), 'sequenceOrder' );
        $trip_type_list['NA']='NA';
        $driver_type_list = $CI->Data_Attributes_Model->getSelectDropdownOptions ( array (
                'tableName' => strtoupper ( 'DRIVER_TYPE' ),
                'isVisible' => Status_Type_Enum::ACTIVE 
        ), 'sequenceOrder' );
        $earning_type_list = $CI->Data_Attributes_Model->getSelectDropdownOptions ( array (
                'tableName' => strtoupper ( 'EARNING_TYPE' ),
                'isVisible' => Status_Type_Enum::ACTIVE 
        ), 'sequenceOrder' );

       
        $i = 2;
        $count_history=1;
        foreach ($driver_earning_list as $row){
            // print_r($row); exit;
            $CI->excel->getActiveSheet()->setCellValue('A'.$i, $count_history);
            $CI->excel->getActiveSheet()->setCellValue('B'.$i, $row->tripId);
            if (array_key_exists ($row->tripType, $trip_type_list )) 
            {
                $CI->excel->getActiveSheet()->setCellValue('C'.$i, $trip_type_list[$row->tripType]);
            }
            else
            {
                $CI->excel->getActiveSheet()->setCellValue('C'.$i, $row->tripType);
            }

            if (array_key_exists ($row->driverType, $driver_type_list )) 
            {
                $CI->excel->getActiveSheet()->setCellValue('D'.$i, $driver_type_list[$row->driverType]);
            }
            else
            {
                $CI->excel->getActiveSheet()->setCellValue('D'.$i, $row->driverType);
            }
            
            if (array_key_exists ($row->earningType, $earning_type_list )) 
            {
                $CI->excel->getActiveSheet()->setCellValue('E'.$i, $earning_type_list[$row->earningType]);
            }
            else
            {
                $CI->excel->getActiveSheet()->setCellValue('E'.$i, $row->earningType);
            }
            $CI->excel->getActiveSheet()->setCellValue('F'.$i, $row->earningAmount);
            $CI->excel->getActiveSheet()->setCellValue('G'.$i, $row->earningDate);
            $CI->excel->getActiveSheet()->setCellValue('H'.$i, $row->createdDate);
           $count_history++;
           $i++;
        }
       
        $filename='earning_summary_for_driver_'.str_ireplace(' ', '-', $driver_name).'.xls'; //save our workbook as this file name
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
 
        $objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');
 
         if($mail_attachment)
         {
            $filepath='reports/'.$filename;
            $objWriter->save($filepath);
            return $filepath;
         }
        else
        {   
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }
    }

    function DownloadDriverSessionsSummaryExcel($mail_attachment=0,$driver_id=0)
    {
        //$post_data=$this->input->post();
        $trip_city='';
        $driver_id=$driver_id;
        
        //load our new PHPExcel library
        $CI =& get_instance();
        //$this->load->library('email');
        $CI->load->library('excel');
        //activate worksheet number 1
        $CI->excel->setActiveSheetIndex(0);
        //name the worksheet
        $CI->excel->getActiveSheet()->setTitle('Drivers Sessions Summary');
 
        $CI->excel->getActiveSheet()->setCellValue('A1', 'Serial Number');
        //change the font size
        $CI->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->setSize(12);
        //make the font become bold
        $CI->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true);
        $driver_session_list = $CI->Driver_Model->get_driver_sessions($driver_id);

        // Code by Aditya on May 04 2017
        // Aditya code start here
        // You may need to load the model if it hasn't been pre-loaded
        $CI->load->model('Driver_Login_Incentive_Model');
        $loginIncentiveDetails = $CI->Driver_Login_Incentive_Model->getB2CLoginIncentiveDetails();

        $CI->load->model('Trip_Details_Model');
        $outstationTripDetails = $CI->Trip_Details_Model->getOutstationTripDetails($driver_id);
        
        //print_r($outstationTripDetails);
        //die();
        
        $formattedDriverSessionList = [];
        if (is_array($driver_session_list)) {
            $shiftOutTime = '0000-00-00 00:00:00';
            $shiftInTime = '0000-00-00 00:00:00';
            
            $adityaCounter = 0;
            foreach ($driver_session_list as $key => $value) {
                // This logic only applies for Shift OUT status
                // Currently Database makes a new Entry for IN, but updates IN entry for OUT
                // So OUT are not new entries rather old IN getting updated to OUT,
                // But you have some OUT entries that are actually new
                if ($value->shiftStatus == 'OUT') {
                    if ($value->createdDate == $value->updatedDate) {
                        // This means that this OUT entry is a new Entry and not updated from IN entry
                        // The next entry in Iteration should be IN entry for this OUT entry
                        $shiftOutTime = $shiftInTime = $value->updatedDate;
                        continue;
                    } else {
                        // This means that this OUT entry got updated into OUT from IN
                        // So createdDate becomes IN entry DateTime & updatedDate becomes OUT entry DateTime
                        $shiftInTime = $value->createdDate;
                        $shiftOutTime = $value->updatedDate;
                    }
                } else if ($value->shiftStatus == 'IN') {
                    // This means that this IN entry belong to previous iterations OUT entry, unless it is the first entry
                    $shiftInTime = $value->createdDate;
                    if ($shiftOutTime == '0000-00-00 00:00:00') {
                        // This means that this IN entry is the first iteration
                        $shiftOutTime = '';
                    }
                }

                $formattedDriverSessionList[$adityaCounter]['shiftInTime'] = $shiftInTime;
                $formattedDriverSessionList[$adityaCounter]['shiftOutTime'] = $shiftOutTime;

                $adityaCounter++;
            }
        }
        //print_r($formattedDriverSessionList);
        //die();
        // Aditya Code end here
        
        $driver_details=$CI->Driver_Model->getById($driver_id);
        $driver_name=$driver_details->firstName." ".$driver_details->lastName;
        $availability_status_list = $CI->Data_Attributes_Model->getSelectDropdownOptions ( array (
                'tableName' => strtoupper ( 'DRIVER_AVAILABLE_STATUS' ),
                //'isVisible' => Status_Type_Enum::ACTIVE 
        ), 'sequenceOrder' );
        /* Original Code, commented by Aditya Sharma on 02 May 2017
        $CI->excel->getActiveSheet()->setCellValue('A1', 'Serial Number');
        $CI->excel->getActiveSheet()->setCellValue('B1', 'Status Date 1');
        $CI->excel->getActiveSheet()->setCellValue('C1', 'Shift Status');
        $CI->excel->getActiveSheet()->setCellValue('D1', 'Status Date 2');
        $CI->excel->getActiveSheet()->setCellValue('E1', 'Shift Status');
        $CI->excel->getActiveSheet()->setCellValue('F1', 'Total Hours');
        */

        /* Code created by Aditya Sharma on 02 May 2017 */
        $CI->excel->getActiveSheet()->setCellValue('A1', 'Serial Number');
        $CI->excel->getActiveSheet()->setCellValue('B1', 'Month');
        $CI->excel->getActiveSheet()->setCellValue('C1', 'Date');
        $CI->excel->getActiveSheet()->setCellValue('D1', 'Day Of Week');
        $CI->excel->getActiveSheet()->setCellValue('E1', 'On Duty');
        $CI->excel->getActiveSheet()->setCellValue('F1', 'Manual Off-Duty');
        $CI->excel->getActiveSheet()->setCellValue('G1', 'Auto Off-Duty');
        $CI->excel->getActiveSheet()->setCellValue('H1', 'Total Hours');
        $CI->excel->getActiveSheet()->setCellValue('I1', 'Day Shift Incentive Eligible');
        $CI->excel->getActiveSheet()->setCellValue('J1', 'Night Shift Incentive Eligible');
        $CI->excel->getActiveSheet()->setCellValue('K1', 'Total Incentive');
        $CI->excel->getActiveSheet()->setCellValue('L1', 'No Incentive Reason');

        $CI->excel->getActiveSheet()->setCellValue('M1', 'Incentive Day Shift Start Time');
        $CI->excel->getActiveSheet()->setCellValue('N1', 'Incentive Day Shift End Time');
        $CI->excel->getActiveSheet()->setCellValue('O1', 'Incentive Night Shift Start Time');
        $CI->excel->getActiveSheet()->setCellValue('P1', 'Incentive Night Shift End Time');
        /* Aditya code end here */

        $i = 2;
        $count_history=1;
        /* ORIGINAL CODE COMMENTED BY ADITYA ON Wed May 03 2017
        foreach ($driver_session_list as $row){
            // print_r($row); exit;
            $CI->excel->getActiveSheet()->setCellValue('A'.$i, $count_history);
            $CI->excel->getActiveSheet()->setCellValue('B'.$i, $row->createdDate1);
            $CI->excel->getActiveSheet()->setCellValue('C'.$i, $row->shiftStatus1);
            $CI->excel->getActiveSheet()->setCellValue('D'.$i, $row->createdDate2);
            $CI->excel->getActiveSheet()->setCellValue('E'.$i, $row->shiftStatus2);
            $CI->excel->getActiveSheet()->setCellValue('F'.$i,$row->timedifference);
            
           $count_history++;
           $i++;
        }*/
        // Updated code based on above commented Code by Aditya on Wed May 03 2017
        $outstationIncentiveCancelReason = '';
        $outstationIncentiveCancelCounter = 0;
        foreach ($formattedDriverSessionList as $row){
            $outstationIncentiveCancelReason = '';
            $outstationIncentiveCancelCounter = 0;

            // print_r($row); exit;
            $date = new DateTime($row['shiftInTime']);// on duty time
            $startTime = new DateTime($row['shiftInTime']);// on duty time
            $endTime = '';
            $totalSeconds = '';
            $totalHours = '';

            // Shift Incentive Array Variable
            $shiftIncentiveDetails = [];
            $shiftIncentiveDetails['day'] = [];
            $shiftIncentiveDetails['night'] = [];

            $shiftIncentiveDetails['day']['startTime'] = '';
            $shiftIncentiveDetails['day']['endTime'] = '';

            $shiftIncentiveDetails['night']['startTime'] = '';
            $shiftIncentiveDetails['night']['endTime'] = '';



            //This counter value will be used to run the loop and calculate how much Incentive to provide
            $dateDiffCounter = 0;
            $endTimeString = '';
            if ($row['shiftOutTime'] != '') {
                $endTime = new DateTime($row['shiftOutTime']);// off duty time
                $diff = $endTime->diff($startTime);
                $totalHours = $diff->format('%d Days %H:%I');
                $endTimeString = $endTime->format('Y-m-d H:i:s');

                

                $strStartTime = strtotime($row['shiftInTime']);
                $strEndTime = strtotime($row['shiftOutTime']);
                $strDateDiff = $strEndTime - $strStartTime;
                $dateDiffCounter = ceil($strDateDiff / (60 * 60 * 24));
            }

            // Finding and setting Incentive Start Time, End Time periods for this particular day of the week
            // Currently this will only work if Incentive is same for all days
            $dayLoginIncentiveRate = 0;
            $nightLoginIncentiveRate = 0;
            if (is_array($loginIncentiveDetails)) {
                foreach ($loginIncentiveDetails as $incentiveDetails) {
                    if ($incentiveDetails->day == $startTime->format('N')) {
                        if ($incentiveDetails->shift == 'Day') {
                            $shiftIncentiveDetails['day']['startTime'] = new DateTime($date->format('Y-m-d').' '.$incentiveDetails->start_time);
                            $shiftIncentiveDetails['day']['endTime'] = new DateTime($date->format('Y-m-d').' '.$incentiveDetails->end_time);
                            $dayLoginIncentiveRate = $incentiveDetails->rate;
                        } else if ($incentiveDetails->shift == 'Night') {
                            $shiftIncentiveDetails['night']['startTime'] = new DateTime($date->format('Y-m-d').' '.$incentiveDetails->start_time);
                            $shiftIncentiveDetails['night']['endTime'] = new DateTime($date->format('Y-m-d').' '.$incentiveDetails->end_time);
                            $nightLoginIncentiveRate = $incentiveDetails->rate;
                            date_add( $shiftIncentiveDetails['night']['endTime'], date_interval_create_from_date_string("1 days") );
                        }
                    }
                }
            }

            // Now parse and process Outstation Trip Timings
            // need to ignore in trip timings


            // Now checking if Login Incentive is Applicable or not, if yes then how much
            $dayLoginIncentive = 0;
            $nightLoginIncentive = 0;
            for ($li = 0; $li <= $dateDiffCounter; $li++) {
                if ($li > 0) {
                    date_add( $tmpIncentiveStartTime['day']['startTime'], date_interval_create_from_date_string("1 days") );
                    date_add( $tmpIncentiveStartTime['day']['endTime'], date_interval_create_from_date_string("1 days") );

                    date_add( $tmpIncentiveStartTime['night']['startTime'], date_interval_create_from_date_string("1 days") );
                    date_add( $tmpIncentiveStartTime['night']['endTime'], date_interval_create_from_date_string("1 days") );
                }
                $tmpIncentiveStartTime = $shiftIncentiveDetails;
                if ($startTime <= $tmpIncentiveStartTime['day']['startTime']) {
                    if ($endTime >= $tmpIncentiveStartTime['day']['endTime']) {
                        $dayLoginIncentive++;
                        // if it falls in outstation trip timing then ignore rollback
                        foreach ($outstationTripDetails as $oTDValue) {
                            $oTDActualPickupDateTime = new DateTime($oTDValue->actualPickupDateTime);
                            $oTDDropDateTime = new DateTime($oTDValue->dropDateTime);

                            if (($startTime <= $oTDActualPickupDateTime) && ($oTDDropDateTime <= $endTime) ) {
                                $dayLoginIncentive--;
                                $outstationIncentiveCancelCounter++;
                                $outstationIncentiveCancelReason = $outstationIncentiveCancelCounter.' incentive(s) cancelled due to Outstation Trip';
                            }
                        }
                        
                    }
                }

                if ($startTime <= $tmpIncentiveStartTime['night']['startTime']) {
                    if ($endTime >= $tmpIncentiveStartTime['night']['endTime']) {
                        // if it falls in outstation trip timing then ignore it
                        $nightLoginIncentive++;
                        // if it falls in outstation trip timing then ignore rollback
                        foreach ($outstationTripDetails as $oTDValue) {
                            $oTDActualPickupDateTime = new DateTime($oTDValue->actualPickupDateTime);
                            $oTDDropDateTime = new DateTime($oTDValue->dropDateTime);

                            if (($startTime <= $oTDActualPickupDateTime) && ($oTDDropDateTime <= $endTime) ) {
                                $nightLoginIncentive--;
                                $outstationIncentiveCancelCounter++;
                                $outstationIncentiveCancelReason = $outstationIncentiveCancelCounter.' incentive(s) cancelled due to Outstation Trip';
                            }
                        }
                    }
                }
            }
            
            $totalIncentive = 0;
            $totalIncentive = ($dayLoginIncentiveRate * $dayLoginIncentive) + ($nightLoginIncentiveRate * $nightLoginIncentive);
            

            $CI->excel->getActiveSheet()->setCellValue('A'.$i, $count_history);
            $CI->excel->getActiveSheet()->setCellValue('B'.$i, $date->format('M'));
            $CI->excel->getActiveSheet()->setCellValue('C'.$i, $date->format('d-m-Y'));
            $CI->excel->getActiveSheet()->setCellValue('D'.$i, $startTime->format('D'));
            $CI->excel->getActiveSheet()->setCellValue('E'.$i, $startTime->format('Y-m-d H:i:s'));
            $CI->excel->getActiveSheet()->setCellValue('F'.$i, $endTimeString);
            //$CI->excel->getActiveSheet()->setCellValue('G'.$i, $totalSeconds);
            $CI->excel->getActiveSheet()->setCellValue('H'.$i, $totalHours);

            $CI->excel->getActiveSheet()->setCellValue('I'.$i, $dayLoginIncentive);
            $CI->excel->getActiveSheet()->setCellValue('J'.$i, $nightLoginIncentive);
            $CI->excel->getActiveSheet()->setCellValue('K'.$i, $totalIncentive);

            $CI->excel->getActiveSheet()->setCellValue('L'.$i, $outstationIncentiveCancelReason);

            $CI->excel->getActiveSheet()->setCellValue('M'.$i, $shiftIncentiveDetails['day']['startTime']);
            $CI->excel->getActiveSheet()->setCellValue('N'.$i, $shiftIncentiveDetails['day']['endTime']);

            $CI->excel->getActiveSheet()->setCellValue('O'.$i, $shiftIncentiveDetails['night']['startTime']);
            $CI->excel->getActiveSheet()->setCellValue('P'.$i, $shiftIncentiveDetails['night']['endTime']);

           $count_history++;
           $i++;
        }

       
        $filename='Session_summary_for_driver_'.str_ireplace(' ', '-', $driver_name).'.xls'; //save our workbook as this file name
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
 
        $objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');
 
         if($mail_attachment)
         {
            $filepath='reports/'.$filename;
            $objWriter->save($filepath);
            return $filepath;
         }
        else
        {   
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }
    }

    function download_shift_drivers($shiftStartDatetime, $shiftEndDatetime)
    {
        $CI =& get_instance();
           $CI->load->library('excel');
            //activate worksheet number 1
           $CI->excel->setActiveSheetIndex(0);
            //name the worksheet
           $CI->excel->getActiveSheet()->setTitle('Shift Drivers Summary');
     
            //change the font size
           $CI->excel->getActiveSheet()->getStyle('A1:G1')->getFont()->setSize(12);
            //make the font become bold
           $CI->excel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);
           /*Drivers logged in provided shift **/
           $shift_driver_list1 =$CI->Driver_Model->get_shift_driver_sessions($shiftStartDatetime, $shiftEndDatetime);

           /**Drivers logged before provided shift end time and not logged off */
           $shift_driver_list2 =$CI->Driver_Model->get_logged_drivers_by_date($shiftEndDatetime); 

           $shift_driver_list=array_merge($shift_driver_list1,$shift_driver_list2);
           
           $driver_type_list = $CI->Data_Attributes_Model->getSelectDropdownOptions ( array (
                'tableName' => strtoupper ( 'DRIVER_TYPE' ),
                'isVisible' => Status_Type_Enum::ACTIVE 
            ), 'sequenceOrder' );
           $CI->excel->getActiveSheet()->setCellValue('A1', 'Serial No.');
           $CI->excel->getActiveSheet()->setCellValue('B1', 'Driver Name');
           $CI->excel->getActiveSheet()->setCellValue('C1', 'Driver Mobile');
           $CI->excel->getActiveSheet()->setCellValue('D1', 'Driver Type');
           $CI->excel->getActiveSheet()->setCellValue('E1', 'Status Date Time');
           $CI->excel->getActiveSheet()->setCellValue('F1', 'Shift Status');

         
            $i = 2;
            $count=1;
            foreach ($shift_driver_list as $row){
                // print_r($row); exit;
               $CI->excel->getActiveSheet()->setCellValue('A'.$i, $count);
               $CI->excel->getActiveSheet()->setCellValue('B'.$i, $row->driver_name);
               $CI->excel->getActiveSheet()->setCellValue('C'.$i, $row->driver_phone);
                $CI->excel->getActiveSheet()->setCellValue('D'.$i, $driver_type_list[$row->driverType]);
               $CI->excel->getActiveSheet()->setCellValue('E'.$i, $row->createdDate);
               $CI->excel->getActiveSheet()->setCellValue('F'.$i, $row->shiftStatus);
                
               $count++;
               $i++;
            }
           
            $filename='summary_of_Shift_driver_sessions_from_'.$shiftStartDatetime.'_to_'.$shiftEndDatetime.'.xls'; //save our workbook as this file name
                         
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
     
            $objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');
  
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
    }
?>
