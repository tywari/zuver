<?php
/**
 * To get last occurance string based on delimeter
 *
 * @param unknown $delimiter
 * @param unknown $string
 * @param unknown $side
 * @param string $keep_character
 * @return boolean
 * 
 * example:$address[5]=cut_string_using_last(',', $address[5], 'left', false);
 */
function cut_string_using_last($delimiter, $string, $side, $keep_character = true)
{
	$offset = ($keep_character ? 1 : 0);
	$whole_length = strlen ( $string );
	$right_length = (strlen ( strrchr ( $string, $delimiter ) ) - 1);
	$left_length = ($whole_length - $right_length - 1);
	switch ($side) {
		case 'left' :
			$piece = substr ( $string, 0, ($left_length + $offset) );
			break;
		case 'right' :
			$start = (0 - ($right_length + $offset));
			$piece = substr ( $string, $start );
			break;
		default :
			$piece = false;
			break;
	}
	return ($piece);
}