<?php

	function css_url($uri = '')
	{
		$CI = get_instance();
		return base_url().$CI->config->slash_item('css_path').ltrim($uri,'/');
	}
	function js_url($uri = '')
	{
		$CI = get_instance();
		return base_url().$CI->config->slash_item('js_path').ltrim($uri,'/');
	}
	function image_url($uri = '')
	{
		$CI = get_instance();
		return base_url().$CI->config->slash_item('image_path').ltrim($uri,'/');
	}
	function passenger_content_url($uri = '')
	{
		$CI = get_instance();
		return base_url().$CI->config->slash_item('passenger_content_path').ltrim($uri,'/');
	}
	function driver_content_url($uri = '')
	{
		$CI = get_instance();
		return base_url().$CI->config->slash_item('driver_content_path').ltrim($uri,'/');
	}
	function user_content_url($uri = '')
	{
		$CI = get_instance();
		return base_url().$CI->config->slash_item('user_content_path').ltrim($uri,'/');
	}
	function bank_content_url($uri = '')
	{
		$CI = get_instance();
		return base_url().$CI->config->slash_item('bank_content_path').ltrim($uri,'/');
	}
?>