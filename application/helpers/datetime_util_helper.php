<?php

function getCurrentDate($timezone=TIMEZONE)
{
	date_default_timezone_set($timezone);
	return (1 ) ? date('Y-m-d') : $_SESSION['current_date'];
}
function getCurrentUnixDate($timezone=TIMEZONE)
{
	date_default_timezone_set($timezone);
	return strtotime(getCurrentDate());
}

function getCurrentDateTime($timezone=TIMEZONE)
{
	date_default_timezone_set($timezone);
	return (1) ? date('Y-m-d H:i:s') : $_SESSION['current_date'];
}
function getCurrentUnixDateTime($timezone=TIMEZONE)
{
	date_default_timezone_set($timezone);
	return strtotime(getCurrentDateTime());
}

function dateDropDownDisplayFormat($mysqlDate){
	if($mysqlDate)
	{
		$date = DateTime::createFromFormat(DATE_MYSQL_FORMAT, $mysqlDate);
		return $date->format(DATE_DROPDOWN_FORMAT);
	}
	return '';
}
function dateTimeDropDownDisplayFormat($mysqlDate){
	if($mysqlDate)
	{
		$date = DateTime::createFromFormat(DATETIME_MYSQL_FORMAT, $mysqlDate);
		return $date->format(DATETIME_DROPDOWN_FORMAT);
	}
	return '';
}
/**
 * 
 * @param type $mysqlDate
 * @return mysql date format
 */
function dateMySQLFormat($mysqlDate){
	if($mysqlDate)
	{
		$date = DateTime::createFromFormat(DATE_TABLE_FORMAT, $mysqlDate);
		return $date->format(DATE_MYSQL_FORMAT);
	}
	return '';
}
function datePageFormat($mysqlDate,$timestamp=true)
{
	if($mysqlDate && $timestamp==true)
	{
		return date(DATE_PAGE_FORMAT, $mysqlDate);
	}
	else if($mysqlDate){
		return date(DATE_PAGE_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateDisplayFormat($mysqlDate)
{
	if($mysqlDate)
	{	
		return date(DATE_DISPLAY_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateTimeMySQLFormat($mysqlDate){
	if($mysqlDate)
	{
		//$date = DateTime::createFromFormat(DATETIME_TABLE_FORMAT, $mysqlDate);
		//return $date->format(DATETIME_MYSQL_FORMAT);
		return date(DATETIME_MYSQL_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateTimePageFormat($mysqlDate,$timestamp=true)
{
	if($mysqlDate && $timestamp==true)
	{
		return date(DATETIME_PAGE_FORMAT, $mysqlDate);
	}
	else if($mysqlDate){
		return date(DATETIME_PAGE_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateTime24DisplayFormat($mysqlDate)
{
	if($mysqlDate)
	{
		return date(DATETIME_DISPLAY_24_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
function dateTime12DisplayFormat($mysqlDate)
{
	if($mysqlDate)
	{
		return date(DATETIME_DISPLAY_12_FORMAT, strtotime($mysqlDate));
	}
	return '';
}
