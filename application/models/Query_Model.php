<?php

require_once(APPPATH . 'config/company_type_enum.php');
require_once(APPPATH . 'config/device_type_enum.php');
require_once(APPPATH . 'config/driver_accepted_status_enum.php');
require_once(APPPATH . 'config/driver_available_status_enum.php');
require_once(APPPATH . 'config/driver_request_status_enum.php');
require_once(APPPATH . 'config/driver_shift_status_enum.php');
require_once(APPPATH . 'config/driver_type_enum.php');
require_once(APPPATH . 'config/favourite_type_enum.php');
require_once(APPPATH . 'config/gender_type_enum.php');
require_once(APPPATH . 'config/payment_method_enum.php');
require_once(APPPATH . 'config/payment_mode_enum.php');
require_once(APPPATH . 'config/promocode_type_enum.php');
require_once(APPPATH . 'config/role_type_enum.php');
require_once(APPPATH . 'config/signup_type_enum.php');
require_once(APPPATH . 'config/register_type_enum.php');
require_once(APPPATH . 'config/transaction_type_enum.php');
require_once(APPPATH . 'config/transmission_type_enum.php');
require_once(APPPATH . 'config/trip_status_enum.php');
require_once(APPPATH . 'config/trip_type_enum.php');
require_once(APPPATH . 'config/emergency_status_enum.php');
require_once(APPPATH . 'config/user_type_enum.php');
require_once(APPPATH . 'config/bill_type_enum.php');
require_once(APPPATH . 'config/status_type_enum.php');
require_once(APPPATH . 'config/manipulation_type_enum.php');
require_once(APPPATH . 'config/car_type_enum.php');
require_once(APPPATH . 'config/driver_skill_enum.php');
require_once(APPPATH . 'config/license_type_enum.php');
require_once(APPPATH . 'config/earning_type_enum.php');
require_once(APPPATH . 'config/measuring_type_enum.php');

class Query_Model extends MY_Model
{

    /**
     * Default Constructor
     */
    function __construct($args = NULL)
    {
        parent::__construct();
        if (is_object($args))
            $args = get_object_vars($args);
        if (is_array($args)) {
            foreach ($args As $key => $value) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * To get top 10 trip collection based on day, month & year by passenger.
     *
     * @param unknown $current_date
     * @param number $day
     * @param number $month
     * @param number $year
     * @return array $result
     */
    public function getTopTripColletion($current_date, $day = 0, $week = 0, $month = 0, $year = 0)
    {
        $where_caluse = "td.tripStatus=" . Trip_Status_Enum::TRIP_COMPLETED;
        if ($current_date && $day) {
            $where_caluse .= " AND YEAR(td.createdDate)=YEAR(" . $current_date . ") AND MONTH(td.createdDate)=MONTH(" . $current_date . ") DAY(td.createdDate)=DAY(" . $current_date . ")";
        } else if ($current_date && $week) {
            $where_caluse .= " AND WEEKOFYEAR(td.createdDate)=WEEKOFYEAR(" . $current_date . ")";
        } else if ($current_date && $month) {
            $where_caluse .= " AND YEAR(td.createdDate)=YEAR(" . $current_date . ") AND MONTH(td.createdDate)=MONTH(" . $current_date . ")";
        } else if ($current_date && $year) {
            $where_caluse .= " AND YEAR(td.createdDate)=YEAR(" . $current_date . ")";
        }
        $query = "select ttd.totalTripCost As totalTripCost, td.createdDate As createdDate,p.firstName As passengerFirstName, p.lastName As passengerLastName
				from triptransactiondetails As ttd left join tripdetails As td on ttd.tripId=td.id left join passenger As p on p.id=td.passengerId" . $where_caluse . "  order by ttd.totalTripCost desc Limit 0,10";
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getTotalCollectionByPaymentMode($city_id = NULL, $company_id = NULL)
    {
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = " AND td.bookingCityId=" . $city_id;
        }
        if ($company_id) {
            $where_company = " AND td.companyId=" . $company_id;
        }
        $query = "SELECT
		SUM(CASE WHEN ttd.paymentMode ='" . Payment_Mode_Enum::CASH . "'  THEN ttd.totalTripCost ELSE 0 END) As 'totalCollectedCash',
		SUM(CASE WHEN ttd.paymentMode ='" . Payment_Mode_Enum::PAYTM . "'  THEN ttd.totalTripCost ELSE 0 END) As 'totalCollectedPaytm',
		SUM(CASE WHEN ttd.paymentMode ='" . Payment_Mode_Enum::WALLET . "'  THEN ttd.totalTripCost ELSE 0 END) As 'totalCollectedWallet',
		SUM(CASE WHEN ttd.paymentMode ='" . Payment_Mode_Enum::INVOICE . "'  THEN ttd.totalTripCost ELSE 0 END) As 'totalCollectedInvoice',
		SUM(CASE WHEN ttd.companyTax  THEN ttd.companyTax ELSE 0 END) As 'totalCompanyTax',
		SUM(CASE WHEN (ttd.promoDiscountAmount OR ttd.otherDiscountAmount)  THEN (ttd.adminAmount OR ttd.otherDiscountAmount) ELSE 0 END) As 'totalDiscount',
		SUM(CASE WHEN ttd.adminAmount  THEN ttd.adminAmount ELSE 0 END) As 'totalAdminCommission',
		SUM(CASE WHEN ttd.driverEarning  THEN ttd.driverEarning ELSE 0 END) As 'totalDriverEarnings'
		FROM triptransactiondetails As ttd left join tripdetails As td on ttd.tripId=td.id
		where td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' " . $where_city . " " . $where_company;

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getTripStatusCount($city_id = NULL, $company_id = NULL)
    {
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = " AND td.bookingCityId=" . $city_id;
        }
        if ($company_id) {
            $where_company = " AND td.companyId=" . $company_id;
        }
        $query = "SELECT
		SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "') THEN 1 ELSE 0 END) As 'totalUnassignedTrip',
		SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_CONFIRMED . "','" . Trip_Status_Enum::READY_TO_START . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "')  THEN 1 ELSE 0 END) As 'totalUpcomingTrip',
		SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "')  THEN 1 ELSE 0 END) As 'totalOngoingTrip',
		SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "','" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "')  THEN 1 ELSE 0 END) As 'totalCancelledTrip',
		SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_COMPLETED . "')  THEN 1 ELSE 0 END) As 'totalCompletedTrip'
		FROM tripdetails As td
		where td.id !=0 " . $where_city . " " . $where_company;

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getPassengerList($city_id = NULL, $company_id = NULL, $start = 0, $limit = 0, $filter = '', $order_by = '')
    {
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = " where p.cityId=" . $city_id;
        }
        $query = "SELECT p.id As 'id',p.firstName As 'firstName',p.lastName As 'lastName',p.status As 'status',p.passengerCode As 'passengerCode',
				p.walletAmount As 'walletAmount',p.email As 'email',p.mobile As 'mobile',p.lastLoggedIn As 'lastLoggedIn',das.description As 'signupFrom',
				dar.description As 'registerType',cp.name As 'companyName',c.name As 'cityName',
				SUM(CASE WHEN td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' THEN 1 ELSE 0 END) As 'totalTripComplete',
				AVG(CASE WHEN td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' THEN td.driverRating ELSE 0 END) As 'rating'
				from passenger As p
				left join corporatepartnersdetails As cp on cp.id = p.companyId
				left join city As c on c.id=p.cityId
			    left join dataattributes As dar on dar.enumName = p.registerType AND dar.tableName='REGISTER_TYPE'
				left join dataattributes As das on das.enumName = p.signupFrom AND das.tableName='SIGNUP_TYPE'
				left join tripdetails As td on td.passengerId =p.id AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "'
				" . $where_city;
        if ($filter) {
            $search_val = $filter;
            $query .= " AND (p.firstName like '%" . $search_val . "%' OR p.lastName like '%" . $search_val . "%' OR concat(p.firstName, ' ', p.lastName) like '%" . $search_val . "%' OR p.email like '%" . $search_val . "%' OR p.walletAmount like '%" . $search_val . "%' OR p.mobile like '%" . $search_val . "%')";
        }
        $query .= " GROUP BY p.id";

        if ($order_by) {
            $query .= " Order By " . $order_by;
        }

        if ($limit) {
            $query .= " LIMIT " . $start . ", " . $limit;
        }
        //echo $query; die();
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getPassengerCount($city_id = NULL, $company_id = NULL, $filter = '')
    {
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = " where p.cityId=" . $city_id;
        }
        $query = "SELECT p.id As 'id',p.firstName As 'firstName',p.lastName As 'lastName',p.status As 'status',p.passengerCode As 'passengerCode',
                p.walletAmount As 'walletAmount',p.email As 'email',p.mobile As 'mobile',p.lastLoggedIn As 'lastLoggedIn',das.description As 'signupFrom',
                dar.description As 'registerType',cp.name As 'companyName',c.name As 'cityName',
                SUM(CASE WHEN td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' THEN 1 ELSE 0 END) As 'totalTripComplete',
                AVG(CASE WHEN td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' THEN td.driverRating ELSE 0 END) As 'rating'
                from passenger As p
                left join corporatepartnersdetails As cp on cp.id = p.companyId
                left join city As c on c.id=p.cityId
                left join dataattributes As dar on dar.enumName = p.registerType AND dar.tableName='REGISTER_TYPE'
                left join dataattributes As das on das.enumName = p.signupFrom AND das.tableName='SIGNUP_TYPE'
                left join tripdetails As td on td.passengerId =p.id AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "'
                " . $where_city;
        if ($filter) {
            $search_val = $filter;
            $query .= " AND (p.firstName like '%" . $search_val . "%' OR p.lastName like '%" . $search_val . "%' OR p.email like '%" . $search_val . "%' OR p.walletAmount like '%" . $search_val . "%' )";
        }
        $query .= " GROUP BY p.id";
        $result = $this->db->query($query);
        return $result->num_rows();
    }

    public function getDriverList($city_id = NULL, $company_id = NULL)
    {
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = " AND d.cityId=" . $city_id;
        }
        $query = "SELECT d.id As 'id',d.firstName As 'firstName',d.lastName As 'lastName',d.status As 'status',d.driverCode As 'driverCode', d.doj As joiningDate,d.createdDate As 'JoinedDate', da.description As 'driverType',
				das.description As 'skills',d.profileImage As 'image',d.driverSkill As 'driverSkill', d.email As 'email',d.mobile As 'mobile',d.driverLanguagesKnown As 'driverLanguagesKnown',d.loginStatus As 'loginStatus',
				d.driverExperience As 'driverExperience',d.isVerified As 'isVerified', d.doj As 'doj', cp.name As 'companyName',c.name As 'cityName',
				SUM(CASE WHEN td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' THEN 1 ELSE 0 END) As 'totalTripComplete',
				SUM(CASE WHEN ttd.driverEarning THEN ttd.driverEarning ELSE 0 END) As 'driverEarning',
				AVG(CASE WHEN td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' THEN td.passengerRating ELSE 0 END) As 'rating',
				(SELECT dsh.shiftStatus FROM drivershifthistory As dsh where dsh.driverId=d.id ORDER BY id DESC LIMIT 0,1) As 'shiftStatus',
				(SELECT dsh.availabilityStatus FROM drivershifthistory As dsh where dsh.driverId=d.id ORDER BY id DESC LIMIT 0,1) As 'availabilityStatus',
				(SELECT dps.licenseType FROM driverpersonaldetails As dps where dps.driverId=d.id ORDER BY id DESC LIMIT 0,1) As 'licenseType'
				from driver As d
				left join corporatepartnersdetails As cp on cp.id = d.companyId
				left join dataattributes As da on da.enumName = d.driverType AND da.tableName='DRIVER_TYPE'
				left join dataattributes As das on das.enumName = d.skills AND das.tableName='TRANSMISSION_TYPE'
				left join city As c on c.id=d.cityId
				left join tripdetails As td on td.driverId =d.id AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' AND YEAR(td.createdDate)=YEAR('" . getCurrentDateTime() . "') AND MONTH(td.createdDate)=MONTH('" . getCurrentDateTime() . "')
				left join triptransactiondetails As ttd ON ttd.tripId=td.id AND YEAR(ttd.createdDate)=YEAR('" . getCurrentDateTime() . "') AND MONTH(ttd.createdDate)=MONTH('" . getCurrentDateTime() . "')
				where d.isDeleted='" . Status_Type_Enum::INACTIVE . "'" . $where_city . "
				GROUP BY d.id";

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }


    public function getUserList($city_id = NULL, $company_id = NULL)
    {
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = "where u.cityId=" . $city_id;
        }
        $query = "SELECT u.id As 'id',u.firstName As 'firstName',u.lastName As 'lastName',u.status As 'status',u.userIdentity As 'userIdentity',u.designation As 'designation',u.internalEmail As 'internalEmail',
				ud.email As 'email',ud.mobile As 'mobile',ud.qualification As 'qualification',da.description As 'roleType',u.doj As 'doj', cp.name As 'companyName'
				from userlogincredential As u
				left join userpersonaldetails As ud on ud.userId=u.id
				left join dataattributes As da on da.enumName = u.roleType AND da.tableName='ROLE_TYPE'
				left join corporatepartnersdetails As cp on cp.id = u.companyId
		 		" . $where_city;

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getPromocodeList($city_id = NULL, $company_id = NULL)
    {
        /*
         * $query="SELECT pc.id As 'id',pc.promoCode As 'promoCode',pc.promoDiscountAmount As 'promoDiscountAmount',pc.status As 'status',pc.promoStartDate As 'promoStartDate',
         * pc.promoEndDate As 'promoEndDate',c.name As 'cityName',da.description As 'promoDiscountType', c.name As 'cityName',CONCAT(p.firstName,' ',p.lastName) As 'passengerName'
         * from passengerpromocodedetails As pc
         * left join passenger As p on pc.passengerId=p.id
         * left join dataattributes As da on da.enumName = pc.promoDiscountType
         * left join city As c on c.id=pc.cityId
         * where pc.status='Y' AND pc.promoStartDate >='".getCurrentDateTime()."' AND pc.promoEndDate <='".getCurrentDateTime()."' order by pc.promoStartDate";
         */
        // removed pc.promoStartDate >= condition and added da.tableName='PROMOCODE_TYPE', and added promoCodeLimit column
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = " AND pc.cityId=" . $city_id;
        }
        $query = "SELECT pc.id As 'id',pc.promoCode As 'promoCode',pc.promoDiscountAmount As 'promoDiscountAmount',
                       pc.status As 'status',pc.promoStartDate As 'promoStartDate',pc.promoCodeLimit As 'promoCodeLimit',pc.promoCodeUserLimit As 'promoCodeUserLimit',
		       pc.promoEndDate As 'promoEndDate',c.name As 'cityName',da.description As 'promoDiscountType',
                       c.name As 'cityName',CONCAT(p.firstName,' ',p.lastName) As 'passengerName'
		       from passengerpromocodedetails As pc
		       left join passenger As p on pc.passengerId=p.id
		       left join dataattributes As da on da.enumName = pc.promoDiscountType AND da.tableName='PROMOCODE_TYPE'
		       left join city As c on c.id=pc.cityId
		       where pc.promoStartDate <='" . getCurrentDateTime() . "'  " . $where_city . " order by pc.promoStartDate";

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getStateList()
    {
        $query = "SELECT s.id As 'id',s.name As 'stateName',c.name As 'countryName',s.status As 'status'
				from state As s
				left join country As c on c.id=s.countryId";

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getTariffList($city_id = NULL, $company_id = NULL)
    {
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = " where rc.cityId=" . $city_id;
        }

        $query = "SELECT rc.id As 'id',rc.name As 'name',rc.dayConvenienceCharge As 'dayConvenienceCharge',rc.nightConvenienceCharge As 'nightConvenienceCharge',rc.fixedAmount As 'fixedAmount',rc.status As 'status',c.name As 'cityName'
				from ratecarddetails As rc
				left join city As c on c.id=rc.cityId " . $where_city;

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getDriverTariffList()
    {
        $query = "SELECT drc.* from driversratecarddetails As drc GROUP BY drc.driverType";

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getTransactionList($city_id = NULL, $company_id = NULL, $is_zuver = NULL)
    {
        $where_city = "";
        $where_company = "";
        $where_is_zuver = " AND td.companyId != " . DEFAULT_COMPANY_ID;
        if ($is_zuver) {
            $where_is_zuver = " AND td.companyId=" . DEFAULT_COMPANY_ID;
        }
        if ($city_id) {
            $where_city = " AND td.bookingCityId=" . $city_id;
        }
        if ($company_id) {
            $where_company = " AND td.companyId=" . $company_id;
        }
        $query = "SELECT ttd.id As 'id',td.mapUrl AS 'mapUrl', td.id As 'tripId', p.firstName As passengerFirstName,p.lastName As 'passengerLastName',d.firstName As driverFirstName,d.lastName As 'driverLastName',
				ttd.invoiceNo As 'invoiceNo',td.bookingKey As 'bookingKey',td.actualPickupDatetime As 'actualPickupDatetime',das.description As 'status',
				dat.description As 'tripType',dap.description As 'paymentMode',cp.name As 'companyName',c.name As 'cityName', ttd.transactionId As 'transactionId',
				ttd.companyTax As 'companyTax',ttd.totalTripCost As 'totalTripCost',(ttd.convenienceCharge + ttd.travelCharge + ttd.parkingCharge + ttd.tollCharge) As 'tripFare',
				(ttd.promoDiscountAmount + ttd.otherDiscountAmount) As 'DiscountAmount',ttd.driverEarning As 'driverEarning',ttd.paymentStatus As 'paymentStatus'
				from triptransactiondetails As ttd
				left join tripdetails As td on ttd.tripId=td.id
				left join passenger As p on td.passengerId=p.id
				left join driver As d on td.driverId=d.id
				left join dataattributes As das on das.enumName = td.tripStatus AND das.tableName='TRIP_STATUS'
				left join dataattributes As dat on dat.enumName = td.tripType AND dat.tableName='TRIP_TYPE'
				left join dataattributes As dap on dap.enumName = td.paymentMode AND dap.tableName='PAYMENT_MODE'
				left join corporatepartnersdetails As cp on cp.id = td.companyId
				left join city As c on c.id=td.bookingCityId
				where td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' " . $where_city . " " . $where_company . " " . $where_is_zuver . " ORDER BY ttd.id DESC";

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getEmergencyList($city_id = NULL, $company_id = NULL)
    {
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = " AND p.cityId=" . $city_id;
        }
        if ($company_id) {
            $where_company = " AND p.companyId=" . $company_id;
        }
        $query = "SELECT er.id As 'id',p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',er.location As 'location',er.latitude As 'latitude',da.description As 'emergencyStatus',er.longitude As 'longitude'
				from emergencyrequestdetails As er
				left join passenger As p on p.id=er.passengerId
				left join dataattributes As da on da.enumName = er.emergencyStatus AND da.tableName='EMERGENCY_STATUS'
				where er.emergencyStatus !='CL' " . $where_city;
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getWalletList($passenger_id, $city_id = NULL, $company_id = NULL)
    {
        $where_passenger = "pwd.passengerId= '" . $passenger_id . "' ";
        $where_city = "";
        $where_company = "";
        if ($city_id) {
            $where_city = " AND p.cityId=" . $city_id;
        }
        if ($company_id) {
            $where_company = " AND p.companyId=" . $company_id;
        }
        $query = "SELECT pwd.id As 'id',pwd.transactionAmount As 'transactionAmount',pwd.previousAmount As 'previousAmount',
                    pwd.currentAmount As 'currentAmount',pwd.updatedDate As 'modifyDatetime',pwd.passengerReferralId As 'passengerReferralId',
                    pwd.tripId As 'tripId',pwd.transactionStatus As 'transactionStatus',pwd.transactionId As 'transactionId',
                    dat.description As 'transactionType',da.description As 'manipulationType',pwd.comments As 'comments',
                    pwd.manipulationType As 'manipulationTypeStatus',pwd.transactionType As 'transactionTypeStatus',dap.description As 'paymentMode',
                    p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',p.mobile As 'passengerMobile',p.email As 'passengerEmail',
                    u.firstName As 'userFirstName',u.lastName As 'userLastName',u.internalEmail As 'userInternalEmail'
                    from passengerwalletdetails As pwd
                    left join passenger As p on p.id=pwd.passengerId
                    left join userlogincredential As u on u.id=pwd.updatedBy
                    left join tripdetails As td on td.id=pwd.tripId
                    left join dataattributes As da on da.enumName = pwd.manipulationType AND da.tableName='MANIPULATION_TYPE'
                    left join dataattributes As dat on dat.enumName = pwd.transactionType AND dat.tableName='TRANSACTION_TYPE'
                    left join dataattributes As dap on dap.enumName = td.paymentMode AND dap.tableName='PAYMENT_MODE'
                    where " . $where_passenger . ' ' . $where_city . " " . $where_company;
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getCompanyNameById($company_id)
    {
        $company_name = "";
        $this->db->select('name');
        $this->db->from('corporatepartnersdetails');
        $this->db->where('id', $company_id);
        $query = $this->db->get();
        $res = $query->row();
        if ($res) {
            $company_name = $res->name;
        }
        return $company_name;
    }

    // created by Aditya on May 31 2017
    //
    public function getB2BDriverList($city_id = '', $company_id = '', $trip_status = '', $is_zuver = NULL)
    {
        $filter_city_qry = "";
        $filter_company_qry = "";
        $where_is_zuver = " AND td.companyId != " . DEFAULT_COMPANY_ID;
        if ($is_zuver) {
            $where_is_zuver = " AND td.companyId=" . DEFAULT_COMPANY_ID;
        }
        /*         * * city based filter ** */
        if ($city_id != '') {
            $filter_city_qry = " AND bookingCityId=$city_id";
        }
        /*         * * company based filter ** */
        if ($company_id != '') {
            $filter_company_qry = " AND td.companyId=$company_id";
        }
        /*         * * Trip status filter   ** */

        $trip_status_filter = " where td.tripStatus NOT IN ('" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "','" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "','" . Trip_Status_Enum::NO_NOTIFICATION . "','" . Trip_Status_Enum::TRIP_COMPLETED . "')";

        $query = "
                SELECT
                    td.id As 'id', td.passengerId As 'passengerId', td.driverId As 'driverId', td.bookingKey As 'bookingKey', td.companyId As 'companyId',
				    td.bookingCityId As 'bookingCityId', td.pickupLocation As 'pickupLocation', td.pickupLatitude As 'pickupLatitude', td.pickupLongitude As 'pickupLongitude',
				    td.dropLocation As 'dropLocation', td.dropLatitude As 'dropLatitude', td.dropLongitude As 'dropLongitude', td.pickupDatetime As 'pickupDatetime',
                    td.createdDate As 'createdDate', td.tripStatus,

                    d.firstName As 'driverFirstName', d.lastName As 'driverLastName', d.mobile As driverMobile,

                    dtti.latitude AS dttiLat, dtti.longitude AS dttiLon,

                    cpd.address

                FROM
                    tripdetails td

                left join
                    corporatepartnersdetails cpd
                    ON td.companyId = cpd.id

                left join
                    driver As d ON d.id=td.driverId

                left join
                    (
                        SELECT
                            driverId, latitude, longitude, createdDate
                        FROM
                            drivertemptrackinginfo
                        ORDER BY
                            createdDate DESC
                        LIMIT
                            0,1
                    ) AS dtti
                    ON td.driverId = dtti.driverId

                $trip_status_filter " .
            $filter_city_qry . " " .
            $filter_company_qry . " " .
            $where_is_zuver . "
                AND td.driverId > 0
                GROUP BY
                   td.id
                ORDER BY
                    td.tripStatus DESC,
                    td.updatedDate DESC,
                    td.pickupDatetime DESC
                ;";

        /*
SELECT
    td.id As 'id', td.passengerId As 'passengerId', td.driverId As 'driverId', td.bookingKey As 'bookingKey', td.companyId As 'companyId', td.bookingCityId As 'bookingCityId', td.pickupLocation As 'pickupLocation', td.pickupLatitude As 'pickupLatitude', td.pickupLongitude As 'pickupLongitude', td.dropLocation As 'dropLocation', td.dropLatitude As 'dropLatitude', td.dropLongitude As 'dropLongitude', td.pickupDatetime As 'pickupDatetime', td.createdDate As 'createdDate', d.firstName As 'driverFirstName', d.lastName As 'driverLastName', d.mobile As driverMobile, td.tripStatus,

    dtti.latitude AS dttiLat, dtti.longitude AS dttiLon

FROM
    tripdetails td

left join
    driver As d ON d.id = td.driverId

left join
    (
        SELECT
            driverId, latitude, longitude, createdDate
        FROM
            drivertemptrackinginfo
        ORDER BY
            createdDate DESC
        LIMIT
            0,1
    ) AS dtti
    ON td.driverId = dtti.driverId

WHERE
    td.tripStatus NOT IN ('CBP','CBD','NON','TCT')
AND td.companyId=30
AND td.companyId != 1
AND td.driverId > 0
GROUP BY
    td.id
ORDER BY
    td.tripStatus DESC,
    td.updatedDate DESC,
    td.pickupDatetime DESC
        */

        //echo $query.'<br/><br/>';
        //die();

        $result = $this->db->query($query);//$this->getTripList($city_id, $company_id, $trip_status, $is_zuver);
        $tripResult = $this->fetchAll($result);
        //print_r($tripResult);
        //echo '<br/><br/>';
        $driverArray = [];
        foreach ($tripResult as $key => $value) {
            if ($value->driverId > 0) {
                $tmpArray = [];
                $tmpArray['driverId'] = $value->driverId;
                $tmpArray['d_lat'] = $value->dttiLat;
                $tmpArray['d_lon'] = $value->dttiLon;

                $tmpArray['p_address'] = $value->address;

                $driverArray[] = $tmpArray;
            }
        }
        return $driverArray;
    }

/*
* Code updated By Anuj Tiwari
*/
    public function getTripList($city_id = '', $company_id = '', $trip_status = '', $is_zuver = NULL)
    {
        $filter_city_qry = "";

        $filter_company_qry = "";

        $where_is_zuver = " AND td.companyId != " . DEFAULT_COMPANY_ID;

        if ($is_zuver) {

            $where_is_zuver = " AND td.companyId=" . DEFAULT_COMPANY_ID;

        }
        /*         * * city based filter ** */

        if ($city_id != '') {

            $filter_city_qry = " AND bookingCityId=$city_id";

        }
        /*         * * company based filter ** */

        if ($company_id != '') {

            $filter_company_qry = " AND td.companyId=$company_id";

        }

        /*         * * Trip status filter   ** */

        if ($trip_status == 'freshTrip') {

            $trip_status_filter = " where td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "') ";

        } elseif ($trip_status == 'unassigned') {

            $trip_status_filter = " where td.tripStatus IN ('" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "') ";

        } elseif ($trip_status == 'assigned') {

            $trip_status_filter = " where td.tripStatus IN ('" . Trip_Status_Enum::TRIP_CONFIRMED . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "','" . Trip_Status_Enum::READY_TO_START . "')";

        } elseif ($trip_status == 'inprogress') {

            $trip_status_filter = " where td.tripStatus IN ('" . Trip_Status_Enum::DRIVER_ARRIVED . "','" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::NO_NOTIFICATION . "')";

        } elseif ($trip_status == 'completed') {

            $trip_status_filter = " where td.tripStatus IN ('" . Trip_Status_Enum::TRIP_COMPLETED . "')";


        } else if ($trip_status == 'cancelled') {

            $trip_status_filter = " where td.tripStatus IN ('" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "')";

        } else {

            $trip_status_filter = " where td.tripStatus !='" . Trip_Status_Enum::TRIP_COMPLETED ."' ";

        }

         $query = " SELECT td.id As 'id',td.passengerId As 'passengerId',td.driverId As 'driverId',td.bookingKey As 'bookingKey',td.companyId As 'companyId',
				   td.bookingCityId As 'bookingCityId',td.pickupLocation As 'pickupLocation', td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
				   td.dropLocation As 'dropLocation',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupDatetime As 'pickupDatetime',
                   td.driverAssignedDatetime As 'driverAssignedDatetime',td.actualPickupDatetime As 'actualPickupDatetime',td.dropDatetime As 'dropDatetime',td.createdDate As 'createdDate',
                   td.clientName As 'clientName',td.vehicleNo As 'vehicleNo',td.vehicleMake As 'vehicleMake',td.vehicleModel As 'vehicleModel',
				   das.description As 'tripStatus',dan.description As 'notificationStatus',daa.description As 'driverAcceptedStatus',dat.description As 'tripType',
				   dapm.description As 'paymentMode',datt.description As 'transmissionType',c.name As 'company',c.phone As 'phone',ct.name As 'city',dact.description As 'carType',
				   p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',p.mobile As passengerMobile,
				   d.firstName As 'driverFirstName',d.lastName As 'driverLastName',d.mobile As driverMobile,
				   u.firstName As 'userFirstName',u.lastName As 'userLastName',up.mobile As 'userMobile',
				   td.tripStatus As 'tripStatusEnum',dabt.description As 'billType'
				   from tripdetails td
				   left join passenger As p ON p.id=td.passengerId
				   left join driver As d ON d.id=td.driverId
                   left join dataattributes As das on das.enumName   = td.tripStatus AND das.tableName='TRIP_STATUS'
                   left join dataattributes As dan on dan.enumName   = td.notificationStatus AND dan.tableName='TRIP_STATUS'
                   left join dataattributes As daa on daa.enumName   = td.driverAcceptedStatus AND daa.tableName='DRIVER_ACCEPTED_STATUS'
                   left join dataattributes As dat on dat.enumName   = td.tripType AND dat.tableName='TRIP_TYPE'
                   left join dataattributes As dapm on dapm.enumName = td.paymentMode AND dapm.tableName='PAYMENT_MODE'
                   left join dataattributes As datt on datt.enumName = td.transmissionType AND datt.tableName='TRANSMISSION_TYPE'
                   left join dataattributes As dact on dact.enumName = td.carType AND dact.tableName='CAR_TYPE'
                   left join dataattributes As dabt on dabt.enumName = td.billType AND dabt.tableName='BILL_TYPE'
                   left join city ct on td.bookingCityId = ct.id
                   left join corporatepartnersdetails c on td.companyId = c.id
                   left join userlogincredential u on td.createdBy = u.id
                   left join userpersonaldetails up on up.userId = u.id
                   $trip_status_filter " . $filter_city_qry . " " . $filter_company_qry . " " . $where_is_zuver . " GROUP BY td.id ORDER BY td.tripStatus DESC,td.updatedDate DESC,td.pickupDatetime DESC";

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    /*
    *  Function getting used by Assign Driver popup in Trip Details page to populate Available Drivers list for assignment
    */
    public function getDriverListForAssign($trip_id)
    {

        /* Commented by Aditya on 28th Apr because result coming from this query is not being used anywhere in this function */
        $query = "SELECT drd.rejectedDrivers  As 'rejectedDrivers' FROM driverrequestdetails As drd where drd.tripid=" . $trip_id . " ORDER BY drd.tripid DESC LIMIT 0,1";
        $result = $this->db->query($query);
        $rejected_driver = $this->fetchAll($result);
        $rejected_driver_where = '';


        /*if (count($rejected_driver) > 0)
        {

            $rejected_driver_where=" AND d.id NOT IN (".$rejected_driver[0]->rejectedDrivers.")";
        }*/

        /*original query
            $query = "SELECT d.id As  'id', CONCAT( d.firstName,  ' ', d.lastName ) As  'driver_name', d.mobile As  'mobile', d.driverLanguagesKnown As  'driverLanguagesKnown',
                  d.driverExperience As  'driverExperience',da.description As 'skill',d.loginStatus As 'loginStatus',
                  111.045 * DEGREES(ACOS(COS(RADIANS(td.pickupLatitude))
                  * COS(RADIANS(d.currentLatitude))
                  * COS(RADIANS(d.currentLongitude) - RADIANS(td.pickupLongitude))
                  + SIN(RADIANS(td.pickupLatitude))
                  * SIN(RADIANS(d.currentLatitude))))
                  As 'distance',
                  (SELECT dsh.shiftStatus FROM drivershifthistory As dsh where dsh.driverId=d.id ORDER BY id DESC LIMIT 0,1) As 'shiftStatus',
                  (SELECT dsh.availabilityStatus FROM drivershifthistory As dsh where dsh.driverId=d.id ORDER BY id DESC LIMIT 0,1) As 'availabilityStatus',
                  (SELECT AVG(td.passengerRating) FROM tripdetails AS td where td.driverId=d.id AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "') As 'driverRating'
                  FROM tripdetails As td

                  left join driver As d ON d.cityId=td.bookingCityId ".$rejected_driver_where."
                  left join dataattributes As da on da.enumName = d.skills AND da.tableName='TRANSMISSION_TYPE'
                  left join drivershifthistory As dsh ON dsh.driverId = d.id AND dsh.availabilityStatus!='" . Driver_Available_Status_Enum::NOTAVAILABLE . "'
                  where td.id=" . $trip_id . " AND d.status='" . Status_Type_Enum::ACTIVE . "'  AND d.isDeleted='" . Status_Type_Enum::INACTIVE . "'  GROUP BY d.id ORDER BY driverRating DESC,distance,availabilityStatus DESC,shiftStatus";
        */

        $query = "
            SELECT
                d.id As 'id', CONCAT( d.firstName,  ' ', d.lastName ) As 'driver_name', d.mobile As 'mobile', d.driverLanguagesKnown As 'driverLanguagesKnown',
				  d.driverExperience As 'driverExperience', d.loginStatus As 'loginStatus',

                  da.description As 'skill',

                  0 AS driverRating,

                  CASE
                    WHEN ((dtti.latitude > 0) AND (dtti.longitude > 0))
                    THEN
                        111.045
                        * DEGREES(ACOS(COS(RADIANS(td.pickupLatitude))
                        * COS(RADIANS(dtti.latitude))
                        * COS(RADIANS(dtti.longitude)
                        - RADIANS(td.pickupLongitude))
                        + SIN(RADIANS(td.pickupLatitude))
                        * SIN(RADIANS(dtti.latitude))))
                    WHEN ((d.currentLatitude > 0) AND (d.currentLongitude > 0))
                    THEN
                        111.045
                        * DEGREES(ACOS(COS(RADIANS(td.pickupLatitude))
                        * COS(RADIANS(d.currentLatitude))
                        * COS(RADIANS(d.currentLongitude)
                        - RADIANS(td.pickupLongitude))
                        + SIN(RADIANS(td.pickupLatitude))
                        * SIN(RADIANS(d.currentLatitude))))
                    ELSE
                        0
                END AS distance,

        		  dsh.shiftStatus, dsh.availabilityStatus

                  FROM
                    tripdetails As td

				  left join
                    driver As d
                  ON d.cityId=td.bookingCityId " . $rejected_driver_where . "

                  left join
                        dataattributes As da
                  on    da.enumName = d.skills AND da.tableName='TRANSMISSION_TYPE'

                  left join
                    (
                        SELECT
                            driverId, shiftStatus, availabilityStatus
                        FROM
                            drivershifthistory
                        ORDER BY
                            id DESC
                        LIMIT
                            0,1
                    ) As dsh ON
                    dsh.driverId = d.id
                  AND   dsh.availabilityStatus!='" . Driver_Available_Status_Enum::NOTAVAILABLE . "'

                  left join
                    (
                        SELECT
                            driverId, latitude, longitude
                        FROM
                            drivertemptrackinginfo
                        ORDER BY
                            createdDate DESC
                        LIMIT
                            0,1
                    ) As dtti ON d.id = dtti.driverId

                  where
                        td.id=" . $trip_id . "
                    AND d.status='" . Status_Type_Enum::ACTIVE . "'
                    AND d.isDeleted='" . Status_Type_Enum::INACTIVE . "'
                    GROUP BY
                        d.id
                    ORDER BY
                        distance,
                        availabilityStatus DESC,
                        shiftStatus";
        /*$query = "
SELECT
    d.id As  'id', CONCAT( d.firstName,  ' ', d.lastName ) As  'driver_name', d.mobile As  'mobile', d.driverLanguagesKnown As  'driverLanguagesKnown',d.currentLatitude ,d.currentLongitude, d.driverExperience As  'driverExperience',da.description As 'skill',d.loginStatus As 'loginStatus',

    dtti.latitude, dtti.longitude,

    CASE
        WHEN ((dtti.latitude > 0) AND (dtti.longitude > 0))
        THEN
            111.045
            * DEGREES(ACOS(COS(RADIANS(".$inPLat."))
            * COS(RADIANS(dtti.latitude))
            * COS(RADIANS(dtti.longitude)
            - RADIANS(".$inPLon."))
            + SIN(RADIANS(".$inPLat."))
            * SIN(RADIANS(dtti.latitude))))
        WHEN ((d.currentLatitude > 0) AND (d.currentLongitude > 0))
        THEN
            111.045
            * DEGREES(ACOS(COS(RADIANS(".$inPLat."))
            * COS(RADIANS(d.currentLatitude))
            * COS(RADIANS(d.currentLongitude)
            - RADIANS(".$inPLon."))
            + SIN(RADIANS(".$inPLat."))
            * SIN(RADIANS(d.currentLatitude))))
        ELSE
            0
    END AS distance,

    dsh.shiftStatus, dsh.availabilityStatus

    FROM
        driver As d

    left join
        dataattributes As da on da.enumName = d.skills
    AND da.tableName='TRANSMISSION_TYPE'

    left join
    (
        SELECT
            driverId, shiftStatus, availabilityStatus
        FROM
            drivershifthistory
        ORDER BY
            id DESC
    ) As dsh ON d.id = dsh.driverId

    left join
    (
        SELECT
            driverId, latitude, longitude
        FROM
            drivertemptrackinginfo
        ORDER BY
            createdDate DESC
        LIMIT
            0,1
    ) As dtti ON d.id = dtti.driverId

    where
        d.cityId=".$city_id."
    AND d.status='" . Status_Type_Enum::ACTIVE . "'
    AND d.isDeleted='" . Status_Type_Enum::INACTIVE . "'
    GROUP BY
        d.id
    ORDER BY
        availabilityStatus DESC, shiftStatus
                    ";*/
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getCurrentDriversForMapByCity($city_id, $inPLat = '', $inPLon = '')
    {

        /*$query = "
SELECT
    d.id As  'id', CONCAT( d.firstName,  ' ', d.lastName ) As  'driver_name', d.mobile As  'mobile', d.driverLanguagesKnown As  'driverLanguagesKnown',d.currentLatitude ,d.currentLongitude, d.driverExperience As  'driverExperience',da.description As 'skill',d.loginStatus As 'loginStatus',

    111.045 * DEGREES(ACOS(COS(RADIANS(td.pickupLatitude))
	* COS(RADIANS(d.currentLatitude))
	* COS(RADIANS(d.currentLongitude) - RADIANS(td.pickupLongitude))
	+ SIN(RADIANS(td.pickupLatitude))
	* SIN(RADIANS(d.currentLatitude))))
	As 'distance',

    (
        SELECT
            dsh.shiftStatus
        FROM
            drivershifthistory As dsh
        where
            dsh.driverId=d.id
        ORDER BY
            id DESC
        LIMIT
            0,1
    ) As 'shiftStatus',
	(
        SELECT
            dsh.availabilityStatus
        FROM
            drivershifthistory As dsh
        where
            dsh.driverId=d.id
        ORDER BY
            id DESC
        LIMIT 0,1
    ) As 'availabilityStatus',
	(
        SELECT
            AVG(td.passengerRating)
        FROM
            tripdetails AS td
        where
            td.driverId=d.id
        AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "'
    ) As 'driverRating'

    FROM
        tripdetails As td

    left join driver As d ON d.companyId=td.companyId AND d.cityId=td.bookingCityId

    left join dataattributes As da on da.enumName = d.skills AND da.tableName='TRANSMISSION_TYPE'

    left join drivershifthistory As dsh ON dsh.driverId = d.id

    where
        d.cityId=".$city_id."
    AND d.status='" . Status_Type_Enum::ACTIVE . "'
    AND d.isDeleted='" . Status_Type_Enum::INACTIVE . "'
    AND dsh.availabilityStatus!='" . Driver_Available_Status_Enum::NOTAVAILABLE . "'
    GROUP BY
        d.id
    ORDER BY
        driverRating DESC,distance,availabilityStatus DESC,shiftStatus";*/

        /*$query = "
    SELECT
        d.id As  'id', CONCAT( d.firstName,  ' ', d.lastName ) As  'driver_name', d.mobile As  'mobile', d.driverLanguagesKnown As  'driverLanguagesKnown',d.currentLatitude ,d.currentLongitude, d.driverExperience As  'driverExperience',da.description As 'skill',d.loginStatus As 'loginStatus',

        111.045
        * DEGREES(ACOS(COS(RADIANS(td.pickupLatitude))
        * COS(RADIANS(d.currentLatitude))
        * COS(RADIANS(d.currentLongitude)
        - RADIANS(td.pickupLongitude))
        + SIN(RADIANS(td.pickupLatitude))
        * SIN(RADIANS(d.currentLatitude))))
        As 'distance',

        dsh.shiftStatus, dsh.availabilityStatus

        FROM
            tripdetails As td

        left join
            driver As d ON d.companyId=td.companyId
        AND d.cityId=td.bookingCityId

        left join
            dataattributes As da on da.enumName = d.skills
        AND da.tableName='TRANSMISSION_TYPE'

        left join
        (
            SELECT
                driverId, shiftStatus, availabilityStatus
            FROM
                drivershifthistory
            ORDER BY
                id DESC
        ) As dsh ON d.id = dsh.driverId

        where
            d.cityId=".$city_id."
        AND d.status='" . Status_Type_Enum::ACTIVE . "'
        AND d.isDeleted='" . Status_Type_Enum::INACTIVE . "'
        GROUP BY
            d.id
        ORDER BY
            distance, availabilityStatus DESC, shiftStatus
                        ";*/

        /*$query = "
SELECT
    d.id As  'id', CONCAT( d.firstName,  ' ', d.lastName ) As  'driver_name', d.mobile As  'mobile', d.driverLanguagesKnown As  'driverLanguagesKnown',d.currentLatitude ,d.currentLongitude, d.driverExperience As  'driverExperience',da.description As 'skill',d.loginStatus As 'loginStatus',

    111.045 * DEGREES(ACOS(COS(RADIANS(td.pickupLatitude))
    * COS(RADIANS(d.currentLatitude))
    * COS(RADIANS(d.currentLongitude) - RADIANS(td.pickupLongitude))
    + SIN(RADIANS(td.pickupLatitude))
    * SIN(RADIANS(d.currentLatitude))))
    As 'distance',

    (
        SELECT
            dsh.shiftStatus
        FROM
            drivershifthistory As dsh
        where
            dsh.driverId=d.id
        ORDER BY
            id DESC
        LIMIT
            0,1
    ) As 'shiftStatus',
    (
        SELECT
            dsh.availabilityStatus
        FROM
            drivershifthistory As dsh
        where
            dsh.driverId=d.id
        ORDER BY
            id DESC
        LIMIT 0,1
    ) As 'availabilityStatus',
    (
        SELECT
            AVG(td.passengerRating)
        FROM
            tripdetails AS td
        where
            td.driverId=d.id
        AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "'
    ) As 'driverRating'

    FROM
        tripdetails As td

    left join driver As d ON d.companyId=td.companyId AND d.cityId=td.bookingCityId

    left join dataattributes As da on da.enumName = d.skills AND da.tableName='TRANSMISSION_TYPE'

    left join drivershifthistory As dsh ON dsh.driverId = d.id

    where
        d.cityId=".$city_id."
    AND d.status='" . Status_Type_Enum::ACTIVE . "'
    AND d.isDeleted='" . Status_Type_Enum::INACTIVE . "'
    AND dsh.availabilityStatus!='" . Driver_Available_Status_Enum::NOTAVAILABLE . "'
    GROUP BY
        d.id
    ORDER BY
        driverRating DESC,distance,availabilityStatus DESC,shiftStatus";*/

        /*$query = "
    SELECT
        d.id As  'id', CONCAT( d.firstName,  ' ', d.lastName ) As  'driver_name', d.mobile As  'mobile', d.driverLanguagesKnown As  'driverLanguagesKnown',d.currentLatitude ,d.currentLongitude, d.driverExperience As  'driverExperience',da.description As 'skill',d.loginStatus As 'loginStatus',

        dtti.latitude, dtti.longitude,

        CASE
            WHEN ((dtti.latitude <= 0) AND (dtti.longitude <= 0))
            THEN
                111.045
                * DEGREES(ACOS(COS(RADIANS(td.pickupLatitude))
                * COS(RADIANS(d.currentLatitude))
                * COS(RADIANS(d.currentLongitude)
                - RADIANS(td.pickupLongitude))
                + SIN(RADIANS(td.pickupLatitude))
                * SIN(RADIANS(d.currentLatitude))))
            ELSE
                111.045
                * DEGREES(ACOS(COS(RADIANS(td.pickupLatitude))
                * COS(RADIANS(dtti.latitude))
                * COS(RADIANS(dtti.longitude)
                - RADIANS(td.pickupLongitude))
                + SIN(RADIANS(td.pickupLatitude))
                * SIN(RADIANS(dtti.latitude))))
        END AS distance,

        dsh.shiftStatus, dsh.availabilityStatus

        FROM
            tripdetails As td

        left join
            driver As d ON d.companyId=td.companyId
        AND d.cityId=td.bookingCityId

        left join
            dataattributes As da on da.enumName = d.skills
        AND da.tableName='TRANSMISSION_TYPE'

        left join
        (
            SELECT
                driverId, shiftStatus, availabilityStatus
            FROM
                drivershifthistory
            ORDER BY
                id DESC
        ) As dsh ON d.id = dsh.driverId

        left join
        (
            SELECT
                driverId, latitude, longitude
            FROM
                drivertemptrackinginfo
            ORDER BY
                createdDate DESC
            LIMIT
                0,1
        ) As dtti ON d.id = dtti.driverId

        where
            d.cityId=".$city_id."
        AND d.status='" . Status_Type_Enum::ACTIVE . "'
        AND d.isDeleted='" . Status_Type_Enum::INACTIVE . "'
        GROUP BY
            d.id
        ORDER BY
            distance, availabilityStatus DESC, shiftStatus
                        ";*/

        $distanceQueryStatement = ' 0 AS distance,';
        if (($inPLat != '') && ($inPLon != '')) {
            $distanceQueryStatement = '
            CASE
                WHEN ((dtti.latitude > 0) AND (dtti.longitude > 0))
                THEN
                    111.045
                    * DEGREES(ACOS(COS(RADIANS(".$inPLat."))
                    * COS(RADIANS(dtti.latitude))
                    * COS(RADIANS(dtti.longitude)
                    - RADIANS(".$inPLon."))
                    + SIN(RADIANS(".$inPLat."))
                    * SIN(RADIANS(dtti.latitude))))
                WHEN ((d.currentLatitude > 0) AND (d.currentLongitude > 0))
                THEN
                    111.045
                    * DEGREES(ACOS(COS(RADIANS(".$inPLat."))
                    * COS(RADIANS(d.currentLatitude))
                    * COS(RADIANS(d.currentLongitude)
                    - RADIANS(".$inPLon."))
                    + SIN(RADIANS(".$inPLat."))
                    * SIN(RADIANS(d.currentLatitude))))
                ELSE
                    0
            END AS distance,
        ';
        }
        $query = "
SELECT
    d.id As  'id', CONCAT( d.firstName,  ' ', d.lastName ) As  'driver_name', d.mobile As  'mobile', d.driverLanguagesKnown As  'driverLanguagesKnown',d.currentLatitude ,d.currentLongitude, d.driverExperience As  'driverExperience',da.description As 'skill',d.loginStatus As 'loginStatus',

    dtti.latitude, dtti.longitude,

    " . $distanceQueryStatement . "

    dsh.shiftStatus, dsh.availabilityStatus

    FROM
        driver As d

    left join
        dataattributes As da on da.enumName = d.skills
    AND da.tableName='TRANSMISSION_TYPE'

    left join
    (
        SELECT
            driverId, shiftStatus, availabilityStatus
        FROM
            drivershifthistory
        ORDER BY
            id DESC
    ) As dsh ON d.id = dsh.driverId

    left join
    (
        SELECT
            driverId, latitude, longitude
        FROM
            drivertemptrackinginfo
        ORDER BY
            createdDate DESC
        LIMIT
            0,1
    ) As dtti ON d.id = dtti.driverId

    where
        d.cityId=" . $city_id . "
    AND d.status='" . Status_Type_Enum::ACTIVE . "'
    AND d.isDeleted='" . Status_Type_Enum::INACTIVE . "'
    GROUP BY
        d.id
    ORDER BY
        availabilityStatus DESC, shiftStatus
                    ";

        //return $this->_compile_select();
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    ///----------------------------------------------Start Reports query----------------------------------------------------

    public function getCompletedTripReports($trip_type, $payment_mode, $city_id, $company_id, $partner_user_id, $date_filter_type, $start_date = NULL, $end_date = NULL)
    {
        //instialize by default value for parmeters data
        $where_caluse = '';
        $trip_type_where = "td.tripType='" . $trip_type . "'";
        $payment_mode_where = "td.paymentMode='" . $payment_mode . "'";

        $city_id_where = '';
        $company_id_where = '';
        $partner_id_where = '';
        if ($city_id) {
            $city_id_where = "AND td.bookingCityId=" . $city_id;
        }
        if ($company_id) {
            $company_id_where = "AND td.companyId=" . $company_id;
        }
        if ($partner_user_id) {
            $partner_id_where = "OR td.createdBy=" . $partner_user_id;
        }
        //trip type if not selected by default is all trip type
        if ($trip_type == "" || empty($trip_type)) {
            $trip_type_where = "td.tripType IN ('" . Trip_Type_Enum::ONEWAY_TRIP . "','" . Trip_Type_Enum::RETURN_TRIP . "','" . Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY . "','" . Trip_Type_Enum::OUTSTATION_TRIP_RETURN . "','" . Trip_Type_Enum::OUTSTATION_TRIP . "')";
        }
        //payment mode if not selected by default is all payment mode
        if ($payment_mode == "" || empty($payment_mode)) {
            $payment_mode_where = "td.paymentMode IN ('" . Payment_Mode_Enum::CASH . "','" . Payment_Mode_Enum::PAYTM . "','" . Payment_Mode_Enum::WALLET . "','" . Payment_Mode_Enum::INVOICE . "')";
        }
        $where_caluse = $trip_type_where . " AND " . $payment_mode_where . " " . $city_id_where . " " . $company_id_where . " " . $partner_id_where;

        //date filter based on date filter type M(Month with start_date), Y(Year with start_date), CD(Customize date with start_date & end_date)
        if ($date_filter_type == 'M' && $start_date) {
            $where_caluse .= " AND YEAR(ttd.createdDate)=YEAR('" . $start_date . "') AND MONTH(ttd.createdDate)=MONTH('" . $start_date . "')";
        } else if ($date_filter_type == 'Y' && $start_date) {
            $where_caluse .= " AND YEAR(ttd.createdDate)=YEAR('" . $start_date . "')";
        } else if ($date_filter_type == 'CD' && $start_date && $end_date) {
            $where_caluse .= " AND ttd.createdDate >='" . date('Y-m-d H:i:s', strtotime($start_date)) . "' AND ttd.createdDate <='" . date('Y-m-d H:i:s', strtotime($end_date)) . "'";
        }
        //actual query
        $query = "select td.id As 'tripId',
				SUM(CASE WHEN ttd.totalTripCost THEN ttd.totalTripCost ELSE 0 END) As 'totalTripAmountColleted',
				ttd.totalTripCost As totalTripCost, td.actualPickupDatetime As actualPickupDatetime,dast.description As 'bookedFrom',
				td.driverAssignedDatetime As 'driverAssignedDatetime',td.driverArrivedDatetime As 'driverArrivedDatetime',
				td.pickupLocation As 'pickupLocation',td.createdDate As 'tripCreatedDate',td.promoCode As 'promoCode',
				td.dropDatetime As 'dropDatetime',datt.description As 'transmissionType',ct.name As 'cityName',cpd.name As 'companyName',
				ttd.travelHoursMinutes As 'travelHoursMinutes',td.pickupDatetime As 'pickupDatetime',
				dat.description As 'tripType',dapm.description As 'paymentMode',ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.otherDiscountAmount As 'otherDiscountAmount',
				ttd.convenienceCharge As 'convenienceCharge',ttd.travelCharge As 'travelCharge', ttd.parkingCharge As 'parkingCharge',ttd.tollCharge As 'tollCharge',
				ttd.walletPaymentAmount As 'walletPaymentAmount',ttd.companyTax As 'companyTax',ttd.adminAmount As 'adminAmount',ttd.companyAmount As 'companyAmount',ttd.driverEarning As 'driverEarning',
				p.firstName As 'passengerFirstName', p.lastName As 'passengerLastName',p.mobile As 'passengerMobile',
				d.firstName As 'driverFirstName', d.lastName As 'driverLastName',d.mobile As 'driverMobile',
        		u.firstName As 'userFirstName', u.lastName As 'userLastName',upd.mobile As 'userMobile'
				from triptransactiondetails As ttd
				left join tripdetails As td on ttd.tripId=td.id
				left join passenger As p on p.id=td.passengerId
				left join driver As d on d.id=td.driverId
				left join city As ct on ct.id=td.bookingCityId
				left join corporatepartnersdetails As cpd on cpd.id=td.companyId
        		left join userlogincredential As u on u.id=td.createdBy
        		left join userpersonaldetails As upd on upd.userId=u.id
				left join dataattributes As dat on dat.enumName   = td.tripType AND dat.tableName='TRIP_TYPE'
				left join dataattributes As datt on datt.enumName   = td.transmissionType AND datt.tableName='TRANSMISSION_TYPE'
                left join dataattributes As dapm on dapm.enumName = td.paymentMode AND dapm.tableName='PAYMENT_MODE'
				left join dataattributes As dast on dast.enumName = td.bookedFrom AND dast.tableName='SIGNUP_TYPE'
				where " . $where_caluse . "  AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' group by td.id";
        //std.subTripId, std.pickupLocation As 'subtrip_pickupLocation',std.pickupDatetime As 'subtrip_pickupDatetime',std.dropLocation As 'subtrip_dropLocation',std.dropDatetime As 'subtrip_dropDatetime'
        // left join subtripdetails As std on ttd.tripId=std.masterTripId AND (std.dropDatetime!='' OR std.dropDatetime!='00-00-0000 00:00:00')

        $query2 = "select std.subTripId As 'tripId',
                SUM(CASE WHEN ttd.totalTripCost THEN ttd.totalTripCost ELSE 0 END) As 'totalTripAmountColleted',
                ttd.totalTripCost As totalTripCost, std.pickupDatetime As actualPickupDatetime,dast.description As 'bookedFrom',
                td.driverAssignedDatetime As 'driverAssignedDatetime',td.driverArrivedDatetime As 'driverArrivedDatetime',
                std.pickupLocation As 'pickupLocation',std.createdDate As 'tripCreatedDate',td.promoCode As 'promoCode',
                std.dropDatetime As 'dropDatetime',datt.description As 'transmissionType',ct.name As 'cityName',cpd.name As 'companyName',
                SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(std.dropDatetime,std.pickupDatetime)))) As 'travelHoursMinutes',std.pickupDatetime As 'pickupDatetime',
                dat.description As 'tripType',dapm.description As 'paymentMode',ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.otherDiscountAmount As 'otherDiscountAmount',
                ttd.convenienceCharge As 'convenienceCharge',ttd.travelCharge As 'travelCharge', ttd.parkingCharge As 'parkingCharge',ttd.tollCharge As 'tollCharge',
                ttd.walletPaymentAmount As 'walletPaymentAmount',ttd.companyTax As 'companyTax',ttd.adminAmount As 'adminAmount',ttd.companyAmount As 'companyAmount',ttd.driverEarning As 'driverEarning',
                p.firstName As 'passengerFirstName', p.lastName As 'passengerLastName',p.mobile As 'passengerMobile',
                d.firstName As 'driverFirstName', d.lastName As 'driverLastName',d.mobile As 'driverMobile',
                u.firstName As 'userFirstName', u.lastName As 'userLastName',upd.mobile As 'userMobile'
                from triptransactiondetails As ttd
                left join subtripdetails As std on ttd.tripId=std.masterTripId
                left join tripdetails As td on ttd.tripId=td.id
                left join passenger As p on p.id=td.passengerId
                left join driver As d on d.id=td.driverId
                left join city As ct on ct.id=td.bookingCityId
                left join corporatepartnersdetails As cpd on cpd.id=td.companyId
                left join userlogincredential As u on u.id=td.createdBy
                left join userpersonaldetails As upd on upd.userId=u.id
                left join dataattributes As dat on dat.enumName   = td.tripType AND dat.tableName='TRIP_TYPE'
                left join dataattributes As datt on datt.enumName   = td.transmissionType AND datt.tableName='TRANSMISSION_TYPE'
                left join dataattributes As dapm on dapm.enumName = td.paymentMode AND dapm.tableName='PAYMENT_MODE'
                left join dataattributes As dast on dast.enumName = td.bookedFrom AND dast.tableName='SIGNUP_TYPE'
                where " . $where_caluse . " AND std.id!='' AND (std.dropDatetime!='' OR std.dropDatetime!='00-00-0000 00:00:00') AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' group by std.id";

        $sql = $query . " UNION " . $query2;
        $result = $this->db->query($sql);
        return $this->fetchAll($result);
    }

    public function getCancelledTripReports($trip_type, $cancelled_status, $city_id, $company_id, $date_filter_type, $start_date = NULL, $end_date = NULL)
    {
        //instialize by default value for parmeters data
        $where_caluse = '';
        $trip_type_where = "td.tripType='" . $trip_type . "'";
        $cancelled_status_where = "td.tripStatus='" . $cancelled_status . "'";

        $city_id_where = '';
        $company_id_where = '';
        if ($city_id) {
            $city_id_where = "AND td.bookingCityId=" . $city_id;
        }
        if ($company_id) {
            $company_id_where = "AND td.companyId=" . $company_id;
        }


        //trip type if not selected by default is all trip type
        if ($trip_type == "" || empty($trip_type)) {
            $trip_type_where = "td.tripType IN ('" . Trip_Type_Enum::ONEWAY_TRIP . "','" . Trip_Type_Enum::RETURN_TRIP . "','" . Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY . "','" . Trip_Type_Enum::OUTSTATION_TRIP_RETURN . "','" . Trip_Type_Enum::OUTSTATION_TRIP . "')";
        }
        //payment mode if not selected by default is all payment mode
        if ($cancelled_status == "" || empty($cancelled_status)) {
            $cancelled_status_where = "td.tripStatus IN ('" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "','" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "')";
        }
        $where_caluse = $trip_type_where . " AND " . $cancelled_status_where . " " . $city_id_where . " " . $company_id_where;

        //date filter based on date filter type M(Month with start_date), Y(Year with start_date), CD(Customize date with start_date & end_date)
        if ($date_filter_type == 'M' && $start_date) {
            $where_caluse .= " AND YEAR(td.pickupDatetime)=YEAR('" . $start_date . "') AND MONTH(td.pickupDatetime)=MONTH('" . $start_date . "')";
        } else if ($date_filter_type == 'Y' && $start_date) {
            $where_caluse .= " AND YEAR(td.createdDate)=YEAR('" . $start_date . "')";
        } else if ($date_filter_type == 'CD' && $start_date && $end_date) {
            $where_caluse .= " AND td.pickupDatetime >='" . date('Y-m-d H:i:s', strtotime($start_date)) . "' AND td.pickupDatetime <='" . date('Y-m-d H:i:s', strtotime($end_date)) . "'";
        }
        //actual query
        $query = "select td.id As 'tripId',
				SUM(CASE WHEN td.tripStatus='" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "' THEN 1 ELSE 0 END) As 'totalPassengerCancelledTripCount',
				SUM(CASE WHEN td.tripStatus='" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "' THEN 1 ELSE 0 END) As 'totalDriverCancelledTripCount',
				td.pickupDatetime As pickupDatetime,ct.name As 'cityName',cpd.name As 'companyName',
				dat.description As 'tripType',dats.description As 'tripStatus',td.tripStatus As 'tripStatusType',
				p.firstName As 'passengerFirstName', p.lastName As 'passengerLastName',p.mobile As 'passengerMobile',td.passengerRejectComments As 'passengerRejectComments',
				d.firstName As 'driverFirstName', d.lastName As 'driverLastName',d.mobile As 'driverMobile',drtd.rejectionReason As 'rejectionReason'
				from tripdetails As td
				left join driverrejectedtripdetails As drtd on drtd.tripId=td.id
				left join passenger As p on p.id=td.passengerId
				left join driver As d on d.id=drtd.driverId
				left join city As ct on ct.id=td.bookingCityId
				left join corporatepartnersdetails As cpd on cpd.id=td.companyId
				left join dataattributes As dat on dat.enumName   = td.tripType AND dat.tableName='TRIP_TYPE'
				left join dataattributes As dats on dats.enumName   = td.tripStatus AND dats.tableName='TRIP_STATUS'
				where " . $where_caluse . " group by td.id,drtd.id";
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getTripStatusReports($trip_type, $payment_mode, $trip_status, $city_id, $company_id, $date_filter_type, $start_date = NULL, $end_date = NULL)
    {
        //instialize by default value for parmeters data
        $where_caluse = '';
        $trip_type_where = "td.tripType='" . $trip_type . "'";
        $payment_mode_where = "td.paymentMode='" . $payment_mode . "'";
        $trip_status_where = "td.tripStatus='" . $trip_status . "'";

        $city_id_where = '';
        $company_id_where = '';
        if ($city_id) {
            $city_id_where = "AND td.bookingCityId=" . $city_id;
        }
        if ($company_id) {
            $company_id_where = "AND td.companyId=" . $company_id;
        }

        //trip type if not selected by default is all trip type
        if ($trip_type == "" || empty($trip_type)) {
            $trip_type_where = "td.tripType IN ('" . Trip_Type_Enum::ONEWAY_TRIP . "','" . Trip_Type_Enum::RETURN_TRIP . "','" . Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY . "','" . Trip_Type_Enum::OUTSTATION_TRIP_RETURN . "','" . Trip_Type_Enum::OUTSTATION_TRIP . "')";
        }
        //payment mode if not selected by default is all payment mode
        if ($payment_mode == "" || empty($payment_mode)) {
            $payment_mode_where = "td.paymentMode IN ('" . Payment_Mode_Enum::CASH . "','" . Payment_Mode_Enum::PAYTM . "','" . Payment_Mode_Enum::WALLET . "','" . Payment_Mode_Enum::INVOICE . "')";
        }
        //trip status if not selected by default is all trip status
        if ($trip_status == "" || empty($trip_status)) {
            $trip_status_where = "td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "','" . Trip_Status_Enum::TRIP_COMPLETED . "','" . Trip_Status_Enum::TRIP_CONFIRMED . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "','" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "','" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "','" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "','" . Trip_Status_Enum::READY_TO_START . "')";
        }
        $where_caluse = $trip_type_where . " AND " . $payment_mode_where . " " . $city_id_where . " " . $company_id_where . " AND " . $trip_status_where;

        //date filter based on date filter type M(Month with start_date), Y(Year with start_date), CD(Customize date with start_date & end_date)
        if ($date_filter_type == 'M' && $start_date) {
            $where_caluse .= " AND YEAR(td.createdDate)=YEAR('" . $start_date . "') AND MONTH(td.createdDate)=MONTH('" . $start_date . "')";
        } else if ($date_filter_type == 'Y' && $start_date) {
            $where_caluse .= " AND YEAR(td.createdDate)=YEAR('" . $start_date . "')";
        } else if ($date_filter_type == 'CD' && $start_date && $end_date) {
            $where_caluse .= " AND td.createdDate >='" . date('Y-m-d H:i:s', strtotime($start_date)) . "' AND td.createdDate <='" . date('Y-m-d H:i:s', strtotime($end_date)) . "'";
        }
        //actual query
        $query = "select td.id As 'tripId',
				SUM(CASE WHEN ttd.totalTripCost THEN ttd.totalTripCost ELSE 0 END) As 'totalTripAmountColleted',
				ttd.totalTripCost As totalTripCost, td.actualPickupDatetime As actualPickupDatetime,dast.description As 'bookedFrom',ct.name As 'cityName',cpd.name As 'companyName',
				dat.description As 'tripType',dats.description As 'tripStatus',dapm.description As 'paymentMode',ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.otherDiscountAmount As 'otherDiscountAmount',
				ttd.walletPaymentAmount As 'walletPaymentAmount',ttd.companyTax As 'companyTax',ttd.adminAmount As 'adminAmount',ttd.companyAmount As 'companyAmount',ttd.driverEarning As 'driverEarning',
				p.firstName As 'passengerFirstName', p.lastName As 'passengerLastName',p.mobile As 'passengerMobile',
				d.firstName As 'driverFirstName', d.lastName As 'driverLastName',d.mobile As 'driverMobile'
				from tripdetails As td
				left join triptransactiondetails As ttd on ttd.tripId=td.id
				left join passenger As p on p.id=td.passengerId
				left join driver As d on d.id=td.driverId
				left join city As ct on ct.id=td.bookingCityId
				left join corporatepartnersdetails As cpd on cpd.id=td.companyId
				left join dataattributes As dat on dat.enumName   = td.tripType AND dat.tableName='TRIP_TYPE'
                left join dataattributes As dapm on dapm.enumName = td.paymentMode AND dapm.tableName='PAYMENT_MODE'
				left join dataattributes As dats on dats.enumName = td.tripStatus AND dats.tableName='TRIP_STATUS'
				left join dataattributes As dast on dast.enumName = td.bookedFrom AND dast.tableName='SIGNUP_TYPE'
				where " . $where_caluse . " group by td.id";
        //debug_exit($query);
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    ///----------------------------------------------End Reports query----------------------------------------------------

    public function getPassengerInfoForAutoComplete($keyword = '')
    {
        if ($keyword != '') {
            $query = "SELECT CONCAT(`firstName` ,' ',`lastName`) as passenger_name,id as passenger_id,walletAmount as previous_wallet_amt "
                . "FROM passenger" . " WHERE (mobile like '" . $keyword . "%' OR email like '" . $keyword . "%')"
                . " AND status='" . Status_Type_Enum::ACTIVE . "'"
                . " ORDER BY firstName LIMIT 0,6";
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        }
    }

    public function getDriverLatLongForTripDetails($trip_id = '')
    {
        if ($trip_id != '') {
            $query = "SELECT td.id,td.pickupLatitude,td.pickupLongitude,ttk.latitude as driver_latitude,ttk.longitude as driver_longitude
			FROM `tripdetails` td
			LEFT JOIN triptemptrackinginfo ttk ON td.id = ttk.tripId
			WHERE ttk.latitude<>'NULL' AND ttk.longitude<>'NULL'
			AND td.id='$trip_id' ORDER BY ttk.createdDate DESC LIMIT 0,1;";
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        }
    }

    public function getPassengerInfoForTripBooking($keyword = '', $type = '')
    {
        if ($keyword != '') {
            if ($type == 'mobile') {
                $filter_qry = " WHERE (mobile like '" . $keyword . "%')";
            }
            if ($type == 'email') {
                $filter_qry = " WHERE (email like '" . $keyword . "%')";
            }
            $query = "SELECT CONCAT(`firstName` ,' ',`lastName`) as passenger_name,id as passenger_id,walletAmount as previous_wallet_amt,"
                . " email as passenger_email,mobile as passenger_mobile "
                . " FROM passenger" . " $filter_qry"
                . " AND status='" . Status_Type_Enum::ACTIVE . "'"
                . " ORDER BY firstName LIMIT 0,6";
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        }
    }

    public function getTripsDownload($filter = array())
    {
        $filter_start_date = $filter_city_qry = '';

        if (isset($filter['trip_city'])) {
            $filter_city_qry = " AND bookingCityId=" . $filter['trip_city'];
        }

        if (isset($filter['trip_start_date'])) {
            $filter_start_date = " AND DATE(td.pickupDatetime)=DATE('" . $filter['trip_start_date'] . "')";
        }

        $query = " SELECT td.id As 'tripId', DATE(td.createdDate) As 'createdDate', ct.name As 'city', dat.description As 'tripType', das.description As 'tripStatus', concat(d.firstName,' ',d.lastName) As 'driverName', td.clientName As 'clientName', concat(p.firstName,' ',p.lastName) As 'passengerName',  td.actualPickupDatetime AS 'tripStartTime', td.dropDatetime AS 'tripEndTime', tt.totalTripCost AS 'fareAmount'
                   from tripdetails td
                   left join passenger As p ON p.id=td.passengerId
                   left join driver As d ON d.id=td.driverId
                   left join dataattributes As das on das.enumName   = td.tripStatus AND das.tableName='TRIP_STATUS'
                   left join dataattributes As dat on dat.enumName   = td.tripType AND dat.tableName='TRIP_TYPE'
                   left join city ct on td.bookingCityId = ct.id
                   left join triptransactiondetails tt on td.id = tt.tripId
                   WHERE 1=1 " . $filter_city_qry . " " . $filter_start_date . " GROUP BY td.id ORDER BY td.createdDate DESC";

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    public function getTripsFilterCount($status = NULL, $filter_data = array())
    {
        $where_city = "";
        $where_company = "";
        $where_pickup_date = "";
        $where_pickup_date_month = '';
        if (isset($filter_data['city_id'])) {
            $where_city = " AND td.bookingCityId=" . $filter_data['city_id'];
        }
        if (isset($filter_data['company_id'])) {
            $where_company = " AND td.companyId = " . DEFAULT_COMPANY_ID;
        } else {
            $where_company = " AND td.companyId != " . DEFAULT_COMPANY_ID;
        }

        if (isset($filter_data['pickup_date'])) {
            $where_pickup_date = " AND DATE(td.pickupDatetime)=DATE('" . $filter_data['pickup_date'] . "')";
        }

        if (isset($filter_data['pickup_date_month'])) {
            $where_pickup_date_month = " AND MONTH(td.pickupDatetime)=MONTH('" . $filter_data['pickup_date_month'] . "')";
        }

        if (isset($filter_data['driver_id'])) {
            $where_driver_id = " AND td.driver_id='" . $filter_data['driver_id'] . "'";
        }


        //totalUnassignedTrip
        if ($status == 'UnassignedTrips')
            $select = " SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "') THEN 1 ELSE 0 END)";

        elseif ($status == 'UpcomingTrips')
            //totalUpcomingTrip
            $select = " SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_CONFIRMED . "','" . Trip_Status_Enum::READY_TO_START . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "')  THEN 1 ELSE 0 END) ";

        elseif ($status == 'OngoingTrips')
            //totalOngoingTrip
            $select = " SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "')  THEN 1 ELSE 0 END)";

        elseif ($status == 'CancelledTrips')
            //totalCancelledTrip
            $select = " SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "','" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "')  THEN 1 ELSE 0 END)";

        elseif ($status == 'CompletedTrips')
            //totalCompletedTrip
            $select = " SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_COMPLETED . "')  THEN 1 ELSE 0 END)";

        else
            //Total Trips
            $select = " COUNT(td.id)";

        $query = "SELECT " . $select . " As 'totalTrip' FROM tripdetails As td
        where td.id !=0 " . $where_city . " " . $where_company . " " . $where_pickup_date;

        //echo $query;
        $result = $this->db->query($query);

        if ($result->row()->totalTrip) {
            return $result->row()->totalTrip;
        } else {
            return '0';
        }

    }

    public function getTripsCollectionFilterCount($filter_data = array())
    {

        $where_city = "";
        $where_company = " AND td.companyId != " . DEFAULT_COMPANY_ID;
        $where_pickup_date = "";

        if (isset($filter_data['city_id'])) {
            $where_city = " AND td.bookingCityId=" . $filter_data['city_id'];
        }

        if (isset($filter_data['company_id'])) {
            $where_company = " AND td.companyId = " . DEFAULT_COMPANY_ID;
        }

        if (isset($filter_data['pickup_date'])) {
            $where_pickup_date = " AND DATE(td.pickupDatetime)=DATE('" . $filter_data['pickup_date'] . "')";
        }

        $query = "select SUM(ttd.totalTripCost) As totalTripCost, round(SUM((SUBSTRING_INDEX(travelHoursMinutes, '.', 1)*60)+(SUBSTRING_INDEX(travelHoursMinutes, '.', -1))) / 60,2) AS totalTripHours, Count(DISTINCT td.driverId) As totalTripDrivers, td.bookingCityId from triptransactiondetails As ttd left join tripdetails As td on ttd.tripId=td.id WHERE td.id!=0 " . $where_city . " " . $where_company . " " . $where_pickup_date . "  Group By td.bookingCityId";

        $result = $this->db->query($query);
        if ($result) {
            return $result->row();
        } else {
            return 0;
        }
    }
}
