<?php
require_once(APPPATH . 'config/promocode_type_enum.php');
class Passenger_Promocode_Details_Model extends MY_Model {

	protected $_table = 'passengerpromocodedetails';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
        
        public function addUpdatePromoCodeDetails($data){
            $promocode_id        = array();
            $city_id             = $data['cityId'];
            $data['updatedDate'] = getCurrentDateTime();
            $data['createdDate'] = getCurrentDateTime();
            $data['createdBy']   = ($this->session->userdata('user_id') !='') ? $this->session->userdata('user_id') : 0;
            $data['updatedBy']   = ($this->session->userdata('user_id') !='') ? $this->session->userdata('user_id') : 0;
            //update
            if($data['id'] > 0){
                foreach($city_id as $city){
                    $data['cityId'] = $city;
                    $this->db->where('id', $data['id']);
                    $status = $this->db->update('passengerpromocodedetails', $data);
                }
                return $status;
            }else{
            //insert    
            $data['id']          = '';
                foreach($city_id as $city){
                    $data['cityId'] = $city;
                    $this->db->insert('passengerpromocodedetails', $data);
                    $promocode_id[] = $this->db->insert_id();
                }
            return $promocode_id;
            }
        }
        /**
         * To get first free ride Valid & available promocode for passenger first trip while trip start.
         * @param string $passenger_id
         * @return boolean
         */
        public function getFreeRidePromoCode($passenger_id=NULL)
        {
        	$promocode=FALSE;
        	if ($passenger_id > 0)
        	{
        		$this->load->model('Trip_Details_Model');
        		$is_first_ride=$this->Trip_Details_Model->getByKeyValueArray(array('passengerId'=>$passenger_id,
        				'tripStatus'=>Trip_Status_Enum::TRIP_COMPLETED
        		),'id');
        		if (count($is_first_ride) <= 0)
        		{
        			$promocode_details=$this->getOneByKeyValueArray(array (
        					'promoStartDate <=' => getCurrentDateTime(),'promoEndDate>='=>getCurrentDateTime(),'isFreeRide'=>Status_Type_Enum::ACTIVE
        			),'id');
        			if ($promocode_details)
        			{
        				
        				$promocode= $promocode_details->promoCode;
        				return $promocode;
        			}
        		}
        	}
        	return $promocode;
        }
}