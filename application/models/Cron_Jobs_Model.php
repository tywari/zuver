<?php

require_once(APPPATH . 'config/device_type_enum.php');
require_once(APPPATH . 'config/driver_type_enum.php');

/**
 * Created by PhpStorm.
 * User: Dragons
 * Date: 6/28/2017
 * Time: 11:43 AM
 */
class Cron_Jobs_Model extends CI_Model
{
    protected $_table = 'driver';//model table_name

    /**
     *  Default Constructor
     */
    function __construct($args = NULL)
    {
        parent::__construct();
        if (is_object($args)) $args = get_object_vars($args);
        if (is_array($args)) {
            foreach ($args AS $key => $value) {
                $this->{$key} = $value;
            }
        }

    }

    public function update_driver_shift_status_cron($status)
    {

        if ($status == 1) {
            $shift_status = 'IN';
            $notification = 1;
        } else {
            $shift_status = 'OUT';
            $notification = 0;
        }
        $sql_query = array(
            'shift_status' => $shift_status,
        );
        $notification_query = array(
            'notification_setting' => $notification,
        );

        $query="UPDATE drivershifthistory SET shiftStatus $shift_status";
        $results=$this->db->query($query);
        return $results;

    }

}