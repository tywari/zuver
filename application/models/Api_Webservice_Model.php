<?php
include(APPPATH . 'libraries/PHPMailer.php');

require_once(APPPATH . 'config/company_type_enum.php');
require_once(APPPATH . 'config/device_type_enum.php');
require_once(APPPATH . 'config/driver_accepted_status_enum.php');
require_once(APPPATH . 'config/driver_available_status_enum.php');
require_once(APPPATH . 'config/driver_request_status_enum.php');
require_once(APPPATH . 'config/driver_shift_status_enum.php');
require_once(APPPATH . 'config/driver_type_enum.php');
require_once(APPPATH . 'config/favourite_type_enum.php');
require_once(APPPATH . 'config/gender_type_enum.php');
require_once(APPPATH . 'config/payment_method_enum.php');
require_once(APPPATH . 'config/payment_mode_enum.php');
require_once(APPPATH . 'config/promocode_type_enum.php');
require_once(APPPATH . 'config/role_type_enum.php');
require_once(APPPATH . 'config/signup_type_enum.php');
require_once(APPPATH . 'config/register_type_enum.php');
require_once(APPPATH . 'config/transaction_type_enum.php');
require_once(APPPATH . 'config/transmission_type_enum.php');
require_once(APPPATH . 'config/trip_status_enum.php');
require_once(APPPATH . 'config/trip_type_enum.php');
require_once(APPPATH . 'config/emergency_status_enum.php');
require_once(APPPATH . 'config/user_type_enum.php');
require_once(APPPATH . 'config/bill_type_enum.php');
require_once(APPPATH . 'config/status_type_enum.php');
require_once(APPPATH . 'config/manipulation_type_enum.php');
require_once(APPPATH . 'config/car_type_enum.php');
require_once(APPPATH . 'config/driver_skill_enum.php');
require_once(APPPATH . 'config/license_type_enum.php');
require_once(APPPATH . 'config/earning_type_enum.php');

class Api_Webservice_Model extends MY_Model
{

    /**
     * Default Constructor
     */
    function __construct($args = NULL)
    {
        parent::__construct();
        if (is_object($args))
            $args = get_object_vars($args);
        if (is_array($args)) {
            foreach ($args as $key => $value) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * To get the trip details based on Company,city & trip type for particular passenger/driver
     *
     * @param unknown $company_id
     * @param string $city_id
     * @return Ambigous <multitype:, multitype:object >
     */

    public function getRateCardDetails($company_id, $city_id = NULL, $is_monthly = NULL)
    {
        $where_city_clause = '';
        if ($city_id) {
            $where_city_clause = " AND rc.cityId=" . $city_id;
        }
        $where_monthly_clause = '';
        if ($is_monthly) {
            $where_monthly_clause = " AND rc.isMonthly='" . Status_Type_Enum::ACTIVE . "'";
        }

        $query = "SELECT rc.id As 'rateCardId',rc.tripType as 'tripType',rc.name As 'rateCardName',rc.dayConvenienceCharge As 'dayConvenienceCharge',
				rc.nightConvenienceCharge As 'nightConvenienceCharge',rc.firstHourCharge As 'firstHourCharge',rc.fifthHourCharge As 'fifthHourCharge',
				rc.ninthHourCharge As 'ninthHourCharge',rc.above12HourCharge As 'above12HourCharge',rc.fixedAmount As 'fixedAmount',
				rc.distanceCostPerKm As 'waitingCharge',rc.timeCostPerHour As 'overtimeChargePerHour',rc.companyId As 'companyId',
				rc.distanceConvenienceCharge As 'distanceConvenienceCharge',rc.timeConvenienceCharge As 'timeConvenienceCharge',
				rc.minDistance As 'minDistance',rc.distanceConvenienceCharge As 'distanceConvenienceCharge',rc.distanceCostPerKm As 'distanceCostPerKm'
				from  ratecarddetails As rc
				where rc.companyId=" . $company_id . " " . $where_city_clause . " " . $where_monthly_clause . " AND rc.status='" . Status_Type_Enum::ACTIVE . "'";
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    /**
     * To send SMS
     * @param unknown $mobile
     * @param unknown $message
     * @return boolean
     */
    public function sendSMS($mobile, $message)
    {


        $query = "SELECT c.* from country as c
		  where c.id=1 AND c.status='" . Status_Type_Enum::ACTIVE . "'";
        $result = $this->db->query($query);
        $country_details = $this->fetchAll($result);
        $to = str_replace('+', '', $country_details[0]->telephoneCode . $mobile);
        // $phone=urlencode($to);
        $msg = urlencode($message);
        $URL = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=$to&msg=$msg&msg_type=TEXT&userid=" . SMS_USERNAME . "&auth_scheme=plain&password=" . SMS_PASSWORD . "&v=1.1&format=text";
        // print_r($URL); exit;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $URL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $Result = curl_exec($curl);
        // print_r($Result);exit;
        curl_close($curl);
        return $Result;

        //return TRUE;
    }

    /**
     * Send Email using SMTP
     * @param unknown $to
     * @param string $subject
     * @param string $message
     * @param string $cc
     * @return boolean
     */
    public function sendEmail($to, $subject = '', $message = '', $cc = NULL, $bcc = NULL, $attachment = NULL)
    {

        /*
          $query = "SELECT s.* from smtpsetting as s
          where s.id=1 AND s.status='" . Status_Type_Enum::ACTIVE."'";
          $result = $this->db->query ( $query );
          $smtp_details = $this->fetchAll ( $result );
          // Email configuration
          $config = Array (
          'protocol' => 'smtp',
          'smtp_host' => $smtp_details [0]->smtpHost,
          'smtp_port' => $smtp_details [0]->smtpPort,
          'smtp_user' => $smtp_details [0]->smtpUsername, // change it to yours
          'smtp_pass' => $smtp_details [0]->smtpPassword, // change it to yours
          // 'smtp_crypto' => $smtp_details [0]->smtpTransportLayerSecurity,
          'mailtype' => 'html',
          'charset' => 'iso-8859-1',
          'wordwrap' => TRUE
          );
          if (is_array ( $to ) && empty ( $to )) {
          $to = implode ( ',', $to );
          }
          if (is_array ( $cc ) && empty ( $cc )) {
          $cc = implode ( ',', $cc );
          }
          $this->load->library ( 'email', $config );
          $this->email->from ( SMTP_EMAIL_ID, SMTP_EMAIL_NAME );
          $this->email->to ( $to );
          $this->email->cc ( $cc );
          $this->email->subject ( $subject );
          $this->email->message ( $message );

          // $data['message'] = "Sorry Unable to send email...";
          if ($this->email->send ()) {
          // $data['message'] = "Mail sent...";
          return TRUE;
          } else {
          return FALSE;
          }


          $config = Array (
          'protocol' => 'smtp',
          'smtp_host' => "smtp.gmail.com",
          'smtp_port' => 465,
          'smtp_user' => "administrator@cordl.in", // change it to yours
          'smtp_pass' => "Iamadmin1@", // change it to yours
          // 'smtp_crypto' => $smtp_details [0]->smtpTransportLayerSecurity,
          'mailtype' => 'html',
          'charset' => 'iso-8859-1',
          'wordwrap' => TRUE
          );
          if (is_array ( $to ) && empty ( $to )) {
          $to = implode ( ',', $to );
          }
          if (is_array ( $cc ) && empty ( $cc )) {
          $cc = implode ( ',', $cc );
          }
          $this->load->library ( 'email', $config );
          $this->email->from ( "administrator@cordl.in", SMTP_EMAIL_NAME );
          $this->email->to ( $to );
          $this->email->cc ( $cc );
          $this->email->subject ( $subject );
          $this->email->message ( $message );

          if ($this->email->send ()) {
          return TRUE;
          } else {
          return FALSE;
          }

        */

        $this->mail = new PHPMailer(true);
        $this->mail->ClearAllRecipients();
        $this->mail->IsSMTP(); // telling the class to use SMTP
        //$this->mail->Mailer = "smtp";
        $this->mail->Encoding = "base64";
        $this->mail->CharSet = "iso-8859-1";                  // CharSet
        $this->mail->SMTPDebug = 0;
        // enables SMTP debug information
        $this->mail->IsHTML(true);
        $this->mail->SMTPSecure = "ssl";                    // sets the prefix to the servier
        $this->mail->Port = 465;
        $this->mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
        $this->mail->Username = SMTP_EMAIL_ID;
        $this->mail->Password = SMTP_EMAIL_PASSWORD;
        $this->mail->SMTPAuth = true;
        $setFrom = SMTP_EMAIL_ID;
        $FromName = SMTP_EMAIL_NAME;
        $this->mail->SetFrom($setFrom, $FromName);
        $to_name = "";
        $this->mail->AddAddress($to, $to_name);
        if (is_array($cc) && !empty ($cc)) {
            foreach ($cc as $cc_email) {
                $this->mail->AddCC($cc_email['email'], $cc_email['name']);
            }
        }
        if (is_array($bcc) && !empty ($bcc)) {
            foreach ($bcc as $bcc_email) {
                $this->mail->AddBCC($bcc_email['email'], $bcc_email['name']);
            }
        }
        //$this->mail->AddCC('sidhanth@zuver.in', 'Sidhanth Mally');
        // $this->mail->AddBCC('rupin@tailoredtech.in', 'Rupin');
        //$this->mail->AddBCC('aakash@tailoredtech.in', 'Akkash');
        if (is_array($attachment) && !empty ($attachment)) {
            $this->mail->AddAttachment($attachment['file_path'], $attachment['file_name']);
        }
        $this->mail->Subject = $subject;
        $this->mail->Body = $message;
        $result = $this->mail->Send();
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
        //return TRUE;
    }

    /**
     * To get trip details list including driver,passenger,trip & transaction
     * @param string $trip_id
     * @param string $passenger_id
     * @return Ambigous <multitype:, multitype:object >
     */

    public function getTripDetails($trip_id = NULL, $passenger_id = NULL)
    {
        if ($trip_id && !$passenger_id) {
            $where = "td.id=" . $trip_id;
        }
        if ($passenger_id && $trip_id) {
            $where = "td.id=" . $trip_id . " AND td.passengerId=" . $passenger_id . " group by td.id";
        } else if ($passenger_id && !$trip_id) {
            $where = "td.passengerId=" . $passenger_id . ' AND DATE(td.createdDate)=' . getCurrentDate() . ' order by td.id DESC';
        }

        $query = "SELECT td.id As 'tripId',td.bookingKey as 'bookingKey',td.pickupLocation As 'pickupLocation',td.pickupLatitude As 'pickupLatitude',
				td.pickupLongitude As 'pickupLongitude',td.dropLocation As 'dropLocation',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',
				td.pickupDatetime As 'pickupDatetime',td.actualPickupDatetime As 'actualPickupDatetime',td.dropDatetime As 'dropDatetime',td.bookingCityId As 'bookingCityId',td.companyId As 'companyId',
				td.tripStatus As 'tripStatus',td.notificationStatus As 'notificationStatus',td.driverAcceptedStatus As 'driverAcceptedStatus',td.tripType As 'tripType',td.paymentMode As 'paymentMode',
				td.transmissionType As 'transmissionType',td.billType As 'billType',td.promoCode As 'promoCode', td.landmark As 'landmark',
				td.passengerId As 'passengerId',td.driverId As 'driverId',td.driverRating As 'driverRating', td.driverComments As 'driverComments', td.mapUrl As 'mapUrl',
				td.passengerRating As 'passengerRating',td.passengerComments As 'passengerComments',td.createdBy As 'createdBy',
				ttd.id As 'transactionId',ttd.travelDistance As 'travelDistance',ttd.travelHoursMinutes As 'travelHoursMinutes',ttd.convenienceCharge As 'convenienceCharge',
				ttd.travelCharge As 'travelCharge',ttd.parkingCharge As 'parkingCharge',ttd.tollCharge As 'tollCharge',ttd.totalTripCost As 'totalTripCost',
				ttd.companyTax As 'companyTax',ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.walletPaymentAmount As 'walletPaymentAmount',ttd.otherCharge As 'otherCharge',
				p.firstName As 'passengerFirstName' ,p.lastName As 'passengerLastName' ,p.mobile As 'passengerMobile' ,p.profileImage As 'passengerProfileImage',
				d.firstName As 'diverFirstName' ,d.lastName As 'diverLastName' ,d.mobile As 'diverMobile' ,d.driverExperience As 'driverExperience',
				d.profileImage As 'driverProfileImage',d.driverLanguagesKnown As 'driverLanguagesKnown',d.currentLatitude As 'currentLatitude',d.currentLongitude As 'currentLongitude',
				dpd.dob As 'driverDob',
				u.firstName As 'userFirstName' ,u.lastName As 'userLastName',
				dsh.availabilityStatus As 'availabilityStatus', dsh.shiftStatus As 'shiftStatus',dsh.inLatitude As 'inLatitude',dsh.inLongitude As 'inLongitude',
				dsh.outLatitude As 'outLatitude',dsh.outLongitude As 'outLongitude'
				from tripdetails as td
				left join passenger as p on td.passengerId=p.id
				left join driver as d on td.driverId=d.id
				left join driverpersonaldetails as dpd on dpd.driverId=td.driverId
				left join drivershifthistory as dsh on dsh.driverId=td.driverId
				left join triptransactiondetails as ttd on ttd.tripId=td.id
				left join userlogincredential as u on td.createdBy=u.id
				where " . $where;
        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }


    /**
     * To check promocode availabilty for particular passenger including in City
     * @param unknown $promocode
     * @param number $passenger_id
     * @param number $city_id
     * @return Ambigous <multitype:, multitype:object >|boolean
     */
    public function checkPromocodeAvailablity($promocode, $passenger_id = 0, $city_id = 0)
    {
        if ($promocode) {
            $where_clause = "ppd.promoCode='" . $promocode . "'";
            if ($city_id > 0) {
                $where_clause = "ppd.promoCode='" . $promocode . "' AND (ppd.passengerId=" . $passenger_id . " OR ppd.passengerId=0) AND ppd.cityId=" . $city_id;
            }
            if ($passenger_id > 0) {
                $where_clause = "ppd.promoCode='" . $promocode . "' AND ppd.passengerId=" . $passenger_id . " AND (ppd.cityId=" . $city_id . " OR ppd.cityId=0)";
            }
            $query = "SELECT ppd.* from passengerpromocodedetails as ppd where " . $where_clause;
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        } else {
            return FALSE;
        }
    }

    /**
     * To get the existing trip which is in completed such as waiting for payment/waiting for PAYTM payment/In-progress to particular driver
     *
     * @param unknown $driver_id
     */
    public function getExistingTripDetails($driver_id)
    {
        if ($driver_id) {
            $query = "SELECT td.id As 'tripId',td.tripStatus As 'tripStatus',td.driverAcceptedStatus As 'driverAcceptedStatus' from tripdetails as td where td.driverId=" . $driver_id . " AND (td.tripStatus='" . Trip_Status_Enum::IN_PROGRESS . "' OR td.tripStatus='" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "' OR td.tripStatus='" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "' OR td.tripStatus='" . Trip_Status_Enum::DRIVER_ARRIVED . "') AND td.tripStatus!='" . Trip_Status_Enum::TRIP_COMPLETED . "' AND td.tripStatus!='" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "' AND td.tripStatus!='" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "'";
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        } else {
            return FALSE;
        }
    }

    /**
     *To get the driver earning details
     * @param unknown $driver_id
     * @return Ambigous <multitype:, multitype:object >|boolean
     */
    public function getDriverEarningDetails($driver_id, $current_date = NULL, $start_date = NULL, $end_date = NULL)
    {
        $where_clause = '';
        if ($current_date) {
            $where_clause = "AND td.actualPickupDatetime  LIKE '" . getCurrentDate() . "%'";
        }
        if ($start_date && $end_date) {
            $where_clause = "AND td.actualPickupDatetime >='" . $start_date . "' AND td.actualPickupDatetime <='" . $end_date . "'";
        }
        if ($driver_id) {
            $query = "SELECT td.id As 'tripId',td.driverId As 'driverId',td.passengerRating As 'driverRating',td.actualPickupDatetime As 'actualPickupDatetime',
					td.dropDatetime As 'dropDatetime',ttd.totalTripCost As 'totalTripCost',ttd.driverEarning As 'driverEarning'
					from tripdetails as td
					left join triptransactiondetails as ttd on ttd.tripId=td.id
					where td.driverId=" . $driver_id . " AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' " . $where_clause;
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        } else {
            return FALSE;
        }
    }

    /**
     * To get pending trip deatils for particular passenger
     * @param unknown $passenger_id
     * @param unknown $pagination
     * @param unknown $start_offset
     * @param unknown $end_limit
     * @return Ambigous <multitype:, multitype:object >|boolean
     */
    public function getPendingTripList($passenger_id, $pagination, $start_offset, $end_limit)
    {
        if ($passenger_id) {
            $limit = '';
            if ($pagination) {
                $limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
            }
            $query = "SELECT td.id As 'tripId',td.bookingKey As 'bookingKey',td.driverId As 'driverId',td.passengerId As 'passengerId',td.pickupDatetime As 'pickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',d.firstName As 'driverFirstName',d.lastName As 'driverLastName',
					TIMEDIFF(td.dropDatetime,td.actualPickupDatetime) As tripDuration
					from tripdetails as td
					left join passenger as p on td.passengerId=p.id
					left join driver as d on td.driverId=d.id
					where td.passengerId=" . $passenger_id . " AND td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "','" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "','" . Trip_Status_Enum::READY_TO_START . "','" . Trip_Status_Enum::TRIP_CONFIRMED . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "') AND td.pickupDatetime >='" . getCurrentDateTime() . "' order by td.pickupDatetime DESC,td.tripStatus DESC " . $limit;
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        } else {
            return FALSE;
        }
    }


    /**
     * Created By Aditya on May 09 2017
     * To get latest booked trip deatils for particular passenger
     * @param unknown $passenger_id
     * @return Ambigous <multitype:, multitype:object >|boolean
     */
    public function getPassengerLatestBookedTrip($passenger_id)
    {
        if ($passenger_id) {

            $query = "
						SELECT
							td.id As 'tripId', td.pickupDatetime As 'pickupDatetime',
							td.pickupLocation As 'pickupLocation', td.dropLocation As 'dropLocation',
							td.paymentMode, td.tripType, td.transmissionType, td.carType, td.bookingKey, td.createdDate,
							td.tripStatus

						FROM
							tripdetails as td

						WHERE
							td.passengerId=" . $passenger_id . "
						AND (
									td.tripStatus <> '" . Trip_Status_Enum::TRIP_COMPLETED . "'
								AND td.tripStatus <> '" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "'
							)
						ORDER BY
							createdDate DESC
						LIMIT
							0, 1
					";
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        } else {
            return FALSE;
        }
    }


    /**
     * To get complete trip details for particular passenger
     * @param unknown $passenger_id
     * @param unknown $pagination
     * @param string $start_offset
     * @param string $end_limit
     * @return Ambigous <multitype:, multitype:object >|boolean
     */
    public function getPastTripList($passenger_id, $pagination, $start_offset = NULL, $end_limit = NULL)
    {
        if ($passenger_id) {
            $limit = '';
            if ($pagination) {
                $limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
            }
            $query = "SELECT td.id As 'tripId',td.bookingKey As 'bookingKey',td.driverId As 'driverId',td.passengerId As 'passengerId',td.actualPickupDatetime As 'actualPickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',d.firstName As 'driverFirstName',d.lastName As 'driverLastName',
					TIMEDIFF(td.dropDatetime,td.actualPickupDatetime) As tripDuration
					from tripdetails as td
					left join passenger as p on td.passengerId=p.id
					left join driver as d on td.driverId=d.id
					left join triptransactiondetails as ttd on ttd.tripId=td.id
					where td.passengerId=" . $passenger_id . " AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' order by td.id DESC " . $limit;
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        } else {
            return FALSE;
        }
    }

    /**
     * To get pending trip deatils for particular driver
     * @param unknown $driver_id
     * @param number $pagination
     * @param unknown $start_offset
     * @param unknown $end_limit
     * @param string $start_date
     * @param string $end_date
     * @return Ambigous <multitype:, multitype:object >|boolean
     */
    public function getDriverPendingTripList($driver_id, $pagination = 1, $start_offset, $end_limit, $start_date = NULL, $end_date = NULL)
    {
        if ($driver_id) {
            $limit = '';
            $date_range = "AND td.pickupDatetime >='" . getCurrentDateTime() . "'";
            if ($start_date && $end_date) {
                $date_range = " AND td.pickupDatetime >='" . date('Y-m-d H:i:s', strtotime($start_date)) . "' AND td.pickupDatetime <='" . date('Y-m-d H:i:s', strtotime($end_date)) . "'";
            }
            if ($pagination) {
                $limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
            }
            $query = "SELECT td.id As 'tripId',td.bookingKey As 'bookingKey',td.driverId As 'driverId',td.passengerId As 'passengerId',td.pickupDatetime As 'pickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',d.firstName As 'driverFirstName',d.lastName As 'driverLastName',
					TIMEDIFF(td.dropDatetime,td.actualPickupDatetime) As tripDuration
					from tripdetails as td
					left join passenger as p on td.passengerId=p.id
					left join driver as d on td.driverId=d.id
					where td.driverId=" . $driver_id . " AND td.tripStatus IN ('" . Trip_Status_Enum::DRIVER_ACCEPTED . "','" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "','" . Trip_Status_Enum::READY_TO_START . "','" . Trip_Status_Enum::TRIP_CONFIRMED . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "') " . $date_range . " order by td.pickupDatetime DESC,td.tripStatus DESC " . $limit;
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        } else {
            return FALSE;
        }
    }

    /**
     * To get pending trip deatils for particular driver
     * @param unknown $driver_id
     * @param number $pagination
     * @param unknown $start_offset
     * @param unknown $end_limit
     * @param string $start_date
     * @param string $end_date
     * @return Ambigous <multitype:, multitype:object >|boolean
     */
    public function getDriverCancelledTripList($driver_id, $pagination = 1, $start_offset, $end_limit, $start_date = NULL, $end_date = NULL)
    {
        if ($driver_id) {
            $limit = '';
            //to get from current date & furture cancelled trip list
            //$date_range = "AND td.pickupDatetime >='" . getCurrentDateTime () . "'";
            //to get all cancelled trip list
            $date_range = "";
            if ($start_date && $end_date) {
                $date_range = " AND td.pickupDatetime >='" . date('Y-m-d H:i:s', strtotime($start_date)) . "' AND td.pickupDatetime <='" . date('Y-m-d H:i:s', strtotime($end_date)) . "'";
            }
            if ($pagination) {
                $limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
            }
            $query = "SELECT td.id As 'tripId',td.bookingKey As 'bookingKey',td.driverId As 'driverId',td.passengerId As 'passengerId',td.pickupDatetime As 'pickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation',td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',d.firstName As 'driverFirstName',d.lastName As 'driverLastName',
					TIMEDIFF(td.dropDatetime,td.actualPickupDatetime) As tripDuration
					from driverrejectedtripdetails as drtd
					left join tripdetails as td on td.id=drtd.tripId
					left join passenger as p on drtd.passengerId=p.id
					left join driver as d on drtd.driverId=d.id
					where drtd.driverId=" . $driver_id . " " . $date_range . " order by td.pickupDatetime DESC,td.tripStatus DESC " . $limit;
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        } else {
            return FALSE;
        }
    }

    /**
     * To get complete trip details for particular driver
     * @param unknown $driver_id
     * @param number $pagination
     * @param string $start_offset
     * @param string $end_limit
     * @param string $start_date
     * @param string $end_date
     * @return Ambigous <multitype:, multitype:object >|boolean
     */
    public function getDriverPastTripList($driver_id, $pagination = 1, $start_offset = NULL, $end_limit = NULL, $start_date = NULL, $end_date = NULL)
    {
        if ($driver_id) {
            $limit = '';
            $date_range = "";
            if ($start_date && $end_date) {
                $date_range = " AND td.pickupDatetime >='" . date('Y-m-d H:i:s', strtotime($start_date)) . "' AND td.pickupDatetime <='" . date('Y-m-d H:i:s', strtotime($end_date)) . "'";
            }
            if ($pagination) {
                $limit = ' LIMIT ' . $start_offset . ',' . $end_limit;
            }
            $query = "SELECT td.id As 'tripId',td.bookingKey As 'bookingKey',td.driverId As 'driverId',td.passengerId As 'passengerId',
					td.actualPickupDatetime As 'actualPickupDatetime',td.pickupLatitude As 'pickupLatitude',td.pickupLongitude As 'pickupLongitude',
					td.dropDatetime As 'dropDatetime',td.dropLatitude As 'dropLatitude',td.dropLongitude As 'dropLongitude',td.pickupLocation As 'pickupLocation', td.mapUrl AS 'mapUrl',
					td.dropLocation As 'dropLocation',td.tripStatus As 'tripStatus',td.tripType As 'tripType',td.paymentMode As 'paymentMode',
					p.firstName As 'passengerFirstName',p.lastName As 'passengerLastName',p.profileImage As 'passengerProfileImage',
					d.firstName As 'driverFirstName',d.lastName As 'driverLastName',d.profileImage As 'driverProfileImage',
					TIMEDIFF(td.dropDatetime,td.actualPickupDatetime) As tripDuration,
					ttd.travelDistance As 'travelDistance',ttd.convenienceCharge As 'convenienceCharge',ttd.travelCharge As 'travelCharge',
					ttd.parkingCharge As 'parkingCharge',ttd.tollCharge As 'tollCharge',ttd.companyTax As 'companyTax',ttd.totalTripCost As 'totalTripCost',
					ttd.promoDiscountAmount As 'promoDiscountAmount',ttd.walletPaymentAmount As 'walletPaymentAmount'
					from tripdetails as td
					left join passenger as p on td.passengerId=p.id
					left join driver as d on td.driverId=d.id
					left join triptransactiondetails as ttd on ttd.tripId=td.id
					where td.driverId=" . $driver_id . " AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' " . $date_range . " order by td.id DESC " . $limit;
            $result = $this->db->query($query);
            return $this->fetchAll($result);
        } else {
            return FALSE;
        }
    }

    public function getDriverAverageRating($driver_id)
    {
        $query = "SELECT AVG(td.passengerRating) As 'driverRating'
				  FROM tripdetails AS td
				  where td.driverId=" . $driver_id . " AND td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "'";
        $result = $this->db->query($query);
        return $this->fetchAll($result);


    }

    public function getNearestDriverList($latitude, $longitude, $city_id)
    {
        $query = "SELECT d.id As 'driverId',d.currentLatitude As 'currentLatitude',d.currentLongitude As 'currentLongitude',d.firstName As 'firstName',d.lastName As 'lastName',d.mobile As 'mobile',
		111.045 * DEGREES(ACOS(COS(RADIANS($latitude))
		* COS(RADIANS(d.currentLatitude))
		* COS(RADIANS(d.currentLongitude) - RADIANS($longitude))
		+ SIN(RADIANS($latitude))
		* SIN(RADIANS(d.currentLatitude))))
		As 'distance'
				  FROM driver As d
				  left join (SELECT * From drivershifthistory where id IN (SELECT max(id) From drivershifthistory GROUP BY driverId)) as dsh on dsh.driverId=d.id
				  where d.loginStatus='" . Status_Type_Enum::ACTIVE . "' AND d.status='" . Status_Type_Enum::ACTIVE . "'
				  AND d.cityId=" . $city_id . " AND dsh.availabilityStatus='" . Driver_Available_Status_Enum::FREE . "' AND dsh.shiftStatus='" . Driver_Shift_Status_Enum::SHIFTIN . "'  AND d.isDeleted='" . Status_Type_Enum::INACTIVE . "' GROUP BY d.id ORDER BY d.id DESC LIMIT 0,20";
        $result = $this->db->query($query);
        return $this->fetchAll($result);

        /*
         * Here latitude = 37 & longitude = -122. So you just pass your own
         *
         * SELECT d.id As 'driverId',d.currentLatitude As 'currentLatitude',d.currentLongitude As 'currentLongitude', ( 3959 * acos( cos( radians(".$latitude.") ) * cos( radians( d.currentLatitude ) ) *
         * cos( radians( d.currentLongitude) - radians(".$longitude.") ) + sin( radians(".$latitude.") ) *
         * sin( radians( d.currentLatitude) ) ) ) As 'distance_in_km' FROM driver As d left join drivershifthistory as dsh on dsh.driverId=d.id
         * where d.loginStatus='".Status_Type_Enum::ACTIVE."' AND d.status='".Status_Type_Enum::ACTIVE."'
         * AND d.cityId=".$city_id." AND dsh.availabilityStatus='".Driver_Available_Status_Enum::FREE."' AND dsh.shiftStatus='".Driver_Shift_Status_Enum::SHIFTIN."' AND d.isDeleted='".Status_Type_Enum::INACTIVE."'
         * HAVING distance < 25 GROUP BY d.id ORDER BY distance ASC,dsh.id DESC LIMIT 0 , 20;
         */
    }

    public function getBillDetails($transaction_id)
    {
        $query = "SELECT ttd.tripId As tripId, CONCAT(p.firstName ,' ',p.lastName) As 'passengerFullName',p.email As passengerEmail,
				 CONCAT(d.firstName ,' ',d.lastName) As 'driverFullName',ttd.invoiceNo As 'invoiceNo',ttd.createdDate As 'invoiceDatetime',
				 td.actualPickupDatetime As 'actualPickupDatetime',td.dropDatetime As 'dropDatetime',td.createdDate As 'bookedDatetime',td.paymentMode As 'paymentMode',
				 dat.description As 'tripType',ttd.companyTax As 'companyTax',ttd.totalTripCost As 'totalTripCost', ttd.travelDistance As 'travelDistance',ttd.convenienceCharge As 'convenienceCharge',
				ttd.travelCharge As 'travelCharge', ttd.parkingCharge As 'parkingCharge', ttd.tollCharge As 'tollCharge',
				ttd.promoDiscountAmount As 'promoDiscountAmount', ttd.otherDiscountAmount As 'otherDiscountAmount',ttd.walletPaymentAmount As 'walletPaymentAmount',
				ttd.travelHoursMinutes As 'travelHoursMinutes',ttd.otherCharge As 'otherCharge'
				from triptransactiondetails As ttd
				left join tripdetails As td on ttd.tripId=td.id
				left join passenger As p on td.passengerId=p.id
				left join driver As d on td.driverId=d.id
				left join dataattributes As dat on dat.enumName = td.tripType AND dat.tableName='TRIP_TYPE'
				where ttd.id=" . $transaction_id . " AND (td.tripStatus='" . Trip_Status_Enum::TRIP_COMPLETED . "' OR td.tripStatus='" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "')";

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }

    /*
     * Zoomcar Api to update status of trip
     * Api is being us for driver assign and
     * also for the cancellation purpose
     */

    public function zoomCarTripResponse($trip_id, $booking_key, $driver_id = NULL, $is_trip_confirmed = NULL,$cancel_msg = NULL)
    {
        $access_token = NULL;
        $api_details = $this->getPartnersApiDetails();
        $auth_key = base64_encode($booking_key . '|' . $api_details->secretKey);
        $access_token = $api_details->accessToken;

        $driver_name = '';
        $driver_code = '';
        $driver_mobile = '';
        $driver_license = '';
        $post_data = array();

        if ($driver_id) {
            $query = "SELECT d.*,dp.drivingLicenseNo AS 'license' from driver as d left join driverpersonaldetails 
                  As dp on dp.driverId=d.id WHERE d.id=" . $driver_id;
            $result = $this->db->query($query);
            $driver_details = $this->fetchAll($result);
            $driver_name = $driver_details[0]->firstName . ' ' . $driver_details[0]->lastName;
            $driver_code = $driver_details[0]->driverCode;
            $driver_mobile = $driver_details[0]->mobile;
            $driver_license = $driver_details[0]->license;
        }



        if ($is_trip_confirmed) {

            $post_data = array("data" => array("booking_status" => "driver_allotted", "booking_key" => $booking_key, "reference_key" => $trip_id, "driver_details" => array("rating" => "", "name" => $driver_name, "driver_key" => $driver_code, "license" => $driver_license, "phone_number" => $driver_mobile)));

        }else if($cancel_msg) {

            $post_data = array("data" => array("booking_status" => "cancelled", "booking_key" => $booking_key, "reference_key" => $trip_id));
        }
        else {

//		    echo "reached here";
            $post_data = array("data" => array("booking_status" => "cancelled", "booking_key" => $booking_key, "reference_key" => $trip_id));
        }
        $post_data = json_encode($post_data);

        $url = "api.zoomcar.com/public/v1/hd_vendors/update_driver";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Token token=' . $access_token));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        curl_close($ch);
        $this->db->insert('zoomcar_api_log', array("url" => $url, "token" => $access_token, "post_data" => $post_data, "response" => json_encode($response)));
        return $response;
    }

    /*
     * Zoomcar Api to create checklist and
     * it is being called on driver arrive
     */

    public function zoomCarGetChecklist($trip_id, $booking_key)
    {
        $access_token = NULL;
        $api_details = $this->getPartnersApiDetails();
        $auth_key = base64_encode($booking_key . '|' . $api_details->secretKey);
        $access_token = $api_details->accessToken;
        $trip_details = $this->getTripDetails($trip_id);

        $post_data = array();
        //$post_data=array("auth_key"=>$auth_key,"booking_key"=>$booking_key,"vendor_reference_key"=>$trip_id,"trip_info"=>array("app_type"=>"driver","lat"=>$trip_details[0]->pickupLatitude,"lng"=>$trip_details[0]->pickupLongitude));
        $post_data = array("auth_key" => $auth_key, "booking_key" => $booking_key, "vendor_reference_key" => $trip_id, "trip_info" => array("app_type" => "admin", "lat" => "", "lng" => ""));

        $post_data = json_encode($post_data);

        $url = "api.zoomcar.com/public/v1/hd_vendors/checklist_request";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Token token=' . $access_token));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        curl_close($ch);
        $this->db->insert('zoomcar_api_log', array("url" => $url, "token" => $access_token, "post_data" => $post_data, "response" => json_encode($response)));

        return $response;
    }

    /*
     * Zoomcar Api to update status of trip
     * Api is being us for start and end trip
     */

    public function zoomCarUpdateTripStatus($trip_id, $booking_key, $status)
    {
        $access_token = NULL;
        $api_details = $this->getPartnersApiDetails();
        $access_token = $api_details->accessToken;

        $post_data = array();
        $post_data = array("booking_key" => $booking_key, "vendor_reference_key" => $trip_id, "status" => $status);

        $post_data = json_encode($post_data);

        $url = "api.zoomcar.com/public/v1/hd_vendors/driver_status";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Token token=' . $access_token));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        curl_close($ch);
        $this->db->insert('zoomcar_api_log', array("url" => $url, "token" => $access_token, "post_data" => $post_data, "response" => json_encode($response)));

        return $response;
    }

    /*
     * Get Partners Api Details
     */
    public function getPartnersApiDetails()
    {
        $query = "SELECT cpad.* from corporatepartnersapidetails as cpad
		  where cpad.companyId=" . ZOOMCAR_COMPANYID;
        $result = $this->db->query($query);
        $api_details = $this->fetchAll($result);
        $access_token = $api_details[0]->accessToken;

        /** to check the Zoomcar Api access token genrated hour difference */

        $last_api_date = strtotime($api_details[0]->updatedDate);
        $current_date = strtotime(getCurrentDateTime());
        $hour = abs($current_date - $last_api_date) / (60 * 60);
        if ($hour > 24) {
            $token_response = $this->generate_zoomcar_token();
            if ($token_response['status'] == 'success') {
                $this->getPartnersApiDetails();
            }
        }
        return $api_details[0];
    }

    /*
     * Generate Zoomcar Access Token
     */
    public function generate_zoomcar_token()
    {
        //$api_details=$this->getPartnersApiDetails();
        $query = "SELECT cpad.* from corporatepartnersapidetails as cpad
		  where cpad.companyId=" . ZOOMCAR_COMPANYID;
        $result = $this->db->query($query);
        $api_details_result = $this->fetchAll($result);
        $api_details = $api_details_result[0];

        $url = "api.zoomcar.com/public/regenerate_access_token?partner_key=" . $api_details->partnerKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Token token=' . $api_details->accessToken));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, true);

        $this->db->insert('zoomcar_api_log', array("url" => $url, "token" => $api_details->accessToken, "response" => json_encode($response)));
        if ($response) {
            if ($response['status'] == 'success') {
                $access_token = $response['data']['access_token'];
                //$access_token=md5(uniqid(rand(), true));
                $query = "update corporatepartnersapidetails SET accessToken='" . $access_token . "', updatedDate='" . getCurrentDateTime() . "' WHERE companyId=" . ZOOMCAR_COMPANYID;
                $result = $this->db->query($query);
                $file = 'zoomcar_token_log.txt';
                file_put_contents($file, $access_token . ", " . getCurrentDateTime() . "\n", FILE_APPEND | LOCK_EX);
                $this->db->insert('zoomcar_token_log', array("token" => $access_token));
                //echo $query;
            }
        }
        return $response;
    }

    /*
     * Get Driver Rate Card Details
     */

    public function getDriverRateCardDetails()
    {
        $query = " SELECT * FROM driversratecarddetails ORDER BY id ASC ";

        $result = $this->db->query($query);
        return $this->fetchAll($result);
    }
}
