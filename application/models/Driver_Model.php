<?php
require_once(APPPATH.'config/device_type_enum.php');
require_once(APPPATH.'config/driver_type_enum.php');
class Driver_Model extends MY_Model {

	protected $_table = 'driver';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}

	//temp api
	//Update Driver Shift Status
	public function update_driver_shift_status($id,$status,$stat = null)
	{
		if($status == 1)
		{
			$shift_status = 'IN';
			$notification = 1;
		}
		else
		{
			$shift_status = 'OUT';
			$notification = 0;
		}
		$sql_query = array(
				'shift_status' => $shift_status,
		);
		$notification_query = array(
				'notification_setting' => $notification,
		);
		/*DB::update(DRIVER)->set($sql_query)
		->where('driver_id', '=' ,$id)
		->execute();
		DB::update(PEOPLE)->set($notification_query)
		->where('id', '=' ,$id)
		->execute();*/

		$this->db->where('driver_id', $id);
		$this->db->update(DRIVER, $sql_query);

		$this->db->where('id', $id);
		$this->db->update(PEOPLE, $notification_query);

	}

	//Get Driver Current Status if he is break,Avtive,Free
	public function get_driver_current_status($id)
	{
		/*$result = DB::select('*')->from(DRIVER)
		->where(DRIVER.'.driver_id','=',$id)
		->order_by('id', 'ASC')
		->as_object()
		->execute();
		//print_r($result);*/
		$query="select * from ".DRIVER." where driver_id=".$id." order by id ASC";
		$results=$this->db->query($query);
		$result=$results->result();
		return $result;

	}

	/***Aditya Code Start***/
	/*** Get Driver current On Duty / Off Duty Status ***/
	public function currentShiftStatus($driverID)
	{
		/*$this->db->select('shiftStatus');
		$this->db->from('drivershifthistory');
		$this->db->where('driverId', $driverID);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(0, 1);

		$query = $this->db->get();
		$result = $query->result_array();
		return $result;*/

		//$sql = 'SELECT shiftStatus ';
		$query="SELECT shiftStatus FROM drivershifthistory WHERE driverId=".$driverID." ORDER BY id DESC LIMIT 0,1";
		$results=$this->db->query($query);
		$result=$results->result_array();
		return $result;
	}

	// This function is being used during registration of Driver to verify whether the Mobile value entered is unique or not
	// created by Aditya on May 18 2017
	public function checkIfMobileAlreadyPresent($inMobile, $inDriverID = 0) {
		$query="SELECT COUNT(mobile) AS cMobile FROM driver WHERE mobile='".$inMobile."' ";
		//$results=$this->db->query($query);
		//$result=$results->result_array();
		//$shiftStatus = $result[0]['cMobile'];
		if ($inDriverID > 0) {
			$query="SELECT COUNT(mobile) AS cMobile FROM driver WHERE mobile='".$inMobile."' AND id <> '".$inDriverID."' ";
		}
		$cMobile = $this->db->query($query)->row()->cMobile;
		return $cMobile;
	}

	// This function is being used during registration of Driver to verify whether the Email value if entered is unique or not
	// created by Aditya on May 18 2017
	public function checkIfEmailAlreadyPresent($inEmail, $inDriverID = 0) {
		$query="SELECT COUNT(email) AS cEmail FROM driver WHERE email='".$inEmail."' ";
		if ($inDriverID > 0) {
			$query="SELECT COUNT(email) AS cEmail FROM driver WHERE email='".$inEmail."' AND id <> '".$inDriverID."' ";
		}
		$cEmail = $this->db->query($query)->row()->cEmail;
		return $cEmail;
	}

	// third party code
	// integrated by Aditya on May 22 2017
	// this is to help find distance between 2 Geo Points
	function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K') {

		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}

	// created by Aditya on May 22 2017
	// This function simply provides which Test Center is closest to a Drivers Home Location
	public function getDriverTestCenter($inHomeLatitude, $inHomeLongitude) {
		$minDetails['distance'] = 0;
		$minDetails['id'] = '';
		$minDetails['name'] = '';
		$minDetails['address'] = '';
		$minDetails['lat'] = '';
		$minDetails['lon'] = '';


		$counter = 0;
		$query = "
					SELECT
						*
					FROM
						sample_test_center
					ORDER BY
						id ASC
				;";
		$result = $this->db->query($query);
		foreach ($result->result() as $row) {
			$dist = $this->distance($inHomeLatitude, $inHomeLongitude, $row->latitude, $row->longitude, 'K');
			//echo 'Name: '.$row->display_name.', Dis: '.$dist;
			//echo '<br/>';
			if ($counter == 0) {
				// Meaning first iteration
				$minDetails['distance'] = $dist;
				$minDetails['id'] = $row->id;
				$minDetails['name'] = $row->display_name;
				$minDetails['address'] = $row->full_address;
				$minDetails['lat'] = $row->latitude;
				$minDetails['lon'] = $row->longitude;
			} else {
				// Now check if previous Dist Balance is greater than new Dist Balance
				// If yes then use new Dist Balance
				if ($minDetails['distance'] > $dist) {
					$minDetails['distance'] = $dist;
					$minDetails['id'] = $row->id;
					$minDetails['name'] = $row->display_name;
					$minDetails['address'] = $row->full_address;
					$minDetails['lat'] = $row->latitude;
					$minDetails['lon'] = $row->longitude;
				}
			}
			$counter++;
		}
		//print_r($minDetails);
		//die();
		return $minDetails;

	}

	// created by Aditya on May 23 2017
	public function getDriverJoiningDate($inDriverID) {

		$this->db->select('doj');
		$this->db->where('id', $inDriverID);
		$query = $this->db->get('driver');

		//$row = $this->db->query($query)->row();
		$row = $query->row();
		return $row->doj;
	}

	// created by Aditya on May 26 2017
	// this function is to set all Available OnDuty
	public function getAllAvailableOnDutyDriverList() {
		// get a list of all the drivers who are currently logged in and verified and not blocked
		// now check who all drivers are currently NOT doing a trip i.e. FREE
		$query="
				SELECT
					d.id, d.firstName, d.lastName, d.email, d.mobile,
					(
						SELECT
							availabilityStatus
						FROM
							drivershifthistory
						WHERE
							driverId = d.id
						ORDER BY
							id DESC
						LIMIT
							0,1
					) AS availabilityStatus,
					(
						SELECT
							shiftStatus
						FROM
							drivershifthistory
						WHERE
							driverId = d.id
						ORDER BY
							id DESC
						LIMIT
							0,1
					) AS shiftStatus,
					(
						SELECT
							createdDate
						FROM
							drivershifthistory
						WHERE
							driverId = d.id
						ORDER BY
							id DESC
						LIMIT
							0,1
					) AS shiftCreatedDate
				FROM
					driver d
				WHERE
					d.isVerified = 'Y'
				AND d.loginStatus = 'Y'
				AND
					(
						SELECT
							shiftStatus
						FROM
							drivershifthistory
						WHERE
							driverId = d.id
						ORDER BY
							id DESC
						LIMIT
							0,1
					) = 'IN'
				AND
					(SELECT
						availabilityStatus
					FROM
						drivershifthistory
					WHERE
						driverId = d.id
					ORDER BY
						id DESC
					LIMIT
						0,1) = 'F'
				";
		$results=$this->db->query($query);
		$result=$results->result_array();
		//print_r($result);
		return $result;
	}
	/***Aditya Code End***/

	public function get_driver_earnings($driver_id)
	{
		$query = "select *, sum(fare) as total_amount from ".PASSENGERS_LOG." join
		".TRANS." on ".PASSENGERS_LOG.".passengers_log_id = ".TRANS.".passengers_log_id
		where ".PASSENGERS_LOG.".driver_id='$driver_id' and ".PASSENGERS_LOG.".travel_status='1'";
		$results=$this->db->query($query);
		$result=$results->result();

		return $result;
	}

	public function get_driver_total_trips($driver_id, $driver_type, $status, $filter_data=array())
	{
		$where_city = "";
        $where_trip_completed_date="";
        $where_trip_completed_month='';
        $where_driver_id='';
        if (isset($filter_data['city_id'])) {
            $where_city = " AND td.bookingCityId=" . $filter_data['city_id'];
        }

        if (isset($filter_data['trip_completed_date']))
        {
            $where_trip_completed_date = " AND DATE(td.dropDatetime)=DATE('".$filter_data['trip_completed_date']."')";
        }

        if(isset($filter_data['trip_completed_month']))
        {
            $where_trip_completed_month = " AND MONTH(td.dropDatetime)=MONTH('".$filter_data['trip_completed_month']."')";
        }

        if($driver_id)
        {
            $where_driver_id = " AND td.driverId='".$driver_id."'";
        }



        //totalUnassignedTrip
        if($status=='UnassignedTrips'){
        $select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "') THEN 1 ELSE 0 END)";
    	}
        elseif ($status=='UpcomingTrips')
        { //totalUpcomingTrip
        $select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_CONFIRMED . "','" . Trip_Status_Enum::READY_TO_START . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "')  THEN 1 ELSE 0 END) ";
    	}
        elseif ($status=='OngoingTrips')
        { //totalOngoingTrip
        $select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "')  THEN 1 ELSE 0 END)";
    	}
        elseif ($status=='CancelledTrips')
        { //totalCancelledTrip
        $select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "','" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "')  THEN 1 ELSE 0 END)";
        }
        elseif ($status=='CompletedTrips')
        { //totalCompletedTrip
        	if($driver_type=='TBD')
        	{
        		$select=" SUM(CASE WHEN (select count(id) from subtripdetails where masterTripId=td.id)>0 then 0 ELSE (CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_COMPLETED . "')  THEN 1 ELSE 0 END) END )  ";
        	}
        	else
        	{
        		$select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_COMPLETED . "')  THEN 1 ELSE 0 END)  ";
        	}
        }
        else
        { //Total Trips
        	$select=" COUNT(td.id)";
    	}
        $query="SELECT ".$select." As 'totalTrip' FROM tripdetails As td
        where td.id !=0 ".$where_city. " ".$where_trip_completed_date. " ".$where_trip_completed_month. " ".$where_driver_id;

        $result = $this->db->query($query);

        if($result->row()->totalTrip)
            { return $result->row()->totalTrip; }
        else
            { return '0'; }
	}

	public function get_driver_total_Sub_trips($driver_id, $status, $filter_data=array())
	{
		$where_city = "";
        $where_trip_completed_date="";
        $where_trip_completed_month='';
        $where_driver_id='';

        if (isset($filter_data['trip_completed_date']))
        {
            $where_trip_completed_date = " AND DATE(std.dropDatetime)=DATE('".$filter_data['trip_completed_date']."')";
        }

        if(isset($filter_data['trip_completed_month']))
        {
            $where_trip_completed_month = " AND MONTH(std.dropDatetime)=MONTH('".$filter_data['trip_completed_month']."')";
        }

        if($driver_id)
        {
            $where_driver_id = " AND td.driverId='".$driver_id."'";
        }



        //totalUnassignedTrip
        if($status=='UnassignedTrips')
        $select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::BOOKED . "') THEN 1 ELSE 0 END)";

        elseif ($status=='UpcomingTrips')
        //totalUpcomingTrip
        $select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_CONFIRMED . "','" . Trip_Status_Enum::READY_TO_START . "','" . Trip_Status_Enum::DRIVER_ACCEPTED . "')  THEN 1 ELSE 0 END) ";

        elseif ($status=='OngoingTrips')
        //totalOngoingTrip
        $select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::IN_PROGRESS . "','" . Trip_Status_Enum::WAITING_FOR_PAYMENT . "','" . Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT . "','" . Trip_Status_Enum::DRIVER_ARRIVED . "')  THEN 1 ELSE 0 END)";

        elseif ($status=='CancelledTrips')
        //totalCancelledTrip
        $select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::CANCELLED_BY_DRIVER . "','" . Trip_Status_Enum::CANCELLED_BY_PASSENGER . "')  THEN 1 ELSE 0 END)";

        elseif ($status=='CompletedTrips')
        //totalCompletedTrip
        $select=" SUM(CASE WHEN td.tripStatus IN ('" . Trip_Status_Enum::TRIP_COMPLETED . "')  THEN 1 ELSE 0 END)";

        else
        //Total Trips
        $select=" COUNT(td.id)";

        $query="SELECT ".$select." As 'totalTrip' FROM subtripdetails std LEFT JOIN tripdetails td  ON std.masterTripId=td.id
        where td.id !=0 ".$where_city. " ".$where_trip_completed_date. " ".$where_trip_completed_month. " ".$where_driver_id;

      //  echo $query;
        $result = $this->db->query($query);

        if($result->row()->totalTrip)
            { return $result->row()->totalTrip; }
        else
            { return '0'; }
	}

	public function get_driver_rejected_trips($driver_id, $start_date='', $end_date='', $time_diff=0)
	{
		$query="SELECT DRT.*, TIMEDIFF(DRT.`createdDate`, TD.`pickupDatetime`) AS rejecttimedifference FROM driverrejectedtripdetails DRT INNER JOIN tripdetails TD ON DRT.tripId=TD.id WHERE DRT.`driverId`='".$driver_id."'";
		if($start_date)
		{
			$query.=" AND DRT.createdDate>='".$start_date."'";
		}
		if($end_date)
		{
			$query.=" AND DRT.createdDate<='".$end_date."'";
		}
		if($time_diff)
		{
			$query.=" AND TIMEDIFF(DRT.`createdDate`, TD.`pickupDatetime`) ".$time_diff;
		}
		$result = $this->db->query($query);
        return $result->result();
	}

	public function get_driver_trip_hours_distance($driver_id, $filter_data=array())
	{
		$where_trip_completed_date="";
		$where_trip_completed_month="";
        $where_driver_id='';

        if (isset($filter_data['trip_completed_date']))
        {
            $where_trip_completed_date = " AND DATE(td.dropDatetime)=DATE('".$filter_data['trip_completed_date']."')";
        }
         if(isset($filter_data['trip_completed_month']))
        {
            $where_trip_completed_month = " AND MONTH(td.dropDatetime)=MONTH('".$filter_data['trip_completed_month']."')";
        }


        if($driver_id)
        {
            $where_driver_id = " AND td.driverId='".$driver_id."'";
        }

        $query="SELECT round(SUM((SUBSTRING_INDEX(travelHoursMinutes, '.', 1)*60)+(SUBSTRING_INDEX(travelHoursMinutes, '.', -1))) / 60,2) AS totalTripHours, SUM(travelDistance) AS totalTripDistance FROM triptransactiondetails As ttd left join tripdetails As td on ttd.tripId=td.id WHERE td.id!=0 AND td.tripStatus IN ('" . Trip_Status_Enum::TRIP_COMPLETED . "') AND (select count(id) from subtripdetails where masterTripId=td.id)=0 ".$where_trip_completed_date. " ".$where_trip_completed_month. " ".$where_driver_id;

      //  echo $query;
        $result = $this->db->query($query);

       return $result->row();
	}

	public function get_driver_sub_trip_hours_distance($driver_id, $filter_data=array())
	{
		$where_trip_completed_date="";
		$where_trip_completed_month="";
        $where_driver_id='';

        if (isset($filter_data['trip_completed_date']))
        {
            $where_trip_completed_date = " AND DATE(std.dropDatetime)=DATE('".$filter_data['trip_completed_date']."')";
        }
         if(isset($filter_data['trip_completed_month']))
        {
            $where_trip_completed_month = " AND MONTH(std.dropDatetime)=MONTH('".$filter_data['trip_completed_month']."')";
        }


        if($driver_id)
        {
            $where_driver_id = " AND td.driverId='".$driver_id."'";
        }

        $query="SELECT round(SUM((HOUR(TIMEDIFF(std.dropDatetime, std.pickupDatetime))*60)+(MINUTE(TIMEDIFF(std.dropDatetime, std.pickupDatetime)))) / 60,2) AS totalTripHours FROM subtripdetails As std left join tripdetails As td on std.masterTripId=td.id WHERE td.id!=0 AND td.tripStatus IN ('" . Trip_Status_Enum::TRIP_COMPLETED . "') ".$where_trip_completed_date. " ".$where_trip_completed_month. " ".$where_driver_id;

      //  echo $query;
        $result = $this->db->query($query);

       return $result->row();
	}
	public function get_driver_logged_hours($driver_id, $filter_date)
	{
		$query="SELECT A.id, A.shiftStatus, B.id, B.shiftStatus, A.`driverId`, A.`createdDate`, B.createdDate, SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(B.`createdDate`, A.`createdDate`)))) AS totaltimedifference FROM drivershifthistory A CROSS JOIN drivershifthistory B WHERE B.`createdDate` IN (SELECT MIN(C.`createdDate`) FROM drivershifthistory C WHERE C.`createdDate` > A.`createdDate` AND C.`driverId`='".$driver_id."') AND (A.`driverId`='".$driver_id."' AND B.`driverId`='".$driver_id."') AND (DATE(A.`createdDate`)=DATE(B.`createdDate`)) AND ((A.`shiftStatus`='IN' AND B.`shiftStatus`='IN') OR (A.`shiftStatus`='IN' AND B.`shiftStatus`='OUT')) AND DATE(A.`createdDate`)=DATE('".$filter_date."') GROUP BY DATE(A.`createdDate`) ORDER BY `A`.`createdDate` ASC";
		$result = $this->db->query($query);
        if($result->row())
            { return $result->row()->totaltimedifference; }
        else
            { return '00:00:00'; }

	}

	// Need to make changes to this code
	// It needs to find and return following information
	// Aditya updated this function, on Wed May 3 2017
	public function get_driver_sessions($driver_id)
	{
		//Query by jyoti before merge
		//$query="SELECT A.id,A.`createdDate` AS createdDate1, A.shiftStatus AS shiftStatus1, A.availabilityStatus, B.id, B.createdDate AS createdDate2, B.shiftStatus AS shiftStatus2, A.`driverId`, TIMEDIFF(B.`createdDate`, A.`createdDate`) AS timedifference FROM drivershifthistory A CROSS JOIN drivershifthistory B WHERE B.`id` IN (SELECT MIN(C.`id`) FROM drivershifthistory C WHERE C.`createdDate` > A.`createdDate` AND C.`driverId`='".$driver_id."') AND (A.`driverId`='".$driver_id."' AND B.`driverId`='".$driver_id."') AND (A.`shiftStatus`='IN' AND B.`shiftStatus`='OUT') AND (A.createdDate BETWEEN DATE_ADD( NOW(), INTERVAL -3 MONTH) AND NOW()) ORDER BY `A`.`createdDate` ASC";

		/* ORIGINAL CODE
		$query="SELECT A.id,A.`createdDate` AS createdDate1, A.shiftStatus AS shiftStatus1, A.availabilityStatus, B.id, B.createdDate AS createdDate2, B.shiftStatus AS shiftStatus2, A.`driverId`, TIMEDIFF(B.`createdDate`, A.`createdDate`) AS timedifference FROM drivershifthistory A CROSS JOIN drivershifthistory B WHERE B.`id` IN (SELECT MIN(C.`id`) FROM drivershifthistory C WHERE C.`createdDate` > A.`createdDate` AND C.`driverId`='".$driver_id."') AND (A.`driverId`='".$driver_id."' AND B.`driverId`='".$driver_id."') AND (A.createdDate BETWEEN DATE_ADD( NOW(), INTERVAL -3 MONTH) AND NOW()) ORDER BY `A`.`createdDate` ASC";
		$result = $this->db->query($query);
        return $result->result();*/

        // Code written by Aditya on Wed May 03 2017
        // AND MONTH(createdDate) = MONTH(CURRENT_DATE())
        $query = '
        	SELECT
        		shiftStatus, createdDate, updatedDate
        	FROM
        		drivershifthistory
        	WHERE
        		driverId = '.$driver_id.'

        	ORDER BY
        		id DESC
        ';
        $result = $this->db->query($query);

        return $result->result();
    }

	public function get_logged_drivers_by_date($logged_date='', $driver_type='')
	{
		$query="SELECT p1.*,d.driverType,concat(d.firstName,' ',d.lastName)AS driver_name,d.mobile AS driver_phone FROM drivershifthistory p1 LEFT JOIN driver d ON p1.driverId=d.id INNER JOIN ( SELECT max(createdDate) MaxDate, driverId FROM drivershifthistory WHERE createdDate <= '".$logged_date."' GROUP BY driverId ) p2 ON p1.driverId = p2.driverId AND p1.createdDate = p2.MaxDate ";
		if($driver_type)
		{
			$query.=" AND d.driverType='".$driver_type."' ";
		}
		$query.=" WHERE p1.shiftStatus='IN' ORDER BY `p1`.`driverId` DESC";
		$results=$this->db->query($query);
		$result=$results->result();
		return $result;
	}

	public function get_drivers_logout($driver_id, $start_date='', $end_date='')
	{
		$query="SELECT p1.* FROM drivershifthistory p1 WHERE p1.shiftStatus='OUT' AND p1.driverId='".$driver_id."' ";
		if($start_date && $end_date)
		{
			$query.=" AND p1.createdDate >='".$start_date."' AND p1.createdDate <= '".$end_date."'";
		}
		$query.=" ORDER BY `p1`.`id` DESC";
		$results=$this->db->query($query);
		$result=$results->result();
		return $result;
	}

	public function get_available_drivers_by_date($logged_date='', $driver_type='')
	{
		$query="SELECT p1.*,d.driverType FROM drivershifthistory p1 LEFT JOIN driver d ON p1.driverId=d.id INNER JOIN ( SELECT max(createdDate) MaxDate, driverId FROM drivershifthistory WHERE createdDate < '".$logged_date."' GROUP BY driverId ) p2 ON p1.driverId = p2.driverId AND p1.createdDate = p2.MaxDate ";
		if($driver_type)
		{
			$query.=" AND d.driverType='".$driver_type."' ";
		}
		$query.="  ORDER BY `p1`.`driverId` DESC";
		$results=$this->db->query($query);
		$result=$results->result();
		return $result;
	}

	/*** Get Passenger Profile details using passenger log id ***/
	public function get_passenger_log_details($passengerlog_id="")
	{
		$sql = "SELECT * , ".COMPANY.".cid as get_companyid, ".PEOPLE.".name AS driver_name,".PEOPLE.".phone AS driver_phone, ".PASSENGERS.".discount AS passenger_discount, ".PASSENGERS.".name AS passenger_name,".PASSENGERS.".email AS passenger_email,".PASSENGERS.".creditcard_no AS creditcard_no,".PASSENGERS.".phone AS passenger_phone FROM  ".PASSENGERS_LOG." JOIN  ".COMPANY." ON (  ".PASSENGERS_LOG.".`company_id` =  ".COMPANY.".`cid` ) JOIN  ".PASSENGERS." ON (  ".PASSENGERS_LOG.".`passengers_id` =  ".PASSENGERS.".`id` ) JOIN  ".PEOPLE." ON (  ".PEOPLE.".`id` =  ".PASSENGERS_LOG.".`driver_id` ) WHERE  ".PASSENGERS_LOG.".`passengers_log_id` =  '$passengerlog_id'";
		$results=$this->db->query($sql);
		$result=$results->result();
		return $result;
	}

	/**Save earnings**/
	public function save_driver_earnings($driver_id=0, $trip_id='', $data=array())
	{
		if($driver_id && $trip_id)
		{
			$this->db->where('driverId', $driver_id);
			$this->db->where('tripId', $trip_id);
			$this->db->delete('driverearninghistory');
		}
		if($data)
		{
			$this->db->insert_batch('driverearninghistory', $data);
		}
	}

	public function save_driver_daily_earnings($data=array())
	{
		foreach ($data as $earning_data)
		{
			$this->db->where('driverId', $earning_data['driverId']);
			$this->db->where('earningType', $earning_data['earningType']);
			$this->db->where('earningDate', $earning_data['earningDate']);
			$this->db->delete('driverearninghistory');

			$this->db->insert('driverearninghistory', $earning_data);
		}
	}

	public function get_driver_earnings_history($driver_id)
	{
		$query="select * from driverearninghistory where driverId=".$driver_id;
		$results=$this->db->query($query);
		$result=$results->result();
		return $result;
	}
	public function get_drivers_last_shift($filter_date)
	{
		$query="SELECT  `driverId`, `inLatitude`, `inLongitude`, `outLatitude`, `outLongitude`, `availabilityStatus`, `shiftStatus` FROM `drivershifthistory` WHERE DATE(createdDate)=DATE('".$filter_date."') Group By driverId ORDER BY `createdDate` DESC";
		$results=$this->db->query($query);
		$result=$results->result();
		return $result;
	}

	public function get_shift_driver_sessions($startDateTime, $endDateTime)
	{
		if(!$startDateTime)
		{
			$startDateTime=date('Y-m-d H:i:s');
		}
		if(!$endDateTime)
		{
			$endDateTime=date('Y-m-d H:i:s');
		}
		$query="SELECT concat(D.firstName,' ',D.lastName)AS driver_name,D.mobile AS driver_phone, A.`createdDate`, A.shiftStatus, D.driverType FROM drivershifthistory A LEFT JOIN driver D ON A.driverId=D.id WHERE D.id>0 ";

		$query.="  AND A.shiftStatus='IN' AND A.createdDate >='".$startDateTime."' AND A.createdDate <='".$endDateTime."'";

		$query.=" ORDER BY `A`.`createdDate` DESC";

		$result = $this->db->query($query);
        return $result->result();
	}
}
