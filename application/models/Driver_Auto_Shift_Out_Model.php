<?php
/*
 * created by Aditya on May 29 2018
 * use of this model is to simply handle requests related to Driver Auto Shift Out
 * purpose of the table is to store a list of Drivers that were auto shifted out and whether SMS was sent to them or not
*/
require_once(APPPATH.'config/device_type_enum.php');
require_once(APPPATH.'config/driver_type_enum.php');
class Driver_Auto_Shift_Out_Model extends MY_Model {

	protected $_table = 'driverautoshiftout';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}

	// created by Aditya on May 30 2017
	// this function simply returns entire list of drivers to whom SMS needs to be sent
	// returns result in array format
	public function getDriverListForSMS() {
		$query = '
			SELECT
				daso.id, daso.driverId,
				d.mobile, d.firstName

			FROM
				driverautoshiftout daso

			LEFT JOIN
				driver d
				ON daso.driverId = d.id

			WHERE
				daso.sms_sent = "No"
			ORDER BY
				daso.id ASC
		';
		$results = $this->db->query($query);
		$result = $results->result_array();
		return $result;
	}
}
