<?php
require_once(APPPATH.'config/company_type_enum.php');
class Corporate_Partners_Details_Model extends MY_Model {

	protected $_table = 'corporatepartnersdetails';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'name';
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}

	/**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
}