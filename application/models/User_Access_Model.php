<?php

require_once(APPPATH . 'config/user_access_level_enum.php');
require_once(APPPATH . 'config/role_type_enum.php');

class User_Access_Model extends My_Model {

    private $role = null;
    private $userid = null;

    function __construct() {

        parent::__construct();

        $this->role   = $this->session->userdata('role_type');
        $this->userid = $this->session->userdata('user_id');
    }

    private function getRole() {
        return $this->role;
    }

    private function getUserid() {
        return $this->userid;
    }

    public function showEmergency()
    {
    	$showEmergency = false;
    	switch ($this->getRole()) {

    		case Role_Type_Enum::SUPER_ADMIN    :
    		case Role_Type_Enum::MANAGER        :
    		case Role_Type_Enum::ADMIN          :
    		case Role_Type_Enum::STAFF          :
    		case Role_Type_Enum::SUPERVISOR     :
    			$showEmergency = true;
    			break;
    		default:
    			$showEmergency = false;
    			break;
    	}

    	return $showEmergency;
    }
    public function showReports() {
        $showReports = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::ACCOUNTANT     :
            case Role_Type_Enum::SUPER_ADMIN    :
            case Role_Type_Enum::MANAGER        :
            case Role_Type_Enum::ADMIN          :
                $showReports = true;
                break;
            default:
                $showReports = false;
                break;
        }

        return $showReports;
    }

    public function showDriver() {
        $showDriver = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF       :
                $showDriver = true;
                break;
            default:
                $showDriver = false;
                break;
        }

        return $showDriver;
    }

    public function deleteDriver() {
    	$deleteDriver = false;
    	switch ($this->getRole()) {
    		case Role_Type_Enum::SUPER_ADMIN :
    		case Role_Type_Enum::MANAGER     :
    		case Role_Type_Enum::ADMIN       :
    			$deleteDriver = true;
    			break;
    		default:
    			$deleteDriver = false;
    			break;
    	}

    	return $deleteDriver;
    }

    public function verificationDriver() {
    	$verificationDriver = false;
    	switch ($this->getRole()) {
    		case Role_Type_Enum::SUPER_ADMIN :
    		case Role_Type_Enum::MANAGER     :
    		case Role_Type_Enum::ADMIN       :
    			$verificationDriver = true;
    			break;
    		default:
    			$verificationDriver = false;
    			break;
    	}

    	return $verificationDriver;
    }

    public function showPassenger() {
        $showPassenger = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF:
                $showPassenger = true;
                break;
            default:
                $showPassenger = false;
                break;
        }

        return $showPassenger;
    }
    public function showPartners() {
        $showPartners = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
                $showPartners = true;
                break;
            default:
                $showPartners = false;
                break;
        }

        return $showPartners;
    }

    public function showPromoCode() {
        $showPromoCode = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::STAFF  :
                $showPromoCode = true;
                break;
            default:
                $showPromoCode = false;
                break;
        }

        return $showPromoCode;
    }

    public function showEmployees() {
        $showEmployees = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::SUPERVISOR  :
                $showEmployees = true;
                break;
            default:
                $showEmployees = false;
                break;
        }

        return $showEmployees;
    }

     public function showRateCard() {
        $showRateCard = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::STAFF       :  // only view
                $showRateCard = true;
                break;
            default:
                $showRateCard = false;
                break;
        }

        return $showRateCard;
    }

    public function showBillSummary() {
        $showBillSummary = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::STAFF       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::ACCOUNTANT  :
            case Role_Type_Enum::COMPANY     :
                $showBillSummary = true;
                break;
            default:
                $showBillSummary = false;
                break;
        }

        return $showBillSummary;
    }

    public function showWalletTransaction() {
    	$showWalletTransaction = false;
    	switch ($this->getRole()) {
    		case Role_Type_Enum::SUPER_ADMIN :
    		case Role_Type_Enum::MANAGER     :
    		case Role_Type_Enum::ADMIN       :
    		case Role_Type_Enum::STAFF       :
    		case Role_Type_Enum::SUPERVISOR  :
    			$showWalletTransaction = true;
    			break;
    		default:
    			$showWalletTransaction = false;
    			break;
    	}

    	return $showWalletTransaction;
    }

    public function showBankDetails() {
    	$showBankDetails = false;
    	switch ($this->getRole()) {
    		case Role_Type_Enum::SUPER_ADMIN :
    		case Role_Type_Enum::ADMIN       :
    		case Role_Type_Enum::ACCOUNTANT  :
    			$showBankDetails = true;
    			break;
    		default:
    			$showBankDetails = false;
    			break;
    	}

    	return $showBankDetails;
    }

    public function showTrips() {
        $showTrips = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
            case Role_Type_Enum::STAFF       :
            case Role_Type_Enum::SUPERVISOR  :
            case Role_Type_Enum::COMPANY     :
                $showTrips = true;
                break;
            default:
                $showTrips = false;
                break;
        }

        return $showTrips;
    }

    public function showDriverMap() {
    	$showDriverMap = false;
    	switch ($this->getRole()) {
    		case Role_Type_Enum::SUPER_ADMIN :
    		case Role_Type_Enum::MANAGER     :
    		case Role_Type_Enum::ADMIN       :
    		case Role_Type_Enum::STAFF       :
    		case Role_Type_Enum::SUPERVISOR  :

    			$showDriverMap = true;
    			break;
    		default:
    			$showDriverMap = false;
    			break;
    	}

    	return $showDriverMap;
    }

    // this function has been added by Aditya on May 30 2017
    public function showDriverMapForPartner() {
        //return $this->getRole();//CO->Company->Partner

    	$showDriverMapForPartner = false;
    	switch ($this->getRole()) {
    		case Role_Type_Enum::COMPANY :

    			$showDriverMapForPartner = true;
    			break;
    		default:
    			$showDriverMapForPartner = false;
    			break;
    	}

    	return $showDriverMapForPartner;
    }

    public function showSettings() {
        $showSettings = false;
        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN :
            case Role_Type_Enum::MANAGER     :
            case Role_Type_Enum::ADMIN       :
                $showSettings = true;
                break;
            default:
                $showSettings = false;
                break;
        }

        return $showSettings;
    }
    public function getAccessLevelForTrip() {

        $accessLevel = User_Access_Level_Enum::NO_ACCESS;

        //$userRole = $this->getRoleInRelease($releaseModel->get('id'));

        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::STAFF:
                $accessLevel = User_Access_Level_Enum::EDIT;
                  break;
            case Role_Type_Enum::ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::MANAGER:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            default:
                $accessLevel = User_Access_Level_Enum::NO_ACCESS;
                break;
        }

        return $accessLevel;
    }


    public function getAccessLevelForRateCard() {

        $accessLevel = User_Access_Level_Enum::NO_ACCESS;

        //$userRole = $this->getRoleInRelease($releaseModel->get('id'));

        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::MANAGER:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::STAFF:
                $accessLevel = User_Access_Level_Enum::READ_ONLY;
                break;
            default:
                $accessLevel = User_Access_Level_Enum::NO_ACCESS;
                break;
        }

        return $accessLevel;
    }

    public function getAccessLevelForPromoCode() {

        $accessLevel = User_Access_Level_Enum::NO_ACCESS;

        //$userRole = $this->getRoleInRelease($releaseModel->get('id'));

        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::MANAGER:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::SUPERVISOR:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::STAFF:
                $accessLevel = User_Access_Level_Enum::READ_ONLY;
                break;
            default:
                $accessLevel = User_Access_Level_Enum::NO_ACCESS;
                break;
        }

        return $accessLevel;
    }

    public function getAccessLevelForTripEdit() {

        $accessLevel = User_Access_Level_Enum::NO_ACCESS;


        switch ($this->getRole()) {
            case Role_Type_Enum::SUPER_ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::ADMIN:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::MANAGER:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::SUPERVISOR:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::STAFF:
                $accessLevel = User_Access_Level_Enum::EDIT;
                break;
            case Role_Type_Enum::COMPANY:
                $accessLevel = User_Access_Level_Enum::READ_ONLY;
                break;
            default:
                $accessLevel = User_Access_Level_Enum::NO_ACCESS;
                break;
        }

        return $accessLevel;
    }




}//end of class
