<?php
require_once(APPPATH.'config/trip_type_enum.php');
require_once(APPPATH.'config/trip_status_enum.php');
require_once(APPPATH.'config/payment_mode_enum.php');
require_once(APPPATH.'config/driver_accepted_status_enum.php');
require_once(APPPATH.'config/transmission_type_enum.php');
require_once(APPPATH.'config/bill_type_enum.php');
class Trip_Details_Model extends MY_Model {

	protected $_table = 'tripdetails';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}

	// Function created by Aditya on 04 May 2017
	// This function simply returns timings of all Outstation trips for a particular driver
	// these values will mostly be used to calculate Login Time Incentive
	// AND MONTH(actualPickupDateTime) = MONTH(CURRENT_DATE())
	public function getOutstationTripDetails ($driverId) {
		$query = '
        	SELECT 
        		id, actualPickupDateTime, dropDateTime
			FROM 
				`tripdetails`
			WHERE 
				(
					tripType = "XR"
				OR	tripType = "XO"
				OR 	tripType = "X"
				)
			AND (
					actualPickupDateTime <> "0000-00-00 00:00:00"
				AND	dropDateTime <> "0000-00-00 00:00:00"
				)
			AND (
					actualPickupDateTime <> ""
				AND	dropDateTime <> ""
				)
			AND tripStatus = "TCT"
			
			AND driverId = '.$driverId.'
			ORDER BY 
				id ASC
        ';
        $result = $this->db->query($query);

        return $result->result();
	}

}