<?php
class Passenger_Referral_Details_Model extends MY_Model {

	protected $_table = 'passengerreferraldetails';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}

}