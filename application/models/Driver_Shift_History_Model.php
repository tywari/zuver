<?php
require_once(APPPATH . 'config/driver_available_status_enum.php');
require_once(APPPATH . 'config/driver_shift_status_enum.php');
class Driver_Shift_History_Model extends MY_Model {

	protected $_table = 'drivershifthistory';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}

}