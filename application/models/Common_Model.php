<?php
class Common_Model extends MY_Model {

	protected $_table = '';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
//Getting Current Time of the company Time
//@todo check agian
	public function getcompany_all_currenttimestamp($company_id)
	{
		//echo $company_id;exit;
		if($company_id == '')
		{
			$current_time = convert_timezone('now',TIMEZONE);
			$current_date = explode(' ',$current_time);
			$start_time = $current_date[0].' 00:00:01';
			$end_time = $current_date[0].' 23:59:59';
			$date = $current_date[0].' %';
		}	
		else
		{
			$timezone_base_query = "select time_zone from  company where cid='$company_id' "; 
			
			$timezone_fetch = $this->db->query($timezone_base_query);
			$timezone_fetch= $this->fetchAll($timezone_fetch);
					
			$timezonefetch=isset($timezone_fetch[0]['time_zone'])?$timezone_fetch[0]['time_zone']:"";
			if($timezonefetch != '')
			{
				$current_time = convert_timezone('now',$timezone_fetch[0]['time_zone']);
				$current_date = explode(' ',$current_time);
			}
			else
			{
				$current_time = convert_timezone('now',TIMEZONE);
				$current_date = explode(' ',$current_time);
				$start_time = $current_date[0].' 00:00:01';
				$end_time = $current_date[0].' 23:59:59';
				$date = $current_date[0].' %';
			}
		}
		return $current_time;
	}
	
	public function sms_message_by_title($sms_title='')
	{
		$query="select * from ".SMS_TEMPLATE ." where sms_title=".$sms_title." order by sms_id asc";
		$result = $this->db->query($query);
			$result= $this->fetchAll($result);
		return $result;
	}
	
	/**get state details**/
	public function gateway_details($company_id=null,$booktype=null)
	{
		//echo $company_id;exit;
		/*$get_company = DB::select('company_id')->from(COMPANY_PAYMENT_MODULES)->where('company_id','=',$company_id)
		->execute()
		->as_array();*/
		$query="select company_id from ".COMPANY_PAYMENT_MODULES." where company_id=".$company_id;
		$result = $this->db->query($query);
		$get_company= $this->fetchAll($result);
		//print_r($get_company);exit;
		//if($company_id !="")
		if(count($get_company)>0)
		{
			if($booktype == 1)
			{
				/// Query which is used for getiing payment modules without cash payments for dispatch
				/*$result = DB::select('pay_mod_id','pay_mod_name','pay_mod_default')->from(COMPANY_PAYMENT_MODULES)
				->where('pay_active','=','1')
				->where('pay_mod_id','!=','1')
				->where('company_id','=',$company_id)
				->order_by('pay_mod_id','asc')
				->execute()
				->as_array();*/
				$query="select pay_mod_id,pay_mod_name,pay_mod_default from ".COMPANY_PAYMENT_MODULES." where pay_active=1 AND pay_mod_id!=1 AND company_id=".$company_id." order by pay_mod_id";
				$result = $this->db->query($query);
				$result= $this->fetchAll($result);
			}
			else
			{
				/*$result = DB::select('pay_mod_id','pay_mod_name','pay_mod_default')->from(COMPANY_PAYMENT_MODULES)
				->where('pay_active','=','1')
				->where('company_id','=',$company_id)
				->order_by('pay_mod_id','asc')
				->execute()
				->as_array();*/
				$query="select pay_mod_id,pay_mod_name,pay_mod_default from ".COMPANY_PAYMENT_MODULES." where pay_active=1 AND company_id=".$company_id." order by pay_mod_id";
				$result = $this->db->query($query);
				$result= $this->fetchAll($result);
			}
		}
		else
		{
			if($booktype == 1)
			{
				/// Query which is used for getiing payment modules without cash payments for dispatch
				/*$result = DB::select('pay_mod_id','pay_mod_name','pay_mod_default')->from(PAYMENT_MODULES)->where('pay_mod_active','=','1')->where('pay_mod_id','!=','1')->order_by('pay_mod_id','asc')
				->execute()
				->as_array();*/
				$query="select pay_mod_id,pay_mod_name,pay_mod_default from ".PAYMENT_MODULES." where pay_mod_active=1 AND pay_mod_id!=1 order by pay_mod_id";
				$result = $this->db->query($query);
				$result= $this->fetchAll($result);
			}
			else
			{
				/*$result = DB::select('pay_mod_id','pay_mod_name','pay_mod_default')->from(PAYMENT_MODULES)->where('pay_mod_active','=','1')->order_by('pay_mod_id','asc')
				->execute()
				->as_array();*/
				$query="select pay_mod_id,pay_mod_name,pay_mod_default from ".PAYMENT_MODULES." where pay_mod_active=1 order by pay_mod_id";
				$result = $this->db->query($query);
				$result= $this->fetchAll($result);
			}
		}
		return $result;
	}
	
	public function update_commission($pass_logid,$total_amount,$admin_commission)
	{
			
		$company_query = "select company_id from passengers_log where passengers_log_id =".$pass_logid;
	
		$company_results = $this->db->query($company_query);
		$company_results= $this->fetchAll($company_results);
	
		$company_id = $company_results[0]['company_id'];
	
	
		$first_query = "select check_package_type from " . PACKAGE_REPORT . " join ".PACKAGE." on ".PACKAGE.".package_id =".PACKAGE_REPORT.".upgrade_packageid  where ".PACKAGE_REPORT.".upgrade_companyid = ".$company_id."  order by upgrade_id desc limit 0,1";
	
		$first_results  = $this->db->query($first_query);
		$first_results= $this->fetchAll($first_results);
	
		if(count($first_results) > 0)
		{
			$check_package_type = $first_results[0]['check_package_type'];
		}
		else
		{
			$check_package_type = 'T';
	
		}
	
		if($check_package_type != 'N')
		{
			$admin_amt = ($total_amount * $admin_commission)/100; //payable to admin
			$admin_amt = round($admin_amt, 2);
			$total_balance = round($total_amount,2);
	
			//Set Commission to Admin
			$updatequery = " UPDATE ". PEOPLE ." SET account_balance=account_balance+$total_balance WHERE user_type = 'A'";
	
			/*$updateresult = Db::query(Database::UPDATE, $updatequery)
			->execute();*/
			$updateresult =$this->db->query($updatequery);
		}
		else
		{
		$admin_amt = 0;
		}
	
		$company_amt = $total_amount - $admin_amt;
		$company_amt = round($company_amt, 2);
	
		//Set Commission to Admin
		$updatequery = " UPDATE ". PEOPLE ." SET account_balance=account_balance+$company_amt WHERE user_type = 'C' and company_id=".$company_id;
	
		$updateresult =$this->db->query($updatequery);
		/*$updateresult = Db::query(Database::UPDATE, $updatequery)
		->execute();*/
	
			
		$result_array = array();
		$result_array['admin_commission'] = $admin_amt;
		$result_array['company_commission'] = $company_amt;
		$result_array['trans_packtype'] = $check_package_type;
	
	
		return $result_array;
	}

}