<?php
require_once(APPPATH . 'config/device_type_enum.php');
require_once(APPPATH . 'config/signup_type_enum.php');
class Passenger_Model extends MY_Model {

	protected $_table = 'passenger';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}

}