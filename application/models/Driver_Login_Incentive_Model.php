<?php
class Driver_Login_Incentive_Model extends MY_Model {

	protected $_table = 'driverloginincentiverate';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'name';

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	/**
	 *  helper method to load the key value into dropdown boxes
	 * @return type
	 */
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}

	/*
	*  Created by Aditya on May 04 2017
	*  This function will simply return details of Login Incentive rules for B2C Drivers only
	*/
	public function getB2CLoginIncentiveDetails() {
		$query = '
        	SELECT 
        		*
			FROM 
				`driverloginincentiverate`
			WHERE 
				driverType = "B2C"
			ORDER BY 
				id ASC
        ';
        $result = $this->db->query($query);

        return $result->result();
	}
}