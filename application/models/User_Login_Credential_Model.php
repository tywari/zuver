<?php
require_once(APPPATH.'config/role_type_enum.php');
class User_Login_Credential_Model extends MY_Model {

	protected $_table = 'userlogincredential';//model table_name
	public $_keyName = 'id';
	public $_valueName = 'firstName';
	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	public function getKeyName(){
		return $this->_keyName;
	}
	
	public function getValueName(){
		return $this->_valueName;
	}
	
	public function generateUserIndentity($company_name,$role_type){
		$companyname     = substr($company_name, 0, 3);
		$user_id= 001;
		$this->db->select('id');
		$this->db->from('userlogincredential');
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get();
		$result = $query->row();
		if($result)
		{
			$id=$result->id+1;
			$user_id= $id;
		}
		$user_identity =  strtoupper($companyname.$role_type).$user_id;
		return $user_identity;
	}

}