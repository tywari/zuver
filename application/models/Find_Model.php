<?php
class Find_Model extends MY_Model {

	protected $_table = '';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}
	
	// search nearest drivers around passengers pickup location
	
	public function nearestdrivers($lat,$long,$taxi_model,$passenger_id,$distance = NULL,$company_id,$unit)
	{
		//print_r($remove_driver_list);
		$free_driver = $this->availabledrivers($passenger_id,$company_id);
		//$this->currentdate = Commonfunction::getCurrentTimeStamp();
		$where = '';
		$driver_list ='';
		$driver_count = '';
		$unit_conversion = "";
		$driver_list_array = array();
	
		if($free_driver > 0)
		{
			foreach($free_driver as $key => $value)
			{
				$driver_list_array[] = $value['id'];
			}
		}
		else
		{
			$driver_list_array = array();
		}
	
		// Find already rejected and timeout drivers in the current trip
		$flag=1;
		$get_passenger_driverid = $this->unset_driverlist_app($passenger_id,$flag,$company_id);
		if($get_passenger_driverid != "")
		{
			$remove_driver_list=explode(',',$get_passenger_driverid);
		}
		else
		{
			$remove_driver_list = array();
		}
		// Exclude already rejected and timeout drivers in the current trip
		$driver_arraylist = array_diff($driver_list_array,$remove_driver_list);
	
		foreach($driver_arraylist as $key => $value)
		{
			$driver_count = 1;
			$driver_list .= "'".$value."',";
		}
	
		if($driver_count > 0)
		{
			$driver_list = substr_replace($driver_list ,"",-1);
		}
		else
		{
			$driver_list = "''";
		}
		$company_condition="";
		if($company_id != ""){
			$company_condition = "AND tmap.mapping_companyid = '$company_id' AND taxi.taxi_company = '$company_id'";
		}
	
		if(($taxi_model != 'All') && ($taxi_model != null)){
			$where.= " AND taxi.`taxi_model`='".$taxi_model."' ";
		}
	
		if($company_id == '')
		{
			$current_time = convert_timezone('now',TIMEZONE);
			$current_date = explode(' ',$current_time);
			$start_time = $current_date[0].' 00:00:01';
			$end_time = $current_date[0].' 23:59:59';
		}
		else
		{
			$model_base_query = "select time_zone from  company where cid='$company_id' ";
			$model_fetch = $this->db->query($model_base_query);
		$model_fetch= $this->fetchAll($model_fetch);
	
			if($model_fetch[0]['time_zone'] != '')
			{
					$current_time = convert_timezone('now',$model_fetch[0]['time_zone']);
				$current_date = explode(' ',$current_time);
					$start_time = $current_date[0].' 00:00:01';
				$end_time = $current_date[0].' 23:59:59';
			}
							else
							{
							$current_time =	date('Y-m-d H:i:s');
							$start_time = date('Y-m-d').' 00:00:01';
							$end_time = date('Y-m-d').' 23:59:59';
							}
		}
		$distance_query="";
		if($distance)
		{
			$distance_query = "HAVING d_distance <='$distance'";
		}
	
		if($unit == '0')
		{
			$unit_conversion = '*1.609344';
		}
	
		$query =" select list.name as name,list.driver_id as driver_id,list.latitude as latitude,list.longitude as longitude,list.d_distance as distance_km,taxi.taxi_id as taxi_id,taxi.taxi_speed as taxi_speed,list.updatetime_difference as updatetime_difference from ( SELECT people.name,driver.*,(((acos(sin((".$lat."*pi()/180)) * sin((driver.latitude*pi()/180))+cos((".$lat."*pi()/180)) *  cos((driver.latitude*pi()/180)) * cos(((".$long."- driver.longitude)* pi()/180))))*180/pi())*60*1.1515$unit_conversion) AS d_distance,(TIME_TO_SEC(TIMEDIFF('$current_time',driver.update_date))) AS updatetime_difference
		FROM ".DRIVER." AS driver JOIN ".PEOPLE." AS people ON driver.driver_id=people.id  where people.login_status='S' $distance_query AND driver.status='F' AND driver.shift_status='IN' and driver_id IN ($driver_list) order by d_distance ) as list
		JOIN ".TAXIMAPPING." as tmap ON list.`driver_id`=tmap.`mapping_driverid`
			JOIN ".TAXI." as taxi ON tmap.`mapping_taxiid`=taxi.`taxi_id`
			JOIN ".COMPANY." AS comp ON tmap.`mapping_companyid`=comp.`cid`  where updatetime_difference  <= '".LOCATIONUPDATESECONDS."' and tmap.mapping_startdate <= '$current_time' AND  tmap.mapping_enddate >= '$current_time' AND tmap.`mapping_status`='A' ".$where." $company_condition group by list.driver_id";
	
				//updatetime_difference  <= '51' and
		//"select list.name as name,list.driver_id as driver_id,list.latitude as latitude,list.longitude as longitude,list.d_distance as distance_km,list.update_date as location_update_date,list.updatetime_difference as updatetime_difference,taxi.taxi_id as taxi_id,taxi.taxi_speed as taxi_speed from ( SELECT people.name,driver.*,(((acos(sin((11.021366*pi()/180)) * sin((driver.latitude*pi()/180))+cos((11.021366*pi()/180)) * cos((driver.latitude*pi()/180)) * cos(((76.9166495- driver.longitude)* pi()/180))))*180/pi())*60*1.1515*1.609344) AS d_distance,(TIME_TO_SEC(TIMEDIFF('2014-07-14 09:45:50','2014-07-14 09:45:00'))) AS updatetime_difference  FROM driver AS driver JOIN people AS people ON driver.driver_id=people.id where people.login_status='S'   HAVING d_distance <='15' AND driver.status='F'  and driver.shift_status='IN' and driver_id IN ('35','36','37','48','50','51','52') order by d_distance ) as list JOIN taxi_driver_mapping as tmap ON list.`driver_id`=tmap.`mapping_driverid` JOIN taxi as taxi ON tmap.`mapping_taxiid`=taxi.`taxi_id` JOIN company AS comp ON tmap.`mapping_companyid`=comp.`cid` where updatetime_difference  <= '51' and tmap.mapping_startdate <= '2014-07-14 12:08:05' AND tmap.mapping_enddate >= '2014-07-14 12:08:05' AND tmap.`mapping_status`='A' AND tmap.mapping_companyid = '1' AND taxi.taxi_company = '1' group by list.driver_id"
		//echo $query.'<br/><br/>';
	
		$result = $this->db->query($query);
		$result= $this->fetchAll($result);
    	return $result;
	
	}
	
	public function unset_driverlist_app($log_id,$flag,$company_id)
	{
		//$log_id = $this->get_sublogid($log_id);
	
		//$result = DB::select('driver_id')->from(PASSENGERS_LOG)->where('sub_logid','=',$log_id)->order_by('passengers_log_id','asc')
		if($flag == '0')
		{
			//based on passenger log id
			$query = "select tdriver_ids from ".PASSENGER_LOG_TEMP." where  tpassenger_log_id=".$log_id;
			//->order_by('tpassengers_log_id','asc')
			$result = $this->db->query($query);
			$result= $this->fetchAll($result);
		}
		else
		{
			//$Commonmodel = Model::factory('Commonmodel');
			$company_all_currenttimestamp = $commonmodel->getcompany_all_currenttimestamp($company_id);
			//based on passenger id and date
			$query ="SELECT tdriver_ids FROM  ".PASSENGER_LOG_TEMP." WHERE  `tpassenger_id` =  '$log_id' AND DATE(createdate) !=  '$company_all_currenttimestamp' order by 'tpassengers_log_id' ASC";
			$result = $this->db->query($query);
			$result= $this->fetchAll($result);
		}
		if(count($result)>0)
		{
			$output = $result[0]['tdriver_ids'];
		}
		else
		{
			$output="";
		}
		return $output;
	}
	
	public function availabledrivers($availabledrivers,$company_id ='')
	{
		//print_r($request);
	
	
		$current_time = convert_timezone('now',TIMEZONE);
		$current_date = explode(' ',$current_time);
		$start_time = $current_date[0].' 00:00:01';
		$end_time = $current_date[0].' 23:59:59';
	
		//$cuurentdate = date('Y-m-d H:i:s');
		//$enddate = date('Y-m-d').' 23:59:59';
			
	
		$company_condition="";
		if($company_id != ""){
			$company_condition = "AND taximapping.mapping_companyid = '$company_id' AND people.company_id = '$company_id' AND taxi.taxi_company = '$company_id'";
		}
	
		$sql ="SELECT people.id,taxi.taxi_id  ,(select check_package_type from ".PACKAGE_REPORT." where ".PACKAGE_REPORT.".upgrade_companyid = ".TAXI.".taxi_company  order by upgrade_id desc limit 0,1 ) as check_package_type,(select upgrade_expirydate from ".PACKAGE_REPORT." where ".PACKAGE_REPORT.".upgrade_companyid = ".TAXI.".taxi_company order by upgrade_id desc limit 0,1 ) as upgrade_expirydate FROM ".TAXI." as taxi
		 JOIN ".COMPANY." as company ON taxi.taxi_company = company.cid
		 JOIN ".TAXIMAPPING." as taximapping  ON taxi.taxi_id = taximapping.mapping_taxiid
		 JOIN ".PEOPLE." as people ON people.id = taximapping.mapping_driverid WHERE people.status = 'A' AND taxi.taxi_status = 'A' AND taxi.taxi_availability = 'A' AND people.availability_status = 'A'   and people.booking_limit > (SELECT COUNT( passengers_log_id ) FROM  ".PASSENGERS_LOG." WHERE driver_id = people.id AND  `createdate` >='".$start_time."' AND  `travel_status` =  '1' AND booking_from != '2')
			 AND taximapping.mapping_status = 'A' $company_condition AND company.company_status='A' AND taximapping.mapping_startdate <='$current_time' AND  taximapping.mapping_enddate >='$current_time'  group by taxi_id Having ( check_package_type = 'T' or upgrade_expirydate >='$current_time')";
		//AND taximapping.mapping_startdate <='$current_time' AND  taximapping.mapping_enddate >='$current_time'
		//AND people.notification_setting = '1'
		//echo $sql;
		$result = $this->db->query($sql);
			$result= $this->fetchAll($result);
		return $result;
	}
}