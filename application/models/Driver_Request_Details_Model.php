<?php
require_once(APPPATH.'config/driver_request_status_enum.php');
class Driver_Request_Details_Model extends MY_Model {

	protected $_table = 'driverrequestdetails';//model table_name

	/**
	 *  Default Constructor
	 */
	function __construct($args=NULL)
	{
		parent::__construct();
		if( is_object($args))   $args = get_object_vars($args);
		if( is_array($args)){
			foreach( $args AS $key => $value ){
				$this->{$key} = $value;
			}
		}

	}

}