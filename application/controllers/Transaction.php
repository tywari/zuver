<?php
class Transaction extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Trip_Details_Model' );
		$this->load->model('Sub_Trip_Details_Model');
		$this->load->model ( 'Api_Webservice_Model' );
		$this->load->model ( 'City_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Trip_Transaction_Details_Model' );
		$this->load->model ( 'Trip_Tracked_Info_Model' );
	}
	public function index() {
		// $data = array();
		$this->addJs ( 'app/transaction.js' );
		
		// $this->render("transaction/add_edit_driver", $data);
	}
	public function add() {
		if ($this->User_Access_Model->showBillSummary () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/transaction.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "transaction";
		$data ['active_menu'] = 'transaction';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'id' );
		$data ['payment_mode'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		$data ['transaction_details_model'] = $this->Trip_Transaction_Details_Model->getBlankModel ();
		$this->render ( 'transaction/add_edit_transaction', $data );
	}
	public function saveTransaction() {
		$data = array ();
		
		$this->addJs ( 'app/transaction.js' );
		
		$data = $this->input->post ();
		$transaction_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($transaction_id > 0) {
			$transcation_updated = $this->Trip_Transaction_Details_Model->update ( $data, array (
					'id' => $transaction_id 
			) );
			if ($transcation_updated) {
				
				$msg = $msg_data ['updated'];
			} else {
				$msg = $msg_data ['failed'];
			}
		} else {
			$transaction_id = $this->Trip_Transaction_Details_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'transaction_id' => $transaction_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($transaction_id, $view_mode) {
		if ($this->User_Access_Model->showBillSummary () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/transaction.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		if ($this->input->post ( 'id' )) {
			$transaction_id = $this->input->post ( 'id' );
		}
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'id' );
		$data ['payment_mode'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		if ($transaction_id > 0) {
			// Get release details by transaction_id
			$data ['transaction_details_model'] = $this->Api_Webservice_Model->getBillDetails ( $transaction_id );
			/*
			 * $data['transaction_details_model']=$this->Trip_Transaction_Details_Model->getOneByKeyValueArray(
			 * array('id'=>$data['transaction_details_model']->id));
			 */
			$data ['sub_trips_list'] = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId'=>$data['transaction_details_model'][0]->tripId),'pickupDatetime ');
		} else {
			// Blank Model
			
			$data ['transaction_details_model'] = $this->Trip_Transaction_Details_Model->getBlankModel ();
		}
		$data ['activetab'] = "transaction";
		$data ['active_menu'] = 'transaction';
		if ($this->input->post ( 'id' )) {
			$html = $this->load->view ( "transaction/add_edit_transaction", $data );
			
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'transaction/add_edit_transaction', $data );
		}
		// echo json_encode(array('html'=>$html));
	}
	public function getBtoBTransactionList()
	{
		if ($this->User_Access_Model->showBillSummary () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// get type whether to load or render the ouptput
		
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/transaction.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "transaction";
		$data ['active_menu'] = 'transaction';
		
		$company_id = $this->session->userdata ( 'user_company' );
		$city_id = $this->session->userdata ( 'user_city' );
		$post_data = $this->input->post ();
		if (array_key_exists ( 'city_id', $post_data )) {
			$city_id = $post_data ['city_id'];
		}
		if ($this->session->userdata('role_type') == 'CO') {
			$company_id = $this->session->userdata('user_company');
		} else {
			$company_id = '';
		}
		
		$data ['transaction_model_list'] = $this->Query_Model->getTransactionList ( $city_id, $company_id,0 );
		
		
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "transaction/manage_transaction_b2b", $data );
			echo json_encode ( array (
					'html' => $html
			)
			);
		} else {
			$html = $this->render ( "transaction/manage_transaction_b2b", $data );
		}
	}
	
	public function getBtoCTransactionList()
	{
		if ($this->User_Access_Model->showBillSummary () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// get type whether to load or render the ouptput
		
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/transaction.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "transaction";
		$data ['active_menu'] = 'transaction';
		
		$city_id = $this->session->userdata ( 'user_city' );
		$post_data = $this->input->post ();
		if (array_key_exists ( 'city_id', $post_data )) {
			$city_id = $post_data ['city_id'];
		}
		if ($this->session->userdata('role_type') == 'CO') {
			$company_id = $this->session->userdata('user_company');
		} else {
			$company_id = '';
		}
		
		
		$data ['transaction_model_list'] = $this->Query_Model->getTransactionList ( $city_id, $company_id, 1);
		
		
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "transaction/manage_transaction_b2c", $data );
			echo json_encode ( array (
					'html' => $html
			)
			);
		} else {
			$html = $this->render ( "transaction/manage_transaction_b2c", $data );
		}
	} 
	public function getTransactionList() {
		if ($this->User_Access_Model->showBillSummary () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// get type whether to load or render the ouptput
		
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/transaction.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "transaction";
		$data ['active_menu'] = 'transaction';
		
		$company_id = $this->session->userdata ( 'user_company' );
		$city_id = $this->session->userdata ( 'user_city' );
		$post_data = $this->input->post ();
		if (array_key_exists ( 'city_id', $post_data )) {
			$city_id = $post_data ['city_id'];
		}
		
		if ($this->session->userdata ( 'role_type' ) == Role_Type_Enum::COMPANY) {
			$data ['transaction_model_list'] = $this->Query_Model->getTransactionList ( $city_id, $company_id );
		} else {
			$data ['transaction_model_list'] = $this->Query_Model->getTransactionList ( $city_id );
		}
		
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "transaction/manage_transaction", $data );
			echo json_encode ( array (
					'html' => $html 
			)
			 );
		} else {
			$html = $this->render ( "transaction/manage_transaction", $data );
		}
	}
	public function changeTransactionStatus() {
		$data = array ();
		
		$this->addJs ( 'app/transaction.js' );
		
		$data = $this->input->post ();
		$transaction_id = $data ['id'];
		$transaction_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($transaction_id) {
			$driver_updated = $this->Trip_Transaction_Details_Model->update ( array (
					'paymentStatus' => $transaction_status 
			), array (
					'id' => $transaction_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'transaction_id' => $transaction_id 
		);
		echo json_encode ( $response );
	}
}