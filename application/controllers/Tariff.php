<?php
class Tariff extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'City_Model' );
		$this->load->model ( 'Rate_Card_Details_Model' );
		$this->load->model ( 'Driver_Rate_Card_Details_Model' );
		$this->load->model ( 'Driver_Login_Incentive_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		//$this->load->model('Api_Webservice_Model');
	}
	public function index() {
		// $data = array();
		$this->addJs ( 'app/tariff.js' );

		// $this->render("driver/add_edit_driver", $data);
	}
	public function validationTariff() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'name',
						'label' => 'Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[25]'
				),
				array (
						'field' => 'dayConvenienceCharge',
						'label' => 'Day Convenience Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => 'nightConvenienceCharge',
						'label' => 'Night Convenience Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => 'fixedAmount',
						'label' => 'Fixed Trip fare',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '1HourCharge',
						'label' => '1st Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '2HourCharge',
						'label' => '2nd Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '3HourCharge',
						'label' => '3rd Hour Charge',
						'rules' => 'trim|required|decimal'
				),

				array (
						'field' => '4HourCharge',
						'label' => '4th Hour Charge',
						'rules' => 'trim|required|decimal'
				),

				array (
						'field' => '5HourCharge',
						'label' => '5th Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '6HourCharge',
						'label' => '6th Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '7HourCharge',
						'label' => '7th Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '8HourCharge',
						'label' => '8th Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '9HourCharge',
						'label' => '9th Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '10HourCharge',
						'label' => '10th Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '11HourCharge',
						'label' => '11th Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => '12HourCharge',
						'label' => '12th Hour Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => 'above12HourCharge',
						'label' => 'Above 12 Hours Charge',
						'rules' => 'trim|decimal|required'
				),
				array (
						'field' => 'waitingCharge',
						'label' => 'Waiting Charge',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => 'overtimeChargePerHour',
						'label' => 'Over Time Charge/Hour',
						'rules' => 'trim|required|decimal'
				),
				array (
						'field' => 'cityId',
						'label' => 'City Name',
						'rules' => 'trim|required'
				)
		);

		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showRateCard () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/tariff.js' );

		$data = array (
				'mode' => EDIT_MODE
		);
		$data ['activetab'] = "tariff";
		$data ['active_menu'] = 'tariff';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'id' );
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['bill_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'BILL_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['tariff_model'] = $this->Rate_Card_Details_Model->getBlankModel ();
		$this->render ( 'tariff/add_edit_tariff', $data );
	}
	public function saveTariff() {
		$this->addJs ( 'app/tariff.js' );
		$data = array ();
		$data = $this->input->post ();
		$tariff_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		if ($data ['billType'] == Bill_Type_Enum::MONTHLY) {
			$data ['isMonthly'] = Status_Type_Enum::ACTIVE;
		}

		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($tariff_id > 0) {
			$this->Rate_Card_Details_Model->update ( $data, array (
					'id' => $tariff_id
			) );
			$msg = $msg_data ['updated'];
		} else {

			$tariff_id = $this->Rate_Card_Details_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}

		// $this->render("passenger/add_edit_passenger", $data);

		$response = array (
				'msg' => $msg,
				'tariff_id' => $tariff_id
		);
		echo json_encode ( $response );
	}

	public function saveDriverTariff() {
		$this->addJs ( 'app/tariff.js' );
		$data = array ();
		$data = $this->input->post();
		//print_r($data); die();
		$driver_type = $data ['driverType'];

		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		foreach ($data['driver_rate_card'] as $key => $value) {
			$this->Driver_Rate_Card_Details_Model->update($value, array (
					'id' => $key
			) );
		}
		$msg = $msg_data ['updated'];

		$response = array (
				'msg' => $msg,
				'driver_type' => $driver_type
		);
		echo json_encode ( $response );
	}

	public function saveDriverLoginIncentive()
	{
		$this->addJs ( 'app/tariff.js' );
		$data = array ();
		$data = $this->input->post();
		//print_r($data); die();
		$driver_type = $data ['driverType'];

		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		foreach ($data['driver_incentive'] as $key => $value) {
			$this->Driver_Login_Incentive_Model->update($value, array (
					'id' => $key
			) );
		}
		$msg = $msg_data ['updated'];

		$response = array (
				'msg' => $msg,
				'driver_type' => $driver_type
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($tariff_id, $view_mode) {
		if ($this->User_Access_Model->showRateCard () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();

		$this->addJs ( 'app/tariff.js' );

		$data = array (
				'mode' => $view_mode
		);
		if ($this->input->post ( 'id' )) {
			$tariff_id = $this->input->post ( 'id' );
		}
		$data ['activetab'] = "tariff";
		$data ['active_menu'] = 'tariff';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'id' );
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['bill_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'BILL_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		if ($tariff_id > 0) {
			// Get release details by release_id
			$data ['tariff_model'] = $this->Rate_Card_Details_Model->getById ( $tariff_id );
		} else {
			// Blank Model
			$data ['tariff_model'] = $this->Rate_Card_Details_Model->getBlankModel ();
		}

		if ($this->input->post ( 'id' )) {
			$html = $this->load->view ( 'tariff/add_edit_tariff', $data, TRUE );

			echo json_encode ( array (
					'html' => $html
			) );
		} else {
			$this->render ( 'tariff/add_edit_tariff', $data );
		}
	}
	public function getTariffList() {
		if ($this->User_Access_Model->showRateCard () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'app/tariff.js' );
		$this->addJs ( 'app/tabel.js' );
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );

		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );

		$data ['activetab'] = "tariff";
		$data ['active_menu'] = 'tariff';

		$company_id = $this->session->userdata ( 'user_company' );
		$city_id = $this->session->userdata ( 'user_city' );

		$data ['tariff_model_list'] = $this->Query_Model->getTariffList ( $city_id );
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "tariff/manage_tariff", $data );
			echo json_encode ( array (
					'html' => $html
			) );
		} else {
			$html = $this->render ( "tariff/manage_tariff", $data );
		}
	}
	public function changeTariffStatus() {
		$data = array ();

		$this->addJs ( 'app/tariff.js' );

		$data = $this->input->post ();
		$tariff_id = $data ['id'];
		$tariff_status = $data ['status'];

		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($tariff_id) {
			$traiff_updated = $this->Rate_Card_Details_Model->update ( array (
					'status' => $tariff_status
			), array (
					'id' => $tariff_id
			) );
			$msg = $msg_data ['status_change_success'];
		}

		$response = array (
				'msg' => $msg,
				'tariff_id' => $tariff_id
		);
		echo json_encode ( $response );
	}

	public function getPartnerTripTypeList()
	{
		$company_id=$this->input->post('company_id');
		$trip_type_list = $this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName' => strtoupper('TRIP_TYPE'),
				'isVisible' => 'Y'), 'sequenceOrder');
		$city_list = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );
		$data=array();
		$options='';
		$status=0;
		$data['user_list']=array();
		$company_id=$this->input->post('company_id');
		if ($company_id > 0)
		{
			$options.='<option value="">-- Select --<options>';
			$data['rate_list']=$this->Rate_Card_Details_Model->getByKeyValueArray(array('companyId'=>$company_id,'status'=>Status_Type_Enum::ACTIVE));
			if (count($data['rate_list']) > 0)
			{
				$status=1;
				foreach ($data['rate_list'] as $value)
				{
					$options.='<option value="'.$value->tripType.'">'.$trip_type_list[$value->tripType].'-'.$value->name.'('.$city_list[$value->cityId].')<options>';
				}
			}
		}

		$response = array (
				'data' => $options,
				'status'=>$status
		);
		echo json_encode ( $response );
	}

	public function getBillTypeList()
	{
		$city_list = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => Status_Type_Enum::ACTIVE
		), 'name' );

		$company_id=$this->input->post('company_id');
		$trip_type=$this->input->post('trip_type');
		$city_name=$this->input->post('city_name');
		$city_name=str_ireplace(')', '', $city_name);
		$city_id=array_search($city_name, $city_list);
		$data=array();
		$options='';
		$status=0;
		$data['user_list']=array();
		$company_id=$this->input->post('company_id');
		if ($company_id > 0)
		{
			$options.='<option value="">-- Select --<options>';
			$data['rate_list']=$this->Rate_Card_Details_Model->getByKeyValueArray(array('companyId'=>$company_id,'tripType'=>$trip_type,'cityId'=>$city_id,'status'=>Status_Type_Enum::ACTIVE));
			if (count($data['rate_list']) > 0)
			{
				$status=1;
				foreach ($data['rate_list'] as $value)
				{
					if (($value->dayConvenienceCharge > 0 || $value->nightConvenienceCharge > 0) || ($value->firstHourCharge > 0 || $value-secondHourCharge > 0 || $value->thirdHourCharge > 0 || $value->fourthHourCharge > 0 || $value->fifthHourCharge > 0 || $value->sixthHourCharge > 0 || $value->seventhHourCharge > 0 || $value->eighthHourCharge > 0 || $value->ninthHourCharge > 0 || $value->tenthHourCharge > 0 || $value->eleventhHourCharge > 0 || $value->twelveHourCharge > 0 || $value->above12HourCharge > 0))
					{
						if ($value->isMonthly == Status_Type_Enum::ACTIVE)
						{
							$options.='<option value="'.Bill_Type_Enum::MONTHLY.'">'.Bill_Type_Enum::M.'<options>';
						}
						else
						{
							$options.='<option value="'.Bill_Type_Enum::HOURLY.'">'.Bill_Type_Enum::N.'<options>';
						}
					}
					if ($value->fixedAmount > 0)
					{
						$options.='<option value="'.Bill_Type_Enum::FIXED.'">'.Bill_Type_Enum::F.'<options>';
					}
					if ($value->distanceConvenienceCharge > 0 &&  ($value->distanceCostPerKm > 0 || $value->minDistance > 0))
					{
						$options.='<option value="'.Bill_Type_Enum::DISTANCE.'">'.Bill_Type_Enum::D.'<options>';
					}
					if ($value->timeConvenienceCharge > 0 &&  ($value->timeCostPerHour > 0 || $value->minTime > 0))
					{
						$options.='<option value="'.Bill_Type_Enum::TIME.'">'.Bill_Type_Enum::T.'<options>';
					}

				}
			}
		}

		$response = array (
				'data' => $options,
				'status'=>$status
		);
		echo json_encode ( $response );
	}

	public function getDriverTariffDetailsByDriverType($driver_type, $view_mode) {
		if ($this->User_Access_Model->showRateCard () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();

		$this->addJs ( 'app/tariff.js' );
		$this->addJs ( 'datepicker/jquery.timepicker.js' );
		$this->addCss('app/jquery.timepicker.css');

		$data = array (
				'mode' => $view_mode
		);
		if ($this->input->post ( 'driverType' )) {
			$driver_type = $this->input->post ( 'driverType' );
		}
		$data ['activetab'] = "tariff";
		$data ['active_menu'] = 'tariff';

		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				//'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );

		$data ['trip_type_list']['ALL']='ALL';
		$data ['trip_type_list']['B2B']='B2B';
		$data ['driver_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'DRIVER_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['earning_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'EARNING_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		$data ['measuring_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'MEASURING_TYPE' ),
				'isVisible' => Status_Type_Enum::ACTIVE
		), 'sequenceOrder' );
		if (!empty($driver_type)) {
			// Get release details by release_id
			$data ['driver_tariff_model'] = $this->Driver_Rate_Card_Details_Model->getByKeyValue('driverType',$driver_type);
			$data ['driver_login_incentive_model'] = $this->Driver_Login_Incentive_Model->getByKeyValue('driverType',$driver_type);
		} else {
			// Blank Model
			$data ['driver_tariff_model'] = $this->Driver_Rate_Card_Details_Model->getBlankModel ();
			$data ['driver_login_incentive_model'] = $this->Driver_Login_Incentive_Model->getBlankModel ();
		}
		if ($this->input->post ( 'driverType' )) {
			$html = $this->load->view ( 'tariff/add_edit_driver_tariff', $data, TRUE );

			echo json_encode ( array (
					'html' => $html
			) );
		} else {
			$this->render ( 'tariff/add_edit_driver_tariff', $data );
		}
	}
	public function getDriverTariffList() {
		if ($this->User_Access_Model->showRateCard () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'app/tariff.js' );
		$this->addJs ( 'app/tabel.js' );
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );

		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );

		$data ['activetab'] = "tariff";
		$data ['active_menu'] = 'tariff';

		$data ['driver_tariff_model_list'] = $this->Query_Model->getDriverTariffList ();
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "tariff/manage_driver_tariff", $data );
			echo json_encode ( array (
					'html' => $html
			) );
		} else {
			$html = $this->render ( "tariff/manage_driver_tariff", $data );
		}
	}
}
