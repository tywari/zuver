<?php
class Country extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Country_Model' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/country.js' );
		
		// $this->render("country/add_edit_country", $data);
	}
	public function validationCountry() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'name',
						'label' => 'Country Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[20]' 
				),
				array (
						'field' => 'isoCode',
						'label' => 'ISO Code',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'address',
						'label' => 'Address',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'telephoneCode',
						'label' => 'Telephone Code',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'currencyCode',
						'label' => 'Currency Code',
						'rules' => 'trim|alpha|required' 
				),
				array (
						'field' => 'currencySymbol',
						'label' => 'Currency Symbol',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
		$this->addJs ( 'app/country.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "country";
		$data ['active_menu'] = 'country';
		// $data['city_list']=$this->City_Model->getColumnByKeyValueArray('id,name',array('status'=>'Y'),'name');
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getColumnByKeyValueArray ( 'id,name', array (
				'status' => 'Y' 
		), 'id' );
		$data ['country_model'] = $this->Country_Model->getBlankModel ();
		$this->render ( 'country/add_edit_country', $data );
	}
	public function saveCountry() {
		$this->addJs ( 'app/country.js' );
		$data = array ();
		$data = $this->input->post ();
		$country_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($country_id > 0) {
			$this->Country_Model->update ( $data, array (
					'id' => $country_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			
			$country_id = $this->Country_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		
		// $this->render("country/add_edit_country", $data);
		
		$response = array (
				'msg' => $msg,
				'country_id' => $country_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($country_id, $view_mode) {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/country.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		
		/* $country_id = $this->input->post('id'); */
		
		if ($country_id > 0) {
			// Get release details by release_id
			$data ['country_model'] = $this->Country_Model->getById ( $country_id );
		} else {
			// Blank Model
			$data ['country_model'] = $this->Country_Model->getBlankModel ();
		}
		$data ['activetab'] = "country";
		$data ['active_menu'] = 'country';
		$this->render ( 'country/add_edit_country', $data );
		/*
		 * $html = $this->load->view('country/add_edit_country', $data, TRUE);
		 *
		 * echo json_encode(array(
		 * 'html' => $html
		 *
		 * ));
		 */
	}
	public function getCountryList() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/country.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		// $this->addJs('vendors/jszip/dist/jszip.min.js');
		// $this->addJs('vendors/pdfmake/build/pdfmake.min.js');
		// $this->addJs('vendors/pdfmake/build/vfs_fonts.js');
		
		$data ['activetab'] = "country";
		$data ['active_menu'] = 'country';
		$data ['country_model_list'] = $this->Country_Model->getAll ( 'name' );
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "country/manage_country", $data );
			echo json_encode ( array (
					'html' => $html 
			)
			 );
		} else {
			$html = $this->render ( "country/manage_country", $data );
		}
	}
	public function changeCountryStatus() {
		$data = array ();
		
		$this->addJs ( 'app/country.js' );
		
		$data = $this->input->post ();
		$country_id = $data ['id'];
		$driver_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($country_id) {
			$driver_updated = $this->Country_Model->update ( array (
					'status' => $driver_status 
			), array (
					'id' => $country_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'country_id' => $country_id 
		);
		echo json_encode ( $response );
	}
}