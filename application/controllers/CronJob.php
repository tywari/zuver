<?php
/*
 * created by Aditya on May 27 2018
 * use of this controller is to simply provice a script for Cron to run
 * this controller only includes cron jobs
*/
class CronJob extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ('Driver_Model');
		$this->load->model ('Driver_Shift_History_Model');
		$this->load->model ('Driver_Auto_Shift_Out_Model');
		$this->load->model ('Sms_Template_Model');
		$this->load->model ('Api_Webservice_Model');
	}

	public function index() {

	}
	// how to create a cron job in PHP/Ubuntu
	// crontab -e
	// 0 4 * * * php /var/www/html/index.php CronJob autoOffDuty
	public function autoOffDuty() {
		// this function is to turn drivers off-duty automatically at 04:00 am daily
		// this function should only be accessible through cron/cli not browser
		if($this->input->is_cli_request()) {
			$result = $this->Driver_Model->getAllAvailableOnDutyDriverList();
			$turnedOffDuty = '';
			foreach ($result as $row) {
				$dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $row['shiftCreatedDate']);
				$nowDateTime = new DateTime();
				$interval = $nowDateTime->diff($dateTime);

				$diff = $interval->format('%a');
				// echo $row['id'].', '.
				// 		$row['firstName'].', '.
				// 		$row['lastName'].', '.
				// 		$row['shiftCreatedDate'].', '.
				// 		$diff;
				if ($diff >= 1) {
					// echo ', TURN OFF DUTY';
					// right the code to turn the user off duty
					$shiftStatus = 'OUT';
					$insert_data = array (
							'driverId' => $row['id'],
							'inLatitude' => '',
							'inLongitude' => '',
							'availabilityStatus' => 'F'
						);
					// sets the user as off-duty in driver shift history table
					$driver_shift_insert_id = $this->Driver_Shift_History_Model->insert ( $insert_data );


					$insert_data2 = array (
						'driverId' => $row['id'],
						'sms_sent' => 'No'
					);
					// saving auto shiftedout driver details in table
					// this data will be later on used to send SMS
					$driver_shift_insert_id = $this->Driver_Auto_Shift_Out_Model->insert ( $insert_data2 );
				}
			}
		} else {
			echo 'Only CLI Request accepted';
		}
	}

	// this function will siply fetch list of all the drivers that were auto shifted ouptput
	// and send each of them an SMS at 05:45 AM
	public function sendSMSToAutoShiftedOutDrivers()
	{
		// this function should only be accessible through cron/cli not browser
		if($this->input->is_cli_request()) {
			$drivers = $this->Driver_Auto_Shift_Out_Model->getDriverListForSMS();
			if (!empty($drivers)) {
				// Write code to loop through the array and send SMS 1 at a time
				foreach ($drivers as $row) {
					// try to send SMS only when mobile value is present
					if ($row['mobile'] != '') {
						// try to Send SMS as mobile value is present
						$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
								'smsTitle' => 'driver_auto_shift_out'
						) );
						$message_data = $message_details->smsContent;
						$message_data = str_replace ( "##DRIVERNAME##", $row['firstName'], $message_data );

						$smsResult = $this->Api_Webservice_Model->sendSMS ( $row['mobile'], $message_data );
						// sms result coming from gupshup is in this format
						// msg | mobile | uniqueid
						// succss | 91987654321 | 89713216549843115
						$smsResultArray = explode('|', $smsResult);

						if ($smsResultArray[0] == 'success ') {
							// now update database entry to SMS Sent = Yes
							$data['sms_sent'] = 'Yes';
							$driver_updated = $this->Driver_Auto_Shift_Out_Model->update ( $data, array (
								'id' => $row['id']
							));
						}
					}
				}
			}
		} else {
			echo 'Only CLI Request accepted';
		}
	}

} // end of class
