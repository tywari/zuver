<?php
class Corporatepartners extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Country_Model' );
		$this->load->model ( 'State_Model' );
		$this->load->model ( 'City_Model' );
		$this->load->model ( 'Api_Webservice_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/corporatepartners.js' );
		
		// $this->render("passenger/add_edit_passenger", $data);
	}
	public function validationCorporatePartners() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'name',
						'label' => 'Corporate/Partners Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[50]' 
				),
				array (
						'field' => 'email',
						'label' => 'Email Id',
						'rules' => 'trim|required|valid_email' 
				),
				array (
						'field' => 'address',
						'label' => 'Address',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'description',
						'label' => 'Description',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'url',
						'label' => 'Website URL',
						'rules' => 'trim|alpha_numeric|required' 
				),
				array (
						'field' => 'contactName',
						'label' => 'Contact Person Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[25]' 
				),
				array (
						'field' => 'contactEmail',
						'label' => 'Contact Person Email Id',
						'rules' => 'trim|required|valid_email' 
				),
				
				array (
						'field' => 'contactMobile',
						'label' => 'Contact Person Mobile',
						'rules' => 'trim|required|numeric|exact_length[10]' 
				),
				array (
						'field' => 'companyType',
						'label' => 'Company Type',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'countryId',
						'label' => 'Country Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'stateId',
						'label' => 'State Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'cityId',
						'label' => 'City Name',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showPartners () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/corporatepartners.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "corporatepartners";
		$data ['active_menu'] = 'corporatepartners';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['state_list'] = $this->State_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['company_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'COMPANY_TYPE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		
		$data ['corporatepartners_model'] = $this->Corporate_Partners_Details_Model->getBlankModel ();
		$this->render ( 'corporatepartners/add_edit_corporatepartners', $data );
	}
	public function saveCorporatePartners() {
		$this->addJs ( 'app/corporatepartners.js' );
		$data = array ();
		$data = $this->input->post ();
		$corporate_partner_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($corporate_partner_id > 0) {
			$this->Corporate_Partners_Details_Model->update ( $data, array (
					'id' => $corporate_partner_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			
			$corporate_partner_id = $this->Corporate_Partners_Details_Model->insert ( $data );
			if ($corporate_partner_id > 0) {
				// SMS & email service start
				
				/*
				 * if (SMS && $corporate_partner_id) {
				 * $message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
				 * 'smsTitle' => 'forgot_password_sms'
				 * ) );
				 *
				 * $message_data = $message_details->smsContent;
				 * // $message = str_replace("##USERNAME##",$name,$message);
				 * $message_data = str_replace ( "##PASSWORD##", '', $message_data );
				 * // print_r($message);exit;
				 * if ($data ['mobile'] != "") {
				 *
				 * $this->Api_Webservice_Model->sendSMS ( $data ['mobile'], $message_data );
				 * }
				 * }
				 */
				
				$from = SMTP_EMAIL_ID;
				$to = $data ['email'];
				$subject = 'Company/Partner Registration';
				
				$data = array (
						'company_name' => $data ['name'],
						'company_address' => $data ['address'],
						'company_url' => $data ['url'],
						'person_name' => $data ['contactName'],
						'person_email' => $data ['contactEmail'],
						'person_mobile' => $data ['contactMobile'] 
				);
				$message_data = $this->load->view ( 'emailtemplate/company_partner_register', $data, TRUE );
				
				if (SMTP && $corporate_partner_id) {
					
					if ($to) {
						$this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
					}
				} else {
					
					// To send HTML mail, the Content-type header must be set
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Additional headers
					$headers .= 'From: Zuver<' . $from . '>' . "\r\n";
					$headers .= 'To: <' . $to . '>' . "\r\n";
					if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
						mail ( $to, $subject, $message_data, $headers );
					}
				}
				// SMS & email service end
			}
			$msg = $msg_data ['success'];
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'corporatepartners_id' => $corporate_partner_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($corporatepartners_id, $view_mode) {
		if ($this->User_Access_Model->showPartners () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/corporatepartners.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		
		/* $corporatepartners_id = $this->input->post ( 'id' ); */
		$data ['activetab'] = "corporatepartners";
		$data ['active_menu'] = 'corporatepartners';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['state_list'] = $this->State_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['company_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'COMPANY_TYPE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		
		if ($corporatepartners_id > 0) {
			// Get release details by release_id
			$data ['corporatepartners_model'] = $this->Corporate_Partners_Details_Model->getById ( $corporatepartners_id );
		} else {
			// Blank Model
			$data ['corporatepartners_model'] = $this->Corporate_Partners_Details_Model->getBlankModel ();
		}
		
		$this->render ( 'corporatepartners/add_edit_corporatepartners', $data );
		/*
		 * $html = $this->load->view ( 'corporatepartners/add_edit_corporatepartners', $data, TRUE );
		 *
		 * echo json_encode ( array (
		 * 'html' => $html
		 * ) );
		 */
	}
	public function getCorporatePartnersList() {
		if ($this->User_Access_Model->showPartners () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/corporatepartners.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		// $this->addJs('vendors/jszip/dist/jszip.min.js');
		// $this->addJs('vendors/pdfmake/build/pdfmake.min.js');
		// $this->addJs('vendors/pdfmake/build/vfs_fonts.js');
		
		$data ['activetab'] = "corporatepartners";
		$data ['active_menu'] = 'corporatepartners';
		$data ['company_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'COMPANY_TYPE' ) 
		), 'sequenceOrder' );
		$data ['corporatepartner_model_list'] = $this->Corporate_Partners_Details_Model->getAll ( 'name' );
		if ($get_type == 'ajax_call') {
			$this->load->view ( "corporatepartners/manage_corporatepartners", $data );
		} else {
			$this->render ( "corporatepartners/manage_corporatepartners", $data );
		}
	}
	public function changeCorporatePartnersStatus() {
		$data = array ();
		
		$this->addJs ( 'app/corporatepartners.js' );
		
		$data = $this->input->post ();
		$corporate_partner_id = $data ['id'];
		$driver_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($corporate_partner_id) {
			$driver_updated = $this->Corporate_Partners_Details_Model->update ( array (
					'status' => $driver_status 
			), array (
					'id' => $corporate_partner_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'corporate_partner_id' => $corporate_partner_id 
		);
		echo json_encode ( $response );
	}
}