<?php
class Promocode extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'City_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Trip_Details_Model' );
		$this->load->model ( 'Passenger_Promocode_Details_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		$this->addJs ( 'app/promocode.js' );
		
		// $this->render("driver/add_edit_driver", $data);
	}
	public function validationPromocode() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'passengerId',
						'label' => 'Passengers',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'cityId',
						'label' => 'City Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'promoCode',
						'label' => 'Promocode',
						'rules' => 'trim|required|alpha_numeric|min_length[3]|max_length[15]' 
				),
				array (
						'field' => 'promoDiscountType',
						'label' => 'Discount Type',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'promoStartDate',
						'label' => 'Start Date',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'promoEndDate',
						'label' => 'End Date',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'promoCodeLimit',
						'label' => 'Limit Count',
						'rules' => 'trim|required|integer|min_length[1]|max_length[2]' 
				),
				array (
						'field' => 'promoDiscountAmount',
						'label' => 'Promocode Discount Amount',
						'rules' => 'trim|required|numeric|min_length[1]|max_length[4]' 
				),
				array (
						'field' => 'cityId',
						'label' => 'City Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'companyId',
						'label' => 'Company Name',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showPromoCode () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'datepicker/jquery.datetimepicker.full.js' );
		$this->addJs ( 'app/promocode.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "promocode";
		$data ['active_menu'] = 'promocode';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'id' );
		$data ['promo_discount_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PROMOCODE_TYPE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		$data ['promocode_model'] = $this->Passenger_Promocode_Details_Model->getBlankModel ();
		$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
		$this->render ( 'promocode/add_edit_promocode', $data );
	}
	public function savePromocode() {
		$this->addJs ( 'app/promocode.js' );
		$data = array ();
		$data = $this->input->post ();
		$promocode_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		
		if (array_key_exists ( 'isFreeRide', $data )) {
			$data ['isFreeRide'] = ($data ['isFreeRide']) ? Status_Type_Enum::ACTIVE : Status_Type_Enum::INACTIVE;
		}
		
		if ($promocode_id > 0) {
			$this->Passenger_Promocode_Details_Model->addUpdatePromoCodeDetails ( $data );
			$msg = $msg_data ['updated'];
		} else {
			// @todo unique passenger code
			// $data['passengerCode'] = '';
			$promocode_id = $this->Passenger_Promocode_Details_Model->addUpdatePromoCodeDetails ( $data );
			$msg = $msg_data ['success'];
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'promocode_id' => $promocode_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($promocode_id, $view_mode) {
		if ($this->User_Access_Model->showPromoCode () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/passenger.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		$data ['activetab'] = "promocode";
		$data ['active_menu'] = 'promocode';
		if ($this->input->post ( 'id' )) {
			$promocode_id = $this->input->post ( 'id' );
		}
		if ($promocode_id > 0) {
			// Get release details by release_id
			$data ['promocode_model'] = $this->Passenger_Promocode_Details_Model->getById ( $promocode_id );
			$passenger_id = $data ['promocode_model']->passengerId;
			// $passenger_id = $data ['promocode_model']
			$data ['passenger_model'] = $this->Passenger_Model->getById ( $passenger_id );
		} else {
			// Blank Model
			$data ['promocode_model'] = $this->Passenger_Promocode_Details_Model->getBlankModel ();
		}
		
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'id' );
		$data ['promo_discount_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PROMOCODE_TYPE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		
		if ($this->input->post ( 'id' )) {
			$html = $this->load->view ( 'promocode/add_edit_promocode', $data, TRUE );
			
			echo json_encode ( array (
					'html' => $html 
			)
			 );
		} else {
			$this->render ( 'promocode/add_edit_promocode', $data );
		}
	}
	public function getPromocodeList() {
		if ($this->User_Access_Model->showPromoCode () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'app/promocode.js' );
		$this->addJs ( 'app/tabel.js' );
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'datepicker/jquery.datetimepicker.full.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "promocode";
		$data ['active_menu'] = 'promocode';
		
		$company_id = $this->session->userdata ( 'user_company' );
		
		$city_id = $this->session->userdata ( 'user_city' );
		if ($this->input->post ( 'city_id' )) {
			$city_id = $this->input->post ( 'city_id' );
		}
		$data ['promocode_model_list'] = $this->Query_Model->getPromocodeList ( $city_id );
		
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "promocode/manage_promocode", $data );
			echo json_encode ( array (
					'html' => $html 
			)
			 );
		} else {
			$html = $this->render ( "promocode/manage_promocode", $data );
		}
	}
	public function changePromocodeStatus() {
		$data = array ();
		
		$this->addJs ( 'app/promocode.js' );
		
		$data = $this->input->post ();
		$promocode_id = $data ['id'];
		$driver_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($promocode_id) {
			$driver_updated = $this->Passenger_Promocode_Details_Model->update ( array (
					'status' => $driver_status 
			), array (
					'id' => $promocode_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		
		$response = array (
				'msg' => $msg,
				'promocode_id' => $promocode_id 
		);
		echo json_encode ( $response );
	}
	public function checkPromocodeAvailabilty() {
		$data = array ();
		
		$data = $this->input->post ();
		$promocode = $data ['promoCode'];
		$promocode_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['promocode_not_exists'];
		if ($promocode) {
			$promocode_exists = $this->Passenger_Promocode_Details_Model->getOneByKeyValueArray ( array (
					'promoCode' => $promocode 
			) );
			if ($promocode_exists) {
				$status = 1;
				$msg = $msg_data ['promocode_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $promocode,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
}