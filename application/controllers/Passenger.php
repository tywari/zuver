<?php
class Passenger extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ('form');
		$this->load->model ('Passenger_Model');
		$this->load->model ('Passenger_Emergency_Contact_Model');
		$this->load->model ('Auth_Key_Model');
		$this->load->model ('City_Model');
		$this->load->model ('Api_Webservice_Model');
		$this->load->model ('Query_Model');
		$this->load->model ('Sms_Template_Model');
		$this->load->model ('Corporate_Partners_Details_Model');
		$this->load->model ('Data_Attributes_Model');
		$this->load->library('datatables');
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/passenger.js' );
		
		// $this->render("passenger/add_edit_passenger", $data);
	}
	public function validationPassenger() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'firstName',
						'label' => 'First Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[15]' 
				),
				array (
						'field' => 'lastName',
						'label' => 'Last Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[15]' 
				),
				array (
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email' 
				),
				array (
						'field' => 'mobile',
						'label' => 'Mobile',
						'rules' => 'trim|required|numeric|exact_length[10]' 
				),
				array (
						'field' => 'cityId',
						'label' => 'City Name',
						'rules' => 'trim|alpha|required' 
				),
				array (
						'field' => 'companyId',
						'label' => 'Company Name',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showPassenger () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/passenger.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "passenger";
		$data ['active_menu'] = 'passenger';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'id' );
		$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
		$data ['passenger_emergency_model'] = array ();
		$this->render ( 'passenger/add_edit_passenger', $data );
	}
	/**
	 */
	public function savePassenger() {
		$this->addJs ( 'app/passenger.js' );
		$data = array ();
		$data = $this->input->post ();
		$passenger_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($passenger_id > 0) {
			$this->Passenger_Model->update ( $data, array (
					'id' => $passenger_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			$password = random_string ( 'alnum', 8 );
			// @todo encrypted password
			$data ['password'] = hash ( "sha256", $password );
			// @todo generate 6 digit otp
			$data ['otp'] = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			// @todo unique passenger code
			
			$data ['passengerCode'] = trim ( substr ( strtoupper ( str_replace ( ' ', '', $data ['firstName'] . '' . $data ['lastName'] ) ), 0, 5 ) );
			$data ['passengerCode'] .= $this->Passenger_Model->getAutoIncrementValue ();
			$data ['activationStatus'] = Status_Type_Enum::ACTIVE;
			$data ['signupFrom'] = Signup_Type_Enum::BACKEND;
			$data ['registerType'] = Register_Type_Enum::MANUAL;
			$passenger_id = $this->Passenger_Model->insert ( $data );
			$msg = $msg_data ['success'];
			if ($passenger_id > 0) {
				// SMS & email service start
				
				if (SMS && $passenger_id) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'smsTitle' => 'account_create_sms' 
					) );
					
					$message_data = $message_details->smsContent;
					$web_link = array ();
					$web_link [0] = substr ( APPLINK, 0, 20 );
					$web_link [1] = substr ( APPLINK, 20, 20 );
					$web_link [2] = substr ( APPLINK, 40, 20 );
					$web_link [3] = substr ( APPLINK, 60, 20 );
					$web_link [4] = substr ( APPLINK, 80, 20 );
					$web_link [5] = substr ( APPLINK, 100, 20 );
					// $address[6]=substr($current_location,120,20);
					// $address[7]=substr($current_location,140,20);
					
					if (strlen ( APPLINK ) > 120) {
						$web_link [5] = cut_string_using_last ( ',', $web_link [5], 'left', false );
					}
					$message_data = str_replace ( "##USERNAME##", $data ['mobile'], $message_data );
					$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
					$message_data = str_replace ( "##APPLINK1##", $web_link [0], $message_data );
					$message_data = str_replace ( "##APPLINK2##", $web_link [1], $message_data );
					$message_data = str_replace ( "##APPLINK3##", $web_link [2], $message_data );
					$message_data = str_replace ( "##APPLINK4##", $web_link [3], $message_data );
					$message_data = str_replace ( "##APPLINK5##", $web_link [4], $message_data );
					$message_data = str_replace ( "##APPLINK6##", $web_link [5], $message_data );
					$message_data = str_replace ( "##APPLINK7##", '', $message_data );

					// print_r($message);exit;
					if ($data ['mobile'] != "") {
						
						$this->Api_Webservice_Model->sendSMS ( $data ['mobile'], $message_data );
					}
				}
				
				$from = SMTP_EMAIL_ID;
				$to = $data ['email'];
				$subject = 'Get Ready to ride with Zuver!';
				$data = array (
						'password' => $password,
						'user_name' => $data ['firstName'] . ' ' . $data ['lastName'],
						'mobile' => $data ['mobile'],
						'email' => $data ['email'] 
				);
				$message_data = $this->load->view ( 'emailtemplate/passenger_register', $data, TRUE );
				
				if (SMTP && $passenger_id) {
					
					if ($to) {
						$this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
					}
				} else {
					
					// To send HTML mail, the Content-type header must be set
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Additional headers
					$headers .= 'From: Zuver<' . $from . '>' . "\r\n";
					$headers .= 'To: <' . $to . '>' . "\r\n";
					if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
						mail ( $to, $subject, $message_data, $headers );
					}
				}
				// SMS & email service end
			}
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'passenger_id' => $passenger_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($passenger_id, $view_mode) {
		if ($this->User_Access_Model->showPassenger () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/passenger.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		if ($this->input->post ( 'id' )) {
			$passenger_id = $this->input->post ( 'id' );
		}
		$data ['activetab'] = "passenger";
		$data ['active_menu'] = 'passenger';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'id' );
		
		if ($passenger_id > 0) {
			// Get release details by release_id
			$data ['passenger_model'] = $this->Passenger_Model->getById ( $passenger_id );
			$data ['passenger_emergency_model'] = $this->Passenger_Emergency_Contact_Model->getByKeyValueArray ( array (
					'passengerId' => $passenger_id 
			), 'id' );
		} else {
			// Blank Model
			$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
			$data ['passenger_emergency_model'] = array ();
		}
		
		if ($this->input->post ( 'id' )) {
			$html = $this->load->view ( 'passenger/add_edit_passenger', $data, TRUE );
			
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'passenger/add_edit_passenger', $data );
		}
	}
	public function getPassengerList() {
		if ($this->User_Access_Model->showPassenger () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'app/passenger.js' );
		$this->addJs ( 'app/tabel.js' );
		
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$city_id = $this->session->userdata ( 'user_city' );
		$post_data = $this->input->post ();
		if (array_key_exists ( 'city_id', $post_data )) {
			$city_id = $post_data ['city_id'];
		}
		
		$data ['activetab'] = "passenger";
		$data ['active_menu'] = 'passenger';
		$data ['passenger_model_list'] = $this->Query_Model->getPassengerList ( $city_id );
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "passenger/manage_passenger", $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$html = $this->render ( "passenger/manage_passenger", $data );
		}
	}

	public function generateAuth()
	{
		$secretKey='globalkk';
		$booking_key='JPS6V96BA';
		echo $auth_key=base64_encode($booking_key.'|'.$secretKey);
	}

	public function getPassengersAjax()
	{
		if ($this->User_Access_Model->showPassenger () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$passenger_list=array();
		$this->load->library('Datatables');
		$city_id = $this->session->userdata ('user_city');
		$post_data = $this->input->post();
		if (array_key_exists ( 'city_id', $post_data )) {
			$city_id = $post_data ['city_id'];
		}
		
		//$data ['activetab'] = "passenger";
		//$data ['active_menu'] = 'passenger';
		
		$limit = $this->input->post('length');
        $start = $this->input->post('start');
        $searchValue = $this->input->post('search[value]');
        $searchRegex = $this->input->post('search[regex]');
		$draw = $this->input->post('search[draw]');
		$columnMap = array('id','firstName','email','mobile','walletAmount','totalTripComplete','rating','lastLoggedIn','signupFrom','registerType');
        $orderColumnId =$this->input->post('order[0][column]');
        $orderDir = $this->input->post('order[0][dir]');
        $orderColumnName = $columnMap[$orderColumnId];
        $order_by= $orderColumnName.' '.$orderDir;
               
        $passenger_model_count = $this->Query_Model->getPassengerCount($city_id,NULL,$searchValue);
		$passenger_model_list = $this->Query_Model->getPassengerList($city_id,NULL, $start, $limit, $searchValue, $order_by);
		$data["recordsTotal"] = $passenger_model_count;
		$data["recordsFiltered"]= $passenger_model_count;
		$data["data"] = array();

			foreach ($passenger_model_list as $passenger) {
				$passenger_data=array();
				$passenger_data['actions']="<a href='javascript:void(0)' id='viewPassenger-".$passenger->id."' onclick='viewPassenger(this.id)' class='userlink' title='View Passenger' ><span class='glyphicon glyphicon-eye-open'></span></a> / <a href='javascript:void(0)' id='editPassenger-".$passenger->id."' onclick='editPassenger(this.id)' class='userlink' title='Edit Passenger' ><span class='glyphicon glyphicon-edit'></span></a> / <a href='javascript:void(0)' id='editPassenger-".$passenger->id."' onclick='transctionHistory(this.id)' class='userlink' title='Transcation Hisory'><i class='fa fa-inr></i></span></a>";

				$passenger_data['name']=$passenger->firstName.' '.$passenger->lastName;
				$passenger_data['email']=$passenger->email;
				$passenger_data['mobile']=$passenger->mobile;
				$passenger_data['walletAmount']=$passenger->walletAmount;
				
				$passenger_data['totalTripComplete']=$passenger->totalTripComplete;
				$passenger_data['rating']=round($passenger->rating,1);
				$passenger_data['lastLoggedIn']=$passenger->lastLoggedIn;
				$passenger_data['signupFrom']=$passenger->signupFrom;
				$passenger_data['registerType']=$passenger->registerType;
				$passenger_data['DT_RowId'] = "row_".$passenger->id;
            	$passenger_data['DT_RowData'] = array('passengerId'=>$passenger->id);
				//Status toogle
				$active=($passenger->status==Status_Type_Enum::ACTIVE)?"btn-primary active-status":"btn-default onclick='changePassengerStatus(this.id)'";
				$deactive=($passenger->status==Status_Type_Enum::INACTIVE)?'btn-primary active-status':'btn-default"  onclick="changePassengerStatus(this.id)';
				$passenger_data['status']="<ul class='list-inline'> <li><div class='btn-group btn-toggle'><button id='active-".$passenger->id."' class='btn btn-xs ".$active."'>Active</button><button id='deactive-".$passenger->id."' class='btn btn-xs ".$deactive."'>Deactive</button></li></ul>";
				//$passenger_list[]=$passenger_data;
				$data["data"][] = $passenger_data;
			}
			//print_r($passenger_list); die();
			/*$json_data = array(
                "draw"            => intval($draw),
                "recordsTotal"    => intval( $passenger_model_count ),
                "recordsFiltered" => intval( $passenger_model_count ),
                "data"            => $passenger_list
            );*/
			echo json_encode($data);
			//return json_encode($passenger_list);
	}
	public function changePassengerStatus() {
		$data = array ();
		
		$this->addJs ( 'app/passenger.js' );
		
		$data = $this->input->post ();
		$passenger_id = $data ['id'];
		$passenger_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($passenger_id) {
			$passenger_updated = $this->Passenger_Model->update ( array (
					'status' => $passenger_status 
			), array (
					'id' => $passenger_id 
			) );
			if ($passenger_status == Status_Type_Enum::INACTIVE) {
				$auth_key_exists = $this->Auth_Key_Model->getOneByKeyValueArray ( array (
						'userId' => $passenger_id,
						'userType' => User_Type_Enum::PASSENGER 
				) );
				// If user already exists update auth key
				if ($auth_key_exists) {
					$this->Auth_Key_Model->update ( array (
							'key' => '' 
					), array (
							'userId' => $passenger_id,
							'userType' => User_Type_Enum::PASSENGER 
					) );
				}
				if ($passenger_id > 0) {
					// SMS & email service start
					$passenger_details = $this->Passenger_Model->getById ( $passenger_id );
					// @todo SMS template to be changed later
					if (SMS && $passenger_updated) {
						$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
								'smsTitle' => 'forgot_password_sms' 
						) );
						
						$message_data = $message_details->smsContent;
						// $message = str_replace("##USERNAME##",$name,$message);
						// $message_data = str_replace ( "##PASSWORD##", $password, $message_data );
						// print_r($message);exit;
						if ($passenger_details->mobile != "") {
							
							// $this->Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
						}
					}
					
					$from = SMTP_EMAIL_ID;
					$to = $passenger_details->email;
					$subject = 'Passenger De-activated';
					$data = array (
							'user_name' => $passenger_details->firstName . ' ' . $passenger_details->lastName,
							'mobile' => $passenger_details->mobile,
							'email' => $passenger_details->email 
					);
					$message_data = $this->load->view ( 'emailtemplate/passenger_inactive', $data, TRUE );
					
					if (SMTP && $passenger_updated) {
						
						if ($to) {
							$this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
						}
					} else {
						
						// To send HTML mail, the Content-type header must be set
						$headers = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						// Additional headers
						$headers .= 'From: Zuver<' . $from . '>' . "\r\n";
						$headers .= 'To: <' . $to . '>' . "\r\n";
						if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
							mail ( $to, $subject, $message_data, $headers );
						}
					}
					// SMS & email service end
				}
			}
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'passenger_id' => $passenger_id 
		);
		echo json_encode ( $response );
	}
	public function checkPassengerEmail() {
		$data = array ();
		
		$data = $this->input->post ();
		$email = $data ['email'];
		$email_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['email_not_exists'];
		if ($email) {
			$email_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'email' => $email 
			) );
			if ($email_exists) {
				$status = 1;
				$msg = $msg_data ['email_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $email,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
	public function checkPassengerMobile() {
		$data = array ();
		
		$data = $this->input->post ();
		$mobile = $data ['mobile'];
		$mobile_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['mobile_not_exists'];
		if ($mobile) {
			$mobile_exists = $this->Passenger_Model->getOneByKeyValueArray ( array (
					'mobile' => $mobile 
			) );
			if ($mobile_exists) {
				$status = 1;
				$msg = $msg_data ['mobile_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $mobile,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
}