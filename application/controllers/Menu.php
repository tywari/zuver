<?php
class Menu extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Menu_Content_Details_Model' );
		
		$this->load->model ( 'Query_Model' );
		
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/menu.js' );
		
		// $this->render("passenger/add_edit_passenger", $data);
	}
	public function validationMenu() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'name',
						'label' => 'Menu Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[20]' 
				),
				array (
						'field' => 'title',
						'label' => 'Menu Title',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'address',
						'label' => 'Address',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'telephoneCode',
						'label' => 'Telephone Code',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'currencyCode',
						'label' => 'Currency Code',
						'rules' => 'trim|alpha|required' 
				),
				array (
						'field' => 'currencySymbol',
						'label' => 'Currency Symbol',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/menu.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "menu";
		$data ['active_menu'] = 'menu';
		
		$data ['menu_list'] = $this->Menu_Content_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['menu_model'] = $this->Menu_Content_Details_Model->getBlankModel ();
		$this->render ( 'menu/add_edit_menu', $data );
	}
	public function saveMenu() {
		$this->addJs ( 'app/menu.js' );
		$data = array ();
		$data = $this->input->post ();
		$menu_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($menu_id > 0) {
			$this->Menu_Content_Details_Model->update ( $data, array (
					'id' => $menu_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			$password = $data [email] . "_" . $data ['mobile'];
			// @todo encrypted password
			$data ['password'] = hash ( "sha256", $password );
			// @todo generate 6 digit otp
			$data ['otp'] = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			// @todo unique passenger code
			$data ['passengerCode'] = '';
			
			$passenger_id = $this->Menu_Content_Details_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'passenger_id' => $passenger_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($menu_id, $view_mode) {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/menu.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		if ($this->input->post ( 'id' )) {
			$menu_id = $this->input->post ( 'id' );
		}
		$data ['activetab'] = "menu";
		$data ['active_menu'] = 'menu';
		$data ['menu_list'] = $this->Menu_Content_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		
		if ($menu_id > 0) {
			// Get release details by release_id
			$data ['menu_model'] = $this->Menu_Content_Details_Model->getById ( $menu_id );
		} else {
			// Blank Model
			$data ['menu_model'] = $this->Menu_Content_Details_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {
			
			$html = $this->load->view ( 'menu/add_edit_menu', $data, TRUE );
			
			echo json_encode ( array (
					'html' => $html 
			)
			 );
		} else {
			$this->render ( 'menu/add_edit_menu', $data );
		}
	}
	public function getMenuList() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'app/menu.js' );
		$this->addJs ( 'app/tabel.js' );
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "menu";
		$data ['active_menu'] = 'menu';
		$data ['menu_model_list'] = $this->Menu_Content_Details_Model->getAll ( 'id' );
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "menu/manage_menu", $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$html = $this->render ( "menu/manage_menu", $data );
		}
	}
	public function changeMenuStatus() {
		$data = array ();
		
		$this->addJs ( 'app/menu.js' );
		
		$data = $this->input->post ();
		$menu_id = $data ['id'];
		$menu_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($menu_id) {
			$driver_updated = $this->Menu_Content_Details_Model->update ( array (
					'status' => $menu_status 
			), array (
					'id' => $menu_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'menu_id' => $menu_id 
		);
		echo json_encode ( $response );
	}
}