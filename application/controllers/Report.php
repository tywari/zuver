<?php
class Report extends My_Controller
{
	function __construct()
	{
		
		parent::__construct();
		is_logged_in();
		$this->load->helper('form');
		$this->load->model('Query_Model');
		$this->load->model('Corporate_Partners_Details_Model');
		$this->load->model('Data_Attributes_Model');
		$this->load->model('City_Model');
		$this->load->model('Driver_Model');
		$this->load->helper ('excel_generate_helper');
		$this->load->model('User_Access_Model');
		
               
	}
	
	public function index()
	{
		//$data = array();
		$this->addJs('app/report.js');
		$this->addJs('app/tabel.js');
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ('vendors/datatables.net/js/buttons.min.js' );
		$this->addJs('vendors/datatables.net-buttons/js/buttons.flash.min.js');
		//$this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs('vendors/jszip/dist/jszip.min.js');
		$this->addJs('vendors/pdfmake/build/pdfmake.min.js');
		$this->addJs('vendors/datatables.net/js/vfs_fonts.js');
		$this->addJs ('vendors/datatables.net/js/buttons.html5.min.js');
		$this->addJs('vendors/datatables.net-responsive/js/dataTables.responsive.min.js');
		$this->addJs('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js');
		$this->addJs('vendors/datatables.net-scroller/js/datatables.scroller.min.js');
		
		
		$data = array (
				'mode' => EDIT_MODE
		);
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'id' );
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['payment_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['trip_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),'isVisible' => 'Y'
		), 'sequenceOrder' );
		$this->render ( "report/report_layout", $data );
		
	}
	
	public function getCompleteTripReportLayout()
	{
		//$data = array();
		$this->addJs('app/report.js');
		
		$this->addJs('app/tabel.js');
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		//$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data = array (
				'mode' => EDIT_MODE
		);
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'id' );
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['payment_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['trip_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),'isVisible' => 'Y'
		), 'sequenceOrder' );
		$this->render ( "report/complete_trip_report_layout", $data );
	
	}
	
	public function getCancelledTripReportLayout()
	{
		//$data = array();
		$this->addJs('app/report.js');
		
		$this->addJs('app/tabel.js');
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		//$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data = array (
				'mode' => EDIT_MODE
		);
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'id' );
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['payment_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['trip_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),'isVisible' => 'Y'
		), 'sequenceOrder' );
		$this->render ( "report/cancelled_trip_report_layout", $data );
	
	}
	
	public function getTripStatusReportLayout()
	{
		//$data = array();
		$this->addJs('app/report.js');
		
		$this->addJs('app/tabel.js');
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		//$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data = array (
				'mode' => EDIT_MODE
		);
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'id' );
		$data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_TYPE' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['payment_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'PAYMENT_MODE' ),'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['trip_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRIP_STATUS' ),'isVisible' => 'Y'
		), 'sequenceOrder' );
		$this->render ( "report/trip_status_report_layout", $data );
	
	}
	public function getCompletedTripReports()
	{
		$this->addJs('app/report.js');
		
		
		$post_data=$this->input->post();
		
		$data['complete_trip_report_list']=array();
		if ($post_data)
		{
		$data['complete_trip_report_list']=$this->Query_Model->getCompletedTripReports($post_data['tripType'],$post_data['paymentMode'],$post_data['cityId'],$post_data['companyId'],NULL,$post_data['dateFilterType'],$post_data['startDate'],$post_data['endDate']);
		}
		
			$html = $this->load->view ( "report/complete_trip_report", $data,TRUE );
			echo json_encode ( array (
					'html' => $html
			) );
		
	}
	
	public function getCancelledTripReports()
	{
		$this->addJs('app/report.js');
		
		$post_data=$this->input->post();
	
		$data['cancelled_trip_report_list']=array();
		if ($post_data)
		{
			$data['cancelled_trip_report_list']=$this->Query_Model->getCancelledTripReports($post_data['tripType'],$post_data['tripStatus'],$post_data['cityId'],$post_data['companyId'],$post_data['dateFilterType'],$post_data['startDate'],$post_data['endDate']);
		}
	
		$html = $this->load->view ( "report/cancelled_trip_report", $data,TRUE );
		echo json_encode ( array (
				'html' => $html
		) );
	
	}
	
	public function getTripStatusReports()
	{
		$this->addJs('app/report.js');
		
		
		$post_data=$this->input->post();
		
		$data['trip_status_report_list']=array();
		if ($post_data)
		{
			$data['trip_status_report_list']=$this->Query_Model->getTripStatusReports($post_data['tripType'],$post_data['paymentMode'],$post_data['tripStatus'],$post_data['cityId'],$post_data['companyId'],$post_data['dateFilterType'],$post_data['startDate'],$post_data['endDate']);
		}
		
		$html = $this->load->view ( "report/trip_status_report", $data,TRUE );
		echo json_encode ( array (
				'html' => $html
		) );
	}

	public function getShiftDriverReportLayout()
	{
		//$data = array();
		$this->addJs('app/report.js');
		$data = array (
				'mode' => EDIT_MODE
		);
		$data ['activetab'] = "report";
		$data ['active_menu'] = 'report';
		if($this->input->post())
		{
			 $post_data=$this->input->post();
			 download_shift_drivers($post_data['shiftStartDatetime'], $post_data['shiftEndDatetime']);
		}
		$this->render ( "report/shift_driver_report_layout", $data );
	}
	
}