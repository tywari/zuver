<?php
class Admin extends My_Controller
{
	function __construct()
	{
		
		parent::__construct();
		is_logged_in();
		$this->load->helper('form');
		$this->load->model('Query_Model');
		
	}
	
	public function index()
	{
		$data = array();
		$data['activetab']="admin";
		$data['active_menu'] = 'admin';
		
		//$this->addJs('admin-home.js');
		$this->addJs('app/admin/admin.js');
		$this->addJs('app/admin/settings.js');
		
		$this->render("admin/index", $data);
	}
	
	public function locks()
	{
		$this->load->model('Locks_Model');
		$this->load->model('Releases_Model');
		$this->load->model('Project_Bhu_Model');

		$data = array();
      
		$locks_model_rows = $this->Query_Model->getLockDetails();
		
		if($locks_model_rows>0){
	        foreach($locks_model_rows as $row)
	        {
	        	$user_info = User_Model::getMozaicUser($row->last_updated_by_user_id);
	        	$row->user_name = $user_info->first_name." ".$user_info->last_name;
	        	
	        	$minutes = (time() - strtotime($row->last_updated))/60;
				$hours = floor($minutes/60);
				$minutes = floor($minutes % 60);
				if ($hours > 0)
					$age = $hours.' hour'.($hours != 1 ? 's' : '').', '.$minutes.' minute'.($minutes != 1 ? 's' : '');
				else
					$age = $minutes.' minute'.($minutes != 1 ? 's' : '');
	        	
	        	$row->age= $age;
	        	$data['locks'][]=$row;
	        }
		}
		$html = $this->load->view('admin/locks', $data , TRUE );
		$response = array(
            'html'                    => $html
        );
        echo json_encode($response);
        //$this->render('admin/locks',$data);
	}

	public function tags()
	{
		$this->load->model('Tags_Model');
		$this->load->model('Release_Tags_Model');
		$data = array();
		
		$data['tags'] = $this->Tags_Model->getAll();
		$html = $this->load->view('admin/tags', $data , TRUE );
		$response = array(
            'html'                    => $html
        );
        echo json_encode($response);		
		//$this->render('admin/tags',$data);
	}
	
	
	public function settings()
	{
		
		$data = array();
		
		$html = $this->load->view('admin/settings', $data , TRUE );
		
		$response = array(
            'html'                    => $html
        );
        echo json_encode($response);		
		//$this->render('admin/settings',$data);
		
	}
	
	public function clear_apc()
	{
		$this->load->library('../cache/cachedata');
		$this->cachedata->clearAllCache();
		$response = array(
			'html'                    => 'Cleared APC Cache'	
    	);
    	echo json_encode($response);
        //echo "Called apc_clear_cache()";	
	}
	public function archiveYear()
	{
		$year = $this->input->post('year');
		
		$this->load->model('Query_Model');
		$this->load->model('Releases_Model');
		$this->load->model('Status_Report_Model');
        $this->load->model('Fiscal_Year_Model');
        $flag = FALSE;
        $fiscal_year_row = $this->Fiscal_Year_Model->getByKeyValue('year',$year);
        $year_id = $fiscal_year_row[0]->id;

		//$project_bhu_model = $this->Project_Bhu_Model->getByKeyValue('fiscal_year_id',$year_id);
		$release_ids = $this->Query_Model->getReleaseIdsForArchive($year_id);
		$release_archive = array();
		$release_models = array();
		//$release_model = $this->Releases_Model->getByKeyValue('is_archived',FALSE);
		foreach($release_ids as $row)
		{
			$status_model = $this->Status_Report_Model->getLatestStatusReport($row->id);
			if($status_model->release_status_id==Release_Status_Enum::CANCELED || $status_model->release_status_id==Release_Status_Enum::COMPLETED)
			{
				$release_models[]=$this->Releases_Model->getById($row->id);
			}	
		}
		$auth_user = User_Model::getAuthUser();
		
		if(count($release_models)>0){
			foreach($release_models as $row)
			{
				$row->is_archived=TRUE;
				$row->archived_date = getCurrentDate();
				$row->archived_by_user_id=$auth_user->get('dsid');;
				$row=get_object_vars($row);
				unset($row['_table']);
				$release_archive[]=$row;	
			}
			$flag = $this->Releases_Model->batch_update($release_archive);
		}	
		$response = array(
			'flag'=>$flag
    	);
    	echo json_encode($response);
	}
	public function getBhuReleaseAttr()
	{
		$selected_table = $this->input->post('selected_table');
		$fiscal_year = $this->input->post('fiscal_year');
		$capability_area_id = $this->input->post('capability_area');
		$html_table = '';
		$select_list='';
		//$fiscal_year = '';
		$this->load->model('Fiscal_Year_Model');
		$this->load->model('Capability_Focus_Area_Model');
		switch($selected_table)
		{
			case 'BENEFIT':
			case 'GFSS_IMPACT':
			case 'GEO_IMPACT':
			case 'CROSS_FUNCTIONAL_IMPACT':
				$query_result = $this->Query_Model->getReleaseBenefitsCfiGiCountData($selected_table);
				$html_table = $this->prepTableData($selected_table,$query_result);
				break;
			case 'FUNDED_BY':
				$query_result = $this->Query_Model->getFundedByCountData($selected_table);
				$html_table = $this->prepTableData($selected_table,$query_result);
				break;
			case 'capability_focus_area':
				if(empty($fiscal_year)){
					$select_list='<label> Select Fiscal Year </label>';
					$select_list.=form_dropdown('fiscal_year', $this->Fiscal_Year_Model->getSelectDropdownOptions(),NULL,'id="cpfa_fiscal_year" class="js-status-changer bs-select-hidden"');
				}else{
					$query_result = $this->Query_Model->getCapabilityFocusAreaCountData($fiscal_year);
					$html_table = $this->prepTableData($selected_table,$query_result);	
				}
				break;
			case 'focus_area':
				if(empty($capability_area_id)){
					$query_result = $this->Query_Model->getCapabilityAreaOptions();
					foreach($query_result as $row)
					{
						$options[$row->id]=$row->area.' : '.$row->year;
					}
					$select_list='<label> Select Capability Area </label>';
					$select_list.=form_dropdown('capability_area', $options,NULL,'id="capability_area" class="js-status-changer bs-select-hidden"');	
				}
				else{
					$query_result = $this->Query_Model->getFocusAreaForCapArea($capability_area_id);
					$html_table = $this->prepTableData($selected_table,$query_result);
				}	
				break;	
		}
		
		$response = array(
			'table_data' => $html_table,
			'select_list'=> $select_list
    	);
    	echo json_encode($response);
	}
 	
	public function prepTableData($selected_table,$query_result)
	{
		$html_table = '<form id="settingsTable">';
		$html_table.= '<table class="">';
		$html_table.='<thead><tr class="purple">';
		$html_table.='<th>id</th><th>'.strtolower($selected_table).'</th>';
 
		$template_row = '<tr class="template"><td><input type="text" style="display:none;" value="-1" name=id[]></td>';
		$template_row.='<td><input type="text" name="desc[]"></td>';
		if($selected_table=='BENEFIT')
		{
			$html_table.='<th>visible</th>';
			$template_row.='<td><input type="text" name="visible[]" id="visible"></td>';
		}
		elseif($selected_table=='capability_focus_area'){
			//$html_table.='<th>fiscal_year</th>';
			//$template_row.='<td><input type="text" name="fiscal_year"></td>';
			$template_row.='<td></td>';
			$html_table.='<th>focuses</th>';
		}

		$html_table.='<th>count</th><th><span id="admin-add" class="glyphicon glyphicon-plus pull-right" aria-hidden="true"></span></th>';
		$template_row.='<td></td><td><span class="glyphicon glyphicon-remove pull-right" onclick="removeTableRow(this);" id="delete-approval-obtained"></span></td>';
		$html_table.='</tr></thead><tbody>';
		foreach($query_result as $row)
		{
			$html_table.='<tr>';
			$html_table.='<td><input type="text" name="id[]" style="display:none" value="'.$row->id.'">'.$row->id.'</td>';
			$html_table.='<td><input type="text" name="desc[]" value="'.$row->description.'"></td>';
			
			if($selected_table=='BENEFIT'){
				$html_table.='<td><input type="text" name="visible[]" value="'.$row->visible.'"></td>';
			}
			elseif($selected_table=='capability_focus_area'){
				//$html_table.='<th>'.$row->fiscal_year.'</th>';
				/* dummy property - since allow_delete doesn't exist in capability_focus_area */
				$row->allow_delete=TRUE;
				$html_table.='<td>'.$row->focuses.'</td>';
			}elseif($selected_table=='focus_area'){
				/* dummy property - since allow_delete doesn't exist in capability_focus_area */
				$row->allow_delete=TRUE;				
			}

			if($row->count==0 && $row->allow_delete==TRUE )
				$html_table.='<td>'.$row->count.'</td><td><span class="glyphicon glyphicon-remove pull-right" onclick="removeTableRow(this);" id="delete-approval-obtained"></span></td>';
			else
				$html_table.='<td>'.$row->count.'</td>';
					
			$html_table.='</tr>';
		}
			
		$html_table.=$template_row.'<input type="text" style="display:none;" name="table_name" value="'.$selected_table.'"></tbody></table></form>';
		$html_table.='<input id="admin-settings-save" class="btn btn-default pull-right" type="button" value="Save">';
		return $html_table;
	}
	/*
	 * 
	 */
	public function deleteLockTags()
	{
		$this->load->model('Locks_Model');
		$this->load->model('Tags_Model');
		$this->load->model('Release_Tags_Model');
		
		$table_name = $this->input->post('table_name');
		$id = $this->input->post('id');
		if($table_name=='tags')
		{
			$release_tags_rows = $this->Release_Tags_Model->getByKeyValue('tag_id',$id);
			if(count($release_tags_rows)==0){
				$where_cond = array('id'=>$id);
				$this->Tags_Model->delete($where_cond);						
			}
			$this->tags();
		}
		elseif($table_name=='locks')
		{
			$where_cond = array('id'=>$id);
			$this->Locks_Model->delete($where_cond);
			$this->locks();
		}
	}
	/*
	 * 
	 */
	public function saveTableData()
	{
        $data = $this->input->post();
        parse_str($this->input->post('tableData'),$data);
        $table_name = $data['table_name'];
        $ids = $data['id'];
        $desc = $data['desc'];
        $visible='';
		if(isset($data['visible']))
			$visible = $data['visible'];
        //debug($data);

        switch($table_name)
        {
			case 'BENEFIT':
			case 'GFSS_IMPACT':
			case 'GEO_IMPACT':
			case 'CROSS_FUNCTIONAL_IMPACT':
			case 'FUNDED_BY':
				$this->updateBhuReleaseAttr($desc,$ids,$table_name,$visible);
				break;
			case 'capability_focus_area':
				$this->updateCapabilityFocusArea($desc,$ids,$table_name);
				break;
			case 'focus_area':
				$cap_id=$this->input->post('capability_area_id');
				$this->updateCapabilityFocusArea($desc,$ids,$table_name,$cap_id);
				break;
        }
	}
	public function updateCapabilityFocusArea($desc,$ids,$table_name,$cap_id='')
	{
		$this->load->model('Capability_Focus_Area_Model');
		$this->load->model('Fiscal_Year_Model');
		$year = $this->Fiscal_Year_Model->getFiscalYear();
		$i=0;
		foreach($desc as $row)
		{
			if(!empty($row) && $ids[$i]==-1)
			{
				$insert_data = array(
					'area'=>$row
				);
				if($table_name=='focus_area')
				{
					$insert_data['capability_focus_area_id']=$cap_id;
				}else{
					$insert_data['fiscal_year_id']=$year->id;
				}
				array_push($ids,$this->Capability_Focus_Area_Model->insert($insert_data));
			}elseif(!empty($row) && $ids[$i]!=-1)
			{
				$update_data = array(
					'area'=>$row
				);
				$where = array('id'=>$ids[$i]);
				$this->Capability_Focus_Area_Model->update($update_data,$where);				
			}
			$i++;
		}
		$this->deleteFromAdminSettings('Capability_Focus_Area_Model',$ids,$table_name,$cap_id);
	}
	
	public function deleteFromAdminSettings($model_name,$ids,$table_name,$cap_id='')
	{
		$this->load->model($model_name);
		
		if($table_name=='focus_area'){
			$where_clauses = array('capability_focus_area_id'=>$cap_id);
			$compare = $this->$model_name->getByKeyValueArray( $where_clauses);	
			
		    foreach($compare as $row)
			{
				if(!(in_array($row->id,$ids)))
				{								
					$where_clause = array('id'=>$row->id);
					$this->$model_name->delete($where_clause);
				}
			}
		}elseif($model_name=='Project_Bhu_Release_Attributes_Model'){
	    	
			$where_clauses = array('table_name'=>$table_name);
    		$compare = $this->$model_name->getByKeyValueArray( $where_clauses);
	    	foreach($compare as $row)
			{
				if(!(in_array($row->id,$ids)))
				{								
					$where_clause = array('id'=>$row->id);
					$this->$model_name->delete($where_clause);
				}
			}			    		
		}	
	}
	public function updateBhuReleaseAttr($desc,$ids,$table_name,$visible)
	{
		$this->load->model('Project_Bhu_Release_Attributes_Model');
		$i=0;
		foreach($desc as $row)
		{
			if(!empty($row) && $ids[$i]==-1)
			{
				$internal_name = str_replace('/','_',$row);
				$insert_data = array(
		        			'description'=>$row,
		        			'table_name'=>$table_name,
		        			'internal_name'=>strtoupper($internal_name),
				);
				if($table_name=='BENEFIT')
				{
					$insert_data['visible'] = $visible[$i];
				}
				else
				{
					$insert_data['visible'] = 1;
				}
				array_push($ids,$this->Project_Bhu_Release_Attributes_Model->insert($insert_data));
			}elseif(!empty($row) && $ids[$i]!=-1)
			{
				$internal_name = str_replace('/','_',$row);
				$update_data = array(
		        			'description'=>$row,
		        			'table_name'=>$table_name,
		        			'internal_name'=>strtoupper($internal_name),
				);
				$where = array('id'=>$ids[$i]);
				$this->Project_Bhu_Release_Attributes_Model->update($update_data,$where);
			}
			$i++;
		}
		$this->deleteFromAdminSettings('Project_Bhu_Release_Attributes_Model',$ids,$table_name);
	}

}