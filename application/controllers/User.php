<?php
class User extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Api_Webservice_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'City_Model' );
		$this->load->model ( 'User_Login_Credential_Model' );
		$this->load->model ( 'User_Personal_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		$data = array ();
		
		$this->addJs ( 'app/user.js' );
		
		$this->render ( "user/add_edit_user", $data );
	}
	public function validationUser() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'firstName',
						'label' => 'First Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[15]' 
				),
				array (
						'field' => 'lastName',
						'label' => 'Last Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[15]' 
				),
				array (
						'field' => 'internalEmail',
						'label' => 'Internal Email Id',
						'rules' => 'trim|required|valid_email' 
				),
				array (
						'field' => 'dob',
						'label' => 'Date of Birth',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'doj',
						'label' => 'Date of Joining',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'dor',
						'label' => 'Date of Releave',
						'rules' => 'trim' 
				),
				array (
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email' 
				),
				
				array (
						'field' => 'mobile',
						'label' => 'Mobile',
						'rules' => 'trim|required|numeric|exact_length[10]' 
				),
				
				array (
						'field' => 'alternateMobile',
						'label' => 'Alternate Mobile',
						'rules' => 'trim|required|numeric|exact_length[10]' 
				),
				array (
						'field' => 'qualification',
						'label' => 'Qualification',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'experience',
						'label' => 'Experience',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'permanentAddress',
						'label' => 'Permanent Address',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'communicationAddress',
						'label' => 'Communication Address',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'companyId',
						'label' => 'Company Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'ciyyId',
						'label' => 'City Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'roleType',
						'label' => 'Role Type',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showEmployees () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
		$this->addJs ( 'datepicker/jquery.datetimepicker.full.js' );
		$this->addJs ( 'app/user.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "user";
		$data ['active_menu'] = 'user';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'id' );
		$data ['role_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'ROLE_TYPE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		
		$data ['user_model'] = $this->User_Login_Credential_Model->getBlankModel ();
		$data ['user_personal_details_model'] = $this->User_Personal_Details_Model->getBlankModel ();
		
		$this->render ( 'user/add_edit_user', $data );
	}
	public function saveUser() {
		$data = array ();
		
		$data = $this->input->post ();
		$user_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		$fname = $data ['firstName'];
		$role_type = $data ['roleType'];
		$companyId = $data ['companyId'];
		$company_name = $this->Query_Model->getCompanyNameById ( $companyId );
		$user_identity = $this->User_Login_Credential_Model->generateUserIndentity ( $company_name, $role_type );
		// $user_password = $this->generate_password();
		/* ****** need to be changed ****** */
		$user_password = random_string ( 'alnum', 8 );
		
		$user_encrypted_password = hash ( "sha256", $user_password );
		$password = $user_encrypted_password;
		// $user_encrypted_password = md5($user_password);
		if ($user_id > 0) {
			$this->User_Login_Credential_Model->update ( $data, array (
					'id' => $user_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			$data ['userIdentity'] = $user_identity;
			$data ['password'] = $user_encrypted_password;
			$user_id = $this->User_Login_Credential_Model->insert ( $data );
			// save user personal details
			if ($user_id > 0) {
				$data ['userId'] = $user_id;
				$this->User_Personal_Details_Model->insert ( $data );
			}
			if ($user_id > 0) {
				// SMS & email service start
				
				if (SMS && $user_id) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'smsTitle' => 'account_create_sms' 
					) );
					
					$message_data = $message_details->smsContent;
					$web_link = array ();
					$web_link [0] = substr ( APPLINK, 0, 20 );
					$web_link [1] = substr ( APPLINK, 20, 20 );
					$web_link [2] = substr ( APPLINK, 40, 20 );
					$web_link [3] = substr ( APPLINK, 60, 20 );
					$web_link [4] = substr ( APPLINK, 80, 20 );
					$web_link [5] = substr ( APPLINK, 100, 20 );
					$web_link [6] = substr ( APPLINK, 120, 20 );
					// $address[7]=substr(APPLINK,140,20);
					
					if (strlen ( APPLINK ) > 140) {
						$web_link [5] = cut_string_using_last ( ',', $web_link [5], 'left', false );
					}
					$message_data = str_replace ( "##USERNAME##", $data ['internalEmail'], $message_data );
					$message_data = str_replace ( "##PASSWORD##", $user_password, $message_data );
					$message_data = str_replace ( "##APPLINK1##", $web_link [0], $message_data );
					$message_data = str_replace ( "##APPLINK2##", $web_link [1], $message_data );
					$message_data = str_replace ( "##APPLINK3##", $web_link [2], $message_data );
					$message_data = str_replace ( "##APPLINK4##", $web_link [3], $message_data );
					$message_data = str_replace ( "##APPLINK5##", $web_link [4], $message_data );
					$message_data = str_replace ( "##APPLINK6##", $web_link [5], $message_data );
					$message_data = str_replace ( "##APPLINK7##", $web_link [6], $message_data );
					// $message_data = str_replace("##APPLINK8##",'',$message_data);
					
					// print_r($message);exit;
					if ($data ['mobile'] != "") {
						
						$this->Api_Webservice_Model->sendSMS ( $data ['mobile'], $message_data );
					}
				}
				
				$from = SMTP_EMAIL_ID;
				$to = $data ['internalEmail'];
				$subject = 'Zuver Login Credential';
				
				$data = array (
						'user_name' => $data ['firstName'] . ' ' . $data ['lastName'],
						'email' => $data ['internalEmail'],
						'password' => $user_password,
						'identity' => $user_identity 
				);
				$message_data = $this->load->view ( 'emailtemplate/user_register', $data, TRUE );
				
				if (SMTP && $user_id) {
					
					if ($to) {
						$this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
					}
				} else {
					
					// To send HTML mail, the Content-type header must be set
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Additional headers
					$headers .= 'From: Zuver<' . $from . '>' . "\r\n";
					$headers .= 'To: <' . $to . '>' . "\r\n";
					if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
						mail ( $to, $subject, $message_data, $headers );
					}
				}
				// SMS & email service end
			}
			$msg = $msg_data ['success'];
		}
		
		// $this->render("city/add_edit_city", $data);
		
		$response = array (
				'msg' => $msg,
				'user_id' => $user_id 
		);
		
		echo json_encode ( $response, JSON_UNESCAPED_SLASHES );
	}
	public function getDetailsById($user_id, $view_mode) {
		if ($this->User_Access_Model->showEmployees () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'app/user.js' );
		$data ['activetab'] = "user";
		$data ['active_menu'] = 'user';
		$data ['mode'] = $view_mode;
		
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'id' );
		$data ['role_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'ROLE_TYPE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		if ($this->input->post ( 'id' )) {
			$user_id = $this->input->post ( 'id' );
		}
		if ($user_id > 0) {
			// Get release details by driver_id
			$data ['user_model'] = $this->User_Login_Credential_Model->getById ( $user_id );
			$data ['user_personal_details_model'] = $this->User_Personal_Details_Model->getOneByKeyValueArray ( array (
					'userId' => $data ['user_model']->id 
			) );
		} else {
			// Blank Model
			$data ['driver_model'] = $this->User_Login_Credential_Model->getBlankModel ();
			$data ['user_personal_details_model'] = $this->User_Personal_Details_Model->getBlankModel ();
		}
		
		if ($this->input->post ( 'id' )) {
			$html = $this->load->view ( "user/add_edit_user", $data );
			
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'user/add_edit_user', $data );
		}
	}
	/**
	 * to get users list
	 */
	public function getUserList() {
		if ($this->User_Access_Model->showEmployees () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'datepicker/jquery.datetimepicker.full.js' );
		$this->addJs ( 'app/user.js' );
		$this->addJs ( 'app/tabel.js' );
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "user";
		$data ['active_menu'] = 'user';
		$data ['user_model_list'] = $this->Query_Model->getUserList ();
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "user/manage_user", $data );
			$result_array = array (
					'html' => $html,
					'msg' => 'success' 
			);
			echo json_encode ( $result_array );
		} else {
			$html = $this->render ( "user/manage_user", $data );
		}
	}
	public function changeUserStatus() {
		$data = array ();
		$data = $this->input->post ();
		$user_id = $data ['id'];
		$user_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($user_id) {
			$driver_updated = $this->User_Login_Credential_Model->update ( array (
					'status' => $user_status 
			), array (
					'id' => $user_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		
		$response = array (
				'msg' => $msg,
				'user_id' => $user_id 
		);
		echo json_encode ( $response );
	}
	public function checkUserInternalEmail() {
		$data = array ();
		
		$data = $this->input->post ();
		$email = $data ['internalEmail'];
		$email_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['email_not_exists'];
		if ($email) {
			$email_exists = $this->User_Login_Credential_Model->getOneByKeyValueArray ( array (
					'internalEmail' => $email 
			) );
			if ($email_exists) {
				$status = 1;
				$msg = $msg_data ['email_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $email,
				'status' => $status 
		);
		echo json_encode ( $response );
	}
	public function generate_password($length = 6) {
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";
		$password = substr ( str_shuffle ( $chars ), 0, $length );
		return $password;
	}
	public function getPartnerUserList()
	{
		$data=array();
		$options='';
		$data['user_list']=array();
		$company_id=$this->input->post('company_id');
		if ($company_id > 0)
		{
			$data['user_list']=$this->User_Login_Credential_Model->getByKeyValueArray(array('companyId'=>$company_id,'status'=>Status_Type_Enum::ACTIVE));
			if (count($data['user_list']) > 0)
			{
				$options.='<option value="">-- Select --<options>';
				foreach ($data['user_list'] as $value)
				{
					$options.='<option value="'.$value->id.'">'.$value->firstName.' '.$value->lastName.'<options>';
				}
			}
		}
		
		$response = array (
				'data' => $options
		);
		echo json_encode ( $response );
	}
}