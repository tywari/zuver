<?php
class Dashboard extends My_Controller
{
	function __construct()
	{
		
		parent::__construct();
		is_logged_in();
		$this->load->helper('form');
		$this->load->model('Passenger_Model');
		$this->load->model('Driver_Model');
		$this->load->model('City_Model');
		$this->load->model('Bank_Account_Details_Model');
		$this->load->model('User_Login_Credential_Model');
		$this->load->model('Corporate_Partners_Details_Model');
		$this->load->model('Trip_Details_Model');
		$this->load->model('Query_Model');
        
	}
	
	public function index()
	{
		
               
        $data = array();
        
		$data['activetab']="dashboard";
		$data['active_menu'] = 'dashboard';
		//$this->addJs('admin-home.js');
		
		//$this->addJs('vendors/jquery-sparkline/dist/jquery.sparkline.min.js');
		//$this->addJs('vendors/jqvmap/dist/jquery.vmap.js');
		//$this->addJs('vendors/jqvmap/dist/maps/jquery.vmap.world.js');
		//$this->addJs('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js');
                $this->addJs('vendors/datatables.net/js/jquery.dataTables.min.js');
		$this->addJs('app/tabel.js');
                
		//$this->addJs('vendors/Chart.js/dist/Chart.min.js');
		//$this->addJs('vendors/gauge.js/dist/gauge.min.js');
		//$this->addJs('vendors/Flot/canvasjs.min.js');
		//$this->addJs('vendors/Flot/jquery.flot.pie.js');
		//$this->addJs('vendors/Flot/jquery.flot.time.js');
		//$this->addJs('vendors/Flot/jquery.flot.stack.js');
		//$this->addJs('vendors/Flot/jquery.flot.resize.js');
		//$this->addJs('vendors/echarts/dist/echarts.min.js');
                
                //$this->addJs('vendors/raphael/raphael.min.js');
                //$this->addJs('vendors/morris.js/morris.min.js');
                
                $role_type =  $this->session->userdata('role_type'); 
                //Manager Dashboard
                if($role_type == Role_Type_Enum::MANAGER){
                   // $this->addJs('app/dashboard/dashboard_manager.js');
                    
                    $company_id = $this->session->userdata('user_company');
                    $city_id = $this->session->userdata('user_city');//$this->input->post('city_id');
                    
                    $data=$this->getReportSummary($city_id);
                	$data['activetab']="dashboard";
                	$data['active_menu'] = 'dashboard';
                    
                    $data['total_drivers']    = count($this->Driver_Model->getAll());
                    $data['active_drivers']   = count($this->Driver_Model->getByKeyValueArray ( array ('status' =>'Y',), 'id' ));
                    $data['total_passenger']  = count($this->Passenger_Model->getAll());
                    $data['android_users']    = count($this->Passenger_Model->getByKeyValueArray ( array ('deviceType' =>'A',), 'id' ));
                    $data['ios_users']        = count($this->Passenger_Model->getByKeyValueArray ( array ('deviceType' =>'I',), 'id' ));
                    
                    $data['active_drivers']   = count($this->Driver_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                    $data['deactive_drivers']   = count($this->Driver_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                     
                    $data['active_passengers']   = count($this->Passenger_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                    $data['deactive_passengers']   = count($this->Passenger_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                     
                    $data['active_employees']   = count($this->User_Login_Credential_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                    $data['deactive_employees']   = count($this->User_Login_Credential_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                     
                    $data['active_partners']   = count($this->Corporate_Partners_Details_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                    $data['deactive_partners']   = count($this->Corporate_Partners_Details_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                     
                    $data['active_city']   = count($this->City_Model->getByKeyValueArray ( array ('status' =>'Y',), 'id' ));
                    $data['deactive_city']   = count($this->City_Model->getByKeyValueArray ( array ('status' =>'N',), 'id' ));
                    
                    $this->render("dashboard/manager", $data);
                }
                //Partners Dashboard
                else if($role_type == Role_Type_Enum::COMPANY){
                    $company_id = $this->session->userdata('user_company');
                    $data['no_of_bookings']         = count($this->Trip_Details_Model->getByKeyValueArray ( array ('companyId' =>$company_id), 'id' ));
                    $today  = date('Y-m-d');
                    $data['today_bookings']         = count($this->Trip_Details_Model->getByKeyValueArray ( array ('companyId' =>$company_id,
                                                                                                            'DATE(pickupDatetime)'=>$today), 'id' ));
                    $data['trips_completed']        = count($this->Trip_Details_Model->getByKeyValueArray ( array ('companyId' =>$company_id,
                                                                                                            'tripStatus'=>'TCT'), 'id' ));
                    $data['trips_completed_today']  = count($this->Trip_Details_Model->getByKeyValueArray ( array ('companyId' =>$company_id,
                                                                                                            'tripStatus'=>'TCT','DATE(pickupDatetime)'=>$today),
                                                                                                            'id' ));
                    $data['bank_details_model'] =$this->Bank_Account_Details_Model->getById(DEFAULT_COMPANY_ID);
                    
                    $city_id = $this->input->post('city_id');
                    $trip_status = $this->input->post('trip_status');
                    
                    $data ['trip_details_model_list'] = $this->Query_Model->getTripList($city_id, $company_id, $trip_status);
                    $this->render("dashboard/partners", $data);
                }
                //Finance Dashboard
                else if($role_type == Role_Type_Enum::ACCOUNTANT){
                    //$this->addJs('app/dashboard/dashboard_finance.js');
                    
                    $company_id = $this->session->userdata('user_company');
                    $city_id = $this->session->userdata('user_city');
                    $data=$this->getReportSummary($city_id);
                    $data['activetab']="dashboard";
                    $data['active_menu'] = 'dashboard';
                    //$data['payment_mode_collection']=$this->Query_Model->getTotalCollectionByPaymentMode($city_id,$company_id);
                    $this->render("dashboard/finance", $data);
                }
                //Supervisior Dashboard
                else if($role_type == Role_Type_Enum::SUPERVISOR){
                   // $this->addJs('app/dashboard/dashboard_manager.js');
                    
                    $company_id = $this->session->userdata('user_company');
                    $city_id = $this->session->userdata('user_city');//$this->input->post('city_id');
                    
                    $data=$this->getReportSummary($city_id);
                	$data['activetab']="dashboard";
                	$data['active_menu'] = 'dashboard';
                    
                    $data['total_drivers']    = count($this->Driver_Model->getAll());
                    $data['active_drivers']   = count($this->Driver_Model->getByKeyValueArray ( array ('status' =>'Y',), 'id' ));
                    $data['total_passenger']  = count($this->Passenger_Model->getAll());
                    $data['android_users']    = count($this->Passenger_Model->getByKeyValueArray ( array ('deviceType' =>'A',), 'id' ));
                    $data['ios_users']        = count($this->Passenger_Model->getByKeyValueArray ( array ('deviceType' =>'I',), 'id' ));
                    
                    $data['active_drivers']   = count($this->Driver_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                    $data['deactive_drivers']   = count($this->Driver_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                     
                    $data['active_passengers']   = count($this->Passenger_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                    $data['deactive_passengers']   = count($this->Passenger_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                     
                    $data['active_employees']   = count($this->User_Login_Credential_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                    $data['deactive_employees']   = count($this->User_Login_Credential_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                     
                    $data['active_partners']   = count($this->Corporate_Partners_Details_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                    $data['deactive_partners']   = count($this->Corporate_Partners_Details_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                     
                    $data['active_city']   = count($this->City_Model->getByKeyValueArray ( array ('status' =>'Y',), 'id' ));
                    $data['deactive_city']   = count($this->City_Model->getByKeyValueArray ( array ('status' =>'N',), 'id' ));
                    $this->render("dashboard/supervisior", $data);
                }
                //Staff Dashboard
                else if($role_type == Role_Type_Enum::STAFF){
                    $this->addJs('vendors/datatables.net/js/jquery.dataTables.min.js');
                   // $this->addJs('app/dashboard/dashboard_staff.js');
                    $company_id = $this->session->userdata('user_company');
                    $city_id = $this->session->userdata('user_city');//$this->input->post('city_id');
                    
                    $data['new_bookings']   = count($this->Trip_Details_Model->getByKeyValueArray ( array ('companyId' =>$company_id,'bookingCityId'=>$city_id,
                                                                                                  'tripStatus'=>Trip_Status_Enum::BOOKED), 'id' ));
                    $data['trip_confirmed'] = count($this->Trip_Details_Model->getByKeyValueArray ( array ('companyId' =>$company_id,'bookingCityId'=>$city_id,
                                                                                                  'tripStatus'=>Trip_Status_Enum::TRIP_CONFIRMED), 'id' ));
                    $data['on_board']       = count($this->Trip_Details_Model->getByKeyValueArray ( array ('companyId' =>$company_id,'bookingCityId'=>$city_id,
                                                                                                  'tripStatus'=>Trip_Status_Enum::IN_PROGRESS), 'id' ));
                    $data['trip_canceled']  = count($this->Trip_Details_Model->getByKeyValueArray ( array ('companyId' =>$company_id,'bookingCityId'=>$city_id,
                                                                                                  'tripStatus'=>Trip_Status_Enum::CANCELLED_BY_PASSENGER),'id'));
                    
                    $trip_status = $this->input->post('trip_status');
                    $company_id = $this->session->userdata('user_company');
                    $data ['trip_details_model_list'] = $this->Query_Model->getTripList($city_id, $company_id, $trip_status);
                    $this->render("dashboard/staff", $data);
                }
                //Admin Dashboard
                else if($role_type == Role_Type_Enum::ADMIN){
                	//$this->addJs('app/dashboard/dashboard_admin.js');
                	$company_id = $this->session->userdata('user_company');
                	$city_id = $this->session->userdata('user_city');//$this->input->post('city_id');
                	
                	$data=$this->getReportSummary($city_id);
                	$data['activetab']="dashboard";
                	$data['active_menu'] = 'dashboard';
                	
                	$data['active_drivers']   = count($this->Driver_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                	$data['deactive_drivers']   = count($this->Driver_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                	
                	$data['active_passengers']   = count($this->Passenger_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                	$data['deactive_passengers']   = count($this->Passenger_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                	
                	$data['active_employees']   = count($this->User_Login_Credential_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                	$data['deactive_employees']   = count($this->User_Login_Credential_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                	
                	$data['active_partners']   = count($this->Corporate_Partners_Details_Model->getByKeyValueArray ( array ('status' =>'Y','cityId'=>$city_id), 'id' ));
                	$data['deactive_partners']   = count($this->Corporate_Partners_Details_Model->getByKeyValueArray ( array ('status' =>'N','cityId'=>$city_id), 'id' ));
                	
                	$data['active_city']   = count($this->City_Model->getByKeyValueArray ( array ('status' =>'Y',), 'id' ));
                	$data['deactive_city']   = count($this->City_Model->getByKeyValueArray ( array ('status' =>'N',), 'id' ));
                	
                	
                	$this->render("dashboard/admin", $data);
                }
                else{
                    redirect('login');
                }
	}
        
        public function checkUserIsLogged(){
            if($this->session->userdata('user_email')==''){
                redirect('login');
            }
        }
        
        public function getReportSummary($city_id=NULL,$company_id=NULL)
        {
        	$data=array();
        	$trip_collection_details=$this->Query_Model->getTotalCollectionByPaymentMode($city_id,$company_id);
        	$trip_status_details=$this->Query_Model->getTripStatusCount($city_id,$company_id);
        	
        	$data['total_online_driver']=count($this->Driver_Model->getByKeyValueArray(array('status'=>Status_Type_Enum::ACTIVE,'cityId'=>$city_id,'isDeleted'=>Status_Type_Enum::INACTIVE,'isVerified'=>Status_Type_Enum::ACTIVE)));
        	$data['total_passenger']=count($this->Passenger_Model->getByKeyValueArray(array('status'=>Status_Type_Enum::ACTIVE,'cityId'=>$city_id)));
        	
        	$data['total_cash_collection']=$trip_collection_details[0]->totalCollectedCash;
        	$data['total_wallet_collection']=$trip_collection_details[0]->totalCollectedWallet;
        	$data['total_paytm_collection']=$trip_collection_details[0]->totalCollectedPaytm;
        	$data['total_invoice_collection']=$trip_collection_details[0]->totalCollectedInvoice;
        	$data['total_collection']=$data['total_cash_collection']+$data['total_wallet_collection']+$data['total_paytm_collection']+$data['total_invoice_collection'];
        	$data['total_tax_collection']=$trip_collection_details[0]->totalCompanyTax;
        	$data['total_admin_collection']=$trip_collection_details[0]->totalAdminCommission;
        	$data['total_discount_offered']=$trip_collection_details[0]->totalDiscount;
        	$data['total_diver_earning']=$trip_collection_details[0]->totalDriverEarnings;
        	
        	$data['total_unassigned_trip']=$trip_status_details[0]->totalUnassignedTrip;
        	$data['total_upcoming_trip']=$trip_status_details[0]->totalUpcomingTrip;
        	$data['total_ongoing_trip']=$trip_status_details[0]->totalOngoingTrip;
        	$data['total_cancelled_trip']=$trip_status_details[0]->totalCancelledTrip;
        	$data['total_completed_trip']=$trip_status_details[0]->totalCompletedTrip;
        	$data['total_trip']=$data['total_unassigned_trip']+$data['total_upcoming_trip']+$data['total_ongoing_trip']+$data['total_cancelled_trip']+$data['total_completed_trip'];
        	
        	$data['average_driver_trip']=round(0);
        	$data['average_driver_revenue']=round(0,2);
        	$data['average_driver_earning']=round(0,2);
        	
        	if ($data['total_online_driver'] > 0)
        	{
        		$data['average_driver_trip']=round($data['total_completed_trip']/$data['total_online_driver']);
        		$data['average_driver_revenue']=round($data['total_collection']/$data['total_online_driver'],2);
        		$data['average_driver_earning']=round($data['total_diver_earning']/$data['total_online_driver'],2);
        	}
        	
        	$data['average_passenger_trip']=round(0);
        	$data['average_passenger_revenue']=round(0,2);
        	
        	if ($data['total_passenger'] > 0)
        	{
        		$data['average_passenger_trip']=round($data['total_completed_trip']/$data['total_passenger']);
        		$data['average_passenger_revenue']=round($data['total_collection']/$data['total_passenger'],2);
        	}
        	
        	return $data;
        	
        }
        
        

}