<?php
require_once(APPPATH . 'libraries/Rest_Controller.php');

class ApiWebService extends Rest_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Query_Model');
        $this->load->model('Auth_Key_Model');
        $this->load->model('Site_Details_Model');
        $this->load->model('Site_Social_Media_Details_Model');
        $this->load->model('Api_Webservice_Model');
        $this->load->model('Passenger_Model');
        $this->load->model('Driver_Model');
        $this->load->model('Driver_Personal_Details_Model');
        $this->load->model('Country_Model');

        $this->load->model('Sms_Template_Model');
        $this->load->model('Trip_Details_Model');
        $this->load->model('Sub_Trip_Details_Model');
        $this->load->model('Trip_Transaction_Details_Model');
        $this->load->model('Trip_Temp_Tracking_Info_Model');
        $this->load->model('Trip_Tracked_Info_Model');

        $this->load->model('Rate_Card_Details_Model');
        $this->load->model('Corporate_Partners_Rate_Card_Mapping_Model');
        $this->load->model('Corporate_Partners_Api_Details_Model');
        $this->load->model('Emergency_Request_Details_Model');
        $this->load->model('Passenger_Favourites_Model');
        $this->load->model('Driver_Rejected_Trip_Details_Model');
        $this->load->model('Driver_Shift_History_Model');
        $this->load->model('Driver_Rate_Card_Details_Model');
        $this->load->model('Driver_Login_Incentive_Model');
        $this->load->model('Driver_Productivity_Allowance_Model');
        $this->load->model('Driver_Request_Details_Model');
        $this->load->model('Driver_Temp_Tracking_Info_Model');
        $this->load->model('Passenger_Promocode_Details_Model');
        $this->load->model('Passenger_Emergency_Contact_Model');

        $this->load->model('Passenger_Referral_Details_Model');
        $this->load->model('Passenger_Wallet_Details_Model');

        $this->load->model('User_Login_Credential_Model');
        $this->load->model('User_Personal_Details_Model');

        $this->load->model('Menu_Content_Details_Model');
        $this->load->helper('excel_generate_helper');
    }

    public function index()
    {
        $data = array();

        // $this->addJs('admin-home.js');
        // $this->addJs('app/dashboard.js');

        // $this->render("dashboard", $data);
    }

    public function get_post_data($validate = FALSE)
    {
        $data = file_get_contents('php://input');
        $post = json_decode($data);
        if (!$post) {
            $post = json_decode(json_encode($_POST), FALSE);
        }
        return $post;
    }

    public function checkNull($value)
    {
        if ($value == null) {
            return '';
        } else {
            return $value;
        }
    }

    public function getbase64ImageUrl($passenger_id, $image)
    {
        $file_image = '';
        $img = $image;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file_name = "profile_image.jpg";
        if (!file_exists(passenger_content_url() . "" . $passenger_id)) {
            mkdir(passenger_content_url() . "" . $passenger_id, 0777, true);
        }
        $file = passenger_content_url() . "" . $passenger_id . "/" . $file_name;
        $success = file_put_contents($file, $data);
        if ($success) {
            $file_image = $file_name;
        } else {
            $file_image = '';
        }
        return $file_image;
    }
    // This five common webservice used by both passenger & driver

    /**
     * get the company details and driver fare details
     */
    public function getcoreconfig_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');

        $post_data = array();
        $response_data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }
        // if ($data_auth_key->key == $auth_key) {
        // To get site info details
        $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
            'id' => 1
        ));
        // to get currency code/currency symbol currencySymbol
        $currency_details = $this->Country_Model->getColumnByKeyValueArray('currencySymbol', array(
            'id' => $site_setting_info->countryId
        ));
        // To get site social media info details
        $site_social_media_setting_info = $this->Site_Social_Media_Details_Model->getByKeyValueArray(array(
            'siteId' => $site_setting_info->id
        ));
        if ($site_setting_info || $site_social_media_setting_info) {
            if ($auth_user_type == User_Type_Enum::DRIVER) {
                $response_data ['tell_to_friend_subject'] = $site_setting_info->driverTellToFriendMsg;
                // to get driver details
                // $user_details = $this->Driver_Model->getById ( $auth_user_id );
                $user_details = array(
                    'companyId' => DEFAULT_COMPANY_ID,
                    'cityId' => $site_setting_info->cityId
                );
            } else if ($auth_user_type == User_Type_Enum::PASSENGER) {
                $response_data ['tell_to_friend_subject'] = $site_setting_info->passengerTellToFriendMsg;
                // to get passenger details
                // $user_details = $this->Passenger_Model->getById ( $auth_user_id );
                $user_details = array(
                    'companyId' => DEFAULT_COMPANY_ID,
                    'cityId' => $site_setting_info->cityId
                );
            } else {
                $response_data ['tell_to_friend_subject'] = '';
                $user_details = array(
                    'companyId' => DEFAULT_COMPANY_ID,
                    'cityId' => $site_setting_info->cityId
                );
            }
            // debug_exit($user_details);
            // to get rate card details based on companyId & cityId of passenger/driver
            $rate_card_details = $this->Api_Webservice_Model->getRateCardDetails($user_details ['companyId'], $user_details ['cityId']);

            $response_data = array(
                'noimage_base' => image_url('app/'),
                'api_base' => base_url(),
                'logo_base' => image_url('app/' . $site_setting_info->logo),
                'aboutpage_description' => $site_setting_info->description,
                'admin_email' => $site_setting_info->email,
                'skip_credit' => SKIP_CREDIT_CARD,
                'metric' => UNIT_NAME,
                'referral_amount' => $site_setting_info->referralDiscountAmount,
                'cancellation_setting' => CANCELLATION_FARE,
                'driver_itunes_url' => '',
                'driver_itunes_id' => '',
                'passenger_itunes_url' => '',
                "site_currency" => $currency_details [0]->currencySymbol,
                'passenger_itunes_id' => '',
                'android_passenger_app_ver' => ANDROID_PASSENGER_APP_VER,
                'iphone_passenger_app_ver' => IPHONE_PASSENGER_APP_VER,
                'driver_app_ver' => DRIVER_APP_VER,
                'driver_fare' => array(
                    'fare_id' => $rate_card_details [0]->rateCardId,
                    '1hr_to_4hr' => $rate_card_details [0]->firstHourCharge,
                    '5hr_to_8hr' => $rate_card_details [0]->fifthHourCharge,
                    '9hr_to_12hr' => $rate_card_details [0]->ninthHourCharge,
                    'above_12hr' => $rate_card_details [0]->above12HourCharge,
                    'service_tax' => $site_setting_info->tax
                )
            );
            foreach ($rate_card_details as $fare) {
                if ($fare->tripType == Trip_Type_Enum::ONEWAY_TRIP) {
                    $response_data ['driver_fare'] ['Oneway_7am_9pm'] = $fare->dayConvenienceCharge;
                    $response_data ['driver_fare'] ['Oneway_9pm_7am'] = $fare->nightConvenienceCharge;
                } else if ($fare->tripType == Trip_Type_Enum::RETURN_TRIP) {
                    $response_data ['driver_fare'] ['Return_7am_9pm'] = $fare->dayConvenienceCharge;
                    $response_data ['driver_fare'] ['Return_9pm_7am'] = $fare->nightConvenienceCharge;
                }
            }
            $message = array(
                "message" => $msg_data ['success'],
                "detail" => $response_data,
                "status" => 1
            );
        } else {
            $message = array(
                "message" => $msg_data ['failed'],
                "status" => 2
            );
        }
        /*
		 * } else {
		 * $message = array (
		 * "message" => $msg_data ['auth_failed'],
		 * "status" => 401
		 * );
		 * }
		 */
        echo json_encode($message);
    }

    /**
     * forgot password
     */
    public function forgot_password_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $user_exist = NULL;
        $email = NULL;
        $password = NULL;
        $update_password = NULL;

        // Posted json data
        $post_data = $this->get_post_data();
        /*
		 * foreach (getallheaders() as $name => $value) {
		 * if ($name == 'authKey')
		 * {
		 * $auth_key = $value;
		 * }
		 * if($name == 'userId')
		 * {
		 * $auth_user_id = $value;
		 * }
		 * if($name == 'userType')
		 * {
		 * $auth_user_type = $value;
		 * }
		 * }
		 */
        $mobile = $post_data->phone_no;

        /*
		 * // Checked wheather authentication key availablilty for specfic user(passenger/driver)
		 * $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray ( array (
		 * 'key' => $auth_key,
		 * 'userId' => $auth_user_id,
		 * 'userType' => $auth_user_type
		 * ), 'id' );
		 * if ($data_auth_key->key == $auth_key) {
		 */

        if ($post_data->user_type == User_Type_Enum::PASSENGER) {
            $user_exist = $this->Passenger_Model->getOneByKeyValueArray(array(
                'mobile' => $mobile
            ));
            if ($user_exist) {
                $email = $user_exist->email;
                $password = random_string('alnum', 8);
                // encrypted password
                $password_key = hash("sha256", $password);

                $update_password = $this->Passenger_Model->update(array(
                    'password' => $password_key
                ), array(
                    'id' => $user_exist->id
                ));
            }
        } else if ($post_data->user_type == User_Type_Enum::DRIVER) {
            $user_exist = $this->Driver_Model->getOneByKeyValueArray(array(
                'mobile' => $mobile
            ));
            if ($user_exist) {
                $email = $user_exist->email;
                $password = random_string('alnum', 8);
                // encrypted password
                $password_key = hash("sha256", $password);
                $update_password = $this->Driver_Model->update(array(
                    'password' => $password_key
                ), array(
                    'id' => $user_exist->id
                ));
            }
        }
        if ($user_exist) {

            if (SMS && $update_password) {
                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                    'smsTitle' => 'forgot_password_sms'
                ));

                $message_data = $message_details->smsContent;
                // $message = str_replace("##USERNAME##",$name,$message);
                $message_data = str_replace("##PASSWORD##", $password, $message_data);
                // print_r($message);exit;
                if ($mobile != "") {

                    $this->Api_Webservice_Model->sendSMS($mobile, $message_data);
                    if ($post_data->user_type == User_Type_Enum::DRIVER) {
                        // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                        $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                        // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                    }
                }
            }

            $from = SMTP_EMAIL_ID;
            $to = $user_exist->email;
            $subject = 'Zuver Forgot Password';
            $data = array(
                'password' => $password
            );
            $message_data = $this->load->view('emailtemplate/forgotpassword', $data, TRUE);

            if (SMTP && $update_password) {

                if ($to) {
                    $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                }
            } else {

                // To send HTML mail, the Content-type header must be set
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                // Additional headers
                $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                $headers .= 'To: <' . $to . '>' . "\r\n";
                if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                    mail($to, $subject, $message_data, $headers);
                }
            }
            $message = array(
                "message" => $msg_data ['reset_password'],
                'status' => 1
            );
        } else {
            $message = array(
                "message" => $msg_data ['invalid_user'],
                'status' => 2
            );
        }
        /*
		 * }
		 * else {
		 * $message = array (
		 * "message" => $msg_data ['auth_failed'],
		 * "status" => 401
		 * );
		 * }
		 */
        echo json_encode($message);
    }

    /**
     * used to get the trip details
     */
    public function get_trip_detail_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $response_data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $trip_details = NULL;
        $trip_id = NULL;
        // This input is posted only by IOS APP
        $passenger_id = NULL;

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }
        $trip_id = $post_data->trip_id;
        if (array_key_exists('passenger_id', $post_data)) {
            $passenger_id = $post_data->passenger_id;
        }
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual funtionality
                if ($trip_id != null) {
                    // to get site information details
                    $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                        'id' => 1
                    ));
                    // to get currency code/currency symbol currencySymbol
                    $currency_details = $this->Country_Model->getColumnByKeyValueArray('currencySymbol', array(
                        'id' => $site_setting_info->countryId
                    ));

                    $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id, $passenger_id);
                    if ($trip_details > 0) {

                        $response_data = array(
                            'taxi_min_speed' => '',
                            'trip_id' => $trip_id,
                            'current_location' => $trip_details [0]->pickupLocation,
                            'pickup_latitude' => $trip_details [0]->pickupLatitude,
                            'pickup_longitude' => $trip_details [0]->pickupLongitude,
                            'driver_language' => $trip_details [0]->driverLanguagesKnown,
                            'driver_experience' => $trip_details [0]->driverExperience,
                            'age' => $trip_details [0]->driverDob,
                            'drop_location' => $trip_details [0]->dropLocation,
                            'drop_latitude' => $trip_details [0]->dropLatitude,
                            'drop_longitude' => $trip_details [0]->dropLongitude,
                            'drop_time' => $trip_details [0]->dropDatetime,
                            'pickup_time' => $trip_details [0]->actualPickupDatetime,
                            'time_to_reach_passen' => 0,
                            'no_passengers' => 0,
                            'rating' => $trip_details [0]->passengerRating,
                            'notes' => $trip_details [0]->passengerComments,
                            'driver_name' => $trip_details [0]->diverFirstName,
                            'driver_lastname' => $trip_details [0]->diverLastName,
                            'driver_id' => $trip_details [0]->driverId,
                            'driver_image' => ($trip_details [0]->driverProfileImage) ? (driver_content_url($trip_details [0]->driverId . '/' . $trip_details [0]->driverProfileImage)) : image_url('app/user.png'),
                            'driver_latitute' => $trip_details [0]->currentLatitude,
                            'driver_longtitute' => $trip_details [0]->currentLongitude,
                            'driver_status' => $trip_details [0]->availabilityStatus,
                            'driver_rating' => 0,
                            'taxi_id' => 0,
                            'taxi_number' => 0,
                            'driver_phone' => $trip_details [0]->diverMobile,
                            'passenger_phone' => $trip_details [0]->passengerMobile,
                            'passenger_name' => $trip_details [0]->passengerFirstName,
                            'passenger_lastname' => $trip_details [0]->passengerLastName,
                            'passenger_image' => ($trip_details [0]->passengerProfileImage) ? (passenger_content_url($trip_details [0]->passengerId . '/' . $trip_details [0]->passengerProfileImage)) : image_url('app/user.png'),
                            'map_url' => $trip_details[0]->mapUrl,
                            'waiting_time' => 0,
                            'distance' => $trip_details [0]->travelDistance,
                            'actual_distance' => $trip_details [0]->travelDistance,
                            'amt' => $trip_details [0]->totalTripCost,
                            'job_ref' => $trip_details [0]->pickupLocation,
                            'payment_type' => $trip_details [0]->pickupLocation,
                            'taxi_speed' => 0,
                            'convenience_charge' => $trip_details [0]->convenienceCharge,
                            'overtime_charge' => 0,
                            'parking_charge' => $trip_details [0]->parkingCharge,
                            'tax_amount' => $trip_details [0]->companyTax,
                            'tax_percentage' => $site_setting_info->tax,
                            'other_charge' => $trip_details [0]->tollCharge,
                            'key_pickup' => '',
                            'car_details' => '',
                            'land_mark' => $trip_details [0]->landmark,
                            'ot_hours' => 0,
                            'trip_fare' => $trip_details [0]->travelCharge,
                            'passenger_discount' => $trip_details [0]->promoDiscountAmount,
                            'promo_discount' => $trip_details [0]->promoDiscountAmount,
                            'refferal_discount' => 0,
                            'wallet_payment' => $trip_details [0]->walletPaymentAmount,
                            'pack_cost' => 0,
                            'trip_duration' => $trip_details [0]->travelHoursMinutes,
                            "is_zoomcar_trip" => ($trip_details[0]->companyId == ZOOMCAR_COMPANYID) ? 1 : 0,
                            "is_b2c" => ($trip_details[0]->companyId == DEFAULT_COMPANY_ID) ? 1 : 0
                        );
                        // driver average rating
                        $driver_avg_rating = $this->Api_Webservice_Model->getDriverAverageRating($trip_details [0]->driverId);
                        if ($driver_avg_rating > 0) {
                            $response_data ['driver_rating'] = round($driver_avg_rating [0]->driverRating, 2);
                        }
                        // trip type & trip plan name
                        if ($trip_details [0]->tripType == Trip_Type_Enum::ONEWAY_TRIP) {
                            $response_data ['trip_type'] = 1;
                            $response_data ['trip_plan'] = Trip_Type_Enum::O;
                        } else if ($trip_details [0]->tripType == Trip_Type_Enum::RETURN_TRIP) {
                            $response_data ['trip_type'] = 2;
                            $response_data ['trip_plan'] = Trip_Type_Enum::R;
                        } else if ($trip_details [0]->tripType == Trip_Type_Enum::OUTSTATION_TRIP) {
                            $response_data ['trip_type'] = 3;
                            $response_data ['trip_plan'] = Trip_Type_Enum::X;
                        } else if ($trip_details [0]->tripType == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY) {
                            $response_data ['trip_type'] = $trip_details [0]->tripType;
                            $response_data ['trip_plan'] = Trip_Type_Enum::XO;
                        } else if ($trip_details [0]->tripType == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) {
                            $response_data ['trip_type'] = $trip_details [0]->tripType;
                            $response_data ['trip_plan'] = Trip_Type_Enum::XR;
                        }
                        // payment mode & payment mode name
                        if ($trip_details [0]->paymentMode == Payment_Mode_Enum::CASH) {
                            $response_data ['faretype'] = 1;
                            $response_data ['faretype_name'] = Payment_Mode_Enum::C;
                            $response_data ['payment_type'] = 1;
                            $response_data ['pay_mod_name'] = Payment_Mode_Enum::C;
                        } else if ($trip_details [0]->paymentMode == Payment_Mode_Enum::PAYTM) {
                            $response_data ['faretype'] = 2;
                            $response_data ['faretype_name'] = Payment_Mode_Enum::P;
                            $response_data ['payment_type'] = 2;
                            $response_data ['pay_mod_name'] = Payment_Mode_Enum::P;
                        } else if ($trip_details [0]->paymentMode == Payment_Mode_Enum::WALLET) {
                            $response_data ['faretype'] = 3;
                            $response_data ['faretype_name'] = Payment_Mode_Enum::W;
                            $response_data ['payment_type'] = 3;
                            $response_data ['pay_mod_name'] = Payment_Mode_Enum::W;
                        } else if ($trip_details [0]->paymentMode == Payment_Mode_Enum::INVOICE) {
                            $response_data ['faretype'] = $trip_details [0]->paymentMode;
                            $response_data ['faretype_name'] = Payment_Mode_Enum::I;
                            $response_data ['payment_type'] = $trip_details [0]->paymentMode;
                            $response_data ['pay_mod_name'] = Payment_Mode_Enum::I;
                        }
                        // transmission type
                        if ($trip_details [0]->transmissionType == Transmission_Type_Enum::AUTOMATIC) {
                            $response_data ['vehicle_type'] = Transmission_Type_Enum::A;
                        } else if ($trip_details [0]->transmissionType == Transmission_Type_Enum::MANUAL) {
                            $response_data ['vehicle_type'] = Transmission_Type_Enum::M;
                        } else {
                            $response_data ['vehicle_type'] = $trip_details [0]->transmissionType;
                        }

                        // bookedBy
                        $response_data ['bookedby'] = ($trip_details [0]->createdBy) ? $trip_details [0]->userFirstName . ' ' . $trip_details [0]->userLastName : $trip_details [0]->passengerFirstName . ' ' . $trip_details [0]->passengerLastName;

                        // travel status
                        if ($trip_details [0]->tripStatus == Trip_Status_Enum::BOOKED) {
                            $response_data ['travel_status'] = 0;
                        } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::TRIP_COMPLETED) {
                            $response_data ['travel_status'] = 1;
                        } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::IN_PROGRESS) {
                            $response_data ['travel_status'] = 2;
                        } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_PASSENGER) {
                            $response_data ['travel_status'] = 4;
                        } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT) {
                            $response_data ['travel_status'] = 5;
                        } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED) {
                            $response_data ['travel_status'] = 9;
                        } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER) {
                            $response_data ['travel_status'] = 8;
                        } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::TRIP_CONFIRMED) {
                            $response_data ['travel_status'] = 9;
                        } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED) {
                            $response_data ['travel_status'] = 3;
                        } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT) {
                            $response_data ['travel_status'] = 11;
                        } else {
                            $response_data ['travel_status'] = 6;
                        }
                        // travel status
                        $response_data ['has_sub_trips'] = 0;
                        $response_data ['running_subtrip'] = NULL;
                        $response_data ['sub_trips'] = array();
                        //Sub-trips list
                        $sub_trip_list = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId' => $trip_id));
                        if ($sub_trip_list) {
                            $response_data ['has_sub_trips'] = 1;
                            foreach ($sub_trip_list as $sub_trip_data) {
                                $response_data ['sub_trips'][] = array(
                                    'sub_trip_id' => $sub_trip_data->subTripId,
                                    'pickup_location' => $sub_trip_data->pickupLocation,
                                    'pickup_time' => $sub_trip_data->pickupDatetime,
                                    'pickup_latitude' => $sub_trip_data->pickupLatitude,
                                    'pickup_longitude' => $sub_trip_data->pickupLongitude,
                                    'drop_location' => $sub_trip_data->dropLocation,
                                    'drop_latitude' => $sub_trip_data->dropLatitude,
                                    'drop_longitude' => $sub_trip_data->dropLongitude,
                                    'drop_time' => $sub_trip_data->dropDatetime,
                                    'meter_start' => $sub_trip_data->meterStart,
                                    'meter_end' => $sub_trip_data->meterEnd,
                                    'booking_key' => $sub_trip_data->bookingKey

                                );
                                if ($sub_trip_data->dropDatetime == '0000-00-00 00:00:00') {
                                    $response_data ['running_subtrip'] = $sub_trip_data->subTripId;
                                }
                            }
                        }
                        $message = array(
                            "message" => $msg_data ['success'],
                            "detail" => $response_data,
                            "status" => 1,
                            "site_currency" => $currency_details [0]->currencySymbol
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_trip'],
                            "status" => -1,
                            "site_currency" => $currency_details [0]->currencySymbol
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip'],
                        "status" => -1,
                        "site_currency" => $currency_details [0]->currencySymbol
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to change the password
     */
    public function chg_password_driver_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $user_exist = NULL;
        $old_password = NULL;
        $new_password = NULL;
        $confirm_password = NULL;
        $password_status = NULL;
        $update_password = NULL;

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }
        $old_password = $post_data->old_password;
        $new_password = $post_data->new_password;
        $confirm_password = $post_data->confirm_password;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual funtionality
                if ($post_data->id != null) {
                    // validation for checking $old_password, $new_password, $confirm_password data avilablity
                    if ($old_password && $new_password && $confirm_password) {

                        if ($new_password != $confirm_password) {
                            $password_status = -1;
                        } else {
                            if ($post_data->user_type == User_Type_Enum::PASSENGER) {
                                $user_exist = $this->Passenger_Model->getById($post_data->id);
                                if ($user_exist) {
                                    $password = $user_exist->password;
                                    $confirm_password = hash("sha256", $confirm_password);
                                    if ($password == $confirm_password) {
                                        $password_status = -4;
                                    }
                                    if ($password != hash("sha256", $old_password)) {
                                        $password_status = -2;
                                    } else {
                                        $update_password = $this->Passenger_Model->update(array(
                                            'password' => $confirm_password
                                        ), array(
                                            'id' => $user_exist->id
                                        ));
                                    }
                                }
                            } else if ($post_data->user_type == User_Type_Enum::DRIVER) {
                                $user_exist = $this->Driver_Model->getById($post_data->id);
                                if ($user_exist) {
                                    $password = $user_exist->password;
                                    $confirm_password = hash("sha256", $confirm_password);
                                    if ($password == $confirm_password) {
                                        $password_status = -4;
                                    }
                                    if ($password != hash("sha256", $old_password)) {
                                        $password_status = -2;
                                    } else {
                                        $update_password = $this->Driver_Model->update(array(
                                            'password' => $confirm_password
                                        ), array(
                                            'id' => $user_exist->id
                                        ));
                                    }
                                }
                            }
                            if (!$user_exist) {
                                $password_status = -3;
                            }
                            if ($update_password) {
                                $password_status = 1;
                            }
                        }
                    }
                    switch ($password_status) {
                        case -1 :
                            $message = array(
                                "message" => $msg_data ['confirm_password'],
                                "status" => -1
                            );
                            break;
                        case -2 :
                            $message = array(
                                "message" => $msg_data ['invalid_user'],
                                "status" => -2
                            );
                            break;
                        case -3 :
                            $message = array(
                                "message" => $msg_data ['incorrect_old_password'],
                                "status" => -3
                            );
                            break;
                        case 1 :
                            $message = array(
                                "message" => $msg_data ['password_success'],
                                "status" => 1
                            );
                            break;
                        case -4 :
                            $message = array(
                                "message" => $msg_data ['old_new_password_same'],
                                "status" => -4
                            );
                            break;
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => 0
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * sumbit rating to server
     */
    public function update_ratings_comments_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $user_exist = NULL;
        $trip_id = NULL;
        $ratings = NULL;
        $comments = NULL;
        $password_status = NULL;
        $user_type = NULL;

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $trip_id = $post_data->pass_id;
        $ratings = $post_data->ratings;
        $comments = $post_data->comments;
        $user_type = $post_data->user_type;
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual funtionality
                if ($trip_id != NULL) {
                    if ($ratings && $post_data->user_type) {
                        if ($post_data->user_type == User_Type_Enum::PASSENGER) {
                            $update_data = array(
                                'passengerRating' => $ratings,
                                'passengerComments' => $comments
                            );
                        } else if ($post_data->user_type == User_Type_Enum::DRIVER) {
                            $update_data = array(
                                'driverRating' => $ratings,
                                'driverComments' => $comments
                            );
                        }
                        $update_result = $this->Trip_Details_Model->update($update_data, array(
                            'id' => $trip_id
                        ));
                        if ($update_result) {
                            $message = array(
                                "message" => $msg_data ['rating_success'],
                                "status" => 1
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['rating_failed'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['mandatory_data'],
                            "detail" => '',
                            "status" => -2
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => -401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => -401
            );
        }
        echo json_encode($message);
    }

    // Passenger related 33 webservices

    /**
     * sign-up through google plus
     */
    public function googleplus_connect_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();

        $message = '';
        $email = NULL;
        $first_name = NULL;
        $last_name = NULL;
        $mobile = NULL;
        $userid = NULL;
        $password = Null;
        $device_id = NULL;
        $device_token = NULL;
        $device_type = NULL;
        $refered_code = NULL;
        $profile_image = NULL;
        $referral_passenger_id = Null;
        $passenger_referral_insert_id = NULL;
        $passenger_id = NULL;
        $city_id = NULL;
        $user_exists = array();
        $update_passenger = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();

        $email = $post_data->email;
        $first_name = $post_data->fname;
        $last_name = $post_data->lname;
        $mobile = $post_data->mobile;
        $userid = $post_data->userid;
        $device_id = $post_data->deviceid;
        $device_token = $post_data->devicetoken;
        $device_type = $post_data->devicetype;
        $refered_code = $post_data->refered_code;
        $city_id = $post_data->city_id;
        $password = random_string('alnum', 8);

        // actual functionality
        // to get site information details
        $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
            'id' => 1
        ));

        if ($email && $mobile && $userid) {
            $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                'email' => $email
            ));
            if ($user_exists) {
                $message = array(
                    "message" => $msg_data ['email_exists'],
                    "status" => 2
                );
            } else if (!$user_exists) {
                $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                    'mobile' => $mobile
                ));
                if ($user_exists) {
                    $message = array(
                        "message" => $msg_data ['mobile_exists'],
                        "status" => 2
                    );
                } else if (!$user_exists) {
                    if ($refered_code) {
                        $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                            'passengerCode' => $refered_code
                        ));
                        if (!$user_exists) {
                            $message = array(
                                "message" => $msg_data ['valid_referral_code'],
                                "status" => 3
                            );
                        } else {
                            $referral_passenger_id = $user_exists->id;
                        }
                    } else if (!$user_exists) {

                        $password = hash("sha256", $password);
                        // @todo generate 6 digit otp
                        $otp = str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT);
                        // @todo unique passenger code
                        $passenger_code = trim(substr(strtoupper(str_replace(' ', '', $first_name . '' . $last_name)), 0, 5));
                        $passenger_code .= $this->Passenger_Model->getAutoIncrementValue();

                        $insert_data = array(
                            'firstName' => $first_name,
                            'lastName' => $last_name,
                            'email' => $email,
                            'mobile' => $mobile,
                            'password' => $password,
                            'otp' => $otp,
                            'deviceId' => $device_id,
                            'deviceType' => ($device_type) ? Device_Type_Enum::ANDROID : Device_Type_Enum::IPHONE,
                            'cityId' => $city_id,
                            'companyId' => DEFAULT_COMPANY_ID,
                            'passengerCode' => $passenger_code,
                            'referrerCode' => ($refered_code) ? $refered_code : '',
                            'sourceLead' => 'WEB',
                            'signupFrom' => Signup_Type_Enum::MOBILE,
                            'registerType' => Register_Type_Enum::GOOGLE
                        );
                        $passenger_id = $this->Passenger_Model->insert($insert_data);

                        if ($referral_passenger_id) {
                            $insert_data = array(
                                'passengerId' => $referral_passenger_id,
                                'registeredPassengerId' => $passenger_id,
                                'earnedAmount' => $site_setting_info->referralDiscountAmount,
                                'status' => Status_Type_Enum::ACTIVE
                            );
                            $passenger_referral_insert_id = $this->Passenger_Referral_Details_Model->insert($insert_data);
                        }

                        if ($passenger_id) {
                            /*
							 * if (array_key_exists ( 'profile_image', $post_data ) && $profile_image) {
							 * $data ['file_name'] = $this->getbase64ImageUrl ( $passenger_id, $profile_image );
							 *
							 * if ($data) {
							 *
							 * $update_passenger = $this->Passenger_Model->update ( array (
							 * 'profileImage' => $data ['file_name']
							 * ), array (
							 * 'id' => $passenger_id
							 * ) );
							 * }
							 * }
							 */

                            // response data
                            $response_data = array(
                                "message" => $msg_data ['signup_success'],
                                'email' => $email,
                                'mobile' => $mobile,
                                'otp' => $otp
                            );

                            if ($passenger_id) {

                                if (SMS && $passenger_id) {
                                    $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                        'smsTitle' => 'otp'
                                    ));

                                    $message_data = $message_details->smsContent;
                                    // $message = str_replace("##USERNAME##",$name,$message);
                                    $message_data = str_replace("##OTP##", $otp, $message_data);
                                    // print_r($message);exit;
                                    if ($mobile != "") {

                                        $this->Api_Webservice_Model->sendSMS($mobile, $message_data);
                                    }
                                }

                                $from = SMTP_EMAIL_ID;
                                $to = $email;
                                $subject = 'Zuver OTP';
                                $data = array(
                                    'otp' => $otp
                                );
                                $message_data = $this->load->view('emailtemplate/otp', $data, TRUE);

                                if (SMTP && $passenger_id) {

                                    if ($to) {
                                        $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                                    }
                                } else {

                                    // To send HTML mail, the Content-type header must be set
                                    $headers = 'MIME-Version: 1.0' . "\r\n";
                                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                    // Additional headers
                                    $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                                    $headers .= 'To: <' . $to . '>' . "\r\n";
                                    if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                                        mail($to, $subject, $message_data, $headers);
                                    }
                                }
                                $message = array(
                                    "message" => $msg_data ['otp'],
                                    'status' => 1
                                );
                                if ($passenger_id) {
                                    $message = array(
                                        "message" => $msg_data ['signup_success'],
                                        "detail" => $response_data,
                                        "status" => 1
                                    );
                                } /*
								   * else if (! $update_passenger && $passenger_id) {
								   * $message = array (
								   * "message" => $msg_data['signup_success_image_failed'],
								   * "detail" => $response_data,
								   * "status" => 4
								   * );
								   * }
								   */
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['signup_failed'],
                                "status" => 4
                            );
                        }
                    }
                }
            }
        } else {
            $message = array(
                "message" => $msg_data ['mandatory_data'],
                "status" => 5
            );
        }

        echo json_encode($message);
    }

    /**
     * sign-up through facebook
     */
    public function facebook_connect_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $email = NULL;
        $first_name = NULL;
        $last_name = NULL;
        $mobile = NULL;
        $userid = NULL;
        $city_id = NULL;
        $password = NULL;
        $device_id = NULL;
        $device_token = NULL;
        $device_type = NULL;
        $refered_code = NULL;
        $profile_image = NULL;
        $referral_passenger_id = Null;
        $passenger_referral_insert_id = NULL;
        $passenger_id = NULL;
        $user_exists = array();
        $update_passenger = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();

        $email = $post_data->email;
        $first_name = $post_data->fname;
        $last_name = $post_data->lname;
        $mobile = $post_data->mobile;
        $userid = $post_data->userid;
        $device_id = $post_data->deviceid;
        $device_token = $post_data->devicetoken;
        $device_type = $post_data->devicetype;
        $refered_code = $post_data->refered_code;
        $refered_code = $post_data->refered_code;
        $profile_image = $post_data->profile_image;
        $city_id = $post_data->city_id;

        // actual functionality
        // to get site information details
        $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
            'id' => 1
        ));

        if ($email && $mobile && $userid) {
            $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                'email' => $email
            ));
            if ($user_exists) {
                $message = array(
                    "message" => $msg_data ['email_exists'],
                    "status" => 2
                );
            } else if (!$user_exists) {
                $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                    'mobile' => $mobile
                ));
                if ($user_exists) {
                    $message = array(
                        "message" => $msg_data ['mobile_exists'],
                        "status" => 2
                    );
                } else if (!$user_exists) {
                    if ($refered_code) {
                        $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                            'passengerCode' => $refered_code
                        ));
                        if (!$user_exists) {
                            $message = array(
                                "message" => $msg_data ['valid_referral_code'],
                                "status" => 3
                            );
                        } else {
                            $referral_passenger_id = $user_exists->id;
                        }
                    } else if (!$user_exists) {

                        $password = hash("sha256", $password);
                        // @todo generate 6 digit otp
                        $otp = str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT);
                        // @todo unique passenger code
                        $passenger_code = trim(substr(strtoupper(str_replace(' ', '', $first_name . '' . $last_name)), 0, 5));
                        $passenger_code .= $this->Passenger_Model->getAutoIncrementValue();

                        $insert_data = array(
                            'firstName' => $first_name,
                            'lastName' => $last_name,
                            'email' => $email,
                            'mobile' => $mobile,
                            'password' => $password,
                            'otp' => $otp,
                            'deviceId' => $device_id,
                            'deviceType' => ($device_type) ? Device_Type_Enum::ANDROID : Device_Type_Enum::IPHONE,
                            'cityId' => $city_id,
                            'companyId' => DEFAULT_COMPANY_ID,
                            'passengerCode' => $passenger_code,
                            'referrerCode' => ($refered_code) ? $refered_code : '',
                            'sourceLead' => 'WEB',
                            'signupFrom' => Signup_Type_Enum::MOBILE,
                            'registerType' => Register_Type_Enum::FACEBOOK
                        );
                        $passenger_id = $this->Passenger_Model->insert($insert_data);

                        if ($referral_passenger_id) {
                            $insert_data = array(
                                'passengerId' => $referral_passenger_id,
                                'registeredPassengerId' => $passenger_id,
                                'earnedAmount' => $site_setting_info->referralDiscountAmount,
                                'status' => Status_Type_Enum::ACTIVE
                            );
                            $passenger_referral_insert_id = $this->Passenger_Referral_Details_Model->insert($insert_data);
                        }

                        if ($passenger_id) {
                            if ($profile_image) {

                                $file_name = "profile_image.jpg";
                                $file = passenger_content_url() . "" . $passenger_id . "/" . $file_name;
                                $data ['file_name'] = file_put_contents($file_name, file_get_contents($profile_image)); // $this->getbase64ImageUrl ( $passenger_id, $profile_image );

                                if ($data) {

                                    $update_passenger = $this->Passenger_Model->update(array(
                                        'profileImage' => $file_name
                                    ), array(
                                        'id' => $passenger_id
                                    ));
                                }
                            }

                            // response data
                            $response_data = array(
                                "message" => $msg_data ['signup_success'],
                                'email' => $email,
                                'mobile' => $mobile,
                                'otp' => $otp
                            );

                            if ($passenger_id) {

                                if (SMS && $passenger_id) {
                                    $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                        'smsTitle' => 'otp'
                                    ));

                                    $message_data = $message_details->smsContent;
                                    // $message = str_replace("##USERNAME##",$name,$message);
                                    $message_data = str_replace("##OTP##", $otp, $message_data);
                                    // print_r($message);exit;
                                    if ($mobile != "") {

                                        $this->Api_Webservice_Model->sendSMS($mobile, $message_data);
                                    }
                                }

                                $from = SMTP_EMAIL_ID;
                                $to = $email;
                                $subject = 'Zuver OTP';
                                $data = array(
                                    'otp' => $otp
                                );
                                $message_data = $this->load->view('emailtemplate/otp', $data, TRUE);

                                if (SMTP && $passenger_id) {

                                    if ($to) {
                                        $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                                    }
                                } else {

                                    // To send HTML mail, the Content-type header must be set
                                    $headers = 'MIME-Version: 1.0' . "\r\n";
                                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                    // Additional headers
                                    $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                                    $headers .= 'To: <' . $to . '>' . "\r\n";
                                    if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                                        mail($to, $subject, $message_data, $headers);
                                    }
                                }
                                $message = array(
                                    "message" => $msg_data ['otp'],
                                    'status' => 1
                                );
                                if ($update_passenger && $passenger_id) {
                                    $message = array(
                                        "message" => $msg_data ['signup_success'],
                                        "detail" => $response_data,
                                        "status" => 1
                                    );
                                } else if (!$update_passenger && $passenger_id) {
                                    $message = array(
                                        "message" => $msg_data ['signup_success_image_failed'],
                                        "detail" => $response_data,
                                        "status" => 1
                                    );
                                }
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['signup_failed'],
                                "status" => 4
                            );
                        }
                    }
                }
            }
        } else {
            $message = array(
                "message" => $msg_data ['mandatory_data'],
                "status" => 5
            );
        }

        echo json_encode($message);
    }

    /**
     * normal sign-up
     */
    public function passenger_account_details_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $email = NULL;
        $first_name = NULL;
        $last_name = NULL;
        $mobile = NULL;
        $password = NULL;
        $city_id = NULL;
        $device_id = NULL;
        $device_token = NULL;
        $device_type = NULL;
        $refered_code = NULL;
        $profile_image = NULL;
        $referral_passenger_id = Null;
        $passenger_referral_insert_id = NULL;
        $passenger_id = NULL;
        $user_exists = array();
        $referral_passenger_exists = array();
        $update_passenger = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        /*
		 * foreach ( getallheaders () as $name => $value ) {
		 * if ($name == 'authKey') {
		 * $auth_key = $value;
		 * }
		 * if ($name == 'userId') {
		 * $auth_user_id = $value;
		 * }
		 * if ($name == 'userType') {
		 * $auth_user_type = $value;
		 * }
		 * }
		 */

        $email = $post_data->email;
        $first_name = $post_data->fname;
        $last_name = $post_data->lname;
        $mobile = $post_data->phone;
        $password = $post_data->password;
        $device_id = $post_data->devicetoken;
        $device_id = $post_data->deviceid;
        $device_token = $post_data->devicetoken;
        $device_type = $post_data->devicetype;
        $refered_code = $post_data->refered_code;
        $profile_image = $post_data->profile_image;
        $city_id = $post_data->city_id;
        // actual functionality
        // to get site information details
        $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
            'id' => 1
        ));

        if ($email && $mobile && $password) {
            $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                'email' => $email
            ));
            if ($user_exists) {
                $message = array(
                    "message" => $msg_data ['email_exists'],
                    "status" => 2
                );
            } else if (!$user_exists) {
                $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                    'mobile' => $mobile
                ));
                if ($user_exists) {
                    $message = array(
                        "message" => $msg_data ['mobile_exists'],
                        "status" => 2
                    );
                } else if (!$user_exists) {
                    if ($refered_code) {
                        $referral_passenger_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                            'passengerCode' => $refered_code
                        ));
                        if ($referral_passenger_exists) {
                            $referral_passenger_id = $referral_passenger_exists->id;

                        } else {

                            $message = array(
                                "message" => $msg_data ['valid_referral_code'],
                                "status" => 3
                            );
                        }
                    }


                    $password = hash("sha256", $password);
                    // @todo generate 6 digit otp
                    $otp = str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT);
                    // $otp=123456;
                    // @todo unique passenger code
                    $passenger_code = trim(substr(strtoupper(str_replace(' ', '', $first_name . '' . $last_name)), 0, 5));
                    $passenger_code .= $this->Passenger_Model->getAutoIncrementValue();

                    $insert_data = array(
                        'firstName' => $first_name,
                        'lastName' => $last_name,
                        'email' => $email,
                        'mobile' => $mobile,
                        'password' => $password,
                        'otp' => $otp,
                        'deviceId' => $device_id,
                        'deviceType' => ($device_type) ? Device_Type_Enum::ANDROID : Device_Type_Enum::IPHONE,
                        'cityId' => $city_id,
                        'companyId' => DEFAULT_COMPANY_ID,
                        'passengerCode' => $passenger_code,
                        'referrerCode' => ($refered_code) ? $refered_code : '',
                        'sourceLead' => 'WEB',
                        'signupFrom' => Signup_Type_Enum::MOBILE,
                        'registerType' => Register_Type_Enum::MANUAL
                    );
                    $passenger_id = $this->Passenger_Model->insert($insert_data);

                    if ($referral_passenger_id) {
                        $insert_data = array(
                            'passengerId' => $referral_passenger_id,
                            'registeredPassengerId' => $passenger_id,
                            'earnedAmount' => $site_setting_info->referralDiscountAmount,
                            'status' => Status_Type_Enum::ACTIVE
                        );
                        $passenger_referral_insert_id = $this->Passenger_Referral_Details_Model->insert($insert_data);
                    }

                    if ($passenger_id) {
                        if (array_key_exists('profile_image', $post_data) && $profile_image) {
                            $data ['file_name'] = $this->getbase64ImageUrl($passenger_id, $profile_image);

                            if ($data) {

                                $update_passenger = $this->Passenger_Model->update(array(
                                    'profileImage' => $data ['file_name']
                                ), array(
                                    'id' => $passenger_id
                                ));
                            }
                        }

                        // response data
                        $response_data = array(
                            "message" => $msg_data ['signup_success'],
                            'email' => $email,
                            'mobile' => $mobile,
                            'otp' => $otp
                        );

                        if ($passenger_id) {

                            if (SMS && $passenger_id) {
                                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                    'smsTitle' => 'otp'
                                ));

                                $message_data = $message_details->smsContent;
                                // $message = str_replace("##USERNAME##",$name,$message);
                                $message_data = str_replace("##OTP##", $otp, $message_data);
                                // print_r($message);exit;
                                if ($mobile != "") {

                                    $this->Api_Webservice_Model->sendSMS($mobile, $message_data);
                                }
                            }

                            $from = SMTP_EMAIL_ID;
                            $to = $email;
                            $subject = 'Zuver OTP';
                            $data = array(
                                'otp' => $otp
                            );
                            $message_data = $this->load->view('emailtemplate/otp', $data, TRUE);

                            if (SMTP && $passenger_id) {

                                if ($to) {
                                    $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                                }
                            } else {

                                // To send HTML mail, the Content-type header must be set
                                $headers = 'MIME-Version: 1.0' . "\r\n";
                                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                // Additional headers
                                $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                                $headers .= 'To: <' . $to . '>' . "\r\n";
                                if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                                    mail($to, $subject, $message_data, $headers);
                                }
                            }

                            if ($update_passenger && $passenger_id) {
                                $message = array(
                                    "message" => $msg_data ['signup_success'],
                                    "detail" => $response_data,
                                    "status" => 1
                                );
                            } else if (!$update_passenger && $passenger_id) {
                                $message = array(
                                    "message" => $msg_data ['signup_success_image_failed'],
                                    "detail" => $response_data,
                                    "status" => 1
                                );
                            }
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['signup_failed'],
                            "status" => 4
                        );
                    }

                }
            }
        } else {
            $message = array(
                "message" => $msg_data ['mandatory_data'],
                "status" => 5
            );
        }

        echo json_encode($message);
    }

    /**
     * otp verification
     */
    public function check_otp_activate_post()
    {

        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();

        $device_token = NULL;
        $device_id = NULL;
        $device_type = NULL;
        $message = '';
        $email = NULL;
        $otp = NULL;
        $check_otp = NULL;
        $signup_form = NULL;
        $user_exists = array();
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        /*
		 * foreach ( getallheaders () as $name => $value ) {
		 * if ($name == 'authKey') {
		 * $auth_key = $value;
		 * }
		 * if ($name == 'userId') {
		 * $auth_user_id = $value;
		 * }
		 * if ($name == 'userType') {
		 * $auth_user_type = $value;
		 * }
		 * }
		 */

        $email = $post_data->email;
        $otp = $post_data->otp;
        $device_token = $post_data->devicetoken;
        $device_id = $post_data->device_id;
        $device_type = $post_data->device_type;
        /*
		 * $user_type = $post_data->userType;
		 * // Checked wheather authentication key availablilty for specfic user(passenger/driver)
		 * $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray ( array (
		 * 'key' => $auth_key,
		 * 'userId' => $auth_user_id,
		 * 'userType' => $auth_user_type
		 * ), 'id' );
		 */

        // actual functionality
        // to get site information details
        $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
            'id' => 1
        ));

        // to get currency code/currency symbol currencySymbol
        $currency_details = $this->Country_Model->getColumnByKeyValueArray('currencySymbol', array(
            'id' => $site_setting_info->countryId
        ));

        if ($email && $otp) {
            $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                'email' => $email,
                'otp' => $otp
            ));
            if ($user_exists) {

                $check_otp = $this->Passenger_Model->update(array(
                    'activationStatus' => Status_Type_Enum::ACTIVE,
                    'deviceId' => $device_id,
                    'deviceType' => ($device_type) ? 'A' : 'I',
                    'loginStatus' => Status_Type_Enum::ACTIVE,
                    'lastLoggedIn' => getCurrentDateTime()
                ), array(
                    'email' => $email,
                    'otp' => $otp
                ));
            }

            if ($user_exists && $check_otp) {

                // Auth details
                $key = random_string('alnum', 16);
                $auth_key = hash("sha256", $key);
                $auth_key_exists = $this->Auth_Key_Model->getOneByKeyValueArray(array(
                    'userId' => $user_exists->id,
                    'userType' => User_Type_Enum::PASSENGER
                ));

                // If user already exists update auth key
                if ($auth_key_exists) {
                    $this->Auth_Key_Model->update(array(
                        'key' => $auth_key
                    ), array(
                        'userId' => $user_exists->id,
                        'userType' => User_Type_Enum::PASSENGER
                    ));
                }  // create new auth key for passenger
                else {
                    $this->Auth_Key_Model->insert(array(
                        'key' => $auth_key,
                        'userId' => $user_exists->id,
                        'userType' => User_Type_Enum::PASSENGER
                    ));
                }
                // Authentication details
                $auth_details = array(
                    'authKey' => $auth_key,
                    'userId' => $user_exists->id,
                    'userType' => User_Type_Enum::PASSENGER
                );

                if (SMS && $check_otp) {
                    $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                        'smsTitle' => 'account_create_sms'
                    ));

                    $message_data = $message_details->smsContent;

                    /*
					 * $message_data = str_replace ( "##USERNAME##", $user_exists->firstName . ' ' . $user_exists->lastName, $message_data );
					 * $message_data = str_replace ( "##OTP##", $otp, $message_data );
					 */

                    // print_r($message);exit;
                    if ($user_exists->mobile != "") {

                        // $this->Api_Webservice_Model->sendSMS ( $user_exists->mobile, $message_data );
                    }
                }

                $from = SMTP_EMAIL_ID;
                $to = $user_exists->email;
                $subject = 'Get Ready to ride with Zuver!';
                $data = array(
                    'user_name' => $user_exists->firstName . ' ' . $user_exists->lastName,
                    'mobile' => $user_exists->mobile,
                    'email' => $user_exists->email,
                    'password' => NULL
                );
                $message_data = $this->load->view('emailtemplate/passenger_register', $data, TRUE);

                if (SMTP && $check_otp) {

                    if ($to) {
                        $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                    }
                } else {

                    // To send HTML mail, the Content-type header must be set
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    // Additional headers
                    $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                    $headers .= 'To: <' . $to . '>' . "\r\n";
                    if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                        mail($to, $subject, $message_data, $headers);
                    }
                }

                // response_data
                // @todo check again
                $tell_to_friend_subject = 'Use my Zuver invite code ##REFERRALCODE##, and get a free ride worth Rs. ##REFERRALDISCOUNT##. Download the app & redeem it! App Store: apple.co/1T3N8oD Play Store: http://bit.ly/1KqxoVy'; // $site_setting_info[0]->passengerTellToFriendMsg;
                $tell_to_friend_subject = str_replace('##REFERRALCODE##', ($user_exists->passengerCode) ? $user_exists->passengerCode : '', $tell_to_friend_subject);
                $tell_to_friend_subject = str_replace('##REFERRALDISCOUNT##', $site_setting_info->referralDiscountAmount, $tell_to_friend_subject);

                $share_content = '##USERNAME## is using a Zuver & is currently located at #location#. The request to share this information with you was made by ##USERNAME## through the Zuver app.';
                $share_content = str_replace('##USERNAME##', $user_exists->firstName . ' ' . $user_exists->lastName, $share_content);
                // to get signUpFrom name only for passenger
                if ($user_exists->signupFrom == Signup_Type_Enum::NONE) {
                    $signup_form = Signup_Type_Enum::N;
                }
                if ($user_exists->signupFrom == Signup_Type_Enum::MOBILE) {
                    $signup_form = Signup_Type_Enum::M;
                }
                if ($user_exists->signupFrom == Signup_Type_Enum::WEBSITE) {
                    $signup_form = Signup_Type_Enum::W;
                }
                if ($user_exists->signupFrom == Signup_Type_Enum::BACKEND) {
                    $signup_form = Signup_Type_Enum::B;
                }

                $response_data = array(
                    "auth_details" => $auth_details,
                    "id" => $user_exists->id,
                    "name" => $user_exists->firstName,
                    "lastname" => $user_exists->lastName,
                    "email" => $user_exists->email,
                    "profile_image" => ($user_exists->profileImage) ? passenger_content_url('/' . $user_exists->id . '/' . $user_exists->profileImage) : '',
                    "phone" => $user_exists->mobile,
                    "referral_code" => ($user_exists->passengerCode) ? $user_exists->passengerCode : '',
                    "referral_amount" => $site_setting_info->referralDiscountAmount,
                    "login_from" => ($signup_form) ? $signup_form : '',
                    "telltofriend_message" => ($site_setting_info->passengerTellToFriendMsg) ? $site_setting_info->passengerTellToFriendMsg : '',
                    "aboutpage_description" => $site_setting_info->description,
                    "tell_to_friend_subject" => $tell_to_friend_subject,
                    "site_currency" => $currency_details [0]->currencySymbol,
                    'skip_credit' => SKIP_CREDIT_CARD,
                    'metric' => UNIT_NAME,
                    "share_content" => $share_content
                );
                // response data
                $message = array(
                    "message" => $msg_data ['activation_success'],
                    "detail" => $response_data,
                    "status" => 1
                );
            } else {
                $message = array(
                    "message" => $msg_data ['invaild_otp'],
                    "detail" => array(
                        "message" => $msg_data ['invaild_otp']
                    ),
                    "p_details" => $array,
                    "status" => 2
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['mandatory_data'],
                "detail" => $msg_data ['mandatory_data'],
                "p_details" => array(
                    'email' => $post_data->email,
                    'otp' => $post_data->otp
                ),
                "status" => 3
            );
        }

        echo json_encode($message);
    }

    /**
     * re-send otp
     */
    public function resend_otp_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $otp = NULL;
        $user_exists = NULL;
        $passenger_id = NULL;
        $email = NULL;

        $update_otp = NULL;
        $user_type = array();

        // Posted json data
        $post_data = $this->get_post_data();
        /*
		 * foreach ( getallheaders () as $name => $value ) {
		 * if ($name == 'authKey') {
		 * $auth_key = $value;
		 * }
		 * if ($name == 'userId') {
		 * $auth_user_id = $value;
		 * }
		 * if ($name == 'userType') {
		 * $auth_user_type = $value;
		 * }
		 * }
		 */

        $email = $post_data->email;
        $user_type = $post_data->user_type;

        /*
		 * // Checked wheather authentication key availablilty for specfic user(passenger/driver)
		 * $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray ( array (
		 * 'key' => $auth_key,
		 * 'userId' => $auth_user_id,
		 * 'userType' => $auth_user_type
		 * ), 'id' );
		 * if ($data_auth_key) {
		 * if ($data_auth_key->key == $auth_key) {
		 */

        // actual functionality
        $otp = str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT);

        if ($email) {
            if ($user_type == User_Type_Enum::PASSENGER) {

                $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                    'email' => $email
                ));
                if ($user_exists) {
                    $update_otp = $this->Passenger_Model->update(array(
                        'otp' => $otp
                    ), array(
                        'email' => $email
                    ));
                }
            } else if ($user_type == User_Type_Enum::DRIVER) {

                $user_exists = $this->Driver_Model->getOneByKeyValueArray(array(
                    'email' => $email
                ));
                if ($user_exists) {
                    $update_otp = $this->Driver_Model->update(array(
                        'otp' => $otp
                    ), array(
                        'email' => $email
                    ));
                }
            }
            if ($user_exists && $update_otp) {

                if (SMS && $update_otp) {
                    $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                        'smsTitle' => 'otp'
                    ));

                    $message_data = $message_details->smsContent;

                    $message_data = str_replace("##USERNAME##", $user_exists->firstName . ' ' . $user_exists->lastName, $message_data);
                    $message_data = str_replace("##OTP##", $otp, $message_data);

                    // print_r($message);exit;
                    if ($user_exists->mobile != "") {

                        $this->Api_Webservice_Model->sendSMS($user_exists->mobile, $message_data);
                    }
                }

                $from = SMTP_EMAIL_ID;
                $to = $user_exists->email;
                $subject = 'Zuver OTP';
                $data = array(
                    'otp' => $otp,
                    'user_name' => $user_exists->firstName . ' ' . $user_exists->lastName
                );
                $message_data = $this->load->view('emailtemplate/otp', $data, TRUE);

                if (SMTP && $update_otp) {

                    if ($to) {
                        $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                    }
                } else {

                    // To send HTML mail, the Content-type header must be set
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    // Additional headers
                    $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                    $headers .= 'To: <' . $to . '>' . "\r\n";
                    if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                        mail($to, $subject, $message_data, $headers);
                    }
                }

                $message = array(
                    "message" => $msg_data ['otp_success'],
                    "detail" => array(
                        'email' => $user_exists->email
                    ),
                    "status" => 1
                );
            } else {
                $message = array(
                    "message" => $msg_data ['failed'],
                    "status" => 4
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['invalid_email'],
                "status" => -1
            );
        }
        /*
		 * } else {
		 * $message = array (
		 * "message" => $msg_data ['auth_failed'],
		 * "status" => 401
		 * );
		 * }
		 * } else {
		 * $message = array (
		 * "message" => $msg_data ['auth_failed'],
		 * "status" => 401
		 * );
		 * }
		 */
        echo json_encode($message);
    }

    /**
     * terms and conditions
     */
    public function dynamic_page_post()
    {
        // intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = array();
        $data = array();
        $page_name = NULL;
        $device_type = NULL;
        $response_data = array();
        $message = '';
        // Posted json data

        $post_data = $this->get_post_data(); // $this->input->post ();
        // debug_exit($post_data->phone);

        $page_name = $post_data->pagename;
        $device_type = $post_data->device_type;
        if ($page_name) {
            $data_content = $this->Menu_Content_Details_Model->getOneByKeyValueArray(array(
                'name' => $page_name,
                'status' => Status_Type_Enum::ACTIVE
            ));
            if ($data_content) {
                if ($device_type == 1) {
                    echo $data_content->content;
                    //break;
                } else if ($device_type == 2) {

                    $response_data ['content'] = htmlentities($data_content->content);
                    $response_data ['title'] = $data_content->title;
                    $message = array(
                        "message" => $msg_data ['success'],
                        "detail" => $response_data,
                        "status" => 1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['data_not_found'],
                    "status" => 2
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['invalid_page'],
                "status" => -1
            );
        }
        echo json_encode($message);
    }

    /**
     * normal login
     */
    public function passenger_login_post()
    {
        // we need create API key for login user by adding keys &
        // intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = array();
        $data = array();
        $passenger_exists = NULL;
        $passenger_update = NULL;
        $response_data = array();

        // authentication generated key
        $auth_key = NULL; // $post_data ['authKey'];
        $auth_user_id = NULL; // $post_data ['userId'];
        $auth_user_type = NULL; // $post_data ['userType'];
        $auth_details = array();

        $message = '';
        // Posted json data

        $post_data = $this->get_post_data(); // $this->input->post ();
        // debug_exit($post_data->phone);
        $mobile = $post_data->phone;
        $email = $post_data->email;
        $password = $post_data->password;
        $device_token = $post_data->devicetoken;
        $device_id = $post_data->deviceid;
        $device_type = $post_data->devicetype;
        if ($mobile || $email) {
            if ($email && $password) {
                $passenger_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                    'email' => $email,
                    'password' => hash("sha256", $password),
                    'status' => 'Y'
                ));
            }
            if (!$passenger_exists && $mobile && $password) {
                $passenger_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                    'mobile' => $mobile,
                    'password' => hash("sha256", $password),
                    'status' => 'Y'
                ));
            }
            if ($passenger_exists) {
                $auth_data = array();
                // update the paggenger deviceId & device type by default is 'A'=Android, 'I'=Iphone, 'W'=Windows,'N'=No device
                // @todo change the device based on device
                $passenger_update = $this->Passenger_Model->update(array(
                    'deviceId' => $device_id,
                    'deviceType' => ($device_type) ? 'A' : 'I',
                    'signupFrom' => Signup_Type_Enum::MOBILE,
                    'loginStatus' => Status_Type_Enum::ACTIVE,
                    'lastLoggedIn' => getCurrentDateTime()
                ), array(
                    'id' => $passenger_exists->id
                ));

                // Send OTP if passenger activation staus is failed
                if ($passenger_exists->activationStatus == Status_Type_Enum::INACTIVE) {
                    $detail = array(
                        "email" => $passenger_exists->email,
                        "passenger_id" => $passenger_exists->id,
                        "otp" => $passenger_exists->otp
                    );
                    $message = array(
                        "message" => $msg_data ['otp_verify_failed'],
                        "detail" => $detail,
                        "status" => 5
                    );
                }  // Send response data with Auth details & passenger details if passenger activation staus is success
                else {

                    $key = random_string('alnum', 16);
                    $auth_key = hash("sha256", $key);
                    $auth_key_exists = $this->Auth_Key_Model->getOneByKeyValueArray(array(
                        'userId' => $passenger_exists->id,
                        'userType' => User_Type_Enum::PASSENGER
                    ));

                    // If user already exists update auth key
                    if ($auth_key_exists) {
                        $this->Auth_Key_Model->update(array(
                            'key' => $auth_key
                        ), array(
                            'userId' => $passenger_exists->id,
                            'userType' => User_Type_Enum::PASSENGER
                        ));
                    }  // create new auth key for passenger
                    else {
                        $this->Auth_Key_Model->insert(array(
                            'key' => $auth_key,
                            'userId' => $passenger_exists->id,
                            'userType' => User_Type_Enum::PASSENGER
                        ));
                    }
                    // Authentication details
                    $auth_details = array(
                        'authKey' => $auth_key,
                        'userId' => $passenger_exists->id,
                        'userType' => User_Type_Enum::PASSENGER
                    );
                    // to get site information details
                    $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                        'id' => 1
                    ));
                    // to get currency code/currency symbol currencySymbol
                    $currency_details = $this->Country_Model->getColumnByKeyValueArray('currencySymbol', array(
                        'id' => $site_setting_info->countryId
                    ));
                    // response_data
                    // @todo check again
                    $tell_to_friend_subject = 'Use my Zuver invite code ##REFERRALCODE##, and get a free ride worth Rs. ##REFERRALDISCOUNT##. Download the app & redeem it! App Store: apple.co/1T3N8oD Play Store: http://bit.ly/1KqxoVy'; // $site_setting_info[0]->passengerTellToFriendMsg;
                    $tell_to_friend_subject = str_replace('##REFERRALCODE##', $passenger_exists->passengerCode, $tell_to_friend_subject);
                    $tell_to_friend_subject = str_replace('##REFERRALDISCOUNT##', $site_setting_info->referralDiscountAmount, $tell_to_friend_subject);

                    $share_content = '##USERNAME## is using a Zuver & is currently located at ##LOCATION##. The request to share this information with you was made by ##USERNAME## through the Zuver app.';
                    $share_content = str_replace('##USERNAME##', $passenger_exists->firstName . ' ' . $passenger_exists->lastName, $share_content);
                    $share_content = str_replace('##LOCATION##', $passenger_exists->cityId, $share_content);
                    $response_data = array(
                        'auth_details' => $auth_details,
                        'id' => $passenger_exists->id,
                        'name' => $passenger_exists->firstName . ' ' . $passenger_exists->lastName,
                        'email' => $passenger_exists->email,
                        'phone' => $passenger_exists->mobile,
                        'wallet_amount' => $passenger_exists->walletAmount,
                        'profile_image' => passenger_content_url('/' . $passenger_exists->id . '/' . $passenger_exists->profileImage),
                        'login_from' => $passenger_exists->signupFrom,
                        'referral_code' => $passenger_exists->passengerCode,
                        'referral_amount' => $site_setting_info->referralDiscountAmount,
                        'site_currency' => $currency_details [0]->currencySymbol,
                        'aboutpage_description' => $site_setting_info->description,
                        'telltofriend_message' => $site_setting_info->passengerTellToFriendMsg,
                        'tell_to_friend_subject' => '',
                        'share_content' => $share_content,
                        'model_details' => '', // @todo need to check
                        'credit_card_status' => 0
                    ); // by default it is set zero(0)

                    /*
					 * $total_array ['share_content'] = str_replace ( "##USERNAME##", $total_array ['name'], __ ( 'telltofriend_content' ) );
					 * $total_array ['tell_to_friend_subject'] = str_replace ( "##REFERRALCODE##", $total_array ['referral_code'], __ ( 'invite_friend' ) );
					 * $total_array ['tell_to_friend_subject'] = str_replace ( "##REFERRALDISCOUNT##", REFERRAL_DISCOUNT, $total_array ['tell_to_friend_subject'] );
					 */

                    $message = array(
                        "message" => $msg_data ['login_success'],
                        "detail" => $response_data,
                        "status" => 1
                    );
                }
            } else {

                $message = array(
                    "message" => $msg_data ['account_exists'],
                    "status" => 2
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['mandatory_data'],
                "status" => 2
            );
        }

        echo json_encode($message);
    }

    /**
     * login through google plus
     */
    public function social_media_login_post()
    {

        // we need create API key for login user by adding keys &
        // intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = array();
        $data = array();
        $passenger_exists = NULL;
        $passenger_update = NULL;
        $response_data = array();

        // authentication generated key
        $auth_key = NULL; // $post_data ['authKey'];
        $auth_user_id = NULL; // $post_data ['userId'];
        $auth_user_type = NULL; // $post_data ['userType'];
        $auth_details = array();

        $message = '';
        // Posted json data

        $post_data = $this->get_post_data(); // $this->input->post ();
        // debug_exit($post_data->phone);

        $email = $post_data->email;

        if ($email) {
            if ($email) {
                $passenger_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                    'email' => $email,
                    'status' => 'Y'
                ));
            }

            if ($passenger_exists) {
                $auth_data = array();
                // update the paggenger deviceId & device type by default is 'A'=Android, 'I'=Iphone, 'W'=Windows,'N'=No device
                // @todo change the device based on device

                // Send OTP if passenger activation staus is failed
                if ($passenger_exists->activationStatus == Status_Type_Enum::INACTIVE) {
                    $detail = array(
                        "email" => $passenger_exists->email,
                        "passenger_id" => $passenger_exists->id,
                        "otp" => $passenger_exists->otp
                    );
                    $message = array(
                        "message" => $msg_data ['otp_verify_failed'],
                        "detail" => $detail,
                        "status" => 5
                    );
                }  // Send response data with Auth details & passenger details if passenger activation staus is success
                else {
                    $passenger_update = $this->Passenger_Model->update(array(
                        'signupFrom' => Signup_Type_Enum::MOBILE,
                        'registerType' => Register_Type_Enum::GOOGLE,
                        'loginStatus' => Status_Type_Enum::ACTIVE,
                        'lastLoggedIn' => getCurrentDateTime()
                    ), array(
                        'id' => $passenger_exists->id
                    ));
                    $key = random_string('alnum', 16);
                    $auth_key = hash("sha256", $key);
                    $auth_key_exists = $this->Auth_Key_Model->getOneByKeyValueArray(array(
                        'userId' => $passenger_exists->id,
                        'userType' => User_Type_Enum::PASSENGER
                    ));
                    // If user already exists update auth key
                    if ($auth_key_exists) {
                        $this->Auth_Key_Model->update(array(
                            'key' => $auth_key
                        ), array(
                            'userId' => $passenger_exists->id,
                            'userType' => User_Type_Enum::PASSENGER
                        ));
                    }  // create new auth key for passenger
                    else {
                        $this->Auth_Key_Model->insert(array(
                            'key' => $auth_key,
                            'userId' => $passenger_exists->id,
                            'userType' => User_Type_Enum::PASSENGER
                        ));
                    }
                    // Authentication details
                    $auth_details = array(
                        'authKey' => $auth_key,
                        'userId' => $passenger_exists->id,
                        'userType' => User_Type_Enum::PASSENGER
                    );
                    // to get site information details
                    $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                        'id' => 1
                    ));
                    // to get currency code/currency symbol currencySymbol
                    $currency_details = $this->Country_Model->getColumnByKeyValueArray('currencySymbol', array(
                        'id' => $site_setting_info->countryId
                    ));
                    // response_data
                    // @todo check again
                    $tell_to_friend_subject = 'Use my Zuver invite code ##REFERRALCODE##, and get a free ride worth Rs. ##REFERRALDISCOUNT##. Download the app & redeem it! App Store: apple.co/1T3N8oD Play Store: http://bit.ly/1KqxoVy'; // $site_setting_info[0]->passengerTellToFriendMsg;
                    $tell_to_friend_subject = str_replace('##REFERRALCODE##', $passenger_exists->passengerCode, $tell_to_friend_subject);
                    $tell_to_friend_subject = str_replace('##REFERRALDISCOUNT##', $site_setting_info->referralDiscountAmount, $tell_to_friend_subject);

                    $share_content = '##USERNAME## is using a Zuver & is currently located at ##LOCATION##. The request to share this information with you was made by ##USERNAME## through the Zuver app.';
                    $share_content = str_replace('##USERNAME##', $passenger_exists->firstName . ' ' . $passenger_exists->lastName, $share_content);
                    $share_content = str_replace('##LOCATION##', $passenger_exists->cityId, $share_content);
                    $response_data = array(
                        'auth_details' => $auth_details,
                        'id' => $passenger_exists->id,
                        'name' => $passenger_exists->firstName . ' ' . $passenger_exists->lastName,
                        'email' => $passenger_exists->email,
                        'phone' => $passenger_exists->mobile,
                        'wallet_amount' => $passenger_exists->walletAmount,
                        'profile_image' => passenger_content_url('/' . $passenger_exists->id . '/' . $passenger_exists->profileImage),
                        'login_from' => $passenger_exists->signupFrom,
                        'referral_code' => $passenger_exists->passengerCode,
                        'referral_amount' => $site_setting_info->referralDiscountAmount,
                        'site_currency' => $currency_details [0]->currencySymbol,
                        'aboutpage_description' => $site_setting_info->description,
                        'telltofriend_message' => $site_setting_info->passengerTellToFriendMsg,
                        'tell_to_friend_subject' => '',
                        'share_content' => $share_content,
                        'model_details' => '', // @todo need to check
                        'credit_card_status' => 0
                    ); // by default it is set zero(0)

                    /*
					 * $total_array ['share_content'] = str_replace ( "##USERNAME##", $total_array ['name'], __ ( 'telltofriend_content' ) );
					 * $total_array ['tell_to_friend_subject'] = str_replace ( "##REFERRALCODE##", $total_array ['referral_code'], __ ( 'invite_friend' ) );
					 * $total_array ['tell_to_friend_subject'] = str_replace ( "##REFERRALDISCOUNT##", REFERRAL_DISCOUNT, $total_array ['tell_to_friend_subject'] );
					 */

                    $message = array(
                        "message" => $msg_data ['login_success'],
                        "detail" => $response_data,
                        "status" => 1
                    );
                }
            } else {

                $message = array(
                    "message" => $msg_data ['account_exists'],
                    "status" => 2
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['mandatory_data'],
                "status" => 2
            );
        }

        echo json_encode($message);
    }

    /**
     * login through facebook
     */
    public function passenger_fb_connect_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        // authentication generated key
        $auth_key = NULL; // $post_data ['authKey'];
        $auth_user_id = NULL; // $post_data ['userId'];
        $auth_user_type = NULL; // $post_data ['userType'];
        $auth_details = array();

        $message = '';
        $email = NULL;
        $first_name = NULL;
        $last_name = NULL;
        $mobile = NULL;
        $userid = NULL;
        $password = NULL;
        $device_id = NULL;
        $device_token = NULL;
        $device_type = NULL;
        $refered_code = NULL;
        $accessToken = NULL;

        $profile_image = NULL;
        $referral_passenger_id = Null;
        $passenger_referral_insert_id = NULL;
        $passenger_id = NULL;
        $user_exists = array();
        $update_passenger = NULL;
        $response_data = array();

        $passenger_exists = NULL;
        $passenger_update = NULL;

        // Posted json data
        $post_data = $this->get_post_data();

        $email = $post_data->fbemail;
        $first_name = $post_data->fname;
        $last_name = $post_data->lname;

        $userid = $post_data->userid;

        $device_id = $post_data->deviceid;
        $device_token = $post_data->devicetoken;
        $device_type = $post_data->devicetype;
        // $refered_code = $post_data->refered_code;
        $accessToken = $post_data->accesstoken;

        $password = random_string('alnum', 8);

        // actual functionality
        // to get site information details
        $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
            'id' => 1
        ));

        if ($email && $userid) {
            $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                'email' => $email
            ));
            if ($user_exists) {
                $message = array(
                    "message" => $msg_data ['email_exists'],
                    "status" => 2
                );
            } else if (!$user_exists) {
                if ($mobile) {
                    $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                        'mobile' => $mobile
                    ));
                }
                if ($user_exists) {
                    $message = array(
                        "message" => $msg_data ['mobile_exists'],
                        "status" => 2
                    );
                } else if (!$user_exists) {
                    if (!$user_exists) {

                        $password = hash("sha256", $password);
                        // @todo generate 6 digit otp
                        $otp = str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT);
                        // @todo unique passenger code
                        $passenger_code = trim(substr(strtoupper(str_replace(' ', '', $first_name . '' . $last_name)), 0, 5));
                        $passenger_code .= $this->Passenger_Model->getAutoIncrementValue();

                        $insert_data = array(
                            'firstName' => $first_name,
                            'lastName' => $last_name,
                            'email' => $email,
                            'mobile' => '',
                            'password' => $password,
                            'otp' => $otp,
                            'deviceId' => $device_id,
                            'deviceType' => ($device_type) ? Device_Type_Enum::ANDROID : Device_Type_Enum::IPHONE,
                            'cityId' => 0,
                            'companyId' => DEFAULT_COMPANY_ID,
                            'passengerCode' => $passenger_code,
                            'referrerCode' => ($refered_code) ? $refered_code : '',
                            'sourceLead' => 'WEB',
                            'signupFrom' => Signup_Type_Enum::MOBILE,
                            'registerType' => Register_Type_Enum::FACEBOOK,
                            'activationStatus' => Status_Type_Enum::ACTIVE
                        );
                        $passenger_id = $this->Passenger_Model->insert($insert_data);

                        if ($passenger_id) {

                            $image_data = $this->getbase64ImageUrl($passenger_id, $profile_image);

                            $file_name = "profile_image.jpg";
                            $file = passenger_content_url() . "" . $passenger_id . "/" . $file_name;
                            $data ['file_name'] = file_put_contents($file_name, $image_data); // $this->getbase64ImageUrl ( $passenger_id, $profile_image );

                            if ($data) {

                                $update_passenger = $this->Passenger_Model->update(array(
                                    'profileImage' => $file_name
                                ), array(
                                    'id' => $passenger_id
                                ));
                            }

                            // signin/login implementation
                            if ($email) {
                                if ($email) {
                                    $passenger_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                                        'email' => $email,
                                        'status' => 'Y'
                                    ));
                                }

                                if ($passenger_exists) {
                                    $auth_data = array();
                                    // update the paggenger deviceId & device type by default is 'A'=Android, 'I'=Iphone, 'W'=Windows,'N'=No device
                                    // @todo change the device based on device

                                    // Send OTP if passenger activation staus is failed
                                    if ($passenger_exists->activationStatus == Status_Type_Enum::INACTIVE) {
                                        $detail = array(
                                            "email" => $passenger_exists->email,
                                            "passenger_id" => $passenger_exists->id,
                                            "otp" => $passenger_exists->otp
                                        );
                                        $message = array(
                                            "message" => $msg_data ['otp_verify_failed'],
                                            "detail" => $detail,
                                            "status" => 5
                                        );
                                    }  // Send response data with Auth details & passenger details if passenger activation staus is success
                                    else {
                                        $passenger_update = $this->Passenger_Model->update(array(
                                            'loginStatus' => Status_Type_Enum::ACTIVE,
                                            'lastLoggedIn' => getCurrentDateTime()
                                        ), array(
                                            'id' => $passenger_exists->id
                                        ));
                                        $key = random_string('alnum', 16);
                                        $auth_key = hash("sha256", $key);
                                        $auth_key_exists = $this->Auth_Key_Model->getOneByKeyValueArray(array(
                                            'userId' => $passenger_exists->id,
                                            'userType' => User_Type_Enum::PASSENGER
                                        ));
                                        // If user already exists update auth key
                                        if ($auth_key_exists) {
                                            $this->Auth_Key_Model->update(array(
                                                'key' => $auth_key
                                            ), array(
                                                'userId' => $passenger_exists->id,
                                                'userType' => User_Type_Enum::PASSENGER
                                            ));
                                        }  // create new auth key for passenger
                                        else {
                                            $this->Auth_Key_Model->insert(array(
                                                'key' => $auth_key,
                                                'userId' => $passenger_exists->id,
                                                'userType' => User_Type_Enum::PASSENGER
                                            ));
                                        }
                                        // Authentication details
                                        $auth_details = array(
                                            'authKey' => $auth_key,
                                            'userId' => $passenger_exists->id,
                                            'userType' => User_Type_Enum::PASSENGER
                                        );
                                        // to get site information details
                                        $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                                            'id' => 1
                                        ));
                                        // to get currency code/currency symbol currencySymbol
                                        $currency_details = $this->Country_Model->getColumnByKeyValueArray('currencySymbol', array(
                                            'id' => $site_setting_info->countryId
                                        ));
                                        // response_data
                                        // @todo check again
                                        $tell_to_friend_subject = 'Use my Zuver invite code ##REFERRALCODE##, and get a free ride worth Rs. ##REFERRALDISCOUNT##. Download the app & redeem it! App Store: apple.co/1T3N8oD Play Store: http://bit.ly/1KqxoVy'; // $site_setting_info[0]->passengerTellToFriendMsg;
                                        $tell_to_friend_subject = str_replace('##REFERRALCODE##', $passenger_exists->passengerCode, $tell_to_friend_subject);
                                        $tell_to_friend_subject = str_replace('##REFERRALDISCOUNT##', $site_setting_info->referralDiscountAmount, $tell_to_friend_subject);

                                        $share_content = '##USERNAME## is using a Zuver & is currently located at ##LOCATION##. The request to share this information with you was made by ##USERNAME## through the Zuver app.';
                                        $share_content = str_replace('##USERNAME##', $passenger_exists->firstName . ' ' . $passenger_exists->lastName, $share_content);
                                        $share_content = str_replace('##LOCATION##', $passenger_exists->cityId, $share_content);
                                        $response_data = array(
                                            'auth_details' => $auth_details,
                                            'id' => $passenger_exists->id,
                                            'name' => $passenger_exists->firstName . ' ' . $passenger_exists->lastName,
                                            'email' => $passenger_exists->email,
                                            'phone' => $passenger_exists->mobile,
                                            'wallet_amount' => $passenger_exists->walletAmount,
                                            'profile_image' => passenger_content_url('/' . $passenger_exists->id . '/' . $passenger_exists->profileImage),
                                            'login_from' => $passenger_exists->signupFrom,
                                            'referral_code' => $passenger_exists->passengerCode,
                                            'referral_amount' => $site_setting_info->referralDiscountAmount,
                                            'site_currency' => $currency_details [0]->currencySymbol,
                                            'aboutpage_description' => $site_setting_info->description,
                                            'telltofriend_message' => $site_setting_info->passengerTellToFriendMsg,
                                            'tell_to_friend_subject' => '',
                                            'share_content' => $share_content,
                                            'model_details' => '', // @todo need to check
                                            'credit_card_status' => 0
                                        ); // by default it is set zero(0)

                                        /*
										 * $total_array ['share_content'] = str_replace ( "##USERNAME##", $total_array ['name'], __ ( 'telltofriend_content' ) );
										 * $total_array ['tell_to_friend_subject'] = str_replace ( "##REFERRALCODE##", $total_array ['referral_code'], __ ( 'invite_friend' ) );
										 * $total_array ['tell_to_friend_subject'] = str_replace ( "##REFERRALDISCOUNT##", REFERRAL_DISCOUNT, $total_array ['tell_to_friend_subject'] );
										 */

                                        $message = array(
                                            "message" => $msg_data ['login_success'],
                                            "detail" => $response_data,
                                            "status" => 1
                                        );
                                    }
                                } else {

                                    $message = array(
                                        "message" => $msg_data ['account_exists'],
                                        "status" => 2
                                    );
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['facebook_email_not_found'],
                                    "status" => 2
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['facebook_login_failed'],
                                "status" => -1
                            );
                        }
                    }
                }
            }
        } else {
            $message = array(
                "message" => $msg_data ['mandatory_data'],
                "status" => 5
            );
        }

        echo json_encode($message);
    }

    /**
     * used to get the wallet amount and bank details
     */
    public function bank_name_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $passenger_wallet_amount = NULL;
        $passenger_id = NULL;

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                if ($passenger_id > 0) {
                    $bank_name = array();
                    $passenger_wallet_amount = $this->Passenger_Model->getColumnByKeyValueArray('walletAmount', array(
                        'id' => $passenger_id
                    ));
                    $bal_amount = isset ($passenger_wallet_amount [0]->walletAmount) ? $passenger_wallet_amount [0]->walletAmount : 0;
                    $bank_name [0] ['bank'] = "HDFC Bank";
                    $bank_name [1] ['bank'] = "ICICI BANK";
                    $bank_name [2] ['bank'] = "Andhra Bank";
                    $bank_name [3] ['bank'] = "AXIS Bank";
                    $bank_name [4] ['bank'] = "Vijaya Bank";
                    $bank_name [5] ['bank'] = "IDBI Bank";
                    $bank_name [6] ['bank'] = "Indian Bank";
                    $bank_name [7] ['bank'] = "Punjab National Bank";

                    $message = array(
                        "message" => $msg_data ['success'],
                        "detail" => $bank_name,
                        "wallet_amount" => $bal_amount,
                        "min_balance" => ZUVER_MIN_BALANCE,
                        "status" => 1
                    );
                } else {

                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * Request booking
     */
    public function savebooking_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $promocode_exists = NULL;
        $promocode_limit_exists = NULL;
        $passenger_id = 0;
        $promocode = NULL;
        $city_id = 0;
        $payment_type = Payment_Mode_Enum::WALLET;
        $trip_type = NULL;
        $transmission_type = NULL;
        $booking_key = NULL;
        $trip_id = NULL;
        $passenger_data = array();
        $insert_data = array();
        $data = array();

        // Posted json data
        $post_data = $this->get_post_data();

        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $promocode = ($post_data->promo_code) ? $post_data->promo_code : '';

        if (array_key_exists('passenger_id', $post_data) && $post_data->passenger_id > 0) {
            $passenger_id = $post_data->passenger_id;

        }
        if (array_key_exists('company_id', $post_data) && $post_data->company_id > 0) {
            $city_id = $post_data->company_id;

        }
        // To set payment Type/mode
        if (array_key_exists('faretype', $post_data)) {

            if ($post_data->faretype == 1) {
                $payment_type = Payment_Mode_Enum::CASH;
            } else if ($post_data->faretype == 2) {
                $payment_type = Payment_Mode_Enum::PAYTM;
            } else if ($post_data->faretype == 3) {
                $payment_type = Payment_Mode_Enum::WALLET;
            } else {
                $payment_type = $post_data->faretype;
            }
        }
        // To set trip Type
        if (array_key_exists('trip_type', $post_data)) {

            if ($post_data->trip_type == 1) {
                $trip_type = Trip_Type_Enum::ONEWAY_TRIP;
            } else if ($post_data->trip_type == 2) {
                $trip_type = Trip_Type_Enum::RETURN_TRIP;
            } else if ($post_data->trip_type == 3) {
                $trip_type = Trip_Type_Enum::OUTSTATION_TRIP;
            } else {
                $trip_type = $post_data->trip_type;
            }
        }
        // To set transmission Type
        if (array_key_exists('vehicle_type', $post_data)) {

            if ($post_data->vehicle_type == 'MANUAL') {
                $transmission_type = Transmission_Type_Enum::MANUAL;
            } else if ($post_data->vehicle_type == 'AUTO') {
                $transmission_type = Transmission_Type_Enum::AUTOMATIC;
            } else {
                $transmission_type = $post_data->vehicle_type;
            }

        }

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality

                if ($passenger_id && $post_data->latitude && $post_data->longitude && $post_data->trip_type && $post_data->pickup_time) {
                    // Check promocode exists or not
                    if ($promocode) {
                        // Check promocode exists or not for particular passenger at particular city
                        $promocode_exists = $this->Api_Webservice_Model->checkPromocodeAvailablity($promocode, $passenger_id, $city_id);

                        if ($promocode_exists && count($promocode_exists) > 0) {
                            /* $promocode_exists = $this->Passenger_Promocode_Details_Model->getByKeyValueArray ( array (
									'passengerId' => 0,
									'promoCode' => $promocode,
									'cityId' => $city_id
							) ); */
                            if (strtotime($promocode_exists [0]->promoStartDate) < getCurrentUnixDateTime()) {
                                $message = array(
                                    "message" => $msg_data ['promocode_start'],
                                    "status" => 3
                                );
                            }
                            if (strtotime($promocode_exists [0]->promoEndDate) > getCurrentUnixDateTime()) {
                                $message = array(
                                    "message" => $msg_data ['promocode_expiry'],
                                    "status" => 3
                                );
                            }

                            // check promocode limit exceeds or not for specific passenger (note: only completed trip)
                            $promocode_user_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                                'passengerId' => $passenger_id,
                                'promoCode' => $promocode,
                                'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                            ));
                            // check promocode limit exceeds or not total limit (note: only completed trip)
                            $promocode_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                                'promoCode' => $promocode,
                                'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                            ));
                            if (count($promocode_limit_exists) >= $promocode_exists [0]->promoCodeLimit) {
                                $message = array(
                                    "message" => $msg_data ['promocode_total_limit'],
                                    "status" => 3
                                );
                            } else if (count($promocode_user_limit_exists) >= $promocode_exists [0]->promoCodeUserLimit) {
                                $message = array(
                                    "message" => $msg_data ['promocode_user_limit'],
                                    "status" => 3
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_promocode'],
                                "status" => 3
                            );
                        }
                    }
                    // @todo genereate booking key
                    $next_trip_id = $this->Trip_Details_Model->getAutoIncrementValue();
                    $booking_key = $trip_type . '-' . $payment_type . '-' . $transmission_type . '-' . $next_trip_id;
                    if ($trip_type == Trip_Type_Enum::RETURN_TRIP) {
                        $post_data->dropplace = $post_data->pickupplace;
                        $post_data->drop_latitude = $post_data->latitude;
                        $post_data->drop_longitude = $post_data->longitude;
                    }
                    // insert trip data to trip details table 'promoCode'=>$promocode
                    if ($post_data->latitude != '0' || empty ($post_data->latitude) && $post_data->longitude != '0' || empty ($post_data->longitude)) {
                        $insert_data = array(
                            'passengerId' => $passenger_id,
                            'bookingKey' => $booking_key,
                            'companyId' => DEFAULT_COMPANY_ID,
                            'bookingCityId' => $city_id,
                            'pickupLocation' => $post_data->pickupplace,
                            'pickupLatitude' => $post_data->latitude,
                            'pickupLongitude' => $post_data->longitude,
                            'dropLocation' => $post_data->dropplace,
                            'dropLatitude' => $post_data->drop_latitude,
                            'dropLongitude' => $post_data->drop_longitude,
                            'pickupDatetime' => $post_data->pickup_time,
                            'dropDatetime' => $post_data->latitude,
                            'tripStatus' => Trip_Status_Enum::BOOKED,
                            'notificationStatus' => Trip_Status_Enum::BOOKED,
                            'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE,
                            'tripType' => $trip_type,
                            'bookedFrom' => Signup_Type_Enum::MOBILE,
                            'carType' => Car_Type_Enum::NONE,
                            'paymentMode' => $payment_type,
                            'transmissionType' => $transmission_type,
                            'billType' => Bill_Type_Enum::HOURLY,
                            'promoCode' => $promocode,
                            'landmark' => $post_data->notes
                        );
                        $trip_id = $this->Trip_Details_Model->insert($insert_data);
                        if ($trip_id) {
                            // To get the passenger mobile & email
                            $passenger_data = $this->Passenger_Model->getById($passenger_id);

                            if (SMS && count($passenger_data) > 0) {
                                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                    'smsTitle' => 'booking_confirmed_sms'
                                ));
                                $message_data = $message_details->smsContent;
                                // $message = str_replace("##PASSENGERNAME##",$name,$message);
                                $message_data = str_replace("##PASSENGERNAME##", $passenger_data->firstName . ' ' . $passenger_data->lastName, $message_data);
                                $message_data = str_replace("##BOOKINGID##", $trip_id, $message_data);
                                //$message_data = str_replace ( "##DEMO##", '', $message_data );
                                // print_r($message);exit;

                                // this SMS is sending to below numbers on request from Sidhanth on 07/10/2016
                                $this->Api_Webservice_Model->sendSMS('9619303999', $message_data);
                                $this->Api_Webservice_Model->sendSMS('9819896587', $message_data);
                                $this->Api_Webservice_Model->sendSMS('9619728423', $message_data);
                                // this SMS is sending to below numbers on request from Sidhanth on 07/10/2016

                                if ($passenger_data->mobile != "") {

                                    $this->Api_Webservice_Model->sendSMS($passenger_data->mobile, $message_data);
                                }
                            }

                            $from = SMTP_EMAIL_ID;
                            $to = $passenger_data->email;
                            $subject = 'Trip Booking Request-' . SMTP_EMAIL_NAME;
                            $data = array(
                                'passenger_name' => $passenger_data->firstName . ' ' . $passenger_data->lastName,
                                'booking_key' => $trip_id,
                                'pickup_location' => $post_data->pickupplace,
                                'pickup_date_time' => $post_data->pickup_time,
                                'passenger_mobile' => $passenger_data->mobile
                            );
                            $message_data = $this->load->view('emailtemplate/booking_confirm', $data, TRUE);

                            // Sending email to info@zuver.in on request from Sidhanth on 07/10/2016
                            $to_list = array(
                                'info@zuver.in',
                                'sidhanth@zuver.in',
                                'sovin@zuver.in'
                            );
                            foreach ($to_list as $to) {
                                if (SMTP && count($passenger_data) > 0) {

                                    if ($to) {
                                        $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                                    }
                                } else {

                                    // To send HTML mail, the Content-type header must be set
                                    $headers = 'MIME-Version: 1.0' . "\r\n";
                                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                    // Additional headers
                                    $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                                    $headers .= 'To: <' . $to . '>' . "\r\n";
                                    if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                                        mail($to, $subject, $message_data, $headers);
                                    }
                                }
                            }
                            // Sending email to info@zuver.in on request from Sidhanth on 07/10/2016

                            $message = array(
                                "message" => $msg_data ['booking_success'],
                                "status" => -1,
                                "trip_id" => $trip_id,
                                "triptype" => $trip_type,
                                "booking_date_and_time" => getCurrentDateTime(),
                                "pick_up_location" => $post_data->pickupplace,
                                "drop_off_location" => $post_data->dropplace,
                                "pick_up_time" => $post_data->pickup_time,
                                "car" => $post_data->vehicle_type
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['booking_failed'],
                                "status" => -4
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['lat_long_mandatory'],
                            "status" => -4
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "detail" => $msg_data ['mandatory_data'],
                        "status" => -5
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * Monthly booking
     */
    public function monthly_booking_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $promocode_exists = NULL;
        $promocode_limit_exists = NULL;
        $passenger_id = 0;
        $promocode = NULL;
        $city_id = 0;
        $payment_type = Payment_Mode_Enum::WALLET;
        $trip_type = NULL;
        $transmission_type = NULL;
        $booking_key = NULL;
        $trip_id = NULL;
        $passenger_data = array();
        $insert_data = array();
        $data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $promocode = ($post_data->promo_code) ? $post_data->promo_code : '';

        // @todo genereate booking key
        $booking_key = '';

        if (array_key_exists('passenger_id', $post_data) && $post_data->passenger_id > 0) {
            $passenger_id = $post_data->passenger_id;
        }
        if (array_key_exists('company_id', $post_data) && $post_data->company_id > 0) {
            $city_id = $post_data->company_id;
        }
        // To set payment Type/mode
        if (array_key_exists('faretype', $post_data)) {

            if ($post_data->faretype == 1) {
                $payment_type = Payment_Mode_Enum::CASH;
            } else if ($post_data->faretype == 2) {
                $payment_type = Payment_Mode_Enum::PAYTM;
            } else if ($post_data->faretype == 3) {
                $payment_type = Payment_Mode_Enum::WALLET;
            }
        }
        // To set trip Type
        if (array_key_exists('trip_type', $post_data)) {

            if ($post_data->trip_type == 1) {
                $trip_type = Trip_Type_Enum::ONEWAY_TRIP;
            } else if ($post_data->trip_type == 2) {
                $trip_type = Trip_Type_Enum::RETURN_TRIP;
            } else if ($post_data->trip_type == 3) {
                $trip_type = Trip_Type_Enum::OUTSTATION_TRIP;
            }
        }
        // To set transmission Type
        if (array_key_exists('vehicle_type', $post_data)) {

            if ($post_data->vehicle_type == 'MANUAL') {
                $transmission_type = Transmission_Type_Enum::MANUAL;
            } else if ($post_data->vehicle_type == 'AUTO') {
                $transmission_type = Transmission_Type_Enum::AUTOMATIC;
            } else {
                $transmission_type = $post_data->vehicle_type;
            }
        }
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality

                if ($passenger_id && $post_data->latitude && $post_data->longitude && $post_data->trip_type && $post_data->pickup_time) {

                    // Check promocode exists or not
                    if ($promocode) {
                        // Check promocode exists or not for particular passenger at particular city
                        $promocode_exists = $this->Api_Webservice_Model->checkPromocodeAvailablity($promocode, $passenger_id, $city_id);

                        if ($promocode_exists && count($promocode_exists) > 0) {
                            /* $promocode_exists = $this->Passenger_Promocode_Details_Model->getByKeyValueArray ( array (
									'passengerId' => 0,
									'promoCode' => $promocode,
									'cityId' => $city_id
							) ); */
                            if (strtotime($promocode_exists [0]->promoStartDate) < getCurrentUnixDateTime()) {
                                $message = array(
                                    "message" => $msg_data ['promocode_start'],
                                    "status" => 3
                                );
                            }
                            if (strtotime($promocode_exists [0]->promoEndDate) > getCurrentUnixDateTime()) {
                                $message = array(
                                    "message" => $msg_data ['promocode_expiry'],
                                    "status" => 3
                                );
                            }

                            // check promocode limit exceeds or not for specific passenger (note: only completed trip)
                            $promocode_user_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                                'passengerId' => $passenger_id,
                                'promoCode' => $promocode,
                                'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                            ));
                            // check promocode limit exceeds or not total limit (note: only completed trip)
                            $promocode_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                                'promoCode' => $promocode,
                                'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                            ));
                            if (count($promocode_limit_exists) >= $promocode_exists [0]->promoCodeLimit) {
                                $message = array(
                                    "message" => $msg_data ['promocode_total_limit'],
                                    "status" => 3
                                );
                            } else if (count($promocode_user_limit_exists) >= $promocode_exists [0]->promoCodeUserLimit) {
                                $message = array(
                                    "message" => $msg_data ['promocode_user_limit'],
                                    "status" => 3
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_promocode'],
                                "status" => 3
                            );
                        }
                    }

                    // insert trip data to trip details table 'promoCode'=>$promocode
                    if ($post_data->latitude != '0' || empty ($post_data->latitude) && $post_data->longitude != '0' || empty ($post_data->longitude)) {
                        $pickup_date = trim($post_data->pickup_date);
                        $pickup_date = str_replace('[', '', $post_data->pickup_date);
                        $pickup_date = str_replace(']', '', $pickup_date);

                        $pickup_date = explode(',', $pickup_date);
                        if (is_array($pickup_date)) {
                            foreach ($pickup_date as $list) {
                                $next_trip_id = $this->Trip_Details_Model->getAutoIncrementValue();
                                $booking_key = $trip_type . '-' . $payment_type . '-' . $transmission_type . '-' . $next_trip_id;

                                $pickup_date_time = $list . " " . $post_data->pickup_time;

                                if ($trip_type == Trip_Type_Enum::RETURN_TRIP) {
                                    $post_data->dropplace = $post_data->pickupplace;
                                    $post_data->drop_latitude = $post_data->latitude;
                                    $post_data->drop_longitude = $post_data->longitude;
                                }

                                $insert_data = array(
                                    'passengerId' => $passenger_id,
                                    'bookingKey' => $booking_key,
                                    'companyId' => DEFAULT_COMPANY_ID,
                                    'bookingCityId' => $city_id,
                                    'pickupLocation' => $post_data->pickupplace,
                                    'pickupLatitude' => $post_data->latitude,
                                    'pickupLongitude' => $post_data->longitude,
                                    'dropLocation' => $post_data->dropplace,
                                    'dropLatitude' => $post_data->drop_latitude,
                                    'dropLongitude' => $post_data->drop_longitude,
                                    'pickupDatetime' => $pickup_date_time,
                                    'dropDatetime' => $post_data->latitude,
                                    'tripStatus' => Trip_Status_Enum::BOOKED,
                                    'notificationStatus' => Trip_Status_Enum::BOOKED,
                                    'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE,
                                    'tripType' => $trip_type,
                                    'bookedFrom' => Signup_Type_Enum::MOBILE,
                                    'carType' => Car_Type_Enum::NONE,
                                    'paymentMode' => $payment_type,
                                    'transmissionType' => $transmission_type,
                                    'billType' => Bill_Type_Enum::MONTHLY,
                                    'promoCode' => $promocode,
                                    'landmark' => $post_data->notes
                                );
                                $last_trip_id = $this->Trip_Details_Model->insert($insert_data);
                                if (!$trip_id) {
                                    $trip_id = $last_trip_id;
                                }
                            }
                        } else {
                            $next_trip_id = $this->Trip_Details_Model->getAutoIncrementValue();
                            $booking_key = $trip_type . '-' . $payment_type . '-' . $transmission_type . '-' . $next_trip_id;

                            $pickup_date_time = $post_data->pickup_date . ' ' . $post_data->pickup_time;
                            $insert_data = array(
                                'passengerId' => $passenger_id,
                                'bookingKey' => $booking_key,
                                'companyId' => DEFAULT_COMPANY_ID,
                                'bookingCityId' => $city_id,
                                'pickupLocation' => $post_data->pickupplace,
                                'pickupLatitude' => $post_data->latitude,
                                'pickupLongitude' => $post_data->longitude,
                                'dropLocation' => $post_data->dropplace,
                                'dropLatitude' => $post_data->drop_latitude,
                                'dropLongitude' => $post_data->drop_longitude,
                                'pickupDatetime' => $pickup_date_time,
                                'dropDatetime' => $post_data->latitude,
                                'tripStatus' => Trip_Status_Enum::BOOKED,
                                'notificationStatus' => Trip_Status_Enum::BOOKED,
                                'driverAcceptedStatus' => Driver_Accepted_Status_Enum::NONE,
                                'tripType' => $trip_type,
                                'bookedFrom' => Signup_Type_Enum::MOBILE,
                                'carType' => Car_Type_Enum::NONE,
                                'paymentMode' => $payment_type,
                                'transmissionType' => ($transmission_type == Transmission_Type_Enum::A) ? Transmission_Type_Enum::AUTOMATIC : Transmission_Type_Enum::MANUAL,
                                'billType' => Bill_Type_Enum::MONTHLY,
                                'promoCode' => $promocode,
                                'landmark' => $post_data->notes
                            );
                            $trip_id = $this->Trip_Details_Model->insert($insert_data);
                        }
                        if ($trip_id) {
                            // To get the passenger mobile & email
                            $passenger_data = $this->Passenger_Model->getById($passenger_id);

                            if (SMS && count($passenger_data) > 0) {
                                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                    'smsTitle' => 'booking_confirmed_sms'
                                ));
                                $message_data = $message_details->smsContent;
                                // $message = str_replace("##PASSENGERNAME##",$name,$message);
                                $message_data = str_replace("##PASSENGERNAME##", $passenger_data->firstName . ' ' . $passenger_data->lastName, $message_data);
                                $message_data = str_replace("##BOOKINGID##", $trip_id, $message_data);
                                $message_data = str_replace("##DEMO##", '', $message_data);
                                // print_r($message);exit;

                                // this SMS is sending to below numbers on request from Sidhanth on 07/10/2016
                                $this->Api_Webservice_Model->sendSMS('9619303999', $message_data);
                                $this->Api_Webservice_Model->sendSMS('9819896587', $message_data);
                                $this->Api_Webservice_Model->sendSMS('9619728423', $message_data);
                                // this SMS is sending to below numbers on request from Sidhanth on 07/10/2016

                                if ($passenger_data->mobile != "") {

                                    $this->Api_Webservice_Model->sendSMS($passenger_data->mobile, $message_data);
                                }
                            }

                            $from = SMTP_EMAIL_ID;
                            $to = $passenger_data->email;
                            $subject = 'Trip Booking Request-' . SMTP_EMAIL_NAME;
                            $data = array(
                                'passenger_name' => $passenger_data->firstName . ' ' . $passenger_data->lastName,
                                'booking_key' => $trip_id,
                                'pickup_location' => $post_data->pickupplace,
                                'pickup_date_time' => $pickup_date_time,
                                'passenger_mobile' => $passenger_data->mobile
                            );
                            $message_data = $this->load->view('emailtemplate/booking_confirm', $data, TRUE);

                            // Sending email to info@zuver.in on request from Sidhanth on 07/10/2016
                            $to_list = array(
                                'info@zuver.in',
                                'sidhanth@zuver.in',
                                'sovin@zuver.in'
                            );
                            foreach ($to_list as $to) {
                                if (SMTP && count($passenger_data) > 0) {

                                    if ($to) {
                                        $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                                    }
                                } else {

                                    // To send HTML mail, the Content-type header must be set
                                    $headers = 'MIME-Version: 1.0' . "\r\n";
                                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                    // Additional headers
                                    $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                                    $headers .= 'To: <' . $to . '>' . "\r\n";
                                    if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                                        mail($to, $subject, $message_data, $headers);
                                    }
                                }
                            }
                            // Sending email to info@zuver.in on request from Sidhanth on 07/10/2016

                            $message = array(
                                "message" => $msg_data ['booking_success'],
                                "status" => 1,
                                "trip_id" => $trip_id,
                                "triptype" => $trip_type,
                                "booking_date_and_time" => getCurrentDateTime(),
                                "pick_up_location" => $post_data->pickupplace,
                                "drop_off_location" => $post_data->dropplace,
                                "pick_up_time" => $pickup_date_time,
                                "car" => $post_data->vehicle_type,
                                "pending_trips" => count($pickup_date),
                                "completed_trip" => 0,
                                "wallet_balance" => $passenger_data->walletAmount,
                                "next_trip_date" => ($pickup_date [0]) ? $pickup_date [0] . ' ' . $post_data->pickup_time : ''
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['booking_failed'],
                                "status" => -4
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['lat_long_mandatory'],
                            "status" => -4
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "detail" => $msg_data ['mandatory_data'],
                        "status" => -5
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to get the rate card details
     */
    public function get_rate_card_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $city_id = DEFAULT_CITY_ID;
        $company_id = DEFAULT_COMPANY_ID;
        $is_monthly = NULL;
        $user_exits = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // Actual functionality
                if ($auth_user_type == User_Type_Enum::DRIVER) {
                    $user_exits = $this->Passenger_Model->getById($auth_user_id);
                }
                if ($auth_user_type == User_Type_Enum::PASSENGER) {
                    $user_exits = $this->Driver_Model->getById($auth_user_id);
                }

                if (array_key_exists('company_id', $post_data) && $post_data->company_id) {
                    $city_id = $post_data->company_id;
                }
                if ($user_exits) {
                    $company_id = $user_exits->companyId;
                }
                // To get site info details
                $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                    'id' => 1
                ));

                // to get rate card details based on companyId passenger/driver
                $rate_card_details = $this->Api_Webservice_Model->getRateCardDetails($company_id, $city_id, $post_data->is_monthly);
                if ($rate_card_details) {
                    $response_data = array(
                        'driver_fare' => array(
                            'fare_id' => $rate_card_details [0]->rateCardId,
                            '1hr_to_4hr' => $rate_card_details [0]->firstHourCharge,
                            '5hr_to_8hr' => $rate_card_details [0]->fifthHourCharge,
                            '9hr_to_12hr' => $rate_card_details [0]->ninthHourCharge,
                            'above_12hr' => $rate_card_details [0]->above12HourCharge,
                            'Oneway_7am_9pm' => 0,
                            'Oneway_9pm_7am' => 0,
                            'Return_7am_9pm' => 0,
                            'Return_9pm_7am' => 0,
                            'out_station_time_limit' => OUT_STATION_TIME_LIMIT,
                            'out_oneway_convenience' => 0,
                            'out_oneway_per_hour' => 0,
                            'out_oneway_distance_limit' => 0,
                            'out_oneway_first_tarnsit_charge' => 0,
                            'out_oneway_second_tarnsit_charge' => 0,
                            'out_return_convenience' => 0,
                            'out_return_per_hour' => 0,
                            'out_return_distance_limit' => 0,
                            'out_return_first_tarnsit_charge' => 0,
                            'out_return_second_tarnsit_charge' => 0,
                            'service_tax' => $site_setting_info->tax
                        )

                    );
                    foreach ($rate_card_details as $fare) {
                        if ($fare->tripType == Trip_Type_Enum::ONEWAY_TRIP) {
                            $response_data ['driver_fare'] ['Oneway_7am_9pm'] = $fare->dayConvenienceCharge;
                            $response_data ['driver_fare'] ['Oneway_9pm_7am'] = $fare->nightConvenienceCharge;
                        } else if ($fare->tripType == Trip_Type_Enum::RETURN_TRIP) {
                            $response_data ['driver_fare'] ['Return_7am_9pm'] = $fare->dayConvenienceCharge;
                            $response_data ['driver_fare'] ['Return_9pm_7am'] = $fare->nightConvenienceCharge;
                        } else if ($fare->tripType == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY) {
                            $response_data ['driver_fare'] ['out_oneway_convenience'] = $fare->dayConvenienceCharge;
                            $response_data ['driver_fare'] ['out_oneway_per_hour'] = $fare->firstHourCharge;
                            $response_data ['driver_fare'] ['out_oneway_distance_limit'] = $fare->minDistance;
                            $response_data ['driver_fare'] ['out_oneway_first_tarnsit_charge'] = $fare->distanceConvenienceCharge;
                            $response_data ['driver_fare'] ['out_oneway_second_tarnsit_charge'] = $fare->distanceCostPerKm;
                        } else if ($fare->tripType == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) {
                            $response_data ['driver_fare'] ['out_return_convenience'] = $fare->dayConvenienceCharge;
                            $response_data ['driver_fare'] ['out_return_per_hour'] = $fare->firstHourCharge;
                            $response_data ['driver_fare'] ['out_return_distance_limit'] = $fare->minDistance;
                            $response_data ['driver_fare'] ['out_return_first_tarnsit_charge'] = $fare->distanceConvenienceCharge;
                            $response_data ['driver_fare'] ['out_return_second_tarnsit_charge'] = $fare->distanceCostPerKm;
                        }
                        $message = array(
                            "message" => $msg_data ['success'],
                            "detail" => $response_data,
                            "status" => 1
                        );
                    }
                } else {
                    $response_data ['driver_fare'] = $msg_data ['tariff_failed'];
                    $message = array(
                        "message" => $msg_data ['tariff_failed'],
                        "detail" => $response_data,
                        "status" => 0
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to get the drive estimate and fare estimate
     */
    public function driver_estimate_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';

        $rate_card_details = NULL;

        // postdata intialized
        $trip_tracking_update = NULL;

        $passenger_id = NULL;
        $actual_pickup_time = NULL;
        $actual_drop_time = NULL;
        $actual_distance = NUll;
        $city_id = DEFAULT_COMPANY_ID;
        $trip_type = NULL;

        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;
        $actual_pickup_time = $post_data->start_date;
        $actual_drop_time = $post_data->end_date;
        $trip_type = $post_data->plan_type;
        $actual_distance = $post_data->actual_distance;
        // monthly subscripe calculation
        // $trip_type = $post_data->plan_type;
        // monthly subscripe calculation
        $city_id = $post_data->company_id;

        if ($trip_type == 1) {
            $trip_type = Trip_Type_Enum::ONEWAY_TRIP;
        }
        if ($trip_type == 2) {
            $trip_type = Trip_Type_Enum::RETURN_TRIP;
        }
        if ($trip_type == 3) {
            $trip_type = Trip_Type_Enum::OUTSTATION_TRIP;
        }

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                $total_calculated_hours = 0;
                $travelled_staged_hours = 0;
                $travelled_hours = 0;
                $travelled_minutes = 0;
                $travelled_seconds = 0;
                $travel_hours_minutes = 0;
                $convenience_charge = 0;
                $travel_charge = 0;
                $total_trip_cost = 0;
                $promo_discount_amount = 0;
                $company_tax = 0;
                $wallet_payment_amount = 0;
                if ($passenger_id && $actual_pickup_time && $actual_drop_time && $trip_type) {

                    // get rate card details based on the cityId & trip type
                    $rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray(array(
                        'cityId' => $city_id,
                        'companyId' => DEFAULT_COMPANY_ID,
                        'tripType' => $trip_type,
                        'status' => Status_Type_Enum::ACTIVE,
                        'isMonthly' => Status_Type_Enum::INACTIVE
                    ));
                    // monthly subscripe calculation
                    /*
					 * if ($trip_details->billType == Bill_Type_Enum::MONTHLY)
					 * {
					 * // get rate card details based on the cityId & trip type
					 * $rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
					 * 'cityId' => $city_id,
					 * 'companyId' => DEFAULT_COMPANY_ID,
					 * 'tripType' => $trip_type,
					 * 'status' => Status_Type_Enum::ACTIVE,
					 * 'isMonthly'=>Status_Type_Enum::ACTIVE
					 * ) );
					 * }
					 * else
					 * {
					 * // get rate card details based on the cityId & trip type
					 * $rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray ( array (
					 * 'cityId' => $city_id,
					 * 'companyId' => $trip_details->companyId,
					 * 'tripType' => $trip_type,
					 * 'status' => Status_Type_Enum::ACTIVE
					 * ) );
					 * }
					 */
                    // monthly subscripe calculation
                    if (strtotime($actual_pickup_time) < strtotime($actual_drop_time)) {

                        $dateDiff = intval((strtotime($actual_pickup_time) - strtotime($actual_drop_time)) / 60);
                        $total_calculated_hours = round(abs(strtotime($actual_pickup_time) - strtotime($actual_drop_time)) / 3600, 2);
                        $travelled_hours = intval(abs($dateDiff / 60));
                        $travelled_minutes = intval(abs($dateDiff % 60));
                        $travelled_seconds = intval(abs((strtotime($actual_pickup_time) - strtotime($actual_drop_time)) % 3600) % 60);

                        if ($travelled_seconds > 0) {
                            $travelled_seconds = 0;
                            $travelled_minutes += 1;
                        }
                        if ($travelled_minutes == 60) {
                            $travelled_minutes = 0;
                            $travelled_hours += 1;
                        } else {
                            $travelled_staged_hours = $travelled_hours;
                            /*
							 * if (($travelled_minutes > 0) && ($travelled_minutes < 60)) {
							 * $travelled_staged_hours = $travelled_hours + 1;
							 * }
							 */
                        }

                        $travel_hours_minutes = $travelled_hours . '.' . $travelled_minutes;

                        // to get convenience charge based pickup time day/night

                        if (date('H:i:s', strtotime($actual_pickup_time)) >= DAY_START_TIME && date('H:i:s', strtotime($actual_pickup_time)) <= DAY_END_TIME) {

                            $convenience_charge += $rate_card_details->dayConvenienceCharge;
                        } else if (date('H:i:s', strtotime($actual_pickup_time)) >= NIGHT_END_TIME && date('H:i:s', strtotime($actual_pickup_time)) <= NIGHT_START_TIME) {

                            $convenience_charge += $rate_card_details->nightConvenienceCharge;
                        }

                        if (count($rate_card_details) > 0) {
                            // if ($trip_details->billType == Bill_Type_Enum::HOURLY || $trip_details->billType == Bill_Type_Enum::MONTHLY) {
                            // calculation based on HOURLY i.e transit

                            if ($travelled_minutes == 0 && ($travelled_seconds > 0)) {

                                $travel_charge += $rate_card_details->firstHourCharge;
                            }

                            if ($travelled_hours == 0 && ($travelled_minutes > 0)) {

                                $travel_charge += $rate_card_details->firstHourCharge;
                            }

                            if ($trip_type == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY || $trip_type == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) {
                                $travelled_staged_hours = 0;
                                if ($travelled_hours > OUT_STATION_TIME_LIMIT) {
                                    $travelled_staged_hours = $travelled_hours - OUT_STATION_TIME_LIMIT;
                                }

                                if ($trip_type == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY) {
                                    $distance = explode('.', $rate_card_details->minDistance);
                                    if ($actual_distance <= $distance [0]) {
                                        $travel_charge = $actual_distance * $rate_card_details->distanceConvenienceCharge;

                                        $total_trip_cost = $convenience_charge + $travel_charge;
                                    } else {
                                        $travel_charge = $actual_distance * $rate_card_details->distanceCostPerKm;

                                        $total_trip_cost = $convenience_charge + $travel_charge;
                                    }
                                } else if ($trip_type == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) {
                                    $distance = explode('.', $rate_card_details->minDistance);
                                    if ($actual_distance <= $distance [0]) {
                                        $travel_charge = $actual_distance * $rate_card_details->distanceConvenienceCharge;

                                        $total_trip_cost = $convenience_charge + $travel_charge;
                                    } else {
                                        $travel_charge = $actual_distance * $rate_card_details->distanceCostPerKm;

                                        $total_trip_cost = $convenience_charge + $travel_charge;
                                    }
                                }
                            }
                            // claculation based on travel_hours_minutes
                            if ($travelled_staged_hours > 0) {
                                switch ($travelled_staged_hours) {
                                    case 1 : {
                                        // $travel_charge+=$rate_card_details->firstHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->firstHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->secondHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->secondHourCharge;
                                        }
                                        break;
                                    }
                                    case 2 : {
                                        // $travel_charge+=$rate_card_details->secondHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->secondHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->thirdHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->thirdHourCharge;
                                        }
                                        break;
                                    }

                                    case 3 : {
                                        // $travel_charge+=$rate_card_details->thirdHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->thirdHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->fourthHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->fourthHourCharge;
                                        }
                                        break;
                                    }
                                    case 4 : {
                                        // $travel_charge+=$rate_card_details->fourthHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->fourthHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->fifthHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->fifthHourCharge;
                                        }
                                        break;
                                    }
                                    case 5 : {
                                        // $travel_charge+=$rate_card_details->fifthHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->fifthHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->sixthHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->sixthHourCharge;
                                        }
                                        break;
                                    }
                                    case 6 : {
                                        // $travel_charge+=$rate_card_details->sixthHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->sixthHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->seventhHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->seventhHourCharge;
                                        }
                                        break;
                                    }
                                    case 7 : {
                                        // $travel_charge+=$rate_card_details->seventhHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->seventhHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->eighthHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->eighthHourCharge;
                                        }
                                        break;
                                    }
                                    case 8 : {
                                        // $travel_charge+=$rate_card_details->eighthHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->eighthHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->ninthHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->ninthHourCharge;
                                        }
                                        break;
                                    }
                                    case 9 : {
                                        // $travel_charge+=$rate_card_details->ninthHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->ninthHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->tenthHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->tenthHourCharge;
                                        }
                                        break;
                                    }
                                    case 10 : {
                                        // $travel_charge+=$rate_card_details->tenthHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->tenthHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->eleventhHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->eleventhHourCharge;
                                        }
                                        break;
                                    }
                                    case 11 : {
                                        // $travel_charge+=$rate_card_details->eleventhHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->eleventhHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->twelveHourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->twelveHourCharge;
                                        }
                                        break;
                                    }
                                    case 12 : {
                                        // $travel_charge+=$rate_card_details->twelveHourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->twelveHourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->above12HourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->above12HourCharge;
                                        }
                                        break;
                                    }
                                    default : {
                                        // $travel_charge+=$rate_card_details->above12HourCharge;

                                        $travel_charge += $travelled_hours * $rate_card_details->above12HourCharge;
                                        if (($travelled_minutes > 0)) {
                                            // $travel_charge+=$rate_card_details->above12HourCharge;
                                            $travel_charge += $travelled_minutes / 60 * $rate_card_details->above12HourCharge;
                                        }
                                        break;
                                    }
                                }
                            }
                            $total_trip_cost = $convenience_charge + $travel_charge;
                            $total_trip_cost = round($total_trip_cost, 2);
                            $travel_charge = $total_trip_cost;
                            /*
							 * } else if ($trip_details->billType == Bill_Type_Enum::FIXED) {
							 * // No calculation it is just billing the fixed price + tax with promo discount
							 * $travel_charge = $rate_card_details->fixedAmount - $convenience_charge;
							 * $total_trip_cost = round ( $rate_card_details->fixedAmount, 2 );
							 * } else if ($trip_details->billType == Bill_Type_Enum::DISTANCE) {
							 * // @todo calculation based on distance min-distance fixed + distance cost per distance
							 * $convenience_charge = $rate_card_details->distanceConvenienceCharge;
							 * $distance = explode ( '.', $rate_card_details->minDistance );
							 * if ($travelled_hours <= $distance [0]) {
							 * $travel_charge = 0;
							 * $total_trip_cost = $convenience_charge;
							 * } else {
							 * $travelled_hours = $travelled_hours - $rate_card_details->minDistance;
							 *
							 * $travel_charge = $travelled_hours * $rate_card_details->distanceCostPerKm;
							 * if ($travelled_minutes > 0) {
							 * if ($travelled_minutes > $distance [1]) {
							 *
							 * // $travel_charge+=$rate_card_details->timeCostPerHour;
							 * $travel_charge += $distance [1] % 1000 * $rate_card_details->timeCostPerHour;
							 * }
							 * $total_trip_cost = $convenience_charge + $travel_charge;
							 * }
							 * }
							 * } else if ($trip_details->billType == Bill_Type_Enum::TIME) {
							 * // calculation based on time min-time fixed + time cost per hour
							 * $convenience_charge = $rate_card_details->timeConvenienceCharge;
							 * $hours = explode ( '.', $rate_card_details->minTime );
							 * if ($travelled_hours <= $hours [0]) {
							 * $travel_charge = 0;
							 * $total_trip_cost = $convenience_charge;
							 * } else {
							 * $travelled_hours = $travelled_hours - $rate_card_details->minTime;
							 *
							 * $travel_charge = $travelled_hours * $rate_card_details->timeCostPerHour;
							 * if ($travelled_minutes > 0) {
							 * if ($travelled_minutes > $hours [1]) {
							 *
							 * // $travel_charge+=$rate_card_details->timeCostPerHour;
							 * $travel_charge += $hours [1] / 60 * $rate_card_details->timeCostPerHour;
							 * }
							 * $total_trip_cost = $convenience_charge + $travel_charge;
							 * }
							 * }
							 * } else {
							 * $message = array (
							 * "message" => $msg_data ['invalid_bill_type'],
							 * "status" => - 1
							 * );
							 * }
							 */
                            /*
							 * $total_trip_cost = $convenience_charge + $travel_charge;
							 * $total_trip_cost = round ( $total_trip_cost, 2 );
							 * $travel_charge = $total_trip_cost;
							 */

                            $message = array(
                                "thours_stage" => $travelled_staged_hours,
                                "message" => 'Success',
                                "fare" => $total_trip_cost,
                                "start_date" => $actual_pickup_time,
                                "end_date" => $actual_drop_time,
                                "hours" => $travel_hours_minutes,
                                "status" => 1
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['tariff_failed'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_drop_time'],
                            "status" => -1
                        );
                    }
                } else {

                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "status" => -2,
                        "detail" => $msg_data ['mandatory_data']
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    public function monthly_estimate_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';

        $rate_card_details = NULL;

        // postdata intialized
        $trip_tracking_update = NULL;

        $passenger_id = NULL;
        $trip_hours = NULL;
        $trip_days = NULL;
        $city_id = DEFAULT_COMPANY_ID;
        $trip_type = NULL;

        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;
        $trip_hours = $post_data->trip_hours;
        $trip_days = $post_data->trip_days;
        $trip_type = $post_data->plan_type;
        // monthly subscripe calculation
        // $trip_type = $post_data->plan_type;
        // monthly subscripe calculation
        $city_id = $post_data->company_id;

        if ($trip_type == 1) {
            $trip_type = Trip_Type_Enum::ONEWAY_TRIP;
        }
        if ($trip_type == 2) {
            $trip_type = Trip_Type_Enum::RETURN_TRIP;
        }
        if ($trip_type == 3) {
            $trip_type = Trip_Type_Enum::OUTSTATION_TRIP;
        }
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                $total_calculated_hours = 0;
                $travelled_staged_hours = 0;
                $travelled_hours = 0;
                $travelled_minutes = 0;
                $travelled_seconds = 0;
                $travel_hours_minutes = 0;
                $convenience_charge = 0;
                $travel_charge = 0;
                $total_trip_cost = 0;
                $promo_discount_amount = 0;
                $company_tax = 0;
                $wallet_payment_amount = 0;
                $stage_cost = 0;
                if ($passenger_id && ($trip_hours > 0) && ($trip_days > 0) && $trip_type) {

                    // get rate card details based on the cityId & trip type
                    $rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray(array(
                        'cityId' => $city_id,
                        'companyId' => DEFAULT_COMPANY_ID,
                        'tripType' => $trip_type,
                        'status' => Status_Type_Enum::ACTIVE,
                        'isMonthly' => Status_Type_Enum::ACTIVE
                    ));

                    $travelled_hours = $trip_hours;

                    // $travelled_staged_hours = $travelled_hours;
                    $travelled_staged_hours = $travelled_hours;
                    $travel_hours_minutes = ($travelled_hours * $trip_days) . '.' . $travelled_minutes;

                    // to get convenience charge based on $travelled_hours

                    //if ($travelled_hours <= 4) {
                    $convenience_charge += $rate_card_details->dayConvenienceCharge;
                    //}
                    if (count($rate_card_details) > 0) {

                        // calculation based on HOURLY i.e transit

                        if ($travelled_minutes == 0 && ($travelled_seconds > 0)) {

                            $travel_charge += $rate_card_details->firstHourCharge;
                        }

                        if ($travelled_hours == 0 && ($travelled_minutes > 0)) {

                            $travel_charge += $rate_card_details->firstHourCharge;
                        }

                        // claculation based on travel_hours_minutes
                        if ($travelled_staged_hours > 0) {
                            switch ($travelled_staged_hours) {
                                case 1 : {
                                    // $travel_charge+=$rate_card_details->firstHourCharge;
                                    $stage_cost = $rate_card_details->firstHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->firstHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->secondHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->secondHourCharge;
                                    }
                                    break;
                                }
                                case 2 : {
                                    // $travel_charge+=$rate_card_details->secondHourCharge;
                                    $stage_cost = $rate_card_details->secondHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->secondHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->thirdHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->thirdHourCharge;
                                    }
                                    break;
                                }

                                case 3 : {
                                    // $travel_charge+=$rate_card_details->thirdHourCharge;
                                    $stage_cost = $rate_card_details->thirdHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->thirdHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->fourthHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->fourthHourCharge;
                                    }
                                    break;
                                }
                                case 4 : {
                                    // $travel_charge+=$rate_card_details->fourthHourCharge;
                                    $stage_cost = $rate_card_details->fourthHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->fourthHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->fifthHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->fifthHourCharge;
                                    }
                                    break;
                                }
                                case 5 : {
                                    // $travel_charge+=$rate_card_details->fifthHourCharge;
                                    $stage_cost = $rate_card_details->fifthHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->fifthHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->sixthHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->sixthHourCharge;
                                    }
                                    break;
                                }
                                case 6 : {
                                    // $travel_charge+=$rate_card_details->sixthHourCharge;
                                    $stage_cost = $rate_card_details->sixthHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->sixthHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->seventhHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->seventhHourCharge;
                                    }
                                    break;
                                }
                                case 7 : {
                                    // $travel_charge+=$rate_card_details->seventhHourCharge;
                                    $stage_cost = $rate_card_details->seventhHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->seventhHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->eighthHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->eighthHourCharge;
                                    }
                                    break;
                                }
                                case 8 : {
                                    // $travel_charge+=$rate_card_details->eighthHourCharge;
                                    $stage_cost = $rate_card_details->eighthHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->eighthHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->ninthHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->ninthHourCharge;
                                    }
                                    break;
                                }
                                case 9 : {
                                    // $travel_charge+=$rate_card_details->ninthHourCharge;
                                    $stage_cost = $rate_card_details->ninthHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->ninthHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->tenthHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->tenthHourCharge;
                                    }
                                    break;
                                }
                                case 10 : {
                                    // $travel_charge+=$rate_card_details->tenthHourCharge;
                                    $stage_cost = $rate_card_details->tenthHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->tenthHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->eleventhHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->eleventhHourCharge;
                                    }
                                    break;
                                }
                                case 11 : {
                                    // $travel_charge+=$rate_card_details->eleventhHourCharge;
                                    $stage_cost = $rate_card_details->eleventhHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->eleventhHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->twelveHourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->twelveHourCharge;
                                    }
                                    break;
                                }
                                case 12 : {
                                    // $travel_charge+=$rate_card_details->twelveHourCharge;
                                    $stage_cost = $rate_card_details->twelveHourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->twelveHourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->above12HourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->above12HourCharge;
                                    }
                                    break;
                                }
                                default : {
                                    // $travel_charge+=$rate_card_details->above12HourCharge;
                                    $stage_cost = $rate_card_details->above12HourCharge;
                                    $travel_charge += $travelled_hours * $rate_card_details->above12HourCharge;
                                    if (($travelled_minutes > 0)) {
                                        // $travel_charge+=$rate_card_details->above12HourCharge;
                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->above12HourCharge;
                                    }
                                    break;
                                }
                            }
                        }
                        $per_day_trip_cost = round($convenience_charge + $travel_charge);
                        $total_trip_cost = ($convenience_charge + $travel_charge) * $trip_days;
                        $total_trip_cost = round($total_trip_cost, 2);
                        $travel_charge = $total_trip_cost;

                        $message = array(
                            "thours_stage" => $travelled_staged_hours,
                            "message" => 'Success',
                            "fare" => $total_trip_cost,
                            "trip_hours" => $trip_hours,
                            "trip_days" => $trip_days,
                            "per_day_trip_cost" => $per_day_trip_cost,
                            "rate_per_hour" => $stage_cost,
                            "hours" => $travel_hours_minutes,
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['tariff_failed'],
                            "status" => -1
                        );
                    }
                } else {

                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "status" => -2,
                        "detail" => $msg_data ['mandatory_data']
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    public function monthly_trip_details_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $pending_monthly_trip = array();
        $completed_monthly_trip = array();
        $passenger_details = array();
        $next_trip_date = NULL;
        $passenger_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($passenger_id) {
                    // get passengers favourite lists
                    $pending_monthly_trip = $this->Trip_Details_Model->getByKeyValueArray(array(
                        'passengerId' => $passenger_id,
                        'billType' => Bill_Type_Enum::MONTHLY,
                        'tripStatus!=' => Trip_Status_Enum::TRIP_COMPLETED
                    ), 'pickupDatetime');
                    if (count($pending_monthly_trip) > 0) {
                        $completed_monthly_trip = $this->Trip_Details_Model->getByKeyValueArray(array(
                            'passengerId' => $passenger_id,
                            'billType' => Bill_Type_Enum::MONTHLY,
                            'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                        ), 'pickupDatetime');
                        $passenger_details = $this->Passenger_Model->getById($passenger_id);
                        $message = array(
                            "pending_trips" => count($pending_monthly_trip),
                            "message" => $msg_data ['success'],
                            "completed_trip" => count($completed_monthly_trip),
                            "wallet_balance" => $passenger_details->walletAmount,
                            "next_trip_date" => ($pending_monthly_trip [0]->pickupDatetime) ? $pending_monthly_trip [0]->pickupDatetime : 'No trip',
                            "next_trip_id" => $pending_monthly_trip [0]->id,
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['failed'],
                            "status" => 2
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to get the favourite list
     */
    public function get_favourite_list_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $passenger_favourite_list = array();
        $passenger_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($passenger_id) {
                    // get passengers favourite lists
                    $passenger_favourite_list = $this->Passenger_Favourites_Model->getByKeyValueArray(array(
                        'passengerId' => $passenger_id,
                        'status' => Status_Type_Enum::ACTIVE
                    ));
                    if (count($passenger_favourite_list) > 0) {
                        foreach ($passenger_favourite_list as $list) {
                            $favourite_list = array(
                                'p_favourite_id' => $list->id,
                                'passenger_id' => $list->passengerId,
                                'p_favourite_place' => '',
                                'p_fav_latitude' => '',
                                'p_fav_longtitute' => '',
                                'd_favourite_place' => '',
                                'd_fav_latitude' => '',
                                'd_fav_longtitute' => '',
                                'fav_comments' => $list->comments,
                                'notes' => '',
                                'fav_loction_type' => $list->typeName,
                                'flat_number' => $list->flatNumber,
                                'building_name' => $list->buildingName
                            );
                            if ($list->type == Favourite_Type_Enum::PICKUP) {
                                $favourite_list ['p_favourite_place'] = $list->location;
                                $favourite_list ['p_fav_latitude'] = $list->latitude;
                                $favourite_list ['p_fav_longtitute'] = $list->longitude;
                            } else if ($list->type == Favourite_Type_Enum::DROP) {
                                $favourite_list ['d_favourite_place'] = $list->location;
                                $favourite_list ['d_fav_latitude'] = $list->latitude;
                                $favourite_list ['d_fav_longtitute'] = $list->longitude;
                            }
                            $response_data [] = $favourite_list;
                        }
                        $message = array(
                            "message" => $msg_data ['success'],
                            "detail" => $response_data,
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['data_not_found'],
                            "status" => 0
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to add the favourite item
     */
    public function edit_favourite_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $check_passenger_favourite_pickup = array();
        $check_passenger_favourite_drop = array();
        $passenger_id = NULL;
        $update_data = array();
        $update_favourite = NULL;
        $insert_favourite = NULL;
        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');

        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($passenger_id && $post_data->p_favourite_place) {
                    if ($post_data->p_favourite_place && $post_data->p_favourite_id > 0) {
                        $check_passenger_favourite_pickup = $this->Passenger_Favourites_Model->getByKeyValueArray(array(
                            'id' => $post_data->p_favourite_id,
                            'passengerId' => $passenger_id,
                            'location' => $post_data->p_favourite_place
                        ));
                    }
                    if ($post_data->d_favourite_place && $post_data->p_favourite_id > 0) {
                        $check_passenger_favourite_drop = $this->Passenger_Favourites_Model->getByKeyValueArray(array(
                            'id' => $post_data->p_favourite_id,
                            'passengerId' => $passenger_id,
                            'location' => $post_data->d_favourite_place
                        ));
                    }

                    if ($post_data->p_favourite_id > 0) {
                        if (!$check_passenger_favourite_pickup || !$check_passenger_favourite_drop) {

                            $update_data = array();
                            if ($check_passenger_favourite_pickup [0]->id || $post_data->p_favourite_place || $post_data->p_fav_latitude || $post_data->p_fav_longtitute) {
                                $update_data = array(
                                    'passengerId' => $passenger_id,
                                    'type' => Favourite_Type_Enum::None,
                                    'typeName' => $post_data->p_fav_locationtype,
                                    'comments' => $post_data->fav_comments,
                                    'buildingName' => $post_data->building_name,
                                    'flatNumber' => $post_data->flat_number,
                                    'landmark' => ($post_data->notes) ? $post_data->notes : '',
                                    'type' => Favourite_Type_Enum::PICKUP,
                                    'location' => $post_data->p_favourite_place,
                                    'latitude' => $post_data->p_fav_latitude,
                                    'longitude' => $post_data->p_fav_longtitute
                                );
                                $update_favourite = $this->Passenger_Favourites_Model->update($update_data, array(
                                    'id' => $post_data->p_favourite_id
                                ));
                            } else if ($check_passenger_favourite_drop [0]->id || $post_data->d_favourite_place || $post_data->d_fav_longtitute || $post_data->d_fav_longtitute) {
                                $update_data = array(
                                    'passengerId' => $passenger_id,
                                    'type' => Favourite_Type_Enum::None,
                                    'typeName' => $post_data->p_fav_locationtype,
                                    'comments' => $post_data->fav_comments,
                                    'buildingName' => $post_data->building_name,
                                    'flatNumber' => $post_data->flat_number,
                                    'landmark' => ($post_data->notes) ? $post_data->notes : '',
                                    'type' => Favourite_Type_Enum::DROP,
                                    'location' => $post_data->d_favourite_place,
                                    'latitude' => $post_data->d_fav_longtitute,
                                    'longitude' => $post_data->d_fav_longtitute
                                );
                                $update_favourite = $this->Passenger_Favourites_Model->update($update_data, array(
                                    'id' => $post_data->p_favourite_id
                                ));
                            }
                            if ($update_favourite) {
                                $message = array(
                                    "message" => $msg_data ['fav_edit_success'],
                                    "detail" => $update_data,
                                    "status" => 1
                                );
                            } else {
                                $message = array(
                                    "message" => $msg_data ['fav_edit_failed'],
                                    "status" => 0
                                );
                            }
                        } else {

                            $message = array(
                                "message" => $msg_data ['fav_exists'],
                                "status" => 2
                            );
                        }
                    } else {
                        $insert_data = array();
                        if ($post_data->p_favourite_place || $post_data->p_fav_latitude || $post_data->p_fav_longtitute) {
                            $insert_data = array(
                                'passengerId' => $passenger_id,
                                'type' => Favourite_Type_Enum::None,
                                'typeName' => $post_data->p_fav_locationtype,
                                'comments' => $post_data->fav_comments,
                                'buildingName' => $post_data->building_name,
                                'flatNumber' => $post_data->flat_number,
                                'landmark' => ($post_data->notes) ? $post_data->notes : '',
                                'type' => Favourite_Type_Enum::PICKUP,
                                'location' => $post_data->p_favourite_place,
                                'latitude' => $post_data->p_fav_latitude,
                                'longitude' => $post_data->p_fav_longtitute,
                                'status' => Status_Type_Enum::ACTIVE
                            );
                            $insert_favourite = $this->Passenger_Favourites_Model->insert($insert_data);
                        } else if ($post_data->d_favourite_place || $post_data->d_fav_longtitute || $post_data->d_fav_longtitute) {
                            $insert_data = array(
                                'passengerId' => $passenger_id,
                                'type' => Favourite_Type_Enum::None,
                                'typeName' => $post_data->p_fav_locationtype,
                                'comments' => $post_data->fav_comments,
                                'buildingName' => $post_data->building_name,
                                'flatNumber' => $post_data->flat_number,
                                'landmark' => ($post_data->notes) ? $post_data->notes : '',
                                'type' => Favourite_Type_Enum::DROP,
                                'location' => $post_data->d_favourite_place,
                                'latitude' => $post_data->d_fav_longtitute,
                                'longitude' => $post_data->d_fav_longtitute,
                                'status' => Status_Type_Enum::ACTIVE
                            );
                            $insert_favourite = $this->Passenger_Favourites_Model->insert($insert_data);
                        }
                        if ($insert_favourite) {
                            $message = array(
                                "message" => $msg_data ['fav_add_success'],
                                "detail" => $insert_data,
                                "favourite_id" => $insert_favourite,
                                "status" => 1
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['fav_add_failed'],
                                "status" => 0
                            );
                        }
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "status" => -3
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used the get nearest driver list
     */
    public function nearestdriver_list_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $passenger_wallet_amount = NULL;
        $passenger_id = NULL;
        $latitude = NULL;
        $longitude = NULL;
        $motor_model = NULL;
        $driver_fare_details = array();
        $available_drivers_list = array();
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;
        $latitude = $post_data->latitude;
        $longitude = $post_data->longitude;
        $motor_model = $post_data->motor_model;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                // To get site info details
                $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                    'id' => 1
                ));
                if ($latitude & $longitude) {
                    // get passenger details
                    $passenger_details = $this->Passenger_Model->getById($passenger_id);
                    // to get rate card details based on companyId & cityId of passenger/driver
                    $rate_card_details = $this->Api_Webservice_Model->getRateCardDetails($passenger_details->companyId, $passenger_details->cityId);
                    $available_drivers = $this->Api_Webservice_Model->getNearestDriverList($latitude, $longitude, $passenger_details->cityId);

                    $driver_fare_details = array(
                        'fare_id' => $rate_card_details [0]->rateCardId,
                        '1hr_to_4hr' => $rate_card_details [0]->firstHourCharge,
                        '5hr_to_8hr' => $rate_card_details [0]->fifthHourCharge,
                        '9hr_to_12hr' => $rate_card_details [0]->ninthHourCharge,
                        'above_12hr' => $rate_card_details [0]->above12HourCharge,
                        'service_tax' => $site_setting_info->tax
                    );

                    foreach ($rate_card_details as $fare) {
                        if ($fare->tripType == Trip_Type_Enum::ONEWAY_TRIP) {
                            $driver_fare_details ['Oneway_7am_9pm'] = $fare->dayConvenienceCharge;
                            $driver_fare_details ['Oneway_9pm_7am'] = $fare->nightConvenienceCharge;
                        } else if ($fare->tripType == Trip_Type_Enum::RETURN_TRIP) {
                            $driver_fare_details ['Return_7am_9pm'] = $fare->dayConvenienceCharge;
                            $driver_fare_details ['Return_9pm_7am'] = $fare->nightConvenienceCharge;
                        }
                    }
                    if ($available_drivers > 0) {
                        foreach ($available_drivers as $list) {

                            $available_drivers_list [] = array(
                                'latitude' => $list->currentLatitude,
                                'longitude' => $list->currentLongitude,
                                'driver_id' => $list->driverId,
                                'driver_full_name' => $list->firstName . " " . $list->lastName,
                                'mobile' => $list->mobile,
                                'distance_km' => round($list->distance, 3)
                            );
                        }
                        $message = array(
                            "detail" => $available_drivers_list,
                            "fare_details" => array(),
                            "driver_fare" => $driver_fare_details,
                            "driver_around_miles" => '',
                            "status" => 1,
                            "report_msg" => $msg_data ['zuver_notice'],
                            "message" => 'success'
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['no_driver'],
                            "fare_details" => array(),
                            "driver_fare" => $driver_fare_details,
                            "report_msg" => $msg_data ['zuver_notice'],
                            "driver_around_miles" => '',
                            "status" => 0
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['lat_long_mandatory'],
                        "detail" => $errors,
                        "report_msg" => $msg_data ['zuver_notice'],
                        "status" => -5
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to cancel the trip after booking
     */
    public function getdriver_reply_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';

        $trip_id = NULL;
        $trip_details = NULL;
        $trip_status_update = NULL;
        $trip_request_update = NULL;

        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $trip_id = $post_data->passenger_tripid;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($trip_id) {
                    $trip_details = $this->Trip_Details_Model->getById($trip_id);
                    $driver_details = $this->Driver_Model->getById($trip_details->driverId);
                    if ($trip_details) {
                        if ($trip_details->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED && $trip_details->driverAcceptedStatus == Driver_Accepted_Status_Enum::ACCEPTED) {

                            $message = array(
                                "message" => $msg_data ['trip_confirmed'],
                                "detail" => array(
                                    'trip_id' => $trip_id,
                                    'driverdetails' => ''
                                ),
                                "status" => 1
                            );
                        } else {
                            // update the tripstatus, notification status, driver sccepted status in tripdetails table
                            $trip_status_update = $this->Trip_Details_Model->update(array(
                                'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                                'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER
                            ), array(
                                'id' => $trip_id
                            ));

                            // update the tripStatus in driverrequestdetails table
                            $trip_request_update = $this->Driver_Request_Details_Model->update(array(
                                'tripStatus' => Driver_Request_Status_Enum::PASSENGER_CANCELLED
                            ), array(
                                'tripId' => $trip_id
                            ));

                            if ($trip_status_update && $trip_request_update) {
                                $message = array(
                                    "message" => $msg_data ['passenger_cancel_success'],
                                    "status" => 3
                                );
                            } else {
                                $message = array(
                                    "message" => $msg_data ['passenger_cancel_failed'],
                                    "status" => -1
                                );
                            }
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_trip'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "status" => 0
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to delete the favourite item
     */
    public function delete_favourite_locations_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $delete_favourites = NULL;
        $passenger_id = NULL;

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passengerid;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($passenger_id && $post_data->favourite_id) {
                    $delete_favourites = $this->Passenger_Favourites_Model->delete(array(
                        'id' => $post_data->favourite_id
                    ));
                    if ($delete_favourites) {
                        $message = array(
                            "message" => $msg_data ['fav_delete_success'],
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['fav_delete_failed'],
                            "status" => 2
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "errors" => $msg_data ['mandatory_data'],
                        "status" => 3
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to cancel the trip after driver assign
     */
    public function cancel_trip_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $check_trip_avilablity = NULL;
        $remarks = NULL;
        $trip_id = NULL;
        $trip_details = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $trip_id = $post_data->passenger_log_id;
        $remarks = $post_data->remarks;
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($trip_id) {
                    // get trip details
                    $trip_details = $this->Trip_Details_Model->getById($trip_id);
                    $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id, $trip_details->passengerId);
                    // check trip avilable for driver/any in progress trip before logout
                    $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                        'tripId' => $trip_id
                    ), 'id DESC');
                    if ($check_trip_avilablity) {
                        if (($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED) && ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED || $trip_details [0]->tripStatus == Trip_Status_Enum::IN_PROGRESS || $trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT)) {
                            $message = array(
                                "message" => $msd_data ['trip_progress'],
                                "status" => -1
                            );
                        } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                            $message = array(
                                "message" => $msd_data ['trip_reject_passenger'],
                                "status" => -1
                            );
                        } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                            $message = array(
                                "message" => $msd_data ['trip_completed'],
                                "status" => -1
                            );
                        } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_REJECTED) {
                            $message = array(
                                "message" => $msd_data ['trip_reject_driver'],
                                "status" => -1
                            );
                        } else if (($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) && ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED)) {

                            $trip_status_update = $this->Trip_Details_Model->update(array(
                                'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                                'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                                'passengerRejectComments' => $remarks
                            ), array(
                                'id' => $trip_id
                            ));

                            $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                'tripStatus' => Driver_Request_Status_Enum::PASSENGER_CANCELLED
                            ), array(
                                'tripId' => $trip_id
                            )); // get last inserted shift id for particular driver
                            $get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                'driverId' => $trip_details [0]->driverId
                            ), 'id DESC');
                            $driver_shift_status_update = $this->Driver_Shift_History_Model->update(array(
                                'availabilityStatus' => Driver_Available_Status_Enum::FREE
                            ), array(
                                'id' => $get_last_shift_id->id
                            ));
                            // sms & email start
                            if (SMS && $trip_request_status_update) {
                                // SMS To passenger

                                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                    'smsTitle' => 'trip_cancel'
                                ));

                                $message_data = $message_details->smsContent;
                                $message_data = str_replace("##BOOKINGID##", $trip_details [0]->bookingKey, $message_data);
                                if ($trip_details [0]->passengerMobile) {

                                    $this->Api_Webservice_Model->sendSMS($trip_details [0]->passengerMobile, $message_data);
                                }

                                // SMS TO driver
                                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                    'smsTitle' => 'trip_cancel_driver_sms'
                                ));
                                $customer_care_no = ($trip_details [0]->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details [0]->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details [0]->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
                                $message_data = $message_details->smsContent;
                                $message_data = str_replace("##CUSTOMERCARE##", $customer_care_no, $message_data);

                                // print_r($message);exit;
                                if ($trip_details [0]->diverMobile) {

                                    $this->Api_Webservice_Model->sendSMS($trip_details [0]->diverMobile, $message_data);
                                    // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                    $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                                }
                            }

                            // sms & email end
                            $message = array(
                                "message" => $msg_data ['passenger_cancel_success'],
                                "status" => 1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_trip'],
                            "status" => 2
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip'],
                        "status" => 2
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to get the emergency list
     */
    public function emergency_list_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = array();
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $passenger_emergency_list = NULL;
        $passenger_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functonality
                if ($passenger_id > 0) {
                    $passenger_emergency_list = $this->Passenger_Emergency_Contact_Model->getByKeyValueArray(array(
                        'passengerId' => $passenger_id,
                        'status' => Status_Type_Enum::ACTIVE
                    ));
                    if ($passenger_emergency_list) {
                        foreach ($passenger_emergency_list as $list) {
                            $response_data [] = array(
                                'emergency_phone' => $list->mobile,
                                'emergency_name' => $list->name,
                                'emergency_email' => $list->email,
                                'passenger_id' => $list->passengerId,
                                'emergency_id' => $list->id
                            );
                        }
                    }
                    if (count($response_data) > 0) {
                        $message = array(
                            "emergency_data" => $response_data,
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['data_not_found'],
                            "status" => 2
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }

        echo json_encode($message);
    }

    /**
     * used to delete the emergency item
     */
    public function delete_emergency_contact_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = array();
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $delete_passenger_emergency = NULL;
        $passenger_id = NULL;
        $emergency_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;
        $emergency_id = $post_data->emergency_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functonality
                if ($passenger_id && $emergency_id) {
                    $delete_passenger_emergency = $this->Passenger_Emergency_Contact_Model->delete(array(
                        'id' => $emergency_id,
                        'passengerId' => $passenger_id
                    ));
                    if ($delete_passenger_emergency) {
                        $message = array(
                            "message" => $msg_data ['sos_delete_success'],
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['sos_delete_failed'],
                            "status" => -2
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "detail" => $msg_data ['mandatory_data'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }

        echo json_encode($message);
    }

    /**
     * used to add the emergency contacts
     */
    public function emergency_contact_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $check_passenger_emergency = NULL;
        $passenger_id = NULL;
        $emergency_id = NULL;
        $passenger_emergency_exists = NULL;
        $insert_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functonality
                if ($passenger_id && $post_data->emergency_name && $post_data->emergency_phone && $post_data->emergency_email) {
                    $check_passenger_emergency = $this->Passenger_Emergency_Contact_Model->getByKeyValueArray(array(
                        'passengerId' => $passenger_id
                    ));
                    // debug_exit(EMERGENCY_COUNT_LIMIT);
                    if (count($check_passenger_emergency) < EMERGENCY_COUNT_LIMIT) {
                        $passenger_emergency_exists = $this->Passenger_Emergency_Contact_Model->getByKeyValueArray(array(
                            'passengerId' => $passenger_id,
                            'mobile' => $post_data->emergency_phone,
                            'email' => $post_data->emergency_email,
                            'status' => Status_Type_Enum::ACTIVE
                        ));
                        if (!$passenger_emergency_exists) {
                            $insert_data = array(
                                'passengerId' => $passenger_id,
                                'name' => $post_data->emergency_name,
                                'email' => $post_data->emergency_email,
                                'mobile' => $post_data->emergency_phone,
                                'status' => Status_Type_Enum::ACTIVE
                            );
                            $emergency_id = $this->Passenger_Emergency_Contact_Model->insert($insert_data);
                            if ($emergency_id) {
                                $message = array(
                                    "message" => $msg_data ['sos_add_success'],
                                    "emergency_id" => $emergency_id,
                                    "status" => 1
                                );
                            } else {
                                $message = array(
                                    "message" => $msg_data ['sos_add_failed'],
                                    "status" => 2
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['email_mobile_exists'],
                                "status" => -2
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['sos_limit_exceeds'],
                            "status" => 3
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "detail" => $msg_data ['mandatory_data'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }

        echo json_encode($message);
    }

    /**
     * used to get the wallet amount
     */
    public function passenger_refresh_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $passenger_wallet_amount = NULL;
        $passenger_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                if ($passenger_id) {
                    // actual functionality
                    $passenger_wallet_amount = $this->Passenger_Model->getColumnByKeyValueArray('walletAmount', array(
                        'id' => $passenger_id
                    ));
                    if ($passenger_wallet_amount) {
                        $message = $msg_data ['success'];
                    } else {
                        $message = $msg_data ['failed'];
                    }
                } else {
                    $message = $msg_data ['invalid_user'];
                }
                $message = array(
                    'passenger_data' => $passenger_wallet_amount [0]->walletAmount,
                    'message' => $message
                );
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to verify the promo code
     */
    public function check_promocode_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $promocode_exists = NULL;
        $promocode_limit_exists = NULL;
        $passenger_id = 0;
        $promocode = NULL;
        $city_id = 0;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }
        $promocode = ($post_data->promo_code) ? $post_data->promo_code : '';
        $passenger_id = $post_data->passenger_id;
        $city_id = $post_data->company_id;
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality

                // Check promocode exists or not
                if ($promocode) {
                    // Check promocode exists or not for particular passenger at particular city
                    $promocode_exists = $this->Api_Webservice_Model->checkPromocodeAvailablity($promocode, $passenger_id, $city_id);

                    if ($promocode_exists && count($promocode_exists) > 0) {
                        /*
						 * $promocode_exists = $this->Passenger_Promocode_Details_Model->getByKeyValueArray ( array (
						 * 'passengerId' => 0,
						 * 'promoCode' => $promocode,
						 * 'cityId' => $city_id
						 * ) );
						 */

                        if (strtotime($promocode_exists [0]->promoStartDate) < getCurrentUnixDateTime()) {
                            $message = array(
                                "message" => $msg_data ['promocode_start'],
                                "status" => 2
                            );
                        }
                        if (strtotime($promocode_exists [0]->promoEndDate) > getCurrentUnixDateTime()) {
                            $message = array(
                                "message" => $msg_data ['promocode_expiry'],
                                "status" => 2
                            );
                        }

                        // check promocode limit exceeds or not for specific passenger (note: only completed trip)
                        $promocode_user_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                            'passengerId' => $passenger_id,
                            'promoCode' => $promocode,
                            'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                        ));
                        // check promocode limit exceeds or not total limit (note: only completed trip)
                        $promocode_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                            'promoCode' => $promocode,
                            'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                        ));
                        if (count($promocode_limit_exists) >= $promocode_exists [0]->promoCodeLimit) {
                            $message = array(
                                "message" => $msg_data ['promocode_total_limit'],
                                "status" => 2
                            );
                        } else if (count($promocode_user_limit_exists) >= $promocode_exists [0]->promoCodeUserLimit) {
                            $message = array(
                                "message" => $msg_data ['promocode_user_limit'],
                                "status" => 2
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['promocode_success'],
                                "status" => 1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_promocode'],
                            "status" => 2
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_promocode'],
                        "status" => 3
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }

        echo json_encode($message);
    }

    /**
     * used to log out the app
     */
    public function passenger_logout_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $update_passenger_logout = NULL;
        $passenger_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($passenger_id) {
                    // update the passenger logout data
                    $update_data = array(
                        'signupFrom' => Signup_Type_Enum::NONE,
                        'loginStatus' => Status_Type_Enum::INACTIVE,
                        'deviceId' => '',
                        'deviceType' => Device_Type_Enum::NO_DEVICE
                    );
                    $update_passenger_logout = $this->Passenger_Model->update($update_data, array(
                        'id' => $passenger_id
                    ));
                    // update the passenger logout auth key=''
                    $update_auth_key = $this->Auth_Key_Model->update(array(
                        'key' => ''
                    ), array(
                        'userId' => $auth_user_id,
                        'userType' => $auth_user_type
                    ));
                    // @todo Delete rejected trip once passenger is logged out from logout date time
                    if ($update_auth_key) {
                        $message = array(
                            "message" => $msg_data ['logout_success'],
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['logout_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * send emergency report
     */
    public function emergency_report_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $passenger_exists = array();
        $passenger_id = NULL;
        $latitude = NULL;
        $longitude = NULL;
        $current_location = NULL;
        $emergency_list = array();
        $address = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;
        $latitude = $post_data->latitude;
        $longitude = $post_data->longitude;
        $current_location = $post_data->current_location;

        $insert_data = array('passengerId' => $passenger_id, 'location' => $current_location, 'latitude' => $latitude, 'longitude' => $longitude);
        $emergency_id = $this->Emergency_Request_Details_Model->insert($insert_data);
        // Google map url link
        $google_map_url = "http://maps.google.com/maps?z=11&t=m&q=loc:" . $latitude . "+" . $longitude;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($passenger_id) {
                    $passenger_exists = $this->Passenger_Model->getById($passenger_id);
                    if (count($passenger_exists) > 0) {
                        $emergency_list = $this->Passenger_Emergency_Contact_Model->getByKeyValueArray(array(
                            'passengerId' => $passenger_id,
                            'status' => Status_Type_Enum::ACTIVE
                        ));

                        if (count($emergency_list) > 0) {

                            foreach ($emergency_list as $list) {
                                $address = array();
                                $address [0] = substr($google_map_url, 0, 20);
                                $address [1] = substr($google_map_url, 20, 20);
                                $address [2] = substr($google_map_url, 40, 20);
                                $address [3] = substr($google_map_url, 60, 20);
                                $address [4] = substr($google_map_url, 80, 20);
                                $address [5] = substr($google_map_url, 100, 20);
                                // $address[6]=substr($current_location,120,20);
                                // $address[7]=substr($current_location,140,20);

                                if (strlen($google_map_url) > 120) {
                                    $address [5] = cut_string_using_last(',', $address [5], 'left', false);
                                }

                                if (SMS && $list->mobile) {
                                    $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                        'smsTitle' => 'emergency_contact'
                                    ));

                                    $message_data = $message_details->smsContent;

                                    $message_data = str_replace("##PASSENGERNAME##", $passenger_exists->firstName . ' ' . $passenger_exists->lastName, $message_data);
                                    $message_data = str_replace("##DEMO1##", "", $message_data);
                                    $message_data = str_replace("##Address1##", $address [0], $message_data);
                                    $message_data = str_replace("##Address2##", $address [1], $message_data);
                                    $message_data = str_replace("##Address3##", $address [2], $message_data);
                                    $message_data = str_replace("##Address4##", $address [3], $message_data);
                                    $message_data = str_replace("##Address5##", $address [4], $message_data);
                                    $message_data = str_replace("##Address6##", $address [5], $message_data);
                                    // $message_data = str_replace("##Address7##",$address[6],$message_data);
                                    // $message_data = str_replace("##Address8##",$address[7],$message_data);
                                    // print_r($message);exit;
                                    if ($list->mobile != "") {

                                        $this->Api_Webservice_Model->sendSMS($list->mobile, $message_data);
                                    }
                                }

                                $from = SMTP_EMAIL_ID;
                                $to = $list->email;
                                $subject = $passenger_exists->firstName . " " . $passenger_exists->lastName . " is in an emergency!";
                                $data = array(
                                    'emergency_name' => $list->name,
                                    'address' => $current_location,
                                    'google_map_url' => $google_map_url,
                                    'passenger_name' => $passenger_exists->firstName . " " . $passenger_exists->lastName
                                );
                                $message_data = $this->load->view('emailtemplate/emergency_contact', $data, TRUE);

                                if (SMTP && $list->email) {

                                    if ($to) {
                                        $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                                    }
                                } else {

                                    // To send HTML mail, the Content-type header must be set
                                    $headers = 'MIME-Version: 1.0' . "\r\n";
                                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                    // Additional headers
                                    $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                                    $headers .= 'To: <' . $to . '>' . "\r\n";
                                    if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                                        mail($to, $subject, $message_data, $headers);
                                    }
                                }
                            }
                            $message = array(
                                "message" => $msg_data ['sos_notice'],
                                "status" => 1
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['data_not_found'],
                                "status" => 2
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_user'],
                            "status" => 2
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => 2
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * sent the success and failure response
     */
    public function getPaytmStatus_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $passenger_wallet_amount = NULL;
        $payment_type = NULL;
        $passenger_id = NULL;
        $trip_id = NULL;
        $payment_status = NULL;
        $transaction_id = Null;
        $amount = NULL;
        $trip_details = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $payment_type = $post_data->payment_type;
        $passenger_id = $post_data->passenger_id;
        $trip_id = $post_data->trip_id;
        $payment_status = $post_data->payment_status;
        $transaction_id = $post_data->transaction_id;
        $amount = $post_data->amount;
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($trip_id) {
                    $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id, $passenger_id);
                    if ($payment_status == "SUCCESS" && $trip_details [0]->paymentMode == Payment_Mode_Enum::PAYTM) {
                        $trip_status_update = $this->Trip_Details_Model->update(array(
                            'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED,
                            'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION
                        ), array(
                            'id' => $trip_id
                        ));

                        $transaction_status_update = $this->Trip_Transaction_Details_Model->update(array(
                            'paymentStatus' => $payment_status,
                            'transactionId' => $transaction_id
                        ), array(
                            'id' => $trip_details [0]->transactionId
                        ));
                        $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                            'tripStatus' => Driver_Request_Status_Enum::COMPLETED_TRIP,
                            'selectedDriverId' => 0
                        ), array(
                            'tripId' => $trip_id
                        ));
                        // get last inserted shift id for particular driver
                        $get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                            'driverId' => $trip_details [0]->driverId
                        ), 'id DESC');
                        $driver_shift_status_update = $this->Driver_Shift_History_Model->update(array(
                            'availabilityStatus' => Driver_Available_Status_Enum::FREE
                        ), array(
                            'id' => $get_last_shift_id->id
                        ));
                        if (SMS && $transaction_status_update) {

                            $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                'smsTitle' => 'paytm_success'
                            ));
                            $message_data = $message_details->smsContent;

                            $message_data = str_replace("##AMOUNT##", $amount, $message_data);

                            // SMS To passenger
                            if ($trip_details [0]->passengerMobile) {

                                $this->Api_Webservice_Model->sendSMS($trip_details [0]->passengerMobile, $message_data);
                            }

                            // SMS TO driver
                            if ($trip_details [0]->diverMobile) {

                                $this->Api_Webservice_Model->sendSMS($trip_details [0]->diverMobile, $message_data);
                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                            }
                        }

                        $message = array(
                            "message" => $msg_data ['paytm_success'],
                            "transaction_id" => $transaction_id,
                            "status" => 1,
                            "payment_status" => $payment_status,
                            "amount" => $amount
                        );
                    } else if ($payment_status == "FAILURE" && $trip_details [0]->paymentMode == Payment_Mode_Enum::PAYTM) {
                        if (SMS) {

                            $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                'smsTitle' => 'paytm_failure_driver_sms'
                            ));
                            $message_data = $message_details->smsContent;

                            $customer_care_no = ($trip_details [0]->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details [0]->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
                            $message_data = str_replace("##CUSTOMERCARE##", $customer_care_no, $message_data);

                            // SMS TO driver
                            if ($trip_details [0]->diverMobile) {

                                $this->Api_Webservice_Model->sendSMS($trip_details [0]->diverMobile, $message_data);
                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                            }
                        }
                        $message = array(
                            "message" => $msg_data ['paytm_failed'],
                            "payment_status" => $payment_status,
                            "status" => 0,
                            "amount" => $amount
                        );
                    }
                } else {

                    $message = array(
                        "message" => $msg_data ['invalid_trip'],
                        "status" => 0
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * update the passenger profile
     */
    public function edit_passenger_profile_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $email = NULL;
        $first_name = NULL;
        $mobile = NULL;
        $password = NULL;
        $profile_image = NULL;
        $update_passenger_profile = NULL;

        $passenger_id = NULL;
        $user_exists = array();

        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $email = $post_data->email;
        $first_name = $post_data->firstname;
        $mobile = $post_data->phone;
        $password = $post_data->password;
        $passenger_id = $post_data->passenger_id;
        $profile_image = $post_data->profile_image;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($email) {
                    $user_exists = $this->Passenger_Model->getOneByKeyValueArray(array(
                        'id' => $passenger_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'activationStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($user_exists) {
                        $update_passenger = $this->Passenger_Model->update(array(
                            'firstName' => $first_name
                        ), array(
                            'id' => $passenger_id
                        ));
                        if ($update_passenger) {
                            if ($profile_image) {
                                if (array_key_exists('profile_image', $post_data) && $profile_image) {

                                    $data ['file_name'] = $this->getbase64ImageUrl($passenger_id, $profile_image);

                                    if ($data) {

                                        $update_passenger_profile = $this->Passenger_Model->update(array(
                                            'profileImage' => $data ['file_name']
                                        ), array(
                                            'id' => $passenger_id
                                        ));
                                    }
                                }

                                if ($update_passenger_profile) {

                                    $message = array(
                                        "message" => $msg_data ['signup_success'],
                                        "firstname" => ($first_name) ? $first_name : '',
                                        "profile_image" => $profile_image,
                                        "status" => 1
                                    );
                                } else {
                                    $message = array(
                                        "message" => $msg_data ['signup_success_image_failed'],
                                        "status" => -4
                                    );
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['signup_success'],
                                    "status" => 1
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['signup_failed'],
                                "status" => -4
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_user'],
                            "status" => -4
                        );
                    }
                } else {
                    $message = array(
                        "message" => 'Invalid Email',
                        "status" => -4
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * get the passenger profile info
     */
    public function passenger_profile_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $passenger_data = NULL;
        $passenger_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->userid;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($passenger_id) {
                    $passenger_data = $this->Passenger_Model->getById($passenger_id);
                    if ($passenger_data) {
                        $response_data = array(
                            'profile_image' => ($passenger_data->profileImage) ? passenger_content_url($passenger_id . '/' . $passenger_data->profileImage) : image_url('app/dummy-avatar.png'),
                            'name' => $passenger_data->firstName,
                            'lastname' => $passenger_data->lastName,
                            'email' => $passenger_data->email,
                            'referral_code' => $passenger_data->passengerCode,
                            'wallet_amount' => $passenger_data->walletAmount,
                            'phone' => $passenger_data->mobile,
                            'discount' => 0,
                            'user_status' => ($passenger_data->status == Status_Type_Enum::ACTIVE) ? 'A' : 'N',

                            'login_from' => $passenger_data->signupFrom
                        );

                        $message = array(
                            "message" => $msg_data ['success'],
                            "detail" => $response_data,
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_user'],
                            "status" => 0
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to get the Hash key from server
     */
    public function getpayuhashes_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        // $passenger_id = $post_data->passenger_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                $response_data = array();
                // $firstname, $email can be "", i.e empty string if needed. Same should be sent to PayU server (in request params) also.
                $key = ($post_data->key != NULL && $post_data->key != '') ? $post_data->key : "";
                $salt = ($post_data->salt != NULL && $post_data->salt != '') ? $post_data->salt : "";
                $txnid = $post_data->txnid;
                $amount = $post_data->amount;
                $productinfo = $post_data->productinfo;
                $firstname = $post_data->firstname;
                $email = $post_data->email;
                $udf1 = $post_data->udf1;
                $udf2 = $post_data->udf2;
                $udf3 = $post_data->udf3;
                $udf4 = $post_data->udf4;
                $udf5 = $post_data->udf5;
                $user_credentials = $post_data->user_credentials;
                $offerKey = $post_data->offerKey;
                $cardBin = $post_data->cardBin;

                $payhash_str = $key . '|' . $this->checkNull($txnid) . '|' . $this->checkNull($amount) . '|' . $this->checkNull($productinfo) . '|' . $this->checkNull($firstname) . '|' . $this->checkNull($email) . '|' . $this->checkNull($udf1) . '|' . $this->checkNull($udf2) . '|' . $this->checkNull($udf3) . '|' . $this->checkNull($udf4) . '|' . $this->checkNull($udf5) . '||||||' . $salt;
                $paymentHash = strtolower(hash('sha512', $payhash_str));
                $response_data ['payment_hash'] = $paymentHash;

                $cmnNameMerchantCodes = 'get_merchant_ibibo_codes';
                $merchantCodesHash_str = $key . '|' . $cmnNameMerchantCodes . '|default|' . $salt;
                $merchantCodesHash = strtolower(hash('sha512', $merchantCodesHash_str));
                $response_data ['get_merchant_ibibo_codes_hash'] = $merchantCodesHash;

                $cmnMobileSdk = 'vas_for_mobile_sdk';
                $mobileSdk_str = $key . '|' . $cmnMobileSdk . '|default|' . $salt;
                $mobileSdk = strtolower(hash('sha512', $mobileSdk_str));
                $response_data ['vas_for_mobile_sdk_hash'] = $mobileSdk;

                $cmnPaymentRelatedDetailsForMobileSdk1 = 'payment_related_details_for_mobile_sdk';
                $detailsForMobileSdk_str1 = $key . '|' . $cmnPaymentRelatedDetailsForMobileSdk1 . '|default|' . $salt;
                $detailsForMobileSdk1 = strtolower(hash('sha512', $detailsForMobileSdk_str1));
                $response_data ['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk1;

                // used for verifying payment(optional)
                $cmnVerifyPayment = 'verify_payment';
                $verifyPayment_str = $key . '|' . $cmnVerifyPayment . '|' . $txnid . '|' . $salt;
                $verifyPayment = strtolower(hash('sha512', $verifyPayment_str));
                $response_data ['verify_payment_hash'] = $verifyPayment;

                if ($user_credentials != NULL && $user_credentials != '') {
                    $cmnNameDeleteCard = 'delete_user_card';
                    $deleteHash_str = $key . '|' . $cmnNameDeleteCard . '|' . $user_credentials . '|' . $salt;
                    $deleteHash = strtolower(hash('sha512', $deleteHash_str));
                    $response_data ['delete_user_card_hash'] = $deleteHash;

                    $cmnNameGetUserCard = 'get_user_cards';
                    $getUserCardHash_str = $key . '|' . $cmnNameGetUserCard . '|' . $user_credentials . '|' . $salt;
                    $getUserCardHash = strtolower(hash('sha512', $getUserCardHash_str));
                    $response_data ['get_user_cards_hash'] = $getUserCardHash;

                    $cmnNameEditUserCard = 'edit_user_card';
                    $editUserCardHash_str = $key . '|' . $cmnNameEditUserCard . '|' . $user_credentials . '|' . $salt;
                    $editUserCardHash = strtolower(hash('sha512', $editUserCardHash_str));
                    $response_data ['edit_user_card_hash'] = $editUserCardHash;

                    $cmnNameSaveUserCard = 'save_user_card';
                    $saveUserCardHash_str = $key . '|' . $cmnNameSaveUserCard . '|' . $user_credentials . '|' . $salt;
                    $saveUserCardHash = strtolower(hash('sha512', $saveUserCardHash_str));
                    $response_data ['save_user_card_hash'] = $saveUserCardHash;

                    $cmnPaymentRelatedDetailsForMobileSdk = 'payment_related_details_for_mobile_sdk';
                    $detailsForMobileSdk_str = $key . '|' . $cmnPaymentRelatedDetailsForMobileSdk . '|' . $user_credentials . '|' . $salt;
                    $detailsForMobileSdk = strtolower(hash('sha512', $detailsForMobileSdk_str));
                    $response_data ['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk;
                }

                if ($offerKey != NULL && !empty ($offerKey)) {
                    $cmnCheckOfferStatus = 'check_offer_status';
                    $checkOfferStatus_str = $key . '|' . $cmnCheckOfferStatus . '|' . $offerKey . '|' . $salt;
                    $checkOfferStatus = strtolower(hash('sha512', $checkOfferStatus_str));
                    $response_data ['check_offer_status_hash'] = $checkOfferStatus;
                }

                if ($cardBin != NULL && !empty ($cardBin)) {
                    $cmnCheckIsDomestic = 'check_isDomestic';
                    $checkIsDomestic_str = $key . '|' . $cmnCheckIsDomestic . '|' . $cardBin . '|' . $salt;
                    $checkIsDomestic = strtolower(hash('sha512', $checkIsDomestic_str));
                    $response_data ['check_isDomestic_hash'] = $checkIsDomestic;
                }
                $message = (array(
                    'result' => $response_data,
                    "status" => 1
                ));
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * update the wallet amount
     */
    public function wallet_update_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $wallet_amount = NULL;
        $passenger_id = NULL;
        $transaction_status = NULL;
        $transaction_id = NULL;
        $card_holdername = NULL;
        $wallet_balance_amount = NULL;

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;
        $wallet_amount = $post_data->amount;
        $transaction_amount = $wallet_amount;
        // @todo update the transaction history of the passenger wallet debit with status & transaction id
        $transaction_status = $post_data->transaction_status;
        $transaction_id = $post_data->transaction_id;
        $card_holdername = $post_data->card_holdername;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($passenger_id && $wallet_amount && is_numeric($wallet_amount)) {
                    if ($wallet_amount > 0) {
                        $current_wallet_balance = $this->Passenger_Model->getById($passenger_id);
                        $wallet_amount += $current_wallet_balance->walletAmount;
                        $update_wallet = $this->Passenger_Model->update(array(
                            'walletAmount' => $wallet_amount
                        ), array(
                            'id' => $passenger_id
                        ));

                        if ($update_wallet) {
                            $insert_data = array(
                                'passengerId' => $passenger_id,
                                'transactionAmount' => $transaction_amount,
                                'transactionId' => $transaction_id,
                                'transactionStatus' => $transaction_status,
                                'previousAmount' => $current_wallet_balance->walletAmount,
                                'currentAmount' => $wallet_amount,
                                'manipulationType' => Manipulation_Type_Enum::ADDITION,
                                'transactionType' => Transaction_Type_Enum::PAYUBIZ
                            );
                            $passenger_wallet_details_update = $this->Passenger_Wallet_Details_Model->insert($insert_data);

                            $wallet_balance_amount = $this->Passenger_Model->getColumnByKeyValueArray('walletAmount', array(
                                'id' => $passenger_id
                            ));
                            $message = array(
                                "message" => $msg_data ['wallet_success'],
                                "amount" => $wallet_balance_amount [0]->walletAmount,
                                "status" => 1
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['wallet_failed'],
                                "status" => -2
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['wallet_empty'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "status" => -3,
                        "detail" => $msg_data ['mandatory_data']
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * get the booking list
     */
    public function booking_list_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $passenger_pending_trip = array();
        $passenger_pending_trip_list = NULL;
        $passenger_past_trip = array();
        $passenger_past_trip_list = NULL;
        $passenger_id = NULL;
        $start_offset = NULL;
        $end_limit = NULL;
        $device_type = NULL;
        $passenger_exists = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->id;
        $start_offset = $post_data->start;
        $end_limit = $post_data->limit;
        $device_type = $post_data->device_type;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual funtionality
                if ($passenger_id || $start_offset || $end_limit || $device_type) {
                    $passenger_exists = $this->Passenger_Model->getById($passenger_id);
                    if ($passenger_exists) {
                        $passenger_pending_trip_list = $this->Api_Webservice_Model->getPendingTripList($passenger_id, $device_type, $start_offset, $end_limit);
                        if ($passenger_pending_trip_list) {

                            foreach ($passenger_pending_trip_list as $list) {
                                $travel_status = NULL;
                                $travel_msg = NULL;
                                switch ($list->tripStatus) {
                                    case Trip_Status_Enum::BOOKED :
                                        $travel_status = 0;
                                        $travel_msg = Trip_Status_Enum::BKD;
                                        break;
                                    case Trip_Status_Enum::IN_PROGRESS :
                                        $travel_status = 2;
                                        $travel_msg = Trip_Status_Enum::IPR;
                                        break;
                                    case Trip_Status_Enum::DRIVER_ARRIVED :
                                        $travel_status = 7;
                                        $travel_msg = Trip_Status_Enum::DAD;
                                        break;
                                    case Trip_Status_Enum::READY_TO_START :
                                        $travel_status = 3;
                                        $travel_msg = Trip_Status_Enum::RTS;
                                        break;
                                    case Trip_Status_Enum::TRIP_CONFIRMED :
                                        $travel_status = 9;
                                        $travel_msg = Trip_Status_Enum::TCD;
                                        break;
                                    case Trip_Status_Enum::WAITING_FOR_PAYMENT :
                                        $travel_status = 5;
                                        $travel_msg = Trip_Status_Enum::WFP;
                                        break;
                                    case Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT :
                                        $travel_status = 11;
                                        $travel_msg = Trip_Status_Enum::WPP;
                                        break;
                                    default :
                                        $travel_status = 4;
                                        $travel_msg = Trip_Status_Enum::CANCELLED_BY_PASSENGER;
                                        break;
                                }
                                $passenger_pending_trip [] = array(
                                    'passengers_log_id' => $list->tripId,
                                    'pickup_location' => $list->pickupLocation,
                                    'drop_location' => $list->dropLocation,
                                    'pickup_longitude' => $list->pickupLongitude,
                                    'pickup_latitude' => $list->pickupLatitude,
                                    'drop_longitude' => $list->dropLongitude,
                                    'drop_latitude' => $list->dropLatitude,
                                    'pickuptime' => $list->pickupDatetime,
                                    'travel_status' => $travel_status,
                                    'travel_msg' => $travel_msg,
                                    'passenger_name' => $list->passengerFirstName . ' ' . $list->passengerLastName,
                                    'driver_id' => $list->driverId,
                                    'notes_driver' => '',
                                    'drivername' => $list->driverFirstName . ' ' . $list->driverLastName,
                                    'drop_time' => $list->dropDatetime,
                                    'trip_duration' => $list->tripDuration
                                );
                            }
                        }
                        $passenger_past_trip_list = $this->Api_Webservice_Model->getPastTripList($passenger_id, $device_type, $start_offset, $end_limit);
                        if ($passenger_past_trip_list) {
                            foreach ($passenger_past_trip_list as $list) {
                                $passenger_past_trip [] = array(
                                    'passengers_log_id' => $list->tripId,
                                    'pickup_location' => $list->pickupLocation,
                                    'drop_location' => $list->dropLocation,
                                    'pickup_longitude' => $list->pickupLongitude,
                                    'pickup_latitude' => $list->pickupLatitude,
                                    'drop_longitude' => $list->dropLongitude,
                                    'drop_latitude' => $list->dropLatitude,
                                    'pickuptime' => $list->actualPickupDatetime,
                                    'travel_status' => 1,
                                    'passenger_name' => $list->passengerFirstName . ' ' . $list->passengerLastName,
                                    'driver_id' => $list->driverId,
                                    'notes_driver' => '',
                                    'drivername' => $list->driverFirstName . ' ' . $list->driverLastName,
                                    'drop_time' => $list->dropDatetime,
                                    'trip_duration' => $list->tripDuration
                                );
                            }
                        }

                        $response_data ['pending_bookings'] = $passenger_pending_trip;
                        $response_data ['past_bookings'] = $passenger_past_trip;
                        if (count($response_data) > 0) {
                            // $message = $passengers_current;
                            $message = array(
                                "message" => $msg_data ['success'],
                                "detail" => $response_data,
                                "status" => 1
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['data_not_found'],
                                "status" => 0
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_user'],
                            "status" => -1
                        );
                    }
                } else {

                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "detail" => $msg_data ['mandatory_data'],
                        "status" => 2
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * get the trip update from server(Continues request-every 5 seconds)
     */
    public function getpassenger_update_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';

        $passenger_id = NULL;
        $trip_id = NULL;
        $request_type = NULL;
        $passenger_exists = NULL;
        $trip_details = NULL;

        $arrived_display = NULL;
        $tripstart_display = NULL;
        $trip_complete_display = NULL;
        $tripfare_update_display = NULL;
        $driver_cancel_display = NULL;
        $trip_complete_paytm_display = NULL;
        $payment_confirmed_display = NULL;

        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $passenger_id = $post_data->passenger_id;
        $request_type = $post_data->request_type;
        $trip_id = $post_data->trip_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual funtioanality
                if ($passenger_id) {
                    if (!$request_type) {
                        $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id, $passenger_id);
                        if (count($trip_details) > 0) {
                            if ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_PASSENGER) {
                                $passenger_cancel_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_PASSENGER && $trip_details [0]->notificationStatus == Trip_Status_Enum::CANCELLED_BY_PASSENGER) ? 1 : 0;
                                $message = array(
                                    "message" => $msg_data ['trip_reject_passenger'],
                                    "driver_latitute" => $trip_details [0]->currentLatitude,
                                    "driver_longtitute" => $trip_details [0]->currentLongitude,
                                    "status" => 9,
                                    "display" => $passenger_cancel_display
                                );
                                $trip_status_changed = $this->Trip_Details_Model->update(array(
                                    'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION
                                ), array(
                                    'id' => $trip_id
                                ));
                            } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::BOOKED && $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::NONE) {
                                $message = array(
                                    "message" => $msg_data ['trip_not_started'],
                                    "driver_latitute" => $trip_details [0]->currentLatitude,
                                    "driver_longtitute" => $trip_details [0]->currentLongitude,
                                    "status" => 6
                                );
                            } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT) {
                                $trip_complete_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT && $trip_details [0]->notificationStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT) ? 1 : 0;
                                $message = array(
                                    "message" => $msg_data ['trip_waiting_payment'],
                                    "driver_latitute" => $trip_details [0]->currentLatitude,
                                    "driver_longtitute" => $trip_details [0]->currentLongitude,
                                    "status" => 4,
                                    "display" => $trip_complete_display
                                );
                                $trip_status_changed = $this->Trip_Details_Model->update(array(
                                    'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION
                                ), array(
                                    'id' => $trip_id
                                ));
                            } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::TRIP_COMPLETED) {
                                $payment_confirmed_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::TRIP_COMPLETED && $trip_details [0]->notificationStatus == Trip_Status_Enum::NO_NOTIFICATION) ? 1 : 0;
                                $message = array(
                                    "message" => $msg_data ['trip_completed'],
                                    "driver_latitute" => $trip_details [0]->currentLatitude,
                                    "driver_longtitute" => $trip_details [0]->currentLongitude,
                                    "status" => 5,
                                    "display" => $payment_confirmed_display
                                );
                                $trip_status_changed = $this->Trip_Details_Model->update(array(
                                    'notificationStatus' => Trip_Status_Enum::TRIP_COMPLETED
                                ), array(
                                    'id' => $trip_id
                                ));
                            } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT) {
                                $trip_complete_paytm_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT && $trip_details [0]->notificationStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT) ? 1 : 0;
                                $message = array(
                                    "message" => $msg_data ['trip_waiting_paytm_payment'],
                                    "trip_fare" => $trip_details [0]->totalTripCost,
                                    "trip_id" => $trip_details [0]->tripId,
                                    "passenger_id" => $trip_details [0]->passengerId,
                                    "status" => 11,
                                    "display" => $trip_complete_paytm_display,
                                    "driver_latitute" => $trip_details [0]->currentLatitude,
                                    "driver_longtitute" => $trip_details [0]->currentLongitude
                                );
                                $trip_status_changed = $this->Trip_Details_Model->update(array(
                                    'notificationStatus' => Trip_Status_Enum::NO_NOTIFICATION
                                ), array(
                                    'id' => $trip_id
                                ));
                            } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::IN_PROGRESS && $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::ACCEPTED) {
                                $tripstart_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::IN_PROGRESS && $trip_details [0]->notificationStatus == Trip_Status_Enum::IN_PROGRESS) ? 1 : 0;
                                $message = array(
                                    "message" => $msg_data ['trip_progress'],
                                    "pickup_time" => $trip_details [0]->pickupDatetime,
                                    "trip_id" => $trip_details [0]->tripId,
                                    "driver_latitute" => $trip_details [0]->currentLatitude,
                                    "driver_longtitute" => $trip_details [0]->currentLongitude,
                                    "status" => 3,
                                    "display" => $tripstart_display
                                );
                                $trip_status_changed = $this->Trip_Details_Model->update(array(
                                    'notificationStatus' => Trip_Status_Enum::WAITING_FOR_PAYMENT
                                ), array(
                                    'id' => $trip_id
                                ));
                            } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED && $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::ACCEPTED) {
                                $arrived_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED && $trip_details [0]->notificationStatus == Trip_Status_Enum::DRIVER_ARRIVED) ? 1 : 0;
                                $message = array(
                                    "message" => $msg_data ['trip_driver_arrived'],
                                    "trip_id" => $trip_details [0]->tripId,
                                    "driver_latitute" => $trip_details [0]->currentLatitude,
                                    "driver_longtitute" => $trip_details [0]->currentLongitude,
                                    "status" => 2,
                                    "display" => $arrived_display
                                );
                                $trip_status_changed = $this->Trip_Details_Model->update(array(
                                    'notificationStatus' => Trip_Status_Enum::IN_PROGRESS
                                ), array(
                                    'id' => $trip_id
                                ));
                            } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER && $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::REJECTED || $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::TIMEOUT) {
                                $driver_cancel_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER && $trip_details [0]->notificationStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER) ? 1 : 0;
                                $message = array(
                                    "message" => $msg_data ['trip_reject_driver'],
                                    "driver_latitute" => $trip_details [0]->currentLatitude,
                                    "driver_longtitute" => $trip_details [0]->currentLongitude,
                                    "status" => 8,
                                    "display" => $driver_cancel_display
                                );
                                $trip_status_changed = $this->Trip_Details_Model->update(array(
                                    'notificationStatus' => Trip_Status_Enum::BOOKED
                                ), array(
                                    'id' => $trip_id
                                ));
                            } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED && $trip_details [0]->driverAcceptedStatus == Driver_Accepted_Status_Enum::ACCEPTED) {
                                $driver_confirm_display = ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED && $trip_details [0]->notificationStatus == Trip_Status_Enum::DRIVER_ACCEPTED) ? 1 : 0;
                                $message = array(
                                    "message" => $msg_data ['trip_confirmed'],
                                    "driver_latitute" => $trip_details [0]->currentLatitude,
                                    "driver_longtitute" => $trip_details [0]->currentLongitude,
                                    "status" => 1,
                                    "display" => $driver_confirm_display
                                );
                                $trip_status_changed = $this->Trip_Details_Model->update(array(
                                    'notificationStatus' => Trip_Status_Enum::DRIVER_ARRIVED
                                ), array(
                                    'id' => $trip_id
                                ));
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_trip'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['request_set'],
                            "status" => -5
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['mandatory_data'],
                        "status" => -5,
                        "detail" => $msg_data ['mandatory_data']
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }
    // Driver releated 14 webservice

    /**
     * used to login the driver
     */
    public function driver_login_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_exists = NULL;
        $driver_update = NULL;
        $driver_id = NULL;
        $driver_shift_insert_id = NULL;
        $email = NULL;
        $mobile = NULL;
        $password = NULL;
        $trip_details = array();
        $response_data = array();
        $trip_status = 0;
        // Posted json data
        $post_data = $this->get_post_data();

        $email = $post_data->email;
        $mobile = $post_data->phone;
        $password = $post_data->password;
        $device_type = $post_data->device_type;

        // actual functionality

        if ($email || $mobile && $password) {
            if ($mobile || $email) {
                if ($email && $password) {
                    $driver_exists = $this->Driver_Model->getOneByKeyValueArray(array(
                        'email' => $email,
                        'password' => hash("sha256", $password),
                        'status' => Status_Type_Enum::ACTIVE

                    ));
                }
                if (!$driver_exists && $mobile && $password) {
                    $driver_exists = $this->Driver_Model->getOneByKeyValueArray(array(
                        'mobile' => $mobile,
                        'password' => hash("sha256", $password),
                        'status' => Status_Type_Enum::ACTIVE
                    ));
                }

                if ($driver_exists) {
                    $auth_data = array();
                    if ($driver_exists->loginStatus == Status_Type_Enum::INACTIVE) {

                        // update the paggenger deviceId & device type by default is 'A'=Android, 'I'=Iphone, 'W'=Windows,'N'=No device
                        // @todo change the device based on device
                        $driver_update = $this->Driver_Model->update(array(
                            'deviceId' => '',
                            'lastLoggedIn' => getCurrentDateTime(),
                            'loginStatus' => Status_Type_Enum::ACTIVE,
                            'deviceType' => ($device_type) ? 'A' : 'I'
                        ), array(
                            'id' => $driver_exists->id
                        ));
                        if ($driver_update) {
                            $last_driver_shift = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                'driverId' => $driver_exists->id
                            ), 'id DESC');
                            // insert driver shift history IN data
                            if ($last_driver_shift->shiftStatus == Driver_Shift_Status_Enum::SHIFTOUT) {
                                $insert_data = array(
                                    'driverId' => $driver_exists->id,
                                    'inLatitude' => '',
                                    'inLongitude' => '',
                                    'availabilityStatus' => Driver_Available_Status_Enum::FREE,
                                    'shiftStatus' => Driver_Shift_Status_Enum::SHIFTIN
                                );
                                $driver_shift_insert_id = $this->Driver_Shift_History_Model->insert($insert_data);
                            } else {
                                $driver_shift_insert_id = $last_driver_shift->id;
                            }
                        }

                        // Send response data with Auth details & passenger details if passenger activation staus is success
                        if ($driver_shift_insert_id) {

                            $key = random_string('alnum', 16);
                            $auth_key = hash("sha256", $key);
                            $auth_key_exists = $this->Auth_Key_Model->getOneByKeyValueArray(array(
                                'userId' => $driver_exists->id,
                                'userType' => User_Type_Enum::DRIVER
                            ));
                            // If user already exists update auth key
                            if ($auth_key_exists) {
                                $this->Auth_Key_Model->update(array(
                                    'key' => $auth_key
                                ), array(
                                    'userId' => $driver_exists->id,
                                    'userType' => User_Type_Enum::DRIVER
                                ));
                            }  // create new auth key for passenger
                            else {
                                $this->Auth_Key_Model->insert(array(
                                    'key' => $auth_key,
                                    'userId' => $driver_exists->id,
                                    'userType' => User_Type_Enum::DRIVER
                                ));
                            }
                            // Authentication details
                            $auth_details = array(
                                'authKey' => $auth_key,
                                'userId' => $driver_exists->id,
                                'userType' => User_Type_Enum::DRIVER
                            );
                            // To get the existing trip which is in completed such as waiting for payment/waiting for PAYTM payment/In-progress
                            $trip_details = $this->Api_Webservice_Model->getExistingTripDetails($driver_exists->id);
                            if (count($trip_details) > 0) {

                                switch ($trip_details [0]->tripStatus) {
                                    case Trip_Status_Enum::BOOKED : {
                                        $trip_status = 0;
                                        break;
                                    }
                                    case Trip_Status_Enum::TRIP_CONFIRMED : {
                                        $trip_status = 9;
                                        break;
                                    }
                                    case Trip_Status_Enum::DRIVER_ACCEPTED : {
                                        $trip_status = 7;
                                        break;
                                    }
                                    case Trip_Status_Enum::DRIVER_ARRIVED : {
                                        $trip_status = 7;
                                        break;
                                    }
                                    case Trip_Status_Enum::READY_TO_START : {
                                        $trip_status = 3;
                                        break;
                                    }
                                    case Trip_Status_Enum::IN_PROGRESS : {
                                        $trip_status = 2;
                                        break;
                                    }
                                    case Trip_Status_Enum::WAITING_FOR_PAYMENT : {
                                        $trip_status = 5;
                                        break;
                                    }
                                    case Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT : {
                                        $trip_status = 11;
                                        break;
                                    }
                                    case Trip_Status_Enum::TRIP_COMPLETED : {
                                        $trip_status = 1;
                                        break;
                                    }
                                    case Trip_Status_Enum::CANCELLED_BY_PASSENGER : {
                                        $trip_status = 4;
                                        break;
                                    }
                                    case Trip_Status_Enum::CANCELLED_BY_DRIVER : {
                                        $trip_status = 8;
                                        break;
                                    }
                                    default : {
                                        $trip_status = 0;
                                        break;
                                    }
                                }
                                $response_data = array(
                                    'shiftupdate_id' => $driver_shift_insert_id,
                                    'taxi_id' => '',
                                    'trip_id' => $trip_details [0]->tripId,
                                    'travel_status' => $trip_status,
                                    'driver_status' => $trip_details [0]->driverAcceptedStatus,
                                    'shift_status' => Driver_Shift_Status_Enum::SHIFTIN,
                                    'profile_picture' => ($driver_exists->profileImage) ? driver_content_url('$driver_exists->profileImage') : ''
                                );
                            } else {
                                // To get the new trip details
                                $new_trip_details = $this->Driver_Request_Details_Model->getColumnByKeyValueArray('tripId, selectedDriverId', array(
                                    'selectedDriverId' => $driver_exists->id,
                                    'tripStatus' => Driver_Request_Status_Enum::AVAILABLE_TRIP
                                ));
                                if ($new_trip_details) {
                                    $trip_details = $this->Trip_Details_Model->getColumnByKeyValueArray('id,tripStatus,driverAcceptedStatus', array(
                                        'id' => $new_trip_details [0]->tripId,
                                    ));
                                    $response_data = array(
                                        'shiftupdate_id' => $driver_shift_insert_id,
                                        'taxi_id' => '',
                                        'trip_id' => $trip_details [0]->id,
                                        'travel_status' => $trip_status,
                                        'driver_status' => $trip_details [0]->driverAcceptedStatus,
                                        'shift_status' => Driver_Shift_Status_Enum::SHIFTIN,
                                        'profile_picture' => ($driver_exists->profileImage) ? driver_content_url($driver_exists->profileImage) : ''
                                    );
                                } else {
                                    $response_data = array(
                                        'shiftupdate_id' => $driver_shift_insert_id,
                                        'taxi_id' => '',
                                        'trip_id' => '',
                                        'travel_status' => '',
                                        'driver_status' => '',
                                        'shift_status' => Driver_Shift_Status_Enum::SHIFTIN,
                                        'profile_picture' => ($driver_exists->profileImage) ? driver_content_url($driver_exists->profileImage) : ''
                                    );
                                }
                            }
                            // to get the driver rejected/cancelled trip
                            $driver_rejected_count = $this->Driver_Rejected_Trip_Details_Model->getColumnByKeyValueArray('driverId', array(
                                'driverId' => $driver_exists->id,
                                'createdDate >=' => getCurrentDate(),
                                'createdDate <=' => getCurrentDate()
                            ));

                            $driver_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_exists->id);
                            $driver_today_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_exists->id, 1);

                            // To calculate time driven on current date/today
                            $total_amount = 0;
                            $total_today_amount = 0;
                            $time_result = '00:00';
                            $actual_pickup_time = '';
                            $drop_time = '';
                            $hours = '';
                            $minutes = '';
                            $seconds = '';
                            $date_difference = "";
                            $total_differnce = "";
                            foreach ($driver_earning_details as $get_details) {
                                $actual_pickup_time = strtotime($get_details->actualPickupDatetime);
                                $drop_time = strtotime($get_details->dropDatetime);
                                // echo $actual_pickup_time;
                                // echo '-';
                                // echo $drop_time;
                                // echo '<br>';
                                $date_difference = abs($drop_time - $actual_pickup_time);
                                $total_differnce += $date_difference;
                                // to get total earned amount by the driver
                                $total_amount += $get_details->driverEarning;
                            }
                            foreach ($driver_today_earning_details as $get_details) {
                                // to get total earned amount by the driver
                                $total_today_amount += $get_details->driverEarning;
                            }
                            // $date_difference = $drop_time - $actual_pickup_time;
                            $hours += floor((($total_differnce % 604800) % 86400) / 3600);
                            $minutes += floor(((($total_differnce % 604800) % 86400) % 3600) / 60);
                            $seconds += floor((((($total_differnce % 604800) % 86400) % 3600) % 60));
                            $time_result = $minutes . ':' . $seconds;
                            // To calculate time driven on current date/today

                            $statistics = array(
                                "total_trip" => count($driver_earning_details),
                                "cancel_trips" => count($driver_rejected_count),
                                "total_earnings" => round($total_amount, 2),
                                "overall_rejected_trips" => count($driver_rejected_count),
                                "today_earnings" => round($total_today_amount, 2),
                                "shift_status" => Driver_Shift_Status_Enum::SHIFTIN,
                                "time_driven" => $time_result,
                                "status" => 1
                            );
                            $response_data ['driver_statistics'] = $statistics;

                            // To get driver personal information
                            $driver_personal_details = $this->Driver_Personal_Details_Model->getOneByKeyValueArray(array(
                                'driverId' => $driver_exists->id
                            ));

                            $response_data ['company_name'] = $statistics;
                            $response_data ['language'] = $driver_exists->driverLanguagesKnown;
                            $response_data ['experience'] = $driver_exists->driverExperience;
                            $response_data ['license_number'] = $driver_personal_details->drivingLicenseNo;
                            $response_data ['salutation'] = '';
                            $response_data ['org_password'] = '';
                            $response_data ['dob'] = $driver_personal_details->dob;
                            $response_data ['name'] = $driver_exists->firstName;
                            $response_data ['userid'] = $driver_exists->id;
                            $response_data ['lastname'] = $driver_exists->lastName;
                            $response_data ['email'] = $driver_exists->email;
                            $response_data ['phone'] = $driver_exists->mobile;
                            $response_data ['address'] = $driver_personal_details->address;
                            $response_data ['password'] = '';
                            $response_data ['otp'] = $driver_exists->otp;
                            $response_data ['photo'] = ($driver_exists->profileImage) ? driver_content_url($driver_exists->profileImage) : '';
                            $response_data ['device_type'] = $driver_exists->deviceType;
                            $response_data ['device_token'] = $driver_exists->deviceId;
                            $response_data ['login_status'] = $driver_exists->loginStatus;
                            $response_data ['user_type'] = 'D';
                            $response_data ['driver_referral_code'] = $driver_exists->driverCode;
                            $response_data ['notification_setting'] = '';
                            $response_data ['company_id'] = $driver_exists->companyId;
                            $response_data ['driver_license_id'] = $driver_personal_details->drivingLicenseNo;
                            $response_data ['profile_picture'] = ($driver_exists->profileImage) ? driver_content_url($driver_exists->profileImage) : '';
                            $response_data ['bankname'] = '';
                            $response_data ['bankaccount_no'] = '';
                            $response_data ['company_ownerid'] = 2;
                            $response_data ['auth_details'] = $auth_details;
                            $message = array(
                                "message" => $msg_data ['login_success'],
                                "detail" => array(
                                    "driver_details" => $response_data
                                ),
                                "status" => 1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_status'],
                            "status" => 0
                        );
                    }
                } else {

                    /*
					 * if ($driver_exists->isVerified == Status_Type_Enum::INACTIVE) {
					 * $message = array (
					 * "message" => 'Driver documentation not verified / Verification Failed',
					 * "status" => 2
					 * );
					 * }
					 * if ($driver_exists->status == Status_Type_Enum::INACTIVE) {
					 * $message = array (
					 * "message" => 'Driver documentation not verified / Driver has been blocked by admin !',
					 * "status" => 2
					 * );
					 * } else {
					 */
                    $message = array(
                        "message" => $msg_data ['driver_account_exists'],
                        "status" => 2
                    );
                    /* } */
                }
            } else {
                $message = array(
                    "message" => $msg_data ['login_mandatory'],
                    "status" => 2
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['mandatory_data'],
                "status" => -5,
                "detail" => $msg_data ['mandatory_data']
            );
        }

        echo json_encode($message);
    }

    /**
     * get the driver statistics and details
     */
    public function driver_statistics_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $driver_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($driver_id) {
                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {

                        // to get the driver rejected/cancelled trip
                        $driver_rejected_count = $this->Driver_Rejected_Trip_Details_Model->getColumnByKeyValueArray('driverId', array(
                            'driverId' => $driver_logged_status->id,
                            'createdDate >=' => getCurrentDate(),
                            'createdDate <=' => getCurrentDate()
                        ));

                        $driver_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id);
                        $driver_today_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id, 1);

                        $total_amount = 0;
                        $total_today_amount = 0;
                        $time_result = '00:00';
                        $actual_pickup_time = '';
                        $drop_time = '';
                        $hours = '';
                        $minutes = '';
                        $seconds = '';
                        $date_difference = "";
                        $total_differnce = "";
                        // To calculate time driven overall
                        foreach ($driver_earning_details as $get_details) {
                            $actual_pickup_time = strtotime($get_details->actualPickupDatetime);
                            $drop_time = strtotime($get_details->dropDatetime);
                            // echo $actual_pickup_time;
                            // echo '-';
                            // echo $drop_time;
                            // echo '<br>';
                            $date_difference = abs($drop_time - $actual_pickup_time);
                            $total_differnce += $date_difference;
                            // to get total earned amount by the driver
                            $total_amount += $get_details->driverEarning;
                        }

                        foreach ($driver_today_earning_details as $get_details) {
                            // to get total earned amount by the driver
                            $total_today_amount += $get_details->driverEarning;
                        }

                        // $date_difference = $drop_time - $actual_pickup_time;
                        $hours += floor((($total_differnce % 604800) % 86400) / 3600);
                        $minutes += floor(((($total_differnce % 604800) % 86400) % 3600) / 60);
                        $seconds += floor((((($total_differnce % 604800) % 86400) % 3600) % 60));
                        $time_result = $minutes . ':' . $seconds;
                        // To calculate time driven on current date/today

                        $statistics = array(
                            "drivername" => $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName,
                            "total_trip" => count($driver_earning_details),
                            "cancel_trips" => count($driver_rejected_count),
                            "total_earnings" => round($total_amount, 2),
                            "overall_rejected_trips" => count($driver_rejected_count),
                            "today_earnings" => round($total_today_amount, 2),
                            "shift_status" => Driver_Shift_Status_Enum::SHIFTIN,
                            "time_driven" => $time_result,
                            "status" => 1
                        );

                        $message = array(
                            "message" => $msg_data ['success'],
                            "detail" => $statistics,
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to change the shift status
     */
    public function driver_shift_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_exists = NULL;
        $check_trip_avilablity = NULL;
        $driver_id = NULL;
        $longitude = NULL;
        $latitude = NULL;
        $shift_status = NULL;
        $driver_shift_insert_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $shift_status = $post_data->shiftstatus;
        $longitude = $post_data->longitude;
        $latitude = $post_data->latitude;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($driver_id && $shift_status) {
                    // to driver exists with activation ,login & veriication status before changing the shift status
                    $driver_exists = $this->Driver_Model->getById($driver_id);
                    if ($driver_exists) {
                        if ($driver_exists->status == Status_Type_Enum::ACTIVE) {
                            if ($driver_exists->loginStatus == Status_Type_Enum::ACTIVE) {
                                // check trip avilable for driver/any in progress trip before logout
                                $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                    'selectedDriverId' => $driver_id,
                                    'tripStatus NOT' => array(Driver_Request_Status_Enum::DRIVER_REJECTED, Driver_Request_Status_Enum::PASSENGER_CANCELLED, Driver_Request_Status_Enum::COMPLETED_TRIP)
                                ));
                                if (!$check_trip_avilablity) {
                                    if ($shift_status == Driver_Shift_Status_Enum::SHIFTIN) {
                                        // to get last shift in status of driver before updating the shift out status
                                        $last_driver_shift = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                            'driverId' => $driver_id
                                        ), 'id DESC');
                                        // insert driver shift history IN data
                                        if ($last_driver_shift->shiftStatus == Driver_Shift_Status_Enum::SHIFTOUT) {
                                            $insert_data = array(
                                                'inLatitude' => $latitude,
                                                'inLongitude' => $longitude,
                                                'driverId' => $driver_id,
                                                'availabilityStatus' => Driver_Available_Status_Enum::FREE,
                                                'shiftStatus' => Driver_Shift_Status_Enum::SHIFTIN
                                            );
                                            $driver_shift_insert_id = $this->Driver_Shift_History_Model->insert($insert_data);
                                            if ($driver_shift_insert_id) {
                                                $message = array(
                                                    "message" => $msg_data ['driver_shift_in'],
                                                    "status" => 1
                                                );
                                            } else {
                                                $message = array(
                                                    "message" => $msg_data ['failed'],
                                                    "status" => -2
                                                );
                                            }
                                        } else {
                                            $message = array(
                                                "message" => $msg_data ['driver_shift_already_in'],
                                                "status" => 1
                                            );
                                        }
                                    } else if ($shift_status == Driver_Shift_Status_Enum::SHIFTOUT) {
                                        // to get last shift in status of driver before updating the shift out status
                                        $last_driver_shift = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                            'driverId' => $driver_id
                                        ), 'id DESC');
                                        // insert driver shift history IN data
                                        if ($last_driver_shift->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN) {
                                            $insert_data = array(
                                                'driverId' => $driver_id,
                                                'outLatitude' => $latitude,
                                                'outLongitude' => $longitude,
                                                'availabilityStatus' => Driver_Available_Status_Enum::FREE,
                                                'shiftStatus' => Driver_Shift_Status_Enum::SHIFTOUT
                                            );
                                            /*$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( $update_data, array (
													'id' => $last_driver_shift->id,
													'driverId' => $driver_id
											) );*/
                                            $driver_shift_insert_id = $this->Driver_Shift_History_Model->insert($insert_data);

                                            if ($driver_shift_insert_id) {
                                                $message = array(
                                                    "message" => $msg_data ['driver_shift_out'],
                                                    "status" => 2
                                                );
                                            } else {
                                                $message = array(
                                                    "message" => $msg_data ['failed'],
                                                    "status" => -2
                                                );
                                            }
                                        } else {
                                            $message = array(
                                                "message" => $msg_data ['driver_shift_already_out'],
                                                "status" => 2
                                            );
                                        }
                                    }
                                } else {
                                    if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) {
                                        $message = array(
                                            "message" => $msg_data ['trip_assigned'],
                                            "status" => -4
                                        );
                                    } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED) {
                                        $message = array(
                                            "message" => $msg_data ['trip_progress'],
                                            "status" => -1
                                        );
                                    }
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['driver_login_failed'],
                                    "status" => -1
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['driver_account_exists'],
                                "status" => -7
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['mandatory_data'],
                            "status" => -3,
                            "detail" => $msg_data ['mandatory_data']
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * used to get the driver booking list
     */
    public function driver_booking_list_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $driver_pending_trip_list = NULL;
        $driver_cancelled_trip_list = NULL;
        $driver_past_trip_list = NULL;

        $driver_cancelled_trip = array();
        $driver_pending_trip = array();
        $driver_past_trip = array();

        $driver_id = NULL;
        $start_offset = NULL;
        $end_limit = NULL;
        $device_type = NULL;
        $start_date = NULL;
        $end_date = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $start_offset = $post_data->start;
        $end_limit = $post_data->limit;
        $device_type = $post_data->device_type;
        $start_date = $post_data->start_date;
        $end_date = $post_data->end_date;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if (true || $data_auth_key) {
            if (true || $data_auth_key->key == $auth_key) {
                // actual functionality

                if ($driver_id) {
                    // To get site info details
                    $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                        'id' => 1
                    ));
                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {

                        // Get Driver Rate Card Details and send it over in response
                        $responseDriverRateCardDetails = '';
                        $driverRateCardDetails = $this->Api_Webservice_Model->getDriverRateCardDetails();
                        if ($driverRateCardDetails) {
                            $responseDriverRateCardDetails = $driverRateCardDetails;
                        }

                        $driver_pending_trip_list = $this->Api_Webservice_Model->getDriverPendingTripList($driver_id, $device_type, $start_offset, $end_limit, $start_date, $end_date);
                        if ($driver_pending_trip_list) {

                            foreach ($driver_pending_trip_list as $list) {
                                $travel_status = NULL;
                                $travel_msg = NULL;
                                switch ($list->tripStatus) {
                                    case Trip_Status_Enum::BOOKED :
                                        $travel_status = 0;
                                        $travel_msg = Trip_Status_Enum::BKD;
                                        break;
                                    case Trip_Status_Enum::IN_PROGRESS :
                                        $travel_status = 2;
                                        $travel_msg = Trip_Status_Enum::IPR;
                                        break;
                                    case Trip_Status_Enum::DRIVER_ARRIVED :
                                        $travel_status = 7;
                                        $travel_msg = Trip_Status_Enum::DAD;
                                        break;
                                    case Trip_Status_Enum::READY_TO_START :
                                        $travel_status = 3;
                                        $travel_msg = Trip_Status_Enum::RTS;
                                        break;
                                    case Trip_Status_Enum::TRIP_CONFIRMED :
                                        $travel_status = 9;
                                        $travel_msg = Trip_Status_Enum::TCD;
                                        break;
                                    case Trip_Status_Enum::WAITING_FOR_PAYMENT :
                                        $travel_status = 5;
                                        $travel_msg = Trip_Status_Enum::WFP;
                                        break;
                                    case Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT :
                                        $travel_status = 11;
                                        $travel_msg = Trip_Status_Enum::WPP;
                                        break;
                                    default :
                                        $travel_status = 4;
                                        $travel_msg = Trip_Status_Enum::CANCELLED_BY_PASSENGER;
                                        break;
                                }
                                $driver_pending_trip [] = array(
                                    'passenger_name' => $list->passengerFirstName . ' ' . $list->passengerLastName,
                                    'passengers_log_id' => $list->tripId,
                                    'pickup_location' => $list->pickupLocation,
                                    'pickup_time' => $list->pickupDatetime,
                                    'pickup_latitude' => $list->pickupLatitude,
                                    'pickup_longitude' => $list->pickupLongitude,
                                    'drop_location' => $list->dropLocation,
                                    'drop_latitude' => $list->dropLatitude,
                                    'drop_longitude' => $list->dropLongitude,
                                    'waiting_hour' => 0,
                                    'travel_status' => $travel_status
                                );
                            }
                        }

                        $driver_cancelled_trip_list = $this->Api_Webservice_Model->getDriverCancelledTripList($driver_id, $device_type, $start_offset, $end_limit, $start_date, $end_date);
                        if ($driver_cancelled_trip_list) {

                            foreach ($driver_cancelled_trip_list as $list) {
                                $travel_status = NULL;
                                $travel_msg = NULL;
                                switch ($list->tripStatus) {
                                    case Trip_Status_Enum::BOOKED :
                                        $travel_status = 0;
                                        $travel_msg = Trip_Status_Enum::BKD;
                                        break;
                                    case Trip_Status_Enum::IN_PROGRESS :
                                        $travel_status = 2;
                                        $travel_msg = Trip_Status_Enum::IPR;
                                        break;
                                    case Trip_Status_Enum::DRIVER_ARRIVED :
                                        $travel_status = 7;
                                        $travel_msg = Trip_Status_Enum::DAD;
                                        break;
                                    case Trip_Status_Enum::READY_TO_START :
                                        $travel_status = 3;
                                        $travel_msg = Trip_Status_Enum::RTS;
                                        break;
                                    case Trip_Status_Enum::TRIP_CONFIRMED :
                                        $travel_status = 9;
                                        $travel_msg = Trip_Status_Enum::TCD;
                                        break;
                                    case Trip_Status_Enum::WAITING_FOR_PAYMENT :
                                        $travel_status = 5;
                                        $travel_msg = Trip_Status_Enum::WFP;
                                        break;
                                    case Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT :
                                        $travel_status = 11;
                                        $travel_msg = Trip_Status_Enum::WPP;
                                        break;
                                    case Trip_Status_Enum::CANCELLED_BY_DRIVER :
                                        $travel_status = 8;
                                        $travel_msg = Trip_Status_Enum::CBD;
                                        break;
                                    default :
                                        $travel_status = 4;
                                        $travel_msg = Trip_Status_Enum::CANCELLED_BY_PASSENGER;
                                        break;
                                }
                                $driver_cancelled_trip [] = array(
                                    'passenger_name' => $list->passengerFirstName . ' ' . $list->passengerLastName,
                                    'passengers_log_id' => $list->tripId,
                                    'pickup_location' => $list->pickupLocation,
                                    'pickup_time' => $list->pickupDatetime,
                                    'pickup_latitude' => $list->pickupLatitude,
                                    'pickup_longitude' => $list->pickupLongitude,
                                    'drop_location' => $list->dropLocation,
                                    'drop_latitude' => $list->dropLatitude,
                                    'drop_longitude' => $list->dropLongitude,
                                    'waiting_hour' => 0,
                                    'travel_status' => $travel_status
                                );
                            }
                        }

                        $driver_past_trip_list = $this->Api_Webservice_Model->getDriverPastTripList($driver_id, $device_type, $start_offset, $end_limit, $start_date, $end_date);
                        if ($driver_past_trip_list) {
                            //print_r($driver_past_trip_list);
                            //die();
                            foreach ($driver_past_trip_list as $list) {
                                $travel_status = NULL;
                                $travel_msg = NULL;
                                switch ($list->tripType) {
                                    case Trip_Type_Enum::ONEWAY_TRIP :
                                        $trip_plan = 1;
                                        $trip_plan_name = Trip_Type_Enum::O;
                                        break;
                                    case Trip_Type_Enum::RETURN_TRIP :
                                        $trip_plan = 2;
                                        $trip_plan_name = Trip_Type_Enum::R;
                                        break;
                                    case Trip_Type_Enum::OUTSTATION_TRIP :
                                        $trip_plan = 3;
                                        $trip_plan_name = Trip_Type_Enum::X;
                                        break;
                                    case Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY :
                                        $trip_plan = 4;
                                        $trip_plan_name = Trip_Type_Enum::XO;
                                        break;
                                    case Trip_Type_Enum::OUTSTATION_TRIP_RETURN :
                                        $trip_plan = 5;
                                        $trip_plan_name = Trip_Type_Enum::XR;
                                        break;
                                    default :
                                        $trip_plan = 0;
                                        $trip_plan_name = '';
                                        break;
                                }
                                switch ($list->paymentMode) {
                                    case Payment_Mode_Enum::CASH :
                                        $payment_mode = 1;
                                        $payment_mode_name = Payment_Mode_Enum::C;
                                        break;
                                    case Payment_Mode_Enum::WALLET :
                                        $payment_mode = 3;
                                        $payment_mode_name = Payment_Mode_Enum::W;
                                        break;
                                    case Payment_Mode_Enum::PAYTM :
                                        $payment_mode = 2;
                                        $payment_mode_name = Payment_Mode_Enum::P;
                                        break;
                                    default :
                                        $payment_mode = 0;
                                        $payment_mode_name = '';
                                        break;
                                }
                                $sub_trips = array();
                                $has_sub_trips = 0;
                                //Sub-trips list
                                $sub_trip_list = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId' => $list->tripId));
                                if ($sub_trip_list) {
                                    $has_sub_trips = 1;
                                    foreach ($sub_trip_list as $sub_trip_data) {
                                        $sub_trips[] = array(
                                            'sub_trip_id' => $sub_trip_data->subTripId,
                                            'pickup_location' => $sub_trip_data->pickupLocation,
                                            'pickup_time' => $sub_trip_data->pickupDatetime,
                                            'pickup_latitude' => $sub_trip_data->pickupLatitude,
                                            'pickup_longitude' => $sub_trip_data->pickupLongitude,
                                            'drop_location' => $sub_trip_data->dropLocation,
                                            'drop_latitude' => $sub_trip_data->dropLatitude,
                                            'drop_longitude' => $sub_trip_data->dropLongitude,
                                            'drop_time' => $sub_trip_data->dropDatetime,
                                            'meter_start' => $sub_trip_data->meterStart,
                                            'meter_end' => $sub_trip_data->meterEnd,
                                            'booking_key' => $sub_trip_data->bookingKey
                                        );
                                    }
                                }
                                $driver_past_trip [] = array(
                                    'passenger_name' => $list->passengerFirstName,
                                    'passenger_lastname' => $list->passengerLastName,
                                    'passengers_log_id' => $list->tripId,
                                    'pickup_location' => $list->pickupLocation,
                                    'pickup_time' => $list->actualPickupDatetime,
                                    'wallet_payment' => $list->walletPaymentAmount,
                                    'drop_location' => $list->dropLocation,
                                    'drop_time' => $list->dropDatetime,
                                    'distance' => $list->travelDistance,
                                    'trip_duration' => $list->tripDuration,
                                    'travel_status' => 1,
                                    'amt' => $list->totalTripCost,
                                    'profile_image' => ($list->passengerProfileImage) ? passenger_content_url($list->passengerId . '/' . $list->passengerProfileImage) : image_url('app/user.png'),
                                    'payment_type' => $payment_mode_name,
                                    'parking_charge' => $list->parkingCharge,
                                    'other_charge' => $list->tollCharge,
                                    'driver_name' => $list->driverFirstName . ' ' . $list->driverLastName,
                                    'payment_mode' => $payment_mode,
                                    'trip_type' => $trip_plan,
                                    'faretype' => $payment_mode,
                                    'trip_plan' => $trip_plan_name,
                                    'convenience_charge' => $list->convenienceCharge,
                                    'overtime_charge' => 0,
                                    'tax' => $list->companyTax,
                                    'passenger_discount' => $list->promoDiscountAmount,
                                    'tripfare' => $list->travelCharge,
                                    'tax_percentage' => $site_setting_info->tax,
                                    'drop_latitude' => $list->dropLatitude,
                                    'drop_longitude' => $list->dropLongitude,
                                    'map_url' => $list->mapUrl,
                                    'waiting_hour' => 0,
                                    'has_sub_trips' => $has_sub_trips,
                                    'sub_trips' => $sub_trips
                                );
                            }
                        }
                        $driver_rejected_count = $this->Driver_Rejected_Trip_Details_Model->getColumnByKeyValueArray('driverId', array(
                            'driverId' => $driver_logged_status->id,
                            'createdDate >=' => getCurrentDate(),
                            'createdDate <=' => getCurrentDate()
                        ));

                        $driver_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id, 0, $start_date, $end_date);
                        $driver_today_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id, 1);
                        // To calculate time driven on current date/today
                        $total_amount = 0;
                        $total_trip_amount = 0;
                        $total_today_amount = 0;
                        $time_result = '00:00';
                        $actual_pickup_time = '';
                        $drop_time = '';
                        $hours = '';
                        $minutes = '';
                        $seconds = '';
                        $date_difference = "";
                        $total_differnce = "";
                        foreach ($driver_earning_details as $get_details) {
                            $actual_pickup_time = strtotime($get_details->actualPickupDatetime);
                            $drop_time = strtotime($get_details->dropDatetime);
                            // echo $actual_pickup_time;
                            // echo '-';
                            // echo $drop_time;
                            // echo '<br>';
                            $date_difference = abs($drop_time - $actual_pickup_time);
                            $total_differnce += $date_difference;
                            // to get total earned amount by the driver
                            $total_amount += $get_details->driverEarning;
                            $total_trip_amount += $get_details->totalTripCost;
                        }
                        foreach ($driver_today_earning_details as $get_details) {
                            // to get total earned amount by the driver
                            $total_today_amount += $get_details->driverEarning;
                        }
                        // $date_difference = $drop_time - $actual_pickup_time;
                        $hours += floor((($total_differnce % 604800) % 86400) / 3600);
                        $minutes += floor(((($total_differnce % 604800) % 86400) % 3600) / 60);
                        $seconds += floor((((($total_differnce % 604800) % 86400) % 3600) % 60));
                        $time_result = $hours . ':' . $minutes;
                        // To calculate time driven on current date/today

                        $statistics = array(
                            "drivername" => $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName,
                            "total_trips" => count($driver_earning_details),
                            "cancel_trips" => count($driver_rejected_count),
                            "total_amount" => round($total_trip_amount, 2),
                            "overall_rejected_trips" => count($driver_rejected_count),
                            "today_earnings" => round($total_today_amount, 2),
                            "shift_status" => Driver_Shift_Status_Enum::SHIFTIN,
                            "total_hours" => $time_result,
                            "status" => 1
                        );

                        $response_data ['pending_booking'] = $driver_pending_trip;
                        $response_data ['past_booking'] = $driver_past_trip;
                        $response_data['cancelled_booking'] = $driver_cancelled_trip;
                        $response_data['count_details'] = $statistics;
                        $response_data['driver_rate_card_details'] = $responseDriverRateCardDetails;
                        if (count($response_data) > 0) {
                            // $message = $passengers_current;
                            $message = array(
                                "message" => $msg_data ['success'],
                                "detail" => $response_data,
                                "status" => 1
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['data_not_found'],
                                "status" => 0
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * driver log out
     * Update By Aditya on 17 Apr 2017
     * Update was to solve the Driver Logout problem, earlier driver was not able to properly logout from the system using his mobile app, this error has been resolved
     */
    public function user_logout_post()
    {
        //echo getcwd();
        //$myfile = fopen("custom.log", "w") or die("Unable to open file!");
        //$txt = "John Doe\n";
        //fwrite($myfile, $txt);

        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = false;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $update_driver_logout = NULL;
        $check_trip_avilablity = NULL;
        $driver_id = NULL;
        $longitude = NULL;
        $latitude = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        //getallheaders() returns header parameters- all custome parameters are passed with small letters only
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;

            } else if ($name == 'userId') {
                $auth_user_id = $value;
            } else if ($name == 'userType') {
                $auth_user_type = $value;
            }
            //fwrite($myfile, $name.' => '.$value."\n");
        }

        $driver_id = $post_data->driver_id;
        $longitude = $post_data->longitude;
        $latitude = $post_data->latitude;
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        //fwrite($myfile, 'key => '.$auth_key."\n");
        //fwrite($myfile, 'userId => '.$auth_user_id."\n");
        //fwrite($myfile, 'userType => '.$auth_user_type."\n");

        if (!empty($data_auth_key)) {
            //fwrite($myfile, 'Data_Auth_Key => '.$data_auth_key->key."\n");

            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($driver_id) {
                    // check trip avilable for driver/any in progress trip before logout
                    $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                        'selectedDriverId' => $driver_id,
                        'tripStatus NOT' => array(Driver_Request_Status_Enum::DRIVER_REJECTED, Driver_Request_Status_Enum::PASSENGER_CANCELLED, Driver_Request_Status_Enum::COMPLETED_TRIP)
                    ));

                    if (!$check_trip_avilablity) {
                        // update the driver logout data
                        $update_data = array(
                            'signupFrom' => Signup_Type_Enum::NONE,
                            'loginStatus' => Status_Type_Enum::INACTIVE,
                            'deviceId' => '',
                            'deviceType' => Device_Type_Enum::NO_DEVICE
                        );
                        $update_driver_logout = $this->Driver_Model->update($update_data, array(
                            'id' => $driver_id
                        ));

                        if ($update_driver_logout) {
                            // to get last shift in status of driver before updating the shift out status
                            $last_driver_shift = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                'driverId' => $driver_id
                            ), 'id DESC');
                            // insert driver shift history IN data
                            // debug_exit($last_driver_shift_id);
                            if ($last_driver_shift->shiftStatus == Driver_Shift_Status_Enum::SHIFTIN) {
                                $insert_data = array(
                                    'driverId' => $driver_id,
                                    'outLatitude' => $latitude,
                                    'outLongitude' => $longitude,
                                    'availabilityStatus' => Driver_Available_Status_Enum::FREE,
                                    'shiftStatus' => Driver_Shift_Status_Enum::SHIFTOUT
                                );
                                /*$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( $update_data, array (
										'id' => $last_driver_shift_id->id,
										'driverId' => $driver_id
								) );*/
                                $driver_shift_insert_id = $this->Driver_Shift_History_Model->insert($insert_data);
                            }
                        }
                        // update the passenger logout auth key=''
                        $update_auth_key = $this->Auth_Key_Model->update(array(
                            'key' => ''
                        ), array(
                            'userId' => $auth_user_id,
                            'userType' => $auth_user_type
                        ));
                        // @todo Delete rejected trip once passenger is logged out from logout date time
                        if ($update_auth_key) {

                            $message = array(
                                "message" => $msg_data ['logout_success'],
                                "status" => 1
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['logout_failed'],
                                "status" => -1
                            );
                        }
                    } else {
                        if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) {
                            $message = array(
                                "message" => $msg_data ['trip_assigned'],
                                "status" => -4
                            );
                        } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED) {
                            $message = array(
                                "message" => $msg_data ['trip_progress'],
                                "status" => 0
                            );
                        }
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        //fclose($myfile);
        echo json_encode($message);
    }

    //This is the original code, kept it for backup, above function is the updated code to do the same thing
    /*public function user_logout_post() {
		// Intialized
		// get API messages from message.php
		$msg_data = $this->config->item ( 'api' );
		$post_data = NULL;
		$data = array ();
		$data_auth_key = FALSE;
		$auth_key = NULL;
		$auth_user_id = NULL;
		$auth_user_type = NULL;
		$message = '';
		$update_driver_logout = NULL;
		$check_trip_avilablity = NULL;
		$driver_id = NULL;
		$longitude = NULL;
		$latitude = NULL;
		$response_data = array ();

		// Posted json data
		$post_data = $this->get_post_data ();

		$driver_id = $post_data->driver_id;
		$longitude = $post_data->longitude;
		$latitude = $post_data->latitude;
		// Checked wheather authentication key availablilty for specfic user(passenger/driver)
		// actual functionality
		if ($driver_id) {
			// check trip avilable for driver/any in progress trip before logout
			$check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray ( array (
					'selectedDriverId' => $driver_id,
					'tripStatus NOT' => array(Driver_Request_Status_Enum::DRIVER_REJECTED, Driver_Request_Status_Enum::PASSENGER_CANCELLED)
			) );

			if (! $check_trip_avilablity) {
				// update the driver logout data
				$update_data = array (
						'signupFrom' => Signup_Type_Enum::NONE,
						'loginStatus' => Status_Type_Enum::INACTIVE,
						'deviceId' => '',
						'deviceType' => Device_Type_Enum::NO_DEVICE
				);
				$update_driver_logout = $this->Driver_Model->update ( $update_data, array (
						'id' => $driver_id
				) );

				if ($update_driver_logout) {
					// to get last shift in status of driver before updating the shift out status
					$last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
							'driverId' => $driver_id
					), 'id DESC' );
					// insert driver shift history IN data
					// debug_exit($last_driver_shift_id);
					$update_data = array (
							'outLatitude' => $latitude,
							'outLongitude' => $longitude,
							'availabilityStatus' => Driver_Available_Status_Enum::FREE,
							'shiftStatus' => Driver_Shift_Status_Enum::SHIFTOUT
					);
					$driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( $update_data, array (
							'id' => $last_driver_shift_id->id,
							'driverId' => $driver_id
					) );
				}
				// update the passenger logout auth key=''
				$update_auth_key = $this->Auth_Key_Model->update ( array (
						'key' => ''
				), array (
						'userId' => $auth_user_id,
						'userType' => $auth_user_type
				) );
				// @todo Delete rejected trip once passenger is logged out from logout date time
				if ($update_auth_key) {

					$message = array (
							"message" => $msg_data ['logout_success'],
							"status" => 1
					);
				} else {
					$message = array (
							"message" => $msg_data ['logout_failed'],
							"status" => - 1
					);
				}
			} else {
				if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) {
					$message = array (
							"message" => $msg_data ['trip_assigned'],
							"status" => - 4
					);
				} else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED) {
					$message = array (
							"message" => $msg_data ['trip_progress'],
							"status" => 0
					);
				}
			}
		} else {
			$message = array (
					"message" => $msg_data ['invalid_user'],
					"status" => - 1
			);
		}
		echo json_encode ( $message );
	}*/

    /***Aditya Code START***/
    /**This new API function is created so that Driver Mobile App can call and find its login status**/
    public function driver_current_status_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $driver_shift_status_details = NULL;

        $driver_id = NULL;

        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($driver_id) {
                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {
                        //This means that Driver is currently Logged In as per Backend
                        // get the driver current duty status detail
                        $driver_shift_status_details = $this->Driver_Model->currentShiftStatus($driver_id);
                        if ($driver_shift_status_details) {
                            $shiftStatus = $driver_shift_status_details[0]['shiftStatus'];
                            if ($shiftStatus == 'IN') {
                                $response_data = array(
                                    "loginStatus" => 1,
                                    "dutyStatus" => 1
                                );

                                $message = array(
                                    "message" => $msg_data ['success'],
                                    "detail" => $response_data,
                                    "status" => 1
                                );
                            } else {
                                $response_data = array(
                                    "loginStatus" => 1,
                                    "dutyStatus" => 0
                                );

                                $message = array(
                                    "message" => $msg_data ['success'],
                                    "detail" => $response_data,
                                    "status" => 1
                                );
                            }
                        } else {
                            $response_data = array(
                                "loginStatus" => 1,
                                "dutyStatus" => 0
                            );

                            $message = array(
                                "message" => $msg_data ['success'],
                                "detail" => $response_data,
                                "status" => 1
                            );
                        }


                    } else {
                        $response_data = array(
                            "loginStatus" => 0,
                            "dutyStatus" => 0
                        );

                        $message = array(
                            "message" => $msg_data ['success'],
                            "detail" => $response_data,
                            "status" => 1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    // Created by Aditya on May 09 2017
    // This function is to tell Passanger App if it already has a Trip
    public function passengerLatestBookedTrip_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';

        // input variable declarations
        $passenger_id = 0;

        // output variables
        $trip_type = NULL;

        $pickup_location = '';
        $message = '';
        $booking_datetime = '';
        $trip_id = NULL;
        $passenger_data = array();
        $insert_data = array();
        $data = array();


        // Posted json data
        $post_data = $this->get_post_data();

        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        if (array_key_exists('passenger_id', $post_data) && $post_data->passenger_id > 0) {
            $passenger_id = $post_data->passenger_id;

        }
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if (true || $data_auth_key) {
            if (true || $data_auth_key->key == $auth_key) {
                // actual functionality
                // Now get first on-going trip for this passanger
                $passenger_exists = $this->Passenger_Model->getById($passenger_id);

                $passenger_ongoing_trip_details = false;
                if ($passenger_exists) {
                    // This means that yes passanger ID that was provided is a valid value
                    $passenger_ongoing_trip_details = $this->Api_Webservice_Model->getPassengerLatestBookedTrip($passenger_id);
                }

                if ($passenger_ongoing_trip_details) {
                    $passengerTripDetails = $passenger_ongoing_trip_details[0];
                    //set('O', 'R', 'X', 'XO', 'XR')
                    $tripType = 1;
                    if ($passengerTripDetails->tripType == 'O') {
                        $tripType = 1;
                    } else if ($passengerTripDetails->tripType == 'R') {
                        $tripType = 2;
                    } else if ($passengerTripDetails->tripType == 'X') {
                        $tripType = 3;
                    } else if ($passengerTripDetails->tripType == 'XO') {
                        $tripType = 4;
                    } else if ($passengerTripDetails->tripType == 'XR') {
                        $tripType = 5;
                    }

                    $message = array(
                        "message" => $msg_data ['booking_success'],
                        "status" => -1,
                        "trip_id" => $passengerTripDetails->tripId,
                        "triptype" => $tripType,//$passengerTripDetails->tripType,
                        "booking_date_and_time" => $passengerTripDetails->createdDate,
                        "pick_up_location" => $passengerTripDetails->pickupLocation,
                        "drop_off_location" => $passengerTripDetails->dropLocation,
                        "pick_up_time" => $passengerTripDetails->pickupDatetime,
                        "car" => $passengerTripDetails->transmissionType,
                        "Paymode" => $passengerTripDetails->paymentMode,
                        "trip_status" => $passengerTripDetails->tripStatus
                    );
                } else {
                    $message = array(
                        "message" => '',
                        "status" => 10
                    );
                }

            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    // Created by Aditya on May 17 2017
    // This function is to help register Driver from Driver App
    // currently driver registration happens only from back-end
    // this api will also store driver home address details in Latitude and Longitude
    // currently driver home address details don't store latitude and logitute
    public function driverRegistrationForm_post()
    {
        // if you are updating this function then please also refer to saveDriver() function inside
        // application->controllers->Driver.php
        // this function has taken logic from the original function saveDriver()

        $data = array();

        $data = $this->get_post_data();
        // Posted json data
        //$data = $this->get_post_data ();
        //print_r($data); die();
        $data = (array)$data;
        $driver_id = $data ['id'];
        // $data['activetab']="admin";
        // $data['active_menu'] = 'admin';
        $msg_data = $this->config->item('msg');
        $msg = $msg_data ['failed'];

        // code to create a new driver and store his details in the system
        $password = random_string('alnum', 8);
        // @todo encrypted password
        $data ['password'] = hash("sha256", $password);
        // @todo generate 6 digit otp
        $data ['otp'] = str_pad(rand(0, 999999), 6, '0', STR_PAD_LEFT);
        // @todo unique passenger code
        $data ['driverCode'] = trim(substr(strtoupper(str_replace(' ', '', $data ['firstName'] . '' . $data ['lastName'])), 0, 5));
        $data ['driverCode'] .= $this->Driver_Model->getAutoIncrementValue();


        // validation check for Mobile Present and Unique
        if ($data['mobile'] == '') {
            // simply return a validation problem
            $msg = 'Please provide a valid Mobile Number in order to register yourself as driver';
            $response = array(
                'status' => 0,
                'msg' => $msg,
                'driverId' => $driver_id
            );
            echo json_encode($response);
            die();
        } else {
            if (preg_match('/^\d{10}$/', $data['mobile'])) // phone number is valid
            {
                // Valid phone number
                // Now check from Database wheather this Mobile is already present or not
                $isMobileNotAvailable = $this->Driver_Model->checkIfMobileAlreadyPresent($data['mobile']);
                // if already present
                if ($isMobileNotAvailable > 0) {
                    $msg = 'It seems that this Mobile Number is already in use, please use a different mobile number to register';
                    $response = array(
                        'status' => 0,
                        'msg' => $msg,
                        'driverId' => $driver_id
                    );
                    echo json_encode($response);
                    die();
                }
                // else continue
            } else // phone number is not valid
            {
                // simply return a validation problem
                $msg = 'Please provide a valid 10 digit Mobile Number in order to register yourself as driver';
                $response = array(
                    'status' => 0,
                    'msg' => $msg,
                    'driverId' => $driver_id
                );
                echo json_encode($response);
                die();
            }
        }

        if ($data['email'] != '') {
            // this means some value has been provided
            // since some value has been provided, need to check in the database whether it is already present or not
            // Now check from Database wheather this Mobile is already present or not
            $isEmailNotAvailable = $this->Driver_Model->checkIfEmailAlreadyPresent($data['email']);
            // if already present
            if ($isEmailNotAvailable > 0) {
                $msg = 'It seems that this Email address is already in use, please use a different Email to register';
                $response = array(
                    'status' => 0,
                    'msg' => $msg,
                    'driverId' => $driver_id
                );
                echo json_encode($response);
                die();
            }
        }


        $driver_id = $this->Driver_Model->insert($data);
        if ($driver_id > 0) {
            $data ['driverId'] = $driver_id; // get all files names from the form
            $driver_personal_details = $this->Driver_Personal_Details_Model->insert($data);

            $driver_shift_data = array('driverId' => $driver_id, 'availabilityStatus' => Driver_Available_Status_Enum::FREE, 'shiftStatus' => Driver_Shift_Status_Enum::SHIFTOUT);
            $driver_shift_history = $this->Driver_Shift_History_Model->insert($driver_shift_data);

            /**Entry to productivity performance**/
            // SMS & email service start

            if (SMS && $driver_id) {
                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                    'smsTitle' => 'account_create_sms'
                ));

                $message_data = $message_details->smsContent;
                $web_link = array();
                $web_link [0] = substr(DRIVERAPPLINK, 0, 20);
                $web_link [1] = substr(DRIVERAPPLINK, 20, 20);
                $web_link [2] = substr(DRIVERAPPLINK, 40, 20);
                $web_link [3] = substr(DRIVERAPPLINK, 60, 20);
                $web_link [4] = substr(DRIVERAPPLINK, 80, 20);
                $web_link [5] = substr(DRIVERAPPLINK, 100, 20);
                // $address[6]=substr($current_location,120,20);
                // $address[7]=substr($current_location,140,20);

                if (strlen(DRIVERAPPLINK) > 120) {
                    $web_link [5] = cut_string_using_last(',', $web_link [5], 'left', false);
                }
                $message_data = str_replace("##USERNAME##", $data ['mobile'], $message_data);
                $message_data = str_replace("##PASSWORD##", $password, $message_data);
                $message_data = str_replace("##APPLINK1##", $web_link [0], $message_data);
                $message_data = str_replace("##APPLINK2##", $web_link [1], $message_data);
                $message_data = str_replace("##APPLINK3##", $web_link [2], $message_data);
                $message_data = str_replace("##APPLINK4##", $web_link [3], $message_data);
                $message_data = str_replace("##APPLINK5##", $web_link [4], $message_data);
                $message_data = str_replace("##APPLINK6##", $web_link [5], $message_data);
                $message_data = str_replace("##APPLINK7##", '', $message_data);
                // $message_data = str_replace("##APPLINK8##",'',$message_data);
                // print_r($message);exit;
                if ($data ['mobile'] != "") {

                    $this->Api_Webservice_Model->sendSMS($data ['mobile'], $message_data);
                    // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                    $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                    // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                }
            }

            $from = SMTP_EMAIL_ID;
            $to = $data ['email'];
            $subject = 'Driver Registration';

            $data = array(
                'driver_name' => $data ['firstName'] . ' ' . $data ['lastName'],
                'mobile' => $data ['mobile'],
                'password' => $password
            );
            $message_data = $this->load->view('emailtemplate/driver_register', $data, TRUE);

            if (SMTP && $driver_id) {

                if ($to) {
                    $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                }
            } else {

                // To send HTML mail, the Content-type header must be set
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                // Additional headers
                $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                $headers .= 'To: <' . $to . '>' . "\r\n";
                if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                    mail($to, $subject, $message_data, $headers);
                }
            }


            $msg = 'Step 1 of Driver registration completed successfully';//$msg_data ['success'];

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 1,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        } else {
            $msg = 'Step 1 of Driver registration could not be completed right now, please try again later';

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 0,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        }


        echo json_encode($response);
    }

    // created by Aditya on May 24 2017
    // This Api will help save Step 2 details from Driver App Mobile Registration process
    public function driverRegistrationFormStepTwo_post()
    {
        $data = array();

        $data = $this->get_post_data();
        // Posted json data
        //$data = $this->get_post_data ();
        //print_r($data); die();
        $data = (array)$data;
        $driver_id = $data ['id'];
        // $data['activetab']="admin";
        // $data['active_menu'] = 'admin';
        $msg_data = $this->config->item('msg');
        $msg = $msg_data ['failed'];


        // License Type => driverpersonaldetails table
        // License No => driverpersonaldetails table
        // Transmission Type => driver table
        // Car Category / Driver Skills / Multi-Select option => driver table
        // Driving Experience => driver table

        if ($driver_id > 0) {
            $data ['driverId'] = $driver_id; // get all files names from the form
            $update_driver = $this->Driver_Model->update($data, array(
                'id' => $driver_id
            ));

            $update_driver_personalDetails = $this->Driver_Personal_Details_Model->update($data, array(
                'driverId' => $driver_id
            ));


            $msg = 'Step 2 of Driver registration completed successfully';//$msg_data ['success'];

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 1,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        } else {
            $msg = 'Step 2 of Driver registration could not be completed right now, please try again later';

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 0,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        }

        echo json_encode($response);
    }

    // created by Aditya on May 25 2017
    // This Api will help save Step 3 details from Driver App Mobile Registration process
    public function driverRegistrationFormStepThree_post()
    {

        $data = array();

        $data = $this->get_post_data();
        // Posted json data
        //$data = $this->get_post_data ();
        //print_r($data); die();
        $data = (array)$data;
        $driver_id = $data ['id'];
        // $data['activetab']="admin";
        // $data['active_menu'] = 'admin';
        $msg_data = $this->config->item('msg');
        $msg = $msg_data ['failed'];

        $hours_per_day_12 = $data['duty_start_time'];//"01:30 PM"
        $hours_per_day_24 = date("H:i", strtotime($hours_per_day_12));
        $data['duty_start_time'] = $hours_per_day_24;

        if ($driver_id > 0) {
            $data ['driverId'] = $driver_id; // get all files names from the form
            $update_driver = $this->Driver_Model->update($data, array(
                'id' => $driver_id
            ));


            $msg = 'Step 3 of Driver registration completed successfully';//$msg_data ['success'];

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 1,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        } else {
            $msg = 'Step 3 of Driver registration could not be completed right now, please try again later';

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 0,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        }

        echo json_encode($response);
    }

    // created by Aditya on May 18 2017
    // This Api will help save Step 2 details from Driver App Mobile Registration process
    public function driverRegistrationFormStepFour_post()
    {
        // if you are updating this function then please also refer to saveDriver() function inside
        // application->controllers->Driver.php
        // this function has taken logic from the original function saveDriver()

        try {
            $data = array();

            //$data = $this->input->post ();
            // Posted json data
            $data = $this->get_post_data();
            $data = (array)$data;

            //print_r($data); die();
            $driver_id = $data ['id'];
            // $data['activetab']="admin";
            // $data['active_menu'] = 'admin';
            $msg_data = $this->config->item('msg');
            $msg = $msg_data ['failed'];

            if ($driver_id > 0) {
                // this means some driver

                // Upload & Save Uploaded Image
                // First check what kind of image is being uploaded,
                // is it Profile or License or Pancard
                //$uploadImage = $data['uploadImage'];
                $uploadImageName = $data['uploadImageName'];

                // if ($data['imageType'] == 'Profile' )
                // {
                // 	$uploadImageName = "PRF-" . $driver_id.'.jpg';//PRF-684-0.jpg
                // }
                // else if ($data['imageType'] == 'License' )
                // {
                // 	$uploadImageName = "LICS-" . $driver_id.'.jpg';//PRF-684-0.jpg
                // }
                // else if ($data['imageType'] == 'Pancard' )
                // {
                // 	$uploadImageName = "PAN-" . $driver_id.'.jpg';//PRF-684-0.jpg
                // }

                $data ['profileImage'] = $uploadImageName;
                if ($data['imageType'] == 'Profile') {
                    $data ['profileImage'] = $uploadImageName;
                    $update_prof_imgname = $this->Driver_Model->update($data, array(
                        'id' => $driver_id
                    ));
                    $msg = 'Profile image saved successfully';
                } else if ($data['imageType'] == 'License') {
                    $data ['drivingLicenseImage'] = $uploadImageName;
                    $update_prof_imgname = $this->Driver_Personal_Details_Model->update($data, array(
                        'driverId' => $driver_id
                    ));
                    $msg = 'License image saved successfully';
                } else if ($data['imageType'] == 'addressProof') {
                    $data ['addressProofImage'] = $uploadImageName;
                    $update_prof_imgname = $this->Driver_Personal_Details_Model->update($data, array(
                        'driverId' => $driver_id
                    ));
                    $msg = 'Address Proof image saved successfully';
                }

                $folderName = $driver_id;//684
                //$uploadPath = $this->config->item ( 'driver_content_path' ) . $driver_id; //public/images/app/uploaded/driver/684

                // if (! file_exists ( $uploadPath )) {
                // 	mkdir ( $uploadPath, 0777 );
                // }

                // if ($this->uploadBitMapEncodedImageFromAndroid($uploadImage, $uploadImageName, $uploadPath) === false) {
                // 	$msg = 'Sorry but it seems that uploading of Profile Image failed';
                // 	if ($data['imageType'] == 'Profile' )
                // 	{
                // 		$msg = 'Sorry but it seems that uploading of Profile Image failed';
                // 	}
                // 	else if ($data['imageType'] == 'License' )
                // 	{
                // 		$msg = 'Sorry but it seems that uploading of License Image failed';
                // 	}
                // 	else if ($data['imageType'] == 'Pancard' )
                // 	{
                // 		$msg = 'Sorry but it seems that uploading of Pancard Image failed';
                // 	}
                //
                // 	$response = array (
                // 			'status' => -1,
                // 			'msg' => $msg,
                // 			'driverId' => $driver_id
                // 	);
                // 	echo json_encode ( $response );
                // 	die();
                // } else {
                // 	$data ['profileImage'] = $uploadImageName;
                // 	if ($data['imageType'] == 'Profile' )
                // 	{
                // 		$data ['profileImage'] = $uploadImageName;
                // 		$update_prof_imgname = $this->Driver_Model->update ( $data, array (
                // 				'id' => $driver_id
                // 		) );
                // 	}
                // 	else if ($data['imageType'] == 'License' )
                // 	{
                // 		$data ['drivingLicenseImage'] = $uploadImageName;
                // 		$update_prof_imgname = $this->Driver_Personal_Details_Model->update ( $data, array (
                // 				'driverId' => $driver_id
                // 		) );
                // 	}
                // 	else if ($data['imageType'] == 'Pancard' )
                // 	{
                // 		$data ['panCardImage'] = $uploadImageName;
                // 		$update_prof_imgname = $this->Driver_Personal_Details_Model->update ( $data, array (
                // 				'driverId' => $driver_id
                // 		) );
                // 	}
                // }


                // $this->render("passenger/add_edit_passenger", $data);

                $response = array(
                    'status' => 1,
                    'msg' => $msg,
                    'driverId' => $driver_id
                );
            } else {
                $msg = 'Your image could not be saved please try again later';//$msg_data ['error'];
                $response = array(
                    'status' => 0,
                    'msg' => $msg,
                    'driverId' => $driver_id
                );
            }

            echo json_encode($response);
        } catch (Exception $e) {
            error_log($e);
        }
    }

    // created by Aditya on May 18 2017
    // This Api will help save Step 2 details from Driver App Mobile Registration process
    public function driverRegistrationFormStepFive_post()
    {

        try {
            $data = array();

            //$data = $this->input->post ();
            // Posted json data
            $data = $this->get_post_data();
            $data = (array)$data;

            //print_r($data); die();
            $driver_id = $data ['id'];

            $homeLatitude = 0.000000;
            $homeLongitude = 0.000000;

            $msg_data = $this->config->item('msg');
            $msg = $msg_data ['failed'];

            if ($driver_id > 0) {
                // this means some driver

                $driverDetails = $this->Driver_Model->getOneByKeyValueArray(array(
                    'id' => $driver_id
                ));

                // Now get this Driver's Home Latitude + Longitude
                $driverHomeLocation = $this->Driver_Personal_Details_Model->getOneByKeyValueArray(array(
                    'driverId' => $driver_id
                ));
                //print_r($driverHomeLocation);
                //die();
                $homeLatitude = $driverHomeLocation->latitude;
                $homeLongitude = $driverHomeLocation->longitude;

                $driverTestCenter = $this->Driver_Model->getDriverTestCenter($homeLatitude, $homeLongitude);

                $data ['test_center_id'] = $driverTestCenter['id']; // get all files names from the form
                $update_driver = $this->Driver_Model->update($data, array(
                    'id' => $driver_id
                ));

                //$msg = 'Driving Test Center nearest to your home is: '.$driverTestCenter['name'].'<br/>Full Address: '.$driverTestCenter['address'];
                $msg = 'Success';
                $response = array(
                    'status' => 1,
                    'msg' => $msg,
                    'isVerified' => $driverDetails->isVerified,
                    'driverLatitude' => $homeLatitude,
                    'driverLongitude' => $homeLongitude,
                    'centerLatitude' => $driverTestCenter['lat'],
                    'centerLongitude' => $driverTestCenter['lon'],
                    'centerName' => $driverTestCenter['name'],
                    'centerAddress' => $driverTestCenter['address'],
                    'driverId' => $driver_id
                );
            } else {
                $response = array(
                    'status' => 0,
                    'msg' => 'System couldn\'t calculate your nearest test center, please contact Zuver Customer Support to find your allocated test center',
                    'driverId' => $driver_id
                );
            }

            echo json_encode($response);
        } catch (Exception $e) {
            error_log($e);
        }
    }

    //Created By Anuj Tiwari on 22 June 2017
    //This is a update query fro app for drivers for the 1st step
    public function driverRegistrationFormUpdate_post()
    {
        // if you are updating this function then please also refer to saveDriver() function inside
        // application->controllers->Driver.php
        // this function has taken logic from the original function saveDriver()

        $data = array();

        $data = $this->get_post_data();
        // Posted json data
        //$data = $this->get_post_data ();
        //print_r($data); die();
        $data = (array)$data;
        $driver_id = $data ['id'];
        // $data['activetab']="admin";
        // $data['active_menu'] = 'admin';
        $msg_data = $this->config->item('msg');
        $msg = $msg_data ['failed'];


        if ($driver_id > 0) {
            $data ['driverId'] = $driver_id; // get all files names from the form
            $update_driver = $this->Driver_Model->update($data, array(
                'id' => $driver_id
            ));

            $update_driver_personalDetails = $this->Driver_Personal_Details_Model->update($data, array(
                'driverId' => $driver_id
            ));

            $msg = 'Your Profile has been updated successfully';//$msg_data ['success'];

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 1,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        } else {
            $msg = 'Your profile could not be updated right now, please try again later';

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 0,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        }

        echo json_encode($response);

    }

    //Created By Anuj Tiwari on 22 June 2017
    //This is a update query fro app for drivers for the 1st step
    public function driverAddBankDetails_post()
    {
        $data = array();

        $data = $this->get_post_data();
        // Posted json data
        //$data = $this->get_post_data ();
        //print_r($data); die();
        $data = (array)$data;
        $driver_id = $data ['id'];
        // $data['activetab']="admin";
        // $data['active_menu'] = 'admin';
        $msg_data = $this->config->item('msg');
        $msg = $msg_data ['failed'];


        // License Type => driverpersonaldetails table
        // License No => driverpersonaldetails table
        // Transmission Type => driver table
        // Car Category / Driver Skills / Multi-Select option => driver table
        // Driving Experience => driver table

        if ($driver_id > 0) {
            $data ['driverId'] = $driver_id; // get all files names from the form
            $update_driver = $this->Driver_Model->update($data, array(
                'id' => $driver_id
            ));

            $update_driver_personalDetails = $this->Driver_Personal_Details_Model->update($data, array(
                'driverId' => $driver_id
            ));


            $msg = 'Your bank details has been updated successfully';//$msg_data ['success'];

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 1,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        } else {
            $msg = 'Bank Details could not be Updated right now, please try again later';

            // $this->render("passenger/add_edit_passenger", $data);

            $response = array(
                'status' => 0,
                'msg' => $msg,
                'driverId' => $driver_id
            );
        }


        echo json_encode($response);
    }

    /*public function driverRegistrationFormStepTwo_post() {
		// if you are updating this function then please also refer to saveDriver() function inside
		// application->controllers->Driver.php
		// this function has taken logic from the original function saveDriver()

		try {
			$data = array ();

			//$data = $this->input->post ();
			// Posted json data
			$data = $this->get_post_data ();
			$data =  (array) $data;

			//print_r($data); die();
			$driver_id = $data ['id'];
			// $data['activetab']="admin";
			// $data['active_menu'] = 'admin';
			$msg_data = $this->config->item ( 'msg' );
			$msg = $msg_data ['failed'];

			if ($driver_id > 0) {
				// this means some driver
				//$file_names = array_keys ( $_FILES );

				//$profile_img_selected = $_FILES ['profileImage'] ['name'];
				//$license_img_selected = $_FILES ['drivingLicenseImage'] ['name'];
				//$pancard_img_selected = $_FILES ['panCardImage'] ['name'];


				// Upload & Save Profile Image
				$profileImage = $data['profileImage'];
				$profileImageName = "PRF-" . $driver_id.'.jpg';//PRF-684-0.jpg
				$folderName = $driver_id;//684
				$imageUploadPath = $this->config->item ( 'driver_content_path' ) . $driver_id; //public/images/app/uploaded/driver/684

				if (! file_exists ( $imageUploadPath )) {
					mkdir ( $imageUploadPath, 0777 );
				}

				if ($this->uploadBitMapEncodedImageFromAndroid($profileImage, $profileImageName, $imageUploadPath) === false) {
					$msg = 'Sorry but it seems that uploading of Profile Image failed';
					$response = array (
							'status' => -1,
							'msg' => $msg,
							'driverId' => $driver_id
					);
					echo json_encode ( $response );
					die();
				} else {
					$data ['profileImage'] = $profileImageName;
					$update_prof_imgname = $this->Driver_Model->update ( $data, array (
							'id' => $driver_id
					) );
				}


				// Upload & Save License Image
				$licenseImage = $data['drivingLicenseImage'];
				$licenseImageName = "LICS-" . $driver_id.'.jpg';//PRF-684-0.jpg
				if ($this->uploadBitMapEncodedImageFromAndroid($licenseImage, $licenseImageName, $imageUploadPath) === false) {
					$msg = 'Sorry but it seems that uploading of Driving License Image failed';
					$response = array (
							'status' => -1,
							'msg' => $msg,
							'driverId' => $driver_id
					);
					echo json_encode ( $response );
					die();
				} else {
					$data ['profileImage'] = $profileImageName;
					$update_prof_imgname = $this->Driver_Model->update ( $data, array (
							'id' => $driver_id
					) );
				}


				// Upload & Save PanCard Image
				$pancardImage = $data['drivingLicenseImage'];
				$pancardImageName = "PAN-" . $driver_id.'.jpg';//PRF-684-0.jpg
				if ($this->uploadBitMapEncodedImageFromAndroid($pancardImage, $pancardImageName, $imageUploadPath) === false) {
					$msg = 'Sorry but it seems that uploading of Pancard Image failed';
					$response = array (
							'status' => -1,
							'msg' => $msg,
							'driverId' => $driver_id
					);
					echo json_encode ( $response );
					die();
				} else {
					$data ['profileImage'] = $profileImageName;
					$update_prof_imgname = $this->Driver_Model->update ( $data, array (
							'id' => $driver_id
					) );
				}
			}

			$msg = $msg_data ['success'];

			// $this->render("passenger/add_edit_passenger", $data);

			$response = array (
					'status' => 1,
					'msg' => $msg,
					'driverId' => $driver_id
			);
			echo json_encode ( $response );
		} catch(Exception $e) {
			error_log($e);
		}

	}*/

    public function uploadBitMapEncodedImageFromAndroid($image, $imageName, $uploadFolder)
    {
        $base = $image;//$data['profileImage'];

        $url = $uploadFolder;//"/var/www/html/public/images/app/uploaded/driver/684/";
        $image_name = $imageName;//"img_"."_".date("Y-m-d-H-m-s").".jpg";
        //$path = $url."".$image_name; // path of saved image
        $path = $uploadFolder . "/" . $imageName; // path of saved image

        // base64 encoded utf-8 string
        $binary = base64_decode($base);

        // binary, utf-8 bytes
        header("Content-Type: bitmap; charset=utf-8");

        $file = file_put_contents($path, $binary);
        return $file;
        // $file = fopen($path, "wb"); //
        // $filepath = $image_name;
        // fwrite($file, $binary);

        // fclose($file);
    }

    /***Aditya Code END***/


    /**
     * get the driver profile info
     */
    public function driver_profile_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $trip_status_update = NULL;
        $trip_request_status_update = NULL;
        $trip_tracking_update = NULL;
        $driver_id = NULL;

        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->userid;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($driver_id) {
                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {
                        // get the driver detail
                        $driver_details = $this->Driver_Model->getById($driver_id);
                        // get driver personal details
                        $driver_personal_details = $this->Driver_Personal_Details_Model->getOneByKeyValueArray(array(
                            'driverId' => $driver_id
                        ));
                        // get driver average rating rated by passenger for completed trip
                        $driver_rating = $this->Api_Webservice_Model->getDriverAverageRating($driver_id);


                        if($driver_details->driverSkill == "NL" || $driver_details->driverSkill == "NN") {
                            $carCategory = "";
                        }else {
                            $carCategory = $driver_details->driverSkill;
                        }
                        $hours_per_day_24 = $driver_details->duty_start_time;
                        $hours_per_day_12 = date("h:i A", strtotime($hours_per_day_24));

                        if (!empty($driver_details->profileImage)) {

                            if (strpos($driver_details->profileImage, 'driver_docs') !== false) {

                                $profileImageUrl = 'https://s3-ap-southeast-1.amazonaws.com/zuverlive/' . $driver_details->profileImage;

                            } else {

                                $profileImageUrl = driver_content_url($driver_details->id . '/' . $driver_details->profileImage);

                            }

                        }else {

                            $profileImageUrl = image_url('app/user.png');
                        }

                        if (!empty($driver_personal_details->addressProofImage)) {

                            if (strpos($driver_personal_details->addressProofImage, 'driver_docs') !== false) {

                                $addressImageUrl = 'https://s3-ap-southeast-1.amazonaws.com/zuverlive/' . $driver_personal_details->addressProofImage;

                            } else {

                                $addressImageUrl = driver_content_url($driver_details->id . '/' . $driver_personal_details->addressProofImage);

                            }

                            $proofNameString = $driver_personal_details->addressProofImage;

                            list($number1, $number2) = explode('/', $proofNameString);

                            list($number3, $number4) = explode('-', $number2);


                            if($number3 == 'Aadhaar') {

                                $addressType = 'Aadhaar Card';

                            }elseif ($number3 == 'Voting') {

                                $addressType = 'Voting Card';

                            }elseif ($number3 == 'Electricity') {

                                $addressType = 'Electricity Bill';

                            }elseif($number3 == 'Other') {

                                $addressType = 'Other';

                            }

                        }else {

                            $addressImageUrl = image_url('app/user.png');
                            $addressType = '';
                        }

                        $response_data = array(
                            "salutation" => '',
                            "name" => $driver_details->firstName,
                            "lastname" => $driver_details->lastName,
                            "bankName" => $driver_personal_details->bankName,
                            "bankBranch" => $driver_personal_details->bankBranch,
                            "bankAccountNo"=> $driver_personal_details->bankAccountNo,
                            "bankIfscCode" => $driver_personal_details->bankIfscCode,
                            "email" => $driver_details->email,
                            "phone" => $driver_details->mobile,
                            "main_image_path" => $profileImageUrl,
                            "thumb_image_path" => $profileImageUrl,
                            "address" => $driver_personal_details->address,
                            "addressImage" => $addressImageUrl,
                            "addressType" => $addressType,
                            "driver_license_id" => $driver_personal_details->drivingLicenseNo,
                            "address" => $driver_personal_details->address,
                            "city_id" => $driver_details->cityId,
                            "pan_card" => $driver_personal_details->panCardNo,
                            "company_name" => $driver_details->firstName,
                            "dob" => $driver_personal_details->dob,
                            "joinDate" => $driver_details->doj,
                            "age" => intval(substr(date('Ymd') - date('Ymd', strtotime($driver_personal_details->dob)), 0, -4)),
                            "language" => $driver_details->driverLanguagesKnown,
                            "experience" => $driver_details->driverExperience,
                            "licenseType" => $driver_personal_details->licenseType,
                            "licenseExDate" => $driver_personal_details->drivingLicenseExpireDate,
                            "license_number" => $driver_personal_details->drivingLicenseNo,
                            "carCategory" => $carCategory,
                            "transmissionType" => $driver_details->skills,
                            "workingDays" => $driver_details->working_days,
                            "workingHours" => $driver_details->hours_per_day,
                            "dutyStartTime" => $hours_per_day_12,
                            "password" => '',
                            "driver_rating" => round($driver_rating [0]->driverRating, 2)
                        );
                        $message = array(
                            "message" => $msg_data ['success'],
                            "detail" => $response_data,
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * accept the trip request
     */
    public function driver_reply_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $trip_status_update = NULL;
        $trip_request_status_update = NULL;
        $trip_tracking_update = NULL;
        $driver_id = NULL;
        $trip_id = NULL;
        $driver_reply = NULL;
        $reject_reason = NULL;
        $flag = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $trip_id = $post_data->pass_logid;
        $driver_reply = $post_data->driver_reply;
        $reject_reason = $post_data->field;
        $flag = $post_data->flag;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($driver_id) {
                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {
                        // get the trip details
                        $trip_details = $this->Trip_Details_Model->getById($trip_id);
                        if ($trip_details) {
                            // check trip avilable for driver/any in progress trip before logout
                            $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                'tripId' => $trip_id
                            ));
                            if ($check_trip_avilablity) {
                                if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) {

                                    if ($driver_reply == Driver_Accepted_Status_Enum::ACCEPTED) {

                                        // To update trip status on tripdetails
                                        $trip_status_update = $this->Trip_Details_Model->update(array(
                                            'tripStatus' => Trip_Status_Enum::DRIVER_ACCEPTED,
                                            'notificationStatus' => Trip_Status_Enum::DRIVER_ACCEPTED,
                                            'driverAcceptedStatus' => Driver_Accepted_Status_Enum::ACCEPTED
                                        ), array(
                                            'id' => $trip_id,
                                            'driverId' => $driver_id
                                        ));

                                        if ($trip_status_update) {
                                            // To update trip status on driverrequesttripdetails
                                            $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                                'tripStatus' => Driver_Request_Status_Enum::DRIVER_ACCEPTED
                                            ), array(
                                                'tripId' => $trip_id,
                                                'selectedDriverId' => $driver_id
                                            ));

                                            // to get last shift in status of driver before updating the shift out status
                                            $last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                                'driverId' => $driver_id
                                            ), 'id DESC');
                                            $driver_shift_insert_id = $this->Driver_Shift_History_Model->update(array(
                                                'availabilityStatus' => Driver_Available_Status_Enum::BUSY
                                            ), array(
                                                'id' => $last_driver_shift_id->id,
                                                'driverId' => $driver_id
                                            ));

                                            // @todo if needed to get the trip details with passenger & driver
                                            // $passenger_details=$this->Passenger_Model->getById($trip_details->passengerId);
                                            // $driver_details=$this->Driver_Model->getById($trip_details->driverId);
                                            $response_data ['trip_id'] = $trip_id;
                                            $response_data ['driverdetails'] = array();
                                            // @todo if needed to get the driver statistics
                                            $response_data ['driver_statistics'] = array();
                                            $message = array(

                                                "message" => $msg_data ['driver_confirmed'],

                                                "status" => 1,
                                                "detail" => $response_data
                                            );
                                        } else {
                                            $message = array(
                                                "message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
                                                "status" => -1
                                            );
                                            // log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
                                        }
                                    } else if ($driver_reply == Driver_Accepted_Status_Enum::REJECTED) {

                                        // To update trip status on tripdetails
                                        $trip_status_update = $this->Trip_Details_Model->update(array(
                                            'tripStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
                                            'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
                                            'driverId' => 0,
                                            'driverAcceptedStatus' => Driver_Accepted_Status_Enum::REJECTED
                                        ), array(
                                            'id' => $trip_id,
                                            'driverId' => $driver_id
                                        ));

                                        if ($trip_status_update) {
                                            // To update trip status on driverrequesttripdetails
                                            // to get rejected drivers for particular trip_id
                                            $rejected_drivers_list = NULL;
                                            $rejected_drivers = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                                'tripId' => $trip_id
                                            ));

                                            if ($rejected_drivers) {
                                                if ($rejected_drivers->rejectedDrivers) {
                                                    $rejected_drivers_list = $rejected_drivers->rejectedDrivers;
                                                    $rejected_drivers_list .= ',' . $driver_id;
                                                } else {
                                                    $rejected_drivers_list = $driver_id;
                                                }
                                            }

                                            $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                                'tripStatus' => Driver_Request_Status_Enum::DRIVER_REJECTED,
                                                'selectedDriverId' => 0,
                                                'rejectedDrivers' => $rejected_drivers_list
                                            ), array(
                                                'tripId' => $trip_id
                                            ));
                                        }
                                        if ($trip_request_status_update) {
                                            // Insert/update data to driverrejectedtripdetails table

                                            // To get the passenger id from trip id to update the the trip rejected by driver
                                            $trip_details = $this->Trip_Details_Model->getById($trip_id);
                                            $insert_data = array(
                                                'tripId' => $trip_id,
                                                'driverId' => $driver_id,
                                                'passengerId' => $trip_details->passengerId,
                                                'rejectionType' => Driver_Accepted_Status_Enum::REJECTED,
                                                'rejectionReason' => $reject_reason
                                            );
                                            $exist_rejected_trip = $this->Driver_Rejected_Trip_Details_Model->getOneByKeyValueArray(array(
                                                'tripId' => $trip_id,
                                                'driverId' => $driver_id
                                            ));

                                            if (!$exist_rejected_trip) {
                                                $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->insert($insert_data);
                                            } else {
                                                $update_data = array(
                                                    'rejectionType' => Driver_Accepted_Status_Enum::REJECTED,
                                                    'rejectionReason' => $reject_reason
                                                );
                                                $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->update($update_data, array(
                                                    'tripId' => $trip_id,
                                                    'driverId' => $driver_id
                                                ));
                                            }
                                            $message = array(
                                                "message" => $msg_data ['driver_rejected'],
                                                "status" => 6
                                            );
                                        } else {
                                            $message = array(
                                                "message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
                                                "status" => -1
                                            );
                                            // log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
                                        }
                                    } else if ($driver_reply == Driver_Accepted_Status_Enum::TIMEOUT) {

                                        // To update trip status on tripdetails
                                        $trip_status_update = $this->Trip_Details_Model->update(array(
                                            'tripStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
                                            'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
                                            'driverId' => 0,
                                            'driverAcceptedStatus' => Driver_Accepted_Status_Enum::TIMEOUT
                                        ), array(
                                            'id' => $trip_id,
                                            'driverId' => $driver_id
                                        ));

                                        if ($trip_status_update) {
                                            // To update trip status on driverrequesttripdetails
                                            // to get rejected drivers for particular trip_id
                                            $rejected_drivers_list = NULL;
                                            $rejected_drivers = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                                'tripId' => $trip_id
                                            ));

                                            if ($rejected_drivers) {
                                                if ($rejected_drivers->rejectedDrivers) {
                                                    $rejected_drivers_list = $rejected_drivers->rejectedDrivers;
                                                    $rejected_drivers_list .= ',' . $driver_id;
                                                } else {
                                                    $rejected_drivers_list = $driver_id;
                                                }
                                            }

                                            $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                                'tripStatus' => Driver_Request_Status_Enum::DRIVER_REJECTED,
                                                'selectedDriverId' => 0,
                                                'rejectedDrivers' => $rejected_drivers_list
                                            ), array(
                                                'tripId' => $trip_id
                                            ));
                                        }
                                        if ($trip_request_status_update) {
                                            // Insert/update data to driverrejectedtripdetails table

                                            // To get the passenger id from trip id to update the the trip rejected by driver
                                            $trip_details = $this->Trip_Details_Model->getById($trip_id);
                                            $insert_data = array(
                                                'tripId' => $trip_id,
                                                'driverId' => $driver_id,
                                                'passengerId' => $trip_details->passengerId,
                                                'rejectionType' => Driver_Accepted_Status_Enum::TIMEOUT,
                                                'rejectionReason' => $reject_reason
                                            );
                                            $exist_rejected_trip = $this->Driver_Rejected_Trip_Details_Model->getOneByKeyValueArray(array(
                                                'tripId' => $trip_id,
                                                'driverId' => $driver_id
                                            ));

                                            if (!$exist_rejected_trip) {
                                                $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->insert($insert_data);
                                            } else {
                                                $update_data = array(
                                                    'rejectionType' => Driver_Accepted_Status_Enum::TIMEOUT,
                                                    'rejectionReason' => $reject_reason
                                                );
                                                $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->update($update_data, array(
                                                    'tripId' => $trip_id,
                                                    'driverId' => $driver_id
                                                ));
                                            }
                                            $message = array(
                                                "message" => $msg_data ['driver_rejected'],
                                                "status" => 6
                                            );
                                        } else {
                                            $message = array(
                                                "message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
                                                "status" => -1
                                            );
                                            // log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
                                        }
                                    }
                                } else {
                                    if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                                        $message = array(
                                            "message" => $msg_data ['trip_reject_passenger'],
                                            "status" => -1
                                        );
                                    } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                                        $message = array(
                                            "message" => $msg_data ['trip_completed'],
                                            "status" => -1
                                        );
                                    }
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['invalid_trip_request'],
                                    "status" => -1
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_trip'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * decline the trip request
     */
    public function reject_trip_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $driver_id = NULL;
        $reject_type = NULL;
        $trip_id = NULL;
        $reject_reason = NULL;
        $company_id = NULL;
        $reject_trip_id = NULL;
        $trip_status_update = NULL;
        $trip_request_status_update = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $reject_type = $post_data->reject_type;
        $trip_id = $post_data->trip_id;
        $reject_reason = $post_data->reason;
        $company_id = $post_data->company_id;
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($trip_id && $driver_id) {

                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {
                        // To update trip status on tripdetails
                        $trip_status_update = $this->Trip_Details_Model->update(array(
                            'tripStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
                            'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
                            'driverId' => 0,
                            'driverAssignedDatetime' => 0,
                            'driverAcceptedStatus' => Driver_Accepted_Status_Enum::REJECTED
                        ), array(
                            'id' => $trip_id,
                            'driverId' => $driver_id
                        ));

                        if ($trip_status_update) {
                            // To update trip status on driverrequesttripdetails
                            // to get rejected drivers for particular trip_id
                            $rejected_drivers_list = NULL;
                            $rejected_drivers = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                'tripId' => $trip_id
                            ));

                            if ($rejected_drivers) {
                                if ($rejected_drivers->rejectedDrivers) {
                                    $rejected_drivers_list = $rejected_drivers->rejectedDrivers;
                                    $rejected_drivers_list .= ',' . $driver_id;
                                } else {
                                    $rejected_drivers_list = $driver_id;
                                }
                            }

                            $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                'tripStatus' => Driver_Request_Status_Enum::DRIVER_REJECTED,
                                'selectedDriverId' => 0,
                                'rejectedDrivers' => $rejected_drivers_list
                            ), array(
                                'tripId' => $trip_id
                            ));
                        }
                        if ($trip_request_status_update) {
                            // to get last shift in status of driver before updating the shift out status
                            $last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                'driverId' => $driver_id
                            ), 'id DESC');
                            $driver_shift_insert_id = $this->Driver_Shift_History_Model->update(array(
                                'availabilityStatus' => Driver_Available_Status_Enum::FREE
                            ), array(
                                'id' => $last_driver_shift_id->id,
                                'driverId' => $driver_id
                            ));

                            // Insert/update data to driverrejectedtripdetails table

                            // To get the passenger id from trip id to update the the trip rejected by driver
                            $trip_details = $this->Trip_Details_Model->getById($trip_id);
                            $insert_data = array(
                                'tripId' => $trip_id,
                                'driverId' => $driver_id,
                                'passengerId' => $trip_details->passengerId,
                                'rejectionType' => ($reject_type == 1) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT,
                                'rejectionReason' => $reject_reason
                            );
                            $exist_rejected_trip = $this->Driver_Rejected_Trip_Details_Model->getOneByKeyValueArray(array(
                                'tripId' => $trip_id,
                                'driverId' => $driver_id
                            ));

                            if (!$exist_rejected_trip) {
                                $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->insert($insert_data);
                            } else {
                                $update_data = array(
                                    'rejectionType' => ($reject_type == 1) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT,
                                    'rejectionReason' => $reject_reason
                                );
                                $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->update($update_data, array(
                                    'tripId' => $trip_id,
                                    'driverId' => $driver_id
                                ));
                            }
                            $message = array(
                                "message" => $msg_data ['driver_rejected'],
                                "status" => 6
                            );
                        } else {
                            $message = array(
                                "message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
                                "status" => -1
                            );
                            // log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip'],
                        "status" => '-1'
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * decline request with reason
     */
    public function reject_trip_reason_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $driver_id = NULL;
        $reject_type = NULL;
        $trip_id = NULL;
        $reject_reason = NULL;
        $reject_trip_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $reject_type = $post_data->reject_type;
        $trip_id = $post_data->trip_id;
        $reject_reason = $post_data->reason;
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($trip_id && $driver_id) {

                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {

                        // to get the driver rejected/cancelled trip
                        $driver_rejected_count = $this->Driver_Rejected_Trip_Details_Model->getColumnByKeyValueArray('driverId', array(
                            'driverId' => $driver_logged_status->id,
                            'createdDate >=' => getCurrentDate(),
                            'createdDate <=' => getCurrentDate()
                        ));

                        $driver_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id);
                        $driver_today_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id, 1);

                        // To calculate time driven on current date/today
                        $total_amount = 0;
                        $total_today_amount = 0;
                        $time_result = '00:00';
                        $actual_pickup_time = '';
                        $drop_time = '';
                        $hours = '';
                        $minutes = '';
                        $seconds = '';
                        $date_difference = "";
                        $total_differnce = "";
                        foreach ($driver_earning_details as $get_details) {
                            $actual_pickup_time = strtotime($get_details->actualPickupDatetime);
                            $drop_time = strtotime($get_details->dropDatetime);
                            // echo $actual_pickup_time;
                            // echo '-';
                            // echo $drop_time;
                            // echo '<br>';
                            $date_difference = abs($drop_time - $actual_pickup_time);
                            $total_differnce += $date_difference;
                            // to get total earned amount by the driver
                            $total_amount += $get_details->driverEarning;
                        }
                        foreach ($driver_today_earning_details as $get_details) {
                            // to get total earned amount by the driver
                            $total_today_amount += $get_details->driverEarning;
                        }
                        // $date_difference = $drop_time - $actual_pickup_time;
                        $hours += floor((($total_differnce % 604800) % 86400) / 3600);
                        $minutes += floor(((($total_differnce % 604800) % 86400) % 3600) / 60);
                        $seconds += floor((((($total_differnce % 604800) % 86400) % 3600) % 60));
                        $time_result = $minutes . ':' . $seconds;
                        // To calculate time driven on current date/today

                        $statistics = array(
                            "drivername" => $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName,
                            "total_trip" => count($driver_earning_details),
                            "cancel_trips" => count($driver_rejected_count),
                            "total_earnings" => round($total_amount, 2),
                            "overall_rejected_trips" => count($driver_rejected_count),
                            "today_earnings" => round($total_today_amount, 2),
                            "shift_status" => Driver_Shift_Status_Enum::SHIFTIN,
                            "time_driven" => $time_result,
                            "status" => 1
                        );
                        // To get the passenger id from trip id to update the the trip rejected by driver
                        $trip_details = $this->Trip_Details_Model->getById($trip_id);
                        $insert_data = array(
                            'tripId' => $trip_id,
                            'driverId' => $driver_id,
                            'passengerId' => $trip_details->passengerId,
                            'rejectionType' => ($reject_type == 1) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT,
                            'rejectionReason' => $reject_reason
                        );
                        $exist_rejected_trip = $this->Driver_Rejected_Trip_Details_Model->getOneByKeyValueArray(array(
                            'tripId' => $trip_id,
                            'driverId' => $driver_id
                        ));
                        if (!$exist_rejected_trip) {
                            $statistics['overall_rejected_trips'] += 1;
                            $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->insert($insert_data);
                            if ($reject_trip_id) {

                                $message = array(
                                    "message" => $msg_data ['driver_rejected_reason_success'],
                                    "driver_statistics" => $statistics,
                                    "status" => 1
                                );
                            } else {
                                $message = array(
                                    "message" => $msg_data ['driver_rejected_reason_failed'],

                                    "status" => -1
                                );
                            }
                        } else {
                            $update_data = array(
                                'rejectionType' => ($reject_type == 1) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT,
                                'rejectionReason' => $reject_reason
                            );
                            $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->update($update_data, array(
                                'tripId' => $trip_id,
                                'driverId' => $driver_id
                            ));
                            $message = array(
                                "message" => $msg_data ['driver_rejected_reason_success'],
                                "driver_statistics" => $statistics,
                                "status" => 1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip'],
                        "status" => '-1'
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * driver arrived api
     */
    public function driver_arrived_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $trip_details = NULL;
        $check_trip_avilablity = NULL;
        $trip_id = NULL;
        $db_trip_update = 0;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $trip_id = $post_data->trip_id;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($trip_id) {
                    // To get trip details i.e., trip status,driver accepted status
                    $trip_details = $this->Trip_Details_Model->getById($trip_id);
                    if ($trip_details) {

                        // check trip avilable for driver/any in progress trip before logout
                        $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                            'tripId' => $trip_id
                        ));

                        if ($check_trip_avilablity) {
                            if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP ||
                                $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER ||
                                $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED
                            ) {

                                if ($trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT) {
                                    $message = array(
                                        "message" => $msg_data ['trip_progress'],
                                        "status" => -1
                                    );
                                } else {

                                    $driver_details = $this->Driver_Model->getById($trip_details->driverId);

                                    //    Zoomcar api call for driver accpted trip

                                    if ($trip_details->companyId == ZOOMCAR_COMPANYID) {
                                        $zoom_checklist_response = $this->Api_Webservice_Model->zoomCarGetChecklist($trip_id, $trip_details->bookingKey);
                                        if ($zoom_checklist_response['status'] == 'success') {
                                            $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                'smsTitle' => 'zoomcar_driver_arrived_sms'
                                            ));
                                            $message_data = $message_details->smsContent;
                                            $message_data = str_replace("##TRIP_ID##", $trip_id, $message_data);
                                            $message_data = str_replace("##ZOOMCAR_BOOKING_ID##", $trip_details->bookingKey, $message_data);
                                            $message_data = str_replace("##PICKUP_URL##", $zoom_checklist_response['checklist_1'], $message_data);
                                            $message_data = str_replace("##DROP_URL##", $zoom_checklist_response['checklist_2'], $message_data);
                                            $message_data = str_replace("##OTP_CODE##", $zoom_checklist_response['otp'], $message_data);
                                            $this->Api_Webservice_Model->sendSMS($driver_details->mobile, $message_data);
                                            $db_trip_update = 1;
                                        } else {
                                            $message = array(
                                                "message" => 'Zoomcar API failed. Failure Message: ' . $zoom_checklist_response['message'],
                                                "status" => -1
                                            );
                                        }

                                    } else {
                                        $db_trip_update = 1;
                                    }

                                    //  End zoomcar api called end

                                    if ($db_trip_update) {
                                        $update_trip_status = $this->Trip_Details_Model->update(array(
                                            'tripStatus' => Trip_Status_Enum::DRIVER_ARRIVED,
                                            'notificationStatus' => Trip_Status_Enum::DRIVER_ARRIVED,
                                            'driverAcceptedStatus' => Driver_Accepted_Status_Enum::ACCEPTED,
                                            'driverArrivedDatetime' => getCurrentDateTime()
                                        ), array(
                                            'id' => $trip_id
                                        ));

                                        if ($update_trip_status) {
                                            $update_request_status = $this->Driver_Request_Details_Model->update(array(
                                                'tripStatus' => Driver_Request_Status_Enum::DRIVER_ACCEPTED
                                            ), array(
                                                'tripId' => $trip_id,
                                                'selectedDriverId' => $trip_details->driverId
                                            ));
                                            if ($update_request_status) {
                                                // to get last shift in status of driver before updating the shift out status
                                                /* $last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
                                                        'driverId' => $trip_details->driverId
                                                ), 'id DESC' ); */
                                                // insert driver shift history IN data
                                                /* $update_data = array (
                                                        'outLatitude' => '',
                                                        'outLongitude' => '',
                                                        'availabilityStatus' => Driver_Available_Status_Enum::NOTAVAILABLE,
                                                        'shiftStatus' => Driver_Shift_Status_Enum::SHIFTOUT
                                                );
     */                                            /* $driver_shift_insert_id = $this->Driver_Shift_History_Model->update ( array (
													'availabilityStatus' => Driver_Available_Status_Enum::BUSY
											), array (
													'id' => $last_driver_shift_id->id,
													'driverId' => $trip_details->driverId
											) ); */

                                                if ($update_request_status) {

                                                    // get passenger mobile number
                                                    $passenger_details = $this->Passenger_Model->getById($trip_details->passengerId);
                                                    // get driver full name & mobile number
                                                    $driver_details = $this->Driver_Model->getById($trip_details->driverId);

                                                    if (SMS && $update_request_status) {
                                                        $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                            'smsTitle' => 'driver_arrived'
                                                        ));

                                                        $message_data = $message_details->smsContent;
                                                        $message_data = str_replace("##DRIVERNAME##", $driver_details->firstName . ' ' . $driver_details->lastName, $message_data);
                                                        $message_data = str_replace("##DRIVERMOBILE##", $driver_details->mobile, $message_data);

                                                        // print_r($message);exit;
                                                        if ($passenger_details) {
                                                            if ($passenger_details->mobile) {

                                                                $this->Api_Webservice_Model->sendSMS($passenger_details->mobile, $message_data);
                                                            }
                                                        }
                                                    }

                                                    /*
                                                     * $from = SMTP_EMAIL_ID;
                                                     * $to = $passenger_details->email;
                                                     * $subject = 'Get Ready to ride with Zuver!';
                                                     * $data = array (
                                                     * 'user_name' => $user_exists->firstName . ' ' . $user_exists->lastName,
                                                     * 'mobile'=>$user_exists->mobile,
                                                     * 'email'=>$user_exists->email
                                                     * );
                                                     * $message_data = $this->load->view ( 'emailtemplate/passenger_register', $data, TRUE );
                                                     *
                                                     * if (SMTP && $check_otp) {
                                                     *
                                                     * if ($to) {
                                                     * $this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
                                                     * }
                                                     * } else {
                                                     *
                                                     * // To send HTML mail, the Content-type header must be set
                                                     * $headers = 'MIME-Version: 1.0' . "\r\n";
                                                     * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                                     * // Additional headers
                                                     * $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                                                     * $headers .= 'To: <' . $to . '>' . "\r\n";
                                                     * if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                                                     * mail ( $to, $subject, $message_data, $headers );
                                                     * }
                                                     * }
                                                     */

                                                    $message = array(
                                                        "message" => $msg_data ['driver_arrival'],
                                                        "status" => 1
                                                    );
                                                } else {
                                                    $message = array(
                                                        "message" => $msg_data ['failed'],
                                                        "status" => -1
                                                    );
                                                    log_message('debug', $msg_data ['trip_status_failed'] . '' . $trip_details->driverId);
                                                }
                                            } else {
                                                $message = array(
                                                    "message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
                                                    "status" => -1
                                                );
                                                log_message('debug', $msg_data ['trip_status_failed'] . '' . $trip_id . ' driver_id=' . $trip_details->driverId);
                                            }
                                        } else {
                                            $message = array(
                                                "message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
                                                "status" => -1
                                            );
                                            log_message('debug', $msg_data ['trip_status_failed'] . '' . $trip_id);
                                        }
                                    }
                                }
                            } else {

                                if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                                    $message = array(
                                        "message" => $msg_data ['trip_reject_passenger'],
                                        "status" => -1
                                    );
                                } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                                    $message = array(
                                        "message" => $msg_data ['trip_completed'],
                                        "status" => -1
                                    );
                                }
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_trip_request'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_trip'],
                            "status" => 2
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * driver started the trip api
     */
    public function driver_start_trip_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $trip_status_update = NULL;
        $trip_request_status_update = NULL;
        $trip_tracking_update = NULL;
        $driver_id = NULL;
        $trip_id = NULL;
        $latitude = NULL;
        $longitude = NULL;
        $status = NULL;
        $db_trip_update = 0;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $trip_id = $post_data->trip_id;
        $latitude = $post_data->latitude;
        $longitude = $post_data->longitude;
        $status = $post_data->status;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($driver_id) {
                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {
                        // get the trip details
                        $trip_details = $this->Trip_Details_Model->getById($trip_id);
                        if ($trip_details) {
                            // check trip avilable for driver/any in progress trip before logout
                            $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                'tripId' => $trip_id
                            ));
                            if ($check_trip_avilablity) {
                                if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED
                                    && $trip_details->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED
                                ) {
                                    //getting valid & avilable promocode if trip is first ride for passenger & if no promocode applied by passenger
                                    $promocode = '';
                                    if (!$trip_details->promoCode) {
                                        $promocode = $this->Passenger_Promocode_Details_Model->getFreeRidePromoCode($trip_details->passengerId);
                                    }

                                    // Zoomcar api call Start

                                    if ($trip_details->companyId == ZOOMCAR_COMPANYID) {
                                        $zoom_startTrip_response = $this->Api_Webservice_Model->zoomCarUpdateTripStatus($trip_id, $trip_details->bookingKey, 1);
                                        if ($zoom_startTrip_response['status'] == 'success') {
                                            $db_trip_update = 1;
                                        } else {
                                            $message = array(
                                                "message" => 'Zoomcar API failed. Failure Message: ' . $zoom_checklist_response['message'],
                                                "status" => -1
                                            );
                                        }
                                    } else {
                                        $db_trip_update = 1;
                                    }
                                    // Zoomcar api call End

                                    // To update trip status on tripdetails

                                    if ($db_trip_update) {
                                        $trip_status_update = $this->Trip_Details_Model->update(array(
                                            'tripStatus' => Trip_Status_Enum::IN_PROGRESS,
                                            'notificationStatus' => Trip_Status_Enum::IN_PROGRESS,
                                            'actualPickupDatetime' => getCurrentDateTime(),
                                            'promoCode' => ($promocode) ? $promocode : ''
                                        ), array(
                                            'id' => $trip_id,
                                            'driverId' => $driver_id
                                        ));

                                        if ($trip_status_update) {
                                            // To update trip status on driverrequesttripdetails
                                            $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                                'tripStatus' => Driver_Request_Status_Enum::DRIVER_ACCEPTED
                                            ), array(
                                                'tripId' => $trip_id,
                                                'selectedDriverId' => $driver_id
                                            ));
                                        }
                                        if ($trip_request_status_update) {
                                            // inserting the start trip location of driver into temp tracking table for particular trip
                                            $insert_data = array(
                                                'tripId' => $trip_id,
                                                'latitude' => $latitude,
                                                'longitude' => $longitude
                                            );
                                            $trip_tracking_update = $this->Trip_Temp_Tracking_Info_Model->insert($insert_data);

                                            $message = array(
                                                "message" => $msg_data ['driver_start_trip'],
                                                "status" => 1
                                            );
                                        } else {
                                            $message = array(
                                                "message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
                                                "status" => -1
                                            );
                                            // log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
                                        }
                                    }
                                } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED && $trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS) {
                                    $message = array(
                                        "message" => $msg_data ['trip_started'],
                                        "status" => 2
                                    );
                                } else {


                                    if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                                        $message = array(
                                            "message" => $msg_data ['trip_reject_passenger'],
                                            "status" => -1
                                        );
                                    } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                                        $message = array(
                                            "message" => $msg_data ['trip_completed'],
                                            "status" => -1
                                        );
                                    }
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['invalid_trip_request'],
                                    "status" => -1
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_trip'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * driver completed the trip api
     */
    public function complete_trip_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $trip_status_update = NULL;
        $trip_request_status_update = NULL;
        $rate_card_details = NULL;

        // postdata intialized
        $trip_tracking_update = NULL;
        $wallet_transaction_id = NULL;
        $driver_id = NULL;
        $trip_id = NULL;
        $drop_latitude = NULL;
        $drop_longitude = NULL;
        $drop_location = NULL;
        $distance = NULL;
        $actual_distance = NULL;
        $waiting_hour = NULL;
        $trip_type = NULL;
        $map_url = NULL;
        $db_trip_update = 0;
        $other_fee = NULL;
        $parking_fee = NULL;
        $is_admin_control = NULL;
        $rating = NULL;
        $comments = NULL;

        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $trip_id = $post_data->trip_id;
        $drop_latitude = $post_data->drop_latitude;
        $drop_longitude = $post_data->drop_longitude;
        $drop_location = $post_data->drop_location;
        $distance = $post_data->distance;
        $actual_distance = $post_data->actual_distance;
        $waiting_hour = $post_data->waiting_hour;
        $trip_type = $post_data->plan_type;

        if (array_key_exists('other_fee', $post_data)) {
            $other_fee = $post_data->other_fee;
        }
        if (array_key_exists('parking_fee', $post_data)) {
            $parking_fee = $post_data->parking_fee;
        }
        if (array_key_exists('is_admin_control', $post_data)) {
            $is_admin_control = $post_data->is_admin_control;
        }
        if (array_key_exists('rating', $post_data)) {
            $rating = $post_data->rating;
        }
        if (array_key_exists('comments', $post_data)) {
            $comments = $post_data->comments;
        }
        if (array_key_exists('map_url', $post_data)) {
            $map_url = $post_data->map_url;
        }
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // To get site info details
                $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                    'id' => 1
                ));

                // actual functionality
                if ($trip_id > 0) {

                    // get the trip details
                    $trip_details = $this->Trip_Details_Model->getById($trip_id);

                    if ($trip_details) {

                        // Check if driverId is valid or not

                        if ($trip_details->driverId) {

                            $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                                'id' => $trip_details->driverId,
                                'status' => Status_Type_Enum::ACTIVE,
                                'loginStatus' => Status_Type_Enum::ACTIVE
                            ));

                            if ($driver_logged_status) {

                                // get check wheather trip is allredy ended but waiting for payment
                                if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT ||
                                    $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT
                                ) {

                                    $message = array(
                                        "message" => $msg_data ['trip_waiting_payment'],
                                        "status" => 2
                                    );
                                    echo json_encode($message);
                                    exit ();
                                }

                                // check trip avilable for driver/any in progress trip before logout
                                $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                    'tripId' => $trip_id
                                ));

                                if ($check_trip_avilablity) {

                                    if ($check_trip_avilablity->tripStatus != Driver_Request_Status_Enum::DRIVER_REJECTED &&
                                        $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED
                                    ) {

                                        // intialized for calculation purpose

                                        $actual_pickup_time = $trip_details->actualPickupDatetime;
                                        $actual_drop_time = getCurrentDateTime();
                                        $total_calculated_hours = 0;
                                        $travelled_staged_hours = 0;
                                        $travelled_hours = 0;
                                        $travelled_minutes = 0;
                                        $travelled_seconds = 0;
                                        $travel_hours_minutes = 0;
                                        $convenience_charge = 0;
                                        $travel_charge = 0;
                                        $total_trip_cost = 0;
                                        $promo_discount_amount = 0;
                                        $company_tax = 0;
                                        $wallet_payment_amount = 0;
                                        $is_master_trip = 0;
                                        $total_subtrips = 0;

                                        if ($trip_details->billType == Bill_Type_Enum::MONTHLY) {

                                            // get rate card details based on the cityId & trip type

                                            $rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray(array(
                                                'cityId' => $trip_details->bookingCityId,
                                                'companyId' => $trip_details->companyId,
                                                'tripType' => $trip_details->tripType,
                                                'status' => Status_Type_Enum::ACTIVE,
                                                'isMonthly' => Status_Type_Enum::ACTIVE
                                            ));
                                        } else {
                                            // get rate card details based on the cityId & trip type
                                            $rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray(array(
                                                'cityId' => $trip_details->bookingCityId,
                                                'companyId' => $trip_details->companyId,
                                                'tripType' => $trip_details->tripType,
                                                'status' => Status_Type_Enum::ACTIVE,
                                                'isMonthly' => Status_Type_Enum::INACTIVE
                                            ));
                                        }

                                        /// Start ZoomCar API Call
                                        if ($trip_details->companyId == ZOOMCAR_COMPANYID) {
                                            $zoom_startTrip_response = $this->Api_Webservice_Model->zoomCarUpdateTripStatus($trip_id, $trip_details->bookingKey, 2);
                                            if ($zoom_startTrip_response['status'] == 'success') {
                                                $db_trip_update = 1;
                                            } else {
                                                $message = array(
                                                    "message" => 'Zoomcar API failed. Failure Message: ' . $zoom_checklist_response['message'],
                                                    "status" => -1
                                                );
                                            }
                                        } /// End ZoomCar API Call

                                        else {
                                            if ($trip_details->companyId != ZOOMCAR_COMPANYID &&
                                                $trip_details->companyId != DEFAULT_COMPANY_ID) {

                                                $all_sub_trips = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId' => $trip_id));
                                                if ($all_sub_trips) {

                                                    $last_sub_trip_details = $this->Sub_Trip_Details_Model->getOneByKeyValue('masterTripId', $trip_id, 'dropDatetime Desc');
                                                    if (strtotime($actual_drop_time) < strtotime($last_sub_trip_details->dropDatetime)) {
                                                        $message = array(
                                                            "message" => $msg_data['invalid_master_trip_drop_time'],
                                                            "status" => -1
                                                        );
                                                        echo json_encode($message);
                                                        exit ();
                                                    } else {
                                                        $total_subtrips = count($all_sub_trips);
                                                        $db_trip_update = 1;
                                                        $is_master_trip = 1;
                                                    }
                                                } else {
                                                    if ($trip_details->billType == Bill_Type_Enum::FIXED) {
                                                        $message = array(
                                                            "message" => 'Trip Should have atleast one sub-trip.',
                                                            "status" => 101
                                                        );
                                                        echo json_encode($message);
                                                        exit ();
                                                    } else {
                                                        $db_trip_update = 1;
                                                        $is_master_trip = 0;
                                                    }
                                                }
                                            } else {
                                                $db_trip_update = 1;

                                            }
                                        }

                                        if ($db_trip_update) {
                                            if (strtotime($actual_pickup_time) < strtotime($actual_drop_time)) {

                                                $dateDiff = intval((strtotime($actual_pickup_time) - strtotime($actual_drop_time)) / 60);
                                                $total_calculated_hours = round(abs(strtotime($actual_pickup_time) - strtotime($actual_drop_time)) / 3600, 2);
                                                $travelled_hours = intval(abs($dateDiff / 60));
                                                $travelled_minutes = intval(abs($dateDiff % 60));
                                                $travelled_seconds = intval(abs((strtotime($actual_pickup_time) - strtotime($actual_drop_time)) % 3600) % 60);

                                                if ($travelled_seconds > 0) {
                                                    $travelled_seconds = 0;
                                                    $travelled_minutes += 1;
                                                }
                                                if ($travelled_minutes == 60) {
                                                    $travelled_minutes = 0;
                                                    $travelled_hours += 1;
                                                } else {
                                                    $travelled_staged_hours = $travelled_hours;
                                                    /*
                                                     * if (($travelled_minutes > 0) && ($travelled_minutes < 60)) {
                                                     * $travelled_staged_hours = $travelled_hours + 1;
                                                     * }
                                                     */
                                                }

                                                $travel_hours_minutes = $travelled_hours . '.' . $travelled_minutes;

                                                // to get convenience charge based pickup time day/night

                                                if (date('H:i:s', strtotime($actual_pickup_time)) >= DAY_START_TIME && date('H:i:s', strtotime($actual_pickup_time)) <= DAY_END_TIME) {
                                                    if ($trip_details->billType == Bill_Type_Enum::MONTHLY) {
                                                        //if ($travelled_staged_hours < 4) {
                                                        $convenience_charge += $rate_card_details->dayConvenienceCharge;
                                                        //}
                                                    } else if ($trip_details->billType == Bill_Type_Enum::HOURLY) {
                                                        $convenience_charge += $rate_card_details->dayConvenienceCharge;
                                                    }
                                                } else {
                                                    //if (date ( 'H:i:s', strtotime ( $actual_pickup_time ) ) >= NIGHT_START_TIME && date ( 'H:i:s', strtotime ( $actual_pickup_time ) ) <= NIGHT_END_TIME)
                                                    if ($trip_details->billType == Bill_Type_Enum::MONTHLY) {
                                                        //if ($travelled_staged_hours < 4) {
                                                        $convenience_charge += $rate_card_details->nightConvenienceCharge;
                                                        //}
                                                    } else if ($trip_details->billType == Bill_Type_Enum::HOURLY) {
                                                        $convenience_charge += $rate_card_details->nightConvenienceCharge;
                                                    }
                                                }

                                                if (count($rate_card_details) > 0) {
                                                    if ($trip_details->billType == Bill_Type_Enum::HOURLY || $trip_details->billType == Bill_Type_Enum::MONTHLY) {
                                                        // calculation based on HOURLY i.e transit

                                                        if ($travelled_minutes == 0 && ($travelled_seconds > 0)) {

                                                            $travel_charge += $rate_card_details->firstHourCharge;
                                                        }

                                                        if ($travelled_hours == 0 && ($travelled_minutes > 0)) {

                                                            $travel_charge += $rate_card_details->firstHourCharge;
                                                        }

                                                        if ($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY || $trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) {
                                                            $travelled_staged_hours = 0;
                                                            if ($travelled_hours > OUT_STATION_TIME_LIMIT) {
                                                                $travelled_staged_hours = $travelled_hours - OUT_STATION_TIME_LIMIT;
                                                            }

                                                            if ($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY) {
                                                                $distance_travelled = explode('.', $rate_card_details->minDistance);
                                                                if ($actual_distance <= $distance_travelled [0]) {
                                                                    $travel_charge = $actual_distance * $rate_card_details->distanceConvenienceCharge;

                                                                    $total_trip_cost = $convenience_charge + $travel_charge;
                                                                } else {
                                                                    $travel_charge = $actual_distance * $rate_card_details->distanceCostPerKm;

                                                                    $total_trip_cost = $convenience_charge + $travel_charge;
                                                                }
                                                            } else if ($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) {
                                                                $distance_travelled = explode('.', $rate_card_details->minDistance);
                                                                if ($actual_distance <= $distance_travelled [0]) {
                                                                    $travel_charge = $actual_distance * $rate_card_details->distanceConvenienceCharge;

                                                                    $total_trip_cost = $convenience_charge + $travel_charge;
                                                                } else {
                                                                    $travel_charge = $actual_distance * $rate_card_details->distanceCostPerKm;

                                                                    $total_trip_cost = $convenience_charge + $travel_charge;
                                                                }
                                                            }
                                                        }
                                                        // claculation based on travel_hours_minutes
                                                        if ($travelled_staged_hours > 0) {
                                                            switch ($travelled_staged_hours) {
                                                                case 1 : {
                                                                    // $travel_charge+=$rate_card_details->firstHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->firstHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->secondHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->secondHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 2 : {
                                                                    // $travel_charge+=$rate_card_details->secondHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->secondHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->thirdHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->thirdHourCharge;
                                                                    }
                                                                    break;
                                                                }

                                                                case 3 : {
                                                                    // $travel_charge+=$rate_card_details->thirdHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->thirdHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->fourthHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->fourthHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 4 : {
                                                                    // $travel_charge+=$rate_card_details->fourthHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->fourthHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->fifthHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->fifthHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 5 : {
                                                                    // $travel_charge+=$rate_card_details->fifthHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->fifthHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->sixthHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->sixthHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 6 : {
                                                                    // $travel_charge+=$rate_card_details->sixthHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->sixthHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->seventhHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->seventhHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 7 : {
                                                                    // $travel_charge+=$rate_card_details->seventhHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->seventhHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->eighthHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->eighthHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 8 : {
                                                                    // $travel_charge+=$rate_card_details->eighthHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->eighthHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->ninthHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->ninthHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 9 : {
                                                                    // $travel_charge+=$rate_card_details->ninthHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->ninthHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->tenthHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->tenthHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 10 : {
                                                                    // $travel_charge+=$rate_card_details->tenthHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->tenthHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->eleventhHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->eleventhHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 11 : {
                                                                    // $travel_charge+=$rate_card_details->eleventhHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->eleventhHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->twelveHourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->twelveHourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                case 12 : {
                                                                    // $travel_charge+=$rate_card_details->twelveHourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->twelveHourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->above12HourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->above12HourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                                default : {
                                                                    // $travel_charge+=$rate_card_details->above12HourCharge;

                                                                    $travel_charge += $travelled_hours * $rate_card_details->above12HourCharge;
                                                                    if (($travelled_minutes > 0)) {
                                                                        // $travel_charge+=$rate_card_details->above12HourCharge;
                                                                        $travel_charge += $travelled_minutes / 60 * $rate_card_details->above12HourCharge;
                                                                    }
                                                                    break;
                                                                }
                                                            }
                                                        }

                                                        $total_trip_cost = $convenience_charge + $travel_charge;
                                                        $total_trip_cost = round($total_trip_cost, 2);
                                                        $travel_charge = $total_trip_cost;
                                                    } else if ($trip_details->billType == Bill_Type_Enum::FIXED) {
                                                        if ($is_master_trip) {
                                                            $travel_charge = ($rate_card_details->fixedAmount * $total_subtrips) - $convenience_charge;
                                                            $total_trip_cost = round($rate_card_details->fixedAmount * $total_subtrips, 2);
                                                        } else {
                                                            // No calculation it is just billing the fixed price + tax with promo discount
                                                            $travel_charge = $rate_card_details->fixedAmount - $convenience_charge;
                                                            $total_trip_cost = round($rate_card_details->fixedAmount, 2);
                                                        }

                                                    } else if ($trip_details->billType == Bill_Type_Enum::DISTANCE) {
                                                        // @todo calculation based on distance min-distance fixed + distance cost per distance
                                                        $convenience_charge = $rate_card_details->distanceConvenienceCharge;
                                                        $distance_travelled = explode('.', $rate_card_details->minDistance);
                                                        if ($travelled_hours <= $distance_travelled [0]) {
                                                            $travel_charge = 0;
                                                            $total_trip_cost = $convenience_charge;
                                                        } else {
                                                            $travelled_hours = $travelled_hours - $rate_card_details->minDistance;

                                                            $travel_charge = $travelled_hours * $rate_card_details->distanceCostPerKm;
                                                            if ($travelled_minutes > 0) {
                                                                if ($travelled_minutes > $distance_travelled [1]) {

                                                                    // $travel_charge+=$rate_card_details->timeCostPerHour;
                                                                    $travel_charge += $distance_travelled [1] % 1000 * $rate_card_details->timeCostPerHour;
                                                                }
                                                                $total_trip_cost = $convenience_charge + $travel_charge;
                                                            }
                                                        }
                                                    } else if ($trip_details->billType == Bill_Type_Enum::TIME) {
                                                        // calculation based on time min-time fixed + time cost per hour
                                                        $convenience_charge = $rate_card_details->timeConvenienceCharge;
                                                        $hours = explode('.', $rate_card_details->minTime);
                                                        if ($travelled_hours <= $hours [0]) {
                                                            $travel_charge = 0;
                                                            $total_trip_cost = $convenience_charge;
                                                        } else {
                                                            $travelled_hours = $travelled_hours - $rate_card_details->minTime;

                                                            $travel_charge = $travelled_hours * $rate_card_details->timeCostPerHour;
                                                            if ($travelled_minutes > 0) {
                                                                if ($travelled_minutes > $hours [1]) {

                                                                    // $travel_charge+=$rate_card_details->timeCostPerHour;
                                                                    $travel_charge += $hours [1] / 60 * $rate_card_details->timeCostPerHour;
                                                                }
                                                                $total_trip_cost = $convenience_charge + $travel_charge;
                                                            }
                                                        }
                                                    } else {
                                                        $message = array(
                                                            "message" => $msg_data ['invalid_bill_type'],
                                                            "status" => -1
                                                        );
                                                    }

                                                    // calculate promocode discount if applicable
                                                    if (!empty ($trip_details->promoCode) && $trip_details->promoCode != '') {

                                                        // Check promocode exists or not for particular passenger at particular city
                                                        $promocode_exists = $this->Api_Webservice_Model->checkPromocodeAvailablity($trip_details->promoCode, $trip_details->passengerId, $trip_details->bookingCityId);

                                                        if ($promocode_exists && count($promocode_exists) > 0) {
                                                            if (strtotime($promocode_exists [0]->promoStartDate) < getCurrentUnixDateTime() && strtotime($promocode_exists [0]->promoEndDate) > getCurrentUnixDateTime()) {

                                                                // check promocode limit exceeds or not for specific passenger (note: only completed trip)
                                                                $promocode_user_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                                                                    'passengerId' => $trip_details->passengerId,
                                                                    'promoCode' => $trip_details->promoCode,
                                                                    'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                                                                ));
                                                                // check promocode limit exceeds or not total limit (note: only completed trip)
                                                                $promocode_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                                                                    'promoCode' => $trip_details->promoCode,
                                                                    'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                                                                ));

                                                                if ((count($promocode_limit_exists) < $promocode_exists [0]->promoCodeLimit) && (count($promocode_user_limit_exists) < $promocode_exists [0]->promoCodeUserLimit)) {
                                                                    // get promo code details
                                                                    $promocode_details = $this->Passenger_Promocode_Details_Model->getOneByKeyValueArray(array(
                                                                        'promoCode' => $trip_details->promoCode
                                                                    ));

                                                                    $promo_discount = $promocode_details->promoDiscountAmount;
                                                                    $promo_type = $promocode_details->promoDiscountType;
                                                                    if ($promo_type == Promocode_Type_Enum::PERCENTAGE) {
                                                                        // discount in %
                                                                        $calculate_amt = ($promo_discount / 100) * $total_trip_cost;
                                                                        $promo_discount_amount = round($calculate_amt, 2);
                                                                        $total_trip_cost = $total_trip_cost - $promo_discount_amount;
                                                                        $promo_discount = $promo_discount_amount;
                                                                    } else if ($promo_type == Promocode_Type_Enum::FIXED) {
                                                                        // discount in Rs
                                                                        if ($promo_discount > $total_trip_cost) {

                                                                            $promo_discount_amount = $total_trip_cost;
                                                                            $promo_discount = $promo_discount_amount;
                                                                        } else {

                                                                            $promo_discount_amount = $promo_discount;
                                                                        }
                                                                        $total_trip_cost = $total_trip_cost - $promo_discount;
                                                                    }
                                                                    if ($total_trip_cost < 0) {
                                                                        $total_trip_cost = 0;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // tax calculation based on data of site info table
                                                    if ($total_trip_cost > 0) {
                                                        $tax = $site_setting_info->tax;
                                                        if ($tax > 0) {
                                                            $company_tax = ($tax / 100) * $total_trip_cost;
                                                            $company_tax = round($company_tax, 2);
                                                            $total_trip_cost += $company_tax;
                                                        }
                                                    }
                                                    // deduction from wallet if payment mode is wallet
                                                    if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET) {

                                                        if ($total_trip_cost > 0) {
                                                            // get current wallet amount of passenger
                                                            $current_wallet_amount = $this->Passenger_Model->getById($trip_details->passengerId);

                                                            if ($total_trip_cost >= $current_wallet_amount->walletAmount) {
                                                                $wallet_payment_amount = $current_wallet_amount->walletAmount;
                                                                // $total_trip_cost = $total_trip_cost - $current_wallet_amount->walletAmount;
                                                                $remaing_wallet_amount ['walletAmount'] = 0;
                                                            } else {
                                                                $actual_wallet_amount = $current_wallet_amount->walletAmount;

                                                                $current_wallet_amount->walletAmount = $current_wallet_amount->walletAmount - $total_trip_cost;
                                                                $wallet_payment_amount = $actual_wallet_amount - $current_wallet_amount->walletAmount;
                                                                $remaing_wallet_amount ['walletAmount'] = $current_wallet_amount->walletAmount;
                                                                // $total_trip_cost = 0;
                                                            }
                                                            $wallet_payment_update = $this->Passenger_Model->update($remaing_wallet_amount, array(
                                                                'id' => $trip_details->passengerId
                                                            ));
                                                        }

                                                    }
                                                    // update the trip details trip status to waiting for payment ,driver shift status to free also trip request details trip status to completed

                                                    $trip_status = NULL;
                                                    $notification_status = NULL;
                                                    if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
                                                        $trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
                                                        $notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
                                                    } else if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
                                                        $trip_status = Trip_Status_Enum::NO_NOTIFICATION;
                                                        $notification_status = Trip_Status_Enum::NO_NOTIFICATION;
                                                    } else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
                                                        $trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
                                                        $notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
                                                    }
                                                    $trip_status_update = $this->Trip_Details_Model->update(array(
                                                        'tripStatus' => $trip_status,
                                                        'notificationStatus' => $notification_status,
                                                        'dropLocation' => $drop_location,
                                                        'dropLatitude' => $drop_latitude,
                                                        'dropLongitude' => $drop_longitude,
                                                        'dropDatetime' => $actual_drop_time,
                                                        'mapUrl' => $map_url
                                                    ), array(
                                                        'id' => $trip_id
                                                    ));

                                                    $is_first_ride = $this->Trip_Details_Model->getByKeyValueArray(array('passengerId' => $trip_details->passengerId,
                                                        'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                                                    ), 'id');
                                                    if (count($is_first_ride) <= 0) {
                                                        $passenger_referral_details = $this->Passenger_Referral_Details_Model->getOneByKeyValueArray(array('registeredPassengerId' => $trip_details->passengerId), 'id DESC');
                                                        if ($passenger_referral_details) {
                                                            $passenger_referral_update = $this->Passenger_Referral_Details_Model->update(array('registeredTripId' => $trip_id, 'isReferrerEarned' => Status_Type_Enum::ACTIVE), array('registeredPassengerId' => $trip_details->passengerId));
                                                            $passenger_details = $this->Passenger_Model->getById($passenger_referral_details->passengerId);
                                                            $current_balance = $passenger_details->walletAmount + $passenger_referral_details->earnedAmount;
                                                            $insert_data = array('passengerId' => $passenger_referral_details->passengerId, 'passengerReferralId' => $trip_details->passengerId,
                                                                'tripId' => $trip_id, 'transactionAmount' => $passenger_referral_details->earnedAmount,
                                                                'previousAmount' => $passenger_details->walletAmount, 'currentAmount' => $current_balance, 'transactionStatus' => 'Success',
                                                                'transactionType' => Transaction_Type_Enum::REFERRAL,
                                                                'manipulationType' => Manipulation_Type_Enum::ADDITION
                                                            );
                                                            $passenger_wallet_updated = $this->Passenger_Wallet_Details_Model->insert($insert_data);
                                                            $passenger_updated = $this->Passenger_Model->update(array('walletAmount' => $current_balance), array('id' => $passenger_referral_details->passengerId));
                                                        }
                                                    }
                                                    //Passenger_Referral_Details_Model $trip_request_status_update=$this->Driver_Request_Details_Model->update(array('tripStatus'=>Driver_Request_Status_Enum::COMPLETED_TRIP),array('tripId'=>$trip_id));
                                                    // $driver_shift_status_update=$this->Driver_Shift_History_Model->update(array('availabilityStatus'=>Driver_Available_Status_Enum::FREE),array('driverId'=>$trip_details->driverId));

                                                    // get the temp tracking lat & log to update the tracked trip
                                                    $temp_tracking_info = $this->Trip_Temp_Tracking_Info_Model->getByKeyValueArray(array(
                                                        'tripId' => $trip_id
                                                    ));
                                                    if (count($temp_tracking_info) > 0) {
                                                        $tracking_details = '';
                                                        foreach ($temp_tracking_info as $list) {
                                                            if (empty ($tracking_details)) {
                                                                $tracking_details = '[' . $list->latitude . ',' . $list->longitude . ']';
                                                            } else {
                                                                $tracking_details .= ',[' . $list->latitude . ',' . $list->longitude . ']';
                                                            }
                                                        }
                                                        $tracking_details .= ',[' . $drop_latitude . ',' . $drop_longitude . ']';

                                                        $insert_data = array(
                                                            'tripId' => $trip_id,
                                                            'latitudeLogitude' => $tracking_details,
                                                            'passengerId' => $trip_details->passengerId,
                                                            'driverId' => $trip_details->driverId
                                                        );

                                                        $trip_tracking_update = $this->Trip_Tracked_Info_Model->insert($insert_data);

                                                        // delete the temp tracking lat & log
                                                        $delete_temp_tracking_info = $this->Trip_Temp_Tracking_Info_Model->delete(array(
                                                            'tripId' => $trip_id
                                                        ));
                                                    }
                                                    // response data
                                                    $response_data = array(
                                                        "trip_id" => $trip_id,
                                                        "pass_id" => $trip_details->passengerId,
                                                        "distance" => $distance,
                                                        "trip_fare" => $travel_charge,
                                                        "referdiscount" => 0,
                                                        "promo_discount" => $promo_discount_amount,
                                                        "promodiscount_amount" => $promo_discount_amount,
                                                        "fare" => $total_trip_cost,
                                                        "total_tripcost" => $total_trip_cost,
                                                        "passenger_discount" => $promo_discount_amount,
                                                        "nightfare_applicable" => 0,
                                                        "nightfare" => 0,
                                                        "eveningfare_applicable" => 0,
                                                        "eveningfare" => 0,

                                                        // "waiting_time"=>$waiting_hours,
                                                        "waiting_time" => 0,
                                                        "waiting_cost" => 0,
                                                        "tax_amount" => $company_tax,
                                                        "total_fare" => $total_trip_cost,
                                                        "gateway_details" => array(), // array_reverse ( $gateway_details ),
                                                        "pickup" => $trip_details->pickupLocation,
                                                        "drop" => $trip_details->dropLocation,
                                                        "tax_percentage" => $tax,
                                                        "waiting_per_hour" => 0,
                                                        "roundtrip" => 0,
                                                        "minutes_traveled" => $travel_hours_minutes,
                                                        "minutes_fare" => 0,
                                                        "metric" => UNIT_NAME,
                                                        "credit_card_status" => 0,
                                                        'trip_type' => ($trip_details->tripType == Trip_Type_Enum::ONEWAY_TRIP) ? Trip_Type_Enum::O : (($trip_details->tripType == Trip_Type_Enum::RETURN_TRIP) ? Trip_Type_Enum::R : (($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP) ? Trip_Type_Enum::X : (($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY) ? Trip_Type_Enum::XO : (($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) ? Trip_Type_Enum::XR : '')))),
                                                        "pickup_time" => $actual_pickup_time,
                                                        "drop_time" => $actual_drop_time,
                                                        "display_time" => $travel_hours_minutes,
                                                        'calculation_time' => $total_calculated_hours,
                                                        "convenience_charge" => $convenience_charge,

                                                        // "overtime_cost"=>round($overtime_cost,2),
                                                        // "ot_time"=>round($overtime,2),
                                                        "overtime_cost" => 0,
                                                        "ot_time" => 0,
                                                        "map_url" => $map_url,
                                                        "wallet_payment" => round($wallet_payment_amount, 2),
                                                        "faretype" => ($trip_details->paymentMode == Payment_Mode_Enum::CASH) ? 1 : (($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) ? 2 : (($trip_details->paymentMode == Payment_Mode_Enum::WALLET) ? 3 : ''))
                                                    );
                                                    $message = array(
                                                        "message" => $msg_data ['driver_start_complete'],
                                                        "detail" => $response_data,
                                                        "status" => 4
                                                    );
                                                } else {
                                                    $message = array(
                                                        "message" => $msg_data ['invalid_rate_card'],
                                                        "status" => -1
                                                    );
                                                }
                                            } else {
                                                $message = array(
                                                    "message" => $msg_data ['invalid_drop_time'],
                                                    "status" => -1
                                                );
                                            }
                                        }
                                        /**
                                         * ***************************************************
                                         *
                                         * // To update trip status on tripdetails
                                         * $trip_status_update = $this->Trip_Details_Model->update ( array (
                                         * 'tripStatus' => Trip_Status_Enum::IN_PROGRESS,
                                         * 'notificationStatus' => Trip_Status_Enum::IN_PROGRESS,
                                         * 'actualPickupDatetime' => getCurrentDateTime ()
                                         * ), array (
                                         * 'id' => $trip_id,
                                         * 'driverId' => $trip_details->driverId
                                         * ) );
                                         *
                                         * if ($trip_status_update) {
                                         * // To update trip status on driverrequesttripdetails
                                         * $trip_request_status_update = $this->Driver_Request_Details_Model->update ( array (
                                         * 'tripStatus' => Driver_Request_Status_Enum::DRIVER_ACCEPTED
                                         * ), array (
                                         * 'tripId' => $trip_id,
                                         * 'selectedDriverId' => $trip_details->driverId
                                         * ) );
                                         * }
                                         * if ($trip_request_status_update) {
                                         * // inserting the start trip location of driver into temp tracking table for particular trip
                                         * $insert_data = array (
                                         * 'tripId' => $trip_id,
                                         * 'latitude' => $latitude,
                                         * 'longitude' => $longitude
                                         * );
                                         * $trip_tracking_update = $this->Trip_Temp_Tracking_Info_Model->insert ( $insert_data );
                                         *
                                         * $message = array (
                                         * "message" => 'Trip has been started',
                                         * "status" => 1
                                         * );
                                         * } else {
                                         * $message = array (
                                         * "message" => 'Failed to update trip status & trip request status for trip_id=' . $trip_id,
                                         * "status" => - 1
                                         * );
                                         * // log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
                                         * }
                                         * **************************
                                         */
                                    } else {
                                        if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                                            $message = array(
                                                "message" => $msg_data ['trip_reject_passenger'],
                                                "status" => -1
                                            );
                                        } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                                            $message = array(
                                                "message" => $msg_data ['trip_completed'],
                                                "status" => -1
                                            );
                                        } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP ||
                                            $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER
                                        ) {
                                            $message = array(
                                                "message" => $msg_data ['trip_not_started'],
                                                "status" => -1
                                            );
                                        }
                                    }
                                } else {
                                    $message = array(
                                        "message" => $msg_data ['invalid_trip_request'],
                                        "status" => -1
                                    );
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['driver_login_failed'],
                                    "status" => -1
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_user'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_trip'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }
    /**
     * Trip mapUrl update API
     **/
    public function save_trip_map_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;

        // postdata intialized
        $driver_id = NULL;
        $trip_id = NULL;
        $map_url = NULL;

        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $trip_id = $post_data->trip_id;
        if (array_key_exists('map_url', $post_data)) {
            $map_url = $post_data->map_url;
        }
        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($trip_id > 0) {
                    // get the trip details
                    $trip_details = $this->Trip_Details_Model->getById($trip_id);
                    if ($trip_details) {
                        if ($trip_details->driverId) {
                            $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                                'id' => $trip_details->driverId,
                                'status' => Status_Type_Enum::ACTIVE,
                                'loginStatus' => Status_Type_Enum::ACTIVE
                            ));
                            if ($driver_logged_status) {
                                // check trip is ended or not
                                $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                    'tripId' => $trip_id
                                ));
                                if ($check_trip_avilablity) {
                                    if ($check_trip_avilablity->tripStatus != Driver_Request_Status_Enum::DRIVER_REJECTED && ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT)) {
                                        if ($map_url) {
                                            $trip_update = $this->Trip_Details_Model->update(array(
                                                'mapUrl' => $map_url
                                            ), array(
                                                'id' => $trip_id
                                            ));
                                        }
                                        $message = array(
                                            "message" => $msg_data ['success'],
                                            "status" => 1
                                        );
                                    } else {
                                        if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                                            $message = array(
                                                "message" => $msg_data ['trip_reject_passenger'],
                                                "status" => -1
                                            );
                                        } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) {
                                            $message = array(
                                                "message" => $msg_data ['trip_not_started'],
                                                "status" => -1
                                            );
                                        }
                                    }
                                } else {
                                    $message = array(
                                        "message" => $msg_data ['invalid_trip_request'],
                                        "status" => -1
                                    );
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['driver_login_failed'],
                                    "status" => -1
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_user'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['invalid_trip'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     *  Testing Code By Aditya, on 27th Apr 2017
     *  this function is to simply test Auth Key Authentication
     */
    public function aditya_authKeyTest_post()
    {
        $post_data = $this->get_post_data();

        $auth_key = $post_data->trip_id;
        $auth_user_id = $post_data->userId;
        $auth_user_type = $post_data->userType;

        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        print_r($data_auth_key);
        die();
    }

    /**
     * trip fare update api
     */
    public function tripfare_update_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $transaction_exists = NULL;
        $trip_status_update = NULL;
        $trip_request_status_update = NULL;
        $driver_shift_status_update = NULL;
        $transaction_insert_id = NULL;
        $trip_details = NULL;
        $trip_id = NULL;
        $distance = NULL;
        $actual_amount = NULL;
        $travel_charge = NULL;
        $total_trip_cost = NULL;
        $passenger_promo_discount = NULL;
        $company_tax = NULL;
        $remarks = NULL; // unused
        $faretype = NULL;
        $referdiscount = NULL;
        $wallet_payment = NULL;
        $promo_discount = NULL;
        $nightfare_applicable = NULL;
        $nightfare = NULL;
        $waiting_time = NULL;
        $waiting_cost = NULL;
        $minutes_traveled = NULL;
        $minutes_fare = NULL;
        $pay_mod_id = NULL;
        $other_fee = NULL;
        $parking_fee = NULL;
        $convenience_charge = NULL;
        $overtime_charge = NULL;
        $passenger_discount = NULL;
        $ot_time = NULL;
        $admin_amount = NULL;
        $company_amount = NULL;
        $invoice_no = NULL;
        $driver_earning = 0;
        $response_data = array();
        $statistics = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $trip_id = $post_data->trip_id;
        $distance = $post_data->distance;
        $actual_amount = $post_data->actual_amount;
        $travel_charge = $post_data->trip_fare;
        $total_trip_cost = $post_data->fare;
        $passenger_promo_discount = $post_data->passenger_promo_discount;
        $company_tax = $post_data->tax_amount;
        $payment_mode = $post_data->faretype;
        $referdiscount = $post_data->referdiscount;
        $wallet_payment = $post_data->wallet_payment;
        $promo_discount = $post_data->promo_discount;
        $nightfare_applicable = $post_data->nightfare_applicable;
        $nightfare = $post_data->nightfare;
        $waiting_time = $post_data->waiting_time;
        $waiting_cost = $post_data->waiting_cost;
        $minutes_traveled = $post_data->minutes_traveled;
        $minutes_fare = $post_data->minutes_fare;
        $pay_mod_id = $post_data->pay_mod_id;
        $other_fee = $post_data->other_fee;
        $parking_fee = $post_data->parking_fee;
        $convenience_charge = $post_data->convenience_charge;
        $overtime_charge = $post_data->overtime_charge;
        $passenger_discount = $post_data->passenger_discount;
        $ot_time = $post_data->ot_time;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');

        // Updated by Aditya, Auth Failed Error is coming over here, hence setting any Auth Check as passed
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                // to get site information details
                $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                    'id' => 1
                ));
                $tax = $site_setting_info->tax;
                // to get currency code/currency symbol currencySymbol
                $currency_details = $this->Country_Model->getColumnByKeyValueArray('currencySymbol', array(
                    'id' => $site_setting_info->countryId
                ));

                if ($trip_id) {
                    // get the trip details
                    $trip_details = $this->Trip_Details_Model->getById($trip_id);

                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $trip_details->driverId,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {

                        if ($trip_details) {
                            // check trip avilable for driver/any in progress trip before logout
                            $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                'tripId' => $trip_id
                            ));
                            // start get driver statistics

                            // to get the driver rejected/cancelled trip
                            $driver_rejected_count = $this->Driver_Rejected_Trip_Details_Model->getColumnByKeyValueArray('driverId', array(
                                'driverId' => $driver_logged_status->id,
                                'createdDate >=' => getCurrentDate(),
                                'createdDate <=' => getCurrentDate()
                            ));


                            // end get driver statistics
                            if ($check_trip_avilablity) {
                                if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED) {
                                    // calculate admin amount & company amount
                                    $admin_amount = ($site_setting_info->adminCommission / 100) * $total_trip_cost;
                                    $admin_amount = round($admin_amount, 2);
                                    $company_amount = $total_trip_cost - $admin_amount;
                                    $company_amount == round($company_amount, 2);

                                    // genereate invoice No
                                    $invoice_no = $trip_details->tripType . '-' . $trip_details->paymentMode . '-' . $trip_id;
                                    $total_trip_cost_insert = $total_trip_cost + $parking_fee + $other_fee;
                                    // calculation for driver earning based on driver type
                                    if ($driver_logged_status->driverType == Driver_Type_Enum::COMMISSIONBASED) {
                                        $driver_earning = ($driver_logged_status->commissionTripEarning / 100) * $total_trip_cost;
                                        $driver_earning = round($driver_earning, 2);
                                    } else if ($driver_logged_status->driverType == Driver_Type_Enum::FIXEDBASED) {
                                        $driver_earning = round($driver_logged_status->fixedTripEarning, 2);
                                    }

                                    $passenger_details = $this->Passenger_Model->getById($trip_details->passengerId);

                                    $insert_data = array(
                                        'tripId' => $trip_id,
                                        'travelDistance' => $distance,
                                        'travelHoursMinutes' => $minutes_traveled,
                                        'convenienceCharge' => $convenience_charge,
                                        'travelCharge' => $travel_charge - $convenience_charge,
                                        'parkingCharge' => $parking_fee,
                                        'tollCharge' => $other_fee,
                                        'companyTax' => $company_tax,
                                        'totalTripCost' => $total_trip_cost_insert,
                                        'adminAmount' => $admin_amount,
                                        'companyAmount' => $company_amount,
                                        'promoDiscountAmount' => $passenger_promo_discount,
                                        'walletPaymentAmount' => $wallet_payment,
                                        'otherDiscountAmount' => 0,
                                        'driverEarning' => $driver_earning,
                                        'paymentMode' => $trip_details->paymentMode,
                                        'paymentStatus' => ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details->paymentMode != Payment_Mode_Enum::PAYTM) ? 'Success' : 'Waiting for PAYTM payment'),
                                        'transactionId' => 0,
                                        'invoiceNo' => $invoice_no,
                                        'otherCharge' => 0
                                    );
                                    // to check transcation table exits trip record
                                    $transaction_exists = $this->Trip_Transaction_Details_Model->getOneByKeyValueArray(array(
                                        'tripId' => $trip_id
                                    ));
                                    if ($transaction_exists) {
                                        // update the transaction table

                                        $transaction_insert_id = $this->Trip_Transaction_Details_Model->update($insert_data, array(
                                            'id' => $transaction_exists->id
                                        ));
                                        $transaction_insert_id = $transaction_exists->id;
                                    } else {
                                        $transaction_insert_id = $this->Trip_Transaction_Details_Model->insert($insert_data);
                                    }
                                    if ($transaction_insert_id) {
                                        if ($driver_logged_status->driverType != Driver_Type_Enum::COMMISSIONBASED && $driver_logged_status->driverType != Driver_Type_Enum::FIXEDBASED) {
                                            //Driver earning history
                                            $driver_earning_for_this_trip = $this->calculate_driver_earning($trip_id, $driver_logged_status->id);
                                            $this->Trip_Transaction_Details_Model->update(array('driverEarning' => $driver_earning_for_this_trip), array('id' => $transaction_insert_id));
                                        }
                                        //passenger transaction history
                                        $transaction_amount = $total_trip_cost_insert - $wallet_payment;
                                        if ($transaction_amount > 0 && $passenger_details) {
                                            $insert_wallet_transaction = array('passengerId' => $passenger_details->id, 'tripId' => $trip_id, 'transactionAmount' => $transaction_amount, 'transactionStatus' => 'Success', 'transactionType' => Transaction_Type_Enum::TRIP, 'manipulationType' => Manipulation_Type_Enum::SUBTRACTION);
                                            $wallet_transaction_id = $this->Passenger_Wallet_Details_Model->insert($insert_wallet_transaction);
                                        }
                                        if ($wallet_payment > 0 && $passenger_details) {
                                            $current_amount = $passenger_details->walletAmount - $wallet_payment;
                                            $insert_wallet_transaction = array('passengerId' => $passenger_details->id, 'tripId' => $trip_id, 'transactionAmount' => $wallet_payment, 'previousAmount' => $passenger_details->walletAmount, 'currentAmount' => $current_amount, 'transactionStatus' => 'Success', 'transactionType' => Transaction_Type_Enum::TRIP, 'manipulationType' => Manipulation_Type_Enum::SUBTRACTION);
                                            $wallet_transaction_id = $this->Passenger_Wallet_Details_Model->insert($insert_wallet_transaction);
                                        }
                                        $trip_status = NULL;
                                        $notification_status = NULL;
                                        if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
                                            $trip_status = Trip_Status_Enum::TRIP_COMPLETED;
                                            $notification_status = Trip_Status_Enum::NO_NOTIFICATION;
                                        } else if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
                                            $trip_status = Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT;
                                            $notification_status = Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT;
                                        } else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
                                            $trip_status = Trip_Status_Enum::TRIP_COMPLETED;
                                            $notification_status = Trip_Status_Enum::TRIP_COMPLETED;
                                        }

                                        if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::NO_NOTIFICATION) {
                                            $trip_status_update = $this->Trip_Details_Model->update(array(
                                                'tripStatus' => $trip_status,
                                                'notificationStatus' => $notification_status
                                            ), array(
                                                'id' => $trip_id
                                            ));

                                            $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                                'tripStatus' => Driver_Request_Status_Enum::COMPLETED_TRIP,
                                                'selectedDriverId' => 0
                                            ), array(
                                                'tripId' => $trip_id
                                            ));

                                            //$driver_daily_earning=$this->driver_daily_earning($trip_details->dropDatetime, $trip_details->driverId);

                                            // get last inserted shift id for particular driver
                                            $get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                                'driverId' => $trip_details->driverId
                                            ), 'id DESC');
                                            $driver_shift_status_update = $this->Driver_Shift_History_Model->update(array(
                                                'availabilityStatus' => Driver_Available_Status_Enum::FREE
                                            ), array(
                                                'id' => $get_last_shift_id->id
                                            ));
                                        }
                                        // response data
                                        $response_data = array(
                                            "fare" => $total_trip_cost,
                                            "pickup" => $trip_details->pickupLocation,
                                            "invoice_no" => $invoice_no,
                                            "trip_id" => $trip_id
                                        );
                                        $driver_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id);
                                        $driver_today_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id, 1);

                                        // To calculate time driven on current date/today
                                        $total_amount = 0;
                                        $total_today_amount = 0;
                                        $time_result = '00:00';
                                        $actual_pickup_time = '';
                                        $drop_time = '';
                                        $hours = '';
                                        $minutes = '';
                                        $seconds = '';
                                        $date_difference = "";
                                        $total_differnce = "";
                                        foreach ($driver_earning_details as $get_details) {
                                            $actual_pickup_time = strtotime($get_details->actualPickupDatetime);
                                            $drop_time = strtotime($get_details->dropDatetime);
                                            // echo $actual_pickup_time;
                                            // echo '-';
                                            // echo $drop_time;
                                            // echo '<br>';
                                            $date_difference = abs($drop_time - $actual_pickup_time);
                                            $total_differnce += $date_difference;
                                            // to get total earned amount by the driver
                                            $total_amount += $get_details->driverEarning;
                                        }
                                        foreach ($driver_today_earning_details as $get_details) {
                                            // to get total earned amount by the driver
                                            $total_today_amount += $get_details->driverEarning;
                                        }
                                        // $date_difference = $drop_time - $actual_pickup_time;
                                        $hours += floor((($total_differnce % 604800) % 86400) / 3600);
                                        $minutes += floor(((($total_differnce % 604800) % 86400) % 3600) / 60);
                                        $seconds += floor((((($total_differnce % 604800) % 86400) % 3600) % 60));
                                        $time_result = $minutes . ':' . $seconds;
                                        // To calculate time driven on current date/today

                                        $statistics = array(
                                            "drivername" => $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName,
                                            "total_trip" => count($driver_earning_details),
                                            "cancel_trips" => count($driver_rejected_count),
                                            "total_earnings" => round($total_amount, 2),
                                            "overall_rejected_trips" => count($driver_rejected_count),
                                            "today_earnings" => round($total_today_amount, 2),
                                            "shift_status" => Driver_Shift_Status_Enum::SHIFTIN,
                                            "time_driven" => $time_result,
                                            "status" => 1
                                        );
                                        //
                                        // send sms & email to passenger
                                        if (SMS && $transaction_insert_id) {
                                            // SMS To passenger
                                            /*if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
												$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
														'smsTitle' => 'complete_trip_wallet'
												) );
											} else {
												$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
														'smsTitle' => 'complete_trip'
												) );
											}
											$message_data = $message_details->smsContent;
											$message_data = str_replace ( "##USERNAME##", $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName, $message_data );
											$message_data = str_replace ( "##FARE##", $total_trip_cost, $message_data );

											if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
												$message_data = str_replace ( "##PAID##", $total_trip_cost + $parking_fee + $other_fee, $message_data );
											}*/

                                            $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                'smsTitle' => 'new_complete_trip_sms'
                                            ));
                                            $message_data = $message_details->smsContent;
                                            $message_data = str_replace("##BOOKINGID##", $trip_id, $message_data);
                                            // Added by Aditya on 26th Apr 2017
                                            $message_data = str_replace("##FARE##", $total_trip_cost, $message_data);
                                            // Commented by Aditya on 26th Apr 2017
                                            // Using a new SMS template
                                            //$message_data = str_replace("##STARTDATETIME##", date('Y-m-d H:i:s',$actual_pickup_time), $message_data);
                                            //$message_data = str_replace("##ENDDATETIME##", date('Y-m-d H:i:s',$drop_time), $message_data);


                                            // print_r($message);exit;
                                            if ($passenger_details) {
                                                if ($passenger_details->mobile) {
                                                    // Commented by Aditya on May 22 2017
                                                    // Dhawal wanted to stop sending of this SMS
                                                    //$this->Api_Webservice_Model->sendSMS ( $passenger_details->mobile, $message_data );
                                                }
                                            }
                                            // SMS TO driver
                                            /*$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
													'smsTitle' => 'complete_trip_driver_sms'
											) );
											$customer_care_no = ($trip_details->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
											$message_data = $message_details->smsContent;
											$message_data = str_replace ( "##CUSTOMERCARE##", $customer_care_no, $message_data );
											$message_data = str_replace ( "##FARE##", $total_trip_cost, $message_data );
											$message_data = str_replace ( "##COLLECT##", $total_trip_cost + $parking_fee + $other_fee, $message_data );
											*/
                                            // print_r($message);exit;
                                            if ($driver_logged_status->mobile) {

                                                // Updated and written by Aditya on 27th Apr 2017
                                                // copied from the above commented code with own changes in it
                                                // earlier driver was receiveing the same SMS as Passenger
                                                // now driver receives his respective SMS
                                                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                    'smsTitle' => 'complete_trip_driver_sms'
                                                ));

                                                $message_data = $message_details->smsContent;
                                                $message_data = str_replace("##BOOKINGID##", $trip_id, $message_data);
                                                // Aditya code ended
                                                $this->Api_Webservice_Model->sendSMS($driver_logged_status->mobile, $message_data);
                                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                                $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                            }
                                        }
                                        $bill_details = $this->Api_Webservice_Model->getBillDetails($transaction_insert_id);
                                        $from = SMTP_EMAIL_ID;
                                        $to = ($passenger_details) ? $passenger_details->email : NULL;
                                        $subject = "Your " . date('l', strtotime($bill_details [0]->actualPickupDatetime)) . " trip with Zuver Rs." . $bill_details [0]->totalTripCost;


                                        $data = array(
                                            'passengerFullName' => ($bill_details [0]->passengerFullName) ? $bill_details [0]->passengerFullName : '',
                                            'passengerEmail' => ($bill_details [0]->passengerEmail) ? $bill_details [0]->passengerEmail : '',
                                            'driverFullName' => ($bill_details [0]->driverFullName) ? $bill_details [0]->driverFullName : '',
                                            'invoiceNo' => ($bill_details [0]->invoiceNo) ? $bill_details [0]->invoiceNo : '',
                                            'invoiceDatetime' => ($bill_details [0]->invoiceDatetime) ? date('d M, Y, h:i a', strtotime($bill_details [0]->invoiceDatetime)) : '',
                                            'actualPickupDatetime' => ($bill_details [0]->actualPickupDatetime) ? date('d M, Y, h:i a', strtotime($bill_details [0]->actualPickupDatetime)) : '',
                                            'dropDatetime' => ($bill_details [0]->dropDatetime) ? date('d M, Y, h:i a', strtotime($bill_details [0]->dropDatetime)) : '',
                                            'bookedDatetime' => ($bill_details [0]->bookedDatetime) ? date('d M, Y, h:i a', strtotime($bill_details [0]->bookedDatetime)) : '',
                                            'tripType' => ($bill_details [0]->tripType) ? $bill_details [0]->tripType : '',
                                            'paymentMode' => ($bill_details [0]->paymentMode) ? $bill_details [0]->paymentMode : '',
                                            'totalTripCost' => ($bill_details [0]->totalTripCost) ? round($bill_details [0]->totalTripCost, 2) : '00.00',
                                            'totalCash' => ($bill_details [0]->totalTripCost - $bill_details [0]->walletPaymentAmount) ? round($bill_details [0]->totalTripCost - $bill_details [0]->walletPaymentAmount, 2) : '00.00',
                                            'companyTax' => ($bill_details [0]->companyTax) ? round($bill_details [0]->companyTax, 2) : '00.00',
                                            'taxPercentage' => $tax,
                                            'travelDistance' => ($bill_details [0]->travelDistance) ? round($bill_details [0]->travelDistance, 2) : '00.00',
                                            'convenienceCharge' => ($bill_details [0]->convenienceCharge) ? round($bill_details [0]->convenienceCharge, 2) : '00.00',
                                            'travelCharge' => ($bill_details [0]->travelCharge) ? round($bill_details [0]->travelCharge, 2) : '00.00',
                                            'parkingCharge' => ($bill_details [0]->parkingCharge) ? round($bill_details [0]->parkingCharge, 2) : '00.00',
                                            'tollCharge' => ($bill_details [0]->tollCharge) ? round($bill_details [0]->tollCharge, 2) : '00.00',
                                            'promoDiscountAmount' => ($bill_details [0]->promoDiscountAmount) ? round($bill_details [0]->promoDiscountAmount, 2) : '00.00',
                                            'otherDiscountAmount' => ($bill_details [0]->otherDiscountAmount) ? round($bill_details [0]->otherDiscountAmount, 2) : '00.00',
                                            'walletPaymentAmount' => ($bill_details [0]->walletPaymentAmount) ? round($bill_details [0]->walletPaymentAmount, 2) : '00.00',
                                            'travelHoursMinutes' => ($bill_details [0]->travelHoursMinutes) ? $bill_details [0]->travelHoursMinutes : '',
                                            'otherCharge' => ($bill_details [0]->otherCharge) ? round($bill_details [0]->otherCharge, 2) : '00.00'
                                        );
                                        $message_data = $this->load->view('emailtemplate/tripcomplete-mail', $data, TRUE);

                                        if (SMTP && $transaction_insert_id) {
                                            if ($to) {
                                                $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                                            } else {
                                                //B2B completed trip invoice to be send to 'info@zuver.in' requested by Ayush through email on 10/01/2017
                                                $this->Api_Webservice_Model->sendEmail('info@zuver.in', $subject, $message_data);
                                                //B2B completed trip invoice to be send to 'info@zuver.in' requested by Ayush through email on 10/01/2017
                                            }
                                        } else {

                                            // To send HTML mail, the Content-type header must be set
                                            $headers = 'MIME-Version: 1.0' . "\r\n";
                                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                            // Additional headers
                                            $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                                            $headers .= 'To: <' . $to . '>' . "\r\n";
                                            if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                                                mail($to, $subject, $message_data, $headers);
                                            }
                                        }

                                        // send sms & email to passenger
                                        if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
                                            $message = array(
                                                "message" => $msg_data ['trip_finalize_waiting_paytm_payment'],
                                                "detail" => $response_data,
                                                "status" => 1,
                                                "driver_statistics" => $statistics
                                            );
                                        } else {

                                            $message = array(
                                                "message" => $msg_data ['trip_finalize_success'],
                                                "detail" => $response_data,
                                                "status" => 1,
                                                "driver_statistics" => $statistics
                                            );
                                        }
                                    } else {
                                        $message = array(
                                            "message" => $msg_data ['trip_finalize_failed'],
                                            "detail" => $response_data,
                                            "status" => -1,
                                            "driver_statistics" => $statistics
                                        );
                                    }
                                } else {
                                    if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                                        $message = array(
                                            "message" => $msg_data ['trip_reject_passenger'],
                                            "status" => -1
                                        );
                                    } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                                        $message = array(
                                            "message" => $msg_data ['trip_completed'],
                                            "detail" => $response_data,
                                            "status" => -1,
                                            "driver_statistics" => $statistics
                                        );
                                    } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) {
                                        $message = array(
                                            "message" => $msg_data ['trip_not_started'],
                                            "status" => -1
                                        );
                                    } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_REJECTED) {
                                        $message = array(
                                            "message" => $msg_data ['trip_reject_driver'],
                                            "status" => -1
                                        );
                                    }
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['invalid_trip_request'],
                                    "status" => -1
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_trip'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip_finalize_data'],
                        "status" => -3
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * update trip status to server (Continues request - every 5 seconds)
     */
    public function driver_location_history_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $trip_details = NULL;
        $driver_id = NULL;
        $trip_id = Null;
        $latitude = NULL;
        $longitude = NULL;
        $status = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $trip_id = $post_data->trip_id;
        $status = $post_data->status;
        $latitude = $post_data->latitude;
        $longitude = $post_data->longitude;

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {

            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                // to get site information details
                $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                    'id' => 1
                ));
                // to get currency code/currency symbol currencySymbol
                $currency_details = $this->Country_Model->getColumnByKeyValueArray('currencySymbol', array(
                    'id' => $site_setting_info->countryId
                ));

                if ($driver_id) {
                    $temp_trip_tracking_insert = $this->Driver_Temp_Tracking_Info_Model->insert(array(
                        'driverId' => $driver_id,
                        'latitude' => $latitude,
                        'longitude' => $longitude
                    ));
                    // get the trip details
                    if ($trip_id) {
                        $trip_details = $this->Trip_Details_Model->getById($trip_id);
                    }

                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {

                        // update the driver current latitude & longitude in driver details table
                        $driver_update = $this->Driver_Model->update(array(
                            'currentLatitude' => $latitude,
                            'currentLongitude' => $longitude
                        ), array(
                            'id' => $driver_id
                        ));
                        // check trip avilable for driver/any in progress trip before logout
                        $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                            'selectedDriverId' => $driver_id
                        ), 'id DESC');
                        if ($check_trip_avilablity) {
                            if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED && $check_trip_avilablity->tripStatus != Driver_Request_Status_Enum::COMPLETED_TRIP && $check_trip_avilablity->tripStatus != Driver_Request_Status_Enum::DRIVER_REJECTED) {

                                if ($trip_details) {
                                    // update the temp trip tracking info if driver in the trip
                                    $temp_trip_tracking_insert = $this->Trip_Temp_Tracking_Info_Model->insert(array(
                                        'tripId' => $trip_details->id,
                                        'latitude' => $latitude,
                                        'longitude' => $longitude
                                    ));

                                    $message = array(
                                        "message" => $msg_data ['driver_location_update'],
                                        "status" => 1
                                    );
                                }
                            } else {
                                if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {

                                    if ($status == Driver_Available_Status_Enum::BUSY) {
                                        // to get last shift in status of driver before updating the shift out status
                                        $last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                            'driverId' => $trip_details->driverId
                                        ), 'id DESC');

                                        $driver_shift_insert_id = $this->Driver_Shift_History_Model->update(array(
                                            'availabilityStatus' => Driver_Available_Status_Enum::FREE
                                        ), array(
                                            'id' => $last_driver_shift_id->id,
                                            'driverId' => $trip_details->driverId
                                        ));
                                    }

                                    $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                        'selectedDriverId' => 0
                                    ), array(
                                        'tripId' => $trip_id
                                    ));

                                    if ($trip_id) {
                                        $message = array(
                                            "message" => $msg_data ['trip_reject_passenger'],
                                            "status" => 7
                                        );
                                    } else {
                                        $message = array(
                                            "message" => $msg_data ['ok'],
                                            "status" => 12
                                        );
                                    }
                                    // check trip avilable for driver/any in progress trip before logout
                                    $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                        'selectedDriverId' => 0
                                    ), array(
                                        'tripId' => $trip_id
                                    ));
                                } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                                    $message = array(
                                        "message" => $msg_data ['trip_completed'],
                                        "status" => -1
                                    );
                                } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) {
//									if ($status == Driver_Available_Status_Enum::FREE) {
                                    $trip_details = $this->Trip_Details_Model->getById($check_trip_avilablity->tripId);

                                    $mobile = NULL;
                                    $full_name = NULL;
                                    $passenger_details = $this->Passenger_Model->getById($trip_details->passengerId);
                                    if ($passenger_details) {
                                        $mobile = $passenger_details->mobile;
                                        $full_name = $passenger_details->firstName . ' ' . $passenger_details->lastName;
                                    } else {
                                        $user_details = $this->User_Login_Credential_Model->getById($trip_details->createdBy);
                                        $user_personal_details = $this->User_Personal_Details_Model->getOneByKeyValueArray(array('userId' => $user_details->id));
                                        $mobile = $user_personal_details->mobile;
                                        $full_name = $user_details->firstName . ' ' . $user_details->lastName;
                                    }
                                    $access_token = $this->Corporate_Partners_Api_Details_Model->getOneByKeyValueArray(array('companyId' => ZOOMCAR_COMPANYID));
                                    $response_data = array(
                                        "message" => $msg_data ['driver_booking_request'],
                                        "status" => 1,
                                        "passengers_log_id" => $trip_details->id,
                                        "booking_details" => array(
                                            "pickupplace" => $trip_details->pickupLocation,
                                            "dropplace" => $trip_details->id,
                                            "pickup_time" => $trip_details->pickupDatetime,
                                            "driver_id" => $trip_details->driverId,
                                            "passenger_id" => $trip_details->passengerId,
                                            "roundtrip" => "",
                                            "booking_key" => $trip_details->bookingKey,
                                            "passenger_phone" => $mobile,
                                            "cityname" => "",
                                            "distance_away" => "",
                                            "sub_logid" => $trip_details->id,
                                            "drop_latitude" => $trip_details->dropLatitude,
                                            "drop_longitude" => $trip_details->dropLongitude,
                                            "taxi_id" => 0,
                                            "company_id" => $trip_details->companyId,
                                            "pickup_latitude" => $trip_details->pickupLatitude,
                                            "pickup_longitude" => $trip_details->pickupLongitude,
                                            "bookedby" => 0,
                                            "passenger_name" => $full_name,
                                            "profile_image" => "",
                                            "drop" => $trip_details->dropLocation,
                                            "trip_plan" => ($trip_details->tripType == Trip_Type_Enum::ONEWAY_TRIP) ? Trip_Type_Enum::O : (($trip_details->tripType == Trip_Type_Enum::RETURN_TRIP) ? Trip_Type_Enum::R : (($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP) ? Trip_Type_Enum::X : (($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY) ? Trip_Type_Enum::XO : (($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) ? Trip_Type_Enum::XR : ''))))
                                        ),
                                        "estimated_time" => 0,
                                        "notification_time" => 60,
                                        "notes" => $trip_details->id,
                                        "belowspeed_mins" => 0,
                                        "zoomcar_secret_salt" => ($trip_details->companyId == ZOOMCAR_COMPANYID) ? ZOOMCAR_SECRETKEY : '',
                                        "is_zoomcar_trip" => ($trip_details->companyId == ZOOMCAR_COMPANYID) ? 1 : 0,
                                        "zoomcar_access_token" => ($trip_details->companyId == ZOOMCAR_COMPANYID) ? $access_token->accessToken : ''
                                    );
//									}
                                    $message = array(
                                        "message" => $msg_data ['driver_location_update'],
                                        "trip_details" => $response_data,
                                        "status" => 5
                                    );
                                } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_REJECTED) {
                                    if ($trip_id) {
                                        $message = array(
                                            "message" => $msg_data ['trip_dispatcher_cancel'],
                                            "status" => 10
                                        );
                                    } else {
                                        $message = array(
                                            "message" => $msg_data ['ok'],
                                            "status" => 12
                                        );
                                    }
                                    // check trip avilable for driver/any in progress trip before logout
                                    $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                        'selectedDriverId' => 0
                                    ), array(
                                        'tripId' => $trip_id
                                    ));
                                }
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['driver_location_update'],
                                "status" => 1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    public function zoomcar_booking_post()

    {
        $msg_data = $this->config->item('api');
        $zoomcar_api_details = $this->Corporate_Partners_Api_Details_Model->getOneByKeyValueArray(array('companyId' => ZOOMCAR_COMPANYID));
        $secret_key = $zoomcar_api_details->secretKey;
        $trip_details = array();
        $insert_data = array();
        $city_id = 0;

        $post_data = $this->get_post_data();

        $auth_key = $post_data->auth_key;
        $booking_key = $post_data->booking_key;

        $zoom_hash_str = $this->checkNull($booking_key) . '|' . $this->checkNull($secret_key);
        $encrypted_auth_key = base64_encode(sha1($zoom_hash_str, true));
        if ($auth_key == $encrypted_auth_key) {
            $city = $post_data->city;
            $trip_type = $post_data->trip_type;
            $time = $post_data->time;

            if ($city == 'bangalore') {
                $city_id = 2;
            } else if ($city == 'mumbai') {
                $city_id = 1;
            } else if ($city == 'pune') {
                $city_id = 3;
            } else {
                $city_id = 0;
            }

            //Pick up details
            $pickup_latitude = $post_data->pick->lat;
            $pickup_longitude = $post_data->pick->lng;
            $pickup_location_type = $post_data->pick->location_type;
            $pickup_location_name = '';
            if (array_key_exists('name', $post_data->pick)) {
                $pickup_location_name = $post_data->pick->name;
            }

            $pickup_location_address = $post_data->pick->address;

            //Dop details
            $drop_latitude = $post_data->drop->lat;
            $drop_longitude = $post_data->drop->lng;
            $drop_location_type = $post_data->drop->location_type;

            $drop_location_address = $post_data->drop->address;

            //customer details
            $customer_name = $post_data->customer->name;
            $customer_phone = $post_data->customer->phone_number;
            $customer_driving_license = $post_data->customer->driving_license;

            //zoom car contact details
            $zoom_car_fleet_number = $post_data->zoomcar_contact->fleet_number;
            $zoom_car_escalation_number = $post_data->zoomcar_contact->escalation_number;

            //Zoom car details
            $car_make = $post_data->car_details->make;
            $car_model = $post_data->car_details->model;
            $car_reg_no = '';
            if (array_key_exists('license', $post_data->car_details)) {
                $car_reg_no = $post_data->car_details->license;
            }
            $car_transmission_type = $post_data->car_details->type;


            $insert_data = array("passengerId" => '',
                "clientName" => $customer_name,
                "clientPhoneNumber" => $customer_phone,
                "bookingKey" => $booking_key,
                "companyId" => ZOOMCAR_COMPANYID,
                "bookingCityId" => $city_id,
                "pickupLocation" => $pickup_location_address,
                "pickupLatitude" => $pickup_latitude,
                "pickupLongitude" => $pickup_longitude,
                "dropLocation" => $drop_location_address,
                "dropLatitude" => $drop_latitude,
                "dropLongitude" => $drop_longitude,
                "pickupDatetime" => $time,
                //"dropDatetime"=>'',
                "tripStatus" => Trip_Status_Enum::BOOKED,
                "notificationStatus" => Trip_Status_Enum::BOOKED,
                "tripType" => Trip_Type_Enum::ONEWAY_TRIP,
                "paymentMode" => Payment_Mode_Enum::INVOICE,
                "transmissionType" => ($car_transmission_type == 'manual') ? Transmission_Type_Enum::MANUAL : Transmission_Type_Enum::AUTOMATIC,
                "billType" => Bill_Type_Enum::FIXED,
                "bookedFrom" => Register_Type_Enum::OTHERS,
                "carType" => Car_Type_Enum::NONE,
                "vehicleNo" => $car_reg_no,
                "vehicleMake" => $car_make,
                "vehicleModel" => $car_model,
                "createdBy" => 2,
                "updatedBy" => 2
            );

            $trip_details = $this->Trip_Details_Model->insert($insert_data);
            if ($trip_details) {
                $from = SMTP_EMAIL_ID;
                $subject = 'Trip Booking Request From Zoomcar';
                $data = array(
                    'trip_type' => 'ZOOMCAR',
                    'trip_id' => $trip_details,
                    'passenger_name' => $customer_name,
                    'passenger_mobile' => $customer_phone,
                    'booking_key' => $booking_key,
                    'pickup_location' => $pickup_location_address,
                    'pickup_date_time' => $time,
                );
                $message_data = $this->load->view('emailtemplate/booking_confirm', $data, TRUE);

                // Sending email to info@zuver.in on request from Sidhanth on 07/10/2016
                $to_list = array(
                    'info@zuver.in',
                    'sidhanth@zuver.in',
                    'sovin@zuver.in',
                );
                foreach ($to_list as $to) {
                    if (SMTP) {

                        if ($to) {
                            $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                        }
                    } else {

                        // To send HTML mail, the Content-type header must be set
                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        // Additional headers
                        $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                        $headers .= 'To: <' . $to . '>' . "\r\n";
                        if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                            mail($to, $subject, $message_data, $headers);
                        }
                    }
                }

                $message = array("message" => 'Trip has been accepted and driver shall be allotted later', "status" => "success",
                    "data" => array("booking_status" => "pending", "booking_status_message" => "Trip has been accepted and driver shall be allotted later",
                        "booking_key" => $booking_key, "reference_key" => $trip_details));
            } else {
                $message = array("message" => 'Booking the trip has been failed, Please try again later.', "status" => "error");
            }
        } else {
            $message = array("message" => $msg_data ['auth_failed'], "status" => "error");

        }

        echo json_encode($message);
    }

    public function zoomcar_cancel_booking_post()
    {
        $msg_data = $this->config->item('api');
        $zoomcar_api_details = $this->Corporate_Partners_Api_Details_Model->getOneByKeyValueArray(array('companyId' => ZOOMCAR_COMPANYID));
        $secret_key = $zoomcar_api_details->secretKey;

        $post_data = $this->get_post_data();
        $trip_details = array();

        $auth_key = $post_data->auth_key;
        $booking_key = $post_data->booking_key;
        $reference_key = $post_data->reference_key;

        $zoom_hash_str = $this->checkNull($booking_key) . '|' . $this->checkNull($secret_key);
        $encrypted_auth_key = base64_encode(sha1($zoom_hash_str, true));
        if ($auth_key == $encrypted_auth_key) {


            $update_data = array(
                "tripStatus" => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                "notificationStatus" => Trip_Status_Enum::CANCELLED_BY_PASSENGER
            );
            $trip_details = $this->Trip_Details_Model->update($update_data, array('id' => $reference_key));
            if ($trip_details) {
                $message = array("message" => 'Trip has been cancelled upon request', "status" => "success",
                    "data" => array("booking_status" => "cancelled", "booking_status_message" => "Trip has been cancelled upon request",
                        "booking_key" => $booking_key, "reference_key" => $reference_key));
            } else {
                $message = array("message" => 'Trip cancellation failed, Please try again later.', "status" => "error");
            }
        } else {
            $message = array("message" => $msg_data ['auth_failed'], "status" => "error");

        }

        echo json_encode($message);
    }

    public function zoomcar_check_trip_status_post()
    {
        $msg_data = $this->config->item('api');
        $trip_details = NULL;
        $check_trip_avilablity = NULL;
        $trip_status = NULL;
        $message = '';

        $post_data = $this->get_post_data();


        $booking_key = $post_data->booking_key;
        $trip_id = $post_data->vendor_reference_key;
        $trip_status = $post_data->status;
        // check trip avilable for driver/any in progress trip before logout
        $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
            'tripId' => $trip_id
        ), 'id DESC');

        $trip_details = $this->Trip_Details_Model->getById($trip_id);
        if ($check_trip_avilablity) {

            if ($trip_status == 1) {
                if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED && $trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS) {
                    $message = array("status" => "success");
                } else {
                    $message = array("message" => 'Invalid Trip status, Please try again later.', "status" => "failure");
                }
            } else if ($trip_status == 2) {
                if (($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) && ($trip_details->tripStatus == Trip_Status_Enum::TRIP_COMPLETED || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT)) {
                    $message = array("status" => "success");
                } else {
                    $message = array("message" => 'Invalid Trip status, Please try again later.', "status" => "failure");
                }
            }
        } else {
            $message = array("message" => 'Invalid Vendor Reference Key.', "status" => "failure");
        }
        echo json_encode($message);
    }

    public function sendDailyTripDetailsMail_get()
    {
        $report_date = date('Y-m-d', strtotime('-1 days'));
        $excel_path = DownloadTripDetailsAndSummaryExcel('1');
        $filename = substr($excel_path, strrpos($excel_path, '/') + 1);
        $subject = 'Zuver Daily Report';
        $city_list = $this->City_Model->getSelectDropdownOptions(array(
            'status' => 'Y'
        ), 'name');

        $summary_table = '<table style="border:solid 2px #000; border-collapse: collapse;" width="80%">';
        $row1 = '<tr><th rowspan="2" style="border:solid 1px #000;">' . date('jS M, Y', strtotime($report_date)) . '</th>';
        $row2 = '<tr>';
        $row3 = '<tr><th style="border:solid 1px #000;">Completed trips</th>';
        $row4 = '<tr><th style="border:solid 1px #000;">Cancelled trips</th>';
        $row5 = '<tr><th style="border:solid 1px #000;">On going trips</th>';
        $row6 = '<tr><th style="border:solid 1px #000;">Unassigned trips</th>';
        //$row7='<tr><th style="border:solid 1px #000;">Upcoming trips</th>';
        $row8 = '<tr><th style="border:solid 1px #000;">Total Trips</th>';
        $row9 = '<tr><th style="border:solid 1px #000;">Total Drivers utilized</th>';
        $row10 = '<tr><th style="border:solid 1px #000;">Total Driver hours</th>';
        $row11 = '<tr><th style="border:solid 1px #000;">Total Revenue</th>';

        $Overall_B2C_CompletedTrips = $Overall_B2B_CompletedTrips = $Overall_B2C_CancelledTrips = $Overall_B2B_CancelledTrips = $Overall_B2C_OngoingTrips = $Overall_B2B_OngoingTrips = $Overall_B2C_UnassignedTrips = $Overall_B2B_UnassignedTrips = $Overall_B2C_TotalTrips = $Overall_B2B_TotalTrips = $Overall_B2C_totalTripDrivers = $Overall_B2B_totalTripDrivers = $Overall_B2C_totalTripHours = $Overall_B2B_totalTripHours = $Overall_B2C_totalTripCost = $Overall_B2B_totalTripCost = 0;
        foreach ($city_list as $key => $value) {
            if (!empty($key)) {
                $b2c_filter = array('city_id' => $key, 'pickup_date' => $report_date, 'company_id' => 1);
                $b2b_filter = array('city_id' => $key, 'pickup_date' => $report_date);

                $row1 .= '<th colspan="2" style="border:solid 1px #000;">' . $value . '</th>';
                $row2 .= '<th style="border:solid 1px #000;">B2C</th><th style="border:solid 1px #000;">B2B</th>';

                $Overall_B2C_CompletedTrips += $B2C_CompletedTrips = $this->Query_Model->getTripsFilterCount('CompletedTrips', $b2c_filter);
                $Overall_B2B_CompletedTrips += $B2B_CompletedTrips = $this->Query_Model->getTripsFilterCount('CompletedTrips', $b2b_filter);
                $row3 .= '<th style="border:solid 1px #000;">' . $B2C_CompletedTrips . '</th><th style="border:solid 1px #000;">' . $B2B_CompletedTrips . '</th>';

                $Overall_B2C_CancelledTrips += $B2C_CancelledTrips = $this->Query_Model->getTripsFilterCount('CancelledTrips', $b2c_filter);
                $Overall_B2B_CancelledTrips += $B2B_CancelledTrips = $this->Query_Model->getTripsFilterCount('CancelledTrips', $b2b_filter);
                $row4 .= '<th style="border:solid 1px #000;">' . $B2C_CancelledTrips . '</th><th style="border:solid 1px #000;">' . $B2B_CancelledTrips . '</th>';

                $Overall_B2C_OngoingTrips += $B2C_OngoingTrips = $this->Query_Model->getTripsFilterCount('OngoingTrips', $b2c_filter);
                $Overall_B2B_OngoingTrips += $B2B_OngoingTrips = $this->Query_Model->getTripsFilterCount('OngoingTrips', $b2b_filter);
                $row5 .= '<th style="border:solid 1px #000;">' . $B2C_OngoingTrips . '</th><th style="border:solid 1px #000;">' . $B2B_OngoingTrips . '</th>';

                $Overall_B2C_UnassignedTrips += $B2C_UnassignedTrips = $this->Query_Model->getTripsFilterCount('UnassignedTrips', $b2c_filter);
                $Overall_B2B_UnassignedTrips += $B2B_UnassignedTrips = $this->Query_Model->getTripsFilterCount('UnassignedTrips', $b2b_filter);
                $row6 .= '<th style="border:solid 1px #000;">' . $B2C_UnassignedTrips . '</th><th style="border:solid 1px #000;">' . $B2B_UnassignedTrips . '</th>';

                /*$B2C_UpcomingTrips= $this->Query_Model->getTripsFilterCount('UpcomingTrips',$b2c_filter);
				$B2B_UpcomingTrips= $this->Query_Model->getTripsFilterCount('UpcomingTrips',$b2b_filter);
				$row7.='<th style="border:solid 1px #000;">'.$B2C_UpcomingTrips.'</th><th style="border:solid 1px #000;">'.$B2B_UpcomingTrips.'</th>';*/

                $Overall_B2C_TotalTrips += $B2C_TotalTrips = $this->Query_Model->getTripsFilterCount('TotalTrips', $b2c_filter);
                $Overall_B2B_TotalTrips += $B2B_TotalTrips = $this->Query_Model->getTripsFilterCount('TotalTrips', $b2b_filter);
                $row8 .= '<th style="border:solid 1px #000;">' . $B2C_TotalTrips . '</th><th style="border:solid 1px #000;">' . $B2B_TotalTrips . '</th>';

                $B2C_totalTripCollection = $B2B_totalTripCollection = '';
                $B2C_totalTripCollection = $this->Query_Model->getTripsCollectionFilterCount($b2c_filter);
                $B2B_totalTripCollection = $this->Query_Model->getTripsCollectionFilterCount($b2b_filter);

                $Overall_B2C_totalTripDrivers += ($B2C_totalTripCollection ? $B2C_totalTripCollection->totalTripDrivers : 0);
                $Overall_B2B_totalTripDrivers += ($B2B_totalTripCollection ? $B2B_totalTripCollection->totalTripDrivers : 0);
                $Overall_B2C_totalTripHours += ($B2C_totalTripCollection ? $B2C_totalTripCollection->totalTripHours : 0);
                $Overall_B2B_totalTripHours += ($B2B_totalTripCollection ? $B2B_totalTripCollection->totalTripHours : 0);
                $Overall_B2C_totalTripCost += ($B2C_totalTripCollection ? $B2C_totalTripCollection->totalTripCost : 0);
                $Overall_B2B_totalTripCost += ($B2B_totalTripCollection ? $B2B_totalTripCollection->totalTripCost : 0);

                $row9 .= '<th style="border:s=olid 1px #000;">' . ($B2C_totalTripCollection ? $B2C_totalTripCollection->totalTripDrivers : 0) . '</th><th style="border:solid 1px #000;">' . ($B2B_totalTripCollection ? $B2B_totalTripCollection->totalTripDrivers : 0) . '</th>';
                $row10 .= '<th style="border:solid 1px #000;">' . ($B2C_totalTripCollection ? $B2C_totalTripCollection->totalTripHours : 0) . '</th><th style="border:solid 1px #000;">' . ($B2B_totalTripCollection ? $B2B_totalTripCollection->totalTripHours : 0) . '</th>';
                $row11 .= '<th style="border:solid 1px #000;">' . ($B2C_totalTripCollection ? $B2C_totalTripCollection->totalTripCost : 0) . '</th><th style="border:solid 1px #000;">' . ($B2B_totalTripCollection ? $B2B_totalTripCollection->totalTripCost : 0) . '</th>';
            }

        }

        /*Overall cities total */

        $row1 .= '<th colspan="2" style="border:solid 1px #000;">Total</th>';
        $row2 .= '<th style="border:solid 1px #000;">B2C</th><th style="border:solid 1px #000;">B2B</th>';
        $row3 .= '<th style="border:solid 1px #000;">' . $Overall_B2C_CompletedTrips . '</th><th style="border:solid 1px #000;">' . $Overall_B2B_CompletedTrips . '</th>';
        $row4 .= '<th style="border:solid 1px #000;">' . $Overall_B2C_CancelledTrips . '</th><th style="border:solid 1px #000;">' . $Overall_B2B_CancelledTrips . '</th>';
        $row5 .= '<th style="border:solid 1px #000;">' . $Overall_B2C_OngoingTrips . '</th><th style="border:solid 1px #000;">' . $Overall_B2B_OngoingTrips . '</th>';
        $row6 .= '<th style="border:solid 1px #000;">' . $Overall_B2C_UnassignedTrips . '</th><th style="border:solid 1px #000;">' . $Overall_B2B_UnassignedTrips . '</th>';
        //$row7.='<th style="border:solid 1px #000;">'.$B2C_UpcomingTrips.'</th><th style="border:solid 1px #000;">'.$B2B_UpcomingTrips.'</th>';
        $row8 .= '<th style="border:solid 1px #000;">' . $Overall_B2C_TotalTrips . '</th><th style="border:solid 1px #000;">' . $Overall_B2B_TotalTrips . '</th>';
        $row9 .= '<th style="border:s=olid 1px #000;">' . $Overall_B2C_totalTripDrivers . '</th><th style="border:solid 1px #000;">' . $Overall_B2B_totalTripDrivers . '</th>';
        $row10 .= '<th style="border:solid 1px #000;">' . $Overall_B2C_totalTripHours . '</th><th style="border:solid 1px #000;">' . $Overall_B2B_totalTripHours . '</th>';
        $row11 .= '<th style="border:solid 1px #000;">' . $Overall_B2C_totalTripCost . '</th><th style="border:solid 1px #000;">' . $Overall_B2B_totalTripCost . '</th>';

        /**End Overall Total**/

        $row1 .= '</tr>';
        $row2 .= '</tr>';
        $row3 .= '</tr>';
        $row4 .= '</tr>';
        $row5 .= '</tr>';
        $row6 .= '</tr>';
        //$row7.='</tr>';
        $row8 .= '</tr>';
        $row9 .= '</tr>';
        $row10 .= '</tr>';
        $row11 .= '</tr>';

        $summary_table .= $row1 . $row2 . $row3 . $row4 . $row5 . $row6 . $row8 . $row9 . $row10 . $row11;
        $summary_table . '</table>';

        //echo $summary_table; die();

        $to = 'sidhanth@zuver.in';
        $cc = array(array('name' => 'Sovin Hegde', 'email' => 'sovin@zuver.in'));
        $bcc = array(array('name' => 'Rupin Kirani', 'email' => 'rupin@tailoredtech.in'), array('name' => 'Aakash Kejriwal', 'email' => 'aakash@tailoredtech.in'));
        $attachment = array('file_path' => getcwd() . '/' . $excel_path, 'file_name' => $filename);
        $result = $this->Api_Webservice_Model->sendEmail($to, $subject, $summary_table, $cc, $bcc, $attachment);
        echo $result;
    }

    private function calculate_driver_earning($trip_id, $driver_id)
    {
        $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id);
        $driver_details = $this->Driver_Model->getById($driver_id);

        if (count($trip_details) > 0) {
            $trip_details = $this->Trip_Details_Model->getById($trip_id);
            $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id, $trip_details->passengerId);
            $trip_details = $trip_details[0];
        }

        if ($trip_details->companyId == DEFAULT_COMPANY_ID || $driver_details->driverType == 'B2C') {
            if ($trip_details->tripType == 'XO' || $trip_details->tripType == 'XR') {
                $this_trip_type = 'X';
            } else {
                $this_trip_type = $trip_details->tripType;
            }
        } else {
            $this_trip_type = 'B2B';
        }

        if ($driver_details->driverType == 'B2C') {
            $driver_rate_card = $this->Driver_Rate_Card_Details_Model->getByKeyValueArray(array('tripType' => array($this_trip_type, 'ALL'), 'driverType' => $driver_details->driverType, 'rateFor!=' => 'Day'));
        } else {
            $driver_rate_card = $this->Driver_Rate_Card_Details_Model->getByKeyValueArray(array('driverType' => $driver_details->driverType, 'rateFor!=' => 'Day'));
        }

        $driver_productivity_allowance = array();
        if ($driver_details->driverType == 'TBD') {
            $driver_productivity_allowance = $this->Driver_Productivity_Allowance_Model->getByKeyValueArray(array('driverId' => $driver_details->id), 'extraRateForMin Desc');
        }
        $all_sub_trips = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId' => $trip_id));
        if ($all_sub_trips && $driver_details->driverType == 'TBD') {
            $total_earning = 0;
            $sub_trip_count = 1;
            foreach ($all_sub_trips as $sub_trip) {
                $earnings = array();
                foreach ($driver_rate_card as $earningtype_rate_card) {
                    if (!array_key_exists($earningtype_rate_card->earningType, $earnings)) {
                        $earnings[$earningtype_rate_card->earningType] = 0;
                    }
                    $earning_date = date('Y-m-d', strtotime($sub_trip->dropDatetime));
                    //Check on Earning limit
                    if ($earningtype_rate_card->rateForLimit) {
                        if ($earningtype_rate_card->rateFor == 'Trip') {
                            $driver_total_trips = 0;
                            //For first limited trips for a current month
                            if ($earningtype_rate_card->limitFor == 'M') {
                                $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, $driver_details->driverType, 'CompletedTrips', array('trip_completed_month' => $earning_date)) + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_month' => $earning_date)) + $sub_trip_count;
                            }
                            if ($earningtype_rate_card->limitFor == 'D') {
                                $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $sub_trip_count;
                            }
                            if ($driver_total_trips && $driver_total_trips >= $earningtype_rate_card->extraRateForMin && ($driver_total_trips <= (int)($earningtype_rate_card->rateForLimit + $earningtype_rate_card->extraRateForMin))) {
                                $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                            }
                        }
                    } else {
                        if ($earningtype_rate_card->earningType == 'NE') {
                            $pick_up_time = date("H:i a", strtotime($sub_trip->pickupDatetime));
                            $drop_time = date("H:i a", strtotime($sub_trip->dropDatetime));

                            if (($pick_up_time >= date("H:i:s", strtotime(NIGHT_START_TIME)) || $pick_up_time <= date("H:i a", strtotime(NIGHT_END_TIME))) || ($drop_time >= date("H:i a", strtotime(NIGHT_START_TIME)) || $drop_time <= date("H:i a", strtotime(NIGHT_END_TIME)))) {
                                $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                            }
                        } else {
                            $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                        }
                    }

                    $extra_earning = 0;
                    //Check for Extra Rate on earnings
                    if ($earningtype_rate_card->extraRate) {
                        /*if($earningtype_rate_card->extraRateFor=='Km')
                        {
                            if($sub_trip->travelDistance>=$earningtype_rate_card->extraRateForMin)
                            {
                                $extra_earning=round(($sub_trip->travelDistance-$earningtype_rate_card->extraRateForMin)*$earningtype_rate_card->extraRate);
                            }
                        }*/

                        if ($earningtype_rate_card->extraRateFor == 'Hr') {
                            if ($earningtype_rate_card->limitFor == 'T') {
                                $date_pickup = new DateTime($sub_trip->pickupDatetime);
                                $date_drop = new DateTime($sub_trip->dropDatetime);
                                $driver_total_trip_hours = date_diff($date_drop, $date_pickup);
                                $driver_total_trip_hours->format('%h.%i');
                                // $driver_total_trip_hours = $sub_trip->travelHoursMinutes;
                            }

                            if ($driver_total_trip_hours && $driver_total_trip_hours > $earningtype_rate_card->extraRateForMin) {
                                $exceeded_hours = $driver_total_trip_hours - $earningtype_rate_card->extraRateForMin;
                                if ($exceeded_hours < $driver_total_trip_hours) {
                                    $extra_earning = round($exceeded_hours * $earningtype_rate_card->extraRate);
                                } else {
                                    $extra_earning = round($driver_total_trip_hours * $earningtype_rate_card->extraRate);
                                }
                            }
                        }
                    }
                    $earnings[$earningtype_rate_card->earningType] = $earnings[$earningtype_rate_card->earningType] + $extra_earning;
                }

                if ($driver_productivity_allowance) {
                    foreach ($driver_productivity_allowance as $driver_productivity_allowance_data) {

                        if (!array_key_exists($driver_productivity_allowance_data->earningType, $earnings)) {
                            $earning_date = date('Y-m-d', strtotime($sub_trip->dropDatetime));
                            if ($driver_productivity_allowance_data->extraRate) {

                                if ($driver_productivity_allowance_data->extraRateFor == 'Trip') {
                                    $driver_total_trips = 0;
                                    $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $sub_trip_count;
                                    if ($driver_total_trips && $driver_total_trips >= $driver_productivity_allowance_data->extraRateForMin) {
                                        $earnings[$driver_productivity_allowance_data->earningType] = $driver_productivity_allowance_data->extraRate;
                                    }
                                }
                            }
                        }

                    }
                }

                $earning_history = array();
                foreach ($earnings as $key => $value) {
                    $earning_data = array('driverId' => $driver_id,
                        'tripId' => $sub_trip->subTripId,
                        'tripType' => $trip_details->tripType,
                        'driverType' => $driver_details->driverType,
                        'earningType' => $key,
                        'earningAmount' => $value,
                        'earningDate' => date('Y-m-d H:i:s', strtotime($sub_trip->dropDatetime))
                    );
                    $earning_history[] = $earning_data;
                }

                $this->Driver_Model->save_driver_earnings($driver_id, $sub_trip->subTripId, $earning_history);
                $total_sub_trips = $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $sub_trip_count;
                $driver_daily_earning = $this->driver_daily_earning($sub_trip->dropDatetime, $driver_id, $total_sub_trips);
                $total_earning = $total_earning + array_sum($earnings);
                $sub_trip_count++;
            }
            return $total_earning;
        } else {
            $earnings = array();
            foreach ($driver_rate_card as $earningtype_rate_card) {
                if (!array_key_exists($earningtype_rate_card->earningType, $earnings)) {
                    $earnings[$earningtype_rate_card->earningType] = 0;
                }
                $earning_date = date('Y-m-d', strtotime($trip_details->dropDatetime));
                //Check on Earning limit
                if ($earningtype_rate_card->rateForLimit) {
                    if ($earningtype_rate_card->rateFor == 'Trip') {
                        $driver_total_trips = 0;
                        //For first limited trips for a current month
                        if ($earningtype_rate_card->limitFor == 'M') {
                            $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_month' => $earning_date));
                            if ($driver_details->driverType == 'TBD') {
                                $driver_total_trips = $driver_total_trips + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_month' => $earning_date));
                            }
                        }
                        if ($earningtype_rate_card->limitFor == 'D') {
                            $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date));
                            if ($driver_details->driverType == 'TBD') {
                                $driver_total_trips = $driver_total_trips + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));
                            }
                        }
                        if ($driver_total_trips && $driver_total_trips >= $earningtype_rate_card->extraRateForMin && ($driver_total_trips <= (int)($earningtype_rate_card->rateForLimit + $earningtype_rate_card->extraRateForMin))) {
                            $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                        }
                    }
                } else {
                    if ($earningtype_rate_card->earningType == 'NE') {
                        $pick_up_time = date("H:i a", strtotime($trip_details->actualPickupDatetime));
                        $drop_time = date("H:i a", strtotime($trip_details->dropDatetime));

                        if (($pick_up_time >= date("H:i:s", strtotime(NIGHT_START_TIME)) || $pick_up_time <= date("H:i a", strtotime(NIGHT_END_TIME))) || ($drop_time >= date("H:i a", strtotime(NIGHT_START_TIME)) || $drop_time <= date("H:i a", strtotime(NIGHT_END_TIME)))) {
                            $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                        }
                    } else {
                        if ($earningtype_rate_card->rateFor == 'Hr') {
                            $earnings[$earningtype_rate_card->earningType] = round($trip_details->travelHoursMinutes * $earningtype_rate_card->baseRate);
                        } else {
                            $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                        }
                    }
                }

                $extra_earning = 0;
                //Check for Extra Rate on earnings
                if ($earningtype_rate_card->extraRate) {
                    if ($earningtype_rate_card->extraRateFor == 'Km') {
                        if ($trip_details->travelDistance >= $earningtype_rate_card->extraRateForMin) {
                            $extra_earning = round(($trip_details->travelDistance - $earningtype_rate_card->extraRateForMin) * $earningtype_rate_card->extraRate);
                        }
                    }
                    if ($earningtype_rate_card->extraRateFor == 'Hr') {
                        if ($earningtype_rate_card->limitFor == 'M') {
                            $driver_total_hour_distance = $this->Driver_Model->get_driver_trip_hours_distance($driver_id, 'CompletedTrips', array('trip_completed_month' => $earning_date));
                            $driver_total_trip_hours = $driver_total_hour_distance->totalTripHours;
                        }

                        if ($earningtype_rate_card->limitFor == 'D') {
                            $driver_MainTrip_total_hour_distance = $this->Driver_Model->get_driver_trip_hours_distance($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));

                            $driver_SubTrip_total_hour_distance = $this->Driver_Model->get_driver_sub_trip_hours_distance($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));

                            $driver_total_trip_hours = $driver_MainTrip_total_hour_distance->totalTripHours + $driver_SubTrip_total_hour_distance->totalTripHours;
                        }

                        if ($earningtype_rate_card->limitFor == 'T') {
                            $driver_total_trip_hours = $trip_details->travelHoursMinutes;
                        }

                        if ($driver_total_trip_hours && $driver_total_trip_hours > $earningtype_rate_card->extraRateForMin) {
                            $exceeded_hours = $driver_total_trip_hours - $earningtype_rate_card->extraRateForMin;
                            if ($exceeded_hours < $trip_details->travelHoursMinutes) {
                                $extra_earning = round($exceeded_hours * $earningtype_rate_card->extraRate);
                            } else {
                                $extra_earning = round($trip_details->travelHoursMinutes * $earningtype_rate_card->extraRate);
                            }
                        }
                    }
                }
                $earnings[$earningtype_rate_card->earningType] = $earnings[$earningtype_rate_card->earningType] + $extra_earning;
            }

            if ($driver_productivity_allowance) {
                foreach ($driver_productivity_allowance as $driver_productivity_allowance_data) {
                    if (!array_key_exists($driver_productivity_allowance_data->earningType, $earnings)) {
                        $earning_date = date('Y-m-d', strtotime($trip_details->dropDatetime));
                        if ($driver_productivity_allowance_data->extraRate) {
                            if ($driver_productivity_allowance_data->extraRateFor == 'Trip') {
                                $driver_total_trips = 0;
                                $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));
                                if ($driver_total_trips && $driver_total_trips >= $driver_productivity_allowance_data->extraRateForMin) {
                                    $earnings[$driver_productivity_allowance_data->earningType] = $driver_productivity_allowance_data->extraRate;
                                }
                            }
                        }
                    }

                }
            }

            $earning_history = array();
            foreach ($earnings as $key => $value) {
                $earning_data = array('driverId' => $driver_id,
                    'tripId' => $trip_id,
                    'tripType' => $trip_details->tripType,
                    'driverType' => $driver_details->driverType,
                    'earningType' => $key,
                    'earningAmount' => $value,
                    'earningDate' => date('Y-m-d H:i:s', strtotime($trip_details->dropDatetime))
                );
                $earning_history[] = $earning_data;
            }

            $this->Driver_Model->save_driver_earnings($driver_id, $trip_id, $earning_history);
            $total_sub_trips = $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));
            $driver_daily_earning = $this->driver_daily_earning($trip_details->dropDatetime, $driver_id, $total_sub_trips);
            return array_sum($earnings);
        }
    }

    private function driver_daily_earning($earning_date, $driver_id, $sub_trips_count = 0)
    {
        $earning_history = array();
        $earning_date = date('Y-m-d', strtotime($earning_date));
        $driver_data = $this->Driver_Model->getById($driver_id);
        $driver_daily_rate_card = $this->Driver_Rate_Card_Details_Model->getByKeyValueArray(array('driverType' => $driver_data->driverType, 'rateFor' => 'Day'), 'extraRateForMin Desc');
        foreach ($driver_daily_rate_card as $driver_daily_rate_card_details) {
            if ($driver_daily_rate_card_details->extraRateFor == 'Trip') {
                $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_data->id, $driver_data->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $sub_trips_count;
                if ($driver_total_trips) {
                    if ($driver_total_trips == $driver_daily_rate_card_details->extraRateForMin) {
                        $earning_data = array('driverId' => $driver_data->id,
                            'tripId' => 0,
                            'tripType' => 'NA',
                            'driverType' => $driver_data->driverType,
                            'earningType' => $driver_daily_rate_card_details->earningType,
                            'earningAmount' => $driver_daily_rate_card_details->baseRate,
                            'earningDate' => date('Y-m-d H:i:s', strtotime($earning_date))
                        );
                        $earning_history[$driver_daily_rate_card_details->earningType] = $earning_data;
                        if ($earning_history) {
                            $this->Driver_Model->save_driver_daily_earnings($earning_history);
                        }
                    }
                }
            }
        }
    }

    public function driver_login_incentive_get()
    {
        $logged_date = date('Y-m-d', strtotime('-1 days'));
        //$logged_date= date('Y-m-d', strtotime('2017-03-06'));
        $logged_day = date('w', strtotime($logged_date));
        $earning_history = array();
        //get login incentive data for current week day **/
        $login_incentive_list = $this->Driver_Login_Incentive_Model->getByKeyValueArray(array('day' => $logged_day));
        foreach ($login_incentive_list as $login_incentive_data) {
            $incentive_start_time = $logged_date . ' ' . $login_incentive_data->start_time;
            if ($login_incentive_data->shift == 'Night') {
                $incentive_end_time = date('Y-m-d') . ' ' . $login_incentive_data->end_time;
            } else {
                $incentive_end_time = $logged_date . ' ' . $login_incentive_data->end_time;
            }
            /**Get drivers who logged in before incentive time starts **/
            $get_logged_drivers = $this->Driver_Model->get_logged_drivers_by_date($incentive_start_time, $login_incentive_data->driverType);
            //echo $this->db->last_query();
            foreach ($get_logged_drivers as $logged_driver) {
                /**check whether driver logged out between incentive start and end time **/
                $logout_entries = $this->Driver_Model->get_drivers_logout($logged_driver->driverId, $incentive_start_time, $incentive_end_time);
                //echo $this->db->last_query();
                //print_r($logout_entries);

                /**Check if driver rejected any trip in current shift**/
                $driver_rejected_count_for_shift = $this->Driver_Model->get_driver_rejected_trips($logged_driver->driverId, $incentive_start_time, $incentive_end_time);
                //echo $this->db->last_query();

                $time_diff = '<=2';
                /**Check if driver rejected any trip for today and last 2 days before 2hours or less of the trip start time **/
                $driver_rejected_count_past = $this->Driver_Model->get_driver_rejected_trips($logged_driver->driverId, date('Y-m-d', strtotime('-2 days', strtotime($logged_date))) . ' 00:00:01', $logged_date . ' 23:59:59', $time_diff);
                //echo $this->db->last_query();
                /**Check if driver rejected any trip for today and last 2 days **/
                /*$driver_rejected_count = $this->Driver_Rejected_Trip_Details_Model->getColumnByKeyValueArray ( 'driverId', array (
									'driverId' => $logged_driver->driverId,
									'createdDate >=' => date('Y-m-d', strtotime('-2 days', strtotime($logged_date))).' 00:00:01',
									'createdDate <=' => $logged_date.' 23:59:59'
							) );*/
                //echo $this->db->last_query();

                if (!$logout_entries && !$driver_rejected_count_past && !$driver_rejected_count_for_shift) {
                    $earning_data = array('driverId' => $logged_driver->driverId,
                        'tripId' => 0,
                        'tripType' => 'NA',
                        'driverType' => $logged_driver->driverType,
                        'earningType' => 'Login Incentive - ' . $login_incentive_data->shift,
                        'earningAmount' => $login_incentive_data->rate,
                        'addedDate' => date('Y-m-d H:s', strtotime($logged_date)) . ':01'
                    );
                    $earning_history['earningType'] = $earning_data;
                }
            }
        }
        //print_r($earning_history); die();
        if ($earning_history) {
            $this->Driver_Model->save_driver_earnings(0, 0, $earning_history);
        }
    }

    /**
     * driver started the sub trip api
     */
    public function start_sub_trip_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $driver_id = NULL;
        $trip_id = NULL;
        $latitude = NULL;
        $longitude = NULL;
        $status = NULL;
        $location = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $trip_id = $post_data->trip_id;
        $booking_key = $post_data->booking_key;
        $latitude = $post_data->latitude;
        $longitude = $post_data->longitude;
        $meterStart = $post_data->meterStart;

        if (isset($post_data->location)) {
            $location = $post_data->location;
        }

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($driver_id) {
                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {
                        // get the trip details
                        $trip_details = $this->Trip_Details_Model->getById($trip_id);
                        if ($trip_details) {
                            // check trip avilable for driver/any in progress trip before logout
                            $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                'tripId' => $trip_id
                            ));
                            if ($check_trip_avilablity) {
                                if ($trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS) {
                                    $subTripId = $trip_id . '-A';
                                    // inserting the start trip location of driver into temp tracking table for particular trip
                                    $insert_data = array(
                                        'masterTripId' => $trip_id,
                                        'pickupLocation' => $location,
                                        'pickupLatitude' => $latitude,
                                        'pickupLongitude' => $longitude,
                                        'pickupDatetime' => date('Y-m-d H:i:s'),
                                        'meterStart' => $meterStart,
                                        'bookingKey' => $booking_key
                                    );
                                    $sub_trip_list = $this->Sub_Trip_Details_Model->getOneByKeyValue('masterTripId', $trip_id, 'subTripId Desc');
                                    if ($sub_trip_list) {
                                        $previous_subTripId = $sub_trip_list->subTripId;
                                        ++$previous_subTripId;
                                        $subTripId = $previous_subTripId;
                                    }

                                    if ($sub_trip_list && $sub_trip_list->dropDatetime == '0000-00-00 00:00:00') {
                                        $message = array(
                                            "message" => $msg_data ['exist_running_subtrip'],
                                            "status" => 102
                                        );
                                    } else {
                                        $insert_data['subTripId'] = $subTripId;
                                        $sub_trip_insert = $this->Sub_Trip_Details_Model->insert($insert_data);

                                        $sub_trip_data = $this->Sub_Trip_Details_Model->getOneByKeyValueArray(array('id' => $sub_trip_insert));
                                        $sub_trip_details = array(
                                            'sub_trip_id' => $sub_trip_data->subTripId,
                                            'pickup_location' => $sub_trip_data->pickupLocation,
                                            'pickup_time' => $sub_trip_data->pickupDatetime,
                                            'pickup_latitude' => $sub_trip_data->pickupLatitude,
                                            'pickup_longitude' => $sub_trip_data->pickupLongitude,
                                            'drop_location' => $sub_trip_data->dropLocation,
                                            'drop_latitude' => $sub_trip_data->dropLatitude,
                                            'drop_longitude' => $sub_trip_data->dropLongitude,
                                            'drop_time' => $sub_trip_data->dropDatetime,
                                            'meter_start' => $sub_trip_data->meterStart,
                                            'meter_end' => $sub_trip_data->meterEnd,
                                            'booking_key' => $sub_trip_data->bookingKey
                                        );
                                        $message = array(
                                            "message" => $msg_data ['driver_start_sub_trip'],
                                            "subTripId" => $subTripId,
                                            "status" => 1,
                                            "sub_trip_details" => $sub_trip_details
                                        );
                                    }
                                } else {
                                    $message = array(
                                        "message" => $msg_data ['invalid_trip_status'],
                                        "status" => -1
                                    );
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['invalid_trip_request'],
                                    "status" => -1
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_trip'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * driver started the sub trip api
     */
    public function end_sub_trip_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $driver_id = NULL;
        $trip_id = NULL;
        $latitude = NULL;
        $longitude = NULL;
        $status = NULL;
        $location = NUll;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $trip_id = $post_data->trip_id;
        $sub_trip_id = $post_data->sub_trip_id;
        $booking_key = $post_data->booking_key;
        $latitude = $post_data->latitude;
        $longitude = $post_data->longitude;
        $meterEnd = $post_data->meterEnd;
        $meterStart = $post_data->meterStart;

        if (isset($post_data->location)) {
            $location = $post_data->location;
        }

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality
                if ($driver_id) {
                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $driver_id,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));
                    if ($driver_logged_status) {
                        // get the trip details
                        $trip_details = $this->Trip_Details_Model->getById($trip_id);
                        if ($trip_details) {
                            // check trip avilable for driver/any in progress trip before logout
                            $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                                'tripId' => $trip_id
                            ));
                            if ($check_trip_avilablity) {
                                if ($trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS) {
                                    // updating the start trip location of driver into sub trip table for particular trip
                                    $update_data = array(
                                        'dropLocation' => $location,
                                        'dropLatitude' => $latitude,
                                        'dropLongitude' => $longitude,
                                        'dropDatetime' => date('Y-m-d H:i:s'),
                                        'meterStart' => $meterStart,
                                        'meterEnd' => $meterEnd,
                                        'bookingKey' => $booking_key
                                    );
                                    $sub_trip_update = $this->Sub_Trip_Details_Model->update($update_data, array('masterTripId' => $trip_id, 'subTripId' => $sub_trip_id));

                                    $sub_trip_data = $this->Sub_Trip_Details_Model->getOneByKeyValueArray(array('masterTripId' => $trip_id, 'subTripId' => $sub_trip_id));
                                    $sub_trip_details = array(
                                        'sub_trip_id' => $sub_trip_data->subTripId,
                                        'pickup_location' => $sub_trip_data->pickupLocation,
                                        'pickup_time' => $sub_trip_data->pickupDatetime,
                                        'pickup_latitude' => $sub_trip_data->pickupLatitude,
                                        'pickup_longitude' => $sub_trip_data->pickupLongitude,
                                        'drop_location' => $sub_trip_data->dropLocation,
                                        'drop_latitude' => $sub_trip_data->dropLatitude,
                                        'drop_longitude' => $sub_trip_data->dropLongitude,
                                        'drop_time' => $sub_trip_data->dropDatetime,
                                        'meter_start' => $sub_trip_data->meterStart,
                                        'meter_end' => $sub_trip_data->meterEnd,
                                        'booking_key' => $sub_trip_data->bookingKey
                                    );
                                    if ($sub_trip_update) {
                                        $message = array(
                                            "message" => $msg_data ['driver_end_sub_trip'],
                                            "status" => 1,
                                            "sub_trip_details" => $sub_trip_details
                                        );
                                    } else {
                                        $message = array(
                                            "message" => $msg_data ['invalid_subtrip'],
                                            "status" => -1
                                        );
                                    }
                                } else {
                                    $message = array(
                                        "message" => $msg_data ['invalid_trip_status'],
                                        "status" => -1
                                    );
                                }
                            } else {
                                $message = array(
                                    "message" => $msg_data ['invalid_trip_request'],
                                    "status" => -1
                                );
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data ['invalid_trip'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data ['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }

    /**
     * driver started the sub trip api
     */
    public function get_sub_trips_post()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_id = NULL;
        $trip_id = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->get_post_data();
        foreach (getallheaders() as $name => $value) {
            if ($name == 'authKey') {
                $auth_key = $value;
            }
            if ($name == 'userId') {
                $auth_user_id = $value;
            }
            if ($name == 'userType') {
                $auth_user_type = $value;
            }
        }

        $driver_id = $post_data->driver_id;
        $trip_id = $post_data->trip_id;
        $sub_trips = array();

        // Checked wheather authentication key availablilty for specfic user(passenger/driver)
        $data_auth_key = $this->Auth_Key_Model->getOneByKeyValueArray(array(
            'key' => $auth_key,
            'userId' => $auth_user_id,
            'userType' => $auth_user_type
        ), 'id');
        if ($data_auth_key) {
            if ($data_auth_key->key == $auth_key) {
                // actual functionality

                if ($driver_id) {

                    $sub_trip_list = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId' => $trip_id), 'id DESC');
                    if ($sub_trip_list) {
                        $has_sub_trips = 1;
                        foreach ($sub_trip_list as $sub_trip_data) {
                            $sub_trips[] = array(
                                'sub_trip_id' => $sub_trip_data->subTripId,
                                'pickup_location' => $sub_trip_data->pickupLocation,
                                'pickup_time' => $sub_trip_data->pickupDatetime,
                                'pickup_latitude' => $sub_trip_data->pickupLatitude,
                                'pickup_longitude' => $sub_trip_data->pickupLongitude,
                                'drop_location' => $sub_trip_data->dropLocation,
                                'drop_latitude' => $sub_trip_data->dropLatitude,
                                'drop_longitude' => $sub_trip_data->dropLongitude,
                                'drop_time' => $sub_trip_data->dropDatetime,
                                'meter_start' => $sub_trip_data->meterStart,
                                'meter_end' => $sub_trip_data->meterEnd,
                                'booking_key' => $sub_trip_data->bookingKey
                            );
                        }
                    }
                    if (count($sub_trip_list) > 0) {
                        $message = array(
                            "message" => $msg_data ['success'],
                            "sub_trips" => $sub_trips,
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data ['data_not_found'],
                            "status" => 0
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['driver_login_failed'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['auth_failed'],
                    "status" => 401
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['auth_failed'],
                "status" => 401
            );
        }
        echo json_encode($message);
    }
}
