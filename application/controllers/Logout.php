<?php
class Logout extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
	}
	public function index() {
		// $data = array();
		$this->addJs ( 'app/logout.js' );
		
		// $this->render("driver/add_edit_driver", $data);
	}
	public function signOut() {
		$this->session->unset_userdata ( 'user_id' );
		$this->session->unset_userdata ( 'user_email' );
		$this->session->unset_userdata ( 'role_type' );
		$this->session->unset_userdata ( 'user_name' );
		$this->session->unset_userdata ( 'user_city' );
		$this->session->unset_userdata ( 'user_company' );
		$this->session->unset_userdata ( 'user_profile_image' );
		redirect ( 'login' );
	}
}