<?php
class Login extends My_Controller {
	function __construct() {
		parent::__construct ();
		
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Api_Webservice_Model' );
		$this->load->model ( 'User_Login_Credential_Model' );
		//$this->checkUserIsLogged ();
	}
	public function index() {
		// $data = array();
		$this->addJs ( 'app/login.js' );
		$this->load->view ( 'login/login' );
		// $this- >render("driver/add_edit_driver", $data);
	}
	public function validationLogin() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'name',
						'label' => 'Country Name',
						'rules' => 'trim|required|alpha_numeric' 
				),
				array (
						'field' => 'password',
						'label' => 'ISO Code',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function userLogin() {
		$internal_email = $this->input->post ( 'username' );
		$password = $this->input->post ( 'password' );
		$encpassword = hash ( "sha256", $password );
		$result = $this->User_Login_Credential_Model->getOneByKeyValueArray ( array (
				'internalEmail' => $internal_email,
				'password' => $encpassword,
				'status' => Status_Type_Enum::ACTIVE
		), 'id' );
		if ($result) {
			$id = $result->id;
			$internal_email = $result->internalEmail;
			$role_type = $result->roleType;
			$user_name = $result->firstName . ' ' . $result->lastName;
			$user_city=$result->cityId;
			$user_company=$result->companyId;
			$user_profile_image=$result->profileImage;
			$this->session->set_userdata ( 'user_id', $id );
			$this->session->set_userdata ( 'user_email', $internal_email );
			$this->session->set_userdata ( 'role_type', $role_type );
			$this->session->set_userdata ( 'user_name', $user_name );
			$this->session->set_userdata ( 'user_city', $user_city );
			$this->session->set_userdata ( 'user_company', $user_company );
			$this->session->set_userdata ( 'user_profile_image', $user_profile_image );
			redirect ( 'dashboard' );
		} else {
			$this->session->set_flashdata ( 'login_err', 'Invalid username and password' );
			redirect ( 'login' );
		}
	}
	
	public function resetPassword()
	{
		$internal_email = $this->input->post ( 'username' );
		
		$user_exists=$this->User_Login_Credential_Model->getOneByKeyValueArray(array('internalEmail'=>$internal_email));
		if (count($user_exists) > 0)
		{
		$password = random_string ( 'alnum', 8 );
		// @todo encrypted password
		$data ['password'] = hash ( "sha256", $password );
		// SMS & email service start
		$rest_password=$this->User_Login_Credential_Model->update($data,array('internalEmail'=>$internal_email));
		/* if (SMS && $resst_password) {
			$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
					'smsTitle' => 'forgot_password_sms'
			) );
				
			$message_data = $message_details->smsContent;
			// $message = str_replace("##USERNAME##",$name,$message);
			$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
			// print_r($message);exit;
			if ($data ['mobile'] != "") {
		
				$this->Api_Webservice_Model->sendSMS ( $data ['mobile'], $message_data );
			}
		} */
		
		$from = SMTP_EMAIL_ID;
		$to = $internal_email;
		$subject = 'Zuver Forgot Password';
		$data = array (
				'password' => $password
		);
		$message_data = $this->load->view ( 'emailtemplate/forgotpassword', $data, TRUE );
		
		if ($rest_password)
		{
		if (SMTP && $rest_password) {
				
			if ($to) {
				$this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
			}
		} else {
				
			// To send HTML mail, the Content-type header must be set
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			// Additional headers
			$headers .= 'From: Zuver<' . $from . '>' . "\r\n";
			$headers .= 'To: <' . $to . '>' . "\r\n";
			if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
				mail ( $to, $subject, $message_data, $headers );
			}
		}
		// SMS & email service end
		redirect ( 'login' );
		}
		else {
			$this->session->set_flashdata ( 'login_err', 'Email does not match your Login Email Id' );
			redirect ( 'login#signup' );
		}
		}
		else {
			$this->session->set_flashdata ( 'login_err', 'Email does not match your Login Email Id' );
			redirect ( 'login#signup' );
		}
	}
	public function checkUserIsLogged() {
		if ($this->session->userdata ( 'user_email' ) != '') {
			redirect ( 'dashboard' );
		}
	}
	
	public function changeSessionCityId(){
		$city_id=$this->input->post ( 'city_id' );
		if ($city_id) {
			$this->session->set_userdata ( 'user_city', $city_id );
			$response = array(
				'msg' => 'Success'
			);
		}
		else 
		{
			$response = array(
				'msg' => 'Failed'
			);
		}
		
		echo json_encode($response);
	}
}