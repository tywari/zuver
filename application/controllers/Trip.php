<?php

class Trip extends My_Controller
{

    function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->helper('form');
        // $this->load->model('Query_Model');
        $this->load->model('Trip_Details_Model');
        $this->load->model('Sub_Trip_Details_Model');
        $this->load->model('Trip_Transaction_Details_Model');
        $this->load->model('Passenger_Model');
        $this->load->model('Data_Attributes_Model');
        $this->load->model('Query_Model');
        $this->load->model('City_Model');
        $this->load->model('Data_Attributes_Model');
        $this->load->model('Driver_Request_Details_Model');
        $this->load->model('Driver_Rejected_Trip_Details_Model');
        $this->load->model('Passenger_Wallet_Details_Model');
        $this->load->model('Passenger_Promocode_Details_Model');
        $this->load->model('Passenger_Referral_Details_Model');
        $this->load->model('Sms_Template_Model');
        $this->load->model('Api_Webservice_Model');
        $this->load->model('Driver_Shift_History_Model');
        $this->load->model('Driver_Model');
        $this->load->model('Trip_Temp_Tracking_Info_Model');
        $this->load->model('Site_Details_Model');
        $this->load->model('Rate_Card_Details_Model');
        $this->load->model('Driver_Rate_Card_Details_Model');
        $this->load->model('Driver_Productivity_Allowance_Model');
        $this->load->model('Trip_Tracked_Info_Model');
        $this->load->model('Country_Model');
        $this->load->model('User_Login_Credential_Model');
        $this->load->model('User_Personal_Details_Model');
        $this->load->model('Corporate_Partners_Details_Model');
        if ($this->User_Access_Model->showTrips() == false) {
            show_error("You do not have permission to view this page", '404');
            return;
        }
    }

    public function index()
    {
        // $data = array();
        // $data['activetab']="admin";
        // $data['active_menu'] = 'admin';
        $this->addJs('app/trip.js');

        // $this->render("passenger/add_edit_passenger", $data);
    }

    public function validationTrip()
    {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'firstName',
                'label' => 'First Name',
                'rules' => 'trim|required|alpha|min_length[3]|max_length[15]'
            ),
            array(
                'field' => 'lastName',
                'label' => 'Last Name',
                'rules' => 'trim|required|alpha|min_length[3]|max_length[15]'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'dob',
                'label' => 'Date of Birth',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'profileImage',
                'label' => 'Profile Image',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'mobile',
                'label' => 'Mobile',
                'rules' => 'trim|required|numeric|exact_length[10]'
            ),
            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'driverLanguagesKnown',
                'label' => 'Languages Known',
                'rules' => 'trim|required|alpha'
            ),
            array(
                'field' => 'addressProofNo',
                'label' => 'Address Proof No',
                'rules' => 'trim|required|alpha_numeric'
            ),
            array(
                'field' => 'addressProofImage',
                'label' => 'Address Proof Image',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'identityProofNo',
                'label' => 'Identity Proof No',
                'rules' => 'trim|required|alpha_numeric'
            ),
            array(
                'field' => 'identityProofImage',
                'label' => 'Identity Proof Image',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'panCardNo',
                'label' => 'PAN Card No',
                'rules' => 'trim|required|alpha_numeric'
            ),
            array(
                'field' => 'panCardImage',
                'label' => 'PAN Card Image',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'driverQualification',
                'label' => 'Qualification',
                'rules' => 'trim|required|alpha_numeric'
            ),
            array(
                'field' => 'driverExperience',
                'label' => 'Experience',
                'rules' => 'trim|alpha_numeric|required'
            ),
            array(
                'field' => 'drivingLicenseNo',
                'label' => 'Driving License No',
                'rules' => 'trim|required|alpha_numeric'
            ),
            array(
                'field' => 'drivingLicenseExpireDate',
                'label' => 'Driving License ExpireDate',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'drivingLicenseImage',
                'label' => 'Driving License Image',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'skills',
                'label' => 'Skills',
                'rules' => 'trim|required|alpha'
            ),
            array(
                'field' => 'favouriteCar',
                'label' => 'Favourite Car',
                'rules' => 'trim|required|alpha_numeric'
            ),
            array(
                'field' => 'companyId',
                'label' => 'Company Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'cityId',
                'label' => 'City Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'driverType',
                'label' => 'Driver Type',
                'rules' => 'trim|required'
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function add()
    {
        if ($this->User_Access_Model->getAccessLevelForTripEdit() == User_Access_Level_Enum::NO_ACCESS || $this->User_Access_Model->getAccessLevelForTripEdit() == User_Access_Level_Enum::READ_ONLY) {
            show_error("You do not have permission to view this page", '404');
            return;
        }
        $this->addJs('datepicker/jquery.datetimepicker.full.js');
        $this->addJs('app/trip.js');

        $data = array(
            'mode' => EDIT_MODE
        );
        $data ['activetab'] = "trip";
        $data ['active_menu'] = 'trip';
        $data ['city_list'] = $this->City_Model->getSelectDropdownOptions(array(
            'status' => 'Y'
        ), 'name');
        $data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions(array(
            'status' => 'Y'
        ), 'id');
        // $data['driver_gender']=$this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName'=>strtoupper('GENDER_TYPE'),'isVisible'=>'Y'),'sequenceOrder');
        $data ['transmission_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('TRANSMISSION_TYPE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['payment_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('PAYMENT_MODE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('TRIP_TYPE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['trip_status'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('TRIP_STATUS'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['driver_accepted_status'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('DRIVER_ACCEPTED_STATUS'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['driver_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('DRIVER_TYPE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['car_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('CAR_TYPE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['passenger_model'] = $this->Passenger_Model->getById(1);
        $data ['trip_model'] = $this->Trip_Details_Model->getBlankModel();
        $this->render('trip/add_edit_trip', $data);
    }

    public function saveTrip()
    {
        $data = array();

        $this->addJs('app/trip.js');

        $data = $this->input->post();
        $trip_id = $data ['id'];
        // $data['activetab']="admin";
        // $data['active_menu'] = 'admin';
        $msg_data = $this->config->item('msg');
        if (array_key_exists('billType', $data)) {
            if ($data['billType'] == '') {
                $data['billType'] = Bill_Type_Enum::HOURLY;
            }
        }
        $msg = $msg_data ['failed'];
        if ($trip_id > 0) {
            $trip_updated = $this->Trip_Details_Model->update($data, array(
                'id' => $trip_id
            ));
            if ($trip_updated) {
                $msg = $msg_data ['updated'];
            } else {
                $msg = $msg_data ['failed'];
            }
        } else {
            $next_trip_id = $this->Trip_Details_Model->getAutoIncrementValue();
            $booking_key = $data ['tripType'] . '-' . $data ['paymentMode'] . '-' . $data ['transmissionType'] . '-' . $next_trip_id;
            $data ['bookingKey'] = $booking_key;
            $data ['tripStatus'] = Trip_Status_Enum::BOOKED;
            $data ['notificationStatus'] = Trip_Status_Enum::BOOKED;
            // $data ['companyId'] = $this->session->userdata('user_company');
            $trip_id = $this->Trip_Details_Model->insert($data);

            if ($trip_id) {

                // To get the passenger mobile & email
                $passenger_data = $this->Passenger_Model->getById($data ['passengerId']);

                if (SMS && count($passenger_data) > 0) {

                    $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                        'smsTitle' => 'booking_confirmed_sms'
                    ));
                    $message_data = $message_details->smsContent;
                    // $message = str_replace("##PASSENGERNAME##",$name,$message);
                    $message_data = str_replace("##PASSENGERNAME##", $passenger_data->firstName . ' ' . $passenger_data->lastName, $message_data);
                    $message_data = str_replace("##BOOKINGID##", $booking_key, $message_data);
                    //$message_data = str_replace("##DEMO##", '', $message_data);
                    // print_r($message);exit;
                    // this SMS is sending to below numbers on request from Sidhanth on 07/10/2016
                    $this->Api_Webservice_Model->sendSMS('9619303999', $message_data);
                    $this->Api_Webservice_Model->sendSMS('9819896587', $message_data);
                    $this->Api_Webservice_Model->sendSMS('9619728423', $message_data);
                    // this SMS is sending to below numbers on request from Sidhanth on 07/10/2016

                    if ($passenger_data->mobile != "") {

                        $this->Api_Webservice_Model->sendSMS($passenger_data->mobile, $message_data);
                    }
                }

                $from = SMTP_EMAIL_ID;
                // Sending email to info@zuver.in on request from Sidhanth on 07/10/2016
                $to_list = array(
                    'info@zuver.in',
                    'sidhanth@zuver.in',
                    'sovin@zuver.in'
                );

                $subject = 'Trip Booking Request-' . SMTP_EMAIL_NAME;
                $email_template_data = array(
                    'booking_key' => $booking_key,
                    'pickup_location' => $data['pickupLocation'],
                    'pickup_date_time' => $data['pickupDatetime']
                );

                if (isset($data['companyId']) && $data['companyId'] > 1) {
                    $entity_details = $this->Corporate_Partners_Details_Model->getOneByKeyValueArray(array(
                        'id' => $data['companyId']
                    ));
                    $email_template_data['trip_type'] = 'B2B';
                    $email_template_data['trip_id'] = $trip_id;
                    $email_template_data['entity_name'] = $entity_details->name;
                    $email_template_data['entity_mobile'] = $entity_details->contactMobile;
                } else {
                    $to_list[] = $passenger_data->email;
                    $email_template_data['trip_type'] = 'B2C';
                    $email_template_data['passenger_name'] = $passenger_data->firstName . ' ' . $passenger_data->lastName;
                    $email_template_data['passenger_mobile'] = $passenger_data->mobile;
                }

                $message_data = $this->load->view('emailtemplate/booking_confirm', $email_template_data, TRUE);
                foreach ($to_list as $to) {
                    if (SMTP) {

                        if ($to) {
                            $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                        }
                    } else {

                        // To send HTML mail, the Content-type header must be set
                        $headers = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        // Additional headers
                        $headers .= 'From: Zuver<' . $from . '>' . "\r\n";
                        $headers .= 'To: <' . $to . '>' . "\r\n";
                        if (!filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
                            mail($to, $subject, $message_data, $headers);
                        }
                    }
                }
            }
            // Sending email to info@zuver.in on request from Sidhanth on 07/10/2016
            $msg = $msg_data ['success'];
        }
        // $this->render("passenger/add_edit_passenger", $data);

        $response = array(
            'msg' => $msg,
            'tripId' => $trip_id
        );
        echo json_encode($response);
    }

    public function AddSubTrip()
    {
        $data = array();
        $this->addJs('app/trip.js');

        $data = $this->input->post();
        $masterTripId = $data['masterTripId'];
        $msg_data = $this->config->item('msg');
        $trip_msg_data = $this->config->item('api');
        $msg = $msg_data['failed'];
        $data['subTripId'] = $masterTripId . '-A';
        $master_trip_details = $this->Trip_Details_Model->getById($masterTripId);
        $insert_sub_trip_id = 0;
        $sub_trip_list = $this->Sub_Trip_Details_Model->getOneByKeyValue('masterTripId', $masterTripId, 'subTripId Desc');
        if ($sub_trip_list) {
            $previous_subTripId = $sub_trip_list->subTripId;
            ++$previous_subTripId;
            $data['subTripId'] = $previous_subTripId;
        }

        $check_exist_sub_trip_pickup = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId' => $masterTripId, "pickupDatetime >=" => $data['pickupDatetime'], "pickupDatetime <" => $data['dropDatetime']), 'subTripId Desc');
        $check_exist_sub_trip_drop = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId' => $masterTripId, "dropDatetime >" => $data['pickupDatetime'], "dropDatetime <" => $data['dropDatetime']), 'subTripId Desc');

        if (strtotime($data['pickupDatetime']) < strtotime($master_trip_details->actualPickupDatetime)) {
            $msg = 'Sub-trip Start Time can not be less than Master Trip Start Time';
        } elseif ($check_exist_sub_trip_pickup || $check_exist_sub_trip_drop) {
            $msg = 'Sub-trip already exist in given time slot';
        } elseif (strtotime($data['pickupDatetime']) > strtotime($data['dropDatetime'])) {
            $msg = $trip_msg_data ['invalid_drop_time'];
        } else {
            $insert_sub_trip_id = $this->Sub_Trip_Details_Model->insert($data);
            if ($insert_sub_trip_id) {
                $msg = $msg_data ['success'];
            }
        }
        $response = array(
            'msg' => $msg,
            'tripId' => $masterTripId,
            'sub_trip_id' => $insert_sub_trip_id
        );
        echo json_encode($response);
    }

    public function getSubTrips()
    {
        $data = array();
        $data['sub_trip_list'] = $this->Sub_Trip_Details_Model->getByKeyValueArray(array(
            'masterTripId' => $this->input->post('masterTripId')
        ), 'pickupDatetime');
        $this->load->view('trip/subtrips_list', $data);
    }

    public function getDetailsById($trip_id, $view_mode)
    {
        if ($this->User_Access_Model->getAccessLevelForTripEdit() == User_Access_Level_Enum::NO_ACCESS) {
            show_error("You do not have permission to view this page", '404');
            return;
        }
        $data = array();

        $this->addJs('app/trip.js');
        $this->addJs('vendors/star.js');
        $this->addJs('vendors/star-rating/js/star-rating.js');
        $this->addCss('vendors/star-rating/css/star-rating.css');
        $data = array(
            'mode' => $view_mode
        );

        $data ['activetab'] = "trip";
        $data ['active_menu'] = 'trip';
        $data ['city_list'] = $this->City_Model->getSelectDropdownOptions(array(
            'status' => 'Y'
        ), 'name');
        $data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions(array(
            'status' => 'Y'
        ), 'id');
        // $data['driver_gender']=$this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName'=>strtoupper('GENDER_TYPE'),'isVisible'=>'Y'),'sequenceOrder');
        $data ['transmission_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('TRANSMISSION_TYPE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['payment_mode_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('PAYMENT_MODE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['trip_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('TRIP_TYPE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['trip_status'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('TRIP_STATUS'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['driver_accepted_status'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('DRIVER_ACCEPTED_STATUS'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['driver_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('DRIVER_TYPE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['car_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array(
            'tableName' => strtoupper('CAR_TYPE'),
            'isVisible' => 'Y'
        ), 'sequenceOrder');
        $data ['bill_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName' => strtoupper('BILL_TYPE'),
            'isVisible' => 'Y'), 'sequenceOrder');
        if ($this->input->post('id')) {
            $trip_id = $this->input->post('id');
        }

        if ($trip_id > 0) {
            // Get release details by driver_id
            $data ['trip_model'] = $this->Trip_Details_Model->getById($trip_id);
            if (!$data ['trip_model']) {
                show_error("Invalid trip Id", '404');
                return;
            }
            $data ['partner_model'] = $this->Corporate_Partners_Details_Model->getById($data ['trip_model']->companyId);
            $driver_id = $data ['trip_model']->driverId;
            $passenger_id = ($data ['trip_model'] != '') ? $data ['trip_model']->passengerId : 0;
            $data ['passenger_model'] = $this->Passenger_Model->getById($passenger_id);
            $data ['driver_model'] = $this->Driver_Model->getById($driver_id);

            $data ['user_model'] = $this->User_Login_Credential_Model->getById($data ['trip_model']->createdBy);
            $data ['user_personal_detail'] = $this->User_Personal_Details_Model->getById($data ['trip_model']->createdBy);
            $data ['user_personal_model'] = $this->User_Personal_Details_Model->getOneByKeyValueArray(array('userId' => $data ['trip_model']->createdBy));
            $data ['corporate_partner_model'] = $this->Corporate_Partners_Details_Model->getById($data ['trip_model']->companyId);
            $data['transcation_model'] = $this->Trip_Transaction_Details_Model->getOneByKeyValueArray(array('tripId' => $trip_id));
        } else {
            // Blank Model
            $data ['trip_model'] = $this->Trip_Details_Model->getBlankModel();
            $data ['passenger_model'] = $this->Passenger_Model->getBlankModel();
            $data ['driver_model'] = $this->Driver_Model->getBlankModel();
            $data ['user_model'] = $this->User_Login_Credential_Model->getBlankModel();
            $data ['user_personal_model'] = $this->User_Personal_Details_Model->getBlankModel();
            $data ['corporate_partner_model'] = $this->Corporate_Partners_Details_Model->getBlankModel();
            $data ['partner_model'] = $this->Corporate_Partners_Details_Model->getBlankModel();
            $data ['user_personal_detail'] = $this->User_Personal_Details_Model->getBlankModel();
            $data['transcation_model'] = $this->Trip_Transaction_Details_Model->getBlankModel();
        }

        if ($this->input->post('id')) {
            $html = $this->load->view('trip/add_edit_trip', $data, TRUE);
            echo json_encode(array(
                'html' => $html
            ));
        } else {
            $this->render('trip/add_edit_trip', $data);
        }
    }

    // created by Aditya on May 31 2017
    // this function will simply be used to load/render a view
    public function loadB2BDriverMap()
    {
        $data = array();

        $this->addJs('datepicker/jquery.datetimepicker.full.js');
        $this->addJs('vendors/datatables.net/js/jquery.dataTables.min.js');
        $this->addJs('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js');
        $this->addJs('vendors/datatables.net-buttons/js/dataTables.buttons.min.js');
        $this->addJs('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js');
        $this->addJs('vendors/datatables.net-buttons/js/buttons.flash.min.js');
        $this->addJs('vendors/datatables.net-buttons/js/buttons.html5.min.js');
        $this->addJs('vendors/datatables.net-buttons/js/buttons.print.min.js');
        $this->addJs('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');
        $this->addJs('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js');
        $this->addJs('vendors/datatables.net/js/jquery.dataTables.min.js');
        $this->addJs('vendors/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->addJs('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js');
        $this->addJs('vendors/datatables.net-scroller/js/datatables.scroller.min.js');

        $filter_cityid = $this->session->userdata('user_city');
        // }
        $data ['filter_cityid'] = $filter_cityid;
        $html = $this->load->view("trip/load_b2b_driver_map", $data);
    }

    // created by Aditya on May 31 2017
    // this function will be called by Partner Dashboard only
    public function getB2BDriverList()
    {
        $data = array();

        $city_id = '';//$this->input->post('city_id');
        $trip_status = '';//$this->input->post('trip_status');
        if ($this->session->userdata('role_type') == 'CO') {
            $company_id = $this->session->userdata('user_company');
            //$company_id = 30;// Zoom Car client
            //$city_id = $this->session->userdata('cityId');
            $trip_status = '';// it will return all kinds of trip except for trip-completed
        } else {
            $company_id = '';
        }

        $data ['driver_details_model_list'] = $this->Query_Model->getB2BDriverList($city_id, $company_id, $trip_status, 0);
        $data ['activetab'] = "trip";
        $data ['active_menu'] = 'trip';

        echo json_encode(($data ['driver_details_model_list']));
        die();
    }

    // created by Aditya on June 01 2017
    public function getPartnerDetails()
    {
        if ($this->session->userdata('role_type') == 'CO') {
            $company_id = $this->session->userdata('user_company');
            $client_details = $this->Corporate_Partners_Details_Model->getOneByKeyValueArray(array(
                'id' => $company_id
            ));
            //print_r($client_details->address);
            echo json_encode($client_details->address);
        }
    }

    // comment added by Aditya on May 31 2017
    // this function can be called by Zuver Admin Dashboard as well as partner dashboard
    public function getBtoBTripList()
    {
        $data = array();

        // get type whether to load or render the ouptput
        $get_type = $this->input->post('get_type');
        $this->addJs('datepicker/jquery.datetimepicker.full.js');
        $this->addJs('app/trip.js');
        $this->addJs('app/tabel.js');

        $this->addJs('vendors/datatables.net/js/datatables.min.js');
        $this->addJs('vendors/datatables.net/js/buttons.min.js');
        $this->addJs('vendors/datatables.net-buttons/js/buttons.flash.min.js');
        // $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
        $this->addJs('vendors/jszip/dist/jszip.min.js');
        $this->addJs('vendors/pdfmake/build/pdfmake.min.js');
        $this->addJs('vendors/datatables.net/js/vfs_fonts.js');
        $this->addJs('vendors/datatables.net/js/buttons.html5.min.js');
        $this->addJs('vendors/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->addJs('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js');
        $this->addJs('vendors/datatables.net-scroller/js/datatables.scroller.min.js');

        $city_id = $this->input->post('city_id');
        $trip_status = $this->input->post('trip_status');

        if ($this->session->userdata('role_type') == 'CO') {

            $company_id = $this->session->userdata('user_company');
        } else {
            $company_id = '';
        }

        $data ['trip_details_model_list'] = $this->Query_Model->getTripList($city_id, $company_id, $trip_status, 0);
        $data ['activetab'] = "trip";
        $data ['active_menu'] = 'trip';
        if ($get_type == 'ajax_call') {
            $html = $this->load->view("trip/manage_trip", $data);
            echo json_encode(array(
                'html' => $html
            ));
        } else {
            $html = $this->render("trip/manage_trip", $data);
        }
    }

    // comment added by Aditya on May 31 2017
    // this function will only be called by Zuver Admin Dashboard and not partner dashboard
    public function getBtoCTripList()
    {
        if ($this->User_Access_Model->getAccessLevelForTripEdit() == User_Access_Level_Enum::NO_ACCESS || $this->User_Access_Model->getAccessLevelForTripEdit() == User_Access_Level_Enum::READ_ONLY) {
            show_error("You do not have permission to view this page", '404');
            return;
        }
        $data = array();

        // get type whether to load or render the ouptput
        $get_type = $this->input->post('get_type');
        $this->addJs('datepicker/jquery.datetimepicker.full.js');
        // $this->addJs('app/trip.js');
        $this->addJs('app/tabel.js');

        $this->addJs('vendors/datatables.net/js/datatables.min.js');
        $this->addJs('vendors/datatables.net/js/buttons.min.js');
        $this->addJs('vendors/datatables.net-buttons/js/buttons.flash.min.js');
        // $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
        $this->addJs('vendors/jszip/dist/jszip.min.js');
        $this->addJs('vendors/pdfmake/build/pdfmake.min.js');
        $this->addJs('vendors/datatables.net/js/vfs_fonts.js');
        $this->addJs('vendors/datatables.net/js/buttons.html5.min.js');
        $this->addJs('vendors/datatables.net-responsive/js/dataTables.responsive.min.js');
        $this->addJs('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js');
        $this->addJs('vendors/datatables.net-scroller/js/datatables.scroller.min.js');

        $city_id = $this->input->post('city_id');
        $trip_status = $this->input->post('trip_status');
        if ($this->session->userdata('role_type') == 'CO') {
            $company_id = $this->session->userdata('user_company');
        } else {
            $company_id = '';
        }

        $data ['trip_details_model_list'] = $this->Query_Model->getTripList($city_id, $company_id, $trip_status, 1);
        $data ['activetab'] = "trip";
        $data ['active_menu'] = 'trip';
        if ($get_type == 'ajax_call') {
            $html = $this->load->view("trip/manage_b_c_trip", $data);
            echo json_encode(array(
                'html' => $html
            ));
        } else {
            $html = $this->render("trip/manage_b_c_trip", $data);
        }
    }

    public function changeTripStatus($view_mode)
    {
        $data = array();

        $this->addJs('app/trip.js');

        $data = $this->input->post();
        $trip_id = $data ['id'];
        $driver_status = $data ['status'];

        $msg_data = $this->config->item('msg');
        $msg = $msg_data ['status_change_failed'];
        if ($trip_id) {
            $trip_updated = $this->Trip_Details_Model->update(array(
                'status' => $driver_status
            ), array(
                'id' => $trip_id
            ));
            $msg = $msg_data ['status_change_success'];
        }
        // $html=$this->render("trip/manage_trip", $data);
        echo json_encode(array(
            'html' => $html
        ));
    }

    public function getPassegerDetails()
    {
        $mobile = $_POST ['mobile'];
        $data = 0;
        $result = $this->Passenger_Model->getOneByKeyValueArray(array(
            'mobile' => $mobile,
            'status' => 'Y'
        ), 'id');
        if ($result) {
            $passenger = $result->id;
            $p_name = $result->firstName;
            $p_email = $result->email;
            echo $passenger . ',' . $p_name . ',' . $p_email;
        } else {
            echo $data;
        }
    }

    public function getDriverListToAssign()
    {
        $trip_id = $this->input->post('tripid');
        $data ['driver_list'] = $this->Query_Model->getDriverListForAssign($trip_id);


        $this->load->view('trip/driver_list', $data);
    }

    public function AssignDriverToTrip()
    {

        $msg_data = $this->config->item('msg');
        $driver_id = $this->input->post('driverid');
        $trip_id = $this->input->post('tripid');
        $trip_status = NULL;
        $notification_status = NULL;
        $accept_status = NULL;
        $available_status = Null;
        $request_status = Null;
        $db_trip_update = 0;
        $trip_details = $this->Trip_Details_Model->getById($trip_id);

        //If it is zoom car trip send booking confirmation details through ZOOMCAR API
        if ($trip_details->companyId == ZOOMCAR_COMPANYID) {
            $zoom_response = $this->Api_Webservice_Model->zoomCarTripResponse($trip_id, $trip_details->bookingKey, $driver_id, 1,0);
            if ($zoom_response['status'] == 'success') {
                $db_trip_update = 1;
            } else {
                $message = array(
                    "message" => 'Zoomcar API failed. Failure Message: ' . $zoom_response['message'],
                    "status" => -1
                );
            }

        } else {
            $db_trip_update = 1;
        }

        if ($db_trip_update) {
            $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                'id' => $driver_id,
                'status' => Status_Type_Enum::ACTIVE,
                'loginStatus' => Status_Type_Enum::ACTIVE,
                'isVerified' => Status_Type_Enum::ACTIVE,
                'isDeleted' => Status_Type_Enum::INACTIVE
            ));

            if ($driver_logged_status) {
                $trip_status = Trip_Status_Enum::TRIP_CONFIRMED;
                $notification_status = Trip_Status_Enum::TRIP_CONFIRMED;
                $accept_status = Driver_Accepted_Status_Enum::NONE;
                $request_status = Driver_Request_Status_Enum::AVAILABLE_TRIP;
                $available_status = Driver_Available_Status_Enum::FREE;
            } else {
                $trip_status = Trip_Status_Enum::TRIP_CONFIRMED;
                $notification_status = Trip_Status_Enum::TRIP_CONFIRMED;
                $accept_status = Driver_Accepted_Status_Enum::ACCEPTED;
                $request_status = Driver_Request_Status_Enum::SENT_TO_DRIVER;
                $available_status = Driver_Available_Status_Enum::BUSY;

                // If offline driver make driver avilablestatus is busy
                // to get last shift in status of driver before updating the shift out status

                $last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                    'driverId' => $driver_id
                ), 'id DESC');
                $driver_shift_insert_id = $this->Driver_Shift_History_Model->update(array(
                    'availabilityStatus' => $available_status
                ), array(
                    'id' => $last_driver_shift_id->id,
                    'driverId' => $driver_id
                ));
            }

            // update Tri[p details

            $data['driverId'] = $driver_id;
            $data['driverId'] = $driver_id;
            $data['driverAssignedDatetime'] = getCurrentDateTime();
            $data['tripStatus'] = $trip_status;
            $data['notificationStatus'] = $notification_status;
            $data['driverAcceptedStatus'] = $accept_status;
            $trip_updated = $this->Trip_Details_Model->update($data, array(
                'id' => $trip_id
            ));
            /**
             * ** check trip id exists for driver request details ***
             */
            $trip_exists = $this->Driver_Request_Details_Model->getByKeyValueArray(array(
                'tripId' => $trip_id
            ), 'id');
            $data['tripId'] = $trip_id;
            $data['availableDrivers'] = $driver_id;
            $data['totalDriverAvailable'] = $driver_id;
            $data['selectedDriverId'] = $driver_id;
            $data['tripStatus'] = $request_status;

            if (count($trip_exists) > 0) {
                /**
                 * * update Driver request details if trip id Exists **
                 */
                $update_driver_details = $this->Driver_Request_Details_Model->update($data, array(
                    'tripId' => $trip_id
                ));
            } else {
                /**
                 * * insert into driver request details if trip id dosent exists **
                 */
                $update_driver_details = $this->Driver_Request_Details_Model->insert($data);
            }

            if ($trip_updated) {
                if (SMS && $trip_updated) {

                    // get passenger mobile number

                    $user_details = array();
                    $user_personal_details = array();
                    $customer_details = array();
                    $passenger_details = $this->Passenger_Model->getById($trip_details->passengerId);
                    if (!(count($passenger_details) > 0)) {
                        $user_details = $this->User_Login_Credential_Model->getById($trip_details->createdBy);
                        if ($user_details) {
                            $user_personal_details = $this->User_Personal_Details_Model->getOneByKeyValueArray(array(
                                'userId' => $user_details->id
                            ));
                        }
                    }
                    if ($trip_details->clientPhoneNumber) {
                        $customer_details = array("customerName" => $trip_details->clientName, "customerContact" => $trip_details->clientPhoneNumber);
                    }
                    // get driver full name & mobile number

                    $driver_details = $this->Driver_Model->getById($driver_id);
                    $address = array();
                    $address[0] = substr($trip_details->pickupLocation, 0, 20);
                    $address[1] = substr($trip_details->pickupLocation, 20, 20);
                    $address[2] = substr($trip_details->pickupLocation, 40, 20);
                    $address[3] = substr($trip_details->pickupLocation, 60, 20);
                    $address[4] = substr($trip_details->pickupLocation, 80, 20);
                    $address[5] = substr($trip_details->pickupLocation, 100, 20);
                    $address[6] = substr($trip_details->pickupLocation, 120, 20);
                    $address[7] = substr($trip_details->pickupLocation, 140, 20);
                    if (strlen($trip_details->pickupLocation) > 160) {
                        $address[7] = cut_string_using_last(',', $address[7], 'left', false);
                    }

                    if ($passenger_details) {
                        $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                            'smsTitle' => 'booking_confirmed_driver_sms'
                        ));
                    } else {
                        $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                            'smsTitle' => 'booking_confirmed_b2b_driver_sms'
                        ));
                    }

                    $message_data = $message_details->smsContent;
                    if ($user_details && $user_personal_details) {
                        $client_details = $this->Corporate_Partners_Details_Model->getOneByKeyValueArray(array(
                            'id' => $trip_details->companyId
                        ));
                        if ($customer_details) {
                            $message_data = str_replace("##USERNAME##", $customer_details['customerName'], $message_data);
                            $message_data = str_replace("##USERMOBILE##", $customer_details['customerContact'], $message_data);
                        } else {
                            $message_data = str_replace("##USERNAME##", $user_details->firstName . ' ' . $user_details->lastName, $message_data);
                            $message_data = str_replace("##USERMOBILE##", $user_personal_details->mobile, $message_data);
                        }
                        $message_data = str_replace("##CLIENTNAME##", $client_details->name, $message_data);
                    } else {

                        // B2C booking passenger details

                        $message_data = str_replace("##PASSENGERNAME##", $passenger_details->firstName . ' ' . $passenger_details->lastName, $message_data);
                        $message_data = str_replace("##PASSENGERMOBILE##", $passenger_details->mobile, $message_data);

                        // B2B booking user details with client name ,vehicle name, make, model

                    }
                    $message_data = str_replace("##VEHICLENO##", $trip_details->vehicleNo, $message_data);
                    $message_data = str_replace("##VEHICLEMAKE##", $trip_details->vehicleMake, $message_data);
                    $message_data = str_replace("##VEHICLEMODEL##", $trip_details->vehicleModel, $message_data);
                    $message_data = str_replace("##PICKUPTIME##", $trip_details->pickupDatetime, $message_data);
                    $customer_care_no = ($trip_details->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
                    $message_data = str_replace("##CUSTOMERCARE##", $customer_care_no, $message_data);
                    $message_data = str_replace("##ADDR1##", $address[0], $message_data);
                    $message_data = str_replace("##ADDR2##", $address[1], $message_data);
                    $message_data = str_replace("##ADDR3##", $address[2], $message_data);
                    $message_data = str_replace("##ADDR4##", $address[3], $message_data);
                    $message_data = str_replace("##ADDR5##", $address[4], $message_data);
                    $message_data = str_replace("##ADDR6##", $address[5], $message_data);
                    $message_data = str_replace("##ADDR7##", $address[6], $message_data);
                    $message_data = str_replace("##ADDR8##", $address[7], $message_data);

                    // print_r($message);exit;

                    if ($driver_details) {
                        if ($driver_details->mobile) {

                            $this->Api_Webservice_Model->sendSMS($driver_details->mobile, $message_data);


                            // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017

                            $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);

                            // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017

                        }
                    }
                    if ($passenger_details) {
                        // Aditya Code Updation, 25th Apr 2017, earlier it was arrived at
                        $passanger_message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                            'smsTitle' => 'current_booking_confirm'
                        ));

                        $passanger_message_data = $passanger_message_details->smsContent;

                        /* Aditya Code Start Here, 25th Apr 2017 */
                        // Get Passanger Name and show that in the SMS to be sent to Passenger
                        $passanger_message_data = str_replace("##PASSENGERNAME##", $passenger_details->firstName . ' ' . $passenger_details->lastName, $passanger_message_data);

                        // Get Trip Pickup Time
                        // Then show that in the SMS to be sent to Passenger
                        $passanger_message_data = str_replace("##PICKUPTIME##", $trip_details->pickupDatetime, $passanger_message_data);

                        /* Aditya Code End Here */


                        $passanger_message_data = str_replace("##DRIVERNAME##", $driver_details->firstName . ' ' . $driver_details->lastName, $passanger_message_data);
                        $passanger_message_data = str_replace("##DRIVERMOBILE##", $driver_details->mobile, $passanger_message_data);
                        if ($passenger_details->mobile) {
                            $this->Api_Webservice_Model->sendSMS($passenger_details->mobile, $passanger_message_data);
                        }
                    }
                }

                $message = array(
                    "message" => $msg_data['assign_success'],
                    "status" => 1
                );
            } else {
                $message = array(
                    "message" => $msg_data['assign_failed'],
                    "status" => -1
                );
            }
        }
        echo json_encode($message);
    }

    public function DriverArrived()
    {
        // Intialized
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $trip_details = NULL;
        $check_trip_avilablity = NULL;
        $trip_id = NULL;
        $response_data = array();
        $post_data = $this->input->post();
        $trip_id = $post_data ['trip_id'];
        $db_trip_update = 0;
        // actual functionality
        if ($trip_id) {
            // To get trip details i.e., trip status,driver accepted status
            $trip_details = $this->Trip_Details_Model->getById($trip_id);
            if ($trip_details) {

                // check trip avilable for driver/any in progress trip before logout
                $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                    'tripId' => $trip_id
                ));

                if ($check_trip_avilablity) {
                    if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED) {
                        if ($trip_details->tripStatus == Trip_Status_Enum::IN_PROGRESS || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT) {
                            $message = array(
                                "message" => $msg_data ['trip_progress'],
                                "status" => -1
                            );
                        } else {
                            // get driver full name & mobile number
                            $driver_details = $this->Driver_Model->getById($trip_details->driverId);

                            if ($trip_details->companyId == ZOOMCAR_COMPANYID) {
                                $zoom_checklist_response = $this->Api_Webservice_Model->zoomCarGetChecklist($trip_id, $trip_details->bookingKey);
                                if ($zoom_checklist_response['status'] == 'success') {
                                    $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                        'smsTitle' => 'zoomcar_driver_arrived_sms'
                                    ));
                                    $message_data = $message_details->smsContent;
                                    $message_data = str_replace("##TRIP_ID##", $trip_id, $message_data);
                                    $message_data = str_replace("##ZOOMCAR_BOOKING_ID##", $trip_details->bookingKey, $message_data);
                                    $message_data = str_replace("##PICKUP_URL##", $zoom_checklist_response['checklist_1'], $message_data);
                                    $message_data = str_replace("##DROP_URL##", $zoom_checklist_response['checklist_2'], $message_data);
                                    $message_data = str_replace("##OTP_CODE##", $zoom_checklist_response['otp'], $message_data);
                                    $this->Api_Webservice_Model->sendSMS($driver_details->mobile, $message_data);
                                    $db_trip_update = 1;
                                } else {
                                    $message = array(
                                        "message" => 'Zoomcar API failed. Failure Message: ' . $zoom_checklist_response['message'],
                                        "status" => -1
                                    );
                                }

                            } else {
                                $db_trip_update = 1;
                            }
                            // echo $db_trip_update; die();
                            if ($db_trip_update) {
                                $update_trip_status = $this->Trip_Details_Model->update(array(
                                    'tripStatus' => Trip_Status_Enum::DRIVER_ARRIVED,
                                    'notificationStatus' => Trip_Status_Enum::DRIVER_ARRIVED,
                                    'driverAcceptedStatus' => Driver_Accepted_Status_Enum::ACCEPTED,
                                    'driverArrivedDatetime' => getCurrentDateTime()
                                ), array(
                                    'id' => $trip_id
                                ));

                                if ($update_trip_status) {
                                    $update_request_status = $this->Driver_Request_Details_Model->update(array(
                                        'tripStatus' => Driver_Request_Status_Enum::DRIVER_ACCEPTED
                                    ), array(
                                        'tripId' => $trip_id,
                                        'selectedDriverId' => $trip_details->driverId
                                    ));
                                    if ($update_request_status) {
                                        // to get last shift in status of driver before updating the shift out status
                                        /* $last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                            'driverId' => $trip_details->driverId
                                                ), 'id DESC'); */
                                        // insert driver shift history IN data
                                        /* $update_data = array(
                                            'outLatitude' => '',
                                            'outLongitude' => '',
                                            'availabilityStatus' => Driver_Available_Status_Enum::NOTAVAILABLE,
                                            'shiftStatus' => Driver_Shift_Status_Enum::SHIFTOUT
                                        ); */
                                        /*  $driver_shift_insert_id = $this->Driver_Shift_History_Model->update(array(
                                             'availabilityStatus' => Driver_Available_Status_Enum::BUSY
                                                 ), array(
                                             'id' => $last_driver_shift_id->id,
                                             'driverId' => $trip_details->driverId
                                                 )); */

                                        if ($update_request_status) {

                                            // get passenger mobile number
                                            $passenger_details = $this->Passenger_Model->getById($trip_details->passengerId);

                                            if (SMS && $update_request_status) {
                                                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                    'smsTitle' => 'driver_arrived'
                                                ));

                                                $message_data = $message_details->smsContent;
                                                $message_data = str_replace("##DRIVERNAME##", $driver_details->firstName . ' ' . $driver_details->lastName, $message_data);
                                                $message_data = str_replace("##DRIVERMOBILE##", $driver_details->mobile, $message_data);

                                                // print_r($message);exit;
                                                if ($passenger_details) {
                                                    if ($passenger_details->mobile) {

                                                        $this->Api_Webservice_Model->sendSMS($passenger_details->mobile, $message_data);
                                                    }
                                                }
                                            }

                                            /*
                                             * $from = SMTP_EMAIL_ID;
                                             * $to = ($passenger_details)?$passenger_details->email:NULL;
                                             * $subject = 'Get Ready to ride with Zuver!';
                                             * $data = array (
                                             * 'user_name' => $passenger_details->firstName . ' ' . $passenger_details->lastName
                                             * );
                                             * $message_data = $this->load->view ( 'emailtemplate/passenger_register', $data, TRUE );
                                             *
                                             * if (SMTP && $check_otp) {
                                             *
                                             * if ($to) {
                                             * $this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
                                             * }
                                             * } else {
                                             *
                                             * // To send HTML mail, the Content-type header must be set
                                             * $headers = 'MIME-Version: 1.0' . "\r\n";
                                             * $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                             * // Additional headers
                                             * $headers .= 'From: ' . $from . '' . "\r\n";
                                             * $headers .= 'Bcc: ' . $to . '' . "\r\n";
                                             * mail ( $to, $subject, $message_data, $headers );
                                             * }
                                             */
                                            //If it is zoom car trip send booking confirmation details through ZOOMCAR API
                                            $message = array(
                                                "message" => $msg_data ['driver_arrival'],
                                                "status" => 1
                                            );
                                        } else {
                                            $message = array(
                                                "message" => $msg_data ['failed'],
                                                "status" => -1
                                            );
                                            log_message('debug', $msg_data ['trip_status_failed'] . '' . $trip_details->driverId);
                                        }
                                    } else {
                                        $message = array(
                                            "message" => $msg_data ['trip_status_failed'] . $trip_id,
                                            "status" => -1
                                        );
                                        log_message('debug', $msg_data ['trip_status_failed'] . $trip_id . ' driver_id=' . $trip_details->driverId);
                                    }
                                } else {
                                    $message = array(
                                        "message" => $msg_data ['trip_status_failed'] . $trip_id,
                                        "status" => -1
                                    );
                                    log_message('debug', $msg_data ['trip_status_failed'] . $trip_id);
                                }
                            }
                        }
                    } else {

                        if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                            $message = array(
                                "message" => $msg_data ['trip_reject_passenger'],
                                "status" => -1
                            );
                        } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                            $message = array(
                                "message" => $msg_data ['trip_completed'],
                                "status" => -1
                            );
                        }
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip_request'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['invalid_trip'],
                    "status" => -1
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['invalid_trip'],
                "status" => -1
            );
        }

        echo json_encode($message);
    }

    public function StartTrip()
    {
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $trip_status_update = NULL;
        $trip_request_status_update = NULL;
        $trip_tracking_update = NULL;
        $driver_id = NULL;
        $trip_id = NULL;
        $latitude = NULL;
        $longitude = NULL;
        $status = NULL;
        $response_data = array();

        // Posted json data
        $post_data = $this->input->post();

        $driver_id = $post_data ['driver_id'];
        $trip_id = $post_data ['trip_id'];
        $latitude = $post_data ['latitude'];
        $longitude = $post_data ['longitude'];
        $status = $post_data ['status'];
        $db_trip_update = 0;
        // change driver as logged in
        $data ['loginStatus'] = Status_Type_Enum::ACTIVE;
        $driver_logged_status = $this->Driver_Model->update($data, array(
            'id' => $driver_id
        ));
        $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
            'id' => $driver_id,
            'status' => Status_Type_Enum::ACTIVE,
            'loginStatus' => Status_Type_Enum::ACTIVE
        ));
        if ($driver_logged_status) {
            // get the trip details
            $trip_details = $this->Trip_Details_Model->getById($trip_id);
            if ($trip_details) {

                // check trip avilable for driver/any in progress trip before logout
                $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                    'tripId' => $trip_id
                ));

                if ($check_trip_avilablity) {
                    if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED) {
                        //getting valid & avilable promocode if trip is first ride for passenger & if no promocode applied by passenger
                        $promocode = '';
                        if (!$trip_details->promoCode) {
                            $promocode = $this->Passenger_Promocode_Details_Model->getFreeRidePromoCode($trip_details->passengerId);
                        }
                        if ($trip_details->companyId == ZOOMCAR_COMPANYID) {
                            $zoom_startTrip_response = $this->Api_Webservice_Model->zoomCarUpdateTripStatus($trip_id, $trip_details->bookingKey, 1);
                            if ($zoom_startTrip_response['status'] == 'success') {
                                $db_trip_update = 1;
                            } else {
                                $message = array(
                                    "message" => 'Zoomcar API failed. Failure Message: ' . $zoom_checklist_response['message'],
                                    "status" => -1
                                );
                            }
                        } else {
                            $db_trip_update = 1;
                        }
                        if ($db_trip_update) {
                            // To update trip status on tripdetails
                            $trip_status_update = $this->Trip_Details_Model->update(array(
                                'tripStatus' => Trip_Status_Enum::IN_PROGRESS,
                                'notificationStatus' => Trip_Status_Enum::IN_PROGRESS,
                                'actualPickupDatetime' => getCurrentDateTime(),
                                'promoCode' => ($promocode) ? $promocode : ''
                            ), array(
                                'id' => $trip_id,
                                'driverId' => $driver_id
                            ));

                            if ($trip_status_update) {
                                // To update trip status on driverrequesttripdetails
                                $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                    'tripStatus' => Driver_Request_Status_Enum::DRIVER_ACCEPTED
                                ), array(
                                    'tripId' => $trip_id,
                                    'selectedDriverId' => $driver_id
                                ));
                            }
                            if ($trip_request_status_update) {
                                // inserting the start trip location of driver into temp tracking table for particular trip
                                $insert_data = array(
                                    'tripId' => $trip_id,
                                    'latitude' => $latitude,
                                    'longitude' => $longitude
                                );
                                $trip_tracking_update = $this->Trip_Temp_Tracking_Info_Model->insert($insert_data);
                                $message = array(
                                    "message" => $msg_data ['driver_start_trip'],
                                    "status" => 1
                                );
                            } else {
                                $message = array(
                                    "message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
                                    "status" => -1
                                );
                                // log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
                            }
                        }
                    } else {

                        if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                            $message = array(
                                "message" => $msg_data ['trip_reject_passenger'],
                                "status" => -1
                            );
                        } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                            $message = array(
                                "message" => $msg_data ['trip_completed'],
                                "status" => -1
                            );
                        }
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['invalid_trip_request'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['invalid_trip'],
                    "status" => -1
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['driver_login_failed'],
                "status" => -1
            );
        }
        echo json_encode($message);
    }

    public function EndTrip()
    {
        // get API messages from message.php
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $data_auth_key = FALSE;
        $auth_key = NULL;
        $auth_user_id = NULL;
        $auth_user_type = NULL;
        $message = '';
        $driver_logged_status = NULL;
        $check_trip_avilablity = NULL;
        $trip_status_update = NULL;
        $trip_request_status_update = NULL;
        $rate_card_details = NULL;

        // postdata intialized
        $trip_tracking_update = NULL;
        $wallet_transaction_id = NULL;
        $driver_id = NULL;
        $trip_id = NULL;
        $drop_latitude = NULL;
        $drop_longitude = NULL;
        $drop_location = NULL;
        $drop_time = NULL;
        $distance = NULL;
        $actual_distance = NULL;
        $waiting_hour = NULL;
        $trip_type = NULL;

        $other_fee = NULL;
        $parking_fee = NULL;
        $is_admin_control = NULL;
        $rating = NULL;
        $comments = NULL;

        $response_data = array();
        $post_data = $this->input->post();

        $trip_id = $post_data['trip_id'];
        $drop_latitude = $post_data['drop_latitude'];
        $drop_longitude = $post_data['drop_longitude'];
        $drop_location = $post_data['drop_location'];
        $drop_time = $post_data['drop_time'];
        $distance = $post_data['distance'];
        $actual_distance = $post_data['actual_distance'];
        $waiting_hour = $post_data['waiting_hour'];
        $trip_type = $post_data['plan_type'];
        $db_trip_update = 0;
        $is_master_trip = 0;
        $total_subtrips = 0;

        if (array_key_exists('other_fee', $post_data)) {
            $other_fee = $post_data['other_fee'];
        }
        if (array_key_exists('parking_fee', $post_data)) {
            $parking_fee = $post_data['parking_fee'];
        }
        if (array_key_exists('is_admin_control', $post_data)) {
            $is_admin_control = $post_data['is_admin_control'];
        }
        if (array_key_exists('rating', $post_data)) {
            $rating = $post_data['rating'];
        }
        if (array_key_exists('comments', $post_data)) {
            $comments = $post_data['comments'];
        }
        // To get site info details
        $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
            'id' => 1
        ));

        // actual functionality
        if ($trip_id > 0) {
            // get the trip details
            $trip_details = $this->Trip_Details_Model->getById($trip_id);

            if ($trip_details) {
                if ($trip_details->driverId) {
                    // change driver as logged in
                    $data['loginStatus'] = Status_Type_Enum::ACTIVE;
                    $driver_logged_status = $this->Driver_Model->update($data, array(
                        'id' => $driver_id
                    ));
                    $driver_logged_status = $this->Driver_Model->getOneByKeyValueArray(array(
                        'id' => $trip_details->driverId,
                        'status' => Status_Type_Enum::ACTIVE,
                        'loginStatus' => Status_Type_Enum::ACTIVE
                    ));

                    if ($driver_logged_status) {
                        // get check wheather trip is allredy ended but waiting for payment
                        if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT) {
                            $message = array(
                                "message" => $msg_data['trip_waiting_payment'],
                                "status" => -1
                            );
                            echo json_encode($message);
                            exit();
                        }
                        // check trip avilable for driver/any in progress trip before logout
                        $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                            'tripId' => $trip_id
                        ));
                        if ($check_trip_avilablity) {
                            if ($check_trip_avilablity->tripStatus != Driver_Request_Status_Enum::DRIVER_REJECTED
                                && $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED
                            ) {
                                if ($trip_details->companyId == ZOOMCAR_COMPANYID) {
                                    $zoom_startTrip_response = $this->Api_Webservice_Model->zoomCarUpdateTripStatus($trip_id, $trip_details->bookingKey, 2);
                                    if ($zoom_startTrip_response['status'] == 'success') {
                                        $db_trip_update = 1;
                                    } else {
                                        $message = array(
                                            "message" => 'Zoomcar API failed. Failure Message: ' . $zoom_checklist_response['message'],
                                            "status" => -1
                                        );
                                    }
                                } else {
                                    if ($trip_details->companyId != DEFAULT_COMPANY_ID) {
                                        $all_sub_trips = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId' => $trip_id));
                                        if ($all_sub_trips) {
                                            $last_sub_trip_details = $this->Sub_Trip_Details_Model->getOneByKeyValue('masterTripId', $trip_id, 'dropDatetime Desc');
                                            if (strtotime($drop_time) < strtotime($last_sub_trip_details->dropDatetime)) {
                                                $message = array(
                                                    "message" => $msg_data['invalid_master_trip_drop_time'],
                                                    "status" => -1
                                                );
                                            } else {
                                                $total_subtrips = count($all_sub_trips);
                                                $db_trip_update = 1;
                                                $is_master_trip = 1;
                                            }
                                        } else {
                                            if ($trip_details->billType == Bill_Type_Enum::FIXED) {
                                                $message = array(
                                                    "message" => 'Trip Should have atleast one sub-trip.',
                                                    "status" => -1
                                                );
                                            } else {
                                                $db_trip_update = 1;
                                                $is_master_trip = 0;
                                            }
                                        }
                                    } else {
                                        $db_trip_update = 1;
                                    }
                                }

                                if ($db_trip_update) {
                                    // intialized for calculation purpose
                                    $actual_pickup_time = $trip_details->actualPickupDatetime;
                                    $actual_drop_time = date('Y-m-d H:i:s', strtotime($drop_time)); //getCurrentDateTime();
                                    $total_calculated_hours = 0;
                                    $travelled_staged_hours = 0;
                                    $travelled_hours = 0;
                                    $travelled_minutes = 0;
                                    $travelled_seconds = 0;
                                    $travel_hours_minutes = 0;
                                    $convenience_charge = 0;
                                    $travel_charge = 0;
                                    $total_trip_cost = 0;
                                    $promo_discount_amount = 0;
                                    $company_tax = 0;
                                    $wallet_payment_amount = 0;

                                    if ($trip_details->billType == Bill_Type_Enum::MONTHLY) {
                                        // get rate card details based on the cityId & trip type
                                        $rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray(array(
                                            'cityId' => $trip_details->bookingCityId,
                                            'companyId' => $trip_details->companyId,
                                            'tripType' => $trip_details->tripType,
                                            'status' => Status_Type_Enum::ACTIVE,
                                            'isMonthly' => Status_Type_Enum::ACTIVE
                                        ));
                                    } else {
                                        // get rate card details based on the cityId & trip type
                                        $rate_card_details = $this->Rate_Card_Details_Model->getOneByKeyValueArray(array(
                                            'cityId' => $trip_details->bookingCityId,
                                            'companyId' => $trip_details->companyId,
                                            'tripType' => $trip_details->tripType,
                                            'status' => Status_Type_Enum::ACTIVE
                                        ));
                                    }

                                    if (strtotime($actual_pickup_time) < strtotime($actual_drop_time)) {

                                        $dateDiff = intval((strtotime($actual_pickup_time) - strtotime($actual_drop_time)) / 60);
                                        $total_calculated_hours = round(abs(strtotime($actual_pickup_time) - strtotime($actual_drop_time)) / 3600, 2);
                                        $travelled_hours = intval(abs($dateDiff / 60));
                                        $travelled_minutes = intval(abs($dateDiff % 60));
                                        $travelled_seconds = intval(abs((strtotime($actual_pickup_time) - strtotime($actual_drop_time)) % 3600) % 60);

                                        if ($travelled_seconds > 0) {
                                            $travelled_seconds = 0;
                                            $travelled_minutes += 1;
                                        }
                                        if ($travelled_minutes == 60) {
                                            $travelled_minutes = 0;
                                            $travelled_hours += 1;
                                        } else {
                                            $travelled_staged_hours = $travelled_hours;
                                            /*
                                             * if (($travelled_minutes > 0) && ($travelled_minutes < 60)) {
                                             * $travelled_staged_hours = $travelled_hours + 1;
                                             * }
                                             */
                                        }

                                        $travel_hours_minutes = $travelled_hours .
                                            '.' . $travelled_minutes;
                                        //debug(strtotime($actual_pickup_time));
                                        //debug_exit(date('H:i:s', strtotime($actual_pickup_time)));
                                        if (date('H:i:s', strtotime($actual_pickup_time)) >= DAY_START_TIME && date('H:i:s', strtotime($actual_pickup_time)) <= DAY_END_TIME) {
                                            if ($trip_details->billType == Bill_Type_Enum::MONTHLY) {
                                                if ($travelled_staged_hours < 4) {
                                                    $convenience_charge += $rate_card_details->dayConvenienceCharge;
                                                }
                                            } else if ($trip_details->billType == Bill_Type_Enum::HOURLY) {
                                                $convenience_charge += $rate_card_details->dayConvenienceCharge;
                                            }

                                        } else {
                                            //if (date('H:i:s', strtotime($actual_pickup_time)) >= NIGHT_START_TIME && date('H:i:s', strtotime($actual_pickup_time)) <= NIGHT_END_TIME)
                                            if ($trip_details->billType == Bill_Type_Enum::MONTHLY) {
                                                if ($travelled_staged_hours < 4) {
                                                    $convenience_charge += $rate_card_details->nightConvenienceCharge;
                                                }
                                            } else if ($trip_details->billType == Bill_Type_Enum::HOURLY) {
                                                $convenience_charge += $rate_card_details->nightConvenienceCharge;
                                            }
                                        }

                                        if (count($rate_card_details) > 0) {
                                            if ($trip_details->billType == Bill_Type_Enum::HOURLY || $trip_details->billType == Bill_Type_Enum::MONTHLY) {
                                                // calculation based on HOURLY i.e transit

                                                if ($travelled_minutes == 0 && ($travelled_seconds > 0)) {

                                                    $travel_charge += $rate_card_details->firstHourCharge;
                                                }

                                                if ($travelled_hours == 0 && ($travelled_minutes > 0)) {

                                                    $travel_charge += $rate_card_details->firstHourCharge;
                                                }

                                                if ($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY || $trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) {
                                                    $travelled_staged_hours = 0;
                                                    if ($travelled_hours > OUT_STATION_TIME_LIMIT) {
                                                        $travelled_staged_hours = $travelled_hours - OUT_STATION_TIME_LIMIT;
                                                    }
                                                    $distance_travelled = explode('.', $rate_card_details->minDistance);

                                                    if ($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_ONEWAY) {

                                                        if ($actual_distance <= $distance_travelled[0]) {
                                                            $travel_charge = $actual_distance * $rate_card_details->distanceConvenienceCharge;

                                                            $total_trip_cost = $convenience_charge + $travel_charge;
                                                        } else {
                                                            $travel_charge = $actual_distance * $rate_card_details->distanceCostPerKm;

                                                            $total_trip_cost = $convenience_charge + $travel_charge;
                                                        }
                                                    } else if ($trip_details->tripType == Trip_Type_Enum::OUTSTATION_TRIP_RETURN) {

                                                        if ($actual_distance <= $distance_travelled[0]) {
                                                            $travel_charge = $actual_distance * $rate_card_details->distanceConvenienceCharge;

                                                            $total_trip_cost = $convenience_charge + $travel_charge;
                                                        } else {
                                                            $travel_charge = $actual_distance * $rate_card_details->distanceCostPerKm;

                                                            $total_trip_cost = $convenience_charge + $travel_charge;
                                                        }
                                                    }
                                                }
                                                // claculation based on travel_hours_minutes
                                                if ($travelled_staged_hours > 0) {
                                                    switch ($travelled_staged_hours) {
                                                        case 1: {
                                                            // $travel_charge+=$rate_card_details->firstHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->firstHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->secondHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->secondHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 2: {
                                                            // $travel_charge+=$rate_card_details->secondHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->secondHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->thirdHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->thirdHourCharge;
                                                            }
                                                            break;
                                                        }

                                                        case 3: {
                                                            // $travel_charge+=$rate_card_details->thirdHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->thirdHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->fourthHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->fourthHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 4: {
                                                            // $travel_charge+=$rate_card_details->fourthHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->fourthHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->fifthHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->fifthHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 5: {
                                                            // $travel_charge+=$rate_card_details->fifthHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->fifthHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->sixthHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->sixthHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 6: {
                                                            // $travel_charge+=$rate_card_details->sixthHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->sixthHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->seventhHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->seventhHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 7: {
                                                            // $travel_charge+=$rate_card_details->seventhHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->seventhHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->eighthHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->eighthHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 8: {
                                                            // $travel_charge+=$rate_card_details->eighthHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->eighthHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->ninthHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->ninthHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 9: {
                                                            // $travel_charge+=$rate_card_details->ninthHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->ninthHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->tenthHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->tenthHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 10: {
                                                            // $travel_charge+=$rate_card_details->tenthHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->tenthHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->eleventhHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->eleventhHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 11: {
                                                            // $travel_charge+=$rate_card_details->eleventhHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->eleventhHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->twelveHourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->twelveHourCharge;
                                                            }
                                                            break;
                                                        }
                                                        case 12: {
                                                            // $travel_charge+=$rate_card_details->twelveHourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->twelveHourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->above12HourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->above12HourCharge;
                                                            }
                                                            break;
                                                        }
                                                        default: {
                                                            // $travel_charge+=$rate_card_details->above12HourCharge;

                                                            $travel_charge += $travelled_hours * $rate_card_details->above12HourCharge;
                                                            if (($travelled_minutes > 0)) {
                                                                // $travel_charge+=$rate_card_details->above12HourCharge;
                                                                $travel_charge += $travelled_minutes / 60 * $rate_card_details->above12HourCharge;
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }

                                                $total_trip_cost = $convenience_charge + $travel_charge;
                                                $total_trip_cost = round($total_trip_cost, 2);
                                                $travel_charge = $total_trip_cost;
                                            } else if ($trip_details->billType == Bill_Type_Enum::FIXED) {
                                                // No calculation it is just billing the fixed price + tax with promo discount
                                                if ($is_master_trip) {
                                                    $travel_charge = ($rate_card_details->fixedAmount * $total_subtrips) - $convenience_charge;
                                                    $total_trip_cost = round($rate_card_details->fixedAmount * $total_subtrips, 2);
                                                } else {
                                                    $travel_charge = $rate_card_details->fixedAmount - $convenience_charge;
                                                    $total_trip_cost = round($rate_card_details->fixedAmount, 2);
                                                }
                                            } else if ($trip_details->billType == Bill_Type_Enum::DISTANCE) {
                                                // @todo calculation based on distance min-distance fixed + distance cost per distance
                                                $convenience_charge = $rate_card_details->distanceConvenienceCharge;
                                                $distance_travelled = explode('.', $rate_card_details->minDistance);

                                                if ($travelled_hours <= $distance_travelled[0]) {
                                                    $travel_charge = 0;
                                                    $total_trip_cost = $convenience_charge;
                                                } else {
                                                    $travelled_hours = $travelled_hours - $rate_card_details->minDistance;

                                                    $travel_charge = $travelled_hours * $rate_card_details->distanceCostPerKm;
                                                    if ($travelled_minutes > 0) {
                                                        if ($travelled_minutes > $distance_travelled[1]) {

                                                            // $travel_charge+=$rate_card_details->timeCostPerHour;
                                                            $travel_charge += $distance_travelled[1] % 1000 * $rate_card_details->timeCostPerHour;
                                                        }
                                                        $total_trip_cost = $convenience_charge + $travel_charge;
                                                    }
                                                }
                                            } else if ($trip_details->billType == Bill_Type_Enum::TIME) {
                                                // calculation based on time min-time fixed + time cost per hour
                                                $convenience_charge = $rate_card_details->timeConvenienceCharge;
                                                $hours = explode('.', $rate_card_details->minTime);
                                                if ($travelled_hours <= $hours[0]) {
                                                    $travel_charge = 0;
                                                    $total_trip_cost = $convenience_charge;
                                                } else {
                                                    $travelled_hours = $travelled_hours - $rate_card_details->minTime;

                                                    $travel_charge = $travelled_hours * $rate_card_details->timeCostPerHour;
                                                    if ($travelled_minutes > 0) {
                                                        if ($travelled_minutes > $hours[1]) {

                                                            // $travel_charge+=$rate_card_details->timeCostPerHour;
                                                            $travel_charge += $hours[1] / 60 * $rate_card_details->timeCostPerHour;
                                                        }
                                                        $total_trip_cost = $convenience_charge + $travel_charge;
                                                    }
                                                }
                                            } else {
                                                $message = array(
                                                    "message" => $msg_data['invalid_bill_type'],
                                                    "status" => -1
                                                );
                                            }

                                            // calculate promocode discount if applicable
                                            if (!empty($trip_details->promoCode) && $trip_details->promoCode != '') {

                                                // Check promocode exists or not for particular passenger at particular city
                                                $promocode_exists = $this->Api_Webservice_Model->checkPromocodeAvailablity($trip_details->promoCode, $trip_details->passengerId, $trip_details->bookingCityId);

                                                if ($promocode_exists && count($promocode_exists) > 0) {
                                                    if (strtotime($promocode_exists[0]->promoStartDate) < getCurrentUnixDateTime() && strtotime($promocode_exists[0]->promoEndDate) > getCurrentUnixDateTime()) {
                                                        // check promocode limit exceeds or not for specific passenger (note: only completed trip)
                                                        $promocode_user_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                                                            'passengerId' => $trip_details->passengerId,
                                                            'promoCode' => $trip_details->promoCode,
                                                            'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                                                        ));
                                                        // check promocode limit exceeds or not total limit (note: only completed trip)
                                                        $promocode_limit_exists = $this->Trip_Details_Model->getByKeyValueArray(array(
                                                            'promoCode' => $trip_details->promoCode,
                                                            'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                                                        ));

                                                        if ((count($promocode_limit_exists) < $promocode_exists[0]->promoCodeLimit) && (count($promocode_user_limit_exists) < $promocode_exists[0]->promoCodeUserLimit)) {
                                                            // get promo code details
                                                            $promocode_details = $this->Passenger_Promocode_Details_Model->getOneByKeyValueArray(array(
                                                                'promoCode' => $trip_details->promoCode
                                                            ));

                                                            $promo_discount = $promocode_details->promoDiscountAmount;
                                                            $promo_type = $promocode_details->promoDiscountType;
                                                            if ($promo_type == Promocode_Type_Enum::PERCENTAGE) {
                                                                // discount in %
                                                                $calculate_amt = ($promo_discount / 100) * $total_trip_cost;
                                                                $promo_discount_amount = round($calculate_amt, 2);
                                                                $total_trip_cost = $total_trip_cost - $promo_discount_amount;
                                                                $promo_discount = $promo_discount_amount;
                                                            } else if ($promo_type == Promocode_Type_Enum::FIXED) {
                                                                // discount in Rs
                                                                if ($promo_discount > $total_trip_cost) {

                                                                    $promo_discount_amount = $total_trip_cost;
                                                                    $promo_discount = $promo_discount_amount;
                                                                } else {

                                                                    $promo_discount_amount = $promo_discount;
                                                                }
                                                                $total_trip_cost = $total_trip_cost - $promo_discount;
                                                            }
                                                            if ($total_trip_cost < 0) {
                                                                $total_trip_cost = 0;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            // tax calculation based on data of site info table
                                            if ($total_trip_cost > 0) {
                                                $tax = $site_setting_info->tax;
                                                if ($tax > 0) {
                                                    $company_tax = ($tax / 100) * $total_trip_cost;
                                                    $company_tax = round($company_tax, 2);
                                                    $total_trip_cost += $company_tax;
                                                }
                                            }
                                            // deduction from wallet if payment mode is wallet
                                            if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET) {

                                                if ($total_trip_cost > 0) {
                                                    // get current wallet amount of passenger
                                                    $current_wallet_amount = $this->Passenger_Model->getById($trip_details->passengerId);

                                                    if ($total_trip_cost >= $current_wallet_amount->walletAmount) {
                                                        $wallet_payment_amount = $current_wallet_amount->walletAmount;
                                                        // $total_trip_cost = $total_trip_cost - $current_wallet_amount->walletAmount;
                                                        $remaing_wallet_amount['walletAmount'] = 0;
                                                    } else {
                                                        $actual_wallet_amount = $current_wallet_amount->walletAmount;
                                                        $current_wallet_amount->walletAmount = $current_wallet_amount->walletAmount - $total_trip_cost;
                                                        $wallet_payment_amount = $actual_wallet_amount - $current_wallet_amount->walletAmount;
                                                        $remaing_wallet_amount['walletAmount'] = $current_wallet_amount->walletAmount;
                                                        // $total_trip_cost = 0;
                                                    }
                                                    $wallet_payment_update = $this->Passenger_Model->update($remaing_wallet_amount, array(
                                                        'id' => $trip_details->passengerId
                                                    ));
                                                }

                                            }

                                            // update the trip details trip status to waiting for payment ,driver shift status to free also trip request details trip status to completed

                                            $trip_status = NULL;
                                            $notification_status = NULL;
                                            if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
                                                $trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
                                                $notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
                                            } else if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
                                                $trip_status = Trip_Status_Enum::NO_NOTIFICATION;
                                                $notification_status = Trip_Status_Enum::NO_NOTIFICATION;
                                            } else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
                                                $trip_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
                                                $notification_status = Trip_Status_Enum::WAITING_FOR_PAYMENT;
                                            }
                                            $trip_status_update = $this->Trip_Details_Model->update(array(
                                                'tripStatus' => $trip_status,
                                                'notificationStatus' => $notification_status,
                                                'dropLocation' => $drop_location,
                                                'dropLatitude' => $drop_latitude,
                                                'dropLongitude' => $drop_longitude,
                                                'dropDatetime' => $actual_drop_time
                                            ), array(
                                                'id' => $trip_id
                                            ));

                                            $is_first_ride = $this->Trip_Details_Model->getByKeyValueArray(array('passengerId' => $trip_details->passengerId,
                                                'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED
                                            ), 'id');
                                            if (count($is_first_ride) <= 0) {
                                                $passenger_referral_details = $this->Passenger_Referral_Details_Model->getOneByKeyValueArray(array('registeredPassengerId' => $trip_details->passengerId), 'id DESC');
                                                if ($passenger_referral_details) {
                                                    $passenger_referral_update = $this->Passenger_Referral_Details_Model->update(array('registeredTripId' => $trip_id, 'isReferrerEarned' => Status_Type_Enum::ACTIVE), array('registeredPassengerId' => $trip_details->passengerId));
                                                    $passenger_details = $this->Passenger_Model->getById($passenger_referral_details->passengerId);
                                                    $current_balance = $passenger_details->walletAmount + $passenger_referral_details->earnedAmount;
                                                    $insert_data = array('passengerId' => $passenger_referral_details->passengerId, 'passengerReferralId' => $trip_details->passengerId,
                                                        'tripId' => $trip_id, 'transactionAmount' => $passenger_referral_details->earnedAmount,
                                                        'previousAmount' => $passenger_details->walletAmount, 'currentAmount' => $current_balance, 'transactionStatus' => 'Success',
                                                        'transactionType' => Transaction_Type_Enum::REFERRAL,
                                                        'manipulationType' => Manipulation_Type_Enum::ADDITION
                                                    );
                                                    $passenger_wallet_updated = $this->Passenger_Wallet_Details_Model->insert($insert_data);
                                                    $passenger_updated = $this->Passenger_Model->update(array('walletAmount' => $current_balance), array('id' => $passenger_referral_details->passengerId));
                                                }
                                            }
                                            // $trip_request_status_update=$this->Driver_Request_Details_Model->update(array('tripStatus'=>Driver_Request_Status_Enum::COMPLETED_TRIP),array('tripId'=>$trip_id));
                                            // $driver_shift_status_update=$this->Driver_Shift_History_Model->update(array('availabilityStatus'=>Driver_Available_Status_Enum::FREE),array('driverId'=>$trip_details->driverId));
                                            // get the temp tracking lat & log to update the tracked trip
                                            $temp_tracking_info = $this->Trip_Temp_Tracking_Info_Model->getByKeyValueArray(array(
                                                'tripId' => $trip_id
                                            ));
                                            if (count($temp_tracking_info) > 0) {
                                                $tracking_details = '';
                                                foreach ($temp_tracking_info as $list) {
                                                    if (empty($tracking_details)) {
                                                        $tracking_details = '[' . $list->latitude .
                                                            ',' . $list->longitude .
                                                            ']';
                                                    } else {
                                                        $tracking_details .= ',[' . $list->latitude .
                                                            ',' . $list->longitude .
                                                            ']';
                                                    }
                                                }
                                                $tracking_details .= ',[' . $drop_latitude .
                                                    ',' . $drop_longitude .
                                                    ']';

                                                $insert_data = array(
                                                    'tripId' => $trip_id,
                                                    'latitudeLogitude' => $tracking_details,
                                                    'passengerId' => $trip_details->passengerId,
                                                    'driverId' => $trip_details->driverId
                                                );

                                                $trip_tracking_update = $this->Trip_Tracked_Info_Model->insert($insert_data);

                                                // delete the temp tracking lat & log
                                                $delete_temp_tracking_info = $this->Trip_Temp_Tracking_Info_Model->delete(array(
                                                    'tripId' => $trip_id
                                                ));
                                            }

                                            // admin control trip end i.e trip fare update
                                            if (array_key_exists('is_admin_control', $post_data) && $is_admin_control) {
                                                // Intialized

                                                $data = array();
                                                $message = '';

                                                $transaction_exists = NULL;

                                                $driver_shift_status_update = NULL;
                                                $transaction_insert_id = NULL;

                                                $actual_amount = NULL;

                                                $passenger_promo_discount = NULL;

                                                $wallet_payment = NULL;
                                                $promo_discount = NULL;
                                                $nightfare_applicable = NULL;
                                                $nightfare = NULL;
                                                $waiting_time = NULL;
                                                $waiting_cost = NULL;
                                                $minutes_traveled = NULL;
                                                $minutes_fare = NULL;
                                                $overtime_charge = NULL;
                                                $passenger_discount = NULL;
                                                $admin_amount = NULL;
                                                $company_amount = NULL;
                                                $invoice_no = NULL;
                                                $driver_earning = NULL;

                                                $statistics = array();

                                                $passenger_promo_discount = $promo_discount_amount;
                                                $wallet_payment = round($wallet_payment_amount, 2);
                                                $promo_discount = $promo_discount_amount;
                                                $nightfare_applicable = 0;
                                                $nightfare = 0;
                                                $waiting_time = 0;
                                                $waiting_cost = 0;
                                                $minutes_traveled = $travel_hours_minutes;
                                                $minutes_fare = 0;
                                                $passenger_discount = $promo_discount_amount;

                                                // actual functionality
                                                // get the trip details
                                                $trip_details = $this->Trip_Details_Model->getById($trip_id);

                                                // to get site information details
                                                $site_setting_info = $this->Site_Details_Model->getOneByKeyValueArray(array(
                                                    'id' => 1
                                                ));
                                                // to get currency code/currency symbol currencySymbol
                                                $currency_details = $this->Country_Model->getColumnByKeyValueArray('currencySymbol', array(
                                                    'id' => $site_setting_info->countryId
                                                ));

                                                // start get driver statistics
                                                // to get the driver rejected/cancelled trip
                                                $driver_rejected_count = $this->Driver_Rejected_Trip_Details_Model->getColumnByKeyValueArray('driverId', array(
                                                    'driverId' => $driver_logged_status->id,
                                                    'createdDate >=' => getCurrentDate(),
                                                    'createdDate <=' => getCurrentDate()
                                                ));


                                                // calculate admin amount & company amount
                                                $admin_amount = ($site_setting_info->adminCommission / 100) * $total_trip_cost;
                                                $admin_amount = round($admin_amount, 2);
                                                $company_amount = $total_trip_cost - $admin_amount;
                                                $company_amount == round($company_amount, 2);

                                                // genereate invoice No
                                                $invoice_no = $trip_details->tripType .
                                                    '-' . $trip_details->paymentMode .
                                                    '-' . $trip_id;
                                                $total_trip_cost_insert = $total_trip_cost + $other_fee + $parking_fee;
                                                $driver_earning = 0;
                                                // calculation for driver earning based on driver type
                                                if ($driver_logged_status->driverType == Driver_Type_Enum::COMMISSIONBASED) {
                                                    $driver_earning = ($driver_logged_status->commissionTripEarning / 100) * $total_trip_cost;
                                                    $driver_earning = round($driver_earning, 2);
                                                } else if ($driver_logged_status->driverType == Driver_Type_Enum::FIXEDBASED) {
                                                    $driver_earning = round($driver_logged_status->fixedTripEarning, 2);
                                                }

                                                $passenger_details = $this->Passenger_Model->getById($trip_details->passengerId);

                                                $insert_data = array(
                                                    'tripId' => $trip_id,
                                                    'travelDistance' => $actual_distance,
                                                    'travelHoursMinutes' => $minutes_traveled,
                                                    'convenienceCharge' => $convenience_charge,
                                                    'travelCharge' => $travel_charge - $convenience_charge,
                                                    'parkingCharge' => $parking_fee,
                                                    'tollCharge' => $other_fee,
                                                    'companyTax' => $company_tax,
                                                    'totalTripCost' => $total_trip_cost_insert,
                                                    'adminAmount' => $admin_amount,
                                                    'companyAmount' => $company_amount,
                                                    'promoDiscountAmount' => $passenger_promo_discount,
                                                    'walletPaymentAmount' => $wallet_payment,
                                                    'otherDiscountAmount' => 0,
                                                    'driverEarning' => $driver_earning,
                                                    'paymentMode' => $trip_details->paymentMode,
                                                    'paymentStatus' => ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) ? '' : (($trip_details->paymentMode != Payment_Mode_Enum::PAYTM) ? 'Success' : 'Waiting for PAYTM payment'),
                                                    'transactionId' => 0,
                                                    'invoiceNo' => $invoice_no,
                                                    'otherCharge' => 0
                                                );

                                                // to check transcation table exits trip record
                                                $transaction_exists = $this->Trip_Transaction_Details_Model->getOneByKeyValueArray(array(
                                                    'tripId' => $trip_id
                                                ));
                                                if ($transaction_exists) {
                                                    // update the transaction table
                                                    $transaction_insert_id = $this->Trip_Transaction_Details_Model->update($insert_data, array(
                                                        'id' => $transaction_exists->id
                                                    ));
                                                    $transaction_insert_id = $transaction_exists->id;
                                                } else {
                                                    // insert data to transaction table
                                                    $transaction_insert_id = $this->Trip_Transaction_Details_Model->insert($insert_data);
                                                }

                                                if ($transaction_insert_id) {
                                                    if ($driver_logged_status->driverType != Driver_Type_Enum::COMMISSIONBASED && $driver_logged_status->driverType != Driver_Type_Enum::FIXEDBASED) {
                                                        //Driver earning history
                                                        $driver_earning_for_this_trip = $this->calculate_driver_earning($trip_id, $trip_details->driverId);
                                                        $this->Trip_Transaction_Details_Model->update(array('driverEarning' => $driver_earning_for_this_trip), array('id' => $transaction_insert_id));
                                                    }
                                                    //passenger transaction history
                                                    $transaction_amount = $total_trip_cost_insert - $wallet_payment;
                                                    if ($transaction_amount > 0 && $passenger_details) {
                                                        $insert_wallet_transaction = array('passengerId' => $passenger_details->id, 'tripId' => $trip_id, 'transactionAmount' => $transaction_amount, 'transactionStatus' => 'Success', 'transactionType' => Transaction_Type_Enum::TRIP, 'manipulationType' => Manipulation_Type_Enum::SUBTRACTION);
                                                        $wallet_transaction_id = $this->Passenger_Wallet_Details_Model->insert($insert_wallet_transaction);
                                                    }
                                                    if ($wallet_payment > 0 && $passenger_details) {
                                                        $current_amount = $passenger_details->walletAmount - $wallet_payment;
                                                        $insert_wallet_transaction = array('passengerId' => $passenger_details->id, 'tripId' => $trip_id, 'transactionAmount' => $wallet_payment, 'previousAmount' => $passenger_details->walletAmount, 'currentAmount' => $current_amount, 'transactionStatus' => 'Success', 'transactionType' => Transaction_Type_Enum::TRIP, 'manipulationType' => Manipulation_Type_Enum::SUBTRACTION);
                                                        $wallet_transaction_id = $this->Passenger_Wallet_Details_Model->insert($insert_wallet_transaction);
                                                    }
                                                    $trip_status = NULL;
                                                    $notification_status = NULL;
                                                    if ($trip_details->paymentMode == Payment_Mode_Enum::WALLET || $trip_details->paymentMode == Payment_Mode_Enum::CASH) {
                                                        $trip_status = Trip_Status_Enum::TRIP_COMPLETED;
                                                        $notification_status = Trip_Status_Enum::NO_NOTIFICATION;
                                                    } else if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
                                                        $trip_status = Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT;
                                                        $notification_status = Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT;
                                                    } else if ($trip_details->paymentMode == Payment_Mode_Enum::INVOICE) {
                                                        $trip_status = Trip_Status_Enum::TRIP_COMPLETED;
                                                        $notification_status = Trip_Status_Enum::TRIP_COMPLETED;
                                                    }

                                                    if ($trip_details->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT || $trip_details->tripStatus == Trip_Status_Enum::NO_NOTIFICATION) {
                                                        $trip_status_update = $this->Trip_Details_Model->update(array(
                                                            'tripStatus' => $trip_status,
                                                            'notificationStatus' => $notification_status
                                                        ), array(
                                                            'id' => $trip_id
                                                        ));

                                                        $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                                            'tripStatus' => Driver_Request_Status_Enum::COMPLETED_TRIP,
                                                            'selectedDriverId' => 0
                                                        ), array(
                                                            'tripId' => $trip_id
                                                        ));

                                                        //$driver_daily_earning=$this->driver_daily_earning($trip_details->dropDatetime, $trip_details->driverId);
                                                        // get last inserted shift id for particular driver
                                                        $get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                                            'driverId' => $trip_details->driverId
                                                        ), 'id DESC');
                                                        $driver_shift_status_update = $this->Driver_Shift_History_Model->update(array(
                                                            'availabilityStatus' => Driver_Available_Status_Enum::FREE
                                                        ), array(
                                                            'id' => $get_last_shift_id->id
                                                        ));
                                                    }

                                                    // response data
                                                    $response_data = array(
                                                        "fare" => $total_trip_cost,
                                                        "pickup" => $trip_details->pickupLocation,
                                                        "invoice_no" => $invoice_no,
                                                        "trip_id" => $trip_id
                                                    );

                                                    $driver_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id);
                                                    $driver_today_earning_details = $this->Api_Webservice_Model->getDriverEarningDetails($driver_logged_status->id, 1);
                                                    // To calculate time driven on current date/today
                                                    $total_amount = 0;
                                                    $total_today_amount = 0;
                                                    $time_result = '00:00';
                                                    $actual_pickup_time = '';
                                                    $drop_time = '';
                                                    $hours = '';
                                                    $minutes = '';
                                                    $seconds = '';
                                                    $date_difference = "";
                                                    $total_differnce = "";
                                                    foreach ($driver_earning_details as $get_details) {
                                                        $actual_pickup_time = strtotime($get_details->actualPickupDatetime);
                                                        $drop_time = strtotime($get_details->dropDatetime);
                                                        // echo $actual_pickup_time;
                                                        // echo '-';
                                                        // echo $drop_time;
                                                        // echo '<br>';
                                                        $date_difference = abs($drop_time - $actual_pickup_time);
                                                        $total_differnce += $date_difference;
                                                        // to get total earned amount by the driver
                                                        $total_amount += $get_details->driverEarning;
                                                    }
                                                    foreach ($driver_today_earning_details as $get_details) {
                                                        // to get total earned amount by the driver
                                                        $total_today_amount += $get_details->driverEarning;
                                                    }
                                                    // $date_difference = $drop_time - $actual_pickup_time;
                                                    $hours += floor((($total_differnce % 604800) % 86400) / 3600);
                                                    $minutes += floor(((($total_differnce % 604800) % 86400) % 3600) / 60);
                                                    $seconds += floor((((($total_differnce % 604800) % 86400) % 3600) % 60));
                                                    $time_result = $minutes .
                                                        ':' . $seconds;
                                                    // To calculate time driven on current date/today

                                                    $statistics = array(
                                                        "drivername" => $driver_logged_status->firstName .
                                                            ' ' . $driver_logged_status->lastName,
                                                        "total_trip" => count($driver_earning_details),
                                                        "cancel_trips" => count($driver_rejected_count),
                                                        "total_earnings" => round($total_amount, 2),
                                                        "overall_rejected_trips" => count($driver_rejected_count),
                                                        "today_earnings" => round($total_today_amount, 2),
                                                        "shift_status" => Driver_Shift_Status_Enum::SHIFTIN,
                                                        "time_driven" => $time_result,
                                                        "status" => 1
                                                    );
                                                    // end get driver statistics
                                                    //
                                                    // send sms & email to passenger

                                                    if (SMS && $transaction_insert_id) {
                                                        // SMS To passenger
                                                        /* if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
                                                             $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                                 'smsTitle' => 'complete_trip_wallet'
                                                                     ));
                                                         } else {
                                                             $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                                 'smsTitle' => 'complete_trip'
                                                                     ));
                                                         }
                                                         $message_data = $message_details->smsContent;
                                                         $message_data = str_replace("##USERNAME##", $driver_logged_status->firstName . ' ' . $driver_logged_status->lastName, $message_data);
                                                         $message_data = str_replace("##FARE##", $total_trip_cost, $message_data);

                                                         if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
                                                             $message_data = str_replace("##PAID##", $total_trip_cost + $parking_fee + $other_fee, $message_data);
                                                         }*/

                                                        $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                            'smsTitle' => 'new_complete_trip_sms'
                                                        ));
                                                        $message_data = $message_details->smsContent;
                                                        $message_data = str_replace("##BOOKINGID##", $trip_id, $message_data);
                                                        // Added by Aditya on 26th Apr 2017
                                                        $message_data = str_replace("##FARE##", $total_trip_cost, $message_data);
                                                        // Commented by Aditya on 26th Apr 2017
                                                        // Using a new SMS template
                                                        //$message_data = str_replace("##STARTDATETIME##", date('Y-m-d H:i:s', $actual_pickup_time), $message_data);
                                                        //$message_data = str_replace("##ENDDATETIME##", $actual_drop_time, $message_data);


                                                        // print_r($message);exit;
                                                        if ($passenger_details) {
                                                            if ($passenger_details->mobile) {

                                                                $this->Api_Webservice_Model->sendSMS($passenger_details->mobile, $message_data);
                                                            }
                                                        }
                                                        // SMS TO driver
                                                        /* $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                                    'smsTitle' => 'complete_trip_driver_sms'
                                                                        ));
                                                                $customer_care_no = ($trip_details->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
                                                                $message_data = $message_details->smsContent;
                                                                $message_data = str_replace("##CUSTOMERCARE##", $customer_care_no, $message_data);
                                                                $message_data = str_replace("##FARE##", $total_trip_cost, $message_data);
                                                                $message_data = str_replace("##COLLECT##", $total_trip_cost + $parking_fee + $other_fee, $message_data);
                                           */
                                                        // print_r($message);exit;
                                                        if ($driver_logged_status->mobile) {

                                                            // Updated and written by Aditya on 27th Apr 2017
                                                            // copied from the above commented code with own changes in it
                                                            // earlier driver was receiveing the same SMS as Passenger
                                                            // now driver receives his respective SMS
                                                            $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                                                'smsTitle' => 'complete_trip_driver_sms'
                                                            ));

                                                            $message_data = $message_details->smsContent;
                                                            $message_data = str_replace("##BOOKINGID##", $trip_id, $message_data);
                                                            // Aditya code ended

                                                            $this->Api_Webservice_Model->sendSMS($driver_logged_status->mobile, $message_data);
                                                            // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                                            $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                                                            // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                                        }
                                                    }
                                                    $bill_details = $this->Api_Webservice_Model->getBillDetails($transaction_insert_id);
                                                    $from = SMTP_EMAIL_ID;
                                                    $to = ($passenger_details) ? $passenger_details->email : NULL;
                                                    $subject = "Your " . date('l', strtotime($bill_details[0]->actualPickupDatetime)) .
                                                        " trip with Zuver Rs." . $bill_details[0]->totalTripCost;


                                                    $data = array(
                                                        'passengerFullName' => ($bill_details[0]->passengerFullName) ? $bill_details[0]->passengerFullName : '',
                                                        'passengerEmail' => ($bill_details[0]->passengerEmail) ? $bill_details[0]->passengerEmail : '',
                                                        'driverFullName' => ($bill_details[0]->driverFullName) ? $bill_details[0]->driverFullName : '',
                                                        'invoiceNo' => ($bill_details[0]->invoiceNo) ? $bill_details[0]->invoiceNo : '',
                                                        'invoiceDatetime' => ($bill_details[0]->invoiceDatetime) ? date('d M, Y, h:i a', strtotime($bill_details[0]->invoiceDatetime)) : '',
                                                        'actualPickupDatetime' => ($bill_details[0]->actualPickupDatetime) ? date('d M, Y, h:i a', strtotime($bill_details[0]->actualPickupDatetime)) : '',
                                                        'dropDatetime' => ($bill_details[0]->dropDatetime) ? date('d M, Y, h:i a', strtotime($bill_details[0]->dropDatetime)) : '',
                                                        'bookedDatetime' => ($bill_details[0]->bookedDatetime) ? date('d M, Y, h:i a', strtotime($bill_details[0]->bookedDatetime)) : '',
                                                        'tripType' => ($bill_details[0]->tripType) ? $bill_details[0]->tripType : '',
                                                        'paymentMode' => ($bill_details[0]->paymentMode) ? $bill_details[0]->paymentMode : '',
                                                        'totalTripCost' => ($bill_details[0]->totalTripCost) ? round($bill_details[0]->totalTripCost, 2) : '00.00',
                                                        'totalCash' => ($bill_details[0]->totalTripCost - $bill_details[0]->walletPaymentAmount) ? round($bill_details[0]->totalTripCost - $bill_details[0]->walletPaymentAmount, 2) : '00.00',
                                                        'companyTax' => ($bill_details[0]->companyTax) ? round($bill_details[0]->companyTax, 2) : '00.00',
                                                        'taxPercentage' => $tax,
                                                        'travelDistance' => ($bill_details [0]->travelDistance) ? round($bill_details [0]->travelDistance, 2) : '00.00',
                                                        'convenienceCharge' => ($bill_details[0]->convenienceCharge) ? round($bill_details[0]->convenienceCharge, 2) : '00.00',
                                                        'travelCharge' => ($bill_details[0]->travelCharge) ? round($bill_details[0]->travelCharge, 2) : '00.00',
                                                        'parkingCharge' => ($bill_details[0]->parkingCharge) ? round($bill_details[0]->parkingCharge, 2) : '00.00',
                                                        'tollCharge' => ($bill_details[0]->tollCharge) ? round($bill_details[0]->tollCharge, 2) : '00.00',
                                                        'promoDiscountAmount' => ($bill_details[0]->promoDiscountAmount) ? round($bill_details[0]->promoDiscountAmount, 2) : '00.00',
                                                        'otherDiscountAmount' => ($bill_details[0]->otherDiscountAmount) ? round($bill_details[0]->otherDiscountAmount, 2) : '00.00',
                                                        'walletPaymentAmount' => ($bill_details[0]->walletPaymentAmount) ? round($bill_details[0]->walletPaymentAmount, 2) : '00.00',
                                                        'travelHoursMinutes' => ($bill_details[0]->travelHoursMinutes) ? $bill_details[0]->travelHoursMinutes : '',
                                                        'otherCharge' => ($bill_details[0]->otherCharge) ? round($bill_details[0]->otherCharge, 2) : '00.00'
                                                    );
                                                    $message_data = $this->load->view('emailtemplate/tripcomplete-mail', $data, TRUE);

                                                    if (SMTP && $transaction_insert_id) {

                                                        if ($to) {
                                                            $this->Api_Webservice_Model->sendEmail($to, $subject, $message_data);
                                                        } else {
                                                            //B2B completed trip invoice to be send to 'info@zuver.in' requested by Ayush through email on 10/01/2017
                                                            $this->Api_Webservice_Model->sendEmail('info@zuver.in', $subject, $message_data);
                                                            //B2B completed trip invoice to be send to 'info@zuver.in' requested by Ayush through email on 10/01/2017
                                                        }
                                                    } else {

                                                        // To send HTML mail, the Content-type header must be set
                                                        $headers = 'MIME-Version: 1.0' .
                                                            "\r\n";
                                                        $headers .= 'Content-type: text/html; charset=iso-8859-1' .
                                                            "\r\n";
                                                        // Additional headers
                                                        $headers .= 'From: ' . $from .
                                                            '' .
                                                            "\r\n";
                                                        $headers .= 'Bcc: ' . $to .
                                                            '' .
                                                            "\r\n";
                                                        mail($to, $subject, $message_data, $headers);
                                                    }

                                                    // send sms & email to passenger
                                                    if ($trip_details->paymentMode == Payment_Mode_Enum::PAYTM) {
                                                        $message = array(
                                                            "message" => $msg_data['trip_finalize_waiting_paytm_payment'],
                                                            "detail" => $response_data,
                                                            "status" => 1,
                                                            "driver_statistics" => $statistics
                                                        );
                                                    } else {

                                                        $message = array(
                                                            "message" => $msg_data['trip_finalize_success'],
                                                            "detail" => $response_data,
                                                            "status" => 1,
                                                            "driver_statistics" => $statistics
                                                        );
                                                    }
                                                } else {
                                                    $message = array(
                                                        "message" => $msg_data['trip_finalize_failed'],
                                                        "detail" => $response_data,
                                                        "status" => -1,
                                                        "driver_statistics" => $statistics
                                                    );
                                                }
                                            } else {
                                                $message = array(
                                                    "message" => 'Your trip has been completed. Please process payment.',
                                                    "detail" => $response_data,
                                                    "status" => 4
                                                );
                                            }
                                        } else {
                                            $message = array(
                                                "message" => $msg_data['invalid_rate_card'],
                                                "status" => -1
                                            );
                                        }
                                    } else {
                                        $message = array(
                                            "message" => $msg_data['invalid_drop_time'],
                                            "status" => -1
                                        );
                                    }
                                    /**
                                     * ***************************************************
                                     *
                                     * // To update trip status on tripdetails
                                     * $trip_status_update = $this->Trip_Details_Model->update ( array (
                                     * 'tripStatus' => Trip_Status_Enum::IN_PROGRESS,
                                     * 'notificationStatus' => Trip_Status_Enum::IN_PROGRESS,
                                     * 'actualPickupDatetime' => getCurrentDateTime ()
                                     * ), array (
                                     * 'id' => $trip_id,
                                     * 'driverId' => $trip_details->driverId
                                     * ) );
                                     *
                                     * if ($trip_status_update) {
                                     * // To update trip status on driverrequesttripdetails
                                     * $trip_request_status_update = $this->Driver_Request_Details_Model->update ( array (
                                     * 'tripStatus' => Driver_Request_Status_Enum::DRIVER_ACCEPTED
                                     * ), array (
                                     * 'tripId' => $trip_id,
                                     * 'selectedDriverId' => $trip_details->driverId
                                     * ) );
                                     * }
                                     * if ($trip_request_status_update) {
                                     * // inserting the start trip location of driver into temp tracking table for particular trip
                                     * $insert_data = array (
                                     * 'tripId' => $trip_id,
                                     * 'latitude' => $latitude,
                                     * 'longitude' => $longitude
                                     * );
                                     * $trip_tracking_update = $this->Trip_Temp_Tracking_Info_Model->insert ( $insert_data );
                                     *
                                     * $message = array (
                                     * "message" => 'Trip has been started',
                                     * "status" => 1
                                     * );
                                     * } else {
                                     * $message = array (
                                     * "message" => 'Failed to update trip status & trip request status for trip_id=' . $trip_id,
                                     * "status" => - 1
                                     * );
                                     * // log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
                                     * }
                                     * **************************
                                     */
                                }
                            } else {
                                if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                                    $message = array(
                                        "message" => $msg_data['trip_reject_passenger'],
                                        "status" => -1
                                    );
                                } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                                    $message = array(
                                        "message" => $msg_data['trip_completed'],
                                        "status" => -1
                                    );
                                } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP || $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) {
                                    $message = array(
                                        "message" => $msg_data['trip_not_started'],
                                        "status" => -1
                                    );
                                }
                            }
                        } else {
                            $message = array(
                                "message" => $msg_data['invalid_trip_request'],
                                "status" => -1
                            );
                        }
                    } else {
                        $message = array(
                            "message" => $msg_data['driver_login_failed'],
                            "status" => -1
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data['invalid_user'],
                        "status" => -1
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data['invalid_trip'],
                    "status" => -1
                );
            }
        } else {
            $message = array(
                "message" => $msg_data['invalid_trip'],
                "status" => -1
            );
        }
        echo json_encode($message);
    }

    public function paymentConfirmation()
    {
        // get API messages from message.php
        $msg_data = $this->config->item('msg');
        $trip_id = $this->input->post('trip_id');
        if ($trip_id > 0) {
            $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id);

            if (count($trip_details) > 0) {
                $trip_status_update = $this->Trip_Details_Model->update(array(
                    'tripStatus' => Trip_Status_Enum::TRIP_COMPLETED,
                    'notificationStatus' => Trip_Status_Enum::TRIP_COMPLETED
                ), array(
                    'id' => $trip_id
                ));

                if ($trip_status_update) {
                    $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                        'smsTitle' => 'payment_confirmed_sms'
                    ));

                    $message_data = $message_details->smsContent;
                    $message_data = str_replace("##AMOUNT##", $trip_details [0]->totalTripCost, $message_data);

                    // SMS to passenger
                    if ($trip_details[0]->passengerMobile) {

                        $this->Api_Webservice_Model->sendSMS($trip_details [0]->passengerMobile, $message_data);
                    }
                    // SMS to driver
                    if ($trip_details[0]->diverMobile) {

                        $this->Api_Webservice_Model->sendSMS($trip_details [0]->diverMobile, $message_data);
                        // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                        $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                        // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                    }
                    $message = array(
                        "message" => $msg_data ['payment_confirmation_success'],
                        "status" => 1
                    );
                } else {
                    $message = array(
                        "message" => $msg_data ['payment_confirmation_failed'],
                        "status" => 0
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['invalid_trip'],
                    "status" => 0
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['invalid_trip'],
                "status" => 0
            );
        }
        echo json_encode($message);
    }

    public function updateTripDetails()
    {
        // update Trip details
        $msg_data = $this->config->item('msg');
        $trip_id = $this->input->post('trip_id');

        $data ['pickupDatetime'] = $this->input->post('pickup_dt');
        $data ['paymentMode'] = $this->input->post('payment_mode');
        $data ['tripType'] = $this->input->post('trip_type');
        $data ['transmissionType'] = $this->input->post('trans_type');
        $data ['pickupLocation'] = $this->input->post('p_location');
        $data ['pickupLatitude'] = $this->input->post('pickup_lat');
        $data ['pickupLongitude'] = $this->input->post('pickup_lon');

        $data ['dropLocation'] = $this->input->post('drop_location');
        $data ['dropLatitude'] = $this->input->post('drop_lat');
        $data ['dropLongitude'] = $this->input->post('dp_long');

        $data ['driverAssignedDatetime'] = $this->input->post('d_assigned_date');
        $data ['driverArrivedDatetime'] = $this->input->post('d_arrived_date');
        $data ['dropDatetime'] = $this->input->post('drop_datetime');
        $data ['actualPickupDatetime'] = $this->input->post('actual_datetime');
        $data ['billType'] = $this->input->post('bill_type');
        $data ['carType'] = $this->input->post('car_type');
        $data['promoCode'] = $this->input->post('promoCode');

        $trip_updated = $this->Trip_Details_Model->update($data, array(
            'id' => $trip_id
        ));
        if ($trip_updated) {
            $message = array(
                "message" => $msg_data ['updated'],
                "status" => 1
            );
        } else {
            $message = array(
                "message" => $msg_data ['failed'],
                "status" => 0
            );
        }
        echo json_encode($message);
    }

    public function tripCancelledByPassenger()
    {
        $msg_data = $this->config->item('api');
        $msg_data1 = $this->config->item('msg');
        $post_data = NULL;
        $data = array();
        $message = '';
        $check_trip_avilablity = NULL;
        $remarks = NULL;
        $trip_id = NULL;
        $trip_details = NULL;
        $response_data = array();

        $post_data = $this->input->post();

        $trip_id = $post_data ['passenger_log_id'];
        $remarks = $post_data ['remarks'];
        // actual functionality
        if ($trip_id) {
            // get trip details
            $trip_details = $this->Trip_Details_Model->getById($trip_id);
            $bookingKey = $trip_details->bookingKey;
            $companyId = $trip_details->companyId;
            $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id, $trip_details->passengerId);

            // check trip avilable for driver/any in progress trip before logout
            $check_trip_avilablity = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                'tripId' => $trip_id
            ), 'id DESC');
            if ($check_trip_avilablity) {

                if (($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED)
                    && ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ARRIVED ||
                        $trip_details [0]->tripStatus == Trip_Status_Enum::IN_PROGRESS ||
                        $trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYMENT ||
                        $trip_details [0]->tripStatus == Trip_Status_Enum::WAITING_FOR_PAYTM_PAYMENT)
                ) {
                    $message = array(
                        "message" => $msg_data ['trip_progress'],
                        "status" => -1
                    );
                } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::PASSENGER_CANCELLED) {
                    $message = array(
                        "message" => $msg_data ['trip_reject_passenger'],
                        "status" => -1
                    );
                } else if ($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::COMPLETED_TRIP) {
                    $message = array(
                        "message" => $msg_data ['trip_completed'],
                        "status" => -1
                    );
                } else if (($check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_REJECTED ||
                        $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::DRIVER_ACCEPTED ||
                        $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::AVAILABLE_TRIP ||
                        $check_trip_avilablity->tripStatus == Driver_Request_Status_Enum::SENT_TO_DRIVER) &&
                    ($trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED ||
                        $trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER)
                ) {
                    //ZoomCar cancel APi start
                    if ($companyId == ZOOMCAR_COMPANYID) {

                        $trip_cancelled = $this->Api_Webservice_Model->zoomCarTripResponse($trip_id, $bookingKey, 0, 0,0);

                        if ($trip_cancelled['status'] == 'success') {

                            $trip_status_update = $this->Trip_Details_Model->update(array(
                                'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                                'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                                'passengerRejectComments' => $remarks
                            ), array(
                                'id' => $trip_id
                            ));

                            $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                                'tripStatus' => Driver_Request_Status_Enum::PASSENGER_CANCELLED
                            ), array(
                                'tripId' => $trip_id
                            )); // get last inserted shift id for particular driver

                            $get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                                'driverId' => $trip_details [0]->driverId
                            ), 'id DESC');
                            if ($get_last_shift_id) {
                                $driver_shift_status_update = $this->Driver_Shift_History_Model->update(array(
                                    'availabilityStatus' => Driver_Available_Status_Enum::FREE
                                ), array(
                                    'id' => $get_last_shift_id->id
                                ));
                            }
                            // sms & email start
                            if (SMS && $trip_request_status_update) {
                                // SMS To passenger

                                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                    'smsTitle' => 'trip_cancel'
                                ));

                                $message_data = $message_details->smsContent;
                                $message_data = str_replace("##BOOKINGID##", $trip_details [0]->tripId, $message_data);
                                if ($trip_details [0]->passengerMobile) {

                                    $this->Api_Webservice_Model->sendSMS($trip_details [0]->passengerMobile, $message_data);
                                }

                                // SMS TO driver
                                $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                    'smsTitle' => 'trip_cancel_driver_sms'
                                ));
                                $customer_care_no = ($trip_details [0]->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details [0]->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details [0]->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
                                $message_data = $message_details->smsContent;
                                $message_data = str_replace("##CUSTOMERCARE##", $customer_care_no, $message_data);

                                // print_r($message);exit;
                                if ($trip_details [0]->diverMobile) {

                                    $this->Api_Webservice_Model->sendSMS($trip_details [0]->diverMobile, $message_data);
                                    // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                    $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                                    // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                }
                            }

                            // sms & email end
                            $message = array(
                                "message" => $msg_data ['passenger_cancel_success'],
                                "status" => 1
                            );

                        } else {
                            $message = array(
                                "message" => $msg_data1 ['zoom_trip_cancel_failed'],
                                "status" => '-1'
                            );
                        }
//                    ZoomCar Cancel APi End
                    }
                    if ($companyId != ZOOMCAR_COMPANYID) {
                        $trip_status_update = $this->Trip_Details_Model->update(array(
                            'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                            'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                            'passengerRejectComments' => $remarks
                        ), array(
                            'id' => $trip_id
                        ));

                        $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                            'tripStatus' => Driver_Request_Status_Enum::PASSENGER_CANCELLED
                        ), array(
                            'tripId' => $trip_id
                        )); // get last inserted shift id for particular driver

                        $get_last_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                            'driverId' => $trip_details [0]->driverId
                        ), 'id DESC');
                        if ($get_last_shift_id) {
                            $driver_shift_status_update = $this->Driver_Shift_History_Model->update(array(
                                'availabilityStatus' => Driver_Available_Status_Enum::FREE
                            ), array(
                                'id' => $get_last_shift_id->id
                            ));
                        }
                        // sms & email start
                        if (SMS && $trip_request_status_update) {
                            // SMS To passenger

                            $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                'smsTitle' => 'trip_cancel'
                            ));

                            $message_data = $message_details->smsContent;
                            $message_data = str_replace("##BOOKINGID##", $trip_details [0]->tripId, $message_data);
                            if ($trip_details [0]->passengerMobile) {

                                $this->Api_Webservice_Model->sendSMS($trip_details [0]->passengerMobile, $message_data);
                            }

                            // SMS TO driver
                            $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                'smsTitle' => 'trip_cancel_driver_sms'
                            ));
                            $customer_care_no = ($trip_details [0]->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details [0]->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details [0]->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
                            $message_data = $message_details->smsContent;
                            $message_data = str_replace("##CUSTOMERCARE##", $customer_care_no, $message_data);

                            // print_r($message);exit;
                            if ($trip_details [0]->diverMobile) {

                                $this->Api_Webservice_Model->sendSMS($trip_details [0]->diverMobile, $message_data);
                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                            }
                        }

                        // sms & email end
                        $message = array(
                            "message" => $msg_data ['passenger_cancel_success'],
                            "status" => 1
                        );
                    }
                    //ZoomCar cancel APi End

                }
            } else if ($trip_details [0]->tripStatus == Trip_Status_Enum::BOOKED ||
                $trip_details [0]->tripStatus == Trip_Status_Enum::TRIP_CONFIRMED ||
                $trip_details [0]->tripStatus == Trip_Status_Enum::DRIVER_ACCEPTED ||
                $trip_details [0]->tripStatus == Trip_Status_Enum::CANCELLED_BY_DRIVER ||
                $trip_details [0]->tripStatus == Trip_Status_Enum::READY_TO_START
            ) {
                // ZoomCar Cancel APi Start
                if ($companyId == ZOOMCAR_COMPANYID) {
                    $trip_cancelled = $this->Api_Webservice_Model->zoomCarTripResponse($trip_id, $bookingKey, 0, 0,0);

                    if ($trip_cancelled['status'] == 'success') {

                        $trip_status_update = $this->Trip_Details_Model->update(array(
                            'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                            'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER
                        ), array(
                            'id' => $trip_id
                        ));


                        // sms & email start
                        if (SMS && $trip_status_update) {
                            // SMS To passenger

                            $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                'smsTitle' => 'trip_cancel'
                            ));

                            $message_data = $message_details->smsContent;
                            $message_data = str_replace("##BOOKINGID##", $trip_details [0]->tripId, $message_data);
                            if ($trip_details [0]->passengerMobile) {

                                $this->Api_Webservice_Model->sendSMS($trip_details [0]->passengerMobile, $message_data);
                            }

                            // SMS TO driver
                            $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                                'smsTitle' => 'trip_cancel_driver_sms'
                            ));
                            $customer_care_no = ($trip_details [0]->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details [0]->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details [0]->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
                            $message_data = $message_details->smsContent;
                            $message_data = str_replace("##CUSTOMERCARE##", $customer_care_no, $message_data);

                            // print_r($message);exit;
                            if ($trip_details [0]->diverMobile) {

                                $this->Api_Webservice_Model->sendSMS($trip_details [0]->diverMobile, $message_data);
                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                                $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                                // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                            }
                        }

                        // sms & email end
                        $message = array(
                            "message" => $msg_data ['passenger_cancel_success'],
                            "status" => 1
                        );
                    } else {
                        $message = array(
                            "message" => $msg_data1 ['zoom_trip_cancel_failed'],
                            "status" => -1
                        );
                    }
                }
                if ($companyId != ZOOMCAR_COMPANYID) {
                    $trip_status_update = $this->Trip_Details_Model->update(array(
                        'tripStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                        'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_PASSENGER
                    ), array(
                        'id' => $trip_id
                    ));


                    // sms & email start
                    if (SMS && $trip_status_update) {
                        // SMS To passenger

                        $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                            'smsTitle' => 'trip_cancel'
                        ));

                        $message_data = $message_details->smsContent;
                        $message_data = str_replace("##BOOKINGID##", $trip_details [0]->tripId, $message_data);
                        if ($trip_details [0]->passengerMobile) {

                            $this->Api_Webservice_Model->sendSMS($trip_details [0]->passengerMobile, $message_data);
                        }

                        // SMS TO driver
                        $message_details = $this->Sms_Template_Model->getOneByKeyValueArray(array(
                            'smsTitle' => 'trip_cancel_driver_sms'
                        ));
                        $customer_care_no = ($trip_details [0]->bookingCityId == 1) ? CUSTOMERCARE_MUMBAI : ($trip_details [0]->bookingCityId == 2) ? CUSTOMERCARE_BANGALORE : ($trip_details [0]->bookingCityId == 3) ? CUSTOMERCARE_PUNE : CUSTOMERCARE_DEFAULT;
                        $message_data = $message_details->smsContent;
                        $message_data = str_replace("##CUSTOMERCARE##", $customer_care_no, $message_data);

                        // print_r($message);exit;
                        if ($trip_details [0]->diverMobile) {

                            $this->Api_Webservice_Model->sendSMS($trip_details [0]->diverMobile, $message_data);
                            // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                            $this->Api_Webservice_Model->sendSMS('8451976667', $message_data);
                            // this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
                        }
                    }

                    // sms & email end
                    $message = array(
                        "message" => $msg_data ['passenger_cancel_success'],
                        "status" => 1
                    );

                }
                //ZoomCar Cancel APi End
            } else {
                $message = array(
                    "message" => $msg_data ['invalid_trip'],
                    "status" => -1
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['invalid_trip'],
                "status" => -1
            );
        }
        echo json_encode($message);
    }

    public function tripCancelledByDriver()
    {
        $msg_data = $this->config->item('api');
        $post_data = NULL;
        $data = array();
        $message = '';
        $driver_id = NULL;
        $reject_type = NULL;
        $trip_id = NULL;
        $reject_reason = NULL;
        $company_id = NULL;
        $reject_trip_id = NULL;
        $trip_status_update = NULL;
        $trip_request_status_update = NULL;
        $response_data = array();

        $post_data = $this->input->post();
        $driver_id = $post_data ['driver_id'];
        $reject_type = $post_data ['reject_type'];
        $trip_id = $post_data ['trip_id'];
        $reject_reason = $post_data ['reason'];
        $company_id = $post_data ['company_id'];
        // actual functionality
        if ($trip_id && $driver_id) {

            // To update trip status on tripdetails
            $trip_status_update = $this->Trip_Details_Model->update(array(
                'tripStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
                'notificationStatus' => Trip_Status_Enum::CANCELLED_BY_DRIVER,
                'driverId' => 0,
                'driverAssignedDatetime' => 0,
                'driverAcceptedStatus' => Driver_Accepted_Status_Enum::REJECTED
            ), array(
                'id' => $trip_id,
                'driverId' => $driver_id
            ));

            if ($trip_status_update) {
                // To update trip status on driverrequesttripdetails
                // to get rejected drivers for particular trip_id
                $rejected_drivers_list = NULL;
                $rejected_drivers = $this->Driver_Request_Details_Model->getOneByKeyValueArray(array(
                    'tripId' => $trip_id
                ));

                if ($rejected_drivers) {
                    if ($rejected_drivers->rejectedDrivers) {
                        $rejected_drivers_list = $rejected_drivers->rejectedDrivers;
                        $rejected_drivers_list .= ',' . $driver_id;
                    } else {
                        $rejected_drivers_list = $driver_id;
                    }
                }

                $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                    'tripStatus' => Driver_Request_Status_Enum::DRIVER_REJECTED,
                    'rejectedDrivers' => $rejected_drivers_list
                ), array(
                    'tripId' => $trip_id
                ));
            }
            if ($trip_request_status_update) {
                // to get last shift in status of driver before updating the shift out status
                $last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                    'driverId' => $driver_id
                ), 'id DESC');
                $driver_shift_insert_id = $this->Driver_Shift_History_Model->update(array(
                    'availabilityStatus' => Driver_Available_Status_Enum::FREE
                ), array(
                    'id' => $last_driver_shift_id->id,
                    'driverId' => $driver_id
                ));

                // Insert/update data to driverrejectedtripdetails table
                // To get the passenger id from trip id to update the the trip rejected by driver
                $trip_details = $this->Trip_Details_Model->getById($trip_id);
                $insert_data = array(
                    'tripId' => $trip_id,
                    'driverId' => $driver_id,
                    'passengerId' => $trip_details->passengerId,
                    'rejectionType' => ($reject_type == 1) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT,
                    'rejectionReason' => $reject_reason
                );
                $exist_rejected_trip = $this->Driver_Rejected_Trip_Details_Model->getOneByKeyValueArray(array(
                    'tripId' => $trip_id,
                    'driverId' => $driver_id
                ));

                if (!$exist_rejected_trip) {
                    $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->insert($insert_data);
                } else {
                    $update_data = array(
                        'rejectionType' => ($reject_type == 1) ? Driver_Accepted_Status_Enum::REJECTED : Driver_Accepted_Status_Enum::TIMEOUT,
                        'rejectionReason' => $reject_reason
                    );
                    $reject_trip_id = $this->Driver_Rejected_Trip_Details_Model->update($update_data, array(
                        'tripId' => $trip_id,
                        'driverId' => $driver_id
                    ));
                }

                $message = array(
                    "message" => $msg_data ['driver_rejected'],
                    "status" => 6
                );
            } else {
                $message = array(
                    "message" => $msg_data ['trip_status_failed'] . '' . $trip_id,
                    "status" => -1
                );
                // log_message ( 'debug', 'Failed to update trip status & trip request status for trip_id=' . $trip_id );
            }
        } else {
            $message = array(
                "message" => $msg_data ['invalid_trip'],
                "status" => '-1'
            );
        }
        echo json_encode($message);
    }

    public function loadGmap()
    {
        $trip_id = $this->uri->segment('3');
        // $driver_data = $this->Query_Model->getDriverLatLongForTripDetails($trip_id);
        $driver_start_point = $this->Trip_Temp_Tracking_Info_Model->getOneByKeyValueArray(array(
            'tripId' => $trip_id
        ), 'createdDate');
        $driver_end_point = $this->Trip_Temp_Tracking_Info_Model->getOneByKeyValueArray(array(
            'tripId' => $trip_id
        ), 'createdDate DESC');
        $data ['trip_model'] = $this->Trip_Details_Model->getById($trip_id);
        if ($driver_start_point && $driver_end_point) {
            $data ['pickupLatitude'] = $driver_start_point->latitude;
            $data ['pickupLongitude'] = $driver_start_point->longitude;
            $data ['driverLatitude'] = $driver_end_point->latitude;
            $data ['driverLongitude'] = $driver_end_point->longitude;
            $data ['cityId'] = $data ['trip_model']->bookingCityId;
            $this->load->view('trip/loadIprTripMap', $data);
        } else {

            if ($data ['trip_model']) {

                $data ['pickupLatitude'] = $data ['trip_model']->pickupLatitude;
                $data ['pickupLongitude'] = $data ['trip_model']->pickupLongitude;

                $data ['dropLatitude'] = $data ['trip_model']->dropLatitude;
                $data ['dropLongitude'] = $data ['trip_model']->dropLongitude;

                $data ['cityId'] = $data ['trip_model']->bookingCityId;
                $data ['filter_cityid'] = $this->session->userdata('user_city');
                $data ['diver_mapview'] = FALSE;
                // $html = $this->load->view("driver/driver_map", $data);
                $data['pickupLocation'] = $data ['trip_model']->pickupLocation;
                $data['dropLocation'] = $data ['trip_model']->dropLocation;

                $data['tripId'] = $trip_id;
                $html = $this->load->view('trip/loadTripMap', $data);
            } else {
                echo "Map Loading Failed";
            }
        }
    }

    public function getPassengerInfo()
    {
        $keyword = $this->input->post('keyword');
        $type = $this->input->post('type');
        $data ['passenger_info'] = $this->Query_Model->getPassengerInfoForTripBooking($keyword, $type);
        if (!empty($data ['passenger_info'])) {
            echo "<ul id='passenger-autocompletelist' style=''>";
            foreach ($data ['passenger_info'] as $passenger) {
                $passneger_name = $passenger->passenger_name;
                $passneger_id = $passenger->passenger_id;
                $passenger_email = $passenger->passenger_email;
                $passenger_mobile = $passenger->passenger_mobile;
                echo "<li onClick=\"selectPassenger('$passneger_id','$passenger_mobile','$passenger_email','$passneger_name');\"> $passneger_name </li> ";
            }
            echo "</ul>";
        }
    }

    public function cancelZoomCarTrip()
    {
        $msg_data = $this->config->item('msg');
        $trip_id = $this->input->post('trip_id');
        $rejected_drivers_list = NULL;
        if ($trip_id) {
            $trip_details = $this->Trip_Details_Model->getById($trip_id);

            if ($trip_details) {

                $trip_cancelled = $this->Api_Webservice_Model->zoomCarTripResponse($trip_id, $trip_details->bookingKey, 0, 0,1);

                if ($trip_cancelled['status'] == 'success') {
                    $update_data = array(
                        "tripStatus" => Trip_Status_Enum::CANCELLED_BY_PASSENGER,
                        "notificationStatus" => Trip_Status_Enum::CANCELLED_BY_PASSENGER
                    );
                    $trip_details_updated = $this->Trip_Details_Model->update($update_data, array('id' => $trip_id));

                    if ($trip_details_updated) {

                        $trip_request_status_update = $this->Driver_Request_Details_Model->update(array(
                            'tripStatus' => Driver_Request_Status_Enum::DRIVER_REJECTED,
                            'rejectedDrivers' => $rejected_drivers_list
                        ), array(
                            'tripId' => $trip_id
                        ));
                        $last_driver_shift_id = $this->Driver_Shift_History_Model->getOneByKeyValueArray(array(
                            'driverId' => $trip_details->driverId
                        ), 'id DESC');
                        $driver_shift_insert_id = $this->Driver_Shift_History_Model->update(array(
                            'availabilityStatus' => Driver_Available_Status_Enum::FREE
                        ), array(
                            'id' => $last_driver_shift_id->id,
                            'driverId' => $trip_details->driverId
                        ));
                        $message = array(
                            "message" => $msg_data ['zoom_trip_cancel_success'],
                            "status" => '1'
                        );
                    }
                } else {
                    $message = array(
                        "message" => $msg_data ['zoom_trip_cancel_failed'],
                        "status" => '-1'
                    );
                }
            } else {
                $message = array(
                    "message" => $msg_data ['invalid_trip'],
                    "status" => '-1'
                );
            }
        } else {
            $message = array(
                "message" => $msg_data ['invalid_trip'],
                "status" => '-1'
            );
        }
        echo json_encode($message);

    }

    private function calculate_driver_earning($trip_id, $driver_id)
    {
        $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id);
        $driver_details = $this->Driver_Model->getById($driver_id);

        if (count($trip_details) > 0) {
            $trip_details = $this->Trip_Details_Model->getById($trip_id);
            $trip_details = $this->Api_Webservice_Model->getTripDetails($trip_id, $trip_details->passengerId);
            $trip_details = $trip_details[0];
        }

        if ($trip_details->companyId == DEFAULT_COMPANY_ID || $driver_details->driverType == 'B2C') {
            if ($trip_details->tripType == 'XO' || $trip_details->tripType == 'XR') {
                $this_trip_type = 'X';
            } else {
                $this_trip_type = $trip_details->tripType;
            }
        } else {
            $this_trip_type = 'B2B';
        }

        if ($driver_details->driverType == 'B2C') {
            $driver_rate_card = $this->Driver_Rate_Card_Details_Model->getByKeyValueArray(array('tripType' => array($this_trip_type, 'ALL'), 'driverType' => $driver_details->driverType, 'rateFor!=' => 'Day'));
        } else {
            $driver_rate_card = $this->Driver_Rate_Card_Details_Model->getByKeyValueArray(array('driverType' => $driver_details->driverType, 'rateFor!=' => 'Day'));
        }

        $driver_productivity_allowance = array();
        if ($driver_details->driverType == 'TBD') {
            $driver_productivity_allowance = $this->Driver_Productivity_Allowance_Model->getByKeyValueArray(array('driverId' => $driver_details->id), 'extraRateForMin Desc');
        }
        $all_sub_trips = $this->Sub_Trip_Details_Model->getByKeyValueArray(array('masterTripId' => $trip_id));
        if ($all_sub_trips && $driver_details->driverType == 'TBD') {
            $total_earning = 0;
            $sub_trip_count = 1;
            foreach ($all_sub_trips as $sub_trip) {
                $earnings = array();
                foreach ($driver_rate_card as $earningtype_rate_card) {
                    if (!array_key_exists($earningtype_rate_card->earningType, $earnings)) {
                        $earnings[$earningtype_rate_card->earningType] = 0;
                    }
                    $earning_date = date('Y-m-d', strtotime($sub_trip->dropDatetime));
                    //Check on Earning limit
                    if ($earningtype_rate_card->rateForLimit) {
                        if ($earningtype_rate_card->rateFor == 'Trip') {
                            $driver_total_trips = 0;
                            //For first limited trips for a current month
                            if ($earningtype_rate_card->limitFor == 'M') {
                                $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, $driver_details->driverType, 'CompletedTrips', array('trip_completed_month' => $earning_date)) + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_month' => $earning_date)) + $sub_trip_count;
                            }
                            if ($earningtype_rate_card->limitFor == 'D') {
                                $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $sub_trip_count;
                            }
                            if ($driver_total_trips && $driver_total_trips >= $earningtype_rate_card->extraRateForMin && ($driver_total_trips <= (int)($earningtype_rate_card->rateForLimit + $earningtype_rate_card->extraRateForMin))) {
                                $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                            }
                        }
                    } else {
                        if ($earningtype_rate_card->earningType == 'NE') {
                            $pick_up_time = date("H:i a", strtotime($sub_trip->pickupDatetime));
                            $drop_time = date("H:i a", strtotime($sub_trip->dropDatetime));

                            if (($pick_up_time >= date("H:i:s", strtotime(NIGHT_START_TIME)) || $pick_up_time <= date("H:i a", strtotime(NIGHT_END_TIME))) || ($drop_time >= date("H:i a", strtotime(NIGHT_START_TIME)) || $drop_time <= date("H:i a", strtotime(NIGHT_END_TIME)))) {
                                $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                            }
                        } else {
                            $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                        }
                    }

                    $extra_earning = 0;
                    //Check for Extra Rate on earnings
                    if ($earningtype_rate_card->extraRate) {
                        /*if($earningtype_rate_card->extraRateFor=='Km')
                        {
                            if($sub_trip->travelDistance>=$earningtype_rate_card->extraRateForMin)
                            {
                                $extra_earning=round(($sub_trip->travelDistance-$earningtype_rate_card->extraRateForMin)*$earningtype_rate_card->extraRate);
                            }
                        }*/

                        if ($earningtype_rate_card->extraRateFor == 'Hr') {
                            if ($earningtype_rate_card->limitFor == 'T') {
                                $date_pickup = new DateTime($sub_trip->pickupDatetime);
                                $date_drop = new DateTime($sub_trip->dropDatetime);
                                $driver_total_trip_hours = date_diff($date_drop, $date_pickup);
                                $driver_total_trip_hours->format('%h.%i');
                                // $driver_total_trip_hours = $sub_trip->travelHoursMinutes;
                            }

                            if ($driver_total_trip_hours && $driver_total_trip_hours > $earningtype_rate_card->extraRateForMin) {
                                $exceeded_hours = $driver_total_trip_hours - $earningtype_rate_card->extraRateForMin;
                                if ($exceeded_hours < $driver_total_trip_hours) {
                                    $extra_earning = round($exceeded_hours * $earningtype_rate_card->extraRate);
                                } else {
                                    $extra_earning = round($driver_total_trip_hours * $earningtype_rate_card->extraRate);
                                }
                            }
                        }
                    }
                    $earnings[$earningtype_rate_card->earningType] = $earnings[$earningtype_rate_card->earningType] + $extra_earning;
                }

                if ($driver_productivity_allowance) {
                    foreach ($driver_productivity_allowance as $driver_productivity_allowance_data) {

                        if (!array_key_exists($driver_productivity_allowance_data->earningType, $earnings)) {
                            $earning_date = date('Y-m-d', strtotime($sub_trip->dropDatetime));
                            if ($driver_productivity_allowance_data->extraRate) {

                                if ($driver_productivity_allowance_data->extraRateFor == 'Trip') {
                                    $driver_total_trips = 0;
                                    $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $sub_trip_count;
                                    if ($driver_total_trips && $driver_total_trips >= $driver_productivity_allowance_data->extraRateForMin) {
                                        $earnings[$driver_productivity_allowance_data->earningType] = $driver_productivity_allowance_data->extraRate;
                                    }
                                }
                            }
                        }

                    }
                }

                $earning_history = array();
                foreach ($earnings as $key => $value) {
                    $earning_data = array('driverId' => $driver_id,
                        'tripId' => $sub_trip->subTripId,
                        'tripType' => $trip_details->tripType,
                        'driverType' => $driver_details->driverType,
                        'earningType' => $key,
                        'earningAmount' => $value,
                        'earningDate' => date('Y-m-d H:i:s', strtotime($sub_trip->dropDatetime))
                    );
                    $earning_history[] = $earning_data;
                }

                $this->Driver_Model->save_driver_earnings($driver_id, $sub_trip->subTripId, $earning_history);
                $total_sub_trips = $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $sub_trip_count;
                $driver_daily_earning = $this->driver_daily_earning($sub_trip->dropDatetime, $driver_id, $total_sub_trips);
                $total_earning = $total_earning + array_sum($earnings);
                $sub_trip_count++;
            }
            return $total_earning;
        } else {
            $earnings = array();
            foreach ($driver_rate_card as $earningtype_rate_card) {
                if (!array_key_exists($earningtype_rate_card->earningType, $earnings)) {
                    $earnings[$earningtype_rate_card->earningType] = 0;
                }
                $earning_date = date('Y-m-d', strtotime($trip_details->dropDatetime));
                //Check on Earning limit
                if ($earningtype_rate_card->rateForLimit) {
                    if ($earningtype_rate_card->rateFor == 'Trip') {
                        $driver_total_trips = 0;
                        //For first limited trips for a current month
                        if ($earningtype_rate_card->limitFor == 'M') {
                            $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_month' => $earning_date));
                            if ($driver_details->driverType == 'TBD') {
                                $driver_total_trips = $driver_total_trips + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_month' => $earning_date));
                            }
                        }
                        if ($earningtype_rate_card->limitFor == 'D') {
                            $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date));
                            if ($driver_details->driverType == 'TBD') {
                                $driver_total_trips = $driver_total_trips + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));
                            }
                        }
                        if ($driver_total_trips && $driver_total_trips >= $earningtype_rate_card->extraRateForMin && ($driver_total_trips <= (int)($earningtype_rate_card->rateForLimit + $earningtype_rate_card->extraRateForMin))) {
                            $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                        }
                    }
                } else {
                    if ($earningtype_rate_card->earningType == 'NE') {
                        $pick_up_time = date("H:i a", strtotime($trip_details->actualPickupDatetime));
                        $drop_time = date("H:i a", strtotime($trip_details->dropDatetime));

                        if (($pick_up_time >= date("H:i:s", strtotime(NIGHT_START_TIME)) || $pick_up_time <= date("H:i a", strtotime(NIGHT_END_TIME))) || ($drop_time >= date("H:i a", strtotime(NIGHT_START_TIME)) || $drop_time <= date("H:i a", strtotime(NIGHT_END_TIME)))) {
                            $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                        }
                    } else {
                        if ($earningtype_rate_card->rateFor == 'Hr') {
                            $earnings[$earningtype_rate_card->earningType] = round($trip_details->travelHoursMinutes * $earningtype_rate_card->baseRate);
                        } else {
                            $earnings[$earningtype_rate_card->earningType] = $earningtype_rate_card->baseRate;
                        }
                    }
                }

                $extra_earning = 0;
                //Check for Extra Rate on earnings
                if ($earningtype_rate_card->extraRate) {
                    if ($earningtype_rate_card->extraRateFor == 'Km') {
                        if ($trip_details->travelDistance >= $earningtype_rate_card->extraRateForMin) {
                            $extra_earning = round(($trip_details->travelDistance - $earningtype_rate_card->extraRateForMin) * $earningtype_rate_card->extraRate);
                        }
                    }
                    if ($earningtype_rate_card->extraRateFor == 'Hr') {
                        if ($earningtype_rate_card->limitFor == 'M') {
                            $driver_total_hour_distance = $this->Driver_Model->get_driver_trip_hours_distance($driver_id, 'CompletedTrips', array('trip_completed_month' => $earning_date));
                            $driver_total_trip_hours = $driver_total_hour_distance->totalTripHours;
                        }

                        if ($earningtype_rate_card->limitFor == 'D') {
                            $driver_MainTrip_total_hour_distance = $this->Driver_Model->get_driver_trip_hours_distance($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));

                            $driver_SubTrip_total_hour_distance = $this->Driver_Model->get_driver_sub_trip_hours_distance($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));

                            $driver_total_trip_hours = $driver_MainTrip_total_hour_distance->totalTripHours + $driver_SubTrip_total_hour_distance->totalTripHours;
                        }

                        if ($earningtype_rate_card->limitFor == 'T') {
                            $driver_total_trip_hours = $trip_details->travelHoursMinutes;
                        }

                        if ($driver_total_trip_hours && $driver_total_trip_hours > $earningtype_rate_card->extraRateForMin) {
                            $exceeded_hours = $driver_total_trip_hours - $earningtype_rate_card->extraRateForMin;
                            if ($exceeded_hours < $trip_details->travelHoursMinutes) {
                                $extra_earning = round($exceeded_hours * $earningtype_rate_card->extraRate);
                            } else {
                                $extra_earning = round($trip_details->travelHoursMinutes * $earningtype_rate_card->extraRate);
                            }
                        }
                    }
                }
                $earnings[$earningtype_rate_card->earningType] = $earnings[$earningtype_rate_card->earningType] + $extra_earning;
            }

            if ($driver_productivity_allowance) {
                foreach ($driver_productivity_allowance as $driver_productivity_allowance_data) {
                    if (!array_key_exists($driver_productivity_allowance_data->earningType, $earnings)) {
                        $earning_date = date('Y-m-d', strtotime($trip_details->dropDatetime));
                        if ($driver_productivity_allowance_data->extraRate) {
                            if ($driver_productivity_allowance_data->extraRateFor == 'Trip') {
                                $driver_total_trips = 0;
                                $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_id, $driver_details->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));
                                if ($driver_total_trips && $driver_total_trips >= $driver_productivity_allowance_data->extraRateForMin) {
                                    $earnings[$driver_productivity_allowance_data->earningType] = $driver_productivity_allowance_data->extraRate;
                                }
                            }
                        }
                    }

                }
            }

            $earning_history = array();
            foreach ($earnings as $key => $value) {
                $earning_data = array('driverId' => $driver_id,
                    'tripId' => $trip_id,
                    'tripType' => $trip_details->tripType,
                    'driverType' => $driver_details->driverType,
                    'earningType' => $key,
                    'earningAmount' => $value,
                    'earningDate' => date('Y-m-d H:i:s', strtotime($trip_details->dropDatetime))
                );
                $earning_history[] = $earning_data;
            }

            $this->Driver_Model->save_driver_earnings($driver_id, $trip_id, $earning_history);
            $total_sub_trips = $this->Driver_Model->get_driver_total_Sub_trips($driver_id, 'CompletedTrips', array('trip_completed_date' => $earning_date));
            $driver_daily_earning = $this->driver_daily_earning($trip_details->dropDatetime, $driver_id, $total_sub_trips);
            return array_sum($earnings);
        }
    }

    private function driver_daily_earning($earning_date, $driver_id, $sub_trips_count = 0)
    {
        $earning_history = array();
        $earning_date = date('Y-m-d', strtotime($earning_date));
        $driver_data = $this->Driver_Model->getById($driver_id);
        $driver_daily_rate_card = $this->Driver_Rate_Card_Details_Model->getByKeyValueArray(array('driverType' => $driver_data->driverType, 'rateFor' => 'Day'), 'extraRateForMin Desc');
        foreach ($driver_daily_rate_card as $driver_daily_rate_card_details) {
            if ($driver_daily_rate_card_details->extraRateFor == 'Trip') {
                $driver_total_trips = $this->Driver_Model->get_driver_total_trips($driver_data->id, $driver_data->driverType, 'CompletedTrips', array('trip_completed_date' => $earning_date)) + $sub_trips_count;
                if ($driver_total_trips) {
                    if ($driver_total_trips == $driver_daily_rate_card_details->extraRateForMin) {
                        $earning_data = array('driverId' => $driver_data->id,
                            'tripId' => 0,
                            'tripType' => 'NA',
                            'driverType' => $driver_data->driverType,
                            'earningType' => $driver_daily_rate_card_details->earningType,
                            'earningAmount' => $driver_daily_rate_card_details->baseRate,
                            'earningDate' => date('Y-m-d H:i:s', strtotime($earning_date))
                        );
                        $earning_history[$driver_daily_rate_card_details->earningType] = $earning_data;
                        if ($earning_history) {
                            $this->Driver_Model->save_driver_daily_earnings($earning_history);
                        }
                    }
                }
            }
        }
    }
}

// end of class
