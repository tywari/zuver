<?php

/**
 * Created by PhpStorm.
 * User: Dragons
 * Date: 6/28/2017
 * Time: 11:44 AM
 */
class CronJobs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cron_jobs_test');
    }

    public function index()
    {
        if (!$this->input->is_cli_request()) {
            echo "This script can only be accessed via the command line" . PHP_EOL;
            return;
        }
        $status = 1;
        $driverUpdateShift = $this->Cron_Jobs_Model->update_driver_shift_status_cron($status);
        if ($driverUpdateShift) {
            echo 'Record Updated';
        } else {
            echo "Something went wrong";
        }
    }
}