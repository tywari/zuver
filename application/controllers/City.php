<?php
class City extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'City_Model' );
		$this->load->model ( 'Country_Model' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'State_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/city.js' );
		
		// $this->render("city/add_edit_city", $data);
	}
	public function validationCity() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'stateId',
						'label' => 'State Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'countryId',
						'label' => 'Country Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'name',
						'label' => 'City Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[25]' 
				),
				array (
						'field' => 'latitude',
						'label' => 'Latitude',
						'rules' => 'trim|decimal|required' 
				),
				array (
						'field' => 'longitude',
						'label' => 'Longitude',
						'rules' => 'trim|decimal|required' 
				),
				array (
						'field' => 'radius',
						'label' => 'Radius',
						'rules' => 'trim|numeric|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/city.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "city";
		$data ['active_menu'] = 'city';
		$data ['state_list'] = $this->State_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['city_model'] = $this->City_Model->getBlankModel ();
		$this->render ( 'city/add_edit_city', $data );
	}
	public function saveCity() {
		$this->addJs ( 'app/city.js' );
		$data = array ();
		$data = $this->input->post ();
		$city_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($city_id > 0) {
			$this->City_Model->update ( $data, array (
					'id' => $city_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			
			$city_id = $this->City_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		
		// $this->render("city/add_edit_city", $data);
		
		$response = array (
				'msg' => $msg,
				'city_id' => $city_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($city_id, $view_mode) {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/city.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		
		/* $city_id = $this->input->post('id'); */
		
		if ($city_id > 0) {
			// Get release details by release_id
			$data ['city_model'] = $this->City_Model->getById ( $city_id );
		} else {
			// Blank Model
			$data ['city_model'] = $this->City_Model->getBlankModel ();
		}
		$data ['state_list'] = $this->State_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['activetab'] = "city";
		$data ['active_menu'] = 'city';
		
		$this->render ( 'city/add_edit_city', $data );
		/*
		 * $html = $this->load->view('city/add_edit_city', $data, TRUE);
		 *
		 * echo json_encode(array(
		 * 'html' => $html
		 *
		 * ));
		 */
	}
	public function getCityList() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/city.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		// $this->addJs('vendors/jszip/dist/jszip.min.js');
		// $this->addJs('vendors/pdfmake/build/pdfmake.min.js');
		// $this->addJs('vendors/pdfmake/build/vfs_fonts.js');
		
		$data ['activetab'] = "city";
		$data ['active_menu'] = 'city';
		$data ['city_model_list'] = $this->City_Model->getAll ( 'name' );
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "city/manage_city", $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$html = $this->render ( "city/manage_city", $data );
		}
	}
	public function changeCityStatus() {
		$data = array ();
		
		$this->addJs ( 'app/city.js' );
		
		$data = $this->input->post ();
		$city_id = $data ['id'];
		$driver_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($city_id) {
			$driver_updated = $this->City_Model->update ( array (
					'status' => $driver_status 
			), array (
					'id' => $city_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'city_id' => $city_id 
		);
		echo json_encode ( $response );
	}
}