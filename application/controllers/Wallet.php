<?php
class Wallet extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->model ( 'Passenger_Wallet_Details_Model' );
		$this->load->model ( 'Passenger_Model' );
		
	}
	public function index() {
		// $data = array();
		$this->addJs ( 'app/wallet.js' );
		
		// $this->render("transaction/add_edit_driver", $data);
	}
	public function add() {
		if ($this->User_Access_Model->showPassenger () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/wallet.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "transaction";
		$data ['active_menu'] = 'transaction';
		
		$data ['manipulation_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'MANIPULATION_TYPE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		$data ['wallet_details_model'] = $this->Passenger_Wallet_Details_Model->getBlankModel ();
		$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
		
		$this->render ( 'wallet/add_edit_wallet', $data );
	}
	public function saveWallet() {
		$data = array ();
		
		$this->addJs ( 'app/wallet.js' );
		
		$data = $this->input->post ();
		$wallet_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		$stauts = 0;
		if ($wallet_id > 0) {
			$data ['transactionType'] = Transaction_Type_Enum::MANUAL;
			$data ['transactionStatus'] = 'Success';
			$wallet_updated = $this->Passenger_Wallet_Details_Model->update ( $data, array (
					'id' => $wallet_id 
			) );
			if ($wallet_updated) {
				
				$msg = $msg_data ['updated'];
			} else {
				$msg = $msg_data ['failed'];
			}
		} else {
			$data ['transactionType'] = Transaction_Type_Enum::MANUAL;
			$data ['transactionStatus'] = 'Success';
			
			$wallet_id = $this->Passenger_Wallet_Details_Model->insert ( $data );
			if ($wallet_id) {
				$passenger_id = $this->input->post ( 'passengerId' );
				$data ['walletAmount'] = $this->input->post ( 'currentAmount' );
				$this->Passenger_Model->update ( $data, array (
						'id' => $passenger_id 
				) );
			}
			$msg = $msg_data ['success'];
			$stauts = 1;
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'wallet_id' => $wallet_id,
				'status' => $stauts,
				'passenger_id' => $passenger_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($wallet_id, $view_mode) {
		if ($this->User_Access_Model->showPassenger () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/wallet.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		if ($this->input->post ( 'id' )) {
			$wallet_id = $this->input->post ( 'id' );
		}
		$data ['manipulation_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'MANIPULATION_TYPE' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		if ($wallet_id > 0) {
			// Get release details by transaction_id
			$data ['wallet_details_model'] = $this->Passenger_Wallet_Details_Model->getById ( $wallet_id );
			$data ['passenger_model'] = $this->Passenger_Model->getById ( $data ['wallet_details_model']->passengerId );
			/*
			 * $data['transaction_details_model']=$this->Trip_Transaction_Details_Model->getOneByKeyValueArray(
			 * array('id'=>$data['transaction_details_model']->id));
			 */
		} else {
			// Blank Model
			
			$data ['wallet_details_model'] = $this->Passenger_Wallet_Details_Model->getBlankModel ();
			$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
		}
		$data ['activetab'] = "wallet";
		$data ['active_menu'] = 'wallet';
		
		if ($this->input->post ( 'id' )) {
			$html = $this->load->view ( 'wallet/add_edit_wallet', $data, TRUE );
			
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$this->render ( 'wallet/add_edit_wallet', $data );
		}
	}
	public function getWalletList($passenger_id = NULL) {
		if ($this->User_Access_Model->showPassenger () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// get type whether to load or render the ouptput
		
		$get_type = $this->input->post ( 'get_type' );
		$passenger_id = ($this->input->post ( 'passenger_id' )) ? $this->input->post ( 'passenger_id' ) : $passenger_id;
		$this->addJs ( 'app/transaction.js' );
		$this->addJs ( 'app/tabel.js' );
		$this->addJs ( 'app/wallet.js' );
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "wallet";
		$data ['active_menu'] = 'wallet';
		
		$company_id = $this->session->userdata ( 'user_company' );
		$city_id = $this->session->userdata ( 'user_city' );
		$post_data = $this->input->post ();
		if (array_key_exists ( 'city_id', $post_data )) {
			$city_id = $post_data ['city_id'];
		}
		
		$data ['wallet_model_list'] = $this->Query_Model->getWalletList ( $passenger_id );
		
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "wallet/manage_wallet", $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$html = $this->render ( "wallet/manage_wallet", $data );
		}
	}
	public function getPassengerInfo() {
		$keyword = $this->input->post ( 'keyword' );
		$data ['passenger_info'] = $this->Query_Model->getPassengerInfoForAutoComplete ( $keyword );
		if (! empty ( $data ['passenger_info'] )) {
			echo "<ul id='passenger-autocompletelist' style='width:535px'>";
			foreach ( $data ['passenger_info'] as $passenger ) {
				$passneger_name = $passenger->passenger_name;
				$passneger_id = $passenger->passenger_id;
				$prev_wallet_amount = $passenger->previous_wallet_amt;
				echo "<li onClick=\"selectPassenger('$passneger_id','$passneger_name','$prev_wallet_amount');\"> $passneger_name </li> ";
			}
			echo "</ul>";
		}
	}
}// end of class