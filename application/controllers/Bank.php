<?php
class Bank extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Bank_Account_Details_Model' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/bank.js' );
		
		// $this->render("city/add_edit_city", $data);
	}
	public function validationCity() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'stateId',
						'label' => 'State Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'countryId',
						'label' => 'Country Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'name',
						'label' => 'City Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[25]' 
				),
				array (
						'field' => 'latitude',
						'label' => 'Latitude',
						'rules' => 'trim|decimal|required' 
				),
				array (
						'field' => 'longitude',
						'label' => 'Longitude',
						'rules' => 'trim|decimal|required' 
				),
				array (
						'field' => 'radius',
						'label' => 'Radius',
						'rules' => 'trim|numeric|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/city.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "bank";
		$data ['active_menu'] = 'bank';
		
		$data ['bank_model'] = $this->Bank_Account_Details_Model->getBlankModel ();
		$this->render ( 'bank/add_edit_bank', $data );
	}
	public function saveBank() {
		$this->addJs ( 'app/bank.js' );
		$data = array ();
		$data = $this->input->post ();
		
		$bank_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($bank_id > 0) {
			$this->Bank_Account_Details_Model->update ( $data, array (
					'id' => $bank_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			
			$bank_id = $this->Bank_Account_Details_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		
		$file_names = array_keys ( $_FILES );
		$cheque_img_selected = $_FILES ['chequeImage'] ['name'];
		if ($cheque_img_selected [0] != '') {
			
			$this->cheque_image_upload ( $file_names, $driver_id, $cheque_img_selected );
		}
		
		// $this->render("city/add_edit_city", $data);
		
		$response = array (
				'msg' => $msg,
				'bank_id' => $bank_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($bank_id, $view_mode) {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/bank.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		
		/* $bank_id = $this->input->post('id'); */
		
		if ($bank_id > 0) {
			// Get release details by release_id
			$data ['bank_model'] = $this->Bank_Account_Details_Model->getById ( $bank_id );
		} else {
			// Blank Model
			$data ['bank_model'] = $this->Bank_Account_Details_Model->getBlankModel ();
		}
		
		$data ['activetab'] = "bank";
		$data ['active_menu'] = 'bank';
		
		$this->render ( 'bank/add_edit_bank', $data );
		/*
		 * $html = $this->load->view('bank/add_edit_bank', $data, TRUE);
		 *
		 * echo json_encode(array(
		 * 'html' => $html
		 *
		 * ));
		 */
	}
	public function getBankList() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/bank.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		// $this->addJs('vendors/jszip/dist/jszip.min.js');
		// $this->addJs('vendors/pdfmake/build/pdfmake.min.js');
		// $this->addJs('vendors/pdfmake/build/vfs_fonts.js');
		
		$data ['activetab'] = "bank";
		$data ['active_menu'] = 'bank';
		$data ['bank_model_list'] = $this->Bank_Account_Details_Model->getAll ( 'id' );
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "bank/manage_bank", $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$html = $this->render ( "bank/manage_bank", $data );
		}
	}
	public function changeBankStatus() {
		$data = array ();
		
		$this->addJs ( 'app/bank.js' );
		
		$data = $this->input->post ();
		$bank_id = $data ['id'];
		$driver_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($bank_id) {
			$driver_updated = $this->Bank_Account_Details_Model->update ( array (
					'status' => $driver_status 
			), array (
					'id' => $bank_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'city_id' => $bank_id 
		);
		echo json_encode ( $response );
	}
	/**
	 * To upload the Cheque leaf
	 * 
	 * @param unknown $file_names        	
	 * @param unknown $driver_id        	
	 * @param unknown $profile_img_selected        	
	 */
	public function cheque_image_upload($file_names, $bank_id, $cheque_img_selected) {
		// to get file input name from the form
		$cheque_img_form_name = $file_names [0];
		// in which name img should be uploaded
		$profile_image_name = "CHEQUE-" . $bank_id;
		// upload path
		$folderName = $bank_id;
		$cheque_upload_path = $this->config->item ( 'bank_content_path' ) . $bank_id;
		
		if (! file_exists ( $cheque_upload_path )) {
			mkdir ( $cheque_upload_path, 0777 );
		}
		
		// $profile_upload_path = driver_profile_url();
		$upload = $this->do_upload ( $cheque_img_form_name, $cheque_img_selected, $profile_image_name, $cheque_upload_path );
		$upload_image_name = implode ( ',', $upload );
		if ($upload) {
			$data ['profileImage'] = $upload_image_name;
			$update_prof_imgname = $this->Bank_Account_Details_Model->update ( $data, array (
					'id' => $bank_id 
			) );
		}
	}
	
	// function where files get uploaded
	public function do_upload($img_form_name, $selected_img, $img_prefix, $upload_path) {
		$upload_image_name = array ();
		// Count # of uploaded files in array
		$total = count ( $_FILES [$img_form_name] ['name'] );
		// Loop through each file
		for($i = 0; $i < $total; $i ++) {
			// Get the temp file path
			$tmpFilePath = $_FILES [$img_form_name] ['tmp_name'] [$i];
			
			// Make sure we have a filepath
			if ($tmpFilePath != "") {
				// get image extension
				$img_ext = pathinfo ( $selected_img [$i], PATHINFO_EXTENSION );
				$image_full_name = $img_prefix . "-$i" . '.' . $img_ext;
				$newFilePath = $upload_path . '/' . $image_full_name;
				
				// Upload the file into the temp dir
				if (move_uploaded_file ( $tmpFilePath, $newFilePath )) {
					
					$upload_image_name [] = $image_full_name;
				}
			}
		}
		return $upload_image_name;
	}
}