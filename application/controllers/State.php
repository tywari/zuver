<?php
class State extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		
		$this->load->model ( 'State_Model' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Country_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/state.js' );
		
		// $this->render("state/add_edit_state", $data);
	}
	public function validationState() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'countryId',
						'label' => 'Country Name',
						'rules' => 'trim|required|' 
				),
				array (
						'field' => 'name',
						'label' => 'State Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[25]' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		
		$this->addJs ( 'app/state.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "state";
		$data ['active_menu'] = 'state';
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getColumnByKeyValueArray ( 'id,name', array (
				'status' => 'Y' 
		), 'id' );
		$data ['state_model'] = $this->State_Model->getBlankModel ();
		$this->render ( 'state/add_edit_state', $data );
	}
	public function saveState() {
		$this->addJs ( 'app/state.js' );
		$data = array ();
		$data = $this->input->post ();
		$state_id = $data ['id'];
		
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($state_id > 0) {
			$this->State_Model->update ( $data, array (
					'id' => $state_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			$state_id = $this->State_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		
		// $this->render("state/add_edit_state", $data);
		
		$response = array (
				'msg' => $msg,
				'state_id' => $state_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($state_id, $view_mode) {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/state.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		if ($this->input->post ( 'id' )) {
			$state_id = $this->input->post ( 'id' );
		}
		if ($state_id > 0) {
			// Get release details by release_id
			$data ['state_model'] = $this->State_Model->getById ( $state_id );
		} else {
			// Blank Model
			$data ['state_model'] = $this->State_Model->getBlankModel ();
		}
		$data ['activetab'] = "state";
		$data ['active_menu'] = 'state';
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		
		if ($this->input->post ( 'id' )) {
			$html = $this->load->view ( 'state/add_edit_state', $data, TRUE );
			
			echo json_encode ( array (
					'html' => $html 
			)
			 );
		} else {
			$this->render ( 'state/add_edit_state', $data );
		}
	}
	public function getStateList() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/state.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "state";
		$data ['active_menu'] = 'state';
		$data ['state_model_list'] = $this->Query_Model->getStateList ();
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "state/manage_state", $data );
			
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$html = $this->render ( "state/manage_state", $data );
		}
	}
	public function changeStateStatus() {
		$data = array ();
		
		$this->addJs ( 'app/state.js' );
		
		$data = $this->input->post ();
		$state_id = $data ['id'];
		$driver_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($state_id) {
			$driver_updated = $this->State_Model->update ( array (
					'status' => $driver_status 
			), array (
					'id' => $state_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'state_id' => $state_id 
		);
		echo json_encode ( $response );
	}
}