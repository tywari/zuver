<?php
class Site extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Site_Details_Model' );
		$this->load->model ( 'Site_Social_Media_Details_Model' );
		$this->load->model ( 'City_Model' );
		$this->load->model ( 'State_Model' );
		$this->load->model ( 'Country_Model' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/site.js' );
		
		// $this->render("site/add_edit_site", $data);
	}
	public function validationSite() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'title',
						'label' => 'Title',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[30]' 
				),
				array (
						'field' => 'tagLine',
						'label' => 'Tag Line',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[100]' 
				),
				array (
						'field' => 'description',
						'label' => 'Description',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'metaKeyword',
						'label' => 'Meta Keywords',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email' 
				),
				array (
						'field' => 'contactNo',
						'label' => 'Contact No',
						'rules' => 'trim|required|numeric|exact_length[10]' 
				),
				array (
						'field' => 'emergencyEmail',
						'label' => 'Emergency Email',
						'rules' => 'trim|required|valid_email' 
				),
				
				array (
						'field' => 'emergencyContactNo',
						'label' => 'Emergency Contact No',
						'rules' => 'trim|required|numeric|exact_length[10]' 
				),
				
				array (
						'field' => 'sendSms',
						'label' => 'Send SMS',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'tollFreeNo',
						'label' => 'Toll Free No',
						'rules' => 'trim|required|alpha_numeric' 
				),
				array (
						'field' => 'notificationTimeout',
						'label' => 'Notification Timeout',
						'rules' => 'trim|required|integer|greater_than[1]|less_than[4]' 
				),
				array (
						'field' => 'adminCommission',
						'label' => 'Admin Commission',
						'rules' => 'trim|required|decimal' 
				),
				array (
						'field' => 'referralDiscountAmount',
						'label' => 'Referral Discount Amount',
						'rules' => 'trim|required|decimal' 
				),
				array (
						'field' => 'androidAppUrl',
						'label' => 'Android APP URL',
						'rules' => 'trim|required|alpha_numeric' 
				),
				array (
						'field' => 'iphoneAppUrl',
						'label' => 'Iphone APP URL',
						'rules' => 'trim|required|alpha_numeric' 
				),
				array (
						'field' => 'tax',
						'label' => 'Tax',
						'rules' => 'trim|required|decimal' 
				),
				array (
						'field' => 'copyrights',
						'label' => 'Copy Rights',
						'rules' => 'trim|alpha_numeric|required' 
				),
				array (
						'field' => 'passengerTellToFriendMsg',
						'label' => 'Passenger Tell To Friend Msg',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'driverTellToFriendMsg',
						'label' => 'Driver Tell To Friend Msg',
						'rules' => 'trim|required' 
				),
				
				array (
						'field' => 'logo',
						'label' => 'Logo',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'emailLogo',
						'label' => 'Email Logo',
						'rules' => 'trim|required|alpha' 
				),
				array (
						'field' => 'favicon',
						'label' => 'Favicon',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'countryId',
						'label' => 'Country Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'stateId',
						'label' => 'State Name',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'cityId',
						'label' => 'City Name',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/site.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "site";
		$data ['active_menu'] = 'site';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['state_list'] = $this->State_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		), 'id' );
		$data ['site_model'] = $this->Site_Details_Model->getBlankModel ();
		$data ['site_social_media_model'] = array ();
		$this->render ( 'site/add_edit_site', $data );
	}
	public function saveSite() {
		$this->addJs ( 'app/site.js' );
		$data = array ();
		$data = $this->input->post ();
		$city_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($city_id > 0) {
			$this->site_model->update ( $data, array (
					'id' => $city_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			$password = $data [email] . "_" . $data ['mobile'];
			// @todo encrypted password
			$data ['password'] = hash ( "sha256", $password );
			// @todo generate 6 digit otp
			$data ['otp'] = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			// @todo unique site code
			$data ['passengerCode'] = '';
			
			$city_id = $this->Site_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		
		// $this->render("site/add_edit_site", $data);
		
		$response = array (
				'msg' => $msg,
				'city_id' => $city_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($site_id, $view_mode = EDIT_MODE) {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/site.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		if ($this->input->post ( 'id' )) {
			$site_id = $this->input->post ( 'id' );
		}
		if ($site_id > 0) {
			// Get release details by release_id
			$data ['site_model'] = $this->Site_Details_Model->getById ( $site_id );
			$data ['site_social_media_model'] = $this->Site_Social_Media_Details_Model->getByKeyValueArray ( array (
					'siteId' => $site_id 
			) );
		} else {
			// Blank Model
			$data ['site_model'] = $this->Site_Details_Model->getBlankModel ();
			$data ['site_social_media_model'] = array ();
		}
		$data ['activetab'] = "site";
		$data ['active_menu'] = 'site';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['country_list'] = $this->Country_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		$data ['state_list'] = $this->State_Model->getSelectDropdownOptions ( array (
				'status' => 'Y' 
		) );
		
		if ($this->input->post ( 'id' )) {
			$html = $this->render ( 'site/add_edit_site', $data );
			echo json_encode ( array (
					'html' => $html 
			)
			 );
		} else {
			$this->render ( 'site/add_edit_site', $data );
		}
	}
	public function getSiteList() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'app/site.js' );
		$this->addJs ( 'app/tabel.js' );
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "site";
		$data ['active_menu'] = 'site';
		$data ['site_model_list'] = $this->Site_Details_Model->getAll ( 'id' );
		// $html=$this->render("site/manage_site", $data);
		
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "site/manage_site", $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$html = $this->render ( "site/manage_site", $data );
		}
	}
	public function changeSiteStatus() {
		$data = array ();
		
		$this->addJs ( 'app/site.js' );
		
		$data = $this->input->post ();
		$site_id = $data ['id'];
		$driver_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($site_id) {
			$driver_updated = $this->Site_Details_Model->update ( array (
					'status' => $driver_status 
			), array (
					'id' => $site_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'site_id' => $site_id 
		);
		echo json_encode ( $response );
	}
}