<?php
class Driver extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'City_Model' );
		$this->load->model ( 'Auth_Key_Model' );
		$this->load->model ( 'Api_Webservice_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Driver_Productivity_Allowance_Model' );
		$this->load->model ( 'Driver_Shift_History_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
		$this->load->library ( 'form_validation' );
		$this->load->helper ('excel_generate_helper');
		$this->change_password_validation=array(
			array(
				'field' => 'newPassword',
				'label' => 'New Password',
				'rules' => 'trim|required|min_length[7]|max_length[20]|matches[confirmPassword]' ),
			array(
				'field' => 'confirmPassword',
				'label' => 'Confirm Password',
				'rules' => 'trim|required|min_length[7]|max_length[20]' )
			);
	}
	public function index() {
		// $data = array();
		$this->addJs ( 'app/driver.js' );
		// $this->render("driver/add_edit_driver", $data);
	}
	public function validationDriver() {
		$config = array (
				array (
						'field' => 'firstName',
						'label' => 'First Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[15]'
				),
				array (
						'field' => 'lastName',
						'label' => 'Last Name',
						'rules' => 'trim|required|alpha|min_length[3]|max_length[15]'
				),
				array (
						'field' => 'gender',
						'label' => 'Gender',
						'rules' => 'trim|required'
				),
				array (
						'field' => 'dob',
						'label' => 'Date of Birth',
						'rules' => 'trim|required'
				),
				array (
						'field' => 'profileImage',
						'label' => 'Profile Image',
						'rules' => 'trim|required'
				),
				array (
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email'
				),
				array (
						'field' => 'mobile',
						'label' => 'Mobile',
						'rules' => 'trim|required|numeric|exact_length[10]'
				),

				array (
						'field' => 'address',
						'label' => 'Address',
						'rules' => 'trim|required'
				),

				array (
						'field' => 'driverLanguagesKnown',
						'label' => 'Languages Known',
						'rules' => 'trim|required|alpha'
				),
				array (
						'field' => 'addressProofNo',
						'label' => 'Address Proof No',
						'rules' => 'trim|required|alpha_numeric'
				),
				array (
						'field' => 'addressProofImage',
						'label' => 'Address Proof Image',
						'rules' => 'trim|required'
				),
				array (
						'field' => 'identityProofNo',
						'label' => 'Identity Proof No',
						'rules' => 'trim|required|alpha_numeric'
				),
				array (
						'field' => 'identityProofImage',
						'label' => 'Identity Proof Image',
						'rules' => 'trim|required'
				),
				array (
						'field' => 'panCardNo',
						'label' => 'PAN Card No',
						'rules' => 'trim|required|alpha_numeric'
				),
				array (
						'field' => 'panCardImage',
						'label' => 'PAN Card Image',
						'rules' => 'trim|required'
				),
				array (
						'field' => 'driverQualification',
						'label' => 'Qualification',
						'rules' => 'trim|required|alpha_numeric'
				),
				array (
						'field' => 'driverExperience',
						'label' => 'Experience',
						'rules' => 'trim|alpha_numeric|required'
				),
				array (
						'field' => 'drivingLicenseNo',
						'label' => 'Driving License No',
						'rules' => 'trim|required|alpha_numeric'
				),
				array (
						'field' => 'drivingLicenseExpireDate',
						'label' => 'Driving License ExpireDate',
						'rules' => 'trim|required'
				),

				array (
						'field' => 'drivingLicenseImage',
						'label' => 'Driving License Image',
						'rules' => 'trim|required'
				),
				array (
						'field' => 'skills',
						'label' => 'Skills',
						'rules' => 'trim|required|alpha'
				),
				array (
						'field' => 'favouriteCar',
						'label' => 'Favourite Car',
						'rules' => 'trim|required|alpha_numeric'
				),
				array (
						'field' => 'companyId',
						'label' => 'Company Name',
						'rules' => 'trim|required'
				),
				array (
						'field' => 'cityId',
						'label' => 'City Name',
						'rules' => 'trim|required'
				),
				array (
						'field' => 'driverType',
						'label' => 'Driver Type',
						'rules' => 'trim|required'
				)
		);

		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function add() {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		//<script src="//code.jquery.com/jquery-1.12.4.js"></script>
  		//<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		//$this->addJs('https://code.jquery.com/jquery-1.12.4.js');
		//$this->addJs('https://code.jquery.com/ui/1.12.1/jquery-ui.js');
		$this->addJs ( 'datepicker/jquery.datetimepicker.full.js' );
		$this->addJs ( 'app/driver.js' );

		$data = array (
				'mode' => EDIT_MODE
		);
		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'id' );
		// $data['driver_gender']=$this->Data_Attributes_Model->getSelectDropdownOptions(array('tableName'=>strtoupper('GENDER_TYPE'),'isVisible'=>'Y'),'sequenceOrder');
		$data ['driver_gender'] = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'enumName,description', array (
				'tableName' => strtoupper ( 'GENDER_TYPE' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['driver_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'DRIVER_TYPE' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['driver_skill_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'DRIVER_SKILL' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['transmission_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSMISSION_TYPE' )
		), 'sequenceOrder' );
		$data ['license_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'LICENSE_TYPE' )
		), 'sequenceOrder' );
		$data ['driver_model'] = $this->Driver_Model->getBlankModel ();
		$data ['driver_personal_details_model'] = $this->Driver_Personal_Details_Model->getBlankModel ();
		$data ['driver_productivity_allowance_model'] = array();
		$this->render ( 'driver/add_edit_driver', $data );
	}

	// on May 17 2017, Aditya created a new API in application->controllers->ApiWebService based on his understanding of this function,
	// no changes is being done to this function,
	// this comment is simply to help remind anyone updating this function that you also need to update another function in ApiWebservice file called
	public function saveDriver() {
		$data = array ();

		$this->addJs ( 'app/driver.js' );

		$data = $this->input->post ();
		//print_r($data);
		//error_log(print_r($data));// die();
		$driver_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($driver_id > 0) {
			$driver_updated = $this->Driver_Model->update ( $data, array (
					'id' => $driver_id
			) );
			if ($driver_updated) {
				$driver_personal_details_updated = $this->Driver_Personal_Details_Model->update ( $data, array (
						'driverId' => $driver_id
				) );

				$this->Driver_Productivity_Allowance_Model->delete(array ('driverId' => $driver_id ) );

				if($data['driverType']=='TBD')
				{
					foreach ($data['productivity_allowance'] as $productivity_allowance_data) {
						$productivity_allowance_data['driverId']=$driver_id ;
						$driver_productivity_allowance = $this->Driver_Productivity_Allowance_Model->insert($productivity_allowance_data);
					}
				}
				$msg = $msg_data ['updated'];
			} else {
				$msg = $msg_data ['failed'];
			}
		} else {
			$password = random_string ( 'alnum', 8 );
			// @todo encrypted password
			$data ['password'] = hash ( "sha256", $password );
			// @todo generate 6 digit otp
			$data ['otp'] = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			// @todo unique passenger code
			$data ['driverCode'] = trim ( substr ( strtoupper ( str_replace ( ' ', '', $data ['firstName'] . '' . $data ['lastName'] ) ), 0, 5 ) );
			$data ['driverCode'] .= $this->Driver_Model->getAutoIncrementValue ();

			$driver_id = $this->Driver_Model->insert ( $data );
			if ($driver_id) {
				$data ['driverId'] = $driver_id; // get all files names from the form

				$driver = '';
				$driver_personal_details = $this->Driver_Personal_Details_Model->insert ( $data );
				$driver_shift_data=array('driverId'=>$driver_id,'availabilityStatus'=>Driver_Available_Status_Enum::FREE,'shiftStatus'=>Driver_Shift_Status_Enum::SHIFTOUT);
				$driver_shift_history = $this->Driver_Shift_History_Model->insert($driver_shift_data);
				$file_names = array_keys ( $_FILES );

				$profile_img_selected = $_FILES ['profileImage'] ['name'];
				$addproof_img_selected = $_FILES ['addressProofImage'] ['name'];
				$idproof_img_selected = $_FILES ['identityProofImage'] ['name'];
				$pancard_img_selected = $_FILES ['panCardImage'] ['name'];
				$license_img_selected = $_FILES ['drivingLicenseImage'] ['name'];
				$verified_doc_selected = $_FILES ['verifiedDocument'] ['name'];
				// upload profile image
				if ($profile_img_selected [0] != '') {

					$this->profile_image_upload ( $file_names, $driver_id, $profile_img_selected );
				}
				// address proof image
				if ($addproof_img_selected [0] != '') {
					// in which name img should be uploaded
					$add_prefix = "ADD-" . $driver_id;
					// $address_upload_path = "./public/uploads/driver_documents/address_proof/$driver_id/";
					$address_upload_path = $this->config->item ( 'driver_content_path' ) . $driver_id;
					$this->doc_image_upload ( $file_names [1], $driver_id, $addproof_img_selected, $add_prefix, $address_upload_path );
				}
				// Id Proof Image Upload
				if ($idproof_img_selected [0] != '') {
					$id_prefix = "ID-" . $driver_id;
					// $idproof_upload_path = "./public/uploads/driver_documents/id_proof/$driver_id/";
					$idproof_upload_path = $this->config->item ( 'driver_content_path' ) . $driver_id;

					$this->doc_image_upload ( $file_names [2], $driver_id, $idproof_img_selected, $id_prefix, $idproof_upload_path );
				}

				// pancard image upload
				if ($pancard_img_selected [0] != '') {
					// in which name img should be uploaded
					$pan_prefix = "PAN-" . $driver_id;
					// $pan_upload_path = "./public/uploads/driver_documents/pan/$driver_id/";
					$pan_upload_path = $this->config->item ( 'driver_content_path' ) . $driver_id;
					$this->doc_image_upload ( $file_names [3], $driver_id, $pancard_img_selected, $pan_prefix, $pan_upload_path );
				}

				// License image Upload
				if ($license_img_selected [0] != '') {
					// in which name img should be uploaded
					$lics_prefix = "LICS-" . $driver_id;
					// $lics_upload_path = "./public/uploads/driver_documents/license/$driver_id/";
					$lics_upload_path = $this->config->item ( 'driver_content_path' ) . $driver_id;
					$this->doc_image_upload ( $file_names [4], $driver_id, $license_img_selected, $lics_prefix, $lics_upload_path );
				}
				// Verified Document Upload
				if ($verified_doc_selected [0] != '') {
					// in which name img should be uploaded
					$ver_prefix = "VER-" . $driver_id;
					// $lics_upload_path = "./public/uploads/driver_documents/license/$driver_id/";
					$ver_upload_path = $this->config->item ( 'driver_content_path' ) . $driver_id;
					$this->doc_image_upload ( $file_names [5], $driver_id, $verified_doc_selected, $ver_prefix, $ver_upload_path );
				}
			}
			if ($driver_id > 0) {
				/**Entry to productivity performance**/
				if($data['driverType']=='TBD')
				{
					foreach ($data['productivity_allowance'] as $productivity_allowance_data) {
						$productivity_allowance_data['driverId']=$driver_id ;
						$driver_productivity_allowance = $this->Driver_Productivity_Allowance_Model->insert($productivity_allowance_data);
					}
				}

				// SMS & email service start

				if (SMS && $driver_id) {
					$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
							'smsTitle' => 'account_create_sms'
					) );

					$message_data = $message_details->smsContent;
					$web_link = array ();
					$web_link [0] = substr ( DRIVERAPPLINK, 0, 20 );
					$web_link [1] = substr ( DRIVERAPPLINK, 20, 20 );
					$web_link [2] = substr ( DRIVERAPPLINK, 40, 20 );
					$web_link [3] = substr ( DRIVERAPPLINK, 60, 20 );
					$web_link [4] = substr ( DRIVERAPPLINK, 80, 20 );
					$web_link [5] = substr ( DRIVERAPPLINK, 100, 20 );
					// $address[6]=substr($current_location,120,20);
					// $address[7]=substr($current_location,140,20);

					if (strlen ( DRIVERAPPLINK ) > 120) {
						$web_link [5] = cut_string_using_last ( ',', $web_link [5], 'left', false );
					}
					$message_data = str_replace ( "##USERNAME##", $data ['mobile'], $message_data );
					$message_data = str_replace ( "##PASSWORD##", $password, $message_data );
					$message_data = str_replace ( "##APPLINK1##", $web_link [0], $message_data );
					$message_data = str_replace ( "##APPLINK2##", $web_link [1], $message_data );
					$message_data = str_replace ( "##APPLINK3##", $web_link [2], $message_data );
					$message_data = str_replace ( "##APPLINK4##", $web_link [3], $message_data );
					$message_data = str_replace ( "##APPLINK5##", $web_link [4], $message_data );
					$message_data = str_replace ( "##APPLINK6##", $web_link [5], $message_data );
					$message_data = str_replace ( "##APPLINK7##", '', $message_data );
					// $message_data = str_replace("##APPLINK8##",'',$message_data);
					// print_r($message);exit;
					if ($data ['mobile'] != "") {

						$this->Api_Webservice_Model->sendSMS ( $data ['mobile'], $message_data );
						// this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
						$this->Api_Webservice_Model->sendSMS ( '8451976667', $message_data );
						// this SMS is sending to below numbers on request through WhatsApp from Sidhanth on 10/01/2017
					}
				}

				$from = SMTP_EMAIL_ID;
				$to = $data ['email'];
				$subject = 'Driver Registration';

				$data = array (
						'driver_name' => $data ['firstName'] . ' ' . $data ['lastName'],
						'mobile' => $data ['mobile'],
						'password' => $password
				);
				$message_data = $this->load->view ( 'emailtemplate/driver_register', $data, TRUE );

				if (SMTP && $driver_id) {

					if ($to) {
						$this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
					}
				} else {

					// To send HTML mail, the Content-type header must be set
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Additional headers
					$headers .= 'From: Zuver<' . $from . '>' . "\r\n";
					$headers .= 'To: <' . $to . '>' . "\r\n";
					if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
						mail ( $to, $subject, $message_data, $headers );
					}
				}

				// SMS & email service end
			}
			$msg = $msg_data ['success'];
		}

		// $this->render("passenger/add_edit_passenger", $data);

		$response = array (
				'msg' => $msg,
				'driverId' => $driver_id
		);
		echo json_encode ( $response );
	}
	// driver profile image upload
	public function profile_image_upload($file_names, $driver_id, $profile_img_selected) {
		// to get file input name from the form
		$profile_img_form_name = $file_names [0];
		// in which name img should be uploaded
		$profile_image_name = "PRF-" . $driver_id;
		// upload path
		$folderName = $driver_id;
		$profile_upload_path = $this->config->item ( 'driver_content_path' ) . $driver_id;

		if (! file_exists ( $profile_upload_path )) {
			mkdir ( $profile_upload_path, 0777 );
		}

		// $profile_upload_path = driver_profile_url();
		$upload = $this->do_upload ( $profile_img_form_name, $profile_img_selected, $profile_image_name, $profile_upload_path );
		$upload_image_name = implode ( ',', $upload );
		if ($upload) {
			$data ['profileImage'] = $upload_image_name;
			$update_prof_imgname = $this->Driver_Model->update ( $data, array (
					'id' => $driver_id
			) );
		}
	}
	// driver document image upload
	public function doc_image_upload($img_form_name, $driver_id, $img_selected, $img_prefix, $upload_path) {
		if (! file_exists ( $upload_path )) {
			mkdir ( $upload_path, 0777 );
		}

		// $profile_upload_path = driver_profile_url();
		$upload = $this->do_upload ( $img_form_name, $img_selected, $img_prefix, $upload_path );
		$upload_image_name = implode ( ',', $upload );
		if ($upload) {
			$data [$img_form_name] = $upload_image_name;
			$update_prof_imgname = $this->Driver_Personal_Details_Model->update ( $data, array (
					'driverId' => $driver_id
			) );
		}
	}
	// function where files get uploaded
	public function do_upload($img_form_name, $selected_img, $img_prefix, $upload_path) {
		$upload_image_name = array ();
		// Count # of uploaded files in array
		$total = count ( $_FILES [$img_form_name] ['name'] );
		// Loop through each file
		for($i = 0; $i < $total; $i ++) {
			// Get the temp file path
			$tmpFilePath = $_FILES [$img_form_name] ['tmp_name'] [$i];

			// Make sure we have a filepath
			if ($tmpFilePath != "") {
				// get image extension
				$img_ext = pathinfo ( $selected_img [$i], PATHINFO_EXTENSION );
				$image_full_name = $img_prefix . "-$i" . '.' . $img_ext;
				$newFilePath = $upload_path . '/' . $image_full_name;

				// Upload the file into the temp dir
				if (move_uploaded_file ( $tmpFilePath, $newFilePath )) {

					$upload_image_name [] = $image_full_name;
				}
			}
		}
		return $upload_image_name;
	}
	public function getDetailsById($driver_id, $view_mode) {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();

		$this->addJs ( 'app/driver.js' );
		$data = array (
				'mode' => $view_mode
		);

		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		$data ['city_list'] = $this->City_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'name' );
		$data ['company_list'] = $this->Corporate_Partners_Details_Model->getSelectDropdownOptions ( array (
				'status' => 'Y'
		), 'id' );
		$data ['driver_gender'] = $this->Data_Attributes_Model->getColumnByKeyValueArray ( 'enumName,description', array (
				'tableName' => strtoupper ( 'GENDER_TYPE' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		// new section added by Aditya on May 25 2017
		$data ['working_days_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'working_days' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		// $data ['shift_preference_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
		// 		'tableName' => strtoupper ( 'shift_preference' ),
		// 		'isVisible' => 'Y'
		// ), 'sequenceOrder' );
		// $data ['employment_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
		// 		'tableName' => strtoupper ( 'employment_type' ),
		// 		'isVisible' => 'Y'
		// ), 'sequenceOrder' );
		// section end
		$data ['driver_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'DRIVER_TYPE' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['driver_skill_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'DRIVER_SKILL' ),
				'isVisible' => 'Y'
		), 'sequenceOrder' );
		$data ['transmission_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'TRANSMISSION_TYPE' )
		), 'sequenceOrder' );
		$data ['license_type_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'LICENSE_TYPE' )
		), 'sequenceOrder' );
		if ($this->input->post ( 'id' )) {
			$driver_id = $this->input->post ( 'id' );
		}
		if ($driver_id > 0) {
			// Get release details by driver_id
			$data ['driver_model'] = $this->Driver_Model->getById ( $driver_id );
			$shiftArray = $this->Driver_Model->currentShiftStatus ( $driver_id );
			$data ['shiftStatus'] = $shiftArray[0]['shiftStatus'] ;
			$data ['driver_personal_details_model'] = $this->Driver_Personal_Details_Model->getOneByKeyValueArray ( array (
					'driverId' => $data ['driver_model']->id
			) );
			$data ['driver_productivity_allowance_model'] = $this->Driver_Productivity_Allowance_Model->getByKeyValueArray ( array (
					'driverId' => $data ['driver_model']->id
			) );
		} else {
			// Blank Model
			$data ['driver_model'] = $this->driver_model->getBlankModel ();
			$data ['driver_personal_details_model'] = $this->Driver_Personal_Details_Model->getBlankModel ();
			$data ['driver_productivity_allowance_model'] = $this->Driver_Productivity_Allowance_Model->getBlankModel ();
		}
		if ($this->input->post ( 'id' )) {

			$html = $this->load->view ( 'driver/add_edit_driver', $data );
			echo json_encode ( array (
					'html' => $html
			) );
		} else {
			$this->render ( 'driver/add_edit_driver', $data );
		}
	}
	public function getDriverList() {
		if ($this->User_Access_Model->showDriver () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// debug($this->Driver_Model->getAutoIncrementValue());
		// debug_exit($this->Passenger_Model->getAutoIncrementValue());
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/driver.js' );
		$this->addJs ( 'app/tabel.js' );

		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );

		$city_id = $this->session->userdata ( 'user_city' );
		$post_data = $this->input->post ();
		if (array_key_exists ( 'city_id', $post_data )) {
			$city_id = $post_data ['city_id'];
		}

		$data ['driver_model_list'] = $this->Query_Model->getDriverList ( $city_id );

		$data ['activetab'] = "driver";
		$data ['active_menu'] = 'driver';
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "driver/manage_driver", $data );
			echo json_encode ( array (
					'html' => $html
			) );
		} else {
			$html = $this->render ( "driver/manage_driver", $data );
		}
	}
	public function changeDriverStatus() {
		$data = array ();

		$this->addJs ( 'app/driver.js' );

		$data = $this->input->post ();
		$driver_id = $data ['id'];
		$driver_status = $data ['status'];

		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($driver_id) {
			$driver_updated = $this->Driver_Model->update ( array (
					'status' => $driver_status
			), array (
					'id' => $driver_id
			) );
			if ($driver_status == Status_Type_Enum::INACTIVE) {
				$auth_key_exists = $this->Auth_Key_Model->getOneByKeyValueArray ( array (
						'userId' => $driver_id,
						'userType' => User_Type_Enum::DRIVER
				) );
				// If user already exists update auth key
				if ($auth_key_exists) {
					$this->Auth_Key_Model->update ( array (
							'key' => ''
					), array (
							'userId' => $driver_id,
							'userType' => User_Type_Enum::DRIVER
					) );
				}
				if ($driver_id > 0) {
					// SMS & email service start
					$driver_details = $this->Driver_Model->getById ( $driver_id );
					if (SMS && $driver_updated) {
						// @todo SMS template to be changed later
						$message_details = $this->Sms_Template_Model->getOneByKeyValueArray ( array (
								'smsTitle' => 'forgot_password_sms'
						) );

						$message_data = $message_details->smsContent;
						// $message = str_replace("##USERNAME##",$name,$message);
						// $message_data = str_replace ( "##PASSWORD##", $password, $message_data );
						// print_r($message);exit;
						if ($driver_details->mobile != "") {

							// $this->Api_Webservice_Model->sendSMS ( $driver_details->mobile, $message_data );
						}
					}

					$from = SMTP_EMAIL_ID;
					$to = $driver_details->email;
					$subject = 'Driver De-Activated';

					$data = array (
							'driver_name' => $driver_details->firstName . ' ' . $driver_details->lastName
					);
					$message_data = $this->load->view ( 'emailtemplate/driver_inactive', $data, TRUE );

					if (SMTP && $driver_updated) {

						if ($to) {
							$this->Api_Webservice_Model->sendEmail ( $to, $subject, $message_data );
						}
					} else {

						// To send HTML mail, the Content-type header must be set
						$headers = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						// Additional headers
						$headers .= 'From: Zuver<' . $from . '>' . "\r\n";
						$headers .= 'To: <' . $to . '>' . "\r\n";
						if (! filter_var ( $to, FILTER_VALIDATE_EMAIL ) === false) {
							mail ( $to, $subject, $message_data, $headers );
						}
					}

					// SMS & email service end
				}
			}
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'driver_id' => $driver_id
		);
		echo json_encode ( $response );
	}
	public function changeDriverVerificationStatus() {
		$data = array ();

		$this->addJs ( 'app/driver.js' );


		$data = $this->input->post ();
		$driver_id = $data ['id'];
		$driver_status = $data ['status'];

		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($driver_id) {
			$driver_doj = $this->Driver_Model->getDriverJoiningDate($driver_id);
			if ( (($driver_doj == '0000-00-00') || ($driver_doj == '') || ($driver_doj == null)) && ($driver_status == 'Y') )
			{
				// now also set Joining Date as today's date
				$driver_updated = $this->Driver_Model->update ( array (
						'doj' => date('Y-m-d'),
						'isVerified' => $driver_status
				), array (
						'id' => $driver_id
				) );
			} else {
				$driver_updated = $this->Driver_Model->update ( array (
						'isVerified' => $driver_status
				), array (
						'id' => $driver_id
				) );
			}

			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'driver_id' => $driver_id
		);
		echo json_encode ( $response );
	}
	/***Need to Update this code to disconnect Login/Logout from OnDuty/OffDuty this connection is not right***/
	public function changeDriverLoginStatus() {
		$data = array ();

		$this->addJs ( 'app/driver.js' );

		$data = $this->input->post ();
		$driver_id = $data ['id'];
		$login_status = $data ['status'];

		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($driver_id) {
			$driver_updated = $this->Driver_Model->update ( array (
					'loginStatus' => $login_status
			), array (
					'id' => $driver_id
			) );
			if ($driver_updated)
			{
				$last_driver_shift = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
												'driverId' => $driver_id
										), 'id DESC' );
				//print_r($last_driver_shift);
				$insert_data = array (
						'driverId' => $driver_id,
						'inLatitude' => '',
						'inLongitude' => '',
						'availabilityStatus' => Driver_Available_Status_Enum::FREE
					);
				if($login_status=='Y')
				{
					$shiftStatus = Driver_Shift_Status_Enum::SHIFTIN ;
				}
				else
				{
					$shiftStatus = Driver_Shift_Status_Enum::SHIFTOUT ;
				}
				$insert_data['shiftStatus'] = $shiftStatus;
				//print_r($insert_data); die();
				if($last_driver_shift->shiftStatus!=$shiftStatus)
				{
					// insert driver shift history IN data
					$driver_shift_insert_id = $this->Driver_Shift_History_Model->insert ( $insert_data );
				}
			}
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'driver_id' => $driver_id
		);
		echo json_encode ( $response );
	}

	public function changeDriverDutyStatus() {
		$data = array ();

		//$this->addJs ( 'app/driver.js' );

		$data = $this->input->post ();
		$driver_id = $data ['id'];
		$duty_status = $data ['status'];

		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($driver_id) {
			$last_driver_shift = $this->Driver_Shift_History_Model->getOneByKeyValueArray ( array (
											'driverId' => $driver_id
									), 'id DESC' );
			//print_r($last_driver_shift);
			$insert_data = array (
					'driverId' => $driver_id,
					'inLatitude' => '',
					'inLongitude' => '',
					'availabilityStatus' => Driver_Available_Status_Enum::FREE
				);
			if($duty_status=='Y')
			{
				$shiftStatus = Driver_Shift_Status_Enum::SHIFTIN ;
			}
			else
			{
				$shiftStatus = Driver_Shift_Status_Enum::SHIFTOUT ;
			}
			$insert_data['shiftStatus'] = $shiftStatus;
			//print_r($insert_data); die();
			if($last_driver_shift->shiftStatus!=$shiftStatus)
			{
				// insert driver shift history IN data
				$driver_shift_insert_id = $this->Driver_Shift_History_Model->insert ( $insert_data );
			}

			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'driver_id' => $driver_id
		);
		echo json_encode ( $response );
	}

	public function changeDriverPassword()
	{
		$driver_id=$this->input->post('driver_id');
		$this->form_validation->set_rules($this->change_password_validation);
		if ($this->form_validation->run() == TRUE) {
			if($driver_id)
			{
				$driver_updated = $this->Driver_Model->update ( array (
						'password' => hash ( "sha256", $this->input->post('newPassword'))
				), array (
						'id' => $driver_id
				) );
				if($driver_updated)
				{
					$this->session->set_flashdata ('success-msg', 'Driver password changed Successfully' );
				}
				$response['redirect']='driver/getDetailsById/'.$driver_id.'/view';
			}
		} else
		{
			$response['error']=validation_errors();
		}
		echo json_encode ( $response );
	}
	public function deleteDriver() {
		$data = array ();

		$this->addJs ( 'app/driver.js' );

		$data = $this->input->post ();
		$driver_id = $data ['id'];
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['delete_failed'];
		if ($driver_id > 0) {
			$driver_updated = $this->Driver_Model->update ( array (
					'isDeleted' => Status_Type_Enum::ACTIVE
			), array (
					'id' => $driver_id
			) );
			$msg = $msg_data ['delete_success'];
		}
		$response = array (
				'msg' => $msg,
				'driver_id' => $driver_id
		);
		echo json_encode ( $response );
	}
	public function checkDriverEmail() {
		$data = array ();

		$data = $this->input->post ();
		$email = $data ['email'];
		$email_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['email_not_exists'];
		if ($email) {
			$email_exists = $this->Driver_Model->getOneByKeyValueArray ( array (
					'email' => $email
			) );
			if ($email_exists) {
				$status = 1;
				$msg = $msg_data ['email_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $email,
				'status' => $status
		);
		echo json_encode ( $response );
	}
	public function checkDriverMobile() {
		$data = array ();

		$data = $this->input->post ();
		$mobile = $data ['mobile'];
		$mobile_exists = NULL;
		$status = 0;
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['mobile_not_exists'];
		if ($mobile) {
			$mobile_exists = $this->Driver_Model->getOneByKeyValueArray ( array (
					'mobile' => $mobile
			) );
			if ($mobile_exists) {
				$status = 1;
				$msg = $msg_data ['mobile_exists'];
			}
		}
		$response = array (
				'msg' => $msg,
				'email' => $mobile,
				'status' => $status
		);
		echo json_encode ( $response );
	}
	public function driverCurrentMap() {
		$data = array ();

		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'datepicker/jquery.datetimepicker.full.js' );
		$this->addJs ( 'app/driver_map.js' ); // file not found
		$this->addJs ( 'vendors/datatables.net/js/jquery.dataTables.min.js' );
		$this->addJs ( 'vendors/datatables.net-bs/js/dataTables.bootstrap.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/dataTables.buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.print.min.js' );
		$this->addJs ( 'vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js' );
		$this->addJs ( 'vendors/datatables.net-keytable/js/dataTables.keyTable.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/jquery.dataTables.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );

		if (! empty ( $this->uri->segment ( '3' ) )) {
			$filter_cityid = $this->uri->segment ( '3' );
		} else {
			$filter_cityid = $this->session->userdata ( 'user_city' );
		}
		$data ['activetab'] = "driver_map";
		$data ['active_menu'] = 'driver_map';
		$data ['filter_cityid'] = $filter_cityid;
		$data ['diver_mapview'] = TRUE;
		$html = $this->render ( "driver/driver_map", $data );
	}
	public function loadDriverMap() {
		$data = array ();
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'datepicker/jquery.datetimepicker.full.js' );
		$this->addJs ( 'vendors/datatables.net/js/jquery.dataTables.min.js' );
		$this->addJs ( 'vendors/datatables.net-bs/js/dataTables.bootstrap.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/dataTables.buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.print.min.js' );
		$this->addJs ( 'vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js' );
		$this->addJs ( 'vendors/datatables.net-keytable/js/dataTables.keyTable.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/jquery.dataTables.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		/*
		 * if (! empty ( $this->uri->segment ( '3' ) )) {
		 * $filter_cityid = $this->uri->segment ( '3' );
		 * } else {
		 */
		$filter_cityid = $this->session->userdata ( 'user_city' );
		// }
		$data ['filter_cityid'] = $filter_cityid;
		$html = $this->load->view ( "driver/load_driver_map", $data );
	}
	public function getCurrentDriversForMapView() {
		if (! empty ( $this->uri->segment ( '3' ) )) {
			$filter_cityid = $this->uri->segment ( '3' );
		} else {
			$filter_cityid = $this->session->userdata ( 'user_city' );
		}
		$city_id = $filter_cityid;//$this->input->post ( 'city_id' );
		$driver_availablity_status = array ();
		$data ['driver_list'] = $this->Query_Model->getCurrentDriversForMapByCity ( $city_id );
		if ($data ['driver_list']) {
			foreach ( $data ['driver_list'] as $driver ) {
				$driver_availablity_status [] = $driver->availabilityStatus;
			}
		}
		@$availibility_status_count = array_count_values ( $driver_availablity_status );
		$free_count = isset ( $availibility_status_count ['F'] ) ? $availibility_status_count ['F'] : 0;
		$not_available = isset ( $availibility_status_count ['N'] ) ? $availibility_status_count ['N'] : 0;
		$busy_count = isset ( $availibility_status_count ['B'] ) ? $availibility_status_count ['B'] : 0;
		$data ['driver_list'] ['free_count'] = $free_count;
		$data ['driver_list'] ['notavailable_count'] = $not_available;
		$data ['driver_list'] ['busy_count'] = $busy_count;
		echo json_encode ( $data ['driver_list'] );
	}

	public function getCurrentDriversForMapView_Trip() {
		if (! empty ( $this->uri->segment ( '3' ) )) {
			$filter_cityid = $this->uri->segment ( '3' );
		} else {
			$filter_cityid = $this->session->userdata ( 'user_city' );
		}
		$post_data = $this->input->post();
		$pLat = $post_data['tripPLat'];
		$pLon = $post_data['tripPLon'];

		$city_id = $filter_cityid;//$this->input->post ( 'city_id' );
		$driver_availablity_status = array ();
		$data ['driver_list'] = $this->Query_Model->getCurrentDriversForMapByCity ( $city_id, $pLat, $pLon );
		if ($data ['driver_list']) {
			foreach ( $data ['driver_list'] as $driver ) {
				$driver_availablity_status [] = $driver->availabilityStatus;
			}
		}
		@$availibility_status_count = array_count_values ( $driver_availablity_status );
		$free_count = isset ( $availibility_status_count ['F'] ) ? $availibility_status_count ['F'] : 0;
		$not_available = isset ( $availibility_status_count ['N'] ) ? $availibility_status_count ['N'] : 0;
		$busy_count = isset ( $availibility_status_count ['B'] ) ? $availibility_status_count ['B'] : 0;
		$data ['driver_list'] ['free_count'] = $free_count;
		$data ['driver_list'] ['notavailable_count'] = $not_available;
		$data ['driver_list'] ['busy_count'] = $busy_count;
		echo json_encode ( $data ['driver_list'] );
	}

	public function downloadEarningHistory($driver_id)
	{
		DownloadDriverEarningsSummaryExcel('0', $driver_id);
	}

	public function downloadSessionHistory($driver_id)
	{
		DownloadDriverSessionsSummaryExcel('0', $driver_id);
	}

	// This is a testing function based on above function for getting Session related data for Driver
	// Created by Aditya on Wed May 03 2017
	public function downloadSessionHistoryTest()
	{
		$driver_id = 647;
		DownloadDriverSessionsSummaryExcel('0', $driver_id);
	}

	/***Aditya Sample Rough Testing Code START***/
	public function testForm() {
		$this->load->view ( "driver/test_app_form");
	}
	/***Aditya Sample Rough Testing Code END***/
} // end of class
