<?php
class Smtp extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Smtp_Setting_Model' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/smtp.js' );
		
		// $this->render("smtp/add_edit_smtp", $data);
	}
	public function validationSmtp() {
		$this->load->library ( 'form_validation' );
		$config = array (
				array (
						'field' => 'smtpHost',
						'label' => 'SMTP Host',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'smtpUsername',
						'label' => 'SMTP Username',
						'rules' => 'trim|required' 
				),
				array (
						'field' => 'smtpTransportLayerSecurity',
						'label' => 'SMTP Transport Layer Security',
						'rules' => 'trim|required' 
				) 
		);
		
		$this->form_validation->set_rules ( $config );
		if ($this->form_validation->run () == TRUE) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function add() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/smtp.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "smtp";
		$data ['active_menu'] = 'smtp';
		// $data['city_list']=$this->City_Model->getColumnByKeyValueArray('id,name',array('status'=>'Y'),'name');
		// $data['company_list']=$this->Corporate_Partners_Details_Model->getColumnByKeyValueArray('id,name',array('status'=>'Y'),'id');
		$data ['smtp_model'] = $this->Smtp_Setting_Model->getBlankModel ();
		$this->render ( 'smtp/add_edit_smtp', $data );
	}
	public function saveSmtp() {
		$this->addJs ( 'app/smtp.js' );
		$data = array ();
		$data = $this->input->post ();
		$smtp_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($smtp_id > 0) {
			$this->Smtp_Model->update ( $data, array (
					'id' => $smtp_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			$password = $data [email] . "_" . $data ['mobile'];
			// @todo encrypted password
			$data ['password'] = hash ( "sha256", $password );
			// @todo generate 6 digit otp
			$data ['otp'] = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			// @todo unique smtp code
			$data ['passengerCode'] = '';
			
			$smtp_id = $this->Smtp_Setting_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		
		// $this->render("smtp/add_edit_smtp", $data);
		
		$response = array (
				'msg' => $msg,
				'smtp_id' => $smtp_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($smtp_id, $view_mode = EDIT_MODE) {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/smtp.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		if ($this->input->post ( 'id' )) {
			$smtp_id = $this->input->post ( 'id' );
		}
		
		if ($smtp_id > 0) {
			// Get release details by release_id
			$data ['smtp_model'] = $this->Smtp_Setting_Model->getById ( $smtp_id );
		} else {
			// Blank Model
			$data ['smtp_model'] = $this->Smtp_Setting_Model->getBlankModel ();
		}
		$data ['activetab'] = "smtp";
		$data ['active_menu'] = 'smtp';
		if ($this->input->post ( 'id' )) {
			$html = $this->load->view ( 'smtp/add_edit_smtp', $data, TRUE );
			echo json_encode ( array (
					'html' => $html 
			)
			 );
		} else {
			$this->render ( 'smtp/add_edit_smtp', $data );
		}
	}
	public function getSmtpList() {
		if ($this->User_Access_Model->showSettings () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		$this->addJs ( 'app/smtp.js' );
		$this->addJs ( 'app/tabel.js' );
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		$data ['activetab'] = "smtp";
		$data ['active_menu'] = 'smtp';
		$data ['smtp_model_list'] = $this->Smtp_Setting_Model->getAll ( 'id' );
		
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "smtp/manage_smtp", $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$html = $this->render ( "smtp/manage_smtp", $data );
		}
	}
	public function changeSmtpStatus() {
		$data = array ();
		
		$this->addJs ( 'app/smtp.js' );
		
		$data = $this->input->post ();
		$smtp_id = $data ['id'];
		$driver_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($smtp_id) {
			$driver_updated = $this->Smtp_Setting_Model->update ( array (
					'status' => $driver_status 
			), array (
					'id' => $smtp_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'smtp_id' => $smtp_id 
		);
		echo json_encode ( $response );
	}
}