<?php
class Import extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'City_Model' );
		$this->load->model ( 'Auth_Key_Model' );
		$this->load->model ( 'Api_Webservice_Model' );
		$this->load->model ( 'Sms_Template_Model' );
		$this->load->model ( 'Corporate_Partners_Details_Model' );
		$this->load->model ( 'Driver_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Driver_Personal_Details_Model' );
		$this->load->model ( 'Driver_Shift_History_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		//$this->addJs ( 'app/driver.js' );
		// $this->render("driver/add_edit_driver", $data);
	}
	public function passengerImport() {
		$insert_data = array ();
		$count = 0;
		//$import_file = $_FILES ['imported_file'] ['tmp_name'];
		$file_path=image_url('passengers_2.csv');//debug_exit($file_path);
		$file_open = fopen ( $file_path, "r" ); // print_r($file_open);
		$count_no_lines = count ( $file_open );
		while ( ($data = fgetcsv ( $file_open, 21000, "," )) !== FALSE ) {
			//debug_exit($data);
			
			$passenger_code = '';
			$password = $data [4];
			$password = ($password) ? (hash ( "sha256", $password )) : '';
			$otp = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			if (! $data [7]) {
				$passenger_code = trim ( substr ( strtoupper ( str_replace ( ' ', '', $data [1] . '' . $data [2] ) ), 0, 5 ) );
				$passenger_code .= $this->Passenger_Model->getAutoIncrementValue ();
			} else {
				$passenger_code = $data [7];
			}
			
			$insert_data= array (
					'oldId' => $data [0],
					'firstName' => $data [1],
					'lastName' => $data [2],
					'email' => $data [3],
					'mobile' => $data [6],
					'password' => $password,
					'otp' => $data [5],
					'deviceId' => $data [9],
					'deviceType' => ($data [10]==2)?Device_Type_Enum::IPHONE:Device_Type_Enum::ANDROID,
					'isEmailVerified' => Status_Type_Enum::INACTIVE,
					'signupFrom' => Signup_Type_Enum::MOBILE,
					'registerType' => Register_Type_Enum::MANUAL,
					'loginStatus' => Status_Type_Enum::ACTIVE,
					'cityId' => DEFAULT_CITY_ID,
					'companyId' => DEFAULT_COMPANY_ID,
					'walletAmount' => $data [8],
					'passengerCode' => $passenger_code,
					'status' => ($data [12]=='A')?Status_Type_Enum::ACTIVE:Status_Type_Enum::INACTIVE,
					'activationStatus' => ($data [11]==1)?Status_Type_Enum::ACTIVE:Status_Type_Enum::INACTIVE,
					'sourceLead' => 'WEB',
					'createdBy' => 0 
			);
			//debug($insert_data);
			$passenger_details = $this->Passenger_Model->insert ( $insert_data );
			//$count ++;
			//debug($count);
			/* if ($count == 100) {
				//debug_exit($insert_data);
				$passenger_details = $this->Passenger_Model->batch_insert ( $insert_data );
				$count = 0;
			} */
		}
		//debug('completed import');
		//echo json_encode ( $result_array );
	}
	
	public function driverImport() {
		$file_path=image_url('people.csv');//debug_exit($file_path);
		$file_open = fopen ( $file_path, "r" ); 
		$count_no_lines = count ( $file_open );
		while ( ($data = fgetcsv ( $file_open, 1000, "," )) !== FALSE ) {
			$city_id = 1;
			$driver_code = '';
			$password= $data [5];
			$password = ($password) ? (hash ( "sha256", $password )) : '';
			$otp = str_pad ( rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
			
			$driver_code = trim ( substr ( strtoupper ( str_replace ( ' ', '', $data [1] . '' . $data [2] ) ), 0, 5 ) );
			$driver_code .= $this->Driver_Model->getAutoIncrementValue();
			//debug_exit($driver_code1);
			if($data [8]==2)
			{
				$city_id=1;
			}
			else if($data [8]==33)
			{
				$city_id=2;
			}
			else if($data [8]==34)
			{
				$city_id=3;
			}
				
			$insert_driver_data = array (
					'oldId' => $data [0],
					'firstName' => $data [1],
					'lastName' => $data [2],
					'email' => $data [3],
					'mobile' => $data [9],
					'password' => $password,
					'deviceId' => $data [6],
					'deviceType' => ($data [7]==2)?Device_Type_Enum::IPHONE:Device_Type_Enum::ANDROID,
					'isVerified' => Status_Type_Enum::ACTIVE,
					'loginStatus' => Status_Type_Enum::INACTIVE,
					'cityId' => $city_id,
					'companyId' => DEFAULT_COMPANY_ID,
					'driverCode' => $driver_code,
					'skills'=>Transmission_Type_Enum::MANUAL,
					'driverSkill' => Driver_Skill_Enum::NONLUXURY,
					'favouriteCar'=>'',
					'driverExperience' => $data [15],
					'driverLanguagesKnown' => $data [14],
					'driverType' => Driver_Type_Enum::COMMISSIONBASED,
					'fixedTripEarning'=>'',
					'commissionTripEarning'=>20,
					'currentLatitude' => '',
					'currentLongitude'=>'',
					'status'=>($data [12]=='A')?Status_Type_Enum::ACTIVE:Status_Type_Enum::INACTIVE,
					'isDeleted'=>Status_Type_Enum::INACTIVE
			);
			
				$driver_details = $this->Driver_Model->insert ( $insert_driver_data );
			
				$insert_driver_personal_data = array (
						'driverId' => $driver_details,
						'address' => $data [4],
						'dob' => ($data [11])?date('Y-m-d',strtotime($data [11])):'',
						'gender' => ($data [10]=='Male')?Gender_Type_Enum::MALE:Gender_Type_Enum::FEMALE,
						'driverQualification' => ($data [16])?$data [16]:'',
						'drivingLicenseNo' => ($data [13])?$data [13]:''
				);
				
				$driver_personal_details = $this->Driver_Personal_Details_Model->insert ( $insert_driver_personal_data );
				
				$driver_shift_data=array('driverId'=>$driver_details,'availabilityStatus'=>Driver_Available_Status_Enum::FREE,'shiftStatus'=>Driver_Shift_Status_Enum::SHIFTOUT);
				$driver_shift_history = $this->Driver_Shift_History_Model->insert($driver_shift_data);
				//debug_exit($insert_driver_personal_data);
		}
		debug('import completed');
	}
}