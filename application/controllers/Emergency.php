<?php
class Emergency extends My_Controller {
	function __construct() {
		parent::__construct ();
		is_logged_in ();
		$this->load->helper ( 'form' );
		$this->load->model ( 'Emergency_Request_Details_Model' );
		$this->load->model ( 'Passenger_Emergency_Contact_Model' );
		$this->load->model ( 'Passenger_Model' );
		$this->load->model ( 'Query_Model' );
		$this->load->model ( 'Data_Attributes_Model' );
	}
	public function index() {
		// $data = array();
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$this->addJs ( 'app/emergency.js' );
		
		// $this->render("passenger/add_edit_passenger", $data);
	}
	public function add() {
		if ($this->User_Access_Model->showEmergency () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$this->addJs ( 'app/emergency.js' );
		
		$data = array (
				'mode' => EDIT_MODE 
		);
		$data ['activetab'] = "emergency";
		$data ['active_menu'] = 'emergency';
		$data ['emergency_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'EMERGENCY_STATUS' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		
		$data ['emergency_model'] = $this->Emergency_Request_Details_Model->getBlankModel ();
		$data ['passenger_emergency_model'] = array ();
		$this->render ( 'emergency/add_edit_emergency', $data );
	}
	public function saveEmergency() {
		$this->addJs ( 'app/emergency.js' );
		$data = array ();
		$data = $this->input->post ();
		$emergency_id = $data ['id'];
		// $data['activetab']="admin";
		// $data['active_menu'] = 'admin';
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['failed'];
		if ($emergency_id > 0) {
			$this->Emergency_Request_Details_Model->update ( $data, array (
					'id' => $emergency_id 
			) );
			$msg = $msg_data ['updated'];
		} else {
			
			$emergency_id = $this->Emergency_Request_Details_Model->insert ( $data );
			$msg = $msg_data ['success'];
		}
		
		// $this->render("passenger/add_edit_passenger", $data);
		
		$response = array (
				'msg' => $msg,
				'emergency_id' => $emergency_id 
		);
		echo json_encode ( $response );
	}
	public function getDetailsById($emergency_id, $view_mode) {
		if ($this->User_Access_Model->showEmergency () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		
		$this->addJs ( 'app/emergency.js' );
		
		$data = array (
				'mode' => $view_mode 
		);
		if ($this->input->post ( 'id' )) {
			$emergency_id = $this->input->post ( 'id' );
		}
		$data ['activetab'] = "emergency";
		$data ['active_menu'] = 'emergency';
		$data ['emergency_status_list'] = $this->Data_Attributes_Model->getSelectDropdownOptions ( array (
				'tableName' => strtoupper ( 'EMERGENCY_STATUS' ),
				'isVisible' => 'Y' 
		), 'sequenceOrder' );
		
		if ($emergency_id > 0) {
			// Get release details by release_id
			$data ['emergency_model'] = $this->Emergency_Request_Details_Model->getById ( $emergency_id );
			$data ['passenger_model'] = $this->Passenger_Model->getById ( $data ['emergency_model']->passengerId );
			$data ['passenger_emergency_contacts'] = $this->Passenger_Emergency_Contact_Model->getByKeyValueArray ( array (
					'passengerId' => $data ['emergency_model']->passengerId 
			), 'id' );
		} else {
			// Blank Model
			$data ['emergency_model'] = $this->Emergency_Request_Details_Model->getBlankModel ();
			$data ['passenger_model'] = $this->Passenger_Model->getBlankModel ();
			$data ['passenger_emergency_contacts'] = array ();
		}
		if ($this->input->post ( 'id' )) {
			$html = $this->load->view ( 'emergency/add_edit_emergency', $data, TRUE );
			
			echo json_encode ( array (
					'html' => $html 
			)
			 );
		} else {
			$this->render ( 'emergency/add_edit_emergency', $data );
		}
	}
	public function getEmergencyList() {
		if ($this->User_Access_Model->showEmergency () === FALSE) {
			// redirect 404
			show_error ( "You do not have permission to view this page", '404' );
			return;
		}
		$data = array ();
		// get type whether to load or render the ouptput
		$get_type = $this->input->post ( 'get_type' );
		$this->addJs ( 'app/emergency.js' );
		$this->addJs ( 'app/tabel.js' );
		
		$this->addJs ( 'vendors/datatables.net/js/datatables.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.min.js' );
		$this->addJs ( 'vendors/datatables.net-buttons/js/buttons.flash.min.js' );
		// $this->addJs ('vendors/datatables.net/js/datatables_extension_buttons_init.js');
		$this->addJs ( 'vendors/jszip/dist/jszip.min.js' );
		$this->addJs ( 'vendors/pdfmake/build/pdfmake.min.js' );
		$this->addJs ( 'vendors/datatables.net/js/vfs_fonts.js' );
		$this->addJs ( 'vendors/datatables.net/js/buttons.html5.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive/js/dataTables.responsive.min.js' );
		$this->addJs ( 'vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js' );
		$this->addJs ( 'vendors/datatables.net-scroller/js/datatables.scroller.min.js' );
		
		// $this->addJs('vendors/jszip/dist/jszip.min.js');
		// $this->addJs('vendors/pdfmake/build/pdfmake.min.js');
		// $this->addJs('vendors/pdfmake/build/vfs_fonts.js');
		
		$data ['activetab'] = "emergency";
		$data ['active_menu'] = 'emergency';
		
		$city_id = $this->session->userdata ( 'user_city' );
		$post_data = $this->input->post ();
		if (array_key_exists ( 'city_id', $post_data )) {
			$city_id = $post_data ['city_id'];
		}
		$data ['emergency_model_list'] = $this->Query_Model->getEmergencyList ( $city_id );
		if ($get_type == 'ajax_call') {
			$html = $this->load->view ( "emergency/manage_emergency", $data );
			echo json_encode ( array (
					'html' => $html 
			) );
		} else {
			$html = $this->render ( "emergency/manage_emergency", $data );
		}
	}
	public function changeEmergencyStatus() {
		$data = array ();
		
		$this->addJs ( 'app/emergency.js' );
		
		$data = $this->input->post ();
		$emergency_id = $data ['id'];
		$driver_status = $data ['status'];
		
		$msg_data = $this->config->item ( 'msg' );
		$msg = $msg_data ['status_change_failed'];
		if ($emergency_id) {
			$driver_updated = $this->Emergency_Request_Details_Model->update ( array (
					'status' => $driver_status 
			), array (
					'id' => $emergency_id 
			) );
			$msg = $msg_data ['status_change_success'];
		}
		$response = array (
				'msg' => $msg,
				'emergency_id' => $emergency_id 
		);
		echo json_encode ( $response );
	}
}