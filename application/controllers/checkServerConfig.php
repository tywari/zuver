<?php

/**
 *
 * Enter description here ...
 * @author mukuldas
 *
 */

class checkServerConfig extends My_Controller {


	public function __construct() {
		/* Default Constructor */
		parent::__construct();

	}
	public function index() {
		print('<style type="text/css"> .good { color: green; } .bad {color:red;} </style>');
		print('<pre>');
		print("\n\n Server PHP Version\t");
		print(PHP_VERSION_ID >= 50427 ? '<span class="good">'.phpversion().'</span>' : '<span class="bad">'.phpversion().'</span>');

		print("\n\n Is Apc Enabled ?\t");
		print(extension_loaded('apc') ? '<span class="good">Yes!</span>' : '<span class="bad">No!</span>');
		print("\n\n Is ldap Enabled ?\t");
		print(function_exists('ldap_connect') ? '<span class="good">Yes!</span>' : '<span class="bad">No!</span>');
		print("\n\n Is mbstring Enabled ?\t");
		print(extension_loaded('mbstring') ? '<span class="good">Yes!</span>' : '<span class="bad">No!</span>');
		print("\n\n Is openssl Enabled ?\t");
		print(extension_loaded('openssl') ? '<span class="good">Yes!</span>' : '<span class="bad">No!</span>');

		print("\n\n Is crontab Enabled ?\t");
		print($this->cronCheck()  ? '<span class="good">Yes!</span>' : '<span class="bad">No!</span>');

		print('</pre>');
	}

	private function cronCheck() {
		$output = array();
		$result = exec('crontab -l', $output);
		print_r($result);		
		return $result;
		// return preg_match('|^0 0 \* \* \* /opt/local/bin/php54 /opt/local/www/mozaic.apple.com/public/index.php emailer auto$|', $result) > 0;
	}
}