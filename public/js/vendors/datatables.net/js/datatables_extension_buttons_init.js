/* ------------------------------------------------------------------------------
*
*  # Buttons extension for Datatables. Init datatable-checkboxs
*
*  Specific JS code additions for datatable_extension_buttons_init.html page
*
*  Version: 1.0
*  Latest update: Nov 9, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });


    var buttonCommon = {
        exportOptions: {
            format: {
                body: function ( data, column, row, node ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
                        data.replace( /[$,]/g, '' ) :
                        data;
                }
            }
        }
    };

   
    // Buttons collection
    $('.datatable-button-init-collection').DataTable({
        dom: 'Blfrtip',
        pagingType: "full_numbers" ,
        order : [ [ 1, 'asc' ] ],
		columnDefs : [ {
			orderable : false,
			targets : [ 0 ]
		} ],
        buttons: [
            {
                extend: 'collection',
                text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                className: 'btn btn-default',
                buttons: [
                {extend: 'copy',text:'<i class="icon-copy"></i> COPY'},
                {extend: 'csv' ,text:'<i class="icon-file-excel"></i> CSV'},
                {extend: 'excel',text:'<i class="icon-file-excel"></i> EXCEL'},
                {extend: 'pdf',text:'<i class="icon-file-pdf"></i> PDF'},
                {extend: 'print',text:'<i class="icon-printer"></i> PRINT'}
                ]
            }
        ],
        responsive: true
    });


    // Page length
    $('.datatable-button-init-length').DataTable({
        dom: '<"datatable-header"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
            {
                extend: 'pageLength',
                className: 'btn bg-slate-600'
            }
        ]
    });


    
        // Setup - add a text input to each footer cell
    $('#datatable-checkbox thead tr#filterrow th').each( function () {
        var title = $('#datatable-checkbox thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" class="form-control input-sm " onclick="stopPropagation(event);" placeholder="Search '+title+'" />' );
    } );
  $('th input').click(function(event) {
    event.stopPropagation();
});
    // DataTable
    var table = $('#datatable-checkbox').DataTable();
     
    // Apply the filter
    $("#datatable-checkbox thead input").on( 'keyup change', function () {
        table
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    } );

  function stopPropagation(evt) {
        if (evt.stopPropagation !== undefined) {
            evt.stopPropagation();
        } else {
            evt.cancelBubble = true;
        }
    }

 // Individual column searching with selects


    // External table additions
    // ------------------------------

    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');


  /*  // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });*/
    
});
