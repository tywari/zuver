jQuery(document).ready(function(){
	bankDependency();
});
function bankDependency()
{
	//loadBankList();
	 $('#save_bank_btn').click(function (event){
		 //saveCity(); 
	});
       //load Country list for back button
       $('body').on('click','#load_bank_btn',function(){
          /* loadBankList();
           $("html, body").animate({ scrollTop: 0 }, "slow");*/
           window.location.href = BASE_URL + '/bank/getBankList/';
      }); 
	
}
function viewBank(id){
    var arr = id.split('-');
    var bank_id = arr[1];
    loadBank(bank_id,'view');
}
function editBank(id){
   var arr = id.split('-');
   var bank_id = arr[1];
   loadBank(bank_id,'edit'); 
}

function changeBankStatus(id)
{
	var bank_id=0;
	var status='';
	
	var arr = id.split('-');
    city_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/bank/changeBankStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':bank_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadBankList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function loadBankList()
{
    // Validate the city value again in case the user changed the input
    var url = BASE_URL + '/bank/getBankList/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call'},
    }).done(function (response) {
        
        jQuery('#bank-details-information').replaceWith(response);
        dataTableLoader();
        window.location.href = BASE_URL + '/bank/getBankList/';
    });
}

function loadBank( bank_id , view_mode)
{
    var url = BASE_URL + '/bank/getDetailsById/' + bank_id + '/' + view_mode;
    window.location.href =BASE_URL + '/bank/getDetailsById/' + bank_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: bank_id}
    }).done(function (response) {
        
        jQuery('#bank-details-information').replaceWith(response.html);
        bankDependency();
    });
}

function saveBank() {
    var isValidate=false;
 // Validate the city details again in case the user changed the input
    var url = BASE_URL + '/bank/saveBank/';
    
    var bankData = {};
    
    /*jQuery( jQuery('#edit_bank_form :input').serializeArray() ).each(function( x , y ){
    	bankData[y.name] = y.value;
    });*/
    isValidate=validateBank();
    if(isValidate)
    {
    	 var bankData = new window.FormData($('#edit_bank_form')[0]);
    	jQuery.ajax({
            xhr: function () {  
                return $.ajaxSettings.xhr();
            },
            type: "POST",
            data: bankData,
            cache: false,
            contentType: false,
            processData: false,
            url: url,
        }).done(function (response) {
        	response = jQuery.parseJSON(response);
        if(response.bank_id)
        {
        	
        	successMessage( response.msg);
        	window.location.href = BASE_URL + '/bank/getBankList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateBank( section )
{
    var bank_form = jQuery('form#edit_bank_form')[0];
    var is_bank_form_valid = bank_form.checkValidity();

    if( is_bank_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_bank_form_valid==true);

}
