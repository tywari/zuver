jQuery(document).ready(function(){
	walletDependency();
});
        
  
  function walletDependency()
  {
	//loadPromocodeList();
		
		 $('#save_wallet_btn').click(function (event){
			// savePromocode(); 
	 	});
	    
	    //load Wallet list for back button
	        $('body').on('click','#load_wallet_btn',function(){
	            /*loadWalletList();
	            $("html, body").animate({ scrollTop: 0 }, "slow");*/
	            window.location.href = BASE_URL + '/wallet/getWalletList/';
	       });
            //load passenger list for back button
                $('body').on('click','#load_passenger_btn',function(){
                         window.location.href = BASE_URL + '/passenger/getPassengerList/';
               });   
               
	     // calculate Transaction Amount
	        $('#transactionAmount').blur(function(){
	            calculateWallet();
	        });
	        
	        $(document).on('change', '#manipulationType', function() {
	          calculateWallet();
	        });
	        
	   // auto complete code
	   $("#passengerName").keyup(function(){
	       var keyword = $(this).val();
	       var url = BASE_URL + '/wallet/getPassengerInfo/';
	        if(keyword.length >= 3) {
			$.ajax({ 
			type: "POST",
			url: url,
			data:'keyword='+$(this).val(),
			beforeSend: function(){
				$("#passengerId").css("background","#FFF url(../public/images/loader.gif) no-repeat 485px");
			},
			success: function(data){
	                         $("#suggesstion-box").show();
	                         $("#suggesstion-box").html(data);
	                         $("#passengerId").css("background","#FFF");
	                    }
			});
	         }
	         if(keyword==''){
	             $("#passengerName").val('');
	             $("#passengerId").val('');
	             $("#previousAmount").val('');
	         }
	   });
  }
  function calculateWallet(){
      var manipulation_type  = $('#manipulationType').val();
      var transcation_amount = parseFloat($('#transactionAmount').val());
      var previuos_amount    = parseFloat($('#previousAmount').val());
      var manipulation_type  = $('#manipulationType').val();
      var wallet_amount = '';
    if(transcation_amount > 0) { 
        if(manipulation_type =='A'){
            var wallet_amount = previuos_amount + transcation_amount;
        }
        if(manipulation_type =='S'){
            if(transcation_amount > previuos_amount ){
                  alert('Invalid Transcation Amount');
                  $('#transactionAmount').val('');
                  $('#currentAmount').val('');
                  transcation_amount = 0.00;
              }
          var wallet_amount = previuos_amount - transcation_amount;
        }
        $('#currentAmount').val(wallet_amount);
    }
 }  
    


function selectPassenger(passengerId,passengerName,previousAmount) {
    if(passengerId > 0){
        $("#passengerName").val(passengerName);
        $("#passengerId").val(passengerId);
        $("#previousAmount").val(previousAmount);
    }
    $("#suggesstion-box").hide();
}
function viewPromoCode(id){
    var arr = id.split('-');
    var promocode_id = arr[1];
    loadPromocode(promocode_id,'view');
}
function editPromoCode(id){
   var arr = id.split('-');
   var promocode_id = arr[1];
   loadPromocode(promocode_id,'edit'); 
}

function loadWalletList()
{
    // Validate the promocode value again in case the user changed the input
    var url = BASE_URL + '/wallet/getWalletList/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call'},
    }).done(function (response) {
        jQuery('#wallet-details-information').replaceWith(response);
        
    });
}

function loadWallet( wallet_id , view_mode)
{
    var url = BASE_URL + '/wallet/getDetailsById/' + wallet_id + '/' + view_mode;
    window.location.href =BASE_URL + '/wallet/getDetailsById/' + wallet_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: wallet_id}
    }).done(function (response) {
        jQuery('#wallet-details-information').replaceWith(response.html);
        walletDependency();
    });
}

function saveWallet() {
    var isValidate=false;
 // Validate the promocode details again in case the user changed the input
    var url = BASE_URL + '/wallet/saveWallet/';
    
    var walletData = {};
    
    /*jQuery( jQuery('#edit_promocode_form :input').serializeArray() ).each(function( x , y ){
    	promocodeData[y.name] = y.value;
    });*/
    var walletData = new window.FormData($('#edit_wallet_form')[0]);
    isValidate=validateWallet();
    if(isValidate)
    {
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: walletData,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
        var response = $.parseJSON(response);
        if(response.status == 1)
        {
        	//loadWalletList();
                var passenger_id  = response.passenger_id;
        	successMessage( response.msg);
        	 window.location.href = BASE_URL + '/wallet/getWalletList/'+passenger_id;
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateWallet( section )
{
    var wallet_form = jQuery('form#edit_wallet_form')[0];
    var is_wallet_form_valid = wallet_form.checkValidity();

    if( is_wallet_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_wallet_form_valid==true);

}
