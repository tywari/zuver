jQuery(document).ready(function(){
	//loadSmtpList();
	
	 $('#save_smtp_btn').click(function (event){
	    	
		 saveSmtp(); 
 	
 	});
	 $('body').on('click','#cancel_smtp_btn',function(){
         /*loadCountryList();
         $("html, body").animate({ scrollTop: 0 }, "slow");*/
         window.location.href = BASE_URL + '/dashboard/';
    });   
	
});
function viewSMTP(id){
    var arr = id.split('-');
    var smtp_id = arr[1];
    loadSmtp(smtp_id,'view');
}
function editSMTP(id){
   var arr = id.split('-');
   var smtp_id = arr[1];
   loadSmtp(smtp_id,'edit'); 
}
function changeSmtpStatus(id)
{
	var smtp_id=0;
	var status='';
	
	var arr = id.split('-');
    smtp_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/smtp/changeSmtpStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':smtp_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadSmtpList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function loadSmtpList()
{
    // Validate the smtp value again in case the user changed the input
    var url = BASE_URL + '/smtp/getSmtpList/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json'
    }).done(function (response) {
        
        jQuery('#smtp-details-information').replaceWith(response.html);
        dataTableLoader();
    });
}

function loadSmtp( smtp_id , view_mode)
{
    var url = BASE_URL + '/smtp/getDetailsById/' + smtp_id + '/' + view_mode;
    window.location.href =BASE_URL + '/smtp/getDetailsById/' + smtp_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: smtp_id}
    }).done(function (response) {
        
        jQuery('#smtp-details-information').replaceWith(response.html);
        
    });
}

function saveSmtp() {
    var isValidate=false;
 // Validate the smtp details again in case the user changed the input
    var url = BASE_URL + '/smtp/saveSmtp/';
    
    var smtpData = {};
    
    jQuery( jQuery('#edit_smtp_form :input').serializeArray() ).each(function( x , y ){
    	smtpData[y.name] = y.value;
    });
    isValidate=validateSmtp();
    if(isValidate)
    {
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: smtpData
    }).done(function (response) {
        if(response.smtp_id)
        {
        	loadSmtpList();
        	successMessage( response.msg);
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateSmtp( section )
{
    var smtp_form = jQuery('form#edit_smtp_form')[0];
    var is_smtp_form_valid = smtp_form.checkValidity();

    if( is_smtp_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_smtp_form_valid==true);

}
