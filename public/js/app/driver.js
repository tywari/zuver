jQuery(document).ready(function(){
	driverDependency();

    //$("#change_driver_password_form").submit(function () {
    $(document).delegate('#changeDriverPasswordStatus', 'click', function(event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: BASE_URL +"/Driver/changeDriverPassword",
            data: $('#change_driver_password_form').serialize(),
            success: function (response) {
                response=jQuery.parseJSON(response);
                console.log(response);
                console.log(response.redirect);
                if(response.redirect)
                {
                    window.location.href = BASE_URL +'/'+ response.redirect;
                }
                if(response.error)
                {
                    $('#change_driver_password_form').before('<div class="alert alert-danger fade-in">'+response.error+'</div>');
                      window.setTimeout(function() {
                        $(".alert").fadeTo(5000, 0).slideUp(500, function(){
                             $(this).remove();
                    }); });
                }
            },
            error: function () {
                alert("failure");
            }
        });
    });
});

$(document).delegate('.loginStatus', 'click', function(event) {
    if (event.target.type !== 'radio') {
      $(':radio', this).trigger('click');
    }
    changeDriverLoginStatus();
});

$(document).delegate('.isVerified', 'click', function(event) {
    if (event.target.type !== 'radio') {
      $(':radio', this).trigger('click');
    }
    changeDriverVerificationStatus();
});

function driverDependency()
{
	//loadDriverList();
	 $('#save_driver_btn').click(function (event){
	    //saveDriver();
         });
	 driverTypeControll();
   //date time picker script
   $('#dob').datetimepicker({
       dayOfWeekStart : 1,
       lang           : 'en',
       startDate      : '0',
       timepicker     : false,
       closeOnDateSelect : true,
       format         : 'Y-m-d',
       scrollMonth : false,
       scrollInput : false,
       maxDate:0
   });

   $('#drivingLicenseExpireDate').datetimepicker({
       dayOfWeekStart : 1,
       lang           : 'en',
       startDate      : '',
       timepicker     : false,
       closeOnDateSelect : true,
       scrollMonth : false,
       scrollInput : false,
       format         : 'Y-m-d',
       minDate:0
   });

   $('#driver-details-information').on('change','#driverType',function(){
   driverTypeControll();

   });
   $('#driver-details-information').on('change','.isVerified',function(){
	   changeDriverVerificationStatus();

  });
   $('#driver-details-information').on('change','.loginStatus',function(){
	   changeDriverLoginStatus();

  });

   //load driver list for back button
   $('body').on('click','#load_driver_btn',function(){
       /*loadDriverList();
       $("html, body").animate({ scrollTop: 0 }, "slow");*/
       window.location.href = BASE_URL + '/driver/getDriverList/';
  });
   $(document).on('change', '#filter_cityId', function() {
   	loadDriverList();
    });
   $("#email").blur(function(){
   	checkDriverEmail();
     });
   $("#mobile").blur(function(){
   	checkDriverMobile();
     });
}
function driverTypeControll()
{
	var driver_type=$('#driverType').val();
	$('#fixedTripEarning').addClass('hidden');
	$('#commissionTripEarning').addClass('hidden');
	$('#fixedTripEarning').prop('required', false);
	$('#commissionTripEarning').prop('required', false);
	$('#productivityAllowance').addClass('hidden');
    $('#productivityAllowance').prop('required', false);
	if(driver_type == 'FD')
	{

		$('#fixedTripEarning').removeClass('hidden');
		$('#fixedTripEarning').prop('required', true);

	}
	else if(driver_type == 'CM')
	{

		$('#commissionTripEarning').removeClass('hidden');
		$('#commissionTripEarning').prop('required', true);

	}
    else if(driver_type == 'TBD')
    {

        $('#productivityAllowance').removeClass('hidden');
        $('#productivityAllowance').prop('required', true);

    }

}
function viewDriver(id){
    var arr        = id.split('-');
    var driverid   = arr[1];
    loadDriver(driverid,'view');
}
function editDriver(id){
   var arr         = id.split('-');
   var driverid    = arr[1];
   loadDriver(driverid,'edit');
}
function checkDriverEmail()
{
	var email=$('#email').val();
	if(email != '')
	{
    var url = BASE_URL + '/driver/checkDriverEmail/';
    jQuery.ajax({
        method: "POST",
        data:{'email':email},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#email').val('');
    	}
    	infoMessage(response.msg);
    });
	}
}
function checkDriverMobile()
{
	var mobile=$('#mobile').val();
	if(mobile != '')
	{
    var url = BASE_URL + '/driver/checkDriverMobile/';
    jQuery.ajax({
        method: "POST",
        data:{'mobile':mobile},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#mobile').val('');
    	}
    	infoMessage(response.msg);
    });
	}
}

function deleteDriver()
{
	var driver_id=$('#driver_id').val();
	var driver_first_name=$('#firstName').val();
	var driver_last_name=$('#lastName').val();
    var url = BASE_URL + '/driver/deleteDriver/';
    if (confirm('Are you sure? Want to delete driver '+ driver_first_name +' '+driver_last_name)) {
    jQuery.ajax({
        method: "POST",
        data:{'id':driver_id},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
    	{
    		successMessage( response.msg);
        	loadDriverList();
    	}
    	else
    	{
    		failureMessage( response.msg);
    	}
    	infoMessage(response.msg);
    }).fail(function (jqXHR, textStatus, errorThrown){

    	failureMessage( 'Status change failed.. Please try again later.');

    });
    }
}

function changeDriverStatus(id)
{
	var driver_id=0;
	var status='';

	var arr = id.split('-');
    driver_id = arr[1];

    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/driver/changeDriverStatus/';

    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':driver_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {

        	successMessage( response.msg);
        	loadDriverList();
        }
        else
        {
        	failureMessage( response.msg);
        }

    }).fail(function (jqXHR, textStatus, errorThrown){

    	failureMessage( 'Status change failed.. Please try again later.');

    });

}

function changeDriverVerificationStatus(id)
{
	var driver_id=$('#driver_id').val();
	var status='';

	var driverVerify=$('input[name=isVerified]:checked', '#edit_driver_form').val();

	if(driverVerify==1)
    {
    	status='Y';
    }
    else if(driverVerify==0)
    {
    	status='N';
    }
    var url = BASE_URL + '/driver/changeDriverVerificationStatus/';

    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':driver_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	successMessage( response.msg);
        	loadDriverList();
        	driverDependency();
        }
        else
        {
        	failureMessage( response.msg);

        }

    }).fail(function (jqXHR, textStatus, errorThrown){

    	failureMessage( 'Status change failed.. Please try again later.');


    });

}

// created by Aditya on May 23 2017
// similar to the code written above, above code is original
function changeDriverVerificationStatus2(id, inVerify)
{
	var driver_id = id;//$('#driver_id').val();
	var status='';

	if ((inVerify != 1) && (inVerify != 0)) {
		return false;
	}

	var driverVerify = inVerify;//$('input[name=isVerified]:checked', '#edit_driver_form').val();

	if(driverVerify==1)
    {
    	status='Y';
    }
    else if(driverVerify==0)
    {
    	status='N';
    }
    var url = BASE_URL + '/driver/changeDriverVerificationStatus/';

    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':driver_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	successMessage( response.msg);
        	loadDriverList();
        	//driverDependency();
        }
        else
        {
        	failureMessage( response.msg);

        }

    }).fail(function (jqXHR, textStatus, errorThrown){

    	failureMessage( 'Status change failed.. Please try again later.');


    });

}


/*
    Updated By Aditya Sharma
    on 24th Apr 2017
*/
$(document).delegate('.loginStatus', 'click', function(event) {
    /*if (event.target.type !== 'radio') {
      $(':radio', this).trigger('click');
    }*/
    changeDriverLoginStatus();
});
/*
    Updated By Aditya Sharma
    on 24th Apr 2017
    To bring back Login/Logout Control to Driver Details page and to also fix the Login/Logout status update / button switch issue
    Earlier Login/Logout action would happen in an irratic manner making the switch on 2nd click, then making random switches on following clicks
*/
function changeDriverLoginStatus()
{
    var driver_id=$('#driver_id').val();
    var status='Y';
    var driverLogin = 1;
    var chkd = $('#loginStatus-Y').is(':checked');
    if (chkd) {
        driverLogin = 0;
        status = 'N';
    }

    var url = BASE_URL + '/driver/changeDriverLoginStatus';

    jQuery.ajax({
        method: "POST",
        url: url,
        async: false,
         data:{'id':driver_id,'status':status},
    }).done(function (response) {
        response=jQuery.parseJSON(response);
        if(response)
        {

            successMessage( response.msg);
            //loadDriverList();
            //driverDependency();
            window.setTimeout(reloadPage, 500);
        }
        else
        {
            failureMessage( response.msg);
        }

    }).fail(function (jqXHR, textStatus, errorThrown){

        failureMessage( 'Login Status change failed.. Please try again later.');

    });

}

/***Aditya Code Start***/
$(document).delegate('.dutyStatus', 'click', function(event) {
    /*if (event.target.type !== 'radio') {
      $(':radio', this).trigger('click');
    }*/
    changeDriverDutyStatus();
});

function changeDriverDutyStatus()
{
    //alert('Duty Changed')
    var driver_id=$('#driver_id').val();
    var status='Y';
    var driverDuty = 1;
    var chkd = $('#dutyStatus-Y').is(':checked');
    if (chkd) {
        driverDuty = 0;
        status = 'N';
    }
    //var driverDuty=$('input[name=dutyStatus]:checked').val();
    //alert(driverDuty);

    var url = BASE_URL + '/driver/changeDriverDutyStatus';

    jQuery.ajax({
        method: "POST",
        url: url,
        async: false,
         data:{'id':driver_id,'status':status},
    }).done(function (response) {
        response=jQuery.parseJSON(response);
        if(response)
        {

            successMessage( response.msg);
            window.setTimeout(reloadPage, 500);
        }
        else
        {
            failureMessage( response.msg);
        }

    }).fail(function (jqXHR, textStatus, errorThrown){

        failureMessage( 'Status change failed.. Please try again later.');

    });
}

function reloadPage()
{
    location.reload();
}
/***Aditya Code End***/

function loadDriverList()
{
    // Validate the project bhu value again in case the user changed the input
    var url = BASE_URL + '/driver/getDriverList/';
    var city_id = $('#filter_cityId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call','city_id':city_id}
    }).done(function (response) {

        jQuery('#driver-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadDriver( driver_id , view_mode)
{
    // Validate the project bhu value again in case the user changed the input
    var url = BASE_URL + '/driver/getDetailsById/' + driver_id + '/' + view_mode;
    window.location.href =BASE_URL + '/driver/getDetailsById/' + driver_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        //dataType: 'JSON',
        data: {id: driver_id}
    }).done(function (response) {
        jQuery('#driver-details-information').replaceWith(response);
        driverDependency();
    });
}

// created by Aditya to catch Driver Skill
$('#driverSkill').on('change',function() {
  alert($(this).val());
  console.log($(this).val());
});

function saveDriver() {
    //var isValidate=false;
  //var $this = jQuery(this);


 // Validate the passenger details again in case the user changed the input
    var url = BASE_URL + '/driver/saveDriver/';

    var driverData = {};
    isValidate=validateDriver();
    if(isValidate== true){
        /*jQuery( jQuery('#edit_driver_form :input').serializeArray() ).each(function( x , y ){
            driverData[y.name] = y.value;
        });*/

		var skillArray = $('#driverSkill2').val();
		skillString = skillArray.toString();
		var finalString = skillString.replace(/,/g , ";");
		$('#driverSkill').val(finalString);

		var workingDaysArray = $('#driverWorkDays2').val();
		workingDaysString = workingDaysArray.toString();
		$('#working_days').val(workingDaysString);

	$('#save_driver_btn').button('loading');
	//return false;
    var driverData = new window.FormData($('#edit_driver_form')[0]);


	//alert(driverData);
	//return false;
    jQuery.ajax({
        xhr: function () {
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: driverData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
        response = jQuery.parseJSON(response);
            if(response)
            {
                   // loadDriverList();
                    successMessage(response.msg);
                    window.location.href = BASE_URL + '/driver/getDriverList/';
            }
            else
            {
                    failureMessage( response.msg);
            }

        }).fail(function (jqXHR, textStatus, errorThrown){
            failureMessage( response.msg);

        });
    }
}

function validateDriver( section )
{
    var driver_form = jQuery('form#edit_driver_form')[0];
    var is_driver_form_valid = driver_form.checkValidity();

    if( is_driver_form_valid == false)
    {
        // If the form has an error, add class to input elements
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');

            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_driver_form_valid==true);

}
