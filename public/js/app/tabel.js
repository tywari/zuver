$(document).ready(function(){ 
	dataTableLoader();
});

function dataTableLoader()
{


    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': 'Next', 'previous': 'Previous' }
        }
    });

    
    var buttonCommon = {
        exportOptions: {
            format: {
                body: function ( data, column, row, node ) {
                    // Strip $ from salary column to make it numeric
                    return column === 5 ?
                        data.replace( /[$,]/g, '' ) :
                        data;
                }
            }
        }
    };

   
    // Buttons collection
    $('.datatable-button-init-collection').DataTable({
        dom: 'Blfrtip',
        pagingType: "full_numbers" ,
        order : [ [ 1, 'asc' ] ],
		columnDefs : [ {
			orderable : false,
			targets : [ 0 ]
		} ],

        buttons: [
            {
            	
                pageSize: 'LEGAL',
                extend: 'collection',
                text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                className: 'btn btn-default',
                buttons: [
                {extend: 'copy',orientation: 'landscape',pageSize: 'LEGAL',text:'<i class="icon-copy"></i> COPY'},
                {extend: 'csv' ,orientation: 'landscape',pageSize: 'LEGAL',text:'<i class="icon-file-excel"></i> CSV'},
                {extend: 'excel',orientation: 'landscape',pageSize: 'LEGAL',text:'<i class="icon-file-excel"></i> EXCEL'},
                {extend: 'pdf',orientation: 'landscape',pageSize: 'LEGAL',text:'<i class="icon-file-pdf"></i> PDF'},
                {extend: 'print',orientation: 'landscape',pageSize: 'LEGAL',text:'<i class="icon-printer"></i> PRINT'}
                ]
            }
        ],
        responsive: false,
        fixedHeader: {
            header: true,
            footer: true
        }
    });


    // Page length
    $('.datatable-button-init-length').DataTable({
        dom: '<"datatable-header"fB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
        buttons: [
            {
                extend: 'pageLength',
                className: 'btn bg-slate-600'
            }
        ]
    });


    
        // Setup - add a text input to each footer cell
    $('#datatable-checkbox thead tr#filterrow th').each( function () {
        var title = $('#datatable-checkbox thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" class="form-control input-sm " onclick="stopPropagation(event);" placeholder="Search '+title+'" />' );
    } );
  $('th input').click(function(event) {
    event.stopPropagation();
});
    // DataTable
    var table = $('#datatable-checkbox').DataTable();
     
    // Apply the filter
    $("#datatable-checkbox thead input").on( 'keyup change', function () {
        table
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    } );

  function stopPropagation(evt) {
        if (evt.stopPropagation !== undefined) {
            evt.stopPropagation();
        } else {
            evt.cancelBubble = true;
        }
    }

 // Individual column searching with selects


    // External table additions
    // ------------------------------

    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');


  /*  // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });*/
    
}
$('.btn-toggle').click(function() {
	
	$(this).find('.btn').toggleClass('active');

	if ($(this).find('.btn-primary').size() > 0) {
		$(this).find('.btn').toggleClass('btn-primary');
	}
	if ($(this).find('.btn-danger').size() > 0) {
		$(this).find('.btn').toggleClass('btn-danger');
	}
	if ($(this).find('.btn-success').size() > 0) {
		$(this).find('.btn').toggleClass('btn-success');
	}
	if ($(this).find('.btn-info').size() > 0) {
		$(this).find('.btn').toggleClass('btn-info');
	}

	$(this).find('.btn').toggleClass('btn-default');

});

$('form').submit(function() {
	alert($(this["options"]).val());
	return false;
});
