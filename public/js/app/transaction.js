jQuery(document).ready(function(){
	transactionDependency();
});
function transactionDependency()
{
	//loadTransactionList();
	
	 $('#save_transaction_btn').click(function (event){
		 //saveTransaction(); 
	});
	 //load Transaction list for back button
       $('body').on('click','#load_transaction_btn',function(){
           /*loadTransactionList();
           $("html, body").animate({ scrollTop: 0 }, "slow");*/
           window.location.href = BASE_URL + '/transaction/getBtoCTransactionList/';
      });
       $(document).on('change', '#filter_cityId', function() {
       	loadTransactionList();
        });
}
function viewTransaction(id){
    var arr        = id.split('-');
    var transaction_id   = arr[1];
    loadTransaction(transaction_id,'view');
}
function editTransaction(id){
   var arr         = id.split('-');
   var transaction_id    = arr[1];
   loadTransaction(transaction_id,'edit'); 
}

function editTrip(id){
    var current_url = location.href;
   var checkTransactionList = current_url.substring(current_url.lastIndexOf('/') + 1); 
   if(checkTransactionList =='getBtoBTransactionList'){
       var booking_type = 'b2b';
   }
   if(checkTransactionList =='getBtoCTransactionList'){
       var booking_type = 'b2c';
   }
   var arr         = id.split('-');
   var tripid    = arr[1];
   loadTrip(tripid,'edit',booking_type); 
    $("html, body").animate({ scrollTop: 0 }, "slow");
}


function viewRouteMap(trip_id){
    var url = BASE_URL + '/trip/getMapUrl';
    jQuery.ajax({
        method: "POST",
        url: url,
        // dataType: 'JSON',
        data:{'trip_id':trip_id}
    }).done(function (response) {
        alert(response);
        /*$('#maproute_data').html(response);*/
        $('#maproute_popup').modal('show');
    });

}
function changeTransactionStatus(id)
{
	var passenger_id=0;
	var status='';
	
	var arr = id.split('-');
    passenger_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Success';
    }
    else if(arr[0]=='deactive')
    {
    	status='';
    }
    var url = BASE_URL + '/transaction/changeTransactionStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':passenger_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	window.location.href =BASE_URL + '/transaction/getBtoBTransactionList/';
        	
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}

function loadTransactionList()
{
    // Validate the transaction value again in case the user changed the input
    var url = BASE_URL + '/transaction/getBtoCTransactionList/';
    var city_id = $('#filter_cityId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call','city_id':city_id}
    }).done(function (response) {
        jQuery('#transaction-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadTransaction( transaction_id , view_mode)
{
    var url = BASE_URL + '/transaction/getDetailsById/' + transaction_id + '/' + view_mode;
    window.location.href =BASE_URL + '/transaction/getDetailsById/' + transaction_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        data: {'id': transaction_id}
    }).done(function (response) {
    	
    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert("Failed" + textStatus + jqXHR);
    });
}

function saveTransaction() {
    var isValidate=false;
 // Validate the transaction details again in case the user changed the input
    var url = BASE_URL + '/transaction/saveTransaction/';
    
    var transactionData = {};
    
    jQuery( jQuery('#edit_transaction_form :input').serializeArray() ).each(function( x , y ){
    	transactionData[y.name] = y.value;
    });
    isValidate=validateTransaction();
    if(isValidate)
    {
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: transactionData
    }).done(function (response) {
        if(response.transaction_id)
        {
        	//loadTransactionList();
        	successMessage( response.msg);
        	window.location.href = BASE_URL + '/transaction/getBtoCTransactionList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateTransaction( section )
{
    var transaction_form = jQuery('form#edit_transaction_form')[0];
    var is_transaction_form_valid = transaction_form.checkValidity();

    if( is_transaction_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_transaction_form_valid==true);

}
