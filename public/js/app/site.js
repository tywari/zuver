jQuery(document).ready(function(){
	//loadSiteList();
	
	 $('#save_site_btn').click(function (event){
	    	
		 saveSite(); 
 	
 	});
	 $('body').on('click','#cancel_site_btn',function(){
         /*loadCountryList();
         $("html, body").animate({ scrollTop: 0 }, "slow");*/
         window.location.href = BASE_URL + '/dashboard/';
    }); 
	
});
function viewSite(id){
    var arr = id.split('-');
    var site_id = arr[1];
    loadSite(site_id,'view');
}
function editSite(id){
   var arr = id.split('-');
   var site_id = arr[1];
   loadSite(site_id,'edit'); 
}
function loadSiteList()
{
    // Validate the site value again in case the user changed the input
    var url = BASE_URL + '/site/getSiteList/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json'
    }).done(function (response) {
        
        jQuery('#site-details-information').replaceWith(response.html);
        dataTableLoader();
    });
}

function loadSite( site_id , view_mode)
{
    var url = BASE_URL + '/site/getDetailsById/' + site_id + '/' + view_mode;
    window.location.href =BASE_URL + '/site/getDetailsById/' + site_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: site_id}
    }).done(function (response) {
        
        jQuery('#site-details-information').replaceWith(response.html);
        
    });
}

function saveSite() {
    var isValidate=false;
 // Validate the site details again in case the user changed the input
    var url = BASE_URL + '/site/saveSite/';
    
    var siteData = {};
    
    jQuery( jQuery('#edit_site_form :input').serializeArray() ).each(function( x , y ){
    	siteData[y.name] = y.value;
    });
    isValidate=validateSite();
    if(isValidate)
    {
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: siteData
    }).done(function (response) {
        if(response.site_id)
        {
        	loadSiteList();
        	successMessage( response.msg);
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateSite( section )
{
    var site_form = jQuery('form#edit_site_form')[0];
    var is_site_form_valid = site_form.checkValidity();

    if( is_site_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_site_form_valid==true);

}
