jQuery(document).ready(function(){
	emergencyDependency();
});

function emergencyDependency()
{
	//loadEmergencyList();
	
	 $('#save_emergency_btn').click(function (event){
		 //saveEmergency(); 
	
	});
        //load Emergency list for back button
       $('body').on('click','#load_emergency_btn',function(){
           /*loadEmergencyList();
           $("html, body").animate({ scrollTop: 0 }, "slow");*/
           window.location.href = BASE_URL + '/emergency/getEmergencyList/';
      });
       $(document).on('change', '#filter_cityId', function() {
       	loadEmergencyList();
        });
}
function viewEmergency(id){
    var arr = id.split('-');
    var emergency_id = arr[1];
    loadEmergency(emergency_id,'view');
}
function editEmergency(id){
   var arr = id.split('-');
   var emergency_id = arr[1];
   loadEmergency(emergency_id,'edit'); 
}
function changeEmergencyStatus(id)
{
	var emergency_id=0;
	var status='';
	
	var arr = id.split('-');
    emergency_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/emergency/changeEmergencyStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':emergency_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadEmergencyList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function loadEmergencyList()
{
    // Validate the emergency value again in case the user changed the input
    var url = BASE_URL + '/emergency/getEmergencyList/';
    var city_id = $('#filter_cityId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        //dataType: 'json',
        data:{'get_type':'ajax_call','city_id':city_id},
    }).done(function (response) {
        jQuery('#emergency-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadEmergency( emergency_id , view_mode)
{
    var url = BASE_URL + '/emergency/getDetailsById/' + emergency_id + '/' + view_mode;
    window.location.href =BASE_URL + '/emergency/getDetailsById/' + emergency_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: emergency_id}
    }).done(function (response) {
        
        jQuery('#emergency-details-information').replaceWith(response.html);
        emergencyDependency();
    });
}

function saveEmergency() {
    var isValidate=false;
 // Validate the emergency details again in case the user changed the input
    var url = BASE_URL + '/emergency/saveEmergency/';
    
    var emergencyData = {};
    
    jQuery( jQuery('#edit_emergency_form :input').serializeArray() ).each(function( x , y ){
    	emergencyData[y.name] = y.value;
    });
    isValidate=validateEmergency();
    if(isValidate)
    {
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: emergencyData
    }).done(function (response) {
        if(response.emergency_id)
        {
        	//loadEmergencyList();
        	successMessage( response.msg);
        	window.location.href = BASE_URL + '/emergency/getEmergencyList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateEmergency( section )
{
    var emergency_form = jQuery('form#edit_emergency_form')[0];
    var is_emergency_form_valid = emergency_form.checkValidity();

    if( is_emergency_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_emergency_form_valid==true);

}
