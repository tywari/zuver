  window.onload = function () {  
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Top Revenue Cities"    
      },
      animationEnabled: true,
      axisY: {
        title: "Month of September"
      },
      legend: {
        verticalAlign: "bottom",
        horizontalAlign: "center"
      },
      theme: "theme2",
      data: [

      {        
        type: "column",  
        showInLegend: true, 
        legendMarkerColor: "grey",
        legendText: "OMR = One million Rupee",
        dataPoints: [      
        {y: 297571, label: "Bangalore"},
        {y: 267017,  label: "Mumbai" },
        {y: 175200,  label: "Chennai"},
        {y: 154580,  label: "Delhi"},
        {y: 116000,  label: "Pune"},
        {y: 97800, label: "Kolkata"},
        {y: 20682,  label: "Hyderabad"},        
        {y: 20350,  label: "Jaipur"}        
        ]
      }   
      ]
    });
   
    chart.render();
	// Bar chart
      var ctx = document.getElementById("mybarChart");
      var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["January", "February", "March", "April", "May", "June", "July" , "August" , "September" , "October"],
          datasets: [{
            label: '# Direct Bookings',
            backgroundColor: "#26B99A",
            data: [51, 30, 40, 28, 92, 50, 45, 92, 50, 45]
          }, {
            label: '# Partners Bookings',
            backgroundColor: "#03586A",
            data: [41, 56, 25, 48, 72, 34, 12, 72, 34, 12]
          }]
        },

        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
	  
  }
  
      $(document).ready(function(){
        var options = {
          legend: false,
          responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
          type: 'doughnut',
          tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          data: {
            labels: [
              "App Booking",
              "Online Booking",
              "Call Booking"              
            ],
            datasets: [{
              data: [50, 20, 30],
              backgroundColor: [
                "#BDC3C7",
                "#9B59B6",
                "#E74C3C",
                "#26B99A",
                "#3498DB"
              ],
              hoverBackgroundColor: [
                "#CFD4D8",
                "#B370CF",
                "#E95E4F",
                "#36CAAB",
                "#49A9EA"
              ]
            }]
          },
          options: options
        });
		 // Pie chart
      var ctx = document.getElementById("pieChart");
      var data = {
        datasets: [{
          data: [120, 50],
          backgroundColor: [
           "#26B99A",
            "#3498DB"          
          ],
          label: 'My dataset' // for legend
        }],
        labels: [
          "Booking",
          "Cancellation",
          
        ]
      };

      var pieChart = new Chart(ctx, {
        data: data,
        type: 'pie',
        otpions: {
          legend: false
        }
      });

      });