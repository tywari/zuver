jQuery(document).ready(function(){
	
	userDependency();
});

function userDependency()
{
	//loadUserList();
	 $('#save_user_btn').click(function (event){
		//saveUser(); 
	});
	
    //Date Time picker script
   $('#dob').datetimepicker({
	   dayOfWeekStart : 1,
       lang           : 'en',
       startDate      : '0',
       timepicker     : false,
       closeOnDateSelect : true,
       format         : 'Y-m-d',
       scrollMonth : false,
       scrollInput : false,
       maxDate:0
   });
   $('#doj').datetimepicker({
	   dayOfWeekStart : 1,
       lang           : 'en',
       startDate      : '',
       timepicker     : false,
       closeOnDateSelect : true,
       format         : 'Y-m-d',
       scrollMonth : false,
       scrollInput : false
       
   });
   $('#dor').datetimepicker({
	   dayOfWeekStart : 1,
       lang           : 'en',
       startDate      : '',
       timepicker     : false,
       closeOnDateSelect : true,
       format         : 'Y-m-d',
       scrollMonth : false,
       scrollInput : false
       
   });
 //load user list for back button  
 $('body').on('click','#load_user_btn',function(){
   /*loadUserList();
   $("html, body").animate({ scrollTop: 0 }, "slow");*/
   window.location.href = BASE_URL + '/user/getUserList/';
 }); 
 
 $("#internalEmail").blur(function(){
	  checkUserInternalEmail();
   });
}
function viewUser(id){
    var arr = id.split('-');
    var userid = arr[1];
    loadUser(userid,'view');
}
function editUser(id){
   var arr = id.split('-');
   var userid = arr[1];
   loadUser(userid,'edit'); 
}
function changeUserStatus(id)
{
	var user_id=0;
	var status='';
	
	var arr = id.split('-');
    user_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/user/changeUserStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':user_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadUserList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function checkUserInternalEmail()
{
	var internalEmail=$('#internalEmail').val();
    var url = BASE_URL + '/user/checkUserInternalEmail/'; 
    jQuery.ajax({
        method: "POST",
        data:{'internalEmail':internalEmail},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#internalEmail').val('');
    	}
    	infoMessage(response.msg);
    });
}

function loadUserList()
{
    // Validate the user value again in case the user changed the input
    var url = BASE_URL + '/user/getUserList/'; 
    jQuery.ajax({
        method: "POST",
        data:{'get_type':'ajax_call'},
        url: url
    }).done(function (response) {
        jQuery('#user-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadUser( user_id , view_mode)
{
    //var url = BASE_URL + '/user/saveUser/';
    var url = BASE_URL + '/user/getDetailsById/' + user_id + '/' + view_mode;
    window.location.href =BASE_URL + '/user/getDetailsById/' + user_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        data: {id: user_id},
        
    }).done(function (response) {
        jQuery('#user-details-information').replaceWith(response);
        userDependency();
    });
}

function saveUser() {
    //var isValidate=false;
    //var isValidate = $('#edit_user_form')[0].checkValidity();
    // Validate the passenger details again in case the user changed the input
     
    var url = BASE_URL + '/user/saveUser/';
    
    var userData = {};
    
    jQuery( jQuery('#edit_user_form :input').serializeArray() ).each(function( x , y ){
    	userData[y.name] = y.value;
    });
    isValidate=validateUser();
    if(isValidate==true)
    {
    $('#save_user_btn').button('loading');
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: userData
    }).done(function (response) {
        if(response.user_id)
        {
        	//loadUserList();
        	successMessage( response.msg);
        	window.location.href = BASE_URL + '/user/getUserList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateUser( section )
{
    var user_form = jQuery('form#edit_user_form')[0];
    var is_user_form_valid = user_form.checkValidity();

    if( is_user_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_user_form_valid==true);

}
