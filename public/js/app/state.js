jQuery(document).ready(function(){
	stateDependency();
});

function stateDependency()
{
	//loadStateList();
	
	 $('#save_state_btn').click(function (event){
		 //saveState(); 
	});
	//load Country list for back button
       $('body').on('click','#load_state_btn',function(){
           /*loadStateList();
           $("html, body").animate({ scrollTop: 0 }, "slow");*/
           window.location.href = BASE_URL + '/state/getStateList/';
      }); 
      // $('#state-details-information').on('click','.btn-default',changeStateStatus);
}
function viewState(id){
    var arr = id.split('-');
    var state_id = arr[1];
    loadState(state_id,'view');
}
function editState(id){
   var arr = id.split('-');
   var state_id = arr[1];
   loadState(state_id,'edit'); 
}

function changeStateStatus(id)
{
	var state_id=0;
	var status='';
	
	var arr = id.split('-');
    state_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/state/changeStateStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':state_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadStateList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function loadStateList()
{
    // Validate the state value again in case the user changed the input
    var url = BASE_URL + '/state/getStateList/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'get_type':'ajax_call'},
    }).done(function (response) {
        jQuery('#state-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadState( state_id , view_mode)
{
    var url = BASE_URL + '/state/getDetailsById/' + state_id + '/' + view_mode;
    window.location.href =BASE_URL + '/state/getDetailsById/' + state_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: state_id}
    }).done(function (response) {
        
        jQuery('#state-details-information').replaceWith(response.html);
        stateDependency();
    });
}

function saveState() {
    var isValidate=false;
 // Validate the state details again in case the user changed the input
    var url = BASE_URL + '/state/saveState/';
    
    var stateData = {};
    
    jQuery( jQuery('#edit_state_form :input').serializeArray() ).each(function( x , y ){
    	stateData[y.name] = y.value;
    });
    isValidate=validateState();
    if(isValidate)
    {
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: stateData
    }).done(function (response) {
        if(response.state_id)
        {
        	//loadStateList();
        	successMessage( response.msg);
        	window.location.href = BASE_URL + '/state/getStateList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateState( section )
{
	
    var state_form = jQuery('form#edit_state_form')[0];
    var is_state_form_valid = state_form.checkValidity();

    if( is_state_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_state_form_valid==true);

}
