jQuery(document).ajaxSuccess(function() {
    setTimeout(refreshBootstrapSelect, 800);

});

function refreshBootstrapSelect()
{
    jQuery('.selectpicker').each( function(){   	
    	var id = $(this).attr('id');
    	
		//$('[data-id="'+id+'"]').parent('div').removeClass('error');
    	if(!($(this).hasClass('error')))
    	{
    		$('[data-id="'+id+'"]').removeClass('error');
    		$(this).selectpicker('refresh');
    	}
    });  
}

jQuery(document).ready(function(){
	$('#content').on('change','.selectpicker', function(){
		if($(this).attr('required') != undefined)
		{	
			var selected = $(this).find("option:selected").val();
			if(selected != '')
			{
				var id = $(this).attr('id');
				$('[data-id="'+id+'"]').removeClass('error');
				$(this).selectpicker('refresh');
			}
		}
			
	});
});