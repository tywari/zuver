jQuery(document).ready(function(){
	corporatePartnersDepencency();
});
function corporatePartnersDepencency()
{
	//loadCorporatePartnersList();
	 $('#save_corporatepartners_btn').click(function (event){
		 //saveCorporatePartners(); 
	});
      //load Partners list for back button
       $('body').on('click','#load_partners_btn',function(){
           /*loadCorporatePartnersList();
           $("html, body").animate({ scrollTop: 0 }, "slow");*/
           window.location.href = BASE_URL + '/corporatepartners/getCorporatePartnersList/';
      });
}
function changeCorporatePartnersStatus(id)
{
	var corporate_partner_id=0;
	var status='';
	
	var arr = id.split('-');
    corporate_partner_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/corporatepartners/changeCorporatePartnersStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':corporate_partner_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadCorporatePartnersList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function loadCorporatePartnersList()
{
    // Validate the corporatepartners value again in case the user changed the input
    var url = BASE_URL + '/corporatepartners/getCorporatePartnersList/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'get_type':'ajax_call'},
    }).done(function (response) {
        
        jQuery('#corporatepartners-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function viewCorpratePartner(id){
    var arr = id.split('-');
    var corporateparttner_id = arr[1];
    loadCorporatePartners(corporateparttner_id,'view');
}
function editCorpratePartner(id){
   var arr = id.split('-');
   var corporateparttner_id = arr[1];
   loadCorporatePartners(corporateparttner_id,'edit'); 
}

function loadCorporatePartners( corporatepartners_id , view_mode)
{
    var url = BASE_URL + '/corporatepartners/getDetailsById/' + corporatepartners_id + '/' + view_mode;
    window.location.href =BASE_URL + '/corporatepartners/getDetailsById/' + corporatepartners_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: corporatepartners_id}
    }).done(function (response) {
        jQuery('#corporate-partner-details-information').replaceWith(response.html);
        corporatePartnersDepencency();
    });
}

function saveCorporatePartners() {
    var isValidate=false;
 // Validate the city details again in case the user changed the input
    var url = BASE_URL + '/corporatepartners/saveCorporatePartners/';
    
    var cityData = {};
    
    jQuery( jQuery('#edit_corporatepartners_form :input').serializeArray() ).each(function( x , y ){
    	cityData[y.name] = y.value;
    });
    isValidate=validateCorporatePartners();
    if(isValidate)
    {
     $('#save_corporatepartners_btn').button('loading');
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: cityData
    }).done(function (response) {
        if(response.corporatepartners_id)
        {
        	//loadCorporatePartnersList();
        	successMessage( response.msg);
        	window.location.href = BASE_URL + '/corporatepartners/getCorporatePartnersList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateCorporatePartners( section )
{
    var corporatepartners_form = jQuery('form#edit_corporatepartners_form')[0];
    var is_corporatepartners_form_valid = corporatepartners_form.checkValidity();

    if( is_corporatepartners_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_corporatepartners_form_valid==true);

}
