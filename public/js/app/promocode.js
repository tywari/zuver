jQuery(document).ready(function(){
	promocodeDependency();
	
});

function promocodeDependency()
{
	//loadPromocodeList();
	
	 $('#save_promocode_btn').click(function (event){
		// savePromocode(); 
	});
	 
	 $('#promocode-details-information').on('change','.flat',function(){
         passengerTypeControll();
        
    });
   //date time picker script
   $('#promoStartDate').datetimepicker({
       dayOfWeekStart : 1,
       lang           : 'en',
       startDate      : '0',
       timepicker     : true,
       format         : 'Y-m-d H:i',
       step           :15
       
   });
   $('#promoEndDate').datetimepicker({
       dayOfWeekStart : 1,
       lang           : 'en',
       startDate      : '0',
       timepicker     : true,
       format         : 'Y-m-d H:i',
       step           :15,
    	   minDate: 0,
           //minTime: 0
           onShow:function( ct ){
               if($('#promoStartDate').val()!='') {
                   this.setOptions({
                    minDate:jQuery('#promoStartDate').val()?jQuery('#promoStartDate').val():false,
                    minTime:jQuery('#promoStartDate').val()?jQuery('#promoStartDate').val():false,
                   })
               }
              }
   });
   //load Promocode list for back button
       $('body').on('click','#load_promocode_btn',function(){
           /*loadPromocodeList();
           $("html, body").animate({ scrollTop: 0 }, "slow");*/
           window.location.href = BASE_URL + '/promocode/getPromocodeList/';
      });
       $(document).on('change', '#filter_cityId', function() {
       	loadPromocodeList();
        });
       
    $("#promoCode").blur(function(){
   	 checkPromocodeAvailabilty();
         });
         
         // auto complete code
     $(document).on('keyup','#passengerName',function(){
      var keyword = $(this).val();
      var url = BASE_URL + '/wallet/getPassengerInfo/';
       if(keyword.length >= 3) {
		$.ajax({ 
		type: "POST",
		url: url,
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#passengerId").css("background","#FFF url(../public/images/loader.gif) no-repeat 485px");
		},
		success: function(data){
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#passengerId").css("background","#FFF");
                   }
		});
        }
        if(keyword==''){
            $("#passengerName").val('');
            $("#suggesstion-box").html('');
            $("#passengerId").val('');
            $("#previousAmount").val('');
        }
   });
}
function passengerTypeControll()
{
	var passengerType=$('input[name=passengerType]:checked', '#edit_promocode_form').val();
	$('#passenger-details').removeClass('hidden');
	if(passengerType == 1)
	{
		$('#passenger-details').addClass('hidden');
	}
}
function selectPassenger(passengerId,passengerName,previousAmount) {
    if(passengerId > 0){
        $("#passengerName").val(passengerName);
        $("#passengerId").val(passengerId);
    }
    $("#suggesstion-box").hide();
}
function viewPromoCode(id){
    var arr = id.split('-');
    var promocode_id = arr[1];
    loadPromocode(promocode_id,'view');
}
function editPromoCode(id){
   var arr = id.split('-');
   var promocode_id = arr[1];
   loadPromocode(promocode_id,'edit'); 
}

function checkPromocodeAvailabilty()
{
	var promoCode=$('#promoCode').val();
	if(promoCode != '')
	{
    var url = BASE_URL + '/promocode/checkPromocodeAvailabilty/'; 
    jQuery.ajax({
        method: "POST",
        data:{'promoCode':promoCode},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#promoCode').val('');
    	}
    	infoMessage(response.msg);
    });
	}
}
function changePromocodeStatus(id)
{
	var promocode_id=0;
	var status='';
	
	var arr = id.split('-');
    promocode_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/promocode/changePromocodeStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':promocode_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadPromocodeList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function loadPromocodeList()
{
    // Validate the promocode value again in case the user changed the input
    var url = BASE_URL + '/promocode/getPromocodeList/';
    var city_id = $('#filter_cityId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call','city_id':city_id},
    }).done(function (response) {
        jQuery('#promocode-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadPromocode( promocode_id , view_mode)
{
    var url = BASE_URL + '/promocode/getDetailsById/' + + promocode_id + '/' + view_mode;
    window.location.href =BASE_URL + '/promocode/getDetailsById/' + promocode_id + '/' + view_mode;
    
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: promocode_id}
    }).done(function (response) {
        jQuery('#promocode-details-information').replaceWith(response.html);
        promocodeDependency();
    });
}

function savePromocode() {
    var isValidate=false;
 // Validate the promocode details again in case the user changed the input
    var url = BASE_URL + '/promocode/savePromocode/';
    
    var promocodeData = {};
    
    /*jQuery( jQuery('#edit_promocode_form :input').serializeArray() ).each(function( x , y ){
    	promocodeData[y.name] = y.value;
    });*/
    var promocodeData = new window.FormData($('#edit_promocode_form')[0]);
    isValidate=validatePromocode();
    if(isValidate)
    {
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: promocodeData,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
        if(response)
        {
        	//loadPromocodeList();
        	successMessage( response.msg);
        	 window.location.href = BASE_URL + '/promocode/getPromocodeList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
}

function validatePromocode( section )
{
    var promocode_form = jQuery('form#edit_promocode_form')[0];
    var is_promocode_form_valid = promocode_form.checkValidity();

    if( is_promocode_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_promocode_form_valid==true);

}
