jQuery(document).ready(function(){
	$('#filter_cityId').hide();
	//date time picker script
    $('#startDate').datetimepicker({
        dayOfWeekStart : 1,
        lang           : 'en',
        startDate      : '0',
        timepicker     : false,
        format         : 'Y-m-d',
        step           :15
    });
  //date time picker script
    $('#endDate').datetimepicker({
        dayOfWeekStart : 1,
        lang           : 'en',
        startDate      : '0',
        timepicker     : false,
        format         : 'Y-m-d',
        step           :15
    });
 	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var output = d.getFullYear() + '-' +
		(month<10 ? '0' : '') + month + '-' +
		(day<10 ? '0' : '') + day;

	 $('#shiftStartDatetime').datetimepicker({
	        dayOfWeekStart : 1,
	        lang           : 'en',
	        format         : 'Y-m-d H:i',
	        step       	:30
	    });
	
	  $('#shiftEndDatetime').datetimepicker({
	        dayOfWeekStart : 1,
	        lang           : 'en',
	        format         : 'Y-m-d H:i',
	        step       	:30
	    });
	$("#shiftEndDatetime").val(output + " 00:30");
	$("#shiftStartDatetime").val(output + " 00:00");
	//loadTariffList();
	
	 $('#report-details-information').on('click','#get-complete-trip-report',loadCompleteTripReport);
	 $('#report-details-information').on('click','#get-cancelled-trip-report',loadCancelledTripReport);
	 $('#report-details-information').on('click','#get-trip-status-report',loadTripStatusReport);
	 //load Tariff list for back button
        $('body').on('click','#load_tariff_btn',function(){
            loadTariffList();
            $("html, body").animate({ scrollTop: 0 }, "slow");
       });
        $('#report-details-information').on('change','#dateFilterType',loadDateContent);
        $('#companyId').unbind('change');
   	 $('#companyId').change(function (event){
   		partnerUserControll();
   	 });
});

function partnerUserControll()
{
	var company_id=$('#companyId').val();
	$('#partner-user').addClass('hidden');
	$('#partnerUser').val('');
	if(company_id > 1)
	{
		
		var url = BASE_URL + '/user/getPartnerUserList/';
		jQuery.ajax({
            method: "POST",
            url: url,
            data:{'company_id':company_id}
        }).done(function (response) {
        	
            var result = $.parseJSON(response);
            var data = result.data;
            if(data != '')
            {
            	$('#partnerUser').html(data);
            	$('#partner-user').removeClass('hidden');   
            }
        }).fail(function (jqXHR, textStatus, errorThrown){
               
        });
	
	}
}

function loadDateContent()
{
	var date_filter_type=$(this).val();
	$('.date-filter-type').addClass('hidden');
	if(date_filter_type == 'M')
	{
		$('#startDate-filter').removeClass('hidden');
		$('#startDate').parent().find('label').html('Select Month:');
	}
	else if(date_filter_type == 'Y')
	{
		$('#startDate-filter').removeClass('hidden');
		$('#startDate').parent().find('label').html('Select Year:');
		
	}
	else if(date_filter_type == 'CD')
	{
		$('#startDate').parent().find('label').html('Start Date:');
		$('#startDate-filter').removeClass('hidden');
		$('#endDate-filter').removeClass('hidden');
		$('#endDate').prop('required',true);
	}
	
}
function loadTripStatusReport()
{
	var isValidate=false;
	 // Validate the tariff details again in case the user changed the input
	    var url = BASE_URL + '/report/getTripStatusReports/';
	    var reportData = {};
	   jQuery( jQuery('#edit_report_form :input').serializeArray() ).each(function( x , y ){
	    	reportData[y.name] = y.value;
	    });
	    isValidate=validateReport();
	    if(isValidate)
	    {
	    jQuery.ajax({
	        method: "POST",
	        url: url,
	        dataType: 'json',
	        data: reportData
	    }).done(function (response) {
	        if(response)
	        {
	        	jQuery('#report_container').html(response.html);
	        	dataTableLoader();
	        }
	        
	    }).fail(function (jqXHR, textStatus, errorThrown){
	    	failureMessage( "Failure");
	    	
	    });
	    }
	    
}
function loadCompleteTripReport()
{
	var isValidate=false;
	 // Validate the tariff details again in case the user changed the input
	    var url = BASE_URL + '/report/getCompletedTripReports/';
	    var reportData = {};
	   jQuery( jQuery('#edit_report_form :input').serializeArray() ).each(function( x , y ){
	    	reportData[y.name] = y.value;
	    });
	    isValidate=validateReport();
	    if(isValidate)
	    {
	    jQuery.ajax({
	        method: "POST",
	        url: url,
	        dataType: 'json',
	        data: reportData
	    }).done(function (response) {
	        if(response)
	        {
	        	jQuery('#report_container').html(response.html);
	        	dataTableLoader();
	        }
	        
	    }).fail(function (jqXHR, textStatus, errorThrown){
	    	failureMessage( "Failure");
	    	
	    });
	    }
	    
}

function loadCancelledTripReport()
{
	var isValidate=false;
	 // Validate the tariff details again in case the user changed the input
	    var url = BASE_URL + '/report/getCancelledTripReports/';
	    var reportData={};
	   jQuery( jQuery('#edit_report_form :input').serializeArray() ).each(function( x , y ){
	    	reportData[y.name] = y.value;
	    });
	    isValidate=validateReport();
	    if(isValidate)
	    {
	    jQuery.ajax({
	        method: "POST",
	        url: url,
	        dataType: 'json',
	        data: reportData
	    }).done(function (response) {
	        if(response)
	        {
	        	jQuery('#report_container').html(response.html);
	        	dataTableLoader();
	        }
	        
	    }).fail(function (jqXHR, textStatus, errorThrown){
	    	failureMessage( "Failure");
	    	
	    });
	    }
	    
}
function validateReport( section )
{
    var report_form = jQuery('#edit_report_form')[0];
    var is_report_form_valid = report_form.checkValidity();

    if( is_report_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_report_form_valid==true);

}