jQuery(document).ready(function () {
    tripDependency();

});

function tripDependency() {
    $("#b_b_booking").unbind('click');
    $('#b_b_booking').click(function () {
        partnerTripTypeControll();
        $('#save_b2b_booking').show();
        $('#save_b2c_booking').hide();

        $('#booking_type_title').html('B 2 B Booking');
        $('#bill_type_section').removeClass('hidden');
        $('#client_name_section').removeClass('hidden');
        $('#vechile_no_section').removeClass('hidden');
        $('#vechile_make_section').removeClass('hidden');
        $('#vechile_model_section').removeClass('hidden');
        // $('#b_b_company_section ').removeClass('hidden');
        $('#cust_mobile_section').addClass('hidden');
        $('#cust_email_section').addClass('hidden');
        $('#cust_name_section').addClass('hidden');
        // $('#b_c_company_section ').addClass('hidden');

        $("#companyId-booking option[value='1']").hide();
        $('#companyId-booking').val('');
        $('#companyId-booking').prop('selected', false);
        $('#companyId-booking').prop('disabled', false);

        $("#paymentMode option[value='W']").hide();
        $("#paymentMode option[value='P']").hide();
        $("#paymentMode option[value='C']").hide();

        $('#billType').prop('required', false);
        $('#cmobile-booking').prop('required', false);
        $('#cEmail-booking').prop('required', false);
        $('#cname-booking').prop('required', false);
        $('.compose').fadeIn('fast');
    });
    /** * B TO C Booking ** */
    $("#b_c_booking").unbind('click');
    $('#b_c_booking').click(function () {
        if ($('#save_b2b_booking').is(":visible")) {
            location.reload();
        }
        $('#save_b2b_booking').hide();
        $('#save_b2c_booking').show();
        $('#booking_type_title').html('B 2 C Booking');
        $('#bill_type_section').addClass('hidden');
        $('#createdBy').addClass('hidden');
        $('#client_name_section').addClass('hidden');
        $('#vechile_no_section').addClass('hidden');
        $('#vechile_make_section').addClass('hidden');
        $('#vechile_model_section').addClass('hidden');
        // $('#b_b_company_section ').addClass('hidden');
        $('#cust_mobile_section').removeClass('hidden');
        $('#cust_email_section').removeClass('hidden');
        $('#cust_name_section').removeClass('hidden');
        // $('#b_c_company_section ').removeClass('hidden');

        $("#companyId-booking option[value='1']").show();
        $('#companyId-booking').val('1');
        $('#companyId-booking').prop('disabled', 'disabled');

        $("#paymentMode option[value='W']").show();
        $("#paymentMode option[value='P']").show();
        $("#paymentMode option[value='C']").show();

        $('#billType').prop('required', false);
        $('#cmobile-booking').prop('required', true);
        $('#cEmail-booking').prop('required', true);
        $('#cname-booking').prop('required', true);
        $('.compose').fadeIn('fast');
        // partnerTripTypeControll();
    });
    $(".compose-close").unbind('click');
    $('.compose-close').click(function () {
        $('#booking_type_val').val('');
        $('.compose').fadeOut('fast');
    });
    $("#cancel_trip_btn").unbind('click');
    $('#cancel_trip_btn').on('click', function () {
        $('.compose').fadeOut('fast');
    });

    $("#save_b2b_booking").unbind('click');
    $('#save_b2b_booking').click(function (event) {
        saveTrip('b2b');
    });
    $("#save_b2c_booking").unbind('click');
    $('#save_b2c_booking').click(function (event) {
        saveTrip('b2c');
    });

    $("#tripType").unbind('click');
    $('#tripType').click(function (event) {
        tripTypeControll();
    });


    $("#tripType-booking").unbind('change');
    $('#tripType-booking').change(function (event) {

        tripTypeBookongControll();
        billTypeControll();

    });

    $('#companyId-booking').unbind('change');
    $('#companyId-booking').change(function (event) {
        partnerUserBookongControll();
        partnerTripTypeControll();
    });
    // date time picker script
    var pickup_time = new Date().addHours(1);

    $('#pickupDatetime-booking').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        format: 'Y-m-d H:i',
        minDateTime: pickup_time,
        minDate: 0,
        defaultTime: pickup_time,
        step: 15
    });

    // load Trip list for back button
    $('body').on('click', '#load_trip_btn', function () {
        loadTripList();
        $("html, body").animate({scrollTop: 0}, "slow");
    });

    // get passenger Details
    $("#cmobile-booking").keyup(function () {
        var keyword = $(this).val();
        if (keyword.length >= 3) {
            getPassengerDetailsForAutoSuggest(keyword, 'mobile');
        }
        if (keyword == '') {
            $("#cmobile-booking").val('');
            $("#passengerId").val('');
            $("#cEmail-booking").val('');
            $("#cname-booking").val('');
            $("#mobile-suggesstion-box").hide();
        }
    });
    $("#cEmail-booking").keyup(function () {
        var keyword = $(this).val();
        if (keyword.length >= 3) {
            getPassengerDetailsForAutoSuggest(keyword, 'email');
        }
        if (keyword == '') {
            $("#cmobile-booking").val('');
            $("#passengerId").val('');
            $("#cEmail-booking").val('');
            $("#cname-booking").val('');
            $("#email-suggesstion-box").hide();
        }
    });
    // load trip list for trip page
    var current_url = location.href;
    var checkTripList = current_url.substring(current_url.lastIndexOf('/') + 1);
    if (checkTripList == 'getBtoBTripList' || checkTripList == 'getBtoCTripList' || checkTripList == 'dashboard') {
        loadTripList();
    }
    // load trip based on city drop down
    $(document).on('change', '#filter_cityId', function () {
        // var lastPart = window.location.href.split("/").pop();
        var lastPart = window.location.href.split('/');
        if (lastPart[4] == 'driverCurrentMap') {
            // var city_id = $('#filter_cityId').val();
            // Below line Update By Aditya Sharma
            // On 24th Apr 2017
            // To solve the Map Load on Change City problem, earlier, on changing city map was not loading properly
            // apart from that old City name was being displayed even after trying to change City
            // after first change user could not Change City Name
            // 1st and 3rd problem now solved, 2nd still remaining
            location.href = BASE_URL + 'driver/driverCurrentMap';

        }
        loadTripList();
    });
    // update trip details
    $("#update_trip_details").unbind('click');
    $('#update_trip_details').on('click', updateTripDetails);

    // Driver Rejected Trip
    $("#driver_rejected").unbind('click');
    $('#driver_rejected').on('click', function () {
        driverRejected();
    });
    // Pasenger Rejected Trip
    $("#passenger_rejected").unbind('click');
    $('#passenger_rejected').on('click', function () {

        passengerRejected();

    });
    // cancel zoom car Trip
    $("#cancel_zoomcar_trip").unbind('click');
    $('#cancel_zoomcar_trip').on('click', function () {

        cancelZoomCarTrip();

    });

    // auto suggest part
    initialize();
    // tripDependency();

    $('#pickupDatetime').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        startDate: '',
        format: 'Y-m-d H:i',
        minDate: 0,
        minTime: 0,
        step: 15,
        onShow: function (ct) {
            this.setOptions({
                maxDate: jQuery('#driverAssignedDatetime').val() ? jQuery('#pickupDatetime').val() : false,
                maxTime: jQuery('#driverAssignedDatetime').val() ? jQuery('#pickupDatetime').val() : false,

            })
        }
    });
    $('#driverAssignedDatetime').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        startDate: '0',
        format: 'Y-m-d H:i',
        minDate: 0,
        minTime: 0,
        step: 15,
        onShow: function (ct) {
            if ($('#pickupDatetime').val() != '') {
                this.setOptions({
                    minDate: jQuery('#pickupDatetime').val() ? jQuery('#pickupDatetime').val() : false,
                    minTime: jQuery('#pickupDatetime').val() ? jQuery('#pickupDatetime').val() : false,
                })
            }
        }
    });
    $('#driverArrivedDatetime').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        startDate: '0',
        format: 'Y-m-d H:i',
        minDate: 0,
        step: 15,
        // minTime: 0,
        onShow: function (ct) {
            if ($('#driverAssignedDatetime').val() != '') {
                this.setOptions({
                    minDate: jQuery('#driverAssignedDatetime').val() ? jQuery('#driverAssignedDatetime').val() : false,
                    minTime: jQuery('#driverAssignedDatetime').val() ? jQuery('#driverAssignedDatetime').val() : false,
                })
            }
        }
    });
    $('#actualPickupDatetime').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        // startDate      :'0',
        format: 'Y-m-d H:i',
        //  minDate: 0,
        step: 15,
        // minTime: 0
        /* onShow:function( ct ){
         if($('#driverArrivedDatetime').val()!='') {
         this.setOptions({
         minDate:jQuery('#driverArrivedDatetime').val()?jQuery('#driverArrivedDatetime').val():false,
         minTime:jQuery('#driverArrivedDatetime').val()?jQuery('#driverArrivedDatetime').val():false,
         })
         }
         }*/
    });
    $('#dropDatetime').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        startDate: '0',
        format: 'Y-m-d H:i',
        minDate: 0,
        step: 15,
        // minTime: 0
        onShow: function (ct) {
            if ($('#actualPickupDatetime').val() != '') {
                this.setOptions({
                    minDate: jQuery('#actualPickupDatetime').val() ? jQuery('#actualPickupDatetime').val() : false,
                    minTime: jQuery('#actualPickupDatetime').val() ? jQuery('#actualPickupDatetime').val() : false,
                })
            }
        }
    });
    if ($('#trip-status').val() == 'WPP') {
        $("#paymentMode option[value='W']").hide();
        $("#paymentMode option[value='P']").hide();
        $("#paymentMode option[value='I']").hide();
        // $('#paymentMode').val('C');
    }
    else {
        $("#paymentMode option[value='W']").show();
        $("#paymentMode option[value='P']").show();
        $("#paymentMode option[value='I']").show();
    }
    var current_url = location.href;
    var checkTripList = current_url.substring(current_url.lastIndexOf('/') + 1);
    if (checkTripList == 'getBtoBTripList') {
        var booking_type = 'b2b';
    }
    if (checkTripList == 'getBtoCTripList') {
        var booking_type = 'b2c';
    }
    if (booking_type == 'b2b') {
        $('#b_b_bill_type_sec').removeClass('hidden');

        $("#paymentMode option[value='W']").hide();
        $("#paymentMode option[value='P']").hide();
        $("#paymentMode option[value='C']").hide();
    }
    if (booking_type == 'b2c') {
        $('#b_b_bill_type_sec').addClass('hidden');
    }

}


function tripTypeControll() {
    var tripType = $('#tripType').val();
    $('#drop-date-time').addClass('hidden');
    $('#drop-location').addClass('hidden');
    if (tripType == 'O') {
        // $('#drop-date-time').removeClass('hidden');
        $('#drop-location').removeClass('hidden');
        $('#drop_location').val('');
        $('#dropLongitude').val('');
        $('#dropLatitude').val('');
    }
    else if (tripType == 'XO' || tripType == 'XR') {
        // $('#drop-date-time').removeClass('hidden');
        $('#drop-location').removeClass('hidden');
        $('#drop_location').val('');
        $('#dropLongitude').val('');
        $('#dropLatitude').val('');
    }
    else if (tripType == 'R') {
        $('#drop_location').val($('#searchTextField').val());
        $('#dropLatitude').val($('#cityLat').val());
        $('#dropLongitude').val($('#cityLng').val());
    }
    else {
        $('#drop_location').val('');
        $('#dropLongitude').val('');
        $('#dropLatitude').val('');
    }

}
function tripTypeBookongControll() {
    var tripType = $('#tripType-booking').val();
    $('#drop-date-time').addClass('hidden');
    $('#drop-location-booking').addClass('hidden');
    if (tripType == 'O') {
        // $('#drop-date-time').removeClass('hidden');
        $('#drop-location-booking').removeClass('hidden');
        $('#drop_location').prop('required', true);
        $('#drop_location').val('');
        $('#dropLongitude-booking').val('');
        $('#dropLatitude-booking').val('');
    }
    else if (tripType == 'XO' || tripType == 'XR') {
        // $('#drop-date-time').removeClass('hidden');
        $('#drop-location-booking').removeClass('hidden');
        $('#drop_location').prop('required', true);
        $('#drop_location').val('');
        $('#dropLongitude-booking').val('');
        $('#dropLatitude-booking').val('');
    }
    else if (tripType == 'R') {
        $('#drop_location').val($('#searchTextField').val());
        $('#drop_location').prop('required', false);
        $('#dropLatitude-booking').val($('#cityLat').val());
        $('#dropLongitude-booking').val($('#cityLng').val());
    }
    else {
        $('#drop_location').val('');
        $('#drop_location').prop('required', false);
        $('#dropLongitude-booking').val('');
        $('#dropLatitude-booking').val('');
    }

}

function partnerUserBookongControll() {
    var company_id = $('#companyId-booking').val();
    $('#createdBy').addClass('hidden');
    $('#createdBy-booking').val('');
    if (company_id == 'undefined') {
        company_id = $('#companyId').val();
    }
    if (company_id > 1) {

        var url = BASE_URL + '/user/getPartnerUserList/';
        jQuery.ajax({
            method: "POST",
            url: url,
            data: {'company_id': company_id}
        }).done(function (response) {

            var result = $.parseJSON(response);
            var data = result.data;
            if (data != '') {
                $('#createdBy-booking').html(data);
                $('#createdBy').removeClass('hidden');
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });

    }
}

function partnerTripTypeControll() {
    var company_id = $('#companyId-booking').val();
    if (typeof company_id === 'undefined') {
        company_id = $('#companyId').val();
    }
    $('#createdBy-booking').val('');
    if (company_id > 1) {

        var url = BASE_URL + '/tariff/getPartnerTripTypeList/';
        jQuery.ajax({
            method: "POST",
            url: url,
            data: {'company_id': company_id}
        }).done(function (response) {

            var result = $.parseJSON(response);
            var data = result.data;
            var status = result.status;
            if (data != '' && status > 0) {
                $('#tripType-booking').html(data);

            }
            if (status <= 0) {
                $('#companyId-booking').val('');
                alert('Rate card not found, Please create rate card');
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {

        });

    }
}
function billTypeControll() {
    var company_id = $('#companyId-booking').val();
    if (typeof company_id === 'undefined') {
        company_id = $('#companyId').val();
    }
    var trip_type = $('#tripType-booking').val();
    var city_name = $('#tripType-booking').find('option:selected').text();
    city_name = city_name.split('(');
    city_name = city_name[1];
    if (company_id > 1) {

        var url = BASE_URL + '/tariff/getBillTypeList/';
        jQuery.ajax({
            method: "POST",
            url: url,
            data: {'company_id': company_id, 'trip_type': trip_type, 'city_name': city_name}
        }).done(function (response) {

            var result = $.parseJSON(response);
            var data = result.data;
            var status = result.status;
            if (data != '') {
                $('#billType').html(data);

            }
        }).fail(function (jqXHR, textStatus, errorThrown) {

        });

    }
}
function getPassengerDetailsForAutoSuggest(keyword, type) {

    var url = BASE_URL + '/trip/getPassengerInfo/';
    if (keyword.length >= 3) {
        $.ajax({
            type: "POST",
            url: url,
            data: {'keyword': keyword, 'type': type},
            success: function (data) {
                if (type == 'mobile') {
                    $("#mobile-suggesstion-box").show();
                    $("#mobile-suggesstion-box").html(data);
                }
                if (type == 'email') {
                    $("#email-suggesstion-box").show();
                    $("#email-suggesstion-box").html(data);
                }

            }
        });
    }

}

function selectPassenger(passengerId, passengerMobile, passengerEmail, passengerName) {
    if (passengerId > 0) {
        $("#cmobile-booking").val(passengerMobile);
        $("#passengerId").val(passengerId);
        $("#cEmail-booking").val(passengerEmail);
        $("#cname-booking").val(passengerName);
    }
    $("#mobile-suggesstion-box").hide();
    $("#email-suggesstion-box").hide();
}


function viewTrip(id) {
    var arr = id.split('-');
    var tripid = arr[1];
    loadTrip(tripid, 'view');
}
function editTrip(id) {
    var current_url = location.href;
    var checkTripList = current_url.substring(current_url.lastIndexOf('/') + 1);
    if (checkTripList == 'getBtoBTripList') {
        var booking_type = 'b2b';
    }
    if (checkTripList == 'getBtoCTripList') {
        var booking_type = 'b2c';
    }
    var arr = id.split('-');
    var tripid = arr[1];
    loadTrip(tripid, 'edit', booking_type);
    $("html, body").animate({scrollTop: 0}, "slow");
}


function loadTripList() {

    var current_url = location.href;
    var checkTripList = current_url.substring(current_url.lastIndexOf('/') + 1);
    if (checkTripList == 'getBtoBTripList' || checkTripList == 'getBtoCTripList' || checkTripList == 'dashboard') {
        // Validate the project bhu value again in case the user changed the input
        if (checkTripList == 'getBtoBTripList' || checkTripList == 'dashboard') {
            var url = BASE_URL + '/trip/getBtoBTripList/';
        }
        if (checkTripList == 'getBtoCTripList') {
            var url = BASE_URL + '/trip/getBtoCTripList/';
        }
        var city_id = $('#filter_cityId').val();
        var trip_filter_status = $('#trip_status_filter').val();

        if (trip_filter_status) {

            trip_filter_status = $('#trip_status_filter').val();

        } else {
            trip_filter_status = 'freshTrip';
        }

         $('#loading').show();

        jQuery.ajax({
            method: "POST",
            url: url,
            data: {'get_type': 'ajax_call', 'city_id': city_id, 'trip_status': trip_filter_status}
        }).done(function (response) {

            jQuery('#trip-details-information').html(response);

            $('#loading').hide();

            $('#trip_status_filter').val(trip_filter_status);

            if (trip_filter_status == 'freshTrip') {

                $('#freshTrip_tab').addClass('active');
                $('#assigned_tab').removeClass('active');
                $('#unassigned_tab').removeClass('active');
                $('#inpogress_tab').removeClass('active');
                $('#completed_tab').removeClass('active');
                $('#cancelled_tab').removeClass('active');


            }
            if (trip_filter_status == 'assigned') {

                $('#freshTrip_tab').removeClass('active');
                $('#assigned_tab').addClass('active');
                $('#unassigned_tab').removeClass('active');
                $('#inpogress_tab').removeClass('active');
                $('#completed_tab').removeClass('active');
                $('#cancelled_tab').removeClass('active');

            }
            if (trip_filter_status == 'completed') {

                $('#freshTrip_tab').removeClass('active');
                $('#assigned_tab').removeClass('active');
                $('#unassigned_tab').removeClass('active');
                $('#inpogress_tab').removeClass('active');
                $('#completed_tab').addClass('active');
                $('#cancelled_tab').removeClass('active');

            }
            if (trip_filter_status == 'cancelled') {

                $('#freshTrip_tab').removeClass('active');
                $('#assigned_tab').removeClass('active');
                $('#unassigned_tab').removeClass('active');
                $('#inpogress_tab').removeClass('active');
                $('#completed_tab').removeClass('active');
                $('#cancelled_tab').addClass('active');

            }
            if (trip_filter_status == 'unassigned') {

                $('#freshTrip_tab').removeClass('active');
                $('#assigned_tab').removeClass('active');
                $('#unassigned_tab').addClass('active');
                $('#inpogress_tab').removeClass('active');
                $('#completed_tab').removeClass('active');
                $('#cancelled_tab').removeClass('active');
            }
            if (trip_filter_status == 'inprogress') {

                $('#freshTrip_tab').removeClass('active');
                $('#assigned_tab').removeClass('active');
                $('#unassigned_tab').removeClass('active');
                $('#inpogress_tab').addClass('active');
                $('#completed_tab').removeClass('active');
                $('#cancelled_tab').removeClass('active');
            }
            dataTableLoader();
        });

    }
}
function loadTrip(trip_id, view_mode, booking_type) {

    // Validate the project bhu value again in case the user changed the input
    var url = BASE_URL + '/trip/getDetailsById/' + trip_id + '/' + view_mode;
    window.location.href = BASE_URL + '/trip/getDetailsById/' + trip_id + '/' + view_mode;

    jQuery.ajax({
        method: "POST",
        url: url,
        // dataType: 'JSON',
        data: {id: trip_id}
    }).done(function (response) {

    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("Failed" + textStatus + jqXHR);
    });


    // jQuery('#trip-details-information').replaceWith(response);

    // date time picker script
    // to load google map API


}

function getDriversListToAssign() {
    var url = BASE_URL + '/trip/getDriverListToAssign';
    var trip_id = $('#bookingtrip_id').val();
    var pickupLatitude = $('#latitude').val();
    var pickupLongitude = $('#longitude').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        // dataType: 'JSON',
        data: {
            'tripid': trip_id,
            'pickupLatitude': pickupLatitude,
            'pickupLongitude': pickupLongitude
        }
    }).done(function (response) {
        $('#driver_list').html(response);
    });

}

function getSubTrips() {
    var url = BASE_URL + '/trip/getSubTrips';
    var trip_id = $('#masterTripId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        // dataType: 'JSON',
        data: {'masterTripId': trip_id}
    }).done(function (response) {
        $('#subtrip_list').html(response);
        $('#subtripsListModal').modal('show');
    });

}
/* Created By Aditya Sharma on 26th Apr 2017 */
/* Copied from the original code mentioned below */
/* This code will help assign drivers from Google Map itself */
function AssignDrivers_GoogleMap(driverid) {
    var trip_id = $('#bookingtrip_id').val();
    // Assign Driver id to input hidden field
    $('#driver_id').val(driverid);

    $(".assigndriver").LoadingOverlay("hide");
    $(".assigndriver").LoadingOverlay("show");

    var url = BASE_URL + '/trip/AssignDriverToTrip/';
    jQuery.ajax({
        method: "POST",
        url: url,
        data: {'driverid': driverid, 'tripid': trip_id}
    }).done(function (response) {
        var result = $.parseJSON(response);
        var status = result.status;
        if (status > 0) {
            //$('.assigndriver').slideToggle();
            successMessage(result.message);
            $("#assigndriver").addClass("disabled");
            $(".assigndriver").LoadingOverlay("hide");
            // $('#controller-messages').delay(1000).fadeOut();
            var tpid = 'trip-' + trip_id;
            editTrip(tpid);
        }
        else {
            failureMessage(result.message);
            $(".assigndriver").LoadingOverlay("hide");
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        failureMessage('Driver Assigned failed???');
        $(".assigndriver").LoadingOverlay("hide");

    });
}

function AssignDrivers(id) {
    var arr = id.split('-');
    var driverid = arr[1];
    var trip_id = $('#bookingtrip_id').val();
    // Assign Driver id to input hidden field
    $('#driver_id').val(driverid);

    if (confirm("Do you want to assign driver to tripId " + trip_id + "?")) {
        $(".assigndriver").LoadingOverlay("hide");
        $(".assigndriver").LoadingOverlay("show");

        var url = BASE_URL + '/trip/AssignDriverToTrip/';
        jQuery.ajax({
            method: "POST",
            url: url,
            data: {'driverid': driverid, 'tripid': trip_id}
        }).done(function (response) {
            var result = $.parseJSON(response);
            var status = result.status;
            if (status > 0) {
                $('.assigndriver').slideToggle();
                successMessage(result.message);
                $("#assigndriver").addClass("disabled");
                $(".assigndriver").LoadingOverlay("hide");
                // $('#controller-messages').delay(1000).fadeOut();
                var tpid = 'trip-' + trip_id;
                editTrip(tpid);
            }
            else {
                failureMessage(result.message);
                $(".assigndriver").LoadingOverlay("hide");
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            failureMessage('Driver Assigned failed???');
            $(".assigndriver").LoadingOverlay("hide");

        });
    }

}
function paymentConfirm() {
    var trip_id = $('#bookingtrip_id').val();


    var url = BASE_URL + '/trip/paymentConfirmation/';
    if (confirm("Make sure payment mode changed to cash & payment collected by driver?.")) {
        $("#trip-details-information").LoadingOverlay("hide");
        $("#trip-details-information").LoadingOverlay("show");
        var post_data = {"trip_id": trip_id};
        jQuery.ajax({
            method: "POST",
            url: url,
            data: post_data
        }).done(function (response) {
            var result = $.parseJSON(response);
            var status = result.status;
            if (status > 0) {
                successMessage(result.message);
                $("#trip-details-information").LoadingOverlay("hide");
                var tpid = 'trip-' + trip_id;
                editTrip(tpid);

            } else {
                failureMessage(result.message);
                $("#trip-details-information").LoadingOverlay("hide");
                var tpid = 'trip-' + trip_id;
                editTrip(tpid);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            failureMessage('Payment Confiramtion failed???');
            $("#trip-details-information").LoadingOverlay("hide");

        });
    } else {
        failureMessage("Please try again later.");
    }

}
function driverArrived() {
    var trip_id = $('#bookingtrip_id').val();
    var driver_id = $('#driver_id').val();
    if (driver_id > 0) {
        var url = BASE_URL + '/trip/DriverArrived/';
        if (confirm("Make sure with driver is arrived or not? Before changing the trip status.")) {
            $("#trip-details-information").LoadingOverlay("hide");
            $("#trip-details-information").LoadingOverlay("show");
            var post_data = {"trip_id": trip_id};
            jQuery.ajax({
                method: "POST",
                url: url,
                data: post_data
            }).done(function (response) {
                var result = $.parseJSON(response);
                var status = result.status;
                if (status > 0) {
                    successMessage(result.message);
                    var tpid = 'trip-' + trip_id;
                    editTrip(tpid);
                    $("#trip-details-information").LoadingOverlay("hide");

                } else {
                    failureMessage(result.message);
                    $("#trip-details-information").LoadingOverlay("hide");
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                failureMessage('Trip Status change failed???');
                $("#trip-details-information").LoadingOverlay("hide");

            });
        } else {
            failureMessage("Please try again later.");
        }
    } else {
        alert('Please Assign Driver for Trip');
    }
}
function startTrip() {
    var trip_id = $('#bookingtrip_id').val();
    var driver_id = $('#driver_id').val();
    var pickup_latitude = $('#latitude').val();
    var pickup_logitude = $('#longitude').val();
    if (driver_id > 0) {
        var url = BASE_URL + '/trip/StartTrip/';
        if (confirm("Make sure with driver? Before starting the trip.")) {
            $("#trip-details-information").LoadingOverlay("hide");
            $("#trip-details-information").LoadingOverlay("show");
            var post_data = {
                "latitude": pickup_latitude,
                "driver_id": driver_id,
                "longitude": pickup_logitude,
                "status": "A",
                "trip_id": trip_id
            };
            jQuery.ajax({
                method: "POST",
                url: url,
                data: post_data
            }).done(function (response) {
                var result = $.parseJSON(response);
                var status = result.status;
                if (status > 0) {
                    successMessage(result.message);
                    $("#trip-details-information").LoadingOverlay("hide");
                    var tpid = 'trip-' + trip_id;
                    editTrip(tpid);

                } else {
                    failureMessage(result.message);
                    $("#trip-details-information").LoadingOverlay("hide");
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                failureMessage('Trip Status change failed???');
                $("#trip-details-information").LoadingOverlay("hide");

            });
        } else {
            failureMessage("Please try again later.");
        }
    } else {
        alert('Please Assign Driver for Trip');
    }
}
function endTrip() {
    var trip_id = $('#bookingtrip_id').val();
    var trip_type = $('#tripType').val();
    var driver_id = $('#driver_id').val();
    var pickup_latitude = $('#latitude').val();
    var pickup_logitude = $('#longitude').val();
    var parking_charge = $('#parkingCharge').val();
    var toll_charge = $('#tollCharge').val();
    var rating = $('#driverRating').val();
    var rating_comments = $('#driverComments').val();
    var is_zoom_car_trip = $('#is_zoom_car').val();
    var drop_time = $('#dropDatetime').val();
    if (trip_id > 0) {
        var url = BASE_URL + '/trip/EndTrip/';
        if (confirm("Are you sure end trip")) {
            $(".endtrip_popup").LoadingOverlay("hide");
            $(".endtrip_popup").LoadingOverlay("show");
            var post_data = {
                "trip_id": trip_id,
                "drop_latitude": "",
                "drop_longitude": "",
                "drop_location": "",
                "drop_time": drop_time,
                "distance": "",
                "actual_distance": "",
                "plan_type": trip_type,
                "waiting_hour": "",
                "is_admin_control": 1,
                "parking_fee": parking_charge,
                "other_fee": toll_charge,
                "rating": rating,
                "comments": rating_comments,
                "is_zoom_car_trip": is_zoom_car_trip
            };
            jQuery.ajax({
                method: "POST",
                url: url,
                data: post_data
            }).done(function (response) {
                var result = $.parseJSON(response);
                var status = result.status;
                if (status > 0) {
                    $('.endtrip_popup').slideToggle();
                    successMessage(result.message);

                    $(".endtrip_popup").LoadingOverlay("hide");

                    var tpid = 'trip-' + trip_id;
                    editTrip(tpid);

                } else {
                    failureMessage(result.message);
                    $(".endtrip_popup").LoadingOverlay("hide");
                }


            }).fail(function (jqXHR, textStatus, errorThrown) {
                failureMessage('Trip Status change failed???');
                $(".endtrip_popup").LoadingOverlay("hide");

            });
        } else {
            failureMessage("Please try again later.");
        }
    } else {
        alert('Please try again later');
    }
}

function AddSubTrip() {
    var masterTripId = $('#masterTripId').val();
    var subTrip_pickupLocation = $('#subTrip_pickupLocation').val();
    var subTrip_pickupLatitude = $('#subTrip_pickupLatitude').val();
    var subTrip_pickupLongitude = $('#subTrip_pickupLongitude').val();
    var subTrip_pickupDatetime = $('#subTrip_pickupDatetime').val();
    var subTrip_dropLocation = $('#subTrip_dropLocation').val();
    var subTrip_dropLatitude = $('#subTrip_dropLatitude').val();
    var subTrip_dropLongitude = $('#subTrip_dropLongitude').val();
    var subTrip_dropDatetime = $('#subTrip_dropDatetime').val();
    var subTrip_meterStart = $('#subTrip_meterStart').val();
    var subTrip_meterEnd = $('#subTrip_meterEnd').val();

    isValidate = validateSubTrip();
    if (isValidate) {
        if (masterTripId > 0) {
            var url = BASE_URL + '/trip/AddSubTrip/';
            if (confirm("Are you sure to add this Sub trip")) {
                $(".subtrip_popup").LoadingOverlay("hide");
                $(".subtrip_popup").LoadingOverlay("show");
                var post_data = {
                    "masterTripId": masterTripId,
                    "pickupLocation": subTrip_pickupLocation,
                    "pickupLatitude": subTrip_pickupLatitude,
                    "pickupLongitude": subTrip_pickupLongitude,
                    "pickupDatetime": subTrip_pickupDatetime,
                    "dropLocation": subTrip_dropLocation,
                    "dropLatitude": subTrip_dropLatitude,
                    "dropLongitude": subTrip_dropLongitude,
                    "dropDatetime": subTrip_dropDatetime,
                    "meterStart": subTrip_meterStart,
                    "meterEnd": subTrip_meterEnd
                };
                jQuery.ajax({
                    method: "POST",
                    url: url,
                    data: post_data
                }).done(function (response) {
                    //console.log(response);
                    var result = $.parseJSON(response);
                    var sub_trip_id = result.sub_trip_id;
                    var trip_id = result.tripId;
                    if (sub_trip_id > 0) {
                        $('.subtrip_popup').slideToggle();
                        successMessage(result.msg);

                        $(".subtrip_popup").LoadingOverlay("hide");

                        var tpid = 'trip-' + trip_id;
                        editTrip(tpid);

                    } else {
                        failureMessage(result.msg);
                        $(".subtrip_popup").LoadingOverlay("hide");
                    }


                }).fail(function (jqXHR, textStatus, errorThrown) {
                    failureMessage('Adding Sub Trip Failed???');
                    $(".subtrip_popup").LoadingOverlay("hide");

                });
            } else {
                failureMessage("Please try again later.");
            }
        } else {
            alert('Please try again later');
        }
    }
}

function driverRejected() {
    var trip_id = $('#bookingtrip_id').val();
    var driver_id = $('#driver_id').val();
    var company_id = $('#companyId').val();
    var taxi_id = '';
    var url = BASE_URL + '/trip/tripCancelledByDriver/';
    if (confirm("Make sure with driver is rejecting the trip or not? Before changing the trip status.")) {
        $("#trip-details-information").LoadingOverlay("hide");
        $("#trip-details-information").LoadingOverlay("show");
        var post_data = {
            "trip_id": trip_id,
            "taxi_id": taxi_id,
            "driver_id": driver_id,
            "company_id": company_id,
            "reason": "",
            "reject_type": "1"
        };
        jQuery.ajax({
            method: "POST",
            url: url,
            data: post_data
        }).done(function (response) {
            var result = $.parseJSON(response);
            var status = result.status;
            if (status > 0) {

                successMessage(result.message);

                $("#trip-details-information").LoadingOverlay("hide");

                var tpid = 'trip-' + trip_id;
                editTrip(tpid);

            } else {
                failureMessage(result.message);
                $("#trip-details-information").LoadingOverlay("hide");
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            failureMessage('Trip Status change failed???');
            $("#trip-details-information").LoadingOverlay("hide");

        });
    }
}

function cancelZoomCarTrip() {
    cancel_zoomcar_trip
    var trip_id = $('#bookingtrip_id').val();

    var url = BASE_URL + '/trip/cancelZoomCarTrip/';
    if (confirm("Make sure Zoom car trip to be cancel?.")) {
        $("#trip-details-information").LoadingOverlay("hide");
        $("#trip-details-information").LoadingOverlay("show");
        var post_data = {"trip_id": trip_id};
        jQuery.ajax({
            method: "POST",
            url: url,
            data: post_data
        }).done(function (response) {
            var result = $.parseJSON(response);
            var status = result.status;
            if (status > 0) {

                successMessage(result.message);

                $("#trip-details-information").LoadingOverlay("hide");

                var tpid = 'trip-' + trip_id;
                editTrip(tpid);

            } else {
                failureMessage(result.message);
                $("#trip-details-information").LoadingOverlay("hide");
            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            failureMessage('Trip Status change failed???');
            $("#trip-details-information").LoadingOverlay("hide");

        });
    }
}
function passengerRejected() {
    var trip_id = $('#bookingtrip_id').val();
    var taxi_id = '';
    var url = BASE_URL + '/trip/tripCancelledByPassenger/';
    if (confirm("Make sure you want to cancel the Trip")) {
        $("#trip-details-information").LoadingOverlay("hide");
        $("#trip-details-information").LoadingOverlay("show");
        var post_data = {
            "passenger_log_id": trip_id,
            "travel_status": "4",
            "remarks": "",
            "pay_mod_id": "0",
            "creditcard_cvv": ""
        };
        jQuery.ajax({
            method: "POST",
            url: url,
            data: post_data
        }).done(function (response) {
            var result = $.parseJSON(response);
            var status = result.status;
            if (status > 0) {

                successMessage(result.message);

                $("#trip-details-information").LoadingOverlay("hide");

                var tpid = 'trip-' + trip_id;
                editTrip(tpid);

            } else {
                failureMessage(result.message);
                $("#trip-details-information").LoadingOverlay("hide");
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            failureMessage('Trip Status change failed???');
            $("#trip-details-information").LoadingOverlay("hide");

        });
    }
}

function saveTrip(booking='') {
    // var isValidate=false;
    var url = BASE_URL + '/trip/saveTrip/';
    var tripData = {};
    var isValidate = validateTrip();
    if (isValidate == true) {

        $('#trip_booking_btn_section').hide();
        $('#trip_process_img').show();

        $(".compose").LoadingOverlay("hide");
        $(".compose").LoadingOverlay("show");

        var tripData = new window.FormData($('#edit_trip_form')[0]);
        jQuery.ajax({
            xhr: function () {
                return $.ajaxSettings.xhr();
            },
            type: "POST",
            data: tripData,
            cache: false,
            contentType: false,
            processData: false,
            url: url,
        }).done(function (response) {
            response = $.parseJSON(response);
            if (response) {
                // loadTripList();
                $('.compose').hide();
                successMessage(response.msg);
                if (booking == 'b2b') {
                    location.href = BASE_URL + '/trip/getBtoBTripList';
                }
                else if (booking == 'b2c') {
                    location.href = BASE_URL + '/trip/getBtoCTripList';
                }

                $(".compose").LoadingOverlay("hide");

            }
            else {
                failureMessage(response.msg);
                $(".compose").LoadingOverlay("hide");

            }

        }).fail(function (jqXHR, textStatus, errorThrown) {
            failureMessage(response.msg);
            $(".compose").LoadingOverlay("hide");
        });
    }
}

function setTripStatusFilter(trip_status) {

    $('#trip_status_filter').val(trip_status);
    loadTripList();
}

function updateTripDetails() {

    var trip_id = $('#bookingtrip_id').val();
    var pickup_datetime = $('#pickupDatetime').val();
    var payment_mode = $('#paymentMode').val();
    var trip_type = $('#tripType').val();
    var transmission_type = $('#transmissionType').val();
    var pickup_location = $('#pickupLocation').val();
    var pickupLatitude = $('#pickupLatitude').val();
    var pickupLongitude = $('#pickupLongitude').val();
    var dropLocation = $('#dropLocation').val();
    var dropLatitude = $('#dropLatitude').val();
    var dropLongitude = $('#dropLongitude').val();
    var driverAssignedDatetime = $('#driverAssignedDatetime').val();
    var driverArrivedDatetime = $('#driverArrivedDatetime').val();
    var dropDatetime = $('#dropDatetime').val();
    var actualPickupDatetime = $('#actualPickupDatetime').val();
    var billType = $('#billType').val();
    var carType = $('#carType').val();
    var promoCode = $('#promoCode').val();
    if (confirm("Upate Trip Details?")) {
        $("#trip-details-information").LoadingOverlay("hide");
        $("#trip-details-information").LoadingOverlay("show");
        var url = BASE_URL + '/trip/updateTripDetails/';
        jQuery.ajax({
            method: "POST",
            url: url,
            data: {
                'trip_id': trip_id,
                'pickup_dt': pickup_datetime,
                'payment_mode': payment_mode,
                'trip_type': trip_type,
                'trans_type': transmission_type,
                'p_location': pickup_location,
                'pickup_lat': pickupLatitude,
                'pickup_lon': pickupLongitude,
                'drop_location': dropLocation,
                'drop_lat': dropLatitude,
                'dp_long': dropLongitude,
                'd_assigned_date': driverAssignedDatetime,
                'd_arrived_date': driverArrivedDatetime,
                'drop_datetime': dropDatetime,
                'actual_datetime': actualPickupDatetime,
                'bill_type': billType,
                'car_type': carType,
                'promoCode': promoCode
            }
        }).done(function (response) {
            response = $.parseJSON(response);
            if (response.status > 0) {
                successMessage(response.message);

                var tpid = 'trip-' + trip_id;
                editTrip(tpid);
                $("#trip-details-information").LoadingOverlay("hide");

            }
            else {
                failureMessage(response.message);
                $("#trip-details-information").LoadingOverlay("hide");

            }
        });
    }

}

function validateTrip(section) {
    var trip_form = jQuery('form#edit_trip_form')[0];
    var is_trip_form_valid = trip_form.checkValidity();

    if (is_trip_form_valid == false) {
        // If the form has an error, add class to input elements
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function () {
            if (Boolean($(this)[0].checkValidity) && (!$(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function () {
            if (Boolean($(this)[0].checkValidity) && (!$(this)[0].checkValidity())) {
                jQuery(this).addClass('error');

            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_trip_form_valid == true);

}

function validateSubTrip(section) {
    var add_subtrip_form = jQuery('form#add_subtrip_form')[0];
    var is_add_subtrip_form_valid = add_subtrip_form.checkValidity();

    if (is_add_subtrip_form_valid == false) {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function () {
            if (Boolean($(this)[0].checkValidity) && (!$(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function () {
            if (Boolean($(this)[0].checkValidity) && (!$(this)[0].checkValidity())) {
                jQuery(this).addClass('error');

            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_add_subtrip_form_valid == true);

}
