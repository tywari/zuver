jQuery(document).ready(function(){
	tariffDependency();
   $('.ui-timepicker-input').timepicker({ 'step': 30, 'timeFormat': 'H:i:s' });
});

function tariffDependency()
{
	//loadTariffList();

	 $('#save_tariff_btn').click(function (event){
		// saveTariff();
	});
	 //load Tariff list for back button
       $('body').on('click','#load_tariff_btn',function(){
           /*loadTariffList();
           $("html, body").animate({ scrollTop: 0 }, "slow");*/
           window.location.href = BASE_URL + '/tariff/getTariffList/';
      });
      $('body').on('click','#load_driver_tariff_btn',function(){
          window.location.href = BASE_URL + '/tariff/getDriverTariffList/';
      });
       $(document).on('change', '#filter_cityId', function() {
       	loadTariffList();
        });
       $(document).on('change','#billType',loadRateCardContent);
       $(document).on('change','#tripType',loadRateCardContent);
}
function loadRateCardContent()
{
	var bill_type=$('#billType').val();
	var trip_type=$('#tripType').val();

	$('.rate-card').addClass('hidden');
	if(bill_type == 'N')
	{
		$('#normal_rate_card').removeClass('hidden');
		if(trip_type == 'XO' || trip_type == 'XR')
		{
			$('#distance_rate_card').removeClass('hidden');
		}
	}
	else if(bill_type == 'F')
	{
		$('#fixed_rate_card').removeClass('hidden');
	}
	else if(bill_type == 'D')
	{
		$('#distance_rate_card').removeClass('hidden');
	}
	else if(bill_type == 'T')
	{
		$('#time_rate_card').removeClass('hidden');
	}
	else if(bill_type == 'M')
	{
		$('#normal_rate_card').removeClass('hidden');
		$('#isMonthly').val('Y');
	}
	else
	{
		$('#no_rate_card').removeClass('hidden');
	}
}
function viewTariff(id){
    var arr = id.split('-');
    var country_id = arr[1];
    loadTariff(country_id,'view');
}
function editTariff(id){
   var arr = id.split('-');
   var country_id = arr[1];
   loadTariff(country_id,'edit');
}

function viewDriverTariff(id){
     var arr = id.split('-');
   var driver_type = arr[1];
    loadDriverTariff(driver_type,'view');
}
function editDriverTariff(id){
   var arr = id.split('-');
   var driver_type = arr[1];
    loadDriverTariff(driver_type, 'edit');
}
function changeTariffStatus(id)
{
	var tariff_id=0;
	var status='';

	var arr = id.split('-');
    tariff_id = arr[1];

    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/tariff/changeTariffStatus/';

    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':tariff_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {

        	successMessage( response.msg);
        	loadTariffList();
        }
        else
        {
        	failureMessage( response.msg);
        }

    }).fail(function (jqXHR, textStatus, errorThrown){

    	failureMessage( 'Status change failed.. Please try again later.');

    });

}
function loadTariffList()
{
    // Validate the tariff value again in case the user changed the input
    var url = BASE_URL + '/tariff/getTariffList/';
    var city_id = $('#filter_cityId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'get_type':'ajax_call','city_id':city_id},
    }).done(function (response) {
        jQuery('#tariff-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadTariff( tariff_id , view_mode)
{
    var url = BASE_URL + '/tariff/getDetailsById/' + tariff_id + '/' + view_mode;
    window.location.href =BASE_URL + '/tariff/getDetailsById/' + tariff_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: tariff_id}
    }).done(function (response) {

        jQuery('#tariff-details-information').replaceWith(response.html);
        tariffDependency();
    });
}

function loadDriverTariff( driver_type , view_mode)
{
    var url = BASE_URL + '/tariff/getDriverTariffDetailsByDriverType/' + driver_type + '/' + view_mode;
    window.location.href =BASE_URL + '/tariff/getDriverTariffDetailsByDriverType/' + driver_type + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {driverType: driver_type}
    }).done(function (response) {

        jQuery('#driver-tariff-details-information').replaceWith(response.html);
        tariffDependency();
    });
}

function saveTariff() {
    var isValidate=false;
 // Validate the tariff details again in case the user changed the input
    var url = BASE_URL + '/tariff/saveTariff/';

    var tariffData = {};

    jQuery( jQuery('#edit_tariff_form :input').serializeArray() ).each(function( x , y ){
    	tariffData[y.name] = y.value;
    });
    isValidate=validateTariff();
    if(isValidate)
    {
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: tariffData
    }).done(function (response) {
        if(response.tariff_id)
        {
        	//loadTariffList();
        	successMessage( response.msg);
        	window.location.href = BASE_URL + '/tariff/getTariffList/';
        }
        else
        {
        	failureMessage( response.msg);
        }

    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);

    });
    }

}

function saveDriverTariff() {
    var url = BASE_URL + '/tariff/saveDriverTariff/';

    var driverTariffData = {};

    jQuery( jQuery('#edit_driver_tariff_form :input').serializeArray() ).each(function( x , y ){
        driverTariffData[y.name] = y.value;
    });
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: driverTariffData
    }).done(function (response) {
        if(response.driver_type)
        {
            //loadTariffList();
            successMessage( response.msg);
            window.location.href = BASE_URL + '/tariff/getDriverTariffDetailsByDriverType/'+driver_type;
        }
        else
        {
            failureMessage(response.msg);
        }

    }).fail(function (jqXHR, textStatus, errorThrown){
        failureMessage( response.msg);

    });


}

function saveDriverLoginIncentive() {
    var url = BASE_URL + '/tariff/saveDriverLoginIncentive/';

    var driverTariffData = {};

    jQuery( jQuery('#edit_driver_login_incentive_form :input').serializeArray() ).each(function( x , y ){
        driverTariffData[y.name] = y.value;
    });
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: driverTariffData
    }).done(function (response) {
        if(response.driver_type)
        {
            //loadTariffList();
            successMessage( response.msg);
            window.location.href = BASE_URL + '/tariff/getDriverTariffDetailsByDriverType/'+driver_type;
        }
        else
        {
            failureMessage(response.msg);
        }

    }).fail(function (jqXHR, textStatus, errorThrown){
        failureMessage( response.msg);

    });


}

function validateTariff( section )
{
    var tariff_form = jQuery('#edit_tariff_form')[0];
    var is_tariff_form_valid = tariff_form.checkValidity();

    if( is_tariff_form_valid == false)
    {
        // If the form has an error, add class to input elements
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');

            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_tariff_form_valid==true);

}
