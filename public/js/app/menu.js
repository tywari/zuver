jQuery(document).ready(function(){
	//loadMenuList();
	
	 $('#save_menu_btn').click(function (event){
	    	
		 saveMenu(); 
 	
 	});
	//load driver list for back button
	   $('body').on('click','#cancel_menu_btn',function(){
	       /*loadDriverList();
	       $("html, body").animate({ scrollTop: 0 }, "slow");*/
	       window.location.href = BASE_URL + '/menu/getMenuList/';
	  });
	
});

function changeMenuStatus(id)
{
	var menu_id=0;
	var status='';
	
	var arr = id.split('-');
    menu_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/menu/changeMenuStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':menu_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadCityList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function viewMenu(id){
    var arr        = id.split('-');
    var menu_id   = arr[1];
    loadMenu(menu_id,'view');
}
function editMenu(id){
   var arr         = id.split('-');
   var menu_id    = arr[1];
   loadMenu(menu_id,'edit'); 
}

function loadMenuList()
{
    // Validate the menu value again in case the user changed the input
    var url = BASE_URL + '/menu/getMenuList/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json'
    }).done(function (response) {
        
        jQuery('#menu-details-information').replaceWith(response.html);
        dataTableLoader();
    });
}

function loadMenu( menu_id , view_mode)
{
    var url = BASE_URL + '/menu/getDetailsById/' + menu_id + '/' + view_mode;
    window.location.href =BASE_URL + '/menu/getDetailsById/' + menu_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: menu_id}
    }).done(function (response) {
        
        jQuery('#menu-details-information').replaceWith(response.html);
        
    });
}

function saveMenu() {
    var isValidate=false;
 // Validate the menu details again in case the user changed the input
    var url = BASE_URL + '/menu/saveMenu/';
    
    var menuData = {};
    
    jQuery( jQuery('#edit_menu_form :input').serializeArray() ).each(function( x , y ){
    	menuData[y.name] = y.value;
    });
    isValidate=validateMenu();
    if(isValidate)
    {
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: menuData
    }).done(function (response) {
        if(response.menu_id)
        {
        	//loadMenuList();
        	successMessage( response.msg);
        	 window.location.href = BASE_URL + '/menu/getMenuList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateMenu( section )
{
    var menu_form = jQuery('form#edit_menu_form')[0];
    var is_menu_form_valid = menu_form.checkValidity();

    if( is_menu_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_menu_form_valid==true);

}
