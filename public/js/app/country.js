jQuery(document).ready(function(){
	countryDependency();
});
function countryDependency()
{
	//loadCountryList();
	
	 $('#save_country_btn').click(function (event){
		///saveCountry(); 
	
	});
       
     //load Country list for back button
       $('body').on('click','#load_country_btn',function(){
           /*loadCountryList();
           $("html, body").animate({ scrollTop: 0 }, "slow");*/
           window.location.href = BASE_URL + '/country/getCountryList/';
      });   
}
function changeCountryStatus(id)
{
	var country_id=0;
	var status='';
	
	var arr = id.split('-');
    country_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/country/changeCountryStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':country_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadCountryList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function loadCountryList()
{
    // Validate the country value again in case the user changed the input
    var url = BASE_URL + '/country/getCountryList/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
        //dataType: 'json',
        data:{'get_type':'ajax_call'},
    }).done(function (response) {
         
        jQuery('#country-details-information').replaceWith(response);
        dataTableLoader();
    });
}
function viewCountry(id){
    var arr = id.split('-');
    var country_id = arr[1];
    loadCountry(country_id,'view');
}
function editCountry(id){
   var arr = id.split('-');
   var country_id = arr[1];
   loadCountry(country_id,'edit'); 
}
function loadCountry( country_id , view_mode)
{
    var url = BASE_URL + '/country/getDetailsById/' + country_id + '/' + view_mode;
    window.location.href =BASE_URL + '/country/getDetailsById/' + country_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: country_id}
    }).done(function (response) {
        
        jQuery('#country-details-information').replaceWith(response.html);
        countryDependency();
    });
}

function saveCountry() {
    var isValidate=false;
 // Validate the country details again in case the user changed the input
    var url = BASE_URL + '/country/saveCountry/';
    
    var countryData = {};
    
    jQuery( jQuery('#edit_country_form :input').serializeArray() ).each(function( x , y ){
    	countryData[y.name] = y.value;
    });
    isValidate=validateCountry();
    if(isValidate)
    {
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: countryData
    }).done(function (response) {
        if(response.country_id)
        {
        	//loadCountryList();
        	successMessage( response.msg);
        	window.location.href = BASE_URL + '/country/getCountryList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validateCountry( section )
{
    var country_form = jQuery('form#edit_country_form')[0];
    var is_country_form_valid = country_form.checkValidity();

    if( is_country_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_country_form_valid==true);

}
