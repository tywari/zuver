jQuery(document).ready(function(){
	cityDependency();
});
function cityDependency()
{
	//loadCityList();
	 $('#save_city_btn').click(function (event){
		 //saveCity(); 
	});
       //load Country list for back button
       $('body').on('click','#load_city_btn',function(){
          /* loadCityList();
           $("html, body").animate({ scrollTop: 0 }, "slow");*/
           window.location.href = BASE_URL + '/city/getCityList/';
      }); 
	
}
function viewCity(id){
    var arr = id.split('-');
    var city_id = arr[1];
    loadCity(city_id,'view');
}
function editCity(id){
   var arr = id.split('-');
   var city_id = arr[1];
   loadCity(city_id,'edit'); 
}

function changeCityStatus(id)
{
	var city_id=0;
	var status='';
	
	var arr = id.split('-');
    city_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/city/changeCityStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':city_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadCityList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function loadCityList()
{
    // Validate the city value again in case the user changed the input
    var url = BASE_URL + '/city/getCityList/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call'},
    }).done(function (response) {
        
        jQuery('#city-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadCity( city_id , view_mode)
{
    var url = BASE_URL + '/city/getDetailsById/' + city_id + '/' + view_mode;
    window.location.href =BASE_URL + '/city/getDetailsById/' + city_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: city_id}
    }).done(function (response) {
        
        jQuery('#city-details-information').replaceWith(response.html);
        cityDependency();
    });
}

function saveCity() {
    var isValidate=false;
 // Validate the city details again in case the user changed the input
    var url = BASE_URL + '/city/saveCity/';
    
    var cityData = {};
    
    jQuery( jQuery('#edit_city_form :input').serializeArray() ).each(function( x , y ){
    	cityData[y.name] = y.value;
    });
    isValidate=validateCity();
    if(isValidate)
    {
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: cityData
    }).done(function (response) {
        if(response.city_id)
        {
        	loadCityList();
        	successMessage( response.msg);
        	dataTableLoader();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
   
}

function validateCity( section )
{
    var city_form = jQuery('form#edit_city_form')[0];
    var is_city_form_valid = city_form.checkValidity();

    if( is_city_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_city_form_valid==true);

}
