jQuery(document).ready(function(){
	//loadPassengerList();
	passengerDependency();	
});
function passengerDependency()
{
	 $('#save_passenger_btn').click(function (event){
		 //savePassenger(); 
 	});
	 
        
        //load passenger list for back button
        $('body').on('click','#load_passenger_btn',function(){
        	 window.location.href = BASE_URL + '/passenger/getPassengerList/';
       });
        $("#email").blur(function(){
        	checkPassengerEmail();
          });
        $("#mobile").blur(function(){
        	checkPassengerMobile();
          });
        $(document).on('change', '#filter_cityId', function() {
        	loadPassengerList();
         });
}
function viewPassenger(id){
    var arr = id.split('-');
    var passenger_id = arr[1];
    loadPassenger(passenger_id,'view');
}
function editPassenger(id){
   var arr = id.split('-');
   var passenger_id = arr[1];
   loadPassenger(passenger_id,'edit'); 
}
function transctionHistory(id){
   var arr = id.split('-');
   var passenger_id = arr[1];
   loadTransctionHistory(passenger_id); 
}

function checkPassengerEmail()
{
	var email=$('#email').val();
	if(email != '')
	{
    var url = BASE_URL + '/passenger/checkPassengerEmail/'; 
    jQuery.ajax({
        method: "POST",
        data:{'email':email},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#email').val('');
    	}
    	infoMessage(response.msg);
    });
	}
}
function checkPassengerMobile()
{
	var mobile=$('#mobile').val();
	if(mobile != '')
	{
    var url = BASE_URL + '/passenger/checkPassengerMobile/'; 
    jQuery.ajax({
        method: "POST",
        data:{'mobile':mobile},
        url: url
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response.status==1)
    	{
    		$('#mobile').val('');
    	}
    	infoMessage(response.msg);
    });
	}
}
function changePassengerStatus(id)
{
	var passenger_id=0;
	var status='';
	
	var arr = id.split('-');
    passenger_id = arr[1];
    
    if(arr[0]=='active')
    {
    	status='Y';
    }
    else if(arr[0]=='deactive')
    {
    	status='N';
    }
    var url = BASE_URL + '/passenger/changePassengerStatus/';
    
    jQuery.ajax({
        method: "POST",
        url: url,
         data:{'id':passenger_id,'status':status},
    }).done(function (response) {
    	response=jQuery.parseJSON(response);
    	if(response)
        {
        	
        	successMessage( response.msg);
        	loadPassengerList();
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	
    	failureMessage( 'Status change failed.. Please try again later.');
    	
    });
    
}
function loadPassengerList()
{
    // Validate the passenger value again in case the user changed the input
    var url = BASE_URL + '/passenger/getPassengerList/';
    var city_id = $('#filter_cityId').val();
    jQuery.ajax({
        method: "POST",
        url: url,
        data:{'get_type':'ajax_call','city_id':city_id},
    }).done(function (response) {
        jQuery('#passenger-details-information').replaceWith(response);
        dataTableLoader();
    });
}

function loadPassenger( passenger_id , view_mode)
{
    var url = BASE_URL + '/passenger/getDetailsById/' + passenger_id + '/' + view_mode;
    window.location.href =BASE_URL + '/passenger/getDetailsById/' + passenger_id + '/' + view_mode;
    jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {id: passenger_id}
    }).done(function (response) {
        jQuery('#passenger-details-information').replaceWith(response.html);
        passengerDependency();
    });
}
function loadTransctionHistory( passenger_id )
{
    var url = BASE_URL + '/wallet/getWalletList/';
    
   var response =  jQuery.ajax({
        method: "POST",
        url: url,
        dataType: 'json',
        data: {'passenger_id': passenger_id,'get_type':'ajax_call'},
        async:false
    }).responseText;
    if(response){
        jQuery('#passenger-details-information').replaceWith(response);
        dataTableLoader();
    }else{
        failureMessage('Please Try Again Later');
    }
}

function savePassenger() {
    var isValidate=false;
 // Validate the passenger details again in case the user changed the input
    var url = BASE_URL + '/passenger/savePassenger/';
    
    var passengerData = {};
    var passengerData = new window.FormData($('#edit_passenger_form')[0]);
    /*jQuery( jQuery('#edit_passenger_form :input').serializeArray() ).each(function( x , y ){
    	passengerData[y.name] = y.value;
    });*/
    isValidate=validatePassenger();
    if(isValidate)
    {
    $('#save_psassenger_btn').button('loading');
    
    jQuery.ajax({
        xhr: function () {  
            return $.ajaxSettings.xhr();
        },
        type: "POST",
        data: passengerData,
        cache: false,
        contentType: false,
        processData: false,
        url: url,
    }).done(function (response) {
          response = jQuery.parseJSON(response);
        if(response.passenger_id)
        {
        	
        	successMessage( response.msg);
        	//loadPassengerList();
        	window.location.href = BASE_URL + '/passenger/getPassengerList/';
        }
        else
        {
        	failureMessage( response.msg);
        }
        
    }).fail(function (jqXHR, textStatus, errorThrown){
    	failureMessage( response.msg);
    	
    });
    }
    
}

function validatePassenger( section )
{
    var passenger_form = jQuery('form#edit_passenger_form')[0];
    var is_passenger_form_valid = passenger_form.checkValidity();

    if( is_passenger_form_valid == false)
    {
        // If the form has an error, add class to input elements 
        jQuery(':input').removeClass('error');
        jQuery(':input').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
               // addTooltipErrorMessage($(this).parents('.project_information_wrapper').find('[data-toggle="tooltip"]'));
            }
        });
        jQuery('select').removeClass('error');
        jQuery('select').each(function(){
            if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
                jQuery(this).addClass('error');
                
            }
        });
        // Focus on the first input element with an error
        jQuery(':input.error').first().focus();
    }

    return (is_passenger_form_valid==true);

}
