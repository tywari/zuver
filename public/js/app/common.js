jQuery(document).ready(function() {
	
	$('#filter_cityId').show();
	$('#filter_cityId').on('change',changeSessionCityId);
	startCounter();
});

function startCounter() {
	$('.counter').each(function(index) {
		$(this).prop('Counter', 0).animate({
			Counter : $(this).text()
		}, {
			duration : 2000,
			easing : 'swing',
			step : function(now) {
				$(this).text(parseFloat(now).toFixed(1));
			}
		});
	});
}

function changeSessionCityId() {
	var city_id = $('#filter_cityId').val();
	var url = BASE_URL + '/login/changeSessionCityId/';

	jQuery.ajax({
		method : "POST",
		url : url,
		data : {
			'city_id' : city_id
		},
	}).done(function(response) {
            var current_url = location.href;
            var checkDashboard = current_url.substring(current_url.lastIndexOf('/') + 1);
            if(checkDashboard =='dashboard'){
	            location.reload();
            }
	});
}