<?php 	
	define('ENV_DEV', 'development');
    define('ENV_TEST', 'testing');
    define('ENV_UAT', 'uat');
	define('ENV_PROD', 'production');
	
	define('ENVIRONMENT', ENV_DEV);

	/*
        $application_folder = '/opt/local/apache2/htdocs/mozaic4/application';
        $system_path = '/opt/local/apache2/htdocs/mozaic4/system';
	*/
